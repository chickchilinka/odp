package com.universeprojects.annotation.processor.aspects;

public class ProcessorAspectRequirementDefinition {
    final String name;
    final String description;
    final String aspectClass;

    public ProcessorAspectRequirementDefinition(String name, String description, String aspectClass) {
        this.name = name;
        this.description = description;
        this.aspectClass = aspectClass;
    }

    public String getName() {
        return name;
    }

    public String getAspectClass() {
        return aspectClass;
    }

    public String getDescription() {
        return description;
    }
}
