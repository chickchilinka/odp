package com.universeprojects.annotation.processor.metaClasses;

import com.universeprojects.common.shared.annotations.ServerAutoSerializable;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

@SupportedAnnotationTypes("com.universeprojects.common.shared.annotations.ServerAutoSerializable")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedOptions({"serverAutoSerializerInterface", "serverAutoSerializerClass", "serverAutoSerializerPackage"})
public class ServerAutoSerializerProcessor extends AbstractProcessor {

    public static final String GENERATED_PACKAGE = "com.universeprojects.json.server.serialization";
    public static final String GENERATED_NAME = "ServerAutoSerializerGenerated";

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        return MetaClassGenerator.INSTANCE.generateProcessor(annotations, roundEnv, processingEnv,
                GENERATED_PACKAGE, GENERATED_NAME, "com.universeprojects.json.shared.serialization.AutoSerializer",
                "serverAutoSerializer", ServerAutoSerializable.class, "AutoSerializerTemplate.vm");
    }


}
