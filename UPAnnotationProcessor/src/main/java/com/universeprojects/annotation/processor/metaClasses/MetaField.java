package com.universeprojects.annotation.processor.metaClasses;

public class MetaField {
    protected final String name;
    protected String getter;
    protected String setter;
    protected boolean publicField;
    protected boolean finalField;
    protected String primitiveWrapper;
    protected String type;
    protected boolean list;
    protected boolean map;
    protected String mapClass;
    protected String listClass;
    protected String keyClass;
    protected String valueClass;
    protected String elementClass;

    public MetaField(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getGetter() {
        return getter;
    }

    public String getSetter() {
        return setter;
    }

    public boolean isPublicField() {
        return publicField;
    }

    public boolean isFinalField() {
        return finalField;
    }

    public String getPrimitiveWrapper() {
        return primitiveWrapper;
    }

    public String getType() {
        return type;
    }

    public boolean isList() {
        return list;
    }

    public boolean isMap() {
        return map;
    }

    public String getMapClass() {
        return mapClass;
    }

    public String getListClass() {
        return listClass;
    }

    public String getKeyClass() {
        return keyClass;
    }

    public String getValueClass() {
        return valueClass;
    }

    public String getElementClass() {
        return elementClass;
    }

    public String executeGetter() {
        if(getter != null) {
            return getter+"()";
        } else {
            return name;
        }
    }

    public String executeSetter(String instance, String content) {
        if(setter != null) {
            return instance+"."+setter+"("+content+")";
        } else if(finalField && list) {
            return instance+"."+name+".clear(); "+instance+"."+name+".addAll("+content+")";
        } else if(finalField && map) {
            return instance+"."+name+".clear(); "+instance+"."+name+".putAll("+content+")";
        } else {
            return instance+"."+name+" = "+content;
        }
    }

    public boolean canExecuteGetter() {
        return getter != null || publicField;
    }

    public boolean canExecuteSetter() {
        return (!finalField || map || list) && (setter != null || publicField);
    }

    @Override
    public String toString() {
        return "MetaField{" +
            "name='" + name + '\'' +
            ", getter='" + getter + '\'' +
            ", setter='" + setter + '\'' +
            ", publicField=" + publicField +
            ", finalField=" + finalField +
            ", primitiveWrapper='" + primitiveWrapper + '\'' +
            ", type='" + type + '\'' +
            ", list=" + list +
            ", map=" + map +
            ", mapClass='" + mapClass + '\'' +
            ", listClass='" + listClass + '\'' +
            ", keyClass='" + keyClass + '\'' +
            ", valueClass='" + valueClass + '\'' +
            ", elementClass='" + elementClass + '\'' +
            '}';
    }
}
