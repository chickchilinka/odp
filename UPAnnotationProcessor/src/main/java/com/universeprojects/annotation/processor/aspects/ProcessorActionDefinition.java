package com.universeprojects.annotation.processor.aspects;

public class ProcessorActionDefinition {
    final String name;
    final String description;

    public ProcessorActionDefinition(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
