package com.universeprojects.annotation.processor.aspects;

public class ProcessorCacheDefinition {
    final String name;
    final String description;
    final String cacheType;
    final String elementClass;

    public ProcessorCacheDefinition(String name, String description, String cacheType, String elementClass) {
        this.name = name;
        this.description = description;
        this.cacheType = cacheType;
        this.elementClass = elementClass;
    }

    public String getName() {
        return name;
    }

    public String getCacheType() {
        return cacheType;
    }

    public String getElementClass() {
        return elementClass;
    }

    public String getDescription() {
        return description;
    }
}
