package com.universeprojects.annotation.processor.aspects;

enum FieldType {
    SIMPLE, LIST, MAP, LINK
}
