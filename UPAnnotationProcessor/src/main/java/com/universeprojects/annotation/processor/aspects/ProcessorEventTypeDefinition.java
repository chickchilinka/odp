package com.universeprojects.annotation.processor.aspects;

import java.util.List;

public class ProcessorEventTypeDefinition {
    final String name;
    final String description;
    final String eventTypeClass;
    final List<String> paramTypes;

    public ProcessorEventTypeDefinition(String name, String description, String eventTypeClass, List<String> paramTypes) {
        this.name = name;
        this.description = description;
        this.eventTypeClass = eventTypeClass;
        this.paramTypes = paramTypes;
    }

    public String getName() {
        return name;
    }

    public List<String> getParamTypes() {
        return paramTypes;
    }

    public String getDescription() {
        return description;
    }
}
