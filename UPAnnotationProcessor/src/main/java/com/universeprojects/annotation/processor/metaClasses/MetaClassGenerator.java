package com.universeprojects.annotation.processor.metaClasses;

import com.universeprojects.annotation.processor.TemplateGenerator;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializableMap;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MetaClassGenerator {

    public static MetaClassGenerator INSTANCE = new MetaClassGenerator();
    private ProcessingEnvironment processingEnv;

    protected MetaField getOrCreateMetaField(MetaClass<?> metaClass, String name) {
        MetaField field = metaClass.fields.get(name);
        if(field == null) {
            field = new MetaField(name);
            metaClass.fields.put(name, field);
        }
        return field;
    }

    protected void setupType(MetaField field, VariableElement variable) {
        setupType(field, variable.asType());
        determineSerializableList(field, variable);
        determineSerializableMap(field, variable);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void determineSerializableList(MetaField field, VariableElement variable) {
        SerializableList annotation = variable.getAnnotation(SerializableList.class);
        if (annotation == null) return;
        TypeElement typeElement = (TypeElement) processingEnv.getTypeUtils().asElement(processingEnv.getTypeUtils().erasure(variable.asType()));
        if (!List.class.getCanonicalName().equals(typeElement.getQualifiedName().toString())) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Element annotated with " + SerializableList.class.getCanonicalName() + " but is not of type List", variable);
            return;
        }
        field.list = true;
        //Yes - this is actually the only way to do this....
        try {
            annotation.listClass();
        } catch (MirroredTypeException me) {
            TypeMirror mirror = me.getTypeMirror();
            field.listClass = ((TypeElement) ((DeclaredType) mirror).asElement()).getQualifiedName().toString();
        }
        try {
            annotation.elementClass();
        } catch (MirroredTypeException me) {
            TypeMirror mirror = me.getTypeMirror();
            field.elementClass = ((TypeElement) ((DeclaredType) mirror).asElement()).getQualifiedName().toString();
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void determineSerializableMap(MetaField field, VariableElement variable) {
        SerializableMap annotation = variable.getAnnotation(SerializableMap.class);
        if (annotation == null) return;
        TypeElement typeElement = (TypeElement) processingEnv.getTypeUtils().asElement(processingEnv.getTypeUtils().erasure(variable.asType()));
        if (!Map.class.getCanonicalName().equals(typeElement.getQualifiedName().toString())) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Element annotated with " + SerializableMap.class.getCanonicalName() + " but is not of type Map", variable);
            return;
        }
        field.map = true;
        //Yes - this is actually the only way to do this....
        try {
            annotation.mapClass();
        } catch (MirroredTypeException me) {
            TypeMirror mirror = me.getTypeMirror();
            field.mapClass = ((TypeElement) ((DeclaredType) mirror).asElement()).getQualifiedName().toString();
        }
        try {
            annotation.keyClass();
        } catch (MirroredTypeException me) {
            TypeMirror mirror = me.getTypeMirror();
            field.keyClass = ((TypeElement) ((DeclaredType) mirror).asElement()).getQualifiedName().toString();
        }
        try {
            annotation.valueClass();
        } catch (MirroredTypeException me) {
            TypeMirror mirror = me.getTypeMirror();
            field.valueClass = ((TypeElement) ((DeclaredType) mirror).asElement()).getQualifiedName().toString();
        }
    }

    protected void setupType(MetaField field, TypeMirror type) {
        TypeKind kind = type.getKind();
        if(kind.isPrimitive()) {
            if (kind.equals(TypeKind.BOOLEAN)) {
                field.primitiveWrapper = "Boolean";
            } else if (kind.equals(TypeKind.INT)) {
                field.primitiveWrapper = "Integer";
            } else if (kind.equals(TypeKind.LONG)) {
                field.primitiveWrapper = "Long";
            } else if (kind.equals(TypeKind.DOUBLE)) {
                field.primitiveWrapper = "Double";
            } else if (kind.equals(TypeKind.FLOAT)) {
                field.primitiveWrapper = "Float";
            }
        }

        if(field.primitiveWrapper != null) {
            field.type = field.primitiveWrapper;
        } else if(kind == TypeKind.DECLARED) {
            DeclaredType classType = (DeclaredType) type;
            field.type = ((TypeElement)classType.asElement()).getQualifiedName().toString();
        }
    }

    public <A extends Annotation> List<MetaClass<A>> generateMetaClasses(Class<A> annotationClass, RoundEnvironment roundEnv, ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
        List<MetaClass<A>> annotatedTypes = new ArrayList<>();

        for(Element element : roundEnv.getElementsAnnotatedWith(annotationClass)) {
            if(!(element instanceof TypeElement) || element.getKind() != ElementKind.CLASS) {
                continue;
            }
            TypeElement type = (TypeElement)element;
            A annotation = type.getAnnotation(annotationClass);
            if(annotation == null) {
                continue;
            }


            MetaClass<A> metaClass = new MetaClass<>(type.getQualifiedName().toString(), annotation);
            TypeElement currentType = type;
            do {
                readFields(metaClass, currentType);
                TypeMirror superTypeMirror = currentType.getSuperclass();
                if(!(superTypeMirror instanceof DeclaredType)) {
                    break;
                }
                Element superTypeElement = ((DeclaredType)superTypeMirror).asElement();
                if(!(superTypeElement instanceof TypeElement)) {
                    break;
                }
                currentType = (TypeElement)superTypeElement;
            } while(!currentType.getQualifiedName().toString().equals("java.lang.Object"));
            annotatedTypes.add(metaClass);
        }
        return annotatedTypes;
    }

    protected <A extends Annotation> void readFields(MetaClass<A> metaClass, TypeElement type) {
        for(Element subElement : type.getEnclosedElements()) {
            if(subElement instanceof ExecutableElement && subElement.getKind() == ElementKind.METHOD) {
                readMethod(metaClass, (ExecutableElement) subElement);
            } else if(subElement instanceof VariableElement && subElement.getKind() == ElementKind.FIELD) {
                readSimpleField(metaClass, (VariableElement) subElement);
            }
        }
    }

    private <A extends Annotation> void readMethod(MetaClass<A> metaClass, ExecutableElement method) {
        if (method.getModifiers().contains(Modifier.STATIC) || !method.getModifiers().contains(Modifier.PUBLIC)) {
            return;
        }
        String methodName = method.getSimpleName().toString();
        readGetter(metaClass, method, methodName);
        readSetter(metaClass, method, methodName);
    }

    private <A extends Annotation> void readSimpleField(MetaClass<A> metaClass, VariableElement variable) {
        if (variable.getModifiers().contains(Modifier.STATIC)) {
            return;
        }
        MetaField field = getOrCreateMetaField(metaClass, variable.getSimpleName().toString());
        setupType(field, variable);
        field.publicField = variable.getModifiers().contains(Modifier.PUBLIC);
        field.finalField = variable.getModifiers().contains(Modifier.FINAL);
    }

    private <A extends Annotation> void readSetter(MetaClass<A> metaClass, ExecutableElement method, String methodName) {
        String capitalizedFieldName = null;
        if (methodName.startsWith("set") && methodName.length() > 3 && method.getParameters().size() == 1) {
            capitalizedFieldName = methodName.substring(3);
        }
        if (capitalizedFieldName != null) {
            String fieldName = capitalizedFieldName.substring(0, 1).toLowerCase();
            if (capitalizedFieldName.length() > 1) {
                fieldName += capitalizedFieldName.substring(1);
            }
            MetaField field = getOrCreateMetaField(metaClass, fieldName);
            field.setter = methodName;
            setupType(field, method.getParameters().get(0));
        }
    }

    private <A extends Annotation> void readGetter(MetaClass<A> metaClass, ExecutableElement method, String methodName) {
        String capitalizedFieldName = null;
        if (methodName.startsWith("get") && methodName.length() > 3 && method.getParameters().size() == 0) {
            capitalizedFieldName = methodName.substring(3);
        } else if (methodName.startsWith("is") && methodName.length() > 2 && method.getParameters().size() == 0) {
            capitalizedFieldName = methodName.substring(2);
        }
        if (capitalizedFieldName != null) {
            String fieldName = capitalizedFieldName.substring(0, 1).toLowerCase();
            if (capitalizedFieldName.length() > 1) {
                fieldName += capitalizedFieldName.substring(1);
            }
            MetaField field = getOrCreateMetaField(metaClass, fieldName);
            field.getter = methodName;
            setupType(field, method.getReturnType());
        }
    }

    @SuppressWarnings("UnusedParameters")
    public <T extends Annotation> boolean generateProcessor(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv, ProcessingEnvironment processingEnv ,
                                                  String defaultPackageName, String defaultClassName, String defaultInterfaceName, String optionPrefix, Class<T> clazz, String templateName) {
        List<MetaClass<T>> annotatedTypes = generateMetaClasses(clazz, roundEnv, processingEnv);

        if(annotatedTypes.isEmpty()) {
            return true;
        }

        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, annotatedTypes.toString());

        //noinspection RedundantCast
        Map<String, Object> params = Collections.singletonMap("classes", (Object)annotatedTypes);

        return TemplateGenerator.INSTANCE.generateTemplateProcessor(processingEnv, defaultPackageName, defaultClassName, defaultInterfaceName, optionPrefix, templateName, params);
    }
}
