package com.universeprojects.annotation.processor.metaClasses;

import com.universeprojects.common.shared.annotations.HasMetadata;
import com.universeprojects.common.shared.reflection.MetaDataOracle;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

@SupportedAnnotationTypes("com.universeprojects.common.shared.annotations.HasMetadata")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class MetaDataProcessor extends AbstractProcessor {

    public static final String GENERATED_PACKAGE = "com.universeprojects.annotation.processor";
    public static final String GENERATED_NAME = "MetaDataOracleGenerated";

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        return MetaClassGenerator.INSTANCE.generateProcessor(annotations, roundEnv, processingEnv,
                GENERATED_PACKAGE, GENERATED_NAME, MetaDataOracle.class.getCanonicalName(),
                "metaDataOracle", HasMetadata.class, "MetaDataOracleTemplate.vm");
    }

}
