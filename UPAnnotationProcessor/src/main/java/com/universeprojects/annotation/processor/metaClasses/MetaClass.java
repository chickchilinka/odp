package com.universeprojects.annotation.processor.metaClasses;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class MetaClass<A> {
    protected final Map<String, MetaField> fields = new LinkedHashMap<>();
    protected final String name;
    protected final A annotation;

    public MetaClass(String name, A annotation) {
        this.name = name;
        this.annotation = annotation;
    }

    public Collection<MetaField> getFields() {
        return fields.values();
    }

    public MetaField getField(String name) {
        return fields.get(name);
    }

    public boolean hasField(String name) {
        return fields.containsKey(name);
    }

    public String getName() {
        return name;
    }

    public A getAnnotation() {
        return annotation;
    }

    @Override
    public String toString() {
        return "MetaClass{" +
                "fields=" + fields +
                ", name='" + name + '\'' +
                '}';
    }
}
