package com.universeprojects.annotation.processor.aspects;

import com.universeprojects.annotation.processor.TemplateGenerator;
import com.universeprojects.common.shared.annotations.Aspect;
import com.universeprojects.common.shared.annotations.Description;
import com.universeprojects.common.shared.annotations.EditorConfig;
import com.universeprojects.common.shared.util.ExceptionUtils;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("Since15")
@SupportedAnnotationTypes("com.universeprojects.common.shared.annotations.Aspect")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedOptions({"aspectReflectionDataInterface", "aspectReflectionDataClass", "aspectReflectionDataPackage"})
public class AspectProcessor extends AbstractProcessor {

    public static final String GENERATED_PACKAGE = "com.universeprojects.networking.framework.reflection";
    public static final String GENERATED_NAME = "AspectReflectionDataGenerated";
    public static final String GENERATED_INTERFACE = GENERATED_PACKAGE + "AspectReflectionData";

    private static final String TYPES_PACKAGE = "com.universeprojects.networking.framework.types";

    private static final String SINGLE_LINK_FIELD_TYPE = "com.universeprojects.networking.framework.types.SingleLinkReferenceField";
    private static final String MULTI_LINK_FIELD_TYPE = "com.universeprojects.networking.framework.types.MultiLinkReferenceField";
    private static final String MAP_LINK_FIELD_TYPE = "com.universeprojects.networking.framework.types.MapLinkReferenceField";

    private TypeMirror simulatorType;

    private TypeMirror fieldType;
    private TypeMirror simpleFieldType;
    private TypeMirror listFieldType;
    private TypeMirror mapFieldType;
    private TypeMirror linkType;
    private TypeMirror cacheType;
    private TypeMirror eventTypeType;
    private TypeMirror multiLinkType;
    private TypeMirror mapLinkType;
    private TypeMirror aspectRequirementType;
    private TypeMirror actionType;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        if(processingEnv.getElementUtils().getPackageElement(TYPES_PACKAGE) == null) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "Unable to find type package "+TYPES_PACKAGE+" - not running Aspect-Processor");
            return;
        }
        simulatorType = getTypeInTypesPackage("Simulator");
        fieldType = getTypeInTypesPackage("Field");
        simpleFieldType = getTypeInTypesPackage("SimpleField");
        listFieldType = getTypeInTypesPackage("ListField");
        mapFieldType = getTypeInTypesPackage("MapField");
        linkType = getTypeInTypesPackage("Link");
        cacheType = getTypeInTypesPackage("ClearableCache");
        eventTypeType = getTypeInTypesPackage("EventType");
        multiLinkType = getTypeInTypesPackage("MultiLink");
        mapLinkType = getTypeInTypesPackage("MapLink");
        aspectRequirementType = getTypeInTypesPackage("AspectRequirement");
        actionType = getTypeInTypesPackage("Action");
    }

    private TypeMirror getTypeInTypesPackage(String simpleName) {
        return getType(TYPES_PACKAGE + "." + simpleName);
    }

    private TypeMirror getType(String qualifiedName) {
        return processingEnv.getTypeUtils().erasure(processingEnv.getElementUtils().getTypeElement(qualifiedName).asType());
    }

    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        try {
            return doProcess(roundEnv);
        } catch (Throwable ex) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Exception when running aspect-processor: " + ExceptionUtils.getStackTrace(ex));
        }
        return false;
    }

    protected boolean doProcess(RoundEnvironment roundEnv) {
        Map<String, Object> params = new HashMap<>();
        List<ProcessorAspectDefinition> aspectDefinitions = new ArrayList<>();

        for (Element aspectElement : roundEnv.getElementsAnnotatedWith(Aspect.class)) {
            TypeElement aspectType = (TypeElement) aspectElement;
            if (!aspectType.getModifiers().contains(Modifier.PUBLIC)) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                        "Found non-public class annotated with @Aspect: " + aspectType.getQualifiedName(), aspectElement);
                continue;
            }

            Aspect annotation = aspectType.getAnnotation(Aspect.class);

            final String code = aspectType.getSimpleName().toString();
            final String[] additionalCodes = annotation.additionalCodes();
            final String qualifiedName = aspectType.getQualifiedName().toString();
            final String description = getDescription(aspectType);
            final Map<String, String> editorConfig = getEditorConfig(aspectType);
            ProcessorAspectDefinition aspect = new ProcessorAspectDefinition(code, additionalCodes, qualifiedName, description, editorConfig);

            if (aspectType.getModifiers().contains(Modifier.ABSTRACT)) {
                aspect.isAbstract = true;
            }

            boolean foundConstructor = false;
            for (Element classElement : aspectType.getEnclosedElements()) {
                if (classElement.getKind() == ElementKind.FIELD) {
                    analyzeClassField(aspect, classElement);
                } else if (classElement.getKind() == ElementKind.CONSTRUCTOR && classElement.getModifiers().contains(Modifier.PUBLIC)) {
                    foundConstructor |= isCompatibleConstructor(classElement);
                }
            }

            if (!aspect.isAbstract && !foundConstructor) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                        "Unable to find a public constructor(UPObject) for aspect " + aspect.qualifiedName, aspectElement);
            }
            aspectDefinitions.add(aspect);
        }
        if (roundEnv.errorRaised()) {
            return true;
        }

        if (aspectDefinitions.isEmpty()) {
            return true;
        }

        System.out.println("Aspects: " + aspectDefinitions);

        params.put("aspects", aspectDefinitions);

        return TemplateGenerator.INSTANCE.generateTemplateProcessor(processingEnv,
                GENERATED_PACKAGE, GENERATED_NAME, GENERATED_INTERFACE,
                "aspectReflectionData", "AspectReflectionDataTemplate.vm", params);
    }

    private void analyzeClassField(ProcessorAspectDefinition aspect, Element classElement) {
        VariableElement fieldElement = (VariableElement) classElement;
        TypeMirror type = processingEnv.getTypeUtils().erasure(fieldElement.asType());

        if (processingEnv.getTypeUtils().isAssignable(type, fieldType)) {
            analyzePotentialField(aspect, fieldElement, type);
        } else if (processingEnv.getTypeUtils().isAssignable(type, linkType)) {
            analyzePotentialLink(aspect, fieldElement, type);
        } else if (processingEnv.getTypeUtils().isAssignable(type, eventTypeType)) {
            analyzePotentialEventType(aspect, fieldElement, type);
        } else if (processingEnv.getTypeUtils().isAssignable(type, cacheType)) {
            analyzePotentialCache(aspect, fieldElement, type);
        } else if (processingEnv.getTypeUtils().isAssignable(type, aspectRequirementType)) {
            analyzePotentialAspectRequirement(aspect, fieldElement);
        } else if (processingEnv.getTypeUtils().isAssignable(type, actionType)) {
            analyzePotentialAction(aspect, fieldElement, type);
        }
    }

    private void analyzePotentialEventType(ProcessorAspectDefinition aspect, VariableElement eventTypeElement, TypeMirror type) {
        String name = eventTypeElement.getSimpleName().toString();
        String description = getDescription(eventTypeElement);
        if (!eventTypeElement.getModifiers().contains(Modifier.PUBLIC) || !eventTypeElement.getModifiers().contains(Modifier.STATIC)) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                    "Found an event-type " + name + " on aspect " + aspect.qualifiedName + " which is not public static", eventTypeElement);
            return;
        }
        String typeClass = getSimpleName(getQualifiedName(type));
        DeclaredType declaredType = (DeclaredType) eventTypeElement.asType();
        List<String> types = new ArrayList<>();
        for (TypeMirror paramType : declaredType.getTypeArguments()) {
            types.add(getQualifiedName(paramType));
        }
        ProcessorEventTypeDefinition def = new ProcessorEventTypeDefinition(name, description, typeClass, types);
        aspect.eventTypes.add(def);
    }

    @SuppressWarnings("unused")
    private void analyzePotentialAction(ProcessorAspectDefinition aspect, VariableElement actionElement, TypeMirror type) {
        String name = actionElement.getSimpleName().toString();
        String description = getDescription(actionElement);
        if (!actionElement.getModifiers().contains(Modifier.PUBLIC) || !actionElement.getModifiers().contains(Modifier.STATIC)) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                "Found an action " + name + " on aspect " + aspect.qualifiedName + " which is not public static", actionElement);
            return;
        }
        ProcessorActionDefinition def = new ProcessorActionDefinition(name, description);
        aspect.actions.add(def);
    }

    private void analyzePotentialLink(ProcessorAspectDefinition aspect, VariableElement linkElement, TypeMirror type) {
        String name = linkElement.getSimpleName().toString();
        String typeClass = getQualifiedName(type);
        DeclaredType declaredType = (DeclaredType) linkElement.asType();
        String description = getDescription(linkElement);
        final Map<String, String> editorConfig = getEditorConfig(linkElement);
        String targetClassExpression = null;
        String mapKeyType = null;
        if (declaredType.getTypeArguments().size() == 2) {
            targetClassExpression = getQualifiedName(declaredType.getTypeArguments().get(1));
        }
        if (declaredType.getTypeArguments().size() == 3) {
            mapKeyType = getQualifiedName(declaredType.getTypeArguments().get(0));
            targetClassExpression = getQualifiedName(declaredType.getTypeArguments().get(2));
        }
        ProcessorLinkDefinition def = new ProcessorLinkDefinition(name, description, typeClass, targetClassExpression, mapKeyType, editorConfig);
        aspect.links.add(def);
        String linkTypeClass;
        if(processingEnv.getTypeUtils().isAssignable(type, multiLinkType)) {
            linkTypeClass = MULTI_LINK_FIELD_TYPE;
        } else if(processingEnv.getTypeUtils().isAssignable(type, mapLinkType)) {
            linkTypeClass = MAP_LINK_FIELD_TYPE;
        } else {
            linkTypeClass = SINGLE_LINK_FIELD_TYPE;
        }
        ProcessorFieldDefinition field = new ProcessorFieldDefinition(name, description, editorConfig, linkTypeClass, FieldType.LINK, null, null);
        aspect.fields.add(field);
    }

    private String getDescription(Element linkElement) {
        Description annotation = linkElement.getAnnotation(Description.class);
        if (annotation == null) return "";
        final String value = annotation.value();
        if(value.contains("\\")) {
            return value.replaceAll("\\\\", "\\\\");
        } else {
            return value;
        }
    }

    private Map<String, String> getEditorConfig(Element linkElement) {
        EditorConfig annotation = linkElement.getAnnotation(EditorConfig.class);
        if (annotation == null) return Collections.emptyMap();
        Map<String, String> map = new LinkedHashMap<>();
        map.put("showInEditor", String.valueOf(annotation.showInEditor()));
        map.put("editorFieldType", annotation.editorFieldType());
        map.put("editorFieldDefault", annotation.editorFieldDefault());
        return map;
    }

    private void analyzePotentialField(ProcessorAspectDefinition aspect, VariableElement fieldElement, TypeMirror type) {
        String name = fieldElement.getSimpleName().toString();
        DeclaredType declaredType = (DeclaredType) fieldElement.asType();
        String description = getDescription(fieldElement);
        final Map<String, String> editorConfig = getEditorConfig(fieldElement);
        FieldType fieldType;
        final List<? extends TypeMirror> typeArguments = declaredType.getTypeArguments();
        if(typeArguments.isEmpty()) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                "Found field type " + getQualifiedName(type) + " without declared type-argument", fieldElement);
            return;
        }
        String param1 = getQualifiedName(typeArguments.get(0));
        String param2 = null;
        if (processingEnv.getTypeUtils().isAssignable(type, simpleFieldType)) {
            fieldType = FieldType.SIMPLE;
        } else if (processingEnv.getTypeUtils().isAssignable(type, listFieldType)) {
            fieldType = FieldType.LIST;
        } else if (processingEnv.getTypeUtils().isAssignable(type, mapFieldType)) {
            fieldType = FieldType.MAP;
            param2 = getQualifiedName(typeArguments.get(1));
        } else {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                    "Found unrecognizable field type " + getQualifiedName(type), fieldElement);
            return;
        }
        final String typeClass = getQualifiedName(type);
        ProcessorFieldDefinition def = new ProcessorFieldDefinition(name, description, editorConfig, typeClass, fieldType, param1, param2);
        aspect.fields.add(def);
    }

    private void analyzePotentialCache(ProcessorAspectDefinition aspect, VariableElement cacheElement, TypeMirror type) {
        String name = cacheElement.getSimpleName().toString();
        String typeClass = getSimpleName(getQualifiedName(type));
        DeclaredType declaredType = (DeclaredType) cacheElement.asType();
        String description = getDescription(cacheElement);
        String cacheTypeExpression = null;
        if (declaredType.getTypeArguments().size() == 1) {
            cacheTypeExpression = getQualifiedName(declaredType.getTypeArguments().get(0));
        }
        ProcessorCacheDefinition def = new ProcessorCacheDefinition(name, description, typeClass, cacheTypeExpression);
        aspect.caches.add(def);
    }

    private void analyzePotentialAspectRequirement(ProcessorAspectDefinition aspect, VariableElement requirementElement) {
        String name = requirementElement.getSimpleName().toString();
        DeclaredType declaredType = (DeclaredType) requirementElement.asType();
        String description = getDescription(requirementElement);
        String requirementTypeExpression = null;
        if (declaredType.getTypeArguments().size() == 1) {
            requirementTypeExpression = getQualifiedName(declaredType.getTypeArguments().get(0));
        }
        ProcessorAspectRequirementDefinition def = new ProcessorAspectRequirementDefinition(name, description, requirementTypeExpression);
        aspect.aspectRequirements.add(def);
    }

    @SuppressWarnings("RedundantIfStatement")
    protected boolean isCompatibleConstructor(Element classElement) {
        ExecutableElement element = (ExecutableElement) classElement;
        List<? extends VariableElement> parameters = element.getParameters();
        if (parameters.size() == 1) {
            VariableElement objectParameter = parameters.get(0);
            if (processingEnv.getTypeUtils().isSameType(objectParameter.asType(), simulatorType)) {
                return true;
            }
        }
        return false;
    }

    protected String getSimpleName(String qualifiedName) {
        return qualifiedName.substring(qualifiedName.lastIndexOf('.') + 1);
    }

    protected String getQualifiedName(TypeMirror arg) {
        if(arg instanceof DeclaredType) {
            return ((DeclaredType) arg).asElement().toString();
        } else {
            return arg.toString();
        }
    }
}
