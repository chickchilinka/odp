package com.universeprojects.annotation.processor.aspects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ProcessorAspectDefinition {
    final String code;
    final List<String> additionalCodes;
    final String qualifiedName;
    final String description;
    final Map<String, String> editorConfig;

    boolean isAbstract = false;
    List<ProcessorFieldDefinition> fields = new ArrayList<>();
    List<ProcessorEventTypeDefinition> eventTypes = new ArrayList<>();
    List<ProcessorLinkDefinition> links = new ArrayList<>();
    List<ProcessorCacheDefinition> caches = new ArrayList<>();
    List<ProcessorAspectRequirementDefinition> aspectRequirements = new ArrayList<>();
    List<ProcessorActionDefinition> actions = new ArrayList<>();

    public ProcessorAspectDefinition(String code, String[] additionalCodes, String qualifiedName, String description, Map<String, String> editorConfig) {
        this.code = code;
        this.additionalCodes = Arrays.asList(additionalCodes);
        this.qualifiedName = qualifiedName;
        this.description = description;
        this.editorConfig = editorConfig;
    }

    public String getCode() {
        return code;
    }

    public List<String> getAdditionalCodes() {
        return additionalCodes;
    }

    public String getQualifiedName() {
        return qualifiedName;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public List<ProcessorFieldDefinition> getFields() {
        return fields;
    }

    public List<ProcessorEventTypeDefinition> getEventTypes() {
        return eventTypes;
    }

    public List<ProcessorLinkDefinition> getLinks() {
        return links;
    }

    public List<ProcessorCacheDefinition> getCaches() {
        return caches;
    }

    public String getFullReference(ProcessorEventTypeDefinition def) {
        return qualifiedName + "." + def.name;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, String> getEditorConfig() {
        return editorConfig;
    }

    public List<ProcessorAspectRequirementDefinition> getAspectRequirements() {
        return aspectRequirements;
    }

    public List<ProcessorActionDefinition> getActions() {
        return actions;
    }

    @Override
    public String toString() {
        return "ProcessorAspectDefinition{" +
                "qualifiedName='" + qualifiedName + "'" +
                '}';
    }
}
