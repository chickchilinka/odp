package com.universeprojects.annotation.processor.aspects;

import java.util.Map;

public class ProcessorLinkDefinition {
    final String name;
    final String description;
    final String linkType;
    final String targetClassExpression;
    final String mapKeyType;
    final Map<String, String> editorConfig;

    public ProcessorLinkDefinition(String name, String description, String linkType, String targetClassExpression, String mapKeyType, Map<String, String> editorConfig) {
        this.name = name;
        this.description = description;
        this.linkType = linkType;
        this.targetClassExpression = targetClassExpression;
        this.mapKeyType = mapKeyType;
        this.editorConfig = editorConfig;
    }

    public String getName() {
        return name;
    }

    public String getLinkType() {
        return linkType;
    }

    public String getTargetClassExpression() {
        return targetClassExpression;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, String> getEditorConfig() {
        return editorConfig;
    }

    public String getMapKeyType() {
        return mapKeyType;
    }
}
