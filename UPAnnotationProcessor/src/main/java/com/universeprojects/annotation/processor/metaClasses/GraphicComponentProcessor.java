package com.universeprojects.annotation.processor.metaClasses;

import com.universeprojects.common.shared.annotations.GraphicComponent;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

@SupportedAnnotationTypes("com.universeprojects.common.shared.annotations.GraphicComponent")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedOptions({"graphicComponentInitializerInterface", "graphicComponentInitializerClass", "graphicComponentInitializerPackage"})
public class GraphicComponentProcessor extends AbstractProcessor {

    public static final String GENERATED_PACKAGE = "com.universeprojects.networking.client";
    public static final String GENERATED_NAME = "GraphicComponentsInitializerGenerated";

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        return MetaClassGenerator.INSTANCE.generateProcessor(annotations, roundEnv, processingEnv,
                GENERATED_PACKAGE, GENERATED_NAME, "com.universeprojects.networking.client.GraphicComponentInitializer",
                "graphicComponentInitializer", GraphicComponent.class, "GraphicComponentInitializerTemplate.vm");
    }


}
