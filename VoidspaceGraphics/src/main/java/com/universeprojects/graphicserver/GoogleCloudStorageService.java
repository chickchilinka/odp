package com.universeprojects.graphicserver;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.tools.cloudstorage.*;
import com.universeprojects.common.shared.log.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GoogleCloudStorageService {

    public static List<String> getFileList(String bucket, String directory) {
        GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
        AppIdentityService appIdentity = AppIdentityServiceFactory.getAppIdentityService();

        ListOptions.Builder listOptions = new ListOptions.Builder();
        listOptions.setRecursive(true);
        listOptions.setPrefix(directory);

        List<String> result = new ArrayList<>();
        try {
            ListResult list = gcsService.list(bucket, listOptions.build());

            while (list.hasNext()){
                ListItem l = list.next();
                String name = l.getName();

                result.add("https://"+bucket+"/"+name);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}
