package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.NullSafe;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.elements.GCErrorMessage;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;

public abstract class GCGroupViewer {

    private final H5ELayer layer;
    private final GCSimpleWindow window;
    private final H5ELabel groupNameLabel;
    private final H5ELabel groupDescription;

    private final GCList<GCBasicListItem> membersList;
    private final GCListItemActionHandler<GCBasicListItem> actionHandler;
    private final static int LIST_ITEM_W = 170;
    private final static int LIST_ITEM_H = 35;

    protected final GCErrorMessage errorMessage;

    // admin buttons
    private final H5EButton btnL1;
    private final H5EButton btnR1;
    private final H5EButton btnR2;

    private GCGroupOptionsScreen groupOptionsScreen = null;

    private final String BTN_OPTIONS = "Options";
    private final String BTN_INVITE = "Invite";
    private final String BTN_KICK = "Kick";
    private final String BTN_LEAVE = "Leave Group";

    /**
     * Keeps track of the current user's name
     */
    private String userName = null;

    /**
     * Keeps track of the current user's admin status
     */
    private Boolean isAdmin = null;

    protected GCGroupViewer(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);

        window = new GCSimpleWindow(layer, "group-viewer", "Group Viewer", 600, 545, false);
        window.positionProportionally(.5f, .5f);
        window.onClose.registerHandler(this::clearContent);
        final int contentWidth = (int) window.getWidth();


        // GROUP IMAGE, NAME, DESCRIPTION

        //window.addElement(new H5ESprite(layer, "Group-Image-Placeholder"), Position.NEW_LINE, Alignment.LEFT, 0, 0);

        groupNameLabel = window.addH1("", Position.SAME_LINE, Alignment.LEFT, 125, 0);
        groupNameLabel.setFontScale(1);

        window.addEmptyLine(10);

        groupDescription = new H5ELabel(layer);
        groupDescription.setWidth(contentWidth);
        groupDescription.setHeight(75);
        groupDescription.setFontScale(0.6f);
        window.addElement(groupDescription, Position.NEW_LINE, Alignment.LEFT, 0, 0);

        // MEMBERS LIST

        window.addEmptyLine(15);
        H5ELabel membersLabel = window.addH2("Members", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        membersLabel.setFontScale(1);

        window.addEmptyLine(10);
        membersList = new GCList<>(layer, 2, 5, 5, true);

        window.addElement(membersList, Position.NEW_LINE, Alignment.LEFT, 0, 0);

        actionHandler = new GCListItemActionHandler<GCBasicListItem>() {
            @Override
            protected void onSelectionUpdate(GCBasicListItem lastSelectedItem) {
                // do nothing
            }
        };


        // ERROR MESSAGE

        window.addEmptyLine(10);
        errorMessage = window.addElement(new GCErrorMessage(layer), Position.NEW_LINE, Alignment.CENTER, 0, 0);

        btnL1 = window.addMediumButton("", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        btnL1.addButtonListener(() -> handleBtnClick(btnL1));

        btnR2 = window.addMediumButton("", Position.SAME_LINE, Alignment.RIGHT, -10, 0);
        btnR2.addButtonListener(() -> handleBtnClick(btnR2));

        btnR1 = window.addMediumButton("", Position.SAME_LINE, Alignment.RIGHT, -165, 0);
        btnR1.addButtonListener(() -> handleBtnClick(btnR1));

        clearContent(); // <-- start on a clean slate
    }

    public void attachGroupOptionsScreen(GCGroupOptionsScreen groupOptionsScreen) {
        this.groupOptionsScreen = Dev.checkNotNull(groupOptionsScreen);
    }

    /**
     * Returns this viewer's content to the default state
     */
    private void clearContent() {
        userName = null;
        isAdmin = null;

        groupNameLabel.setText("");
        groupDescription.setText("");
        membersList.clear();

        btnL1.setVisible(false);
        btnL1.setText("");
        btnR1.setVisible(false);
        btnR1.setText("");
        btnR2.setVisible(false);
        btnR2.setText("");

        errorMessage.clearError();
        if (groupOptionsScreen != null) {
            groupOptionsScreen.close();
        }
    }

    /**
     * Opens the window
     */
    public void open() {
        GCGroupViewerContent content = refreshContent();
        if (content != null) {
            window.open();
            updateContent(content);
        }
    }

    public void positionProportionally(float propX, float propY) {
        window.positionProportionally(propX, propY);
    }

    /**
     * Closes the window
     */
    public void close() {
        window.close();
    }

    /**
     * Toggles the window
     */
    public void toggle() {
        if (isOpen()) {
            close();
        } else {
            open();
        }
    }

    /**
     * @return TRUE if the window is open, FALSE otherwise
     */
    public boolean isOpen() {
        return window.isOpen();
    }

    public void handleBtnClick(H5EButton button) {
        String buttonText = button.getText().toString();
        switch (buttonText) {
            case BTN_OPTIONS:
                handleBtnOptions();
                break;
            case BTN_INVITE:
                handleBtnInvite();
                break;
            case BTN_KICK:
                handleBtnKick();
                break;
            case BTN_LEAVE:
                handleBtnLeaveGroup();
                break;
            default:
                Dev.fail("Unexpected button: " + buttonText);
        }
    }

    private void handleBtnOptions() {
        if (!isAdmin) {
            return; // admin only
        }

        errorMessage.clearError();
        groupOptionsScreen.open();
    }

    private void handleBtnInvite() {
        if (!isAdmin) {
            return; // admin only
        }

        errorMessage.clearError();

        String groupName = getGroupName();
        String entityName = getSelectionName();
        if (Strings.isNotEmpty(entityName)) {
            doInvite(groupName, entityName);
        }
    }

    private void handleBtnKick() {
        if (!isAdmin) {
            return; // admin only
        }

        errorMessage.clearError();

        GCBasicListItem selectedItem = actionHandler.getSelectedItem();
        if (selectedItem == null) {
            errorMessage.error("You must select one of the members in the list");
            return;
        }

        final String selectedName = selectedItem.getCaption();
        if (NullSafe.equals(userName, selectedName)) {
            errorMessage.error("You can't kick yourself out of the group, wild child");
            return;
        }

        final String groupName = getGroupName();

        // Ask the user for a confirmation
        GCInfoDialog confirmation = new GCInfoDialog(layer, true) {

            @Override
            protected String getTitleText() {
                return "Confirmation";
            }

            @Override
            protected String[] getCaptionText() {
                return new String[]{
                    "Are you sure that you would like to kick",
                    selectedName + " out of the group?",
                };
            }

            @Override
            protected String getButtonLeftText() {
                return "CONFIRM";
            }

            @Override
            protected String getButtonRightText() {
                return "CANCEL";
            }

            @Override
            protected void onLeftButtonPressed() {
                confirm();
            }

            @Override
            protected void onRightButtonPressed() {
                cancel();
            }

            @Override
            public void onDismiss() {
                cancel(); // closing the window is equivalent to "Cancel"
            }

            private void confirm() {
                // action confirmed
                doKick(groupName, selectedName);
            }

            private void cancel() {
                // changed their mind... phew! nothing needs to be done
            }

        };

        confirmation.positionProportionally(.7f, .2f);
        confirmation.open();
    }

    private void handleBtnLeaveGroup() {
        if (isAdmin) {
            return; // non-admin only
        }

        errorMessage.clearError();

        final String groupName = getGroupName();

        // Ask the user for a confirmation
        GCInfoDialog confirmation = new GCInfoDialog(layer, true) {

            @Override
            protected String getTitleText() {
                return "Confirmation";
            }

            @Override
            protected String[] getCaptionText() {
                return new String[]{
                    "Please confirm that you would like to leave the group:",
                    Strings.inQuotes(groupName),
                };
            }

            @Override
            protected String getButtonLeftText() {
                return "CONFIRM";
            }

            @Override
            protected String getButtonRightText() {
                return "CANCEL";
            }

            @Override
            protected void onLeftButtonPressed() {
                confirm();
            }

            @Override
            protected void onRightButtonPressed() {
                cancel();
            }

            @Override
            public void onDismiss() {
                cancel(); // closing the window is equivalent to "Cancel"
            }

            private void confirm() {
                // action confirmed - leave the group
                doLeaveGroup(groupName);

                // close the group viewer, cause we're not in the group any more
                GCGroupViewer.this.close();
            }

            private void cancel() {
                // changed their mind... phew! nothing needs to be done
            }

        };

        confirmation.positionProportionally(.8f, .2f);
        confirmation.open();
    }

    protected String getGroupName() {
        return groupNameLabel.getText().toString();
    }

    private void updateContent(GCGroupViewerContent content) {
        Dev.checkNotNull(content);

        clearContent();

        groupNameLabel.setText(content.groupName);
        groupDescription.setText(content.groupDescription);

        membersList.clear();
        for (GCGroupMemberContent member : content.members) {
            membersList.addItem(new GCBasicListItem(layer, actionHandler, LIST_ITEM_W, LIST_ITEM_H, member.memberName));
        }

        this.userName = content.userName;
        if (content.isAdmin) {
            this.isAdmin = true;

            btnL1.setVisible(true);
            btnL1.setText(BTN_OPTIONS);
            btnR1.setVisible(true);
            btnR1.setText(BTN_INVITE);
            btnR2.setVisible(true);
            btnR2.setText(BTN_KICK);
        } else {
            this.isAdmin = false;

            btnL1.setVisible(false);
            btnL1.setText("");
            btnR1.setVisible(false);
            btnR1.setText("");
            btnR2.setVisible(true);
            btnR2.setText(BTN_LEAVE);

        }
    }

    public static class GCGroupViewerContent {
        public String groupName = null;
        public String groupDescription = null;
        public String userName = null;
        public boolean isAdmin = false;
        public List<GCGroupMemberContent> members = new ArrayList<>();

        public GCGroupViewerContent addMember(String memberName, String memberIcon) {
            members.add(new GCGroupMemberContent(memberName, memberIcon));
            return this;
        }
    }

    public static class GCGroupMemberContent {
        public String memberName;
        public String memberIcon;

        public GCGroupMemberContent(String memberName, String memberIcon) {
            this.memberName = memberName;
            this.memberIcon = memberIcon;
        }
    }

    /**
     * Sets the content of this window, based on the current user's group
     *
     * @return {@link GCGroupViewerContent} object if the content was set successfully, NULL in case of a problem
     */
    protected abstract GCGroupViewerContent refreshContent();

    /**
     * Retrieves the name of the currently-selected entity (if applicable), or outputs an error message
     * if the entity name can't be retrieved for any reason
     *
     * @return The entity name as a string, or NULL if an entity name could not be retrieved
     */
    protected abstract String getSelectionName();

    /**
     * Implement this method to wire up the functionality of leaving a group.
     * The player's character is assumed to be a member in the group.
     *
     * @param groupName The name of the group
     */
    protected abstract void doLeaveGroup(String groupName);

    /**
     * Implement this method to wire up the functionality of inviting a new entity to a group.
     * The player's character is assumed to be an admin in the group.
     *
     * @param groupName  The name of the group
     * @param entityName The name of the entity to invite into the group
     */
    protected abstract void doInvite(String groupName, String entityName);

    /**
     * Implement this method to wire up the functionality of kicking an entity from a group.
     * The player's character is assumed to be an admin in the group.
     *
     * @param groupName  The name of the group
     * @param entityName The name of the entity to kick from the group
     */
    protected abstract void doKick(String groupName, String entityName);

}
