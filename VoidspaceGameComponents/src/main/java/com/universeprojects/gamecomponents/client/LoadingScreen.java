package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.Disposable;
import com.universeprojects.common.shared.math.UPMath;

public class LoadingScreen extends LoadingScreenBase implements Disposable  {

    private float time = 0f;
    private AssetManager am;

    public LoadingScreen(AssetManager am2) {
        super();
        this.am=am2;

    }
    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // Clear screen
        Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1f);
        time += Gdx.graphics.getDeltaTime(); // Accumulate elapsed animation time

        spriteBatch.begin();

        float backgroundScale = ((float) Gdx.graphics.getHeight()) / (float) UPMath.min(loginBackground.getHeight(), loginBackground.getWidth());
        spriteBatch.draw(loginBackground,0,0, loginBackground.getWidth() * backgroundScale, loginBackground.getHeight() * backgroundScale);

        borderPatchDrawable.draw(spriteBatch, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        spriteBatch.draw(loadingBarBackground, initialX, initialY,loadingBarBackground.getWidth(),loadingBarBackground.getHeight());
        float currentWidth=calculateBarWidth(time);
        loadingBarDrawable.draw(spriteBatch, initialX+29, initialY+13,
                currentWidth, loadingBar.getHeight());


        float factor = Gdx.graphics.getWidth() / voidspaceLogo.getWidth();
        if (factor>1) factor = 1;

        spriteBatch.draw(voidspaceLogo,
                Gdx.graphics.getWidth() / 2f - voidspaceLogo.getWidth() / 2f,
                Gdx.graphics.getHeight() * 0.9f - voidspaceLogo.getHeight(),
                voidspaceLogo.getWidth() * factor, voidspaceLogo.getHeight() * factor);

        spriteBatch.end();

    }

    @Override
    protected float calculateBarWidth(float elapsedTime) {
        float barWidth;
        barWidth=((loadingBar.getWidth() * elapsedTime)/2f )+ (am.getProgress()*1500);


        if(barWidth>889){
            barWidth=889;
        }

        if (barWidth>Gdx.graphics.getWidth())
            barWidth = Gdx.graphics.getWidth();

        return barWidth;
    }


    @Override
    public void dispose() {
        spriteBatch.dispose();
        loadingBar.dispose();
        loadingBarBackground.dispose();
        loginBackground.dispose();
        voidspaceLogo.dispose();
        loginBorders.dispose();
    }
}
