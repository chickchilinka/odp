package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.Collections;
import java.util.Map;

public class GCSimpleInventoryItem implements GCInventoryItem {
    protected String uid;
    protected Object rawEntity;
    protected String name;
    protected String itemClass;
    protected String description;
    protected int quantity;
    protected boolean stackable;
    protected String iconSpriteKey;
    protected Rarity rarity;
    protected Map<String, InspectorParameter> additionalProperties;
    protected String iconOverlaySpriteKey;
    protected boolean canBeExperimentedOn;
    protected boolean canBeCustomized;

    @Override
    public String getUid() {
        return uid;
    }

    @Override
    public Object getRawEntity() {
        return rawEntity;
    }

    @Override
    public String getName() {
        return Strings.nullToEmpty(name);
    }

    @Override
    public String getItemClass() {
        return Strings.nullToEmpty(itemClass);
    }

    @Override
    public String getDescription() {
        return Strings.nullToEmpty(description);
    }

    @Override
    public long getQuantity() {
        return quantity;
    }

    @Override
    public boolean isStackable() {
        return stackable;
    }

    @Override
    public String getIconSpriteKey() {
        return iconSpriteKey;
    }

    @Override
    public String getRarityIconStyle() {
        return rarity != null ? rarity.getIconStyle() : Rarity.COMMON.getIconStyle();
    }

    @Override
    public String getRarityText() {
        return rarity != null ? rarity.text() : Rarity.COMMON.text();
    }

    @Override
    public Color getRarityColor() {
        return rarity != null ? rarity.getColor() : Rarity.COMMON.getColor();
    }

    @Override
    public Map<String, InspectorParameter> getAdditionalProperties() {
        return additionalProperties != null ? additionalProperties : Collections.emptyMap();
    }

    @Override
    public boolean createAdditionalInspectorComponents(H5ELayer layer, Table inspectorTable) {
        return false;
    }

    @Override
    public GCInternalItemInventory getInternalInventory() {
        return null;
    }

    @Override
    public GCInternalTank getInternalTank() {
        return null;
    }

    @Override
    public String getIconOverlayKey() {
        return iconOverlaySpriteKey;
    }

    @Override
    public boolean canBeExperimentedOn() {
        return canBeExperimentedOn;
    }

    @Override
    public boolean canBeCustomized() {
        return canBeCustomized;
    }
}
