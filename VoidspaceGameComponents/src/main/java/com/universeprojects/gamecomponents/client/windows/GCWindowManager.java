package com.universeprojects.gamecomponents.client.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.common.shared.hooks.Hook;
import com.universeprojects.common.shared.hooks.Hooks;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.client.framework.H5EEngine;

import java.util.ArrayList;
import java.util.List;

public class GCWindowManager extends WindowManager {
    private static final Logger logger = Logger.getLogger(GCWindowManager.class);

    public static final int MOBILE_SCREEN_WIDTH = 450;
    public static final int MOBILE_SCREEN_HEIGHT = 750;

    public final Hook<Callable2Args<Boolean, Boolean>> HOOK_WindowFullscreen = Hooks.createHook2Args();
    private final H5EEngine engine;
    private boolean mobileMode = false;
    private boolean disableOptionsButton = true;
    private boolean skipSetMobileModeFromResize = false;

    private int screenWidth;
    private int screenHeight;

    /**
     * This deque contains references to all the windows that are currently open.
     * The first (head) entry references the "active" window.
     */
    private final List<GCWindow> openWindows = new ArrayList<>();


    public GCWindowManager(H5EEngine engine) {
        this.engine = engine;
        this.screenWidth = Gdx.graphics.getWidth();
        this.screenHeight = Gdx.graphics.getHeight();
        //TODO put this into GameControls or find a better place later
        engine.addInputProcessor(0, new InputAdapter() {
            @Override
            public boolean keyUp(int keycode) {
                if(keycode == Input.Keys.ESCAPE) {
                    final GCWindow window = openWindows.isEmpty() ? null : openWindows.get(0);
                    if(window != null && window.isCloseButtonEnabled()) {
                        window.close();
                        return true;
                    }
                }
                if(keycode == Input.Keys.O && isCtrlAltPressed()) {
                    boolean newMode = !getMobileMode();
                    if(newMode) {
                        screenWidth = Gdx.graphics.getWidth();
                        screenHeight = Gdx.graphics.getHeight();
                        Gdx.graphics.setWindowedMode(MOBILE_SCREEN_WIDTH, MOBILE_SCREEN_HEIGHT);
                    } else {
                        Gdx.graphics.setWindowedMode(screenWidth, screenHeight);
                    }
                    skipSetMobileModeFromResize = true;
                    setMobileMode(newMode);
                    return true;
                }
                if(keycode == Input.Keys.L && isCtrlAltPressed()) {
                    boolean newMode = !getMobileMode();
                    if(newMode) {
                        screenWidth = Gdx.graphics.getWidth();
                        screenHeight = Gdx.graphics.getHeight();
                        //noinspection SuspiciousNameCombination
                        Gdx.graphics.setWindowedMode(MOBILE_SCREEN_HEIGHT, MOBILE_SCREEN_WIDTH);
                    } else {
                        Gdx.graphics.setWindowedMode(screenWidth, screenHeight);
                    }
                    skipSetMobileModeFromResize = true;
                    setMobileMode(newMode);
                    return true;
                }
                return super.keyUp(keycode);
            }
        });
    }

    protected boolean isCtrlAltPressed() {
        return (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT)) && (Gdx.input.isKeyPressed(Input.Keys.ALT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.ALT_RIGHT));
    }

    @Override
    public final void closeAll() {
        for (int i = openWindows.size(); i > 0; i--) {
            GCWindow w = openWindows.get(i-1);
            try {
                if (w.isCloseable()) {
                    w.close(true);
                }
            } catch (Throwable ex) {
                logger.error("Unable to close window " + w.getClass().getSimpleName() + " with id " + w.getId(), ex);
            }
        }
    }

    public void setMobileModeFromResize(boolean mobileMode) {
        if(skipSetMobileModeFromResize) {
            skipSetMobileModeFromResize = false;
            return;
        }
        setMobileMode(mobileMode);
    }

    /**
     * @return TRUE if the given window is active, FALSE otherwise
     */
    @Override
    final boolean isActive(GCWindow window) {
        Dev.checkNotNull(window);
        return !openWindows.isEmpty() && window == openWindows.get(0);
    }

    /**
     * This method is called to signal that the given window is to become the "active" window.
     * This will happen when a new window is opened, or when an existing inactive window is clicked.
     */
    @Override
    final void activateWindow(GCWindow window) {
        Dev.checkNotNull(window);
        if (!openWindows.isEmpty()) {
            GCWindow activeWindow = openWindows.get(0);
            if (activeWindow == window) {
                // do nothing - window is already active
                return;
            } else {
                // deactivate the window that's currently active
                activeWindow.setupInactiveState();
            }
        }

        // activate the new window, and place it at the head of the deque
        window.setupActiveState();
        openWindows.remove(window);
        openWindows.add(0, window);

        if (window.isFullscreenOnMobile()) window.setFullscreen(mobileMode);
        updateFullscreen();
    }

    /**
     * This method is called to signal that a given window is no longer open.
     * The window will be removed from the list of open windows. If it was the "active" window and if there are
     * other open windows, the most recently-active open window will become the "active" window.
     */
    @Override
    final void windowClosed(GCWindow window) {
        Dev.checkNotNull(window);
        if (!openWindows.contains(window)) {
            // looks like this was already done - ignore this call
            return;
        }

        if (openWindows.get(0) != window) {
            // this window is not the active window.. simply remove it from the list
            openWindows.remove(window);
            return;
        }

        // this window was the active window
        window.setupInactiveState();
        if (!openWindows.isEmpty()) openWindows.remove(0);
        // if more open windows are left, pick the next one to become active (it would be the most recently-active)
        if (!openWindows.isEmpty()) {
            openWindows.get(0).setupActiveState();
            disableOptionsButtonInFullscreen(openWindows.get(0).getDisableOptionsButtonInFullscreen());
        }

        updateFullscreen();
    }

    @Override
    public final void updateFullscreen() {
        if (isFullScreenWindowCurrentlyOpen()) {
            Hooks.call(HOOK_WindowFullscreen, disableOptionsButton, true);
            engine.setFullscreenWindowOverlaysGameLayer(true);

        } else {
            Hooks.call(HOOK_WindowFullscreen, false, false);
            engine.setFullscreenWindowOverlaysGameLayer(false);
        }
    }

    private boolean isFullScreenWindowCurrentlyOpen() {
        for (int i = openWindows.size(); i > 0; i--) {
            GCWindow w = openWindows.get(i - 1);
            if (w.isFullscreen()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean getMobileMode() {
        return mobileMode;
    }

    @Override
    public void setMobileMode(boolean mobileMode) {
        if (mobileMode == this.mobileMode) return;
        this.mobileMode = mobileMode;
        for (int i = openWindows.size(); i > 0; i--) {
            GCWindow w = openWindows.get(i-1);
            if (w.isFullscreenOnMobile()) w.setFullscreen(mobileMode);
        }
    }

    @Override
    public void subscribeOnWindowFullscreen(Callable2Args<Boolean, Boolean> handler) {
        Hooks.subscribe(HOOK_WindowFullscreen, handler);
    }

    @Override
    public void disableOptionsButtonInFullscreen(boolean disableOptionsButton) {
        this.disableOptionsButton = disableOptionsButton;
    }
}
