package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.vsdata.shared.SpawnOptionData;


public class GCCharacterRespawnScreen<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> extends GCCharacterSpawnDesktopBase<L, S, C> {

    public GCCharacterRespawnScreen(H5ELayer layer, GCLoginManager<L, S, C> loginManager) {
        super(layer, loginManager);
        fieldCharacterName.setDisabled(true);
        btnRandomName.setVisible(false);
        spawnSelectDialog.populateData(loginManager.getDefaultSpawnConfigs());
        spawnSelectDialog.onDoubleClick = (data) -> {
            selectedSpawnOption = data;
            next();
        };
    }

    @Override
    protected void back() {
        close();
        loginManager.backToCharacterSelection();
    }

    @Override
    protected boolean validateSpawn() {
        return true;
    }

    @Override
    protected void spawn(String spawnType, JSONObject spawnParams) {
        loginManager.respawn(loginInfo, serverInfo, characterInfo, spawnType, spawnParams, this::close, this::processError);
    }

    public void open(L loginInfo, S serverInfo, C characterInfo) {
        fieldCharacterName.setText(characterInfo.getName());
        super.open(loginInfo, serverInfo);

        spawnSelectDialog.activateLoadingIndicator();
        loginManager.loadRespawnOptions(loginInfo, serverInfo, characterInfo, (options) -> {
            final GCSpawnSelectDialog.GCSpawnSelectData spawnOptions = new GCSpawnSelectDialog.GCSpawnSelectData();
            for (SpawnOptionData option : options) {
                spawnOptions.add(option);
            }
            spawnOptions.add(loginManager.getDefaultSpawnConfigs());


            spawnSelectDialog.populateData(spawnOptions);
            spawnSelectDialog.deactivateLoadingIndicator();
        }, this::processError);
    }

    @Override
    protected String getJoinText() {
        return "Respawn Character";
    }

    @Override
    protected String getBackButtonText() {
        return "Select Character";
    }
}

