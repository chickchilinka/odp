package com.universeprojects.gamecomponents.client.dialogs.inventory;

import java.util.Objects;

public class EquipmentSlotKey<S extends Comparable> implements Comparable<EquipmentSlotKey<S>> {
    public final S slotReference;
    public final Integer index;

    public EquipmentSlotKey(S slotReference, Integer index) {
        this.slotReference = slotReference;
        this.index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EquipmentSlotKey<?> that = (EquipmentSlotKey<?>) o;
        return Objects.equals(slotReference, that.slotReference) &&
            Objects.equals(index, that.index);
    }

    @Override
    public int hashCode() {
        return Objects.hash(slotReference, index);
    }

    @Override
    public String toString() {
        return "EquipmentSlotKey{" +
            "slotReference=" + slotReference +
            ", index=" + index +
            '}';
    }

    @Override
    public int compareTo(EquipmentSlotKey<S> o) {
        int value = slotReference.compareTo(o.slotReference);
        if(value != 0) return value;
        return Integer.compare(index, o.index);
    }
}
