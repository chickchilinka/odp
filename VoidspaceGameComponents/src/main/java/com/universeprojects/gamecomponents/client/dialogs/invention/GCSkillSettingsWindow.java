package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gefcommon.shared.elements.RecipeValidator;
import com.universeprojects.gefcommon.shared.elements.UserFieldValueConfigurationType;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.UserFieldValueConfigurationData;

public class GCSkillSettingsWindow extends GCSimpleWindow {

    private final H5ELayer layer;
    private final H5EScrollablePane userFieldValueListContainer;
    private final Table userFieldValueListTable;
    private final H5EButton submitBtn;
    private final H5EButton cancelBtn;

    public GCSkillSettingsWindow(H5ELayer layer, Callable0Args onSubmit) {
        super(layer, "skillsettings", "Skill settings", 400, 500, false, "popup-window");
        positionProportionally(0.5f, 0.5f);
        setModal(true);
        disableCloseButton();
        //setFullscreenOnMobile(true);
        this.layer = layer;
        userFieldValueListContainer = new H5EScrollablePane(layer);
        userFieldValueListTable = userFieldValueListContainer.getContent();
        userFieldValueListTable.defaults().left().top();
        userFieldValueListTable.padLeft(10);
        userFieldValueListTable.padBottom(10);
        userFieldValueListTable.background(StyleFactory.INSTANCE.panelInternal12);
        add(userFieldValueListContainer).padTop(20).grow().colspan(2);
        submitBtn = new H5EButton("Start", layer);
        submitBtn.addButtonListener(onSubmit);
        row();
        cancelBtn = new H5EButton("Cancel", layer);
        cancelBtn.addButtonListener(this::close);
        add(submitBtn).growX().left().top();
        add(cancelBtn).growX().left().top();
    }

    public void setData(GCRecipeData recipeData) {
        userFieldValueListTable.clearChildren();
        for (UserFieldValueConfigurationData valueData : recipeData.getData().getUserFieldValueConfigurationsSafe()) {
            userFieldValueListTable.row();
            H5ELabel nameLabel = new H5ELabel(valueData.description, layer);
            if (valueData.isRequiredSafe()) {
                nameLabel.getText().append(" *");
                nameLabel.setColor(Color.RED);
            }
            userFieldValueListTable.add(nameLabel).left().top();
            if (valueData.type == UserFieldValueConfigurationType.STRING) {

                TextField textField = new TextField(Strings.nullToEmpty(recipeData.getUserFieldValueData().get(valueData.targetFieldName)), layer.getEngine().getSkin());
                textField.setText(Strings.nullToEmpty(recipeData.getUserFieldValueData().get(valueData.targetFieldName)));
                textField.setMaxLength(Dev.withDefault(valueData.maxLength, 0));
                userFieldValueListTable.add(textField).padLeft(10f).left().top();
                userFieldValueListTable.row();

                H5ELabel errorLabel = new H5ELabel(valueData.formattingErrorMessage, layer);
                errorLabel.setWrap(true);
                errorLabel.setColor(Color.RED);
                errorLabel.setVisible(false);
                userFieldValueListTable.add(errorLabel).colspan(2);
                textField.setTextFieldListener((field, c) -> {
                    final String text = field.getText();
                    final boolean valid = RecipeValidator.INSTANCE.validateUserFieldValue(valueData, text);
                    recipeData.addUserFieldValueConfig(valueData.targetFieldName, text);
                    errorLabel.setVisible(!valid);
                });
            }
        }
        userFieldValueListTable.row();
        userFieldValueListTable.add().grow();
    }

}
