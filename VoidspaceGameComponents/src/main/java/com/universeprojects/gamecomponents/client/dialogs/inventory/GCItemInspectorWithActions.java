package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCItemInspectorWithActions<K, T extends GCInventoryItem> extends GCItemInspector<K, T> {

    public final Table auxActionsTable;

    public static final int BUTTON_SIZE = 40;
    private final ImageTextButton mergeAllButton;
    private final ImageTextButton splitStackButton;
    private final ImageTextButton experimentButton;

    public GCItemInspectorWithActions(H5ELayer layer, boolean showMergeAndSplit) {
        super(layer);
        auxActionsTable = add(new Table()).colspan(2).fill().getActor();
        auxActionsTable.padBottom(15);
        if(showMergeAndSplit) {
            mergeAllButton = new ImageTextButton("", getEngine().getSkin(), "btn-merge-all");
            mergeAllButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    onMergeAllButtonClicked();
                    event.handle();
                }
            });
            splitStackButton = new ImageTextButton("", getEngine().getSkin(), "btn-split-stack");
            splitStackButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    onSplitStackButtonClicked();
                    event.handle();
                }
            });
        } else {
            mergeAllButton = null;
            splitStackButton = null;
        }
        experimentButton = newExperimentButton();
        auxActionsTable.defaults().size(BUTTON_SIZE).grow();
    }

    @Override
    protected void onCustomizeBtnClick() {
        //noinspection unchecked
        ((InventoryAux<T>) currentComponent.inventoryBase).onCustomize(currentComponent.getItem());
    }

    private ImageTextButton newExperimentButton() {
        ImageTextButton experimentButton = new ImageTextButton("", getEngine().getSkin(), "btn-item-experiment");
        experimentButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                close();
                //noinspection unchecked
                ((InventoryAux<T>) currentComponent.inventoryBase).onStartExperiment(currentComponent.getItem());
            }
        });
        return experimentButton;
    }

    private void onMergeAllButtonClicked() {
        if(currentComponent == null || currentComponent.getItem() == null) return;
        //noinspection unchecked
        ((GCInventory<T>)currentComponent.inventoryBase).onMergeAll(currentComponent.getItem());
    }

    private void onSplitStackButtonClicked() {
        if(currentComponent == null || currentComponent.getItem() == null) return;
        //noinspection unchecked
        ((GCInventory<T>)currentComponent.inventoryBase).onSplitStack((Integer) currentComponent.getKey(), currentComponent.getItem());
    }

    @Override
    @SuppressWarnings("CommentedOutCode")
    protected void onSetSlotComponent(T item, GCInventorySlotComponent<K, T, ?> component) {
        auxActionsTable.clearChildren();
        if(mergeAllButton != null && splitStackButton != null) {
//            TODO re-enable when implemented
//            if (mergeAllButton.isVisible()) {
//                auxActionsTable.add(mergeAllButton);
//            }
            boolean showSplitStack = item.isStackable() && item.getQuantity() > 1;
            if (showSplitStack) {
                auxActionsTable.add(splitStackButton);
            }
        }
        if(item.canBeExperimentedOn()) {
            auxActionsTable.add(experimentButton);
        }
    }
}
