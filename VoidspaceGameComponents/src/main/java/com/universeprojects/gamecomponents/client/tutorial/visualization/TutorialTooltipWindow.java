package com.universeprojects.gamecomponents.client.tutorial.visualization;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.gamecomponents.client.windows.GCWindowManager;
import com.universeprojects.html5engine.client.framework.H5ELayer;

/**
 * Window that has all the interactions disabled,
 * usually used as Tutorial text box
 */
public class TutorialTooltipWindow extends GCWindow {

    public TutorialTooltipWindow(H5ELayer layer) {
        super(layer, 200, 300, "tutorial-tooltip");

        setTitle("TOOLTIP");
        positionProportionally(0.5f, 0.5f);
        setPackOnOpen(true);
        setCloseButtonEnabled(false);
        setMovable(false);
        getTitleTable().removeActor(getTitleLabel());
        defaults().left();
        setTouchable(Touchable.childrenOnly);

        getCaptureListeners().clear();
        getListeners().clear();
    }

    @Override
    public void open() {
        if (isOpen()) {
            toFront();
            return;
        }
        pack();
        setVisible(true);
        toFront();
    }

    @Override
    public void activate() {
    }

    @Override
    public void destroy() {
        remove();
    }

    @Override
    protected GCWindowManager getWindowManager() {
        throw new UnsupportedOperationException("This window does not support GCWindowManager integration");
    }

    @Override
    public void close() {
        if (!isOpen()) {
            return;
        }

        setVisible(false);
    }

    @Override
    public void setColor(Color color) {
        // Disable INACTIVE window state
        super.setColor(new Color(1f, 1f, 1f, 1f));
    }
}
