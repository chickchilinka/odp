package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;

public abstract class InspectorParameter {
    public static final Color BASE_COLOR = Rarity.COMMON.getColor();
    public final Color rarityColor;

    public InspectorParameter() {
        this.rarityColor = BASE_COLOR;
    }

    public InspectorParameter(Color rarityColor) {
        this.rarityColor = rarityColor != null ? rarityColor : BASE_COLOR;
    }

    public InspectorParameter(Rarity rarity) {
        this.rarityColor = rarity != null ? rarity.getColor() : BASE_COLOR;
    }
}
