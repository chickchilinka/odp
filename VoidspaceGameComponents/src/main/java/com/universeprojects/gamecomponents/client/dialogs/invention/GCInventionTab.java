package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

abstract class GCInventionTab {

    protected final GCInventionSystemDialog dialog;
    protected final H5ELayer layer;

    protected final Table lowerButtonRow;
    protected final Table leftContent;
    protected final Table rightContent;
    protected final Table rightTitleArea;
    protected final H5EScrollLabel titleLabel;
    protected final H5EInputBox filter;
    protected final Cell filterCell;
    protected String filterText="";

    protected GCInventionTab(GCInventionSystemDialog dialog, String title) {
        this.dialog = dialog;
        this.layer = dialog.getLayer();

        lowerButtonRow = new Table();
        dialog.lowerButtonRow.add(lowerButtonRow);
        lowerButtonRow.defaults().left().top();
        lowerButtonRow.left().top();

        filter=new H5EInputBox(layer);
        filter.setMessageText("Filter");
        filter.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                onFilter();
                changeEvent.handle();
            }
        });
        leftContent = new Table();
        leftContent.defaults().left().top();
        leftContent.left().top();
        leftContent.setFillParent(true);
        filterCell=leftContent.add(filter).growX();
        leftContent.row();
        dialog.leftContent.add(leftContent);

        rightContent = new Table();
        rightContent.defaults().left().top();
        rightContent.left().top();
        dialog.rightContent.add(rightContent);

        rightTitleArea = new Table();
        rightTitleArea.defaults().left().top();
        rightTitleArea.left().top();
        dialog.rightTitleArea.add(rightTitleArea);

        titleLabel = new H5EScrollLabel(title, layer);
        rightTitleArea.add(titleLabel).growX().padLeft(5);
        titleLabel.getLabel().setColor(Color.WHITE);
        titleLabel.getLabel().setFontScale(1.2f);
        titleLabel.setAutoScroll(true);

        hide(); // start hidden
    }

    void show() {
        setVisible(true);
    }

    void hide() {
        setVisible(false);
    }

    public void setVisible(boolean visible) {
        leftContent.setVisible(visible);
        rightTitleArea.setVisible(visible);
        rightContent.setVisible(visible);
        lowerButtonRow.setVisible(visible);
    }

    void deactivate() {
        clearTab();
        hide();
    }

    public boolean isOpen(){
        return rightContent.isVisible();
    }

    abstract protected void clearTab();

    protected void onFilter(){
        filterText=filter.getText();
    }

    public void removeById(long id) {

    }

    public void windowStateChanged(GCInventionSystemDialog.WindowState windowState){
        switch (windowState){
            case DESKTOP:
                rightContent.setBackground(rightTitleArea.getBackground());
                leftContent.setBackground(rightTitleArea.getBackground());
                break;
            case PORTRAIT:
                dialog.mainTable.getCell(dialog.leftContent).height(0);
                dialog.mainTable.getCell(dialog.rightContent).height(0);
                rightContent.setBackground(StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque);
                leftContent.setBackground(StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque);
                break;
            case LANDSCAPE:
                rightContent.setBackground(rightTitleArea.getBackground());
                leftContent.setBackground(rightTitleArea.getBackground());
                break;
        }
    }
    //Click button when a key is pressed

}
