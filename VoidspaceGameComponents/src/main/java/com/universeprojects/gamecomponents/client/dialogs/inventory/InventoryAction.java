package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

public interface InventoryAction<T extends GCInventoryItem> {
    Button createButton(T item);
}
