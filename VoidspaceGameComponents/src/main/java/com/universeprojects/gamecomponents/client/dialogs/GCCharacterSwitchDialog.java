package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.dialogs.GCCharactersListItem;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.vsdata.shared.CharacterData;

import java.util.ArrayList;
import java.util.List;

public class GCCharacterSwitchDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 300;
    private final static int WINDOW_H = 400;

    private final H5EScrollablePane listContainer;
    private final H5ELayer layer;
    private final GCListItemActionHandler<GCCharactersListItem> listHandler;

    private GCList<GCCharactersListItem> itemsList;
    private GCCharactersListItem selection;
    private H5EButton cancelButton;
    private H5EButton switchCharacter;

    public GCCharacterSwitchDialog(H5ELayer layer, String title, String description, String buttonCaption, Callable1Args<List<CharacterData>> onButtonPressed, Callable0Args onCancel) {
        super(layer, "characterSwitchDialog", title, WINDOW_W, WINDOW_H, false);
        this.layer = layer;
        positionProportionally(0.65f, 0.65f);
        H5ELabel descLabel = new H5ELabel(description, layer);
        descLabel.setWrap(true);
        Cell<H5ELabel> labelCell = add(descLabel).left().fill();
        labelCell.padLeft(7).padRight(7);
        row();

        itemsList = new GCList<GCCharactersListItem>(layer, 1, 5, 1, true);
        listHandler = new GCListItemActionHandler<GCCharactersListItem>() {
            @Override
            protected void onSelectionUpdate(GCCharactersListItem lastSelectedItem) {
                if (itemsList.getSelectedItems().size == 0) {
                    switchCharacter.setDisabled(true);
                } else {
                    switchCharacter.setDisabled(false);
                }
            }
        };
        listContainer = new H5EScrollablePane(layer);
        listContainer.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        listContainer.setScrollingDisabled(true, false);
        listContainer.setOverscroll(false, true);
        listContainer.setForceScroll(false, true);
        listContainer.add(itemsList).left().top().grow();
        listContainer.layout();
        add(listContainer).top().left().fill().expand();

        cancelButton = new H5EButton("Cancel", layer);
        switchCharacter = new H5EButton(buttonCaption, layer);
        switchCharacter.addButtonListener(() -> {
            List<CharacterData> selectedData = new ArrayList<CharacterData>();
            for (GCCharactersListItem item : itemsList.getSelectedItems()) {
                selectedData.add(item.data);
            }
            onButtonPressed.call(selectedData);
            close();
        });
        cancelButton.addButtonListener(() -> {
            onCancel.call();
        });
        switchCharacter.setDisabled(true);
        row();

        Table buttonRow = new Table();
        buttonRow.padTop(5).padLeft(4).padRight(4);
        buttonRow.add(cancelButton).left();
        buttonRow.add().growX();
        buttonRow.add(switchCharacter).right();
        add(buttonRow).left().growX();
    }


    public void processData(List<CharacterData> dataList) {
        itemsList.clear();
        for (CharacterData data : dataList) {
            GCCharactersListItem item = new GCCharactersListItem(layer, listHandler, data);
            itemsList.addItem(item);
            item.rebuildActor();
        }
        itemsList.setMaxCheckCount(dataList.size());
    }
}