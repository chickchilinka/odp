package com.universeprojects.gamecomponents.client.windows;

import com.badlogic.gdx.utils.Align;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/*
 * This window defaults to take up the whole screen, and can be set to take up
 * a certain proportion of the screen. It will automatically resize itself when
 * the game window is resized.
 * */
public class GCMobileWindow extends GCWindow {

    private final H5EContainer content;
    private final H5EScrollablePane scrollPane;
    private final H5EContainer topBar;
    private final H5EContainer leftCorner;
    private final H5EContainer rightCorner;
    private final H5ELabel titleLabel;

    private int windowAlign = Align.center;
    private float xProportion = 0.5f;
    private float yProportion = 0.5f;

    private final int EDGE_BUFFER = 100;

    public GCMobileWindow(H5ELayer layer) {
        super(layer, 0, 0, "default");//"inspector-window"
        setMovable(false);
        setTitle(" Mobile Window");

        setSize(getEngine().getWidth() + 2*EDGE_BUFFER, getEngine().getHeight() + 2*EDGE_BUFFER);
        setPosition(-EDGE_BUFFER, -EDGE_BUFFER);

        //setDebug(true, true);

        content = new H5EContainer(layer);
        //content.setBackground("purchase-dialog-subpanel");
        content.setSize(getEngine().getWidth(), getEngine().getHeight());
        content.setPosition(EDGE_BUFFER, EDGE_BUFFER);
        addActor(content);

        scrollPane = content.add(new H5EScrollablePane(layer)).grow().getActor();
        scrollPane.setOverscroll(false, false);

        topBar = new H5EContainer(layer);
        topBar.setBackground("top-graphic-button-bar-right");
        topBar.setHeight(37);
        addActor(topBar);

        leftCorner = new H5EContainer(layer);
        leftCorner.setBackground("top-graphic-left-corner");
        leftCorner.setWidth(52);
        leftCorner.setHeight(42);
        addActor(leftCorner);

        rightCorner = new H5EContainer(layer);
        rightCorner.setBackground("top-graphic-right-corner");
        rightCorner.setWidth(52);
        rightCorner.setHeight(42);
        addActor(rightCorner);

        titleLabel = new H5ELabel(layer);
        addActor(titleLabel);

        btnClose.remove();
        addActor(btnClose);

        updatePosition();
        getEngine().onResize.registerHandler(this::updatePosition);
    }

    public void updatePosition() {
        // Resize the window
        int screenWidth = getEngine().getWidth();
        int screenHeight = getEngine().getHeight();

        setSize(screenWidth + 2*EDGE_BUFFER, screenHeight + 2*EDGE_BUFFER);
        setPosition(-EDGE_BUFFER, -EDGE_BUFFER);
        content.setSize(screenWidth, screenHeight - leftCorner.getHeight() - 2);
        content.setPosition(EDGE_BUFFER, EDGE_BUFFER);

        if (Align.isLeft(windowAlign)) {
            setWidth(Math.round(screenWidth*xProportion + EDGE_BUFFER));
            content.setWidth(Math.round(screenWidth*xProportion));
        } else if (Align.isRight(windowAlign)) {
            setWidth(Math.round(screenWidth*xProportion + EDGE_BUFFER));
            setX(Math.round(screenWidth*(1f-xProportion)));
            content.setWidth(Math.round(screenWidth*xProportion));
            content.setX(0);
        }

        if (Align.isBottom(windowAlign)) {
            setHeight(screenHeight*yProportion + EDGE_BUFFER);
            content.setHeight(Math.round(screenHeight*yProportion));
        } else if (Align.isTop(windowAlign)) {
            setHeight(Math.round(screenHeight*yProportion + EDGE_BUFFER));
            setY(Math.round(screenHeight*(1f-yProportion)));
            content.setHeight(Math.round(screenHeight*yProportion));
            content.setY(0);
        }

        // Reposition components
        float contentX = content.getX();
        float contentY = content.getY();
        topBar.setWidth(screenWidth);
        topBar.setPosition(contentX, contentY + screenHeight - topBar.getHeight());
        leftCorner.setPosition(contentX, contentY + screenHeight - leftCorner.getHeight());
        rightCorner.setPosition(contentX + screenWidth - rightCorner.getWidth(), contentY + screenHeight - rightCorner.getHeight());
        titleLabel.setPosition(Math.round(contentX + (screenWidth - titleLabel.getPrefWidth()) / 2f), Math.round(contentY + screenHeight - 16));

        btnClose.setPosition(contentX + screenWidth - btnClose.getWidth() - 5, contentY + screenHeight - btnClose.getPrefHeight() - 5);
    }

    public H5EContainer getContent() {
        return content;
    }

    public int getWindowAlign() {
        return windowAlign;
    }

    public void setWindowAlign(int align) {
        this.windowAlign = align;
    }

    public float getxProportion() {
        return xProportion;
    }

    public void setxProportion(float xProportion) {
        this.xProportion = xProportion;
    }

    public float getyProportion() {
        return yProportion;
    }

    public void setyProportion(float yProportion) {
        this.yProportion = yProportion;
    }

    public void openWindow(GCWindow window) {
        scrollPane.getContent().clearChildren();
        window.open();
        window.setPosition(0,0);
        window.setMovable(false);
        window.pad(EDGE_BUFFER);
        window.setCloseButtonEnabled(false);
        window.getTitleLabel().setVisible(false);
        titleLabel.setText(window.getTitleLabel().getText());
        titleLabel.setPosition(content.getX() + (getEngine().getWidth() - titleLabel.getPrefWidth()) / 2f, content.getY() + getEngine().getHeight() - 18);
        scrollPane.getContent().add(window);//.grow();
        window.invalidateHierarchy();
        open();
    }

}