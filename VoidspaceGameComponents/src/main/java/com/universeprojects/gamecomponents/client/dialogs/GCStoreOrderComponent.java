package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCStoreOrderComponent extends Table {
    private H5ELayer layer;

    /**
     * The key of the store order. Used so that when a button is pressed to do a transaction or delete the order,
     * the key is passed back to let the store service or aspect know which order is being used.
     */
    private String storeOrderKey;

    private String buyItemName, buyItemIconName;
    private int buyItemQuantity;
    private String sellItemName, sellItemIconName;
    private int sellItemQuantity;

    public GCStoreOrderComponent (H5ELayer layer, String storeOrderKey, String buyItemName, String buyItemIconName, int buyItemQuantity, String sellItemName, String sellItemIconName, int sellItemQuantity) {
        super();
        this.layer = layer;

        this.storeOrderKey = storeOrderKey;

        this.buyItemName = buyItemName;
        this.buyItemIconName = buyItemIconName;
        this.buyItemQuantity = buyItemQuantity;

        this.sellItemName = sellItemName;
        this.sellItemIconName = sellItemIconName;
        this.sellItemQuantity = sellItemQuantity;

        this.setSkin(layer.getEngine().getSkin());
    }

    public void addStoreEntry (Table storeTable, String itemName, String itemIconName, int itemQuantity) {
        Table entry = new Table(layer.getEngine().getSkin());
        final float ENTRY_SIZE = 50;

        // Add the item icon to the window
        H5EContainer itemIconPlaceholder = new H5EContainer(layer);
        itemIconPlaceholder.setWidth(ENTRY_SIZE);
        itemIconPlaceholder.setHeight(ENTRY_SIZE);
        Cell<H5EContainer> buyItemIconPlaceholderCell = entry.add(itemIconPlaceholder).left();
        buyItemIconPlaceholderCell.prefWidth(ENTRY_SIZE).prefHeight(ENTRY_SIZE);

        // Place the icon on top of the invisible placeholder
        H5EIcon buyIcon = H5EIcon.fromSpriteType(layer, itemIconName);
        itemIconPlaceholder.add(buyIcon).width(ENTRY_SIZE).height(ENTRY_SIZE);

        // Add the text label for the item to buy
        H5ELabel itemLabel = new H5ELabel(itemName + " x " + itemQuantity, layer);
        itemLabel.setFontScale(0.8f);
        entry.add(itemLabel).left().grow();

        storeTable.add(entry).top().left().
                height(50);
    }

    public void addStoreBuyButton (Table storeTable) {
        final H5EButton buyBtn = ButtonBuilder
                .inLayer(layer)
                .withStyle("button-small")
                .withText("BUY")
                .build();

        buyBtn.addButtonListener(() -> startTransaction(getStoreOrderKey()));

        storeTable.add(buyBtn).right().height(40).padTop(5).padBottom(5);
    }

    /**
     * For use in the GCStoreManagerDialog. Creates the button to delete a store order.
     */
    public void addStoreOrderDeleteButton (Table storeTable) {
        final H5EButton delBtn = ButtonBuilder
                .inLayer(layer)
                .withStyle("button-red")
                .withText("   X   ")
                .build();

        delBtn.addButtonListener(() -> deleteOrder(getStoreOrderKey()));

        storeTable.add(delBtn).right().padLeft(13).padRight(13).height(30).padTop(10).padBottom(10);
    }

    public String getStoreOrderKey() {
        return storeOrderKey;
    }

    public String getBuyItemName() {
        return buyItemName;
    }

    public String getSellItemName() {
        return sellItemName;
    }

    public String getSellItemIconName() {
        return sellItemIconName;
    }

    public int getSellItemQuantity() {
        return sellItemQuantity;
    }

    public int getBuyItemQuantity() {
        return buyItemQuantity;
    }

    public String getBuyItemIconName() {
        return buyItemIconName;
    }

    /**
     * Give a method to use for starting a transaction on button press
     */
    public void startTransaction (String storeOrderKey) {

    }

    /**
     * Method to be overridden, for deleting a store order
     */
    public void deleteOrder (String storeOrderKey) {

    }
}
