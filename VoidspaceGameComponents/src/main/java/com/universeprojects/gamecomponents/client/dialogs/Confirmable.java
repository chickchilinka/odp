package com.universeprojects.gamecomponents.client.dialogs;


/**
 * Any class that requires a user to confirm a decision should implement this interface. See {@link GCConfirmationScreen}.
 */
public interface Confirmable {

    /**
     * This method should be implemented and called after the user confirms their decision. This method
     * should also reopen the window that called it to resume where the user left off.
     */
    void confirmed();
}
