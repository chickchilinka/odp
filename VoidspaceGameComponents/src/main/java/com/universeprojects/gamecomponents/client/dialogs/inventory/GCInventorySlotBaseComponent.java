package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.inputs.H5EButtonInvalidateListener;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public abstract class GCInventorySlotBaseComponent<K, T extends GCInventoryItem, C extends GCInventorySlotBaseComponent<K,T,C>> extends Button implements GraphicElement {
    private T item;
    private final K key;
    protected final GCInventoryBase<K,T,C> inventoryBase;
    ComponentSource<K, T, C> dragAndDropSource;
    ComponentTarget<K, T, C> dragAndDropTarget;

    public boolean checkDisconnectLocked() {
        return false;
    }

    public boolean needsSetOnlyCheck() {
        return false;
    }

    @Override
    public H5ELayer getLayer() {
        return inventoryBase.getLayer();
    }

    @Override
    public H5EEngine getEngine() {
        return inventoryBase.getEngine();
    }

    public GCInventorySlotBaseComponent(GCInventoryBase<K,T,C> inventoryBase, String styleName, K key) {
        super(inventoryBase.getEngine().getSkin(), styleName);
        addListener(new H5EButtonInvalidateListener(this));
        this.inventoryBase = inventoryBase;
        this.key = key;
        final H5ELayer layer = inventoryBase.getLayer();
        setStage(layer);
        addListener(new ClickListener() {
            public void clicked (InputEvent event, float x, float y) {
                //noinspection unchecked
                inventoryBase.select((C)GCInventorySlotBaseComponent.this);
                event.handle();
                event.setBubbles(false);
            }
        });
    }

    public K getKey() {
        return key;
    }

    public boolean isEmpty() {
        return item == null;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public abstract void onDragStart();

    public abstract void onDragStop();

    @Override
    public String getName() {
        if(item != null) {
            return item.getName();
        }
        else {
            return "";
        }
    }
}
