package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.vsdata.shared.SpawnOptionData;

public abstract class GCCharacterSpawnBase<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo>  {
    protected final GCSpawnSelectDialog spawnSelectDialog;
    protected final FriendSpawnSelectionWindow friendWindow;
    protected final OrganisationSpawnSelectionWindow organisationWindow;
    protected final CharacterListSpawnSelection<C> characterSpawnWindow;

    protected final GCLoadingIndicator loadingIndicator;
    protected final H5ELayer layer;
    protected final GCLoginManager<L, S, C> loginManager;

    protected SpawnOptionData selectedSpawnOption = null;

    protected L loginInfo;
    protected S serverInfo;

    public GCCharacterSpawnBase(H5ELayer layer, GCLoginManager<L, S, C> loginManager, boolean mobile) {
        this.layer = layer;
        this.loadingIndicator = new GCLoadingIndicator(layer);
        this.loginManager = loginManager;

        spawnSelectDialog = new GCSpawnSelectDialog(layer, mobile);
        spawnSelectDialog.onChooseSpawn = this::onSpawnSelected;

        friendWindow = new FriendSpawnSelectionWindow(layer,
            (successCallback, errorCallback) -> loginManager.loadUserFriends(loginInfo, serverInfo, successCallback, errorCallback),
            (id) -> spawnWithId("FRIEND", id));
        friendWindow.close();

        organisationWindow = new OrganisationSpawnSelectionWindow(layer,
            (successCallback, errorCallback) -> loginManager.loadUserOrganisations(loginInfo, serverInfo, successCallback, errorCallback),
            (id) -> spawnWithId("ORG", id));
        organisationWindow.close();

        characterSpawnWindow = new CharacterListSpawnSelection<>(layer,
            (successCallback, errorCallback) -> loginManager.loadGameCharacters(loginInfo, serverInfo, successCallback, errorCallback),
            (id) -> spawnWithId("CHAR", id));
        characterSpawnWindow.close();
    }

    protected void onSpawnSelected(SpawnOptionData option) {
        this.selectedSpawnOption = option;
    }

    protected void spawnWithId(String spawnType, Long charId) {
        if(charId == null) {
            return;
        }
        if (validateSpawn()) {
            JSONObject params = new JSONObject();
            params.put("id", charId);
            spawn(spawnType, params);
        }
    }

    protected void executeSpawn() {
        if (!validateSpawn()) {
            return;
        }
        if(selectedSpawnOption == null) {
            displayWarning("Please choose a spawn option to continue.");
            return;
        }
        switch (selectedSpawnOption.spawnType) {
            case "FRIEND":
                loadFriendDataAndOpenSelection();
                return;
            case "CHAR":
                loadCharDataAndOpenSelection();
                return;
            case "ORG":
                loadOrganisationDataAndOpenSelection();
                return;
        }
        spawn(selectedSpawnOption.spawnType, selectedSpawnOption.spawnParams);
    }

    private void loadOrganisationDataAndOpenSelection() {
        organisationWindow.loadOrganisationData();
    }

    private void loadFriendDataAndOpenSelection() {
        friendWindow.loadFriendData();
    }

    private void loadCharDataAndOpenSelection() {
        characterSpawnWindow.loadCharacterDataAndOpenDialog();
    }

    protected abstract boolean validateSpawn();
    protected abstract void spawn(String spawnType, JSONObject spawnParams);
    protected abstract void displayWarning(String text);
    protected abstract void back();

}
