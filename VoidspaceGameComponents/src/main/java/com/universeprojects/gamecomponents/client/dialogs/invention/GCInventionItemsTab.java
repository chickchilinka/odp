package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.components.GCItemInternalTree;
import com.universeprojects.gamecomponents.client.components.GCItemTreeItem;
import com.universeprojects.gamecomponents.client.components.GCItemTreeSubitem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCSkillsData.GCSkillDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

@SuppressWarnings("FieldCanBeLocal")
public class GCInventionItemsTab extends GCInventionTab {

    private final H5EScrollablePane listContainer;
    private GCList<GCItemsListItem> skillsList;
    private final GCListItemActionHandler<GCItemsListItem> listHandler;
    private final H5EButton btnBuildItem;
    private final H5EButton btnDeleteSkill;
    public static final int LIST_SPACING = 5;

    private final GCItemInternalTree leftTree;
    private GCSkillsData allSkills;
    private GCSkillsData curSkills;

    private float paneWidth = 0;

    private String getShortName(String fullName) {
        int startIndex = fullName.indexOf("'s " ) + 3;
        int endIndex = fullName.indexOf(" technique #" );
        if(startIndex < 0 || endIndex < 0 || endIndex < startIndex) {
            return "";
        }
        return fullName.substring(startIndex, endIndex);
    }

    protected GCInventionItemsTab(GCInventionSystemDialog dialog) {
        super(dialog, "Construction");


        listContainer = rightContent.add(new H5EScrollablePane(layer) {
            @Override
            public void act(float delta) {
                super.act(delta);
                if (UPMath.abs(getWidth() - paneWidth) > 5) {
                    paneWidth = getWidth();
                    if (skillsList == null) return;
                    for (GCItemsListItem item : skillsList.getItems()) {
                        item.rebuildActor();
                    }
                }
            }
        }).left().top().grow().getActor();
        listContainer.getContent().top().left();

        listHandler = new GCListItemActionHandler<GCItemsListItem>() {
            @Override
            protected void onSelectionUpdate(GCItemsListItem lastSelectedItem) {
                GCItemsListItem selectedItem = getSelectedItem();
                for (GCItemsListItem item : skillsList.getItems()) {
                    item.hideButtons();
                }
                if (selectedItem != null && selectedItem.data != null && selectedItem.data.name != null && dialog.shouldShowSkillButtons()) {
                    selectedItem.showButtons();
                    Tutorials.trigger("window:builditem:select-item:" + getShortName(selectedItem.data.name));

                }
            }

            @Override
            public void onInfoButtonClicked(GCItemsListItem item) {
                dialog.onSkillIconInfoClicked(item, item.data);
            }
        };

        H5EScrollablePane treeContainer = leftContent.add(new H5EScrollablePane(layer)).padBottom(12).grow().getActor();
        treeContainer.getContent().top().left();
        leftTree = treeContainer.add(new GCItemInternalTree(layer)).top().left().getActor();

        btnBuildItem = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Build").withTutorialId("button:invention:start-build").build();
        btnBuildItem.addButtonListener(() -> {
            GCItemsListItem selectedSkill = listHandler.getSelectedItem();
            Long selectedId = selectedSkill == null ? null : selectedSkill.data.id;
            dialog.onBuildItemBtnPressed(selectedId);
        });
        lowerButtonRow.add(btnBuildItem).grow();

        btnDeleteSkill = ButtonBuilder.inLayer(layer).withStyle("button-red").withText("Forget").build();
        btnDeleteSkill.addButtonListener(() -> {
            GCItemsListItem selectedSkill = listHandler.getSelectedItem();
            Long selectedId = selectedSkill == null ? null : selectedSkill.data.id;
            if(selectedId != null) {
                dialog.onDeleteSkillBtnPressed(selectedId);
            }
        });
        lowerButtonRow.add(btnDeleteSkill).grow();
        layer.getEngine().addInputProcessor(0,new InputAdapter() {
            @Override
            public boolean keyDown(int keycode){
                if(isOpen()){
                    if (keycode == Input.Keys.UP) {
                        selectAbove();
                    }
                    if (keycode == Input.Keys.DOWN) {
                        selectBelow();
                    }
                    if (keycode == Input.Keys.FORWARD_DEL || keycode == Input.Keys.DEL) {
                        final GCItemsListItem selectedItem = skillsList.getSelectedItem();
                        if(selectedItem != null && selectedItem.data != null) {
                            dialog.onDeleteSkillBtnPressed(selectedItem.data.id);
                        }
                    }
                }
                return super.keyDown(keycode);
            }
        });
    }

    @Override
    protected void clearTab() {
        if (skillsList != null) {
            listHandler.reset();

            for(GCItemsListItem listItem : skillsList.getItems()) {
                Tutorials.removeObject("button:invention:item:" + getShortName(listItem.data.name));
            }
            skillsList.clear();
            skillsList.remove();
            skillsList = null;
            listContainer.getContent().clear();
        }
    }

    void populateData(GCSkillsData itemsData) {
        float scrollPosition = listContainer.getScrollPosition();
        clearTab();
        curSkills=itemsData;
        skillsList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        skillsList.left().top();
        listContainer.add(skillsList).left().top().grow();

        for (int index = 0; index < itemsData.size(); index++) {
            GCSkillDataItem dataItem = itemsData.get(index);
            GCItemsListItem listItem = new GCItemsListItem(layer, listHandler, dataItem, listContainer, ()-> dialog.onSkillUpgradeButtonPressed(dataItem));
            if(dataItem.name.toLowerCase().contains(filterText.toLowerCase()) ||dataItem.description.toLowerCase().contains(filterText.toLowerCase())) {
                skillsList.addItem(listItem);
            }
            Tutorials.registerObject("button:invention:item:" + getShortName(listItem.data.name), listItem);
        }
        listContainer.setScrollY(scrollPosition);
    }

    void populateCategories(GCSkillsData itemsData, GCIdeaCategoryData categoryData) {
        leftTree.clearTree();
        allSkills = GCSkillsData.createNew();
        GCItemTreeSubitem leafAll = leftTree.addItem("All",() -> {
            titleLabel.setText("Construction: All");
            populateData(allSkills);
        });
        for (GCIdeaCategoryData.GCIdeaCategoryDataItem item : categoryData.children) {
            extractCategoryData(item, leftTree, itemsData);
        }
        for (int index = itemsData.size()-1; index >= 0; index--) {
            GCSkillsData.GCSkillDataItem dataItem = itemsData.get(index);
            allSkills.add(dataItem);
        }
        leafAll.click();
    }

    @Override
    public void removeById(long id) {
        allSkills.removeById(id);

        populateData(allSkills);
    }

    private boolean extractCategoryData(GCIdeaCategoryData.GCIdeaCategoryDataItem item, GCItemInternalTree currentTree, GCSkillsData ideaData) {
        boolean hasChildren = false;
        if (item.isParentCategory) {
            GCItemTreeItem category = currentTree.addCategory(item.name);
            boolean newHasChildren = false;
            for (GCIdeaCategoryData.GCIdeaCategoryDataItem subitem : item.children) {
                boolean extractionResult = extractCategoryData(subitem, category, ideaData);
                if (extractionResult) newHasChildren = true;
            }
            if (!newHasChildren) {
                currentTree.removeCategory(category);
            }
            hasChildren = newHasChildren;
        } else {
            // Category is a clickable leaf
            GCSkillsData leafData = GCSkillsData.createNew();
            for (int index = ideaData.size()-1; index >= 0; index--) {
                GCSkillsData.GCSkillDataItem dataItem = ideaData.get(index);
                if (dataItem.parentId!=null) {
                    if (dataItem.parentId.equals(item.id)) {
                        hasChildren = true;
                        leafData.add(dataItem);
                        allSkills.add(dataItem);
                        ideaData.remove(index);
                    }
                }
            }
            if (hasChildren) {
                currentTree.addItem(item.name, () -> {
                    titleLabel.setText("Construction: " + item.name);
                    populateData(leafData);
                });
            }
        }
        return hasChildren;
    }

    private void selectBelow(){
        if(isOpen() && skillsList.getSelectedItem()!=null){
            int index = -1;
            for(int i=0;i<skillsList.getItems().size-1;i++) {
                if(skillsList.get(i)==skillsList.getSelectedItem()){
                    skillsList.getSelectedItem().setChecked(false);
                    index = i + 1;
                    break;
                }
            }
            if(index < 0 || index >= skillsList.size()) {
                return;
            }
            final GCItemsListItem itemToSelect = skillsList.get(index);
            itemToSelect.setChecked(true);
            float scrollPositionY = listContainer.getScrollY() + itemToSelect.getHeight();
            listContainer.setScrollY(scrollPositionY);
        }
    }

    private void selectAbove(){
        if(isOpen() &&skillsList.getSelectedItem()!=null){
            int index = -1;
            for(int i=skillsList.getItems().size-1;i>0;i--) {
                if(skillsList.get(i)==skillsList.getSelectedItem()){
                    skillsList.getSelectedItem().setChecked(false);
                    index = i - 1;
                    break;
                }
            }
            if(index < 0 || index >= skillsList.size()) {
                return;
            }
            final GCItemsListItem itemToSelect = skillsList.get(index);
            itemToSelect.setChecked(true);
            float scrollPositionY = listContainer.getScrollY() - itemToSelect.getHeight();
            listContainer.setScrollY(scrollPositionY);
        }

    }

    @Override
    protected void onFilter() {
        super.onFilter();
        if(curSkills!=null)
        populateData(curSkills);
    }
}
