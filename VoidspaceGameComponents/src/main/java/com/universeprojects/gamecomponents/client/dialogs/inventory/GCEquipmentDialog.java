package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5ESprite;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.Marker;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerRectangle;
import com.universeprojects.vsdata.shared.SlotLockState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class GCEquipmentDialog<S extends Comparable<S>, T extends GCInventoryItem> extends GCInventoryBase<EquipmentSlotKey<S>, T, GCEquipmentSlotComponent<S, T>> implements InventoryAux<T> {

    private final Map<EquipmentSlotKey<S>, SlotData<T>> slotMap = new LinkedHashMap<>();
    private GCEquipmentInspector<S, T> inspector;
    private H5ESpriteType spriteType;
    private final Image image;
    private H5EScrollablePane extraSlotsPane;
    private EquipmentGridPane equipmentGridPane;

    public GCEquipmentDialog(H5ELayer layer) {
        super(layer);
        this.image = new Image();
        image.setScaling(Scaling.fit);
        image.setTouchable(Touchable.disabled);
    }

    public void updateItems(Map<EquipmentSlotKey<S>, T> slotItemMap) {
        for (SlotData<T> data : slotMap.values()) {
            data.item = null;
        }
        for (Map.Entry<EquipmentSlotKey<S>, SlotData<T>> entry : slotMap.entrySet()) {
            SlotData<T> slotData = entry.getValue();
            slotData.item = slotItemMap.get(entry.getKey());
        }
        invalidateItems();
    }

    /**
     * Updates the sprite of the equipment dialog and sets up the slotMap keys to be used when populating the
     * equipment dialog slots with items
     * @param spriteType The type of sprite to be used in the equipment dialog
     */
    protected void updateSpriteType(H5ESpriteType spriteType) {
        this.spriteType = spriteType;
        image.setDrawable(new TextureRegionDrawable(spriteType.getTextureRegion()));
        resetGraphics();

        // Setting the keys for equipment
        Map<String, List<MarkerRectangle>> typeToMarkerMap = new TreeMap<>();
        for (Marker marker : spriteType.getMarkers()) {
            if (!(marker instanceof MarkerRectangle)) continue;
            List<MarkerRectangle> markers = typeToMarkerMap.get(marker.getTypeLabel());
            if(markers == null) {
                markers = new ArrayList<>();
                typeToMarkerMap.put(marker.getTypeLabel(), markers);
            }
            markers.add((MarkerRectangle) marker);
        }
        slotMap.clear();
        generateSlotDataMap(typeToMarkerMap, slotMap);

        invalidateComponents();
    }

    protected abstract void generateSlotDataMap(Map<String, List<MarkerRectangle>> typeToMarkerMap, Map<EquipmentSlotKey<S>, SlotData<T>> slotDataMap);

    private void resetGraphics() {
        clearComponents();
        slotMap.clear();
        add(image).grow();
        invalidateHierarchy();
    }

    public void forEachItem(Callable1Args<T> callable) {
        for(SlotData<T> data : slotMap.values()) {
            if(data != null && data.item != null) {
                callable.call(data.item);
            }
        }
    }

    @Override
    public void layout() {
        updateComponents();
        updateItems();
        super.layout();
        if(spriteType == null) return;
        image.layout();
        final float scale = getScaleFactor();
        for (GCEquipmentSlotComponent<S, T> component : getComponents().values()) {
            final MarkerRectangle marker = component.getMarker();
            if(marker == null) {
                continue;
            }
            final float size = Math.min(marker.width, marker.height);
            component.setIconSize(size * scale);
            positionForMarker(component, scale, marker);
            Tutorials.registerObject("equipment:slot:" + marker, component);
        }
        if(equipmentGridPane != null) {
            equipmentGridPane.getContent().align(Align.topLeft);
            MarkerRectangle marker = equipmentGridPane.marker;
            WidgetGroup component = equipmentGridPane.getComponent();
            component.setWidth(marker.width * scale);
            component.setHeight(marker.height * scale);
            positionForMarker(component, scale, marker);
        }
        if(extraSlotsPane != null) {
            extraSlotsPane.getContent().align(Align.top);
            extraSlotsPane.setWidth(400);
            extraSlotsPane.setHeight(60);
            extraSlotsPane.layout();
            extraSlotsPane.setX(getWidth() / 2f, Align.center);
            extraSlotsPane.setY(-10, Align.top);
        }
    }

    protected float getScaleFactor() {
        return image.getImageWidth() / spriteType.getWidth();
    }

    private void positionForMarker(WidgetGroup actor, float scale, MarkerRectangle marker) {
        final UPVector position = marker.getPosition(0, 0, spriteType.getHeight());
        actor.setOrigin(Align.center);
        actor.layout();
        actor.setPosition(
            position.x * scale + image.getImageX(),
            position.y * scale + image.getImageY(),
            Align.center);
    }

    @Override
    protected GCEquipmentSlotComponent<S, T> createComponent(EquipmentSlotKey<S> key) {
        SlotData<T> slotData = slotMap.get(key);
        if(slotData.equipmentGrid) {
            return createGridComponent(key, slotData);
        } else {
            return createSingleComponent(key, slotData);
        }
    }

    private GCEquipmentSlotComponent<S, T> createGridComponent(EquipmentSlotKey<S> key, SlotData<T> slotData) {
        final GCEquipmentSlotComponent<S, T> component = new GridSlotComponent(key, slotData.slotConfig, slotData.lockState);
        getOrCreateEquipmentGrid(slotData.marker).addToCategory(slotData.sizeClass, component);
        return component;
    }

    private GCEquipmentSlotComponent<S, T> createSingleComponent(EquipmentSlotKey<S> key, SlotData<T> slotData) {
        final GCEquipmentSlotComponent<S, T> component = new GCEquipmentSlotComponent<>(this, key, slotData.marker, slotData.slotConfig, slotData.lockState);
        component.addListener(new TextTooltip(slotData.tooltip, getEngine().getSkin()));
        if (slotData.marker != null) {
            addActor(component);
        } else {
            getOrCreateExtraSlotsTable().add(component);
        }
        return component;
    }

    private Table getOrCreateExtraSlotsTable() {
        if(extraSlotsPane == null) {
            extraSlotsPane = new H5EScrollablePane(getLayer());
            addActor(extraSlotsPane);
        }
        return extraSlotsPane.getContent();
    }

    private EquipmentGridPane getOrCreateEquipmentGrid(MarkerRectangle marker) {
        if(equipmentGridPane == null) {
            if(marker == null) {
                marker = new MarkerRectangle();
                marker.x = spriteType.getWidth()/2;
                marker.y = spriteType.getHeight()/2;
                float cellSize = GCInventorySlotComponent.DEFAULT_ICON_SIZE / getScaleFactor();
                marker.width = (int) UPMath.ceil(cellSize * 3.4 + (cellSize * 0.4));
                marker.height = (int) UPMath.ceil(cellSize * 3.2);
            }
            equipmentGridPane = new EquipmentGridPane(marker);
            addActor(equipmentGridPane.getComponent());
        }
        return equipmentGridPane;
    }

    @Override
    public void clearComponents() {
        super.clearComponents();
        if(inspector != null) {
            inspector.close();
        }
        extraSlotsPane = null;
        equipmentGridPane = null;
    }

    @Override
    protected Collection<EquipmentSlotKey<S>> getSlots() {
        return slotMap.keySet();
    }

    @Override
    protected T getItem(EquipmentSlotKey<S> key) {
        SlotData<T> slotData = slotMap.get(key);
        if(slotData == null) {
            return null;
        }
        return slotData.item;
    }

    @Override
    @SuppressWarnings("rawtypes")
    protected List<InventoryAction> getActions(EquipmentSlotKey<S> key) {
        return Collections.emptyList();
    }

    @Override
    protected void onSelected(GCEquipmentSlotComponent<S, T> component) {
        if(inspector == null) {
            if(component == null) {
                return;
            } else {
                inspector = createInspector();
            }
        }
        if(component == null) {
            inspector.close();
        } else {
            inspector.setSlotComponent(component);
            inspector.open(true);
            Tutorials.trigger("window:inspect");
        }
    }

    /**
     * Create and return an inspector window
     */
    protected GCEquipmentInspector<S, T> createInspector() {
        final GCEquipmentInspector<S, T> inspector = new GCEquipmentInspector<>(getLayer());
        inspector.setPositionNextToSlot(true);
        return inspector;
    }

    @SuppressWarnings("rawtypes")
    public GCEquipmentInspector getInspector () {
        return inspector;
    }

    public void setInspector(GCEquipmentInspector<S, T> inspector) {
        this.inspector = inspector;
    }

    public Image getImage() {
        return image;
    }

    public static class SlotData<T extends GCInventoryItem> {
        public T item;
        public String tooltip;
        public boolean equipmentGrid = false;
        public MarkerRectangle marker;
        public SlotConfig slotConfig;
        public int sizeClass;
        public SlotLockState lockState = SlotLockState.OPEN;
    }

    private class EquipmentGridPane {
        private final MarkerRectangle marker;
        private final Table table;
        private final H5EScrollablePane pane;
        int columnCount = 0;
        int lastSizeClass = -1;

        public EquipmentGridPane(MarkerRectangle marker) {
            this.marker = marker;
            this.table = new Table(getEngine().getSkin());
            this.pane = new H5EScrollablePane(getLayer());
            table.setBackground(StyleFactory.INSTANCE.panelInternal12);
            table.add(pane).grow();
        }

        public void addToCategory(int sizeClass, GCEquipmentSlotComponent<S, T> component) {
            Table table = pane.getContent();
            if (sizeClass == lastSizeClass) {
                int columns = UPMath.capMin(getNumColumns(), 1);
                if(columnCount >= columns) {
                    columnCount = 0;
                    table.row();
                    table.add();
                }
            } else {
                table.row();
                H5ESprite sprite = new H5ESprite(getLayer(), "images/GUI/ui2/class"+sizeClass+"-header");
                sprite.setTouchable(Touchable.disabled);
                table.add(sprite).height(GCInventorySlotComponent.DEFAULT_ICON_SIZE);
                columnCount = 0;
                lastSizeClass = sizeClass;
            }
            table.add(component);
            columnCount++;
        }

        public WidgetGroup getComponent() {
            return table;
        }

        public Table getContent() {
            return pane.getContent();
        }

        private int getNumColumns() {
            float imageWidth = getScaleFactor() * (marker.width + 10);
            return UPMath.capMin((int)(imageWidth / GCInventorySlotComponent.DEFAULT_ICON_SIZE) - 1, 1);
        }
    }

    private class GridSlotComponent extends GCEquipmentSlotComponent<S, T> {
        public GridSlotComponent(EquipmentSlotKey<S> key, SlotConfig slotConfig, SlotLockState lockState) {
            super(GCEquipmentDialog.this, key, null, slotConfig, lockState);
        }

        @Override
        public String getEmptySpriteKey() {
            return "images/icons/base/bg_empty.png";
        }

        @Override
        public String getEmptyFrameIconStyle() {
            return null;
        }

        @Override
        public String getFrameStyle() {
            return "icon-item_frame";
        }

        @Override
        public int getFrameBaseIconSize() {
            return GCItemIcon.BASE_FRAME_ICON_SIZE;
        }
    }
}
