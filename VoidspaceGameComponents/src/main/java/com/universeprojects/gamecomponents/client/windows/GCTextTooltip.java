package com.universeprojects.gamecomponents.client.windows;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip;
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager;

/**
 * Deprecated: Use H5ETextTooltip instead (or better yet, just use H5EButton.setTooltip()).
 */
@Deprecated
public class GCTextTooltip extends TextTooltip {
    public GCTextTooltip(String text, Skin skin) {
        super(text, skin);
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        TooltipManager.getInstance().hideAll();
        mouseMoved(event, x, y);
        return super.touchDown(event, x, y, pointer, button);
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        TooltipManager.getInstance().hideAll();
        super.touchUp(event, x, y, pointer, button);
    }
}
