package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;


@SuppressWarnings("FieldCanBeLocal")
public class GCInventoryListItem extends GCListItem {

    public static final int ICON_SIZE = GCItemIcon.DEFAULT_ICON_SIZE;
    private final GCInventoryDataItem data;
    private final GCItemIcon<GCInventoryItem> itemIcon;

    public GCInventoryListItem(H5ELayer layer, GCListItemActionHandler<GCInventoryListItem> actionHandler, GCInventoryDataItem data) {
        super(layer, actionHandler, "gc-list-blank");

        center();
        defaults().center();

        if(data != null) {
            if (data.index < 0) {
                throw new IllegalArgumentException("Index can't be negative");
            }
            if (Strings.isEmpty(data.item.getName())) {
                throw new IllegalArgumentException("Idea name can't be blank");
            }
            if (Strings.isEmpty(data.item.getIconSpriteKey())) {
                throw new IllegalArgumentException("Idea icon key can't be blank");
            }
        }

        this.data = data;

        itemIcon = new GCItemIcon<>(layer);
        itemIcon.setItem(data.item);
        add(itemIcon).size(ICON_SIZE);

    }

    @Override
    public float getPrefWidth() {
        return ICON_SIZE;
    }

    @Override
    public float getPrefHeight() {
        return ICON_SIZE;
    }

    @Deprecated
    public int getIndex() {
        return data.index;
    }

    @Deprecated
    public String getName() {
        return data.item.getName();
    }

    @Deprecated
    public int getQuantity() {
        return (int)data.item.getQuantity();
    }


}
