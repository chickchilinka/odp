package com.universeprojects.gamecomponents.client.elements.actionbar;

import java.util.Map;

public interface GCActionBarController {
    void callAction(GCActionBarType type, int row, int index);

    void clear();

    GCOperation getAction(GCActionBarType type, int row, int index);

    void setAction(GCActionBarType type, int row, int index, GCOperation myAction, boolean save);

    void refreshIcons();

    Map<RowIndexPair, GCOperation> getOperations();

    class RowIndexPair {
        final GCActionBarType type;
        final int row;
        final int index;

        public RowIndexPair(GCActionBarType type, int row, int index) {
            this.type = type;
            this.row = row;
            this.index = index;
        }

        public int getRow() {
            return row;
        }

        public int getIndex() {
            return index;
        }

        public GCActionBarType getType() {
            return type;
        }
    }
}
