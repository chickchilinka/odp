package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.gamecomponents.client.elements.GCSlider;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.drawable.RectShapeDrawable;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * A simple dialog for creating RGB color strings
 *
 * @author J. K. Rowling
 */
public class GCColorChooser extends Table {

    public static final float FACTOR = 255f;
    private final H5ELayer layer;
    private final H5EInputBox fieldRGB;
    private final GCSlider sliderR, sliderG, sliderB;
    private final Color color;

    private final static int FIELD_W = 80;

    private int r, g, b;

    public GCColorChooser(H5ELayer layer, int r, int g, int b) {
        this(layer, new Color(r / FACTOR, g / FACTOR, b / FACTOR, 1f));
    }

    public GCColorChooser(H5ELayer layer, Color defaultColor) {
        this.layer = layer;

        this.color = new Color(defaultColor);
        setRGB(defaultColor);
        RectShapeDrawable rect = new RectShapeDrawable(color);
        Image colorSample = new Image(rect);
        rect.setMinWidth(100);
        rect.setMinHeight(100);
        add(colorSample).left();

        defaults().left();

        //////////////////////////////////////
        //   RED
        //

        row().padTop(25);
        add(new H5ELabel("Red:", this.layer));

        H5EInputBox fieldR = new H5EInputBox(layer);
        fieldR.setWidth(FIELD_W);
        fieldR.setTypeNumber();
        add(fieldR).right();

        sliderR = new GCSlider(layer);
        sliderR.attachInputBox(fieldR);
        sliderR.setRange(0, FACTOR, 1);
        sliderR.setValue(r);
        sliderR.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSliderInput();
                event.handle();
            }
        });
        row();
        add(sliderR).colspan(2).growX();

        //////////////////////////////////////
        //   GREEN
        //

        row().padTop(15);
        add(new H5ELabel("Green:", this.layer));

        H5EInputBox fieldG = new H5EInputBox(layer);
        fieldG.setWidth(FIELD_W);
        fieldG.setTypeNumber();
        add(fieldG).right();

        sliderG = new GCSlider(layer);
        sliderG.attachInputBox(fieldG);
        sliderG.setRange(0, FACTOR, 1);
        sliderG.setValue(g);
        sliderG.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSliderInput();
                event.handle();
            }
        });
        row();
        add(sliderG).colspan(2).growX();


        //////////////////////////////////////
        //   BLUE
        //

        row().padTop(15);
        add(new H5ELabel("Blue:", this.layer));

        H5EInputBox fieldB = new H5EInputBox(layer);
        fieldB.setWidth(FIELD_W);
        fieldB.setTypeNumber();
        add(fieldB).right();

        sliderB = new GCSlider(layer);
        sliderB.attachInputBox(fieldB);
        sliderB.setRange(0, FACTOR, 1);
        sliderB.setValue(b);
        sliderB.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSliderInput();
                event.handle();
            }
        });
        row();
        add(sliderB).colspan(2).growX();


        //////////////////////////////////////
        //   R G B
        //

        row().padTop(15);
        add(new H5ELabel("RGB-Color:", this.layer));

        fieldRGB = new H5EInputBox(layer);
        fieldRGB.setWidth(FIELD_W);
        fieldRGB.setText(color.toString().substring(0, 6));
        fieldRGB.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onRgbInput();
                event.handle();
            }
        });
        add(fieldRGB).right().padLeft(10);
    }

    private void applyColor() {
        color.set(r / FACTOR, g / FACTOR, b / FACTOR, 1f);
        fieldRGB.setText(color.toString().substring(0, 6));
    }

    public void onRgbInput() {
        String colorStr = fieldRGB.getText().trim();
        Color fieldColor;
        try {
            fieldColor = Color.valueOf(colorStr);
        } catch (Throwable ex) {
            return;
        }
        setRGB(fieldColor);

        sliderR.setValue(r);
        sliderG.setValue(g);
        sliderB.setValue(b);

        this.color.set(fieldColor);
    }

    public Color getChosenColor() {
        return color.cpy();
    }

    protected void setRGB(Color color) {
        r = (int) (color.r * FACTOR);
        g = (int) (color.g * FACTOR);
        b = (int) (color.b * FACTOR);
    }

    public void onSliderInput() {
        r = (int) sliderR.getValue();
        g = (int) sliderG.getValue();
        b = (int) sliderB.getValue();

        applyColor();
    }
}
