package com.universeprojects.gamecomponents.client.elements.preselector;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.html5engine.client.framework.*;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCPreselector {

    private static final String INNER_KEY = "images/GUI/pre-selection selector/circle2";
    private static final String OUTTER_KEY = "images/GUI/pre-selection selector/circle1";
    private static final String BRANCH_KEY = "images/GUI/pre-selection selector/main-branch1";
    private static final String CAP_KEY = "images/GUI/pre-selection selector/text-end1";

    private static final int OUTER_OFFSET_X = 7;
    private static final int OUTER_OFFSET_Y = 6;
    private static final int BRANCH_OFFSET_X = 66;
    private static final int BRANCH_OFFSET_Y = 8;
    private static final int CAP_OFFSET_X = 190;
    private static final int CAP_OFFSET_Y = 96;
    private static final int LABEL_OFFSET_Y = 112;


    private Group mainElement;
    private H5ESprite innerCircle;
    private H5ESprite outerCircle;
    private H5ESprite nameBranch;
    private H5ESprite nameCap;
    private H5ELabel nameLabel;
    private H5ELayer layer;
    private H5ESimpleAnimationBehaviour innerSpinBehaviour;
    private H5ESimpleAnimationBehaviour outerSpinBehaviour;
    private H5EBouncyBehaviour innerBounce;
    private H5EBouncyBehaviour outerBounce;
    private H5EBouncyBehaviour branchBounce;
    private H5EBouncyBehaviour capBounce;


    public GCPreselector(H5ELayer layer){
        this.layer = layer;

        mainElement = new Group();

        innerCircle = new H5ESprite(layer, INNER_KEY);
        innerCircle.addTo(layer);
        innerCircle.setName("preselector-inner");
        innerCircle.setOrigin(Align.center);
        innerCircle.setTouchable(Touchable.disabled);
       // innerCircle.setRevertViewportRotation(true); //TODO: WHAT DIS?
        innerCircle.setVisible(true);
        innerCircle.setBlendingMode(BlendingMode.LIGHTER);
        innerBounce = new H5EBouncyBehaviour(innerCircle);
        innerBounce.setSpeedMultiplier(0.15f);
        innerSpinBehaviour = new H5ESimpleAnimationBehaviour(innerCircle);
        mainElement.addActor(innerCircle);

        outerCircle = new H5ESprite(layer, OUTTER_KEY);
        outerCircle.addTo(layer);
        outerCircle.setName("preselector-outter");
        outerCircle.setOrigin(Align.center);
        outerCircle.setTouchable(Touchable.disabled);
        //outerCircle.setRevertViewportRotation(true); //TODO: WHAT DIS?
        outerCircle.setVisible(true);
        outerCircle.setX(OUTER_OFFSET_X);
        outerCircle.setY(OUTER_OFFSET_Y);
        outerCircle.setBlendingMode(BlendingMode.LIGHTER);
        outerBounce = new H5EBouncyBehaviour(outerCircle);
        outerBounce.setSpeedMultiplier(0.15f);
        outerSpinBehaviour = new H5ESimpleAnimationBehaviour(outerCircle);
        mainElement.addActor(outerCircle);

        nameBranch = new H5ESprite(layer, BRANCH_KEY);
        nameBranch.addTo(layer);
        nameBranch.setName("preselector-branch");
        nameBranch.setOrigin(Align.left);
        nameBranch.setTouchable(Touchable.disabled);
        //nameBranch.setRevertViewportRotation(true); //TODO: WHAT DIS?
        nameBranch.setVisible(true);
        nameBranch.setX(BRANCH_OFFSET_X);
        nameBranch.setY(BRANCH_OFFSET_Y);
        nameBranch.setBlendingMode(BlendingMode.LIGHTER);
        branchBounce = new H5EBouncyBehaviour(nameBranch);
        branchBounce.setSpeedMultiplier(0.15f);
        mainElement.addActor(nameBranch);

        nameCap = new H5ESprite(layer, CAP_KEY);
        nameCap.addTo(layer);
        nameCap.setName("preselector-cap");
        nameCap.setTouchable(Touchable.disabled);
        //nameCap.setRevertViewportRotation(true); //TODO: WHAT DIS?
        nameCap.setVisible(true);
        nameCap.setX(CAP_OFFSET_X);
        nameCap.setY(CAP_OFFSET_Y);
        nameCap.setBlendingMode(BlendingMode.LIGHTER);
        capBounce = new H5EBouncyBehaviour(nameCap);
        capBounce.setSpeedMultiplier(0.15f);
        mainElement.addActor(nameCap);

        nameLabel = new H5ELabel("", layer);
        nameLabel.setX(CAP_OFFSET_X);
        nameLabel.setY(LABEL_OFFSET_Y);
        mainElement.addActor(nameLabel);

        mainElement.setOrigin(innerCircle.getOriginX(), innerCircle.getOriginY());
        layer.addActorToTop(mainElement, layer.getDefaultLevel() + 1);
        setVisible(false);
    }

    public void setPosition(float x, float y){
        innerSpinBehaviour.deactivate();
        outerSpinBehaviour.deactivate();
        mainElement.setPosition(x - innerCircle.getOriginX(),y - innerCircle.getOriginX());
    }

    public void setPosition(UPVector pos){
        setPosition(pos.x, pos.y);
    }

    public void setRotation(float rotation){
        mainElement.setRotation(rotation);
    }

    public void setScale(float scale){
        mainElement.setScale(scale);
    }

    public void setScaleY(float scaleY){
        mainElement.setScaleY(scaleY);
    }

    public void setScaleX(float scaleX){
        mainElement.setScaleX(scaleX);
    }

    public void setVisible(boolean visible) {
        mainElement.setVisible(visible);
        if(visible){
            innerCircle.setScale(0,1);
            outerCircle.setScale(0,1);
            nameBranch.setScale(0, 1);
            nameCap.setScale(0, 1);
            innerBounce.setScale(1, 1, true);
            outerBounce.setScale(1, 1, true);
            branchBounce.setScale(1, 1, true);
            capBounce.setScale(1, 1, true);
            innerSpinBehaviour.activate();
            innerSpinBehaviour.setRotationSpeed(1f * 50F);
            outerSpinBehaviour.activate();
            outerSpinBehaviour.setRotationSpeed(-2f * 50F);
        }
    }

    public void setLabel(String name){
        nameLabel.setText(name);
        nameCap.setX(CAP_OFFSET_X + (nameLabel.getPrefWidth()));
    }

    public boolean isVisible(){
        return mainElement.isVisible();
    }
}
