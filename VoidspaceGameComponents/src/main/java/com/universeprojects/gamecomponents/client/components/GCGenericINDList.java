package com.universeprojects.gamecomponents.client.components;

import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.List;

/**
 * IND stands for Item, Name, Description. The icon shows in the left side (if there is one),
 * then the name and description are showed to the right of the icon, on top of each other.
 */
public class GCGenericINDList<T> extends H5EScrollablePane {
    final private GCList<GCGenericINDListItem<T>> list;

    public GCGenericINDList(H5ELayer layer) {
        super(layer);
        list = new GCList<>(layer, 1, true);
        create();
    }

    public GCGenericINDList(H5ELayer layer, int columns, boolean fillRowFirst) {
        super(layer);
        list = new GCList<>(layer, columns, fillRowFirst);
        create();
    }

    public GCGenericINDList(H5ELayer layer, int columns, int rowSpacing, int colSpacing, boolean fillRowFirst) {
        super(layer);
        list = new GCList<>(layer, columns, rowSpacing, colSpacing, fillRowFirst);
        create();
    }

    private void create() {
        setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        add(list).left().top().grow();
    }

    public void addItem(GCGenericINDListItemData<T> item) {
        addItem(item, null);
    }

    public void addItem(GCGenericINDListItemData<T> item, GCListItemActionHandler<GCGenericINDListItem<T>> selectionHandler) {
        list.addItem(new GCGenericINDListItem<>(getLayer(), selectionHandler, item, this));
        layout();
    }

    public T getSelectedItem() {
        GCGenericINDListItem<T> selectedItem = list.getSelectedItem();
        if (selectedItem != null) {
            return selectedItem.data.data;
        }
        return null;
    }

    public int size() {
        return list.size();
    }

    public List<T> getSelectedItems() {
        Array<GCGenericINDListItem<T>> elements = list.getSelectedItems();
        List<T> items = new ArrayList<T>();
        for (GCGenericINDListItem<T> element : elements) {
            items.add(element.getData().data);
        }
        return items;
    }

    public void setMaxCheckCount(int count) {
        list.setMaxCheckCount(count);
    }

    @Override
    public void clear() {
        list.clear();
    }
}
