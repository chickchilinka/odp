package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBar;
import com.universeprojects.gamecomponents.client.windows.GCTextTooltip;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5EResourceManager;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.drawable.RectShapeDrawable;
import com.universeprojects.html5engine.client.framework.drawable.ScaledDrawable;

public class StyleFactory {
    public static final String RESOURCE_DEFAULT = "default";
    public static final String RESOURCE_DEFAULT_HORIZONTAL = "default-horizontal";
    public static final String RESOURCE_DEFAULT_VERTICAL = "default-vertical";

    public static StyleFactory INSTANCE;

    public GCWindow.GCWindowStyle windowStyleDefault;
    public GCWindow.GCWindowStyle windowStyleMainMenu;
    public GCWindow.GCWindowStyle windowStyleInspector;
    public GCWindow.GCWindowStyle windowStyleTooltip;
    public GCWindow.GCWindowStyle windowStyleLoginDialog;
    public GCWindow.GCWindowStyle windowStyleInvention;
    public GCWindow.GCWindowStyle windowStyleSettings;
    public GCWindow.GCWindowStyle windowStyleWhiteRadialGradient;
    public GCWindow.GCWindowStyle windowStylePopup;
    public GCWindow.GCWindowStyle windowStylePopup2;
    public GCWindow.GCWindowStyle windowStyleClear;

    public NinePatchDrawable panelStyleBlueTopAndBottomBorderOpaque;
    public NinePatchDrawable panelStyleBlueCornersSemiTransparent;
    public NinePatchDrawable panelStyleYellowTopAndBottomBorderSemiTransparent;
    public NinePatchDrawable panelStyleBlack;
    public NinePatchDrawable panelInternal12;
    public NinePatchDrawable panelStyleDeepInsetDark;
    public NinePatchDrawable panelStyleSimpleSemiTransparent;
    public NinePatchDrawable panelStyleDarkBorderSemiTransparent;
    public NinePatchDrawable panelStyleWhiteBorder;

    public ScrollPane.ScrollPaneStyle scrollPaneStyleBlueTopAndBottomBorderOpaque;
    public ScrollPane.ScrollPaneStyle scrollPaneStyleBlueCornersSemiTransparent;
    public ScrollPane.ScrollPaneStyle scrollPaneStyleSimpleSemiTransparent;
    public ScrollPane.ScrollPaneStyle scrollPaneStyleYellowTopAndBottomBorderOpaque;
    public ScrollPane.ScrollPaneStyle scrollPaneStyleBlueGridOpaque;

    public ImageTextButton.ImageTextButtonStyle buttonStyleDefault;
    public ImageTextButton.ImageTextButtonStyle buttonStyleBlueYellow;
    public ImageTextButton.ImageTextButtonStyle buttonStyleBlueYellowCheckableNoPadding;
    public ImageTextButton.ImageTextButtonStyle buttonStyleYellowPadding;
    public ImageTextButton.ImageTextButtonStyle buttonStyleBlueYellowNoPadding;
    public ImageTextButton.ImageTextButtonStyle buttonStyleCheckbox;
    public ImageTextButton.ImageTextButtonStyle buttonStyleTabTop;

    public BitmapFont fontElectrolize;
    public BitmapFont fontElectrolizeBold;
    public BitmapFont fontElectrolizeSmall;
    public BitmapFont fontLaconic;
    public BitmapFont fontLaconicBold;
    public BitmapFont fontLaconicLight;
    public BitmapFont fontLaconicSmall;
    public BitmapFont fontArialSmall;
    public BitmapFont fontCourierPrimeSans;

    public BitmapFont font;
    public BitmapFont fontBold;
    public BitmapFont fontTitle;
    public BitmapFont fontButtons;
    public BitmapFont fontSmall;
    public BitmapFont fontMono;


    public Label.LabelStyle labelDefault;
    public Label.LabelStyle labelBoldDefault;
    public Label.LabelStyle labelSmallDefault;
    public Label.LabelStyle labelMono;
    public Label.LabelStyle labelSmallLaconic;
    public Label.LabelStyle labelElectrolize;
    public Label.LabelStyle labelSmallElectrolize;
    public Label.LabelStyle labelSmallElectrolizeBorders;
    public Label.LabelStyle labelElectrolizeBold;
    public Label.LabelStyle labelArialSmall;



    private final H5EResourceManager rm;
    private final TextureAtlas atlas;

    public StyleFactory(H5EResourceManager rm, TextureAtlas atlas) {
        this.rm = rm;
        this.atlas = atlas;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public Skin generateSkin() {
        Skin skin = new Skin();


        fontElectrolize = createFont("fonts/Electrolize/electrolize.fnt");
        skin.add("font-electrolize", fontElectrolize);
        fontElectrolizeBold = createFont("fonts/Electrolize/electrolize-bold.fnt");
        skin.add("font-electrolize-bold", fontElectrolizeBold);
        fontElectrolizeSmall = createFont("fonts/Electrolize/electrolize-small.fnt");
        skin.add("font-electrolize-small", fontElectrolizeSmall);

        fontLaconic = createFont("fonts/Laconic/laconic-regular.fnt");
        skin.add("font-laconic", fontLaconic);
        fontLaconicBold = createFont("fonts/Laconic/laconic-bold.fnt");
        skin.add("font-laconic-bold", fontLaconicBold);
        fontLaconicLight = createFont("fonts/Laconic/laconic-light.fnt");
        skin.add("font-laconic-light", fontLaconicLight);

        fontLaconicSmall =  createFont("fonts/Laconic/laconic-bold.fnt");
        skin.add("font-laconic-smaller", fontLaconicSmall);

        fontArialSmall = createFont("fonts/Arial/small.fnt");
        skin.add("font-arial-small", fontArialSmall);

        fontCourierPrimeSans = createFont("fonts/CourierPrimeSans/CourierPrimeSans.fnt");
        skin.add("font-courier-prime-sans", fontCourierPrimeSans);



        font = fontElectrolize;
        skin.add(RESOURCE_DEFAULT, font);

        fontBold = fontElectrolizeBold;
        skin.add("default-bold", fontBold);

        fontTitle = fontLaconic;
        skin.add("default-title", fontTitle);

        fontButtons = fontLaconic;
        skin.add("default-buttons", fontButtons);

        fontSmall = fontArialSmall;
        skin.add("default-small", fontSmall);

        fontMono = fontCourierPrimeSans;
        skin.add("default-mono", fontMono);


        labelDefault = new Label.LabelStyle(font, Color.WHITE);
        skin.add(RESOURCE_DEFAULT, labelDefault);
        labelBoldDefault = new Label.LabelStyle(fontBold, Color.WHITE);
        skin.add("label-bold", labelBoldDefault);
        labelSmallDefault = new Label.LabelStyle(fontSmall, Color.WHITE);
        skin.add("label-small", labelSmallDefault);
        labelMono = new Label.LabelStyle(fontMono, Color.WHITE);
        skin.add("label-mono", labelMono);
        labelSmallLaconic = new Label.LabelStyle(fontLaconicSmall, Color.WHITE);
        skin.add("label-laconic-small", labelSmallLaconic);
        labelElectrolize = new Label.LabelStyle(fontElectrolize, Color.WHITE);
        skin.add("label-electrolize", labelElectrolize);
        labelSmallElectrolize = new Label.LabelStyle(fontElectrolizeSmall, Color.WHITE);
        labelSmallElectrolizeBorders = new Label.LabelStyle(fontElectrolizeSmall, Color.WHITE);
        skin.add("label-electrolize-small", labelSmallElectrolize);
        labelElectrolizeBold = new Label.LabelStyle(fontElectrolizeBold, Color.WHITE);
        skin.add("label-electrolize-bold", labelElectrolizeBold);


        for (TextureRegion region : font.getRegions()) {
            region.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        for (TextureRegion region : fontBold.getRegions()) {
            region.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }

//        final NinePatch mediumButtonPatch = atlas.createPatch("GUI/blue/generic-button-medium");
//        NinePatchDrawable mediumButtonDrawable = new NinePatchDrawable(mediumButtonPatch);
//        ImageTextButton.ImageTextButtonStyle mediumButtonStyle = createButtonStyle(mediumButtonDrawable, font, 1.1f, 2f);
//        skin.add(RESOURCE_DEFAULT, mediumButtonStyle, ImageTextButton.ImageTextButtonStyle.class);
//        skin.add(RESOURCE_DEFAULT, mediumButtonStyle, Button.ButtonStyle.class);


        final NinePatchDrawable ui2ButtonNormalDrawable   = ninePatchDrawable("GUI/ui2/button_wide_normal1");
        final NinePatchDrawable ui2ButtonHoverDrawable    = ninePatchDrawable("GUI/ui2/button_wide_hovered1");
        final NinePatchDrawable ui2ButtonPressedDrawable  = ninePatchDrawable("GUI/ui2/button_wide_pressed1");
        final NinePatchDrawable ui2ButtonDisabledDrawable = ninePatchDrawable("GUI/ui2/button_wide_disabled1");

        buttonStyleDefault = new ImageTextButton.ImageTextButtonStyle(ui2ButtonNormalDrawable, ui2ButtonPressedDrawable, ui2ButtonNormalDrawable, fontButtons);
        buttonStyleDefault.over = ui2ButtonHoverDrawable;
        buttonStyleDefault.checkedOver = ui2ButtonHoverDrawable;
        buttonStyleDefault.disabled = ui2ButtonDisabledDrawable;
        skin.add(RESOURCE_DEFAULT, buttonStyleDefault, ImageTextButton.ImageTextButtonStyle.class);
        skin.add(RESOURCE_DEFAULT, buttonStyleDefault, Button.ButtonStyle.class);
        skin.add("button-small", buttonStyleDefault, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-small", buttonStyleDefault, Button.ButtonStyle.class);

        final NinePatchDrawable ui2Button2NormalDrawable       = ninePatchDrawable("GUI/ui2/button2_unselected_normal");
        final NinePatchDrawable ui2Button2HoverDrawable        = ninePatchDrawable("GUI/ui2/button2_unselected_hover");
        final NinePatchDrawable ui2Button2CheckedDrawable      = ninePatchDrawable("GUI/ui2/button2_selected_normal");
        final NinePatchDrawable ui2Button2CheckedHoverDrawable = ninePatchDrawable("GUI/ui2/button2_selected_hover");

        buttonStyleBlueYellow = new ImageTextButton.ImageTextButtonStyle(ui2Button2NormalDrawable, ui2Button2CheckedDrawable, null, fontButtons);
        buttonStyleBlueYellow.over = ui2Button2HoverDrawable;
        skin.add("button2", buttonStyleBlueYellow, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button2", buttonStyleBlueYellow, Button.ButtonStyle.class);

        final NinePatchDrawable ui2Button2NormalNoPaddingDrawable       = zeroPadding(ninePatchDrawable("GUI/ui2/button2_unselected_normal"));
        final NinePatchDrawable ui2Button2HoverNoPaddingDrawable        = zeroPadding(ninePatchDrawable("GUI/ui2/button2_unselected_hover"));
        final NinePatchDrawable ui2Button2CheckedNoPaddingDrawable      = zeroPadding(ninePatchDrawable("GUI/ui2/button2_selected_normal"));
        final NinePatchDrawable ui2Button2CheckedHoverNoPaddingDrawable = zeroPadding(ninePatchDrawable("GUI/ui2/button2_selected_hover"));

        buttonStyleBlueYellowCheckableNoPadding = new ImageTextButton.ImageTextButtonStyle(ui2Button2NormalNoPaddingDrawable, null, ui2Button2CheckedNoPaddingDrawable, fontButtons);
        buttonStyleBlueYellowCheckableNoPadding.over = ui2Button2HoverNoPaddingDrawable;
        buttonStyleBlueYellowCheckableNoPadding.checkedOver = ui2Button2CheckedHoverNoPaddingDrawable;

        buttonStyleYellowPadding = new ImageTextButton.ImageTextButtonStyle(ui2Button2CheckedNoPaddingDrawable, null, null, fontButtons);
        buttonStyleYellowPadding.over = ui2Button2CheckedHoverNoPaddingDrawable;

        buttonStyleBlueYellowNoPadding = new ImageTextButton.ImageTextButtonStyle(ui2Button2NormalNoPaddingDrawable, ui2Button2CheckedNoPaddingDrawable, null, fontButtons);
        buttonStyleBlueYellowNoPadding.over = ui2Button2HoverNoPaddingDrawable;

        final NinePatchDrawable uiTabButtonNormalDrawable       = ninePatchDrawable("GUI/ui2/button2_unselected_normal");
        final NinePatchDrawable uiTabButtonHoverDrawable        = ninePatchDrawable("GUI/ui2/button2_unselected_hover");
        final NinePatchDrawable uiTabButtonCheckedDrawable      = ninePatchDrawable("GUI/ui2/button2_selected_normal");
        final NinePatchDrawable uiTabButtonCheckedHoverDrawable = ninePatchDrawable("GUI/ui2/button2_selected_hover");


        final int TAB_BUTTON_PAD_LEFT   = 11;
        final int TAB_BUTTON_PAD_RIGHT  = 8;
        final int TAB_BUTTON_PAD_TOP    = 6;
        final int TAB_BUTTON_PAD_BOTTOM = 6;

        ImageTextButton.ImageTextButtonStyle uiTabButtonStyle = new ImageTextButton.ImageTextButtonStyle(uiTabButtonNormalDrawable, null, uiTabButtonCheckedDrawable, fontButtons);
        uiTabButtonStyle.over = uiTabButtonHoverDrawable;
        uiTabButtonStyle.checkedOver = uiTabButtonCheckedHoverDrawable;
        uiTabButtonStyle.checkedOffsetY = -0.5f;
        setPadding(uiTabButtonStyle.up,          TAB_BUTTON_PAD_LEFT, TAB_BUTTON_PAD_RIGHT, TAB_BUTTON_PAD_TOP, TAB_BUTTON_PAD_BOTTOM);
        setPadding(uiTabButtonStyle.checked,     TAB_BUTTON_PAD_LEFT, TAB_BUTTON_PAD_RIGHT, TAB_BUTTON_PAD_TOP, TAB_BUTTON_PAD_BOTTOM);
        setPadding(uiTabButtonStyle.over,        TAB_BUTTON_PAD_LEFT, TAB_BUTTON_PAD_RIGHT, TAB_BUTTON_PAD_TOP, TAB_BUTTON_PAD_BOTTOM);
        setPadding(uiTabButtonStyle.checkedOver, TAB_BUTTON_PAD_LEFT, TAB_BUTTON_PAD_RIGHT, TAB_BUTTON_PAD_TOP, TAB_BUTTON_PAD_BOTTOM);
        skin.add("button2-tab", uiTabButtonStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button2-tab", uiTabButtonStyle, Button.ButtonStyle.class);

        final NinePatchDrawable uiButtonRedNormal  = ninePatchDrawable("GUI/ui2/button2-red-normal");
        uiButtonRedNormal.setBottomHeight(ui2Button2NormalDrawable.getBottomHeight());
        uiButtonRedNormal.setTopHeight(ui2Button2NormalDrawable.getTopHeight());
        final NinePatchDrawable uiButtonRedHover   = ninePatchDrawable("GUI/ui2/button2-red-hover");
        uiButtonRedHover.setBottomHeight(ui2Button2HoverDrawable.getBottomHeight());
        uiButtonRedHover.setTopHeight(ui2Button2HoverDrawable.getTopHeight());
        final NinePatchDrawable uiButtonRedPressed = ninePatchDrawable("GUI/ui2/button2-red-pressed");
        uiButtonRedPressed.setBottomHeight(ui2Button2CheckedDrawable.getBottomHeight());
        uiButtonRedPressed.setTopHeight(ui2Button2CheckedDrawable.getTopHeight());

        ImageTextButton.ImageTextButtonStyle ui2ButtonRedStyle = new ImageTextButton.ImageTextButtonStyle(uiButtonRedNormal, uiButtonRedPressed, null, fontButtons);
        ui2ButtonRedStyle.over = uiButtonRedHover;
        skin.add("button-red", ui2ButtonRedStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-red", ui2ButtonRedStyle, Button.ButtonStyle.class);

        TextureRegionDrawable uiSettingsNormal = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/settings-tab1-normal"));
        TextureRegionDrawable uiSettingsHover = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/settings-tab1-hover"));
        TextureRegionDrawable uiSettingsPressed = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/settings-tab1-pressed"));

        ImageTextButton.ImageTextButtonStyle ui2ButtonSettingsStyle = new ImageTextButton.ImageTextButtonStyle(uiSettingsNormal, uiSettingsPressed, uiSettingsHover, fontButtons);
        ui2ButtonSettingsStyle.over = uiSettingsHover;
        skin.add("button-settings", ui2ButtonSettingsStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-settings", ui2ButtonSettingsStyle, Button.ButtonStyle.class);

        Color SELECTED_PAYMENT_MODE_COLOR = new Color(0.7f, 1f, 0.7f, 1);

        TextureRegionDrawable creditButtonPaypalNormal = new TextureRegionDrawable(atlas.createSprite("GUI/payment-methods/paypal1"));
        SpriteDrawable creditButtonPaypalPressed = (SpriteDrawable) skin.newDrawable(creditButtonPaypalNormal, SELECTED_PAYMENT_MODE_COLOR);
        SpriteDrawable creditButtonPaypalDown = (SpriteDrawable) skin.newDrawable(creditButtonPaypalNormal, Color.LIGHT_GRAY);
        ImageTextButton.ImageTextButtonStyle creditButtonPaypalStyle = new ImageTextButton.ImageTextButtonStyle(creditButtonPaypalNormal, creditButtonPaypalDown,creditButtonPaypalPressed, fontButtons);
        skin.add("credit-button-paypal", creditButtonPaypalStyle, Button.ButtonStyle.class);
        skin.add("credit-button-paypal", creditButtonPaypalStyle, ImageTextButton.ImageTextButtonStyle.class);

        TextureRegionDrawable creditButtonPlaystoreNormal = new TextureRegionDrawable(atlas.createSprite("GUI/payment-methods/google-pay1"));
        SpriteDrawable creditButtonPlaystorePressed = (SpriteDrawable) skin.newDrawable(creditButtonPlaystoreNormal,SELECTED_PAYMENT_MODE_COLOR);
        SpriteDrawable creditButtonPlaystoreDown = (SpriteDrawable) skin.newDrawable(creditButtonPlaystoreNormal, Color.LIGHT_GRAY);
        ImageTextButton.ImageTextButtonStyle creditButtonPlaystoreStyle = new ImageTextButton.ImageTextButtonStyle(creditButtonPlaystoreNormal, creditButtonPlaystoreDown,creditButtonPlaystorePressed, fontButtons);
        skin.add("credit-button-playstore", creditButtonPlaystoreStyle, Button.ButtonStyle.class);
        skin.add("credit-button-playstore", creditButtonPlaystoreStyle, ImageTextButton.ImageTextButtonStyle.class);

        TextureRegionDrawable creditButtonAppstoreNormal = new TextureRegionDrawable(atlas.createSprite("GUI/payment-methods/apple-pay1"));
        SpriteDrawable creditButtonAppstorePressed = (SpriteDrawable) skin.newDrawable(creditButtonAppstoreNormal,SELECTED_PAYMENT_MODE_COLOR);
        SpriteDrawable creditButtonAppstoreDown = (SpriteDrawable) skin.newDrawable(creditButtonAppstoreNormal, Color.LIGHT_GRAY);
        ImageTextButton.ImageTextButtonStyle creditButtonAppstoreStyle = new ImageTextButton.ImageTextButtonStyle(creditButtonAppstoreNormal, creditButtonAppstoreDown,creditButtonAppstorePressed, fontButtons);
        skin.add("credit-button-appstore", creditButtonAppstoreStyle, Button.ButtonStyle.class);
        skin.add("credit-button-appstore", creditButtonAppstoreStyle, ImageTextButton.ImageTextButtonStyle.class);

        TextureRegionDrawable creditButtonSteamNormal = new TextureRegionDrawable(atlas.createSprite("GUI/payment-methods/steam1"));
        SpriteDrawable creditButtonSteamPressed = (SpriteDrawable) skin.newDrawable(creditButtonSteamNormal,SELECTED_PAYMENT_MODE_COLOR);
        SpriteDrawable creditButtonSteamDown = (SpriteDrawable) skin.newDrawable(creditButtonSteamNormal, Color.LIGHT_GRAY);
        ImageTextButton.ImageTextButtonStyle creditButtonSteamStyle = new ImageTextButton.ImageTextButtonStyle(creditButtonSteamNormal, creditButtonSteamDown,creditButtonSteamPressed, fontButtons);
        skin.add("credit-button-steam", creditButtonSteamStyle, Button.ButtonStyle.class);
        skin.add("credit-button-steam", creditButtonSteamStyle, ImageTextButton.ImageTextButtonStyle.class);

        TextureRegionDrawable creditButtonDogeNormal = new TextureRegionDrawable(atlas.createSprite("GUI/payment-methods/dogecoin1"));
        SpriteDrawable creditButtonDogePressed = (SpriteDrawable) skin.newDrawable(creditButtonDogeNormal,SELECTED_PAYMENT_MODE_COLOR);
        SpriteDrawable creditButtonDogeDown = (SpriteDrawable) skin.newDrawable(creditButtonDogeNormal, Color.LIGHT_GRAY);
        ImageTextButton.ImageTextButtonStyle creditButtonDogeStyle = new ImageTextButton.ImageTextButtonStyle(creditButtonDogeNormal, creditButtonDogeDown,creditButtonDogePressed, fontButtons);
        skin.add("credit-button-doge", creditButtonDogeStyle, Button.ButtonStyle.class);
        skin.add("credit-button-doge", creditButtonDogeStyle, ImageTextButton.ImageTextButtonStyle.class);

        TextureRegionDrawable creditButtonAmazonNormal = new TextureRegionDrawable(atlas.createSprite("GUI/payment-methods/dogecoin1"));
        SpriteDrawable creditButtonAmazonPressed = (SpriteDrawable) skin.newDrawable(creditButtonAmazonNormal,SELECTED_PAYMENT_MODE_COLOR);
        SpriteDrawable creditButtonAmazonDown = (SpriteDrawable) skin.newDrawable(creditButtonAmazonNormal, Color.LIGHT_GRAY);
        ImageTextButton.ImageTextButtonStyle creditButtonAmazonStyle = new ImageTextButton.ImageTextButtonStyle(creditButtonAmazonNormal, creditButtonAmazonDown,creditButtonAmazonPressed, fontButtons);
        skin.add("credit-button-amazon", creditButtonAmazonStyle, Button.ButtonStyle.class);
        skin.add("credit-button-amazon", creditButtonAmazonStyle, ImageTextButton.ImageTextButtonStyle.class);

        TextureRegionDrawable creditButtonAppleNormal = new TextureRegionDrawable(atlas.createSprite("GUI/payment-methods/apple-pay1"));
        SpriteDrawable creditButtonApplePressed = (SpriteDrawable) skin.newDrawable(creditButtonAppleNormal,SELECTED_PAYMENT_MODE_COLOR);
        SpriteDrawable creditButtonAppleDown = (SpriteDrawable) skin.newDrawable(creditButtonAppleNormal, Color.LIGHT_GRAY);
        ImageTextButton.ImageTextButtonStyle creditButtonAppleStyle = new ImageTextButton.ImageTextButtonStyle(creditButtonAppleNormal, creditButtonAppleDown,creditButtonApplePressed, fontButtons);
        skin.add("credit-button-apple", creditButtonAppleStyle, Button.ButtonStyle.class);
        skin.add("credit-button-apple", creditButtonAppleStyle, ImageTextButton.ImageTextButtonStyle.class);


        final NinePatchDrawable uiTabButton2Inactive = ninePatchDrawable("GUI/ui2/tab_button2_inactive");
        final NinePatchDrawable uiTabButton2Hover = ninePatchDrawable("GUI/ui2/tab_button2_hovered");
        final NinePatchDrawable uiTabButton2Pressed = ninePatchDrawable("GUI/ui2/tab_button2_pressed");

        ImageTextButton.ImageTextButtonStyle ui2ButtonTabStyle = new ImageTextButton.ImageTextButtonStyle(uiTabButton2Inactive, uiTabButton2Pressed, null, fontButtons);
        ui2ButtonTabStyle.over = uiTabButton2Hover;
        ui2ButtonTabStyle.checked = uiTabButton2Pressed;
        skin.add("tab-button2", ui2ButtonTabStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("tab-button2", ui2ButtonTabStyle, Button.ButtonStyle.class);
        buttonStyleTabTop = ui2ButtonTabStyle;

        ImageTextButton.ImageTextButtonStyle ui2ButtonDisabledStyle = new ImageTextButton.ImageTextButtonStyle(ui2ButtonDisabledDrawable,ui2ButtonDisabledDrawable,null, fontButtons);
        skin.add("button-disabled",ui2ButtonDisabledStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-disabled", ui2ButtonDisabledStyle, Button.ButtonStyle.class);

        skin.add("button-minimap-map", createMapButtonStyle(fontButtons, "map1", "map1-hover", "map1-pressed"), ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-minimap-settings", createMapButtonStyle(fontButtons, "settings1", "settings1-hover", "settings1-pressed"), ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-minimap-zoom-in", createMapButtonStyle(fontButtons, "zoom-in1", "zoom-in1-hover", "zoom-in1-pressed"), ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-minimap-zoom-out", createMapButtonStyle(fontButtons, "zoom-out1", "zoom-out1-hover", "zoom-out1-pressed"), ImageTextButton.ImageTextButtonStyle.class);

        TextureRegionDrawable uiTreeItemUnopenedNormal = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/tree-item-category1-unopened-normal"));
        TextureRegionDrawable uiTreeItemUnopenedHover = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/tree-item-category1-unopened-hover"));
        TextureRegionDrawable uiTreeItemUnopenedPressed = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/tree-item-category1-unopened-pressed"));
        TextureRegionDrawable uiTreeItemOpenedNormal = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/tree-item-category1-opened-normal"));
        TextureRegionDrawable uiTreeItemOpenedHover = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/tree-item-category1-opened-hover"));
        TextureRegionDrawable uiTreeItemOpenedPressed = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/tree-item-category1-opened-pressed"));

        ImageTextButton.ImageTextButtonStyle uiTreeItemUnopenedStyle = new ImageTextButton.ImageTextButtonStyle(uiTreeItemUnopenedNormal, uiTreeItemUnopenedPressed, null, fontButtons);
        uiTreeItemUnopenedStyle.over = uiTreeItemUnopenedHover;
        uiTreeItemUnopenedStyle.fontColor = Color.valueOf("#89cdfe");
        skin.add("button-tree-item-unopened", uiTreeItemUnopenedStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-tree-item-unopened", uiTreeItemUnopenedStyle, Button.ButtonStyle.class);

        ImageTextButton.ImageTextButtonStyle uiTreeItemOpenedStyle = new ImageTextButton.ImageTextButtonStyle(uiTreeItemOpenedNormal, uiTreeItemOpenedPressed, null, fontButtons);
        uiTreeItemOpenedStyle.over = uiTreeItemOpenedHover;
        uiTreeItemOpenedStyle.fontColor = Color.valueOf("#89cdfe");
        skin.add("button-tree-item-opened", uiTreeItemOpenedStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-tree-item-opened", uiTreeItemOpenedStyle, Button.ButtonStyle.class);

        TextureRegionDrawable uiTreeLeafNormal = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/tree-item-leaf1"));
        TextureRegionDrawable uiTreeLeafPressed = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/tree-item-leaf1-selected"));

        ImageTextButton.ImageTextButtonStyle uiTreeLeafStyle = new ImageTextButton.ImageTextButtonStyle(uiTreeLeafNormal, uiTreeLeafPressed, null, fontButtons);
        uiTreeLeafStyle.checked = uiTreeLeafPressed;
        uiTreeLeafStyle.fontColor = Color.valueOf("#89cdfe");
        skin.add("button-tree-leaf", uiTreeLeafStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-tree-leaf", uiTreeLeafStyle, Button.ButtonStyle.class);


        NinePatchDrawable textFieldBackgroundDrawable = ninePatchDrawable("GUI/ui2/text_input1_background");
        RectShapeDrawable textFieldCursorDrawable = new RectShapeDrawable(Color.WHITE);
        textFieldCursorDrawable.setMinWidth(2);
        RectShapeDrawable textFieldSelectionDrawable = new RectShapeDrawable(Color.CYAN);
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle(fontElectrolizeSmall, Color.WHITE, textFieldCursorDrawable, textFieldSelectionDrawable, textFieldBackgroundDrawable);
        skin.add(RESOURCE_DEFAULT, textFieldStyle);

        //noinspection deprecation
        TextTooltip.TextTooltipStyle tooltipStyle = new GCTextTooltip.TextTooltipStyle(labelDefault, textFieldBackgroundDrawable);
        skin.add(RESOURCE_DEFAULT, tooltipStyle);

        NinePatchDrawable progressBarBackgroundDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_background"));
        NinePatchDrawable progressBarVerticalBackgroundDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_background_vertical"));
        NinePatchDrawable progressBarKnobDrawableBlue = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill"));
        NinePatchDrawable progressBarKnobDrawableGreen = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill_green"));
        NinePatchDrawable progressBarKnobDrawableRed = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill_red"));
        NinePatchDrawable progressVerticalBarKnobDrawableBlue = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill"));
        NinePatchDrawable progressVerticalBarKnobDrawableGreen = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill_green"));
        NinePatchDrawable progressVerticalBarKnobDrawableRed = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill_red"));
        float horPadding = - (progressBarBackgroundDrawable.getLeftWidth() + progressBarBackgroundDrawable.getRightWidth());
        progressBarKnobDrawableBlue.setMinWidth(horPadding);
        progressBarKnobDrawableGreen.setMinWidth(horPadding);
        progressBarKnobDrawableRed.setMinWidth(horPadding);
        float verPadding = - (progressBarVerticalBackgroundDrawable.getBottomHeight() + progressBarVerticalBackgroundDrawable.getTopHeight());
        progressVerticalBarKnobDrawableBlue.setMinHeight(verPadding);
        progressVerticalBarKnobDrawableGreen.setMinHeight(verPadding);
        progressVerticalBarKnobDrawableRed.setMinHeight(verPadding);

        ProgressBar.ProgressBarStyle progressBarStyle = new ProgressBar.ProgressBarStyle(progressBarBackgroundDrawable, null);
        progressBarStyle.knobBefore = progressBarKnobDrawableBlue;
        skin.add(RESOURCE_DEFAULT_HORIZONTAL, progressBarStyle);
        progressBarStyle = new ProgressBar.ProgressBarStyle(progressBarVerticalBackgroundDrawable, null);
        progressBarStyle.knobBefore = progressVerticalBarKnobDrawableBlue;
        skin.add(RESOURCE_DEFAULT_VERTICAL, progressBarStyle);//TODO

        progressBarStyle = new ProgressBar.ProgressBarStyle(progressBarBackgroundDrawable, null);
        progressBarStyle.knobBefore = progressBarKnobDrawableRed;
        skin.add(RESOURCE_DEFAULT_HORIZONTAL + "-red", progressBarStyle);
        progressBarStyle = new ProgressBar.ProgressBarStyle(progressBarVerticalBackgroundDrawable, null);
        progressBarStyle.knobBefore = progressVerticalBarKnobDrawableRed;
        skin.add(RESOURCE_DEFAULT_VERTICAL + "-red", progressBarStyle);//TODO

        progressBarStyle = new ProgressBar.ProgressBarStyle(progressBarBackgroundDrawable, null);
        progressBarStyle.knobBefore = progressBarKnobDrawableGreen;
        skin.add(RESOURCE_DEFAULT_HORIZONTAL + "-green", progressBarStyle);
        progressBarStyle = new ProgressBar.ProgressBarStyle(progressBarVerticalBackgroundDrawable, null);
        progressBarStyle.knobBefore = progressVerticalBarKnobDrawableGreen;
        skin.add(RESOURCE_DEFAULT_VERTICAL + "-green", progressBarStyle);//TODO

        NinePatchDrawable gaugeKnobDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill_grey"));
        gaugeKnobDrawable.setMinWidth(horPadding);

        ProgressBar.ProgressBarStyle gaugeStyle = new ProgressBar.ProgressBarStyle(progressBarBackgroundDrawable, null);
        gaugeStyle.knobBefore = gaugeKnobDrawable;
        skin.add("gauge-horizontal", gaugeStyle);
        skin.add("gauge-vertical", gaugeStyle);//TODO


        TextureRegionDrawable barBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/im/gauge-lowlight-type1"));
        TextureRegionDrawable healthBarDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/im/hull-gauge-highlight"), false, false);
        healthBarDrawable.setMinWidth(0);
        TextureRegionDrawable shieldBarDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/im/shield-gauge-highlight"), true, false);
        shieldBarDrawable.setMinWidth(0);
        TextureRegionDrawable energyBarDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/im/energy-gauge-highlight"), true, false);
        energyBarDrawable.setMinWidth(0);
        TextureRegionDrawable heatBarDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/im/heat-gauge-highlight"), false, false);
        heatBarDrawable.setMinWidth(0);

        ProgressBar.ProgressBarStyle healthBarStyle = new ProgressBar.ProgressBarStyle(barBackgroundDrawable, null);
        healthBarStyle.knobBefore = healthBarDrawable;
        skin.add("health-bar", healthBarStyle);

        ProgressBar.ProgressBarStyle shieldBarStyle = new ProgressBar.ProgressBarStyle(barBackgroundDrawable, null);
        shieldBarStyle.knobBefore = shieldBarDrawable;
        skin.add("shield-bar", shieldBarStyle);

        ProgressBar.ProgressBarStyle energyBarStyle = new ProgressBar.ProgressBarStyle(barBackgroundDrawable, null);
        energyBarStyle.knobBefore = energyBarDrawable;
        skin.add("energy-bar", energyBarStyle);

        ProgressBar.ProgressBarStyle heatBarStyle = new ProgressBar.ProgressBarStyle(barBackgroundDrawable, null);
        heatBarStyle.knobBefore = heatBarDrawable;
        skin.add("heat-bar", heatBarStyle);

        /* Gauges for GCActionBar.java */
        BaseDrawable baseDrawable = new BaseDrawable();
        baseDrawable.setMinWidth(49);
        baseDrawable.setMinHeight(113);

        /*All left gauges*/
        for(GCActionBar.GaugeColor c : GCActionBar.GaugeColor.values())
        {
            String colour = c.toString().toLowerCase();
            TextureRegionDrawable leftGaugeDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/ui2/action-bar-gauge-left-" + colour), false, true);
            leftGaugeDrawable.setMinHeight(0);

            ProgressBar.ProgressBarStyle leftGauge = new ProgressBar.ProgressBarStyle(baseDrawable, null);
            leftGauge.knobBefore = leftGaugeDrawable;
            skin.add("left-gauge-" + colour, leftGauge);
        }

        for(GCActionBar.GaugeColor c : GCActionBar.GaugeColor.values())
        {
            String colour = c.toString().toLowerCase();
            TextureRegionDrawable RightGaugeDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/ui2/action-bar-gauge-right-"+colour), false, true);
            RightGaugeDrawable.setMinHeight(0);

            ProgressBar.ProgressBarStyle rightGauge = new ProgressBar.ProgressBarStyle(baseDrawable, null);
            rightGauge.knobBefore = RightGaugeDrawable;
            skin.add("right-gauge-" + colour, rightGauge);

        }

        /*Small gauges*/
        for(GCActionBar.GaugeColor c : GCActionBar.GaugeColor.values())
        {
            String colour = c.toString().toLowerCase();
            TextureRegionDrawable leftGaugeDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/ui2/small-gauges/action_bar_gauge_left_" + colour + "_small"), false, true);
            leftGaugeDrawable.setMinHeight(0);

            ProgressBar.ProgressBarStyle leftGauge = new ProgressBar.ProgressBarStyle(baseDrawable, null);
            leftGauge.knobBefore = leftGaugeDrawable;
            skin.add("left-gauge-" + colour + "-small", leftGauge);
        }

        for(GCActionBar.GaugeColor c : GCActionBar.GaugeColor.values())
        {
            String colour = c.toString().toLowerCase();
            TextureRegionDrawable rightGaugeDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/ui2/small-gauges/action_bar_gauge_right_" + colour + "_small"), false, true);
            rightGaugeDrawable.setMinHeight(0);

            ProgressBar.ProgressBarStyle rightGauge = new ProgressBar.ProgressBarStyle(baseDrawable, null);
            rightGauge.knobBefore = rightGaugeDrawable;
            skin.add("right-gauge-" + colour + "-small", rightGauge);

        }


        NinePatchDrawable sliderBackgroundDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/slider1_background"));
        TextureRegionDrawable sliderKnobDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/slider1-button-normal"));
        TextureRegionDrawable sliderKnobOverDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/slider1-button-hovered"));
        NinePatchDrawable sliderKnobBeforeDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/slider1_fill"));
        sliderKnobBeforeDrawable.setMinWidth(0);
        sliderBackgroundDrawable.setMinWidth(sliderBackgroundDrawable.getPatch().getLeftWidth() + sliderBackgroundDrawable.getPatch().getRightWidth());


        Slider.SliderStyle sliderStyle = new Slider.SliderStyle();
        sliderStyle.background = sliderBackgroundDrawable;
        sliderStyle.knob = sliderKnobDrawable;
        sliderStyle.knob = sliderKnobOverDrawable;
        sliderStyle.knobBefore = sliderKnobBeforeDrawable;
        skin.add(RESOURCE_DEFAULT_HORIZONTAL, sliderStyle);
        skin.add(RESOURCE_DEFAULT_VERTICAL, sliderStyle);

        ImageButton.ImageButtonStyle closeButtonStyle = new ImageButton.ImageButtonStyle();
        TextureRegion btnCloseNormalTexture = atlas.createSprite("GUI/ui2/window-menu2-button-close-normal");
        TextureRegion btnCloseHoverTexture = atlas.createSprite("GUI/ui2/window-menu2-button-close-hovered");
        TextureRegion btnClosePressedTexture = atlas.createSprite("GUI/ui2/window-menu2-button-close-pressed");

        closeButtonStyle.imageUp = new TextureRegionDrawable(btnCloseNormalTexture);
        closeButtonStyle.imageDown = new TextureRegionDrawable(btnClosePressedTexture);
        closeButtonStyle.imageOver = new TextureRegionDrawable(btnCloseHoverTexture);
        skin.add("btn-close-window", closeButtonStyle);

        ImageButton.ImageButtonStyle chatButtonStyle = new ImageButton.ImageButtonStyle();
        chatButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-chat1-normal"));
        chatButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-chat1-pressed"));
        chatButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-chat1-hover"));
        skin.add("btn-chat", chatButtonStyle);

        ImageButton.ImageButtonStyle questButtonStyle = new ImageButton.ImageButtonStyle();
        questButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-quest1-normal"));
        questButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-quest1-pressed"));
        questButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-quest1-hover"));
        skin.add("btn-quest", questButtonStyle);

        ImageButton.ImageButtonStyle notificationButtonStyle = new ImageButton.ImageButtonStyle();
        notificationButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-notification1-normal"));
        notificationButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-notification1-pressed"));
        notificationButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-notification1-hover"));
        skin.add("btn-notification", notificationButtonStyle);

        ImageButton.ImageButtonStyle switchButtonStyle = new ImageButton.ImageButtonStyle();
        switchButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-characters1-normal"));
        switchButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-characters1-pressed"));
        switchButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-characters1-hover"));
        skin.add("btn-switch-char", switchButtonStyle);

        ImageButton.ImageButtonStyle AIButtonStyle = new ImageButton.ImageButtonStyle();
        AIButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-ai1-normal"));
        AIButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-ai1-pressed"));
        AIButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-ai1-hover"));
        skin.add("btn-ai", AIButtonStyle);

        ImageButton.ImageButtonStyle diceButtonStyle = new ImageButton.ImageButtonStyle();
        diceButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-dice1-normal"));
        diceButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-dice1-pressed"));
        diceButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-dice1-hover"));
        skin.add("btn-random", diceButtonStyle);

        ImageButton.ImageButtonStyle gaugesSidebarButton = new ImageButton.ImageButtonStyle();
        gaugesSidebarButton.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-graph1-normal"));
        gaugesSidebarButton.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-graph1-pressed"));
        gaugesSidebarButton.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-graph1-hover"));
        skin.add("btn-gauge-sidebar", gaugesSidebarButton);

        ImageButton.ImageButtonStyle ellipsisButtonStyle = new ImageButton.ImageButtonStyle();
        ellipsisButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-ellipsis-normal"));
        ellipsisButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-ellipsis-pressed"));
        ellipsisButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-ellipsis-hover"));
        skin.add("btn-ellipsis", ellipsisButtonStyle);

        ImageButton.ImageButtonStyle arrowDownButtonStyle = new ImageButton.ImageButtonStyle();
        arrowDownButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-down1-normal"));
        arrowDownButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-down1-pressed"));
        arrowDownButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-down1-hover"));
        skin.add("btn-arrow-down", arrowDownButtonStyle);

        ImageButton.ImageButtonStyle arrowUpButtonStyle = new ImageButton.ImageButtonStyle();
        arrowUpButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-up1-normal"));
        arrowUpButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-up1-pressed"));
        arrowUpButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-up1-hover"));
        skin.add("btn-arrow-up", arrowUpButtonStyle);

        ImageButton.ImageButtonStyle arrowLeftButtonStyle = new ImageButton.ImageButtonStyle();
        arrowLeftButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-left1-normal"));
        arrowLeftButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-left1-pressed"));
        arrowLeftButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-left1-hover"));
        skin.add("btn-arrow-left", arrowLeftButtonStyle);

        ImageButton.ImageButtonStyle arrowRightButtonStyle = new ImageButton.ImageButtonStyle();
        arrowRightButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-right1-normal"));
        arrowRightButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-right1-pressed"));
        arrowRightButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-arrow-right1-hover"));
        skin.add("btn-arrow-right", arrowRightButtonStyle);

        ImageButton.ImageButtonStyle arrowFullscreenStyle = new ImageButton.ImageButtonStyle();
        arrowFullscreenStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-fullscreen1-normal"));
        arrowFullscreenStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-fullscreen1-pressed"));
        arrowFullscreenStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-fullscreen1-hover"));
        skin.add("btn-fullscreen", arrowFullscreenStyle);

        ImageButton.ImageButtonStyle refreshButtonStyle = new ImageButton.ImageButtonStyle();
        refreshButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-refresh-button1-normal"));
        refreshButtonStyle.imageDown = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-refresh-button1-pressed"));
        refreshButtonStyle.imageOver = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-refresh-button1-hover"));
        skin.add("btn-refresh", refreshButtonStyle);

        createSwitchStyle(skin, font);

        TextureRegion checkOffTexture = atlas.createSprite("GUI/ui2/checkbox1-unchecked");
        TextureRegion checkOnTexture = atlas.createSprite("GUI/ui2/checkbox1-checked");
        buttonStyleCheckbox = new ImageTextButton.ImageTextButtonStyle();
        buttonStyleCheckbox.imageChecked = new TextureRegionDrawable(checkOnTexture);
        buttonStyleCheckbox.imageUp = new TextureRegionDrawable(checkOffTexture);
        buttonStyleCheckbox.font = font;
        skin.add("default-checkable", buttonStyleCheckbox, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("default-checkable", buttonStyleCheckbox, Button.ButtonStyle.class);


        NinePatchDrawable windowBackground = ninePatchDrawable("GUI/ui2/window2_background");
        NinePatchDrawable titleBarBackground = ninePatchDrawable("GUI/ui2/window_title_background1", false);
        skin.add("title-bar-background", titleBarBackground, Drawable.class);

        final Color titleFontColor = Color.valueOf("#e0b487");
        final GCWindow.GCWindowStyle windowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, windowBackground);
        windowStyleDefault = windowStyle;
        windowStyle.titleBackground = titleBarBackground;
        windowStyle.titleFontColor = titleFontColor;
        windowStyle.labelPadLeft = 30;
        windowStyle.labelPadBottom = 36;
        windowStyle.closeButtonPadRight = 1;
        windowStyle.closeButtonPadTop = -12;
        skin.add(RESOURCE_DEFAULT, windowStyle);
        skin.add(RESOURCE_DEFAULT, windowStyle, Window.WindowStyle.class);

        final NinePatchDrawable ui2WindowBackground = ninePatchDrawable("GUI/ui2/window_menu_background1");
        final GCWindow.GCWindowStyle ui2WindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, ui2WindowBackground);
        windowStyleMainMenu =ui2WindowStyle;
        ui2WindowStyle.labelPadLeft = 10;
        ui2WindowStyle.labelPadBottom = 24;
        ui2WindowStyle.titleFontColor = titleFontColor;
        skin.add("ui2-window", ui2WindowStyle);
        skin.add("ui2-window", ui2WindowStyle, Window.WindowStyle.class);

        final NinePatchDrawable inspectorWindowBackground = ninePatchDrawable("GUI/ui2/window3_background");
        final GCWindow.GCWindowStyle inspectorWindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, inspectorWindowBackground);
        windowStyleInspector = inspectorWindowStyle;
        inspectorWindowStyle.closeButtonPadRight = -10;
        inspectorWindowStyle.closeButtonPadTop = 10;
        inspectorWindowStyle.titleFontColor = titleFontColor;
        skin.add("inspector-window", inspectorWindowStyle);
        skin.add("inspector-window", inspectorWindowStyle, Window.WindowStyle.class);

        final NinePatchDrawable popupStyle3Background = ninePatchDrawable("GUI/ui2/window3_background");
        windowStylePopup2 = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, popupStyle3Background);
        windowStylePopup2.titleBackground = titleBarBackground;
        windowStylePopup2.titleFontColor = titleFontColor;
        windowStylePopup2.closeButtonPadRight = -10;
        windowStylePopup2.closeButtonPadTop = 10;
        skin.add("popup-window2", windowStylePopup2);
        skin.add("popup-window2", windowStylePopup2, Window.WindowStyle.class);

        final NinePatchDrawable popupStyle2Background = ninePatchDrawable("GUI/ui2/window5_background");
        final GCWindow.GCWindowStyle popupStyle2WindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, popupStyle2Background);
        windowStylePopup = popupStyle2WindowStyle;
        popupStyle2WindowStyle.titleBackground = titleBarBackground;
        popupStyle2WindowStyle.titleFontColor = titleFontColor;
        popupStyle2WindowStyle.closeButtonPadRight = -10;
        popupStyle2WindowStyle.closeButtonPadTop = 10;
        skin.add("popup-window", popupStyle2WindowStyle);
        skin.add("popup-window", popupStyle2WindowStyle, Window.WindowStyle.class);

        final NinePatchDrawable tutorialTooltipBackground = ninePatchDrawable("GUI/ui2/tutorial_popup_background1");
        final GCWindow.GCWindowStyle tutorialTooltipStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, tutorialTooltipBackground);
        windowStyleTooltip = tutorialTooltipStyle;
        tutorialTooltipStyle.closeButtonPadRight = -10;
        tutorialTooltipStyle.closeButtonPadTop = 10;
        tutorialTooltipStyle.titleFontColor = titleFontColor;
        skin.add("tutorial-tooltip", tutorialTooltipStyle);
        skin.add("tutorial-tooltip", tutorialTooltipStyle, Window.WindowStyle.class);

        final TextureRegionDrawable tutorialMonologueCharacter = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/self-icon1"));
        skin.add("tutorial-character", tutorialMonologueCharacter, Drawable.class);

        final NinePatchDrawable loginScreenBackground = ninePatchDrawable("GUI/ui2/loginpanel1");
        final GCWindow.GCWindowStyle loginScreenStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, loginScreenBackground);
        windowStyleLoginDialog = loginScreenStyle;


        skin.add("login-window", loginScreenStyle);
        skin.add("login-window", loginScreenStyle, Window.WindowStyle.class);

        final NinePatchDrawable ui2InventionWindowBackground = new NinePatchDrawable(atlas.createPatch("GUI/ui2/window4_background"));
//        setupMinSize(ui2InventionWindowBackground);
        ui2InventionWindowBackground.setMinHeight(ui2InventionWindowBackground.getPatch().getTopHeight() + ui2InventionWindowBackground.getPatch().getBottomHeight());
        final GCWindow.GCWindowStyle ui2InventionWindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, ui2InventionWindowBackground);
        windowStyleInvention = ui2InventionWindowStyle;
        ui2InventionWindowStyle.closeButtonPadTop = 11;
        ui2InventionWindowStyle.closeButtonPadRight = -32;
        skin.add("ui2-invention-window", ui2InventionWindowStyle);
        skin.add("ui2-invention-window", ui2InventionWindowStyle, Window.WindowStyle.class);

        final NinePatchDrawable ui2SettingsWindowBackground = new NinePatchDrawable(atlas.createPatch("GUI/ui2/settings_pane_background1"));
        ui2SettingsWindowBackground.setMinHeight(ui2SettingsWindowBackground.getPatch().getTopHeight() + ui2SettingsWindowBackground.getPatch().getBottomHeight());
        final GCWindow.GCWindowStyle ui2SettingsWindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, ui2SettingsWindowBackground);
        windowStyleSettings = ui2SettingsWindowStyle;
        ui2InventionWindowStyle.closeButtonPadTop = 11;
        ui2InventionWindowStyle.closeButtonPadRight = -32;
        skin.add("ui2-settings-window", ui2SettingsWindowStyle);
        skin.add("ui2-settings-window", ui2SettingsWindowStyle, Window.WindowStyle.class);

        NinePatchDrawable settingsBackground = new NinePatchDrawable(atlas.createPatch("GUI/ui2/settings_pane_background1"));
        skin.add("settings-background", settingsBackground, Drawable.class);

        final NinePatchDrawable currencyWindowBackground = new NinePatchDrawable(atlas.createPatch("GUI/ui2/settings_pane_background1"));
        currencyWindowBackground.setMinHeight(currencyWindowBackground.getPatch().getTopHeight() + currencyWindowBackground.getPatch().getBottomHeight());
        final GCWindow.GCWindowStyle currencyWindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, currencyWindowBackground);
        currencyWindowStyle.closeButtonPadTop = 11;
        currencyWindowStyle.closeButtonPadRight = -32;
        skin.add("currency-window", currencyWindowStyle);
        skin.add("currency-window", currencyWindowStyle, Window.WindowStyle.class);

        panelStyleBlueCornersSemiTransparent = new NinePatchDrawable(atlas.createPatch("GUI/ui2/window3_background"));
        skin.add("currency-background", panelStyleBlueCornersSemiTransparent, Drawable.class);

        panelStyleYellowTopAndBottomBorderSemiTransparent = new NinePatchDrawable(atlas.createPatch("GUI/ui2/internal_panel5_background"));

        skin.add("yellow-border", panelStyleYellowTopAndBottomBorderSemiTransparent, Drawable.class);

        TextureRegion settingsGraphicsImage = atlas.createSprite("invention/tab-build-building");
        skin.add("settings-graphics-image", settingsGraphicsImage, Sprite.class);

        TextureRegion settingsAudioImage = atlas.createSprite("invention/tab-build-building");
        skin.add("settings-audio-image", settingsAudioImage, Sprite.class);

        TextureRegion settingsControlsImage = atlas.createSprite("invention/tab-build-building");
        skin.add("settings-controls-image", settingsControlsImage, Sprite.class);

        TextureRegion settingsNetworkImage = atlas.createSprite("invention/tab-build-building");
        skin.add("settings-network-image", settingsNetworkImage, Sprite.class);

        TextureRegion settingsGeneralImage = atlas.createSprite("invention/tab-build-building");
        skin.add("settings-general-image", settingsGeneralImage, Sprite.class);

        TextureRegionDrawable selectionMenuBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-background-gradient"));

        final Window.WindowStyle selectionMenuWindowStyle = new Window.WindowStyle(fontTitle, Color.WHITE, new ScaledDrawable<>(selectionMenuBackgroundDrawable, 1.4f, 1.8f));
//        selectionMenuWindowStyle.titleFontColor = titleFontColor;
        skin.add("selection-menu-window", selectionMenuWindowStyle);

        final TextureRegionDrawable selectionMenuCloseBtnDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-close"));
        final TextureRegionDrawable selectionMenuUpBtnDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-up-arrow"));

        final ImageButton.ImageButtonStyle selectionMenuCloseBtnStyle = createImageButtonStyle(selectionMenuCloseBtnDrawable, 1.1f, 1f);
        final ImageButton.ImageButtonStyle selectionMenuUpBtnStyle = createImageButtonStyle(selectionMenuUpBtnDrawable, 1.1f, 1f);
        skin.add("selection-menu-close-btn", selectionMenuCloseBtnStyle);
        skin.add("selection-menu-up-btn", selectionMenuUpBtnStyle);

        final TextureRegionDrawable selectionMenuArrowDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-selector-arrow"));
        skin.add("selection-menu-arrow", selectionMenuArrowDrawable, Drawable.class);
        final TextureRegionDrawable selectionMenuSubmenuArrowDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-submenu-arrow"));
        skin.add("selection-menu-submenu-arrow", selectionMenuSubmenuArrowDrawable, Drawable.class);


        final TextureRegionDrawable inventionButton = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-bar-buttons/invention1"));
        skin.add("invention-button", inventionButton, Drawable.class);
        final TextureRegionDrawable experimentTab = new TextureRegionDrawable(atlas.createSprite("invention/tab-experiments"));
        skin.add("experiments-tab", experimentTab, Drawable.class);
        final TextureRegionDrawable ideasTab = new TextureRegionDrawable(atlas.createSprite("invention/tab-ideas"));
        skin.add("ideas-tab", ideasTab, Drawable.class);
        final TextureRegionDrawable wreckedHarborShip = new TextureRegionDrawable(atlas.createSprite("ships/oldworld/old-ship-wreck1"));
        skin.add("wrecked-ship", wreckedHarborShip, Drawable.class);
        final TextureRegionDrawable equipmentButton = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-bar-buttons/ship-equipment1"));
        skin.add("equipment-icon", equipmentButton, Drawable.class);
        final TextureRegionDrawable hullRepairKit = new TextureRegionDrawable(atlas.createSprite("icons/hull-repair-kit1"));
        skin.add("hull-repair-kit", hullRepairKit, Drawable.class);
        final TextureRegionDrawable smallWreck = new TextureRegionDrawable(atlas.createSprite("wrecks/small-wreck1"));
        skin.add("small-wreck", smallWreck, Drawable.class);
        final TextureRegionDrawable solarArray = new TextureRegionDrawable(atlas.createSprite("icons/Solar Array_icon"));
        skin.add("solar-array", solarArray, Drawable.class);
        final TextureRegionDrawable constructItemTab = new TextureRegionDrawable(atlas.createSprite("invention/tab-build-item"));
        skin.add("construct-item-tab", constructItemTab, Drawable.class);
        final TextureRegionDrawable gasCanister = new TextureRegionDrawable(atlas.createSprite("icons/tank1"));
        skin.add("gas-canister", gasCanister, Drawable.class);
        final TextureRegionDrawable crowbar = new TextureRegionDrawable(atlas.createSprite("icons/crowbar2"));
        skin.add("crowbar", crowbar, Drawable.class);
        final TextureRegionDrawable spacesuit = new TextureRegionDrawable(atlas.createSprite("suits/standard1-normal"));
        skin.add("spacesuit", spacesuit, Drawable.class);
        final TextureRegionDrawable anchorButtonOff = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-bar-buttons/anchor1-inactive"));
        final TextureRegionDrawable anchorButtonOn = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/button-bar-buttons/anchor1-active"));
        skin.add("anchor-button-off", anchorButtonOff, Drawable.class);
        skin.add("anchor-button-on", anchorButtonOn, Drawable.class);
        final TextureRegionDrawable lifeSupport = new TextureRegionDrawable(atlas.createSprite("icons/life-support1"));
        skin.add("life-support", lifeSupport, Drawable.class);
        final TextureRegionDrawable escapePod = new TextureRegionDrawable(atlas.createSprite("ships/escape1/escape-pod1"));
        skin.add("escape-pod", escapePod, Drawable.class);
        final TextureRegionDrawable leaveAction = new TextureRegionDrawable(atlas.createSprite("aspect-action-icons/exit-ship1"));
        skin.add("exit-action", leaveAction, Drawable.class);
        final TextureRegionDrawable enterAction = new TextureRegionDrawable(atlas.createSprite("aspect-action-icons/enter-ship1"));
        skin.add("enter-action", enterAction, Drawable.class);
        final TextureRegionDrawable selectAction = new TextureRegionDrawable(atlas.createSprite("aspect-action-icons/selector1"));
        skin.add("select-action", selectAction, Drawable.class);

        H5ESpriteType actionProgressBarBackground = rm.getSpriteType("images/cloudWhite.png");
        final Color color = new Color(Color.WHITE);
        color.a = 0.3f;
        TextureRegionDrawable actionProgressBarBackgroundDrawable = new TextureRegionDrawable(actionProgressBarBackground.getGraphicData()) {
            @Override
            public void draw(Batch batch, float x, float y, float width, float height) {
                Color oldColor = batch.getColor();
                batch.setColor(color);
                super.draw(batch, x, y, width, height);
                batch.setColor(oldColor);
            }

            @Override
            public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
                Color oldColor = batch.getColor();
                batch.setColor(color);
                super.draw(batch, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
                batch.setColor(oldColor);
            }
        };
        final Texture cloudTexture = actionProgressBarBackground.getGraphicData().getTexture();
        cloudTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        actionProgressBarBackgroundDrawable.setMinWidth(200);
        actionProgressBarBackgroundDrawable.setMinHeight(200);

        Window.WindowStyle actionProgressBarWindowStyle = new Window.WindowStyle(font, Color.WHITE, actionProgressBarBackgroundDrawable);
        skin.add("action-progress-bar-window", actionProgressBarWindowStyle);

        final GCWindow.GCWindowStyle GCactionProgressBarWindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, actionProgressBarBackgroundDrawable);
        windowStyleWhiteRadialGradient = GCactionProgressBarWindowStyle;
        skin.add("action-progress-bar-window", GCactionProgressBarWindowStyle, Window.WindowStyle.class);

        windowStyleClear = new GCWindow.GCWindowStyle();
        windowStyleClear.titleFont = font;
        windowStyleClear.titleFontColor = Color.WHITE;
        windowStyleClear.background = new BaseDrawable();
        skin.add("clear-window", windowStyleClear, Window.WindowStyle.class);

        final GCWindow.GCWindowStyle gcClearWindowStyle = new GCWindow.GCWindowStyle();
        gcClearWindowStyle.titleFont = font;
        gcClearWindowStyle.titleFontColor = Color.WHITE;
        gcClearWindowStyle.background = new BaseDrawable();

        skin.add("clear-window", gcClearWindowStyle, Window.WindowStyle.class);

        final TextureRegionDrawable actionBar = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/action-bar1"));
        Window.WindowStyle actionBarWindowStyle = new Window.WindowStyle(font, Color.WHITE, actionBar);
        skin.add("action-bar", actionBarWindowStyle);

        final TextureRegionDrawable actionBarMobile = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/action-bar1-mobile"));
        Window.WindowStyle actionBarMobileWindowStyle = new Window.WindowStyle(font, Color.WHITE, actionBarMobile);
        skin.add("action-bar-mobile", actionBarMobileWindowStyle);

        NinePatchDrawable scrollBackgroundVertical = ninePatchDrawable("GUI/ui2/scrollbar1_backing");
        NinePatchDrawable scrollKnobVertical = ninePatchDrawable("GUI/ui2/scrollbar1_slider");
        ScrollPane.ScrollPaneStyle scrollPaneStyle = new ScrollPane.ScrollPaneStyle();
        scrollPaneStyle.vScroll = scrollBackgroundVertical;
        scrollPaneStyle.vScrollKnob = scrollKnobVertical;
        skin.add(RESOURCE_DEFAULT, scrollPaneStyle);

        panelStyleBlueTopAndBottomBorderOpaque = ninePatchDrawable("GUI/ui2/internal_panel4_background");
        scrollPaneStyleBlueTopAndBottomBorderOpaque = new ScrollPane.ScrollPaneStyle(scrollPaneStyle);
        scrollPaneStyleBlueTopAndBottomBorderOpaque.background = panelStyleBlueTopAndBottomBorderOpaque;
        skin.add("scrollpane-backing-1", scrollPaneStyleBlueTopAndBottomBorderOpaque);

        scrollPaneStyleBlueGridOpaque = new ScrollPane.ScrollPaneStyle(scrollPaneStyle);
        scrollPaneStyleBlueGridOpaque.background = ninePatchDrawable("GUI/ui2/internal_panel1_background");
        skin.add("scrollpane-backing-2", scrollPaneStyleBlueGridOpaque);
        skin.add("scrollpane-backing-2-drawable", ninePatchDrawable("GUI/ui2/internal_panel1_background"), Drawable.class);

        scrollPaneStyleYellowTopAndBottomBorderOpaque = new ScrollPane.ScrollPaneStyle(scrollPaneStyle);
        scrollPaneStyleYellowTopAndBottomBorderOpaque.background = ninePatchDrawable("GUI/ui2/internal_panel5_background");
        skin.add("scrollpane-backing-3", scrollPaneStyleYellowTopAndBottomBorderOpaque);

        scrollPaneStyleSimpleSemiTransparent = new ScrollPane.ScrollPaneStyle(scrollPaneStyle);
        panelStyleSimpleSemiTransparent = ninePatchDrawable("GUI/ui2/purchase-dialog/list_backing1");
        scrollPaneStyleSimpleSemiTransparent.background = panelStyleSimpleSemiTransparent;
        skin.add("purchase-list-backing-1", scrollPaneStyleSimpleSemiTransparent);

        scrollPaneStyleBlueCornersSemiTransparent = new ScrollPane.ScrollPaneStyle(scrollPaneStyle);
        scrollPaneStyleBlueCornersSemiTransparent.background = ninePatchDrawable("GUI/ui2/window3_background");
        skin.add("scrollpane-backing-4", scrollPaneStyleBlueCornersSemiTransparent);

        NinePatchDrawable gcListItemNormal = ninePatchDrawable("GUI/ui2/internal_panel3_background");
        NinePatchDrawable gcListItemChecked = ninePatchDrawable("GUI/ui2/internal_panel3_background_selected");
        NinePatchDrawable gcListItemHover = ninePatchDrawable("GUI/ui2/internal_panel3_background_highlight");
        Button.ButtonStyle gcListItemStyle = new Button.ButtonStyle(gcListItemNormal, null, gcListItemChecked);
        gcListItemStyle.over = gcListItemHover;
        ImageTextButton.ImageTextButtonStyle gcListButtomStyle = new ImageTextButton.ImageTextButtonStyle();
        gcListButtomStyle.up=gcListItemNormal;
        gcListButtomStyle.over=gcListItemHover;
        gcListButtomStyle.down=gcListItemChecked;
        gcListButtomStyle.font=font;
        skin.add("gc-list-item", gcListItemStyle);
        skin.add("gc-list-button", gcListButtomStyle);

        Button.ButtonStyle gcListItemStyleNoCheck = new Button.ButtonStyle(gcListItemNormal, gcListItemChecked, gcListItemNormal);
        gcListItemStyleNoCheck.over = gcListItemHover;
        gcListItemStyleNoCheck.checkedOver = gcListItemHover;
        skin.add("gc-list-item-no-check", gcListItemStyleNoCheck);

        skin.add("gc-list-blank", new Button.ButtonStyle());

       // NinePatchDrawable dropDownNormal = ninePatchDrawable("GUI/ui2/dropdown1_normal");

        final TextureRegionDrawable loginBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/preload/startup-background1"));
        skin.add("login-background", loginBackgroundDrawable, Drawable.class);

        final TextureRegionDrawable loginLogoDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/preload/startup-logo1"));
        skin.add("login-logo", loginLogoDrawable, Drawable.class);

        final TextureRegionDrawable chracterEquipmentDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/character-equipment1"));
        skin.add("character-equipment", chracterEquipmentDrawable, Drawable.class);

        final TextureRegionDrawable characterEquipment2 = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/character-equipment2"));
        skin.add("character-equipment2", characterEquipment2, Drawable.class);



        NinePatchDrawable loginBorder = ninePatchDrawable("GUI/ui2/startup-lightborders1");
       // NinePatchDrawable loginBorder =new NinePatchDrawable(atlas.createPatch("GUI/ui2/startup-lightborders1"));

        skin.add("login-border",loginBorder, Drawable.class);

        final TextureRegionDrawable statusMessageBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/announcement1-background"));
        skin.add("status-message-background", statusMessageBackgroundDrawable, Drawable.class);

        /*
        final TextureRegionDrawable loginBorderDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/startup-lightborders1"));
        skin.add("login-border", loginBorderDrawable, Drawable.class);
        */

        final TextureRegionDrawable gameBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("game-background4"));
        skin.add("game-background", gameBackgroundDrawable, Drawable.class);

        final TextureRegionDrawable mapBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("game-background4"));
        skin.add("map-background", mapBackgroundDrawable, Drawable.class);



        NinePatchDrawable mapButtonNormalDrawable = ninePatchDrawable("map-indicators/button-base");

        ImageTextButton.ImageTextButtonStyle mapButtonStyle = createButtonStyle(mapButtonNormalDrawable, fontBold, 1.2f, 2f);
        skin.add("button-map", mapButtonStyle, ImageTextButton.ImageTextButtonStyle.class);

        final Pixmap cursorPixmap = new Pixmap(Gdx.files.internal("images/GUI/ui2/preload/cursor1-normal.png"));
        Cursor cursor = Gdx.graphics.newCursor(cursorPixmap, 3, 3);
        if(cursor != null) {
            skin.add(RESOURCE_DEFAULT, cursor, Cursor.class);
        }
//        NinePatchDrawable smallWindowBackground = new ninePatchDrawable("GUI/blue/window-small");
//        Window.WindowStyle smallWindowStyle = new Window.WindowStyle(font, Color.BLACK, smallWindowBackground);
//        skin.add("window-small", smallWindowStyle);

        String[] icons = new String[] {
            "bg_empty", "common_grade", "junk_grade",
            "uncommon_grade", "rare_grade", "epic_grade", "legendary_grade", "item_selected",
            "invention_skill_grade", "invention_prototype_grade", "invention_experiment_grade"
        };

        for(String icon : icons) {
            skin.add("icon-"+icon, new TextureRegionDrawable(atlas.createSprite("icons/base/"+icon)), Drawable.class);
        }

        skin.add("icon-item_frame", ninePatchDrawable("icons/base/item_frame"), Drawable.class);
        skin.add("icon-equipment_item_frame", ninePatchDrawable("icons/base/equipment_item_frame"), Drawable.class);
//        skin.add("icon-junk_grade", new BaseDrawable(), Drawable.class);

        skin.add("confirm-overlay-rectangle", ninePatchDrawable("GUI/ui2/confirm-overlay-rectangle"), Drawable.class);

        skin.add("inventory-background", ninePatchDrawable("GUI/ui2/internal_panel4_background"), Drawable.class);
        skin.add("action-list-background", ninePatchDrawable("GUI/ui2/window5_background"), Drawable.class);

        skin.add("dialogue-panel-background", ninePatchDrawable("GUI/ui2/dialogue_panel1_background"), Drawable.class);
        panelStyleDarkBorderSemiTransparent = ninePatchDrawable("GUI/ui2/dialogue_panel2_background");
        skin.add("dialogue-panel2-background", panelStyleDarkBorderSemiTransparent, Drawable.class);
        panelStyleWhiteBorder=ninePatchDrawable("GUI/ui2/internal_panel6_background");
        labelSmallElectrolizeBorders.background = panelStyleWhiteBorder;

        skin.add("console-background", ninePatchDrawable("GUI/ui2/chat_window_backing1"), Drawable.class);
        skin.add("console-tab-active", ninePatchDrawable("GUI/ui2/chat_window_tab1_active"), Drawable.class);
        skin.add("console-tab-inactive", ninePatchDrawable("GUI/ui2/chat_window_tab1_inactive"), Drawable.class);
        panelInternal12 = ninePatchDrawable("GUI/ui2/internal_panel2_background");
        skin.add("realm-win-panel-background", panelInternal12, Drawable.class);

        ImageTextButton.ImageTextButtonStyle mergeAllButtonStyle = new ImageTextButton.ImageTextButtonStyle(buttonStyleBlueYellowNoPadding);
        mergeAllButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/mergeall1"));
        mergeAllButtonStyle.checked = null;
        mergeAllButtonStyle.checkedOver = null;
        skin.add("btn-merge-all", mergeAllButtonStyle);

        ImageTextButton.ImageTextButtonStyle splitStackButtonStyle = new ImageTextButton.ImageTextButtonStyle(buttonStyleBlueYellowNoPadding);
        splitStackButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/splitstack1"));
        splitStackButtonStyle.checked = null;
        splitStackButtonStyle.checkedOver = null;
        skin.add("btn-split-stack", splitStackButtonStyle);

        ImageTextButton.ImageTextButtonStyle itemInspectorExperimentButtonStyle = new ImageTextButton.ImageTextButtonStyle(buttonStyleBlueYellowNoPadding);
        itemInspectorExperimentButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("invention/tab-experiments"));
        itemInspectorExperimentButtonStyle.checked = null;
        itemInspectorExperimentButtonStyle.checkedOver = null;
        skin.add("btn-item-experiment", itemInspectorExperimentButtonStyle);

        NinePatchDrawable dropDownNormal      = ninePatchDrawable("GUI/ui2/dropdown1_normal");
        NinePatchDrawable dropDownHovered     = ninePatchDrawable("GUI/ui2/dropdown1_hovered");
        NinePatchDrawable dropDownPressed     = ninePatchDrawable("GUI/ui2/dropdown1_pressed");
        NinePatchDrawable dropDownListBacking = ninePatchDrawable("GUI/ui2/dropdown1_listbacking1");

        List.ListStyle listStyle = new List.ListStyle(font, Color.WHITE, Color.LIGHT_GRAY, gcListItemChecked);
        listStyle.background = dropDownListBacking;
        skin.add(RESOURCE_DEFAULT, listStyle);
        SelectBox.SelectBoxStyle selectBoxStyle = new SelectBox.SelectBoxStyle(font, Color.WHITE, dropDownNormal, scrollPaneStyle, listStyle);
        selectBoxStyle.backgroundOpen = dropDownPressed;
        selectBoxStyle.backgroundOver = dropDownHovered;
        skin.add(RESOURCE_DEFAULT, selectBoxStyle);

        skin.add("top-graphic-right-corner", ninePatchDrawable("GUI/ui2/top_graphic1_right_corner_backing"), Drawable.class);
        skin.add("top-graphic-left-corner", ninePatchDrawable("GUI/ui2/top_graphic1_left_corner_backing"), Drawable.class);
        skin.add("top-graphic-button-bar-right", ninePatchDrawable("GUI/ui2/top_graphic1_button_bar_backing"), Drawable.class);
        skin.add("top-graphic-button-bar-left", ninePatchDrawable("GUI/ui2/top_graphic_left1_button_bar_backing"), Drawable.class);
        skin.add("top-graphic-middle", ninePatchDrawable("GUI/ui2/top_graphic1_middle"), Drawable.class);

        NinePatchDrawable topGraphicButton = ninePatchDrawable("GUI/ui2/top_graphic1_button_backing1");
        Drawable scaledTopGraphicDrawable = scaled(topGraphicButton, 1.05f);
        ImageTextButton.ImageTextButtonStyle uiTopGraphicButtonStyle = new ImageTextButton.ImageTextButtonStyle(topGraphicButton, topGraphicButton, topGraphicButton, fontButtons);
        uiTopGraphicButtonStyle.fontColor = Color.valueOf("#a8e7ff");
        uiTopGraphicButtonStyle.overFontColor = Color.valueOf("#d6f3ff");
        uiTopGraphicButtonStyle.downFontColor = Color.valueOf("#4bcdf1");
        uiTopGraphicButtonStyle.over = scaledTopGraphicDrawable;
        uiTopGraphicButtonStyle.checkedOver = scaledTopGraphicDrawable;
        setPadding(topGraphicButton,5,5,4,4);
        skin.add("top-graphic-button", uiTopGraphicButtonStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("top-graphic-button", uiTopGraphicButtonStyle, Button.ButtonStyle.class);

        panelStyleDeepInsetDark = ninePatchDrawable("GUI/ui2/purchase-dialog/dialog_subpanel1");
        skin.add("purchase-dialog-subpanel", panelStyleDeepInsetDark, Drawable.class);
        skin.add("purchase-list-item-1", new TextureRegionDrawable(atlas.createSprite("GUI/ui2/purchase-dialog/list-item1")), Drawable.class);

        this.panelStyleBlack = ninePatchDrawable("GUI/ui2/black_ninepatch");
        skin.add("black-panel", this.panelStyleBlack, Drawable.class);

        skin.add("harbor-g7-class1a-schematic-backdrop", new TextureRegionDrawable(atlas.createSprite("ships/harbor/harbor-g7-class1a-schematic-backdrop")), Drawable.class);

        return skin;
    }

    protected ImageTextButton.ImageTextButtonStyle createMapButtonStyle(BitmapFont fontButtons, String normal, String hover, String pressed) {
        TextureRegionDrawable btnMapNormal = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/minimap-buttons/" + normal));
        TextureRegionDrawable btnMapHover = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/minimap-buttons/" + hover));
        TextureRegionDrawable btnMapPressed = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/minimap-buttons/" + pressed));
        ImageTextButton.ImageTextButtonStyle btnMapStyle = new ImageTextButton.ImageTextButtonStyle(btnMapNormal, btnMapPressed, null, fontButtons);
        btnMapStyle.over = btnMapHover;
        return btnMapStyle;
    }

    protected BitmapFont createFont(String fontPath) {
        final BitmapFont font = new BitmapFont(Gdx.files.internal(fontPath));
        font.getData().markupEnabled = true;
        return font;
    }

    private <T extends Drawable> T zeroPadding(T drawable) {
        drawable.setLeftWidth(0);
        drawable.setRightWidth(0);
        drawable.setTopHeight(0);
        drawable.setBottomHeight(0);
        return drawable;
    }

    private void setPadding(Drawable drawable, float leftPadding, float rightPadding, float topPadding, float bottomPadding) {
        drawable.setLeftWidth(leftPadding);
        drawable.setRightWidth(rightPadding);
        drawable.setTopHeight(topPadding);
        drawable.setBottomHeight(bottomPadding);
    }

    protected NinePatchDrawable ninePatchDrawable(NinePatch patch, boolean updateMinSize) {
        NinePatchDrawable drawable = new NinePatchDrawable(patch);
        if(updateMinSize) {
            setupMinSize(drawable);
        }
        return drawable;
    }

    protected NinePatchDrawable ninePatchDrawable(String fileRef, boolean updateMinSize) {
        final NinePatch patch = atlas.createPatch(fileRef);
        if(patch == null) throw new IllegalStateException("Unable to find file "+fileRef);
        return ninePatchDrawable(patch, updateMinSize);
    }

    protected NinePatchDrawable ninePatchDrawable(String fileRef) {
        return ninePatchDrawable(fileRef, true);
    }

    protected NinePatchDrawable ninePatchDrawable(NinePatch patch) {
        return ninePatchDrawable(patch, true);
    }

    private void setupMinSize(NinePatchDrawable drawable) {
        drawable.setMinWidth(drawable.getPatch().getLeftWidth() + drawable.getPatch().getRightWidth());
        drawable.setMinHeight(drawable.getPatch().getTopHeight() + drawable.getPatch().getBottomHeight());
    }

    private void createSwitchStyle(Skin skin, BitmapFont font) {
        TextureRegion onTexture = atlas.createSprite("GUI/ui2/switch1-on-normal");
        TextureRegion offTexture = atlas.createSprite("GUI/ui2/switch1-off-normal");
        TextureRegion onOverTexture = atlas.createSprite("GUI/ui2/switch1-on-hovered");
        TextureRegion offOverTexture = atlas.createSprite("GUI/ui2/switch1-off-hovered");

        ImageTextButton.ImageTextButtonStyle switchStyle = new ImageTextButton.ImageTextButtonStyle();
        switchStyle.imageChecked = new TextureRegionDrawable(onTexture);
        switchStyle.imageUp = new TextureRegionDrawable(offTexture);
        switchStyle.imageOver = new TextureRegionDrawable(offOverTexture);
        switchStyle.imageCheckedOver = new TextureRegionDrawable(onOverTexture);
        switchStyle.font = font;
        skin.add("switch", switchStyle);

        TextureRegion checkOffTexture = atlas.createSprite("GUI/ui2/checkbox1-unchecked");
        TextureRegion checkOnTexture = atlas.createSprite("GUI/ui2/checkbox1-checked");

    }

    public static <T extends TransformDrawable> ImageTextButton.ImageTextButtonStyle createButtonStyle(T baseDrawable, BitmapFont font) {
        return createButtonStyle(baseDrawable, font, 1.1f, 1f);
    }

    public static <T extends TransformDrawable> ImageTextButton.ImageTextButtonStyle createButtonStyle(T baseDrawable, BitmapFont font, float overScale, float pressedOffset) {
        Drawable scaledDrawable = scaled(baseDrawable, overScale);
        ImageTextButton.ImageTextButtonStyle style = new ImageTextButton.ImageTextButtonStyle(baseDrawable, baseDrawable, baseDrawable, font);
        style.over = scaledDrawable;
        style.checkedOver = scaledDrawable;
        style.pressedOffsetX = pressedOffset;
        style.pressedOffsetY = pressedOffset;
        return style;
    }

    public static ImageTextButton.ImageTextButtonStyle createButtonStyle(TextureRegion textureRegion, BitmapFont font) {
        return createButtonStyle(textureRegion, font, 1.1f, 1f);
    }

    public static ImageTextButton.ImageTextButtonStyle createButtonStyle(TextureRegion textureRegion, BitmapFont font, float overScale, float pressedOffset) {
        return createButtonStyle(new TextureRegionDrawable(textureRegion), font, overScale, pressedOffset);
    }

    public static <T extends TransformDrawable> ImageButton.ImageButtonStyle createImageButtonStyle(T baseDrawable, float overScale, float pressedOffset) {
        Drawable overDrawable = scaled(baseDrawable, overScale);

        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle(null, null, null, baseDrawable, baseDrawable, baseDrawable);
        style.imageOver = overDrawable;
        style.imageCheckedOver = overDrawable;
        style.pressedOffsetX = pressedOffset;
        style.pressedOffsetY = pressedOffset;
        return style;
    }

    protected static <T extends TransformDrawable> Drawable scaled(T baseDrawable, float overScale) {
        Drawable overDrawable;
        if (overScale == 1f) {
            overDrawable = baseDrawable;
        } else {
            ScaledDrawable<T> scaledDrawable = new ScaledDrawable<>(baseDrawable);
            scaledDrawable.setScale(overScale);
            overDrawable = scaledDrawable;
        }
        return overDrawable;
    }

    public static class ClippingTextureRegionDrawable extends TextureRegionDrawable {

        private final boolean flip;
        private final boolean vertical;

        public ClippingTextureRegionDrawable(TextureRegion region, boolean flip, boolean vertical) {
            super(region);

            this.flip = flip;
            this.vertical = vertical;
        }

        @Override
        public void draw(Batch batch, float x, float y, float width, float height) {
            if (vertical) {

                int diff = (int) (getRegion().getRegionHeight() - height);
                if (diff < 0) diff = 0;
                if (flip) {
                    y += diff;
                }
                batch.draw(getRegion().getTexture(), x, y,
                        width, height, getRegion().getRegionX(), getRegion().getRegionY(), getRegion().getRegionWidth(), getRegion().getRegionHeight() - diff, false, !flip);
            }
            else {
                int diff = (int) (getRegion().getRegionWidth() - width);
                if (diff < 0) diff = 0;
                if (flip) {
                    x += diff;
                }
                batch.draw(getRegion().getTexture(), x, y,
                        width, height, getRegion().getRegionX(), getRegion().getRegionY(), getRegion().getRegionWidth() - diff, getRegion().getRegionHeight(), flip, false);

            }


        }
    }
}
