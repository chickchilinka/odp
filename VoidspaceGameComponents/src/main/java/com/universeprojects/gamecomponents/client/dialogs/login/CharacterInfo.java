package com.universeprojects.gamecomponents.client.dialogs.login;

public interface CharacterInfo {
    String getName();
    Long getId();
}
