package com.universeprojects.gamecomponents.client.dialogs.inventory;

public interface InventoryAux<T extends GCInventoryItem> {
    void onStartExperiment(T item);

    void onCustomize(T item);
}
