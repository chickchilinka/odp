package com.universeprojects.gamecomponents.client.windows;

import com.universeprojects.common.shared.callable.Callable2Args;

public class NoopWindowManager extends WindowManager {
    @Override
    public void closeAll() {
    }

    @Override
    public void updateFullscreen() {
    }

    @Override
    public void disableOptionsButtonInFullscreen(boolean disableOptionsButton) {
    }

    @Override
    public boolean getMobileMode() {
        return false;
    }

    @Override
    public void setMobileMode(boolean mobileMode) {
    }

    @Override
    public void subscribeOnWindowFullscreen(Callable2Args<Boolean, Boolean> handler) {
    }

    @Override
    boolean isActive(GCWindow window) {
        return false;
    }

    @Override
    void activateWindow(GCWindow window) {
    }

    @Override
    void windowClosed(GCWindow window) {
    }
}
