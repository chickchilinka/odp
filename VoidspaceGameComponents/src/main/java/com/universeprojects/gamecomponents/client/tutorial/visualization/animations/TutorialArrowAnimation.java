package com.universeprojects.gamecomponents.client.tutorial.visualization.animations;

public class TutorialArrowAnimation extends EasedAnimation {
    protected float time;
    protected float duration;
    protected float halfDuration;
    protected float delta;

    public TutorialArrowAnimation(float duration, float delta) {
        this.delta = delta;
        setHalfDuration(duration);
    }

    protected void setHalfDuration(float duration) {
        this.duration = duration;
        this.halfDuration = duration / 2;
    }

    public float animate(float delta) {
        time += delta;
        if (time >= duration)
            time = 0;

        return time < halfDuration
                ? quadInOutEasing(time, this.delta, halfDuration)
                : quadInOutEasing(time - halfDuration, -this.delta, halfDuration) + this.delta;
    }
}
