package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class InvalidatingAlphaAction extends AlphaAction {

    @Override
    protected void update(float percent) {
        super.update(percent);
        H5ELayer.invalidateBufferForActor(actor);
    }
}
