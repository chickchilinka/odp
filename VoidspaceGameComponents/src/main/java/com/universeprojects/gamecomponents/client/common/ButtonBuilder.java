package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.windows.GCTextTooltip;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.drawable.RectShapeDrawable;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

@SuppressWarnings("unused")
public class ButtonBuilder {

    public static InitialButtonBuilder inLayer(H5ELayer layer) {
        return new Builder(layer);
    }

    public static InitialButtonBuilder inLayer(GraphicElement element) {
        return new Builder(element.getLayer());
    }

    private static class Builder implements ButtonBuilder.InitialButtonBuilder, ButtonBuilder.StyledButtonBuilder {
        private final H5ELayer layer;
        private final Color color = new Color(Color.WHITE);
        private final Color textColor = new Color(Color.WHITE);
        private BitmapFont font;
        private String text = "";
        private Callable0Args buttonListener0Args;
        private String tooltipText;
        private Callable1Args<H5EButton> buttonListener1Args;
        private ImageTextButton.ImageTextButtonStyle style;
        private ImageTextButton.ImageTextButtonStyle altStyle;
        private Integer originAlign;
        private float fontScale = 1f;
        private boolean factoryStyle = false;
        private String name;
        private String tutorialId;

        private Builder(H5ELayer layer) {
            this.layer = layer;
            this.font = layer.getEngine().getSkin().getFont("default");
        }

        @Override
        public Builder withText(String text) {
            this.text = text;
            return this;
        }

        @Override
        public Builder withFont(BitmapFont font) {
            this.font = font;
            if (style != null) {
                if (factoryStyle) {
                    this.style = new ImageTextButton.ImageTextButtonStyle(style);
                    this.factoryStyle = false;
                }
                style.font = font;
            }
            return this;
        }

        @Override
        public Builder withFontScale(float fontScale) {
            this.fontScale = fontScale;
            return this;
        }

        @Override
        public Builder withColor(Color color) {
            color.set(color);
            return this;
        }

        @Override
        public Builder withTextColor(Color color) {
            textColor.set(color);
            return this;
        }

        @Override
        public Builder withTransparency(float alpha) {
            color.a = alpha;
            return this;
        }

        @Override
        public Builder withButtonListener(Callable0Args listener) {
            this.buttonListener0Args = listener;
            return this;
        }

        @Override
        public Builder withButtonListener(Callable1Args<H5EButton> listener) {
            this.buttonListener1Args = listener;
            return this;
        }

        public Builder withTutorialId(String id) {
            this.tutorialId = id;
            return this;
        }

        @Override
        public Builder withOrigin(int align) {
            originAlign = align;
            return this;
        }

        @Override
        public Builder withOriginCenter() {
            return withOrigin(Align.center);
        }

        @Override
        public Builder withOriginTopLeft() {
            return withOrigin(Align.topLeft);
        }

        @Override
        public Builder withOriginTopRight() {
            return withOrigin(Align.topRight);
        }

        @Override
        public Builder withOriginBottomLeft() {
            return withOrigin(Align.bottomLeft);
        }

        @Override
        public Builder withOriginBottomRight() {
            return withOrigin(Align.bottomRight);
        }

        @Override
        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        @Override
        public StyledButtonBuilder copy() {
            Builder builder = new Builder(layer);
            builder.color.set(this.color);
            builder.textColor.set(this.textColor);
            builder.font = this.font;
            builder.text = this.text;
            builder.buttonListener0Args = this.buttonListener0Args;
            builder.buttonListener1Args = this.buttonListener1Args;
            if (this.style != null) {
                builder.style = new ImageTextButton.ImageTextButtonStyle(this.style);
            }
            builder.originAlign = this.originAlign;
            builder.fontScale = this.fontScale;
            builder.name = name;
            return builder;
        }

        @Override
        public Builder withStyle(ImageTextButton.ImageTextButtonStyle style) {
            this.style = style;
            this.factoryStyle = true;
            return this;
        }

        @Override
        public Builder withStyle(String styleName) {
            this.style = layer.getEngine().getSkin().get(styleName, ImageTextButton.ImageTextButtonStyle.class);
            this.factoryStyle = true;
            return this;
        }

        @Override
        public Builder withDefaultStyle() {
            this.style = layer.getEngine().getSkin().get(ImageTextButton.ImageTextButtonStyle.class);
            this.factoryStyle = true;
            return this;
        }

        @Override
        public Builder withBackground(TransformDrawable background) {
            this.style = StyleFactory.createButtonStyle(background, font);
            return this;
        }

        @Override
        public Builder withBackground(H5ESpriteType spriteType) {
            this.style = StyleFactory.createButtonStyle(spriteType.getGraphicData(), font);
            return this;
        }

        @Override
        public Builder withBackground(String spriteTypeKey) {
            final H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(spriteTypeKey);
            this.style = StyleFactory.createButtonStyle(spriteType.getGraphicData(), font);
            return this;
        }

        @Override
        public Builder withFilledRectBackground() {
            return withFilledRectBackground(Color.WHITE);
        }

        @Override
        public Builder withFilledRectBackground(Color color) {
            final RectShapeDrawable drawable = new RectShapeDrawable(color);
            this.style = StyleFactory.createButtonStyle(drawable, font);
            return this;
        }

        @Override
        public Builder withLineRectBackground() {
            return withLineRectBackground(Color.WHITE);
        }

        @Override
        public Builder withLineRectBackground(Color color) {
            final RectShapeDrawable drawable = new RectShapeDrawable(color);
            drawable.setShapeType(ShapeRenderer.ShapeType.Line);
            this.style = StyleFactory.createButtonStyle(drawable, font);
            return this;
        }

        @Override
        public Builder withForegroundSpriteType(String spriteTypeKey) {
            if (factoryStyle) {
                this.style = new ImageTextButton.ImageTextButtonStyle(style);
                this.factoryStyle = false;
            }
            final H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(spriteTypeKey);
            style.imageUp = new TextureRegionDrawable(spriteType.getGraphicData());
            return this;
        }

        @Override
        public Builder withForegroundDrawable(String drawableStyle) {
            if (factoryStyle) {
                this.style = new ImageTextButton.ImageTextButtonStyle(style);
                this.factoryStyle = false;
            }
            style.imageUp = layer.getEngine().getSkin().getDrawable(drawableStyle);
            return this;
        }

        @Override
        public Builder withForegroundDrawable(Drawable drawable) {
            if (factoryStyle) {
                this.style = new ImageTextButton.ImageTextButtonStyle(style);
                this.factoryStyle = false;
            }
            style.imageUp = drawable;
            return this;
        }

        @Override
        public StyledButtonBuilder withTooltip(String text) {
            tooltipText = text;
            return this;
        }

        @Override
        public H5EButton build() {
            H5EButton button = new H5EButton(text, layer, style);
            button.setColor(color);
            button.getLabel().setColor(textColor);
            button.getLabel().setFontScale(fontScale);
            if (name != null) {
                button.setName(name);
            }
            if (buttonListener0Args != null && buttonListener1Args != null) {
                button.addButtonListener(() -> {
                    buttonListener0Args.call();
                    buttonListener1Args.call(button);
                });
            } else if (buttonListener1Args != null) {
                button.addButtonListener(() ->
                    buttonListener1Args.call(button));
            } else if (buttonListener0Args != null) {
                button.addButtonListener(buttonListener0Args);
            }
            if (tutorialId != null)
                button.setUserObject(tutorialId);

            if(tooltipText != null)
                button.addListener(new GCTextTooltip(tooltipText, layer.getEngine().getSkin()));

            return button;
        }
    }

    public interface InitialButtonBuilder {
        StyledButtonBuilder withStyle(ImageTextButton.ImageTextButtonStyle style);

        StyledButtonBuilder withStyle(String styleName);

        StyledButtonBuilder withDefaultStyle();

        StyledButtonBuilder withBackground(TransformDrawable background);

        StyledButtonBuilder withBackground(H5ESpriteType spriteType);

        StyledButtonBuilder withBackground(String spriteTypeKey);

        StyledButtonBuilder withFilledRectBackground(Color color);

        StyledButtonBuilder withFilledRectBackground();

        StyledButtonBuilder withLineRectBackground(Color color);

        StyledButtonBuilder withLineRectBackground();
    }

    public interface StyledButtonBuilder {
        StyledButtonBuilder withText(String text);

        StyledButtonBuilder withFont(BitmapFont font);

        StyledButtonBuilder withFontScale(float fontScale);

        StyledButtonBuilder withColor(Color color);

        StyledButtonBuilder withTextColor(Color color);

        StyledButtonBuilder withTransparency(float alpha);

        StyledButtonBuilder withButtonListener(Callable0Args listener);

        StyledButtonBuilder withButtonListener(Callable1Args<H5EButton> listener);

        StyledButtonBuilder withOrigin(int align);

        StyledButtonBuilder withOriginCenter();

        StyledButtonBuilder withOriginTopLeft();

        StyledButtonBuilder withOriginTopRight();

        StyledButtonBuilder withOriginBottomLeft();

        StyledButtonBuilder withOriginBottomRight();

        StyledButtonBuilder withName(String name);

        StyledButtonBuilder withTutorialId(String id);

        StyledButtonBuilder copy();

        H5EButton build();

        StyledButtonBuilder withForegroundSpriteType(String spriteTypeKey);

        StyledButtonBuilder withForegroundDrawable(String styleName);

        StyledButtonBuilder withForegroundDrawable(Drawable drawable);

        StyledButtonBuilder withTooltip(String text);

    }

}
