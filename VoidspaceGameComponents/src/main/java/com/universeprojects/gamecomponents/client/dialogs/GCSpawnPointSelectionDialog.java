package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.vsdata.shared.SpawnOptionData;

import java.util.List;

public class GCSpawnPointSelectionDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 450;
    private final static int WINDOW_H = 450;

    private final H5EScrollablePane listContainer;
    private final H5ELayer layer;
    private final GCListItemActionHandler<GCSpawnPointsListItem> listHandler;

    private final GCList<GCSpawnPointsListItem> itemsList;
    private GCSpawnPointsListItem selection;
    private final H5EButton respawn;
    private final H5EButton switchCharacter;


    public GCSpawnPointSelectionDialog(H5ELayer layer, Callable1Args<SpawnOptionData> onSpawnClick, Callable0Args onSwitchCharacterClick) {
        super(layer, "spawnPointSelectionDialog", "Choose respawn point", WINDOW_W, WINDOW_H, false);
        this.layer = layer;
        positionProportionally(0.65f, 0.65f);
        setFullscreenOnMobile(true);

        itemsList = new GCList<>(layer, 1, 5, 1, true);
        listHandler = new GCListItemActionHandler<GCSpawnPointsListItem>() {
            @Override
            protected void onSelectionUpdate(GCSpawnPointsListItem lastSelectedItem) {
                selection = this.getSelectedItem();
                respawn.setDisabled(selection == null);
            }
        };
        listContainer = new H5EScrollablePane(layer);
        listContainer.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        listContainer.setScrollingDisabled(true, false);
        listContainer.setOverscroll(false, true);
        listContainer.add(itemsList).left().top().grow();
        listContainer.layout();
        add(listContainer).top().left().grow();
        setFullscreenOnMobile(true);
        respawn = new H5EButton("Respawn now", layer);
        switchCharacter = new H5EButton("Switch Character", layer);
        switchCharacter.addButtonListener(onSwitchCharacterClick);
        respawn.addButtonListener(() -> onSpawnClick.call(selection.data));
        respawn.setDisabled(true);
        row();
        Table buttonRow = new Table();
        buttonRow.padTop(15);
        buttonRow.add(switchCharacter).left();
        buttonRow.add().growX();
        buttonRow.add(respawn).right().pad(4);
        add(buttonRow).right().growX();
    }

    public void processData(List<SpawnOptionData> dataList) {
        itemsList.clear();
        for (SpawnOptionData data : dataList) {
            GCSpawnPointsListItem item = new GCSpawnPointsListItem(layer, listHandler, data);
            itemsList.addItem(item);
        }
    }

    public void preselectFirstItem() {
        if(itemsList.isEmpty()) {
            return;
        }
        itemsList.get(0).setChecked(true);
    }
}