package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * A simple dialog for creating RGB color strings
 *
 * @author J. K. Rowling
 */
public class GCDemoCompleteDialog extends GCWindow {
    private String storeType;

    final public String playstoreUrl = "https://play.google.com/store/apps/details?id=com.universeprojects.voidspace.paid";
    final public String steamUrl = "https://store.steampowered.com/app/1150500/Voidspace/";
    final public String iosUrl = "https://www.voidspacegame.com/donate";
    final public String websiteUrl = "https://www.voidspacegame.com/donate";

    private Callable0Args onDialogClose;


    public GCDemoCompleteDialog(H5ELayer layer, String storeType, Callable0Args onDialogClose) {
        super(layer, 400, 600);
        this.storeType = storeType;
        this.onDialogClose = onDialogClose;
        setTitle("Continue Playing?");
        setFullscreenOnMobile(true);
        setModal(true);

        Table table = add(new Table()).colspan(2).grow().getActor();
        table.setBackground(StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque);
        H5ELabel text = new H5ELabel("" +
                "Your demo period has expired BUT, you can keep playing if you'd like!\n" +
                "\n" +
                "We're still in early access, so we cannot release the game as a free-to-play " +
                "experience yet. In order to keep playing you will need to donate (for donation " +
                "rewards, on our website) or buy a premium account. The proceeds go " +
                "directly to pay for further development and to help cover our server costs.\n" +
                "\n" +
                "Choose \"continue\" to see the account buying page." +
                "", layer);
        text.setWrap(true);
        table.add(text).grow().center();

        row();

        add(new H5EButton("Close", layer)).left().pad(5).getActor().addButtonListener(this::close);

        add(new H5EButton("Continue Playing!", layer)).right().pad(5).getActor().addButtonListener(this::doContinueClick);
    }

    @Override
    protected void onClose() {
        super.onClose();
        onDialogClose.call();
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    private void doContinueClick() {
        close();

        if (storeType.equals("IOS")) {
            Gdx.net.openURI(iosUrl);
        } else if (storeType.equals("Android")) {
            Gdx.net.openURI(playstoreUrl);
        } else if (storeType.equals("Steam")) {
            Gdx.net.openURI(steamUrl);
        } else {
            Gdx.net.openURI(websiteUrl);
        }
    }

}
