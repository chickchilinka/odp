package com.universeprojects.gamecomponents.client.dialogs.quests;


import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCLevelGauge;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Creates the graphical component for representing the quests in the quest manager dialog
 */
public class GCQuestListItem extends GCListItem {

    /**
     * The default description for quests with no description
     */
    private static final String DEFAULT_DESCRIPTION = "No description";

    // Quest properties begin here -------------------------------------------------------------------------------------

    /**
     * The name of the quest
     */
    protected final String name;
    protected final String id;

    /**
     * The description for the quest
     */
    protected final String description;
    private final int questCompletion;

    /**
     * The quest's current state
     */
    protected QuestState questState;

    protected String helperUrl;
    /**
     * A map of objective keys to objectives
     */
    protected Map<String, GCQuestObjective> objectiveMap = new LinkedHashMap<>();

    // Quest properties end here ---------------------------------------------------------------------------------------

    // Graphical components begin here ---------------------------------------------------------------------------------

    /**
     * The layer for the graphical components
     */
    H5ELayer layer;

    /**
     * The progress bar for representing the fraction of the quest's objectives that have been completed
     */
    private GCLevelGauge progressBar;

    /**
     * The action handler for when the quest is clicked
     */
    protected GCListItemActionHandler<GCQuestListItem> actionHandler;

    // Graphical components end here -----------------------------------------------------------------------------------

    /**
     * Constructor for the graphical component representing the quest
     */
    GCQuestListItem(H5ELayer layer, GCListItemActionHandler<GCQuestListItem> actionHandler, String id, String name, String description, Map<String, GCQuestObjective> objectiveMap, QuestState questState, int questCompletion, String helperUrl){
        super(layer, actionHandler);
        this.layer = layer;

        this.id = id;
        this.name          = (name        == null) ? ("") : (name);
        this.description   = (description == null || description.isEmpty()) ? (DEFAULT_DESCRIPTION)   : (description);
        this.objectiveMap.putAll(objectiveMap);
        this.questState    = (questState  == null) ? (QuestState.Incomplete) : (questState);
        this.actionHandler = actionHandler;
        this.questCompletion = questCompletion;
        this.helperUrl = helperUrl;

        initializeGraphics();
    }

    /**
     * Set up the graphical components
     */
    private void initializeGraphics(){
        //Create the label for displaying the quest's name
        H5ELabel nameLabel = add(new GCLabel(layer)).left().growX().padLeft(5).height(20).getActor();
        nameLabel.setText(name);
        nameLabel.setAlignment(Align.center);
        nameLabel.setFontScale(0.6f);

        //Create the progress bar for displaying the current progress
        progressBar = new GCLevelGauge(layer);
        row();
        add(progressBar).growX();
        progressBar.setValue(0);

        int numObjectives = objectiveMap.size();
        progressBar.setRange(numObjectives,1);
        progressBar.setLabel("0 / " + numObjectives);

        //Update the progress bar
        updateCompletion();
    }

    /**
     * Update the quest's completion based on the number of objectives that have been completed
     */
    private void updateCompletion (){
        //Count the number of objectives that have been completed
        int numObjectives = objectiveMap.size();
        int numObjectivesCompleted = 0;

        for (GCQuestObjective objective : objectiveMap.values()) {
            if (objective.isComplete()) {
                numObjectivesCompleted++;
            }
        }

        //Update the progress bar
        progressBar.setValue(numObjectivesCompleted);
        progressBar.setLabel(numObjectivesCompleted + " / " + numObjectives);

        //Check if the objective has been completed
        if (numObjectivesCompleted == numObjectives) {
            questState = QuestState.Complete;
        }
    }

    /**
     * @return Whether the quest is complete
     */
    public boolean isComplete () {
        return questState == QuestState.Complete;
    }

    // Getters begin here ----------------------------------------------------------------------------------------------

    public String getDescription () {
        return description;
    }

    public QuestState getQuestState() {
        return questState;
    }

    public int getQuestCompletion() { return questCompletion; }

    public String getId() {
        return id;
    }

    public String getHelperUrl() { return helperUrl; }

    // Getters end here ------------------------------------------------------------------------------------------------
}





