package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ETextArea;
import com.universeprojects.html5engine.client.framework.resourceloader.DownloadingSprite;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.gamecomponents.client.common.StyleFactory;


import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class GCMediaTextArea extends Table {
    private H5ETextArea textArea;
    private H5ELayer layer;
    private List<String> lines;
    private String content;
    private boolean readOnlyMode;

    enum ContentType {
        Text, Icon
    }

    public GCMediaTextArea(H5ELayer layer) {
        super();
        this.layer = layer;
        textArea = new H5ETextArea(layer);
    }

    public void setReadOnlyMode(boolean mode) {
        if (mode != readOnlyMode) {
            clearChildren();
            readOnlyMode = mode;
        }
    }

    public ContentType getLineContentType(String line) {
        if (line.length() > 5) {
            if (line.substring(0, 4).equals(".img")) {
                return ContentType.Icon;
            }
        }
        return ContentType.Text;
    }

    public void clearContent() {
        clearChildren();
        this.content = "";
        this.lines = new ArrayList<String>();
        textArea.setText("");
    }

    public String getText() {
        return textArea.getText();
    }

    public void processContent(String content) {
        clearContent();
        this.content = content;
        this.lines = Arrays.asList(content.split("\\r?\\n"));
        if (readOnlyMode) {
            for (String line : lines) {
                if (getLineContentType(line) == ContentType.Text) {
                    addLabel(line);
                } else {
                    addIcon(line);
                }
            }
            add().grow();
        } else {
            if (getCells().size == 0) {
                add(textArea).grow();
            }
            textArea.setText(content);
            invalidate();
        }
    }

    public H5ETextArea getTextArea() {
        return textArea;
    }

    public void addLabel(String text) {
        H5ELabel label = new H5ELabel(text, layer);
        label.setStyle(StyleFactory.INSTANCE.labelSmallElectrolize);
        add(label).growX();
        label.setWrap(true);
        row();
    }

    public void addIcon(String line) {
        String url = line.substring(line.indexOf("url=") + 4, line.length() - 1);
        Cell iconCell = add().center().maxHeight(200);
        H5EIcon newIcon = H5EIcon.fromSpriteType(layer, url, Scaling.fit, Align.center, (state, icon) -> {
            if (state == DownloadingSprite.DownloadState.LOADED) {
                H5EIcon newIcon2 = H5EIcon.fromSpriteType(layer, url, Scaling.fit, Align.center);
                iconCell.setActor(newIcon2);
            }
        });
        iconCell.setActor(newIcon);
        row();
    }


}