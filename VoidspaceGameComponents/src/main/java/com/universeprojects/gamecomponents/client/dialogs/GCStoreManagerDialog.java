package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;

public class GCStoreManagerDialog extends GCSimpleWindow {
    private H5ELayer layer;
    private List<GCStoreOrderComponent> storeOrderComponentsList;
    private H5EScrollablePane storeScrollView;
    private H5ELabel sellLabel;
    private H5ELabel buyLabel;
    private H5EScrollablePane sellPane;
    private H5EScrollablePane buyPane;
    private Table buttonTable;

    public GCStoreManagerDialog (H5ELayer layer) {
        super(layer, "storeDialogWindow", "Store Dialog", 800, 600, false);
        this.layer = layer;
        setFullscreenOnMobile(true);

        Table headerTable = new Table();
        this.add(headerTable).growX();
        sellLabel = new H5ELabel("Selling", layer);
        sellLabel.setFontScale(1.2f);
        buyLabel = new H5ELabel("Buying", layer);
        buyLabel.setFontScale(1.2f);

        headerTable.add(sellLabel).left().grow();
        headerTable.add(buyLabel).left().grow();
        // Empty placeholder to conform to formatting of the scroll pane
        headerTable.add().width(Value.percentWidth(0.1f, headerTable));

        this.row();

        storeOrderComponentsList = new ArrayList<>();
        storeScrollView = new H5EScrollablePane(layer, "scrollpane-backing-1");
        this.add(storeScrollView).grow();
        storeScrollView.getContent().left().top();
        storeScrollView.setOverscroll(false, false);
        storeScrollView.setScrollingDisabled(true, false);

        sellPane = storeScrollView.add(new H5EScrollablePane(layer)).growY().width(Value.percentWidth(0.45f, storeScrollView.getContent())).left().top().getActor();
        sellPane.getContent().left().top();
        sellPane.setOverscroll(false, false);
        sellPane.setWidth(sellPane.getContent().getWidth());
        buyPane = storeScrollView.add(new H5EScrollablePane(layer)).grow().getActor();
        buyPane.getContent().left().top();
        buyPane.setOverscroll(false, false);
        buttonTable = storeScrollView.add(new Table()).left().top().getActor();

        this.row();

        // Add manager specific button to create a new store order
        final H5EButton newOrderBtn = ButtonBuilder
                .inLayer(layer)
                .withStyle("button-small")
                .withText("NEW ORDER")
                .build();

        newOrderBtn.addButtonListener(new Callable0Args() {
            @Override
            public void call() {
                createStoreOrderDialog();
            }
        });

        this.add(newOrderBtn).right();
    }

    //TODO allow player named store
    public GCStoreManagerDialog (H5ELayer layer, String storeName) {
        super(layer, "storeDialogWindow", storeName);
        storeOrderComponentsList = new ArrayList<>();

        this.add("Selling").left().grow();
        this.add("Buying").right().grow();
    }

    /**
     * Adds a store order component to the dialog.
     */
    public void addStoreOrderToDialog (GCStoreOrderComponent order) {
        GCStoreOrderComponent newOrderComponent = new GCStoreOrderComponent(
                getLayer(), order.getStoreOrderKey(),
                order.getBuyItemName(), order.getBuyItemIconName(), order.getBuyItemQuantity(),
                order.getSellItemName(), order.getSellItemIconName(), order.getSellItemQuantity()) {

            @Override
            public void deleteOrder(String storeOrderKey) {
                super.deleteOrder(storeOrderKey);
                deleteStoreOrder(storeOrderKey);
            }
        };

        storeOrderComponentsList.add(newOrderComponent);
        sellPane.getContent().row();
        buyPane.getContent().row();
        buttonTable.row();

        newOrderComponent.addStoreEntry(sellPane.getContent(), newOrderComponent.getBuyItemName(), newOrderComponent.getBuyItemIconName(), newOrderComponent.getBuyItemQuantity());
        newOrderComponent.addStoreEntry(buyPane.getContent(), newOrderComponent.getSellItemName(), newOrderComponent.getSellItemIconName(), newOrderComponent.getSellItemQuantity());
        newOrderComponent.addStoreOrderDeleteButton(buttonTable);
    }

    public void clearOrdersFromDialog() {
        this.storeOrderComponentsList.clear();
        this.sellPane.getContent().clearChildren();
        this.buyPane.getContent().clearChildren();
        this.buttonTable.clearChildren();
    }

    /**
     * Method to be overridden on manager dialog construction
     */
    public void createStoreOrderDialog() {
        GCStoreOrderCreateDialog orderCreateDialog = new GCStoreOrderCreateDialog(getLayer()) {
            @Override
            public void createStoreOrder() {
                executeCreateStoreOrder(
                        this.getItemNameForOffer(), this.getItemQuantityForOfferAsString(),
                        this.getItemNameForPayment(), this.getItemQuantityForPaymentAsString());
                this.close();
            }
        };
        orderCreateDialog.open();
    }

    /**
     * Method to be overridden on construction
     * Takes the quantity as strings so that a system message can be sent later from the StoreService if the value typed into the input box
     * is not a proper number
     */
    public void executeCreateStoreOrder (String offerItemName, String offerItemQuantity, String paymentItemName, String paymentItemQuantity) {

    }

    /**
     * Method to be overridden on construction
     */
    public void deleteStoreOrder (String storeOrderKey) {

    }
}
