package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCEquipmentInspector<S extends Comparable<S>, T extends GCInventoryItem> extends GCItemInspectorWithActions<EquipmentSlotKey<S>, T> {
    public GCEquipmentInspector(H5ELayer layer) {
        super(layer, false);
    }
}
