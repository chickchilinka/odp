package com.universeprojects.gamecomponents.client.dialogs.battleRoyaleLobby;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.util.VoidspaceNumberFormat;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.elements.GCSwitch;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.*;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class BattleRoyaleLobby extends GCWindow {

    private final H5ELayer layer;
    private final ShipSelectorDialog shipSelector;

    private final H5EContainer mainTable;
    private final Table leftSide;
    private final Table rightSide;
    private final Table generalInfoTable;
    private final H5EScrollablePane listPane;
    private final ButtonGroup<NewRealmPlayerListItem> buttonGroup;
    private final H5EScrollablePane chatPane; // This should be replaced by the actual chat pane

    private final H5ELabel playerCountLabel;
    private final H5ELabel playerNameLabel;
    private final H5ELabel playerTitleLabel;
    private final H5ELabel playerGamesLabel;
    private final H5ELabel playerWinsLabel;
    private final H5ELabel playerKillsLabel;

    private final H5EContainer profilePane;
    private final H5ELabel shipName;
    private final Table spriteTable;
    private final H5EIcon mainShipIcon;
    private final H5EIcon topShipIcon;
    private final GCLoadingIndicator shipLoadingIndicator;
    private boolean loadedOneSprite = false;

    private final Table profileButtons;
    private final H5EButton btnProfileBack;
    private final H5EButton btnChangeShip;

    private RealmPlayerData you = null;
    private NewRealmPlayerListItem playerListItem = null;

    private enum WindowState {DESKTOP, LANDSCAPE, PORTRAIT}
    private WindowState lastState = WindowState.DESKTOP;
    private boolean viewProfilePane = false;

    public BattleRoyaleLobby(H5ELayer layer) {
        super(layer,850, 600);
        this.layer = layer;
        setTitle("Battle Royale");
        setId("battle-royale-lobby");
        setCloseButtonEnabled(false);
        positionProportionally(0.5f, 0.5f);
        setKeepWithinStage(true);
        disableOptionsButtonInFullscreen(false);
        setFullscreenOnMobile(true);
        shipSelector = new ShipSelectorDialog(layer, (ship) -> {
            if (you != null) {
                you.ship = ship;
                changeShip(ship);
            }
            if (playerListItem != null) {
                playerListItem.changeShip(ship.mainIconURL);
            }
        });

        mainTable = add(new H5EContainer(layer)).grow().getActor();
        mainTable.setBackground("purchase-dialog-subpanel");

        leftSide = mainTable.add(new Table()).grow().uniformX().pad(5).getActor();
        rightSide = mainTable.add(new Table()).grow().uniformX().pad(5).getActor();

        // Left Side
        generalInfoTable = leftSide.add(new Table()).growX().getActor();
        leftSide.row();
        H5ELabel title = generalInfoTable.add(new H5ELabel("Welcome to the Battle Royale lobby!",layer)).growX().colspan(2).align(Align.left).getActor();
        title.setStyle(layer.getEngine().getSkin().get("label-bold", Label.LabelStyle.class));
        title.setWrap(true);
        generalInfoTable.row();

        H5ELabel topText = generalInfoTable.add(new H5ELabel("Please stand by while we are searching for players to join you in this Battle Royale game. The game will start automatically when enough players are found.", layer)).fillX().colspan(2).getActor();
        topText.setWrap(true);
        topText.setFontScale(0.75f);
        topText.setColor(Color.valueOf("#959595"));
        generalInfoTable.row().padTop(15);

        generalInfoTable.add(new H5ELabel("Game Start Notification", layer)).fillX();
        generalInfoTable.add(new GCSwitch(layer)).align(Align.right);
        generalInfoTable.row().padTop(5);

        generalInfoTable.add(new H5ELabel("Players:", layer)).fillX();
        playerCountLabel = generalInfoTable.add(new H5ELabel("0/10", layer)).align(Align.right).getActor();
        generalInfoTable.row().padTop(15);

        // Pane containing the player list
        listPane = leftSide.add(new H5EScrollablePane(layer, "scrollpane-backing-1")).grow().getActor();
        listPane.setOverscroll(false, false);
        listPane.getContent().top();

        buttonGroup = new ButtonGroup<>();
        buttonGroup.setMinCheckCount(0);


        // Right side
        profilePane = rightSide.add(new H5EContainer(layer)).grow().getActor();
        profilePane.setBackground("scrollpane-backing-2-drawable");
        profilePane.defaults().left().top();
        profilePane.pad(5);

        H5EContainer playerInfo = profilePane.add(new H5EContainer(layer)).growX().pad(10).getActor();
        playerInfo.setBackground("purchase-dialog-subpanel");
        profilePane.row();

        playerNameLabel = playerInfo.add(new H5ELabel("",layer)).align(Align.left).growX().colspan(3).getActor();
        playerNameLabel.setStyle(layer.getEngine().getSkin().get("label-bold", Label.LabelStyle.class));
        playerInfo.row();

        playerTitleLabel = playerInfo.add(new H5ELabel("Noob [Rank: 9,999]",layer)).align(Align.left).growX().colspan(3).getActor();
        playerTitleLabel.setColor(Color.valueOf("#959595"));
        playerTitleLabel.setFontScale(0.75f);
        playerInfo.row();

        playerGamesLabel = playerInfo.add(new H5ELabel("Games:",layer)).align(Align.left).uniformX().getActor();
        playerGamesLabel.setFontScale(0.75f);
        playerGamesLabel.setColor(Color.valueOf("#959595"));

        playerWinsLabel = playerInfo.add(new H5ELabel("Wins:",layer)).align(Align.left).uniformX().getActor();
        playerWinsLabel.setFontScale(0.75f);
        playerWinsLabel.setColor(Color.valueOf("#959595"));

        playerKillsLabel = playerInfo.add(new H5ELabel("Kills:",layer)).align(Align.left).uniformX().getActor();
        playerKillsLabel.setFontScale(0.75f);
        playerKillsLabel.setColor(Color.valueOf("#959595"));
        playerInfo.row();

        Table shipTable = profilePane.add(new Table()).left().getActor();
        H5ELabel shipTitle = shipTable.add(new H5ELabel("Ship: ",layer)).align(Align.left).padLeft(15).getActor();
        shipTitle.setStyle(layer.getEngine().getSkin().get("label-bold", Label.LabelStyle.class));
        shipName = shipTable.add(new H5ELabel("",layer)).align(Align.left).getActor();
        profilePane.row();

        // Ship Sprites
        spriteTable = profilePane.add(new Table()).grow().getActor();
        shipLoadingIndicator = new GCLoadingIndicator(layer);

        mainShipIcon = new H5EIcon(layer, new BaseDrawable());
        mainShipIcon.setScaling(Scaling.fit);
        spriteTable.add(mainShipIcon)
                .grow()
                .pad(50,15,-15,-5)
                .padTop(Value.percentHeight(0.2f, spriteTable));

        topShipIcon = new H5EIcon(layer, new BaseDrawable());
        topShipIcon.setScaling(Scaling.fit);
        spriteTable.add(topShipIcon)
                .width(Value.percentWidth(0.4f, spriteTable))
                .growY()
                .pad(15,5,15,15)
                .padBottom(Value.percentHeight(0.3f, spriteTable));

        profilePane.row();

        // Profile back button
        profileButtons = profilePane.add(new Table()).growX().getActor();
        btnProfileBack = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Back").build();
        btnProfileBack.setVisible(false);
        btnProfileBack.addButtonListener(() -> {
            viewProfilePane = false;
            resetLayout(true);
        });
        profileButtons.add(btnProfileBack);

        // Change Ship Button
        btnChangeShip = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Change Ship").build();
        btnChangeShip.addButtonListener(shipSelector::open);
        profileButtons.add(btnChangeShip).pad(5);
        profilePane.row();

        // Chat pane
        chatPane = new H5EScrollablePane(layer, "scrollpane-backing-1");
        chatPane.getContent().top().left();
        H5ELabel exampleChat = chatPane.add(new H5ELabel(
                "Player 1: iljbse rguioaw evjilbae uilvbasv \n" +
                        "Player 2: uio ehrogiuanh pegoanmw epgoinm\n" +
                        "Player 1: iljbse rguioa wevjilba euil vbasv\n" +
                        "Player 2: uioe hrogi uanhpegoa nmwep goinm\n" +
                        "Player 1: iljb sergu ioawe vjil baeuilv basv\n" +
                        "Player 2: ui oehr ogiuan hpego anmwep goinm\n" +
                        "Player 1: iljbs erguio awevj ilbaeui lvbasv\n" +
                        "Player 2: uioe hrog iuanhp egoa nmwep goinm\n" +
                        "Player 1: ilj bserg uioawev jilb aeuilvb asv\n" +
                        "Player 2: uioehr ogiuanhpe goanmwep goinm\n" +
                        "Player 1: iljbserg uioawe vjilbae uilv basv\n" +
                        "Player 2: uio ehrogiuanh pegoanmwep goinm\n" +
                        "Player 1: iljbs erguio awev jilbaeui lvb asv\n" +
                        "Player 2: uio ehrogiu anh pegoa nmwe pgoinm\n", layer)).top().left().growX().getActor();
        exampleChat.setWrap(true);

        // Test Data
        setPlayerCountText("7/10");

        RealmShipData defaultShip = new RealmShipData(
                "Harbor G7 Class 1A",
                "ship description ship description ship description ship description ship description",
                299,
                "images/ships/harbor/harbor-g7-class1a-icon",
                "images/ships/harbor/harbor-g7-class1a",
                true);

        RealmPlayerData gabeCarvalho = new RealmPlayerData(true,"GabeCarvalho", "Noob", 5978, defaultShip, 25, 6, 38);
        addPlayer(new NewRealmPlayerListItem(layer, gabeCarvalho));
        setProfile(gabeCarvalho);

        addPlayer(new NewRealmPlayerListItem(layer, new RealmPlayerData(false, "Bozo", "Noob", 9999, defaultShip, 83, 0, 0)));
        addPlayer(new NewRealmPlayerListItem(layer, new RealmPlayerData(false, "Nik", "Champion", 1, defaultShip, 9999, 9999, 99999)));
        addPlayer(new NewRealmPlayerListItem(layer, new RealmPlayerData(false, "Infected Ship 1A", "Infected", 9998, defaultShip, 25, 0, 1)));
        addPlayer(new NewRealmPlayerListItem(layer, new RealmPlayerData(false, "Infected Ship 1B", "Infected", 7777, defaultShip, 28, 3, 17)));
        addPlayer(new NewRealmPlayerListItem(layer, new RealmPlayerData(false, "Infected Ship 2A", "Infected", 157, defaultShip, 255, 70, 486)));
        addPlayer(new NewRealmPlayerListItem(layer, new RealmPlayerData(false, "Infected Ship 3A", "Infected", 27, defaultShip, 789, 573, 2432)));
        // End of test data
    }

    public void setProfile(RealmPlayerData player) {
        playerNameLabel.setText(player.name);
        if (player.isYou) {
            playerNameLabel.setColor(0.5f, 0.7f, 1.0f, 1.0f);
        } else {
            playerNameLabel.setColor(1,1,1,1);
        }
        playerTitleLabel.setText(player.title + " [Rank: " + VoidspaceNumberFormat.format(player.rank) + "]");
        playerGamesLabel.setText("Games: " + player.games);
        playerWinsLabel.setText("Wins: " + player.wins);
        playerKillsLabel.setText("Kills: " + player.kills);
        changeShip(player.ship);

        profileButtons.clearChildren();
        profileButtons.add().growX();
        profileButtons.add(btnProfileBack);
        if (player.isYou) profileButtons.add(btnChangeShip);
    }

    public void changeShip(RealmShipData data) {
        shipLoadingIndicator.activate(spriteTable);
        shipName.setText(data.name);

        if (data.mainIconURL == null || data.mainIconURL.length() == 0) data.mainIconURL = "voidspace";
        if (data.topDownURL == null || data.topDownURL.length() == 0) data.topDownURL = "voidspace";

        H5ESpriteType topSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(data.topDownURL);
        topSpriteType.subscribeToLoadStateChange((state)-> {
            topShipIcon.setDrawable(new TextureRegionDrawable(topSpriteType.getGraphicData()));
            onSpriteLoaded();
        });

        H5ESpriteType mainSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(data.mainIconURL);
        mainSpriteType.subscribeToLoadStateChange((state)-> {
            mainShipIcon.setDrawable(new TextureRegionDrawable(mainSpriteType.getGraphicData()));
            onSpriteLoaded();
        });
    }

    private void onSpriteLoaded() {
        if (!loadedOneSprite) {
            loadedOneSprite = true;
        } else {
            shipLoadingIndicator.deactivate();
            loadedOneSprite = false;
        }
    }

    public void addPlayer(NewRealmPlayerListItem player) {
        listPane.getContent().add(player).top().growX().pad(2,2,2,2);
        listPane.getContent().row();
        buttonGroup.add(player);
        player.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                setProfile(player.getData());
                viewProfilePane = true;
                resetLayout(true);
                event.handle();
            }
        });
        if (player.getData().isYou) {
            you = player.getData();
            playerListItem = player;
        }
    }

    public void removePlayer(NewRealmPlayerListItem player) {
        buttonGroup.remove(player);
        listPane.getContent().clearChildren();
        for (NewRealmPlayerListItem newPlayer : buttonGroup.getButtons()) {
            listPane.getContent().add(newPlayer).top().growX().pad(2,2,2,2);
            listPane.getContent().row();
        }
    }

    public void setPlayerCountText(String playerCountText) {
        playerCountLabel.setText(playerCountText);
    }

    public ShipSelectorDialog getShipSelector() {
        return shipSelector;
    }

    @Override
    protected void positionOnResize() {
        resetLayout(false);
        super.positionOnResize();
    }

    private void resetLayout(boolean forceLayout) {
        // Reconstruct the window based on device and orientation
        WindowState currentState = WindowState.DESKTOP;
        if (isFullscreen()) {
            currentState = getEngine().getWidth() > getEngine().getHeight() ? WindowState.LANDSCAPE : WindowState.PORTRAIT;
        }
        if (currentState == lastState && !forceLayout) return;
        lastState = currentState;

        mainTable.clearChildren();
        rightSide.clearChildren();
        leftSide.clearChildren();
        if (currentState == WindowState.DESKTOP) {
            // Default
            mainTable.add(leftSide).grow().uniformX().pad(5).getActor();
            mainTable.add(rightSide).grow().uniformX().pad(5).getActor();
            leftSide.add(generalInfoTable).growX();
            leftSide.row();
            leftSide.add(listPane).grow();
            rightSide.add(profilePane).grow();
            btnProfileBack.setVisible(false);
        } else if (currentState == WindowState.LANDSCAPE) {
            // Landscape
            mainTable.add(leftSide).grow().uniformX().pad(5).getActor();
            mainTable.add(rightSide).grow().uniformX().pad(5).getActor();
            leftSide.add(generalInfoTable).growX();
            leftSide.row();
            leftSide.add(chatPane).grow();
            if (!viewProfilePane) {
                rightSide.add(listPane).grow();
            } else {
                rightSide.add(profilePane).grow();
            }
            btnProfileBack.setVisible(true);
        } else {
            // Portrait
            mainTable.add(leftSide).grow().pad(5).getActor();
            if (!viewProfilePane) {
                leftSide.add(generalInfoTable).growX();
                leftSide.row();
                leftSide.add(listPane).grow();
            } else {
                leftSide.add(profilePane).grow();
            }
            leftSide.row();
            leftSide.add(chatPane).growX().height(Value.percentHeight(0.35f, leftSide));
            btnProfileBack.setVisible(true);
        }
    }
}
