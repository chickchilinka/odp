package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeasData.GCIdeaDataItem;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

@SuppressWarnings("FieldCanBeLocal")
class GCIdeaListItem extends GCListItem {

    public static final int ICON_SIZE = 64;
    final GCIdeaDataItem data;

    private final H5EIcon icon;
    private final Table subTable;
    private final H5EScrollLabel nameLabel;
    private final H5ELabel descTextArea;

    private final H5EScrollablePane parentScrollPane;

    public GCIdeaListItem(H5ELayer layer, GCListItemActionHandler<GCIdeaListItem> actionHandler, GCIdeaDataItem data, H5EScrollablePane parentScrollPane) {
        super(layer, actionHandler);
        this.parentScrollPane = parentScrollPane;

        left().top();
        defaults().left().top();

        this.data = Dev.checkNotNull(data);
        if (data.index < 0) {
            throw new IllegalArgumentException("Index can't be negative");
        }
        if (Strings.isEmpty(data.name)) {
            throw new IllegalArgumentException("Idea name can't be blank");
        }
        if (Strings.isEmpty(data.description)) {
            throw new IllegalArgumentException("Idea description can't be blank");
        }
        if (Strings.isEmpty(data.iconKey)) {
            throw new IllegalArgumentException("Idea icon key can't be blank");
        }

        icon = add(H5EIcon.fromSpriteType(layer, data.iconKey)).size(ICON_SIZE).padRight(5).getActor();

        subTable = new Table();
        subTable.left().top();
        subTable.defaults().left().top();
        add(subTable).growX().fillY();

        nameLabel = new H5EScrollLabel(data.name, layer);
        nameLabel.getLabel().setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));


        String finalDescription = data.description;
        if (data.durationSecs!=null) {
            finalDescription = "[" + UPUtils.secondsToTimeShortString(data.durationSecs) + "] " + finalDescription;
        }
        descTextArea = new H5ELabel(finalDescription, layer);

        descTextArea.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        descTextArea.setColor(Color.valueOf("#AAAAAA"));
        descTextArea.setWrap(true);
        descTextArea.setTouchable(Touchable.disabled);

        rebuildActor();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        nameLabel.setAutoScroll(isChecked() || isOver());
    }

    public void rebuildActor() {
        subTable.clear();
        int newWidth = (int) (parentScrollPane.getWidth() - ICON_SIZE - 17);
        subTable.add(nameLabel).width(newWidth);
        nameLabel.setWidth(newWidth);
        nameLabel.setupScrolling();
        subTable.row();
        subTable.add(descTextArea).growX().getActor();
    }

    @Override
    public String getName() {
        return data.name;
    }
}
