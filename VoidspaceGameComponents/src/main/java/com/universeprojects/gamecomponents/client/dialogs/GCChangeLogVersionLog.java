package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCChangeLogVersionLog extends Table {

    private final H5ELayer layer;
    private final String version;

    public GCChangeLogVersionLog(H5ELayer layer, String version) {
        this.layer = layer;
        this.version = version;

        H5ELabel versionLabel = add(new H5ELabel(version, layer)).grow().left().colspan(1).getActor();
        versionLabel.setStyle(layer.getEngine().getSkin().get("label-bold", Label.LabelStyle.class));
        row();
    }

    public String getVersion() {
        return version;
    }

    public void addChangeLogEntry(String text, String interest, String color) {
        interest = interest.toUpperCase();
        switch (interest) {
            case "LOW": interest = "[S] "; break;
            case "MEDIUM": interest = "[M] "; break;
            case "HIGH": interest = "[L] "; break;
        }
        Table table = add(new Table()).left().grow().getActor();
        table.add(new H5ELabel("- ", layer)).top().left();
        H5ELabel interestLabel = table.add(new H5ELabel(interest, layer)).top().left().getActor();
        interestLabel.setColor(Color.valueOf(color));
        H5ELabel logEntry = table.add(new H5ELabel(text, layer)).grow().left().getActor();
        logEntry.setWrap(true);
        logEntry.setColor(Color.valueOf("#959595"));
        if (interest.equals("[L] ")) logEntry.setColor(Color.valueOf("#CCCCCC"));
            row();
    }
}
