package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class GCInventoryData {

    private final Map<Integer, GCInventoryDataItem> itemsByIndex = new HashMap<>();
    private final Map<String, GCInventoryDataItem> itemsByUid = new HashMap<>();
    private String inventoryName = "Inventory";

    private GCInventoryData() {
    }

    public static GCInventoryData createNew() {
        return new GCInventoryData();
    }

    public GCInventoryData copy() {
        GCInventoryData copy = new GCInventoryData();
        for (GCInventoryDataItem item : itemsByIndex.values()) {
            copy.add(item.item);
        }
        return copy;
    }

    public GCInventoryData add(GCInventoryData other) {
        for (GCInventoryDataItem item : other.itemsByIndex.values()) {
            // we call the add() method sequentially in order for new indices to be assigned
            add(item.item);
        }
        return this;
    }

    public GCInventoryData add(GCInventoryItem item) {
        int index = itemsByIndex.size();
        return add(new GCInventoryDataItem(index, item));
    }

    public GCInventoryData add(GCInventoryItem item, boolean selectable) {
        int index = itemsByIndex.size();
        return add(new GCInventoryDataItem(index, item, selectable));
    }

    public GCInventoryData add(GCInventoryDataItem item) {
        if (itemsByIndex.containsKey(item.index)) {
            throw new IllegalStateException("Duplicate index: " + item.index);
        }
        String uid = item.item.getUid();
        if (itemsByUid.containsKey(uid)) {
            return this;
        }
        itemsByIndex.put(item.index, item);
        itemsByUid.put(uid, item);
        return this;
    }

    public int size() {
        return itemsByIndex.size();
    }

    public GCInventoryDataItem get(int index) {
        return itemsByIndex.get(index);
    }

    public GCInventoryDataItem getByUid(String uid) {
        return itemsByUid.get(uid);
    }

    public boolean hasUid(String uid) {
        return itemsByUid.containsKey(uid);
    }

    public boolean containsItemForUid(String uid) {
        return itemsByUid.get(uid) != null;
    }

    public Set<String> getUids() {
        return new HashSet<>(itemsByUid.keySet());
    }

    public void replace(GCInventoryDataItem item) {
        if (!itemsByUid.containsKey(item.item.getUid())) {
            throw new IllegalStateException("UID not found: " + item.item.getUid());
        }
        remove(item.item.getUid());
        add(item);
    }

    public void remove(String uid) {
        if (itemsByUid.containsKey(uid)) {
            GCInventoryDataItem oldItem = itemsByUid.remove(uid);
            itemsByIndex.remove(oldItem.index);
        }
    }

    public void removeAll(Collection<String> uids) {
        for (String uid : uids) {
            remove(uid);
        }
    }

    public Map<Integer, GCInventoryDataItem> getItemsByIndex() { return Collections.unmodifiableMap(itemsByIndex); }

    public void forEach(Callable1Args<GCInventoryDataItem> consumer) {
        for(GCInventoryDataItem item : itemsByIndex.values()) {
            consumer.call(item);
        }
    }

    public static class GCInventoryDataItem {
        public final int index;
        public final GCInventoryItem item;
        public final boolean selectable;

        //Default to being selectable
        GCInventoryDataItem(int index, GCInventoryItem item) {
            this(index, item, true);
        }

        GCInventoryDataItem(int index, GCInventoryItem item, boolean selectable) {
            this.index = index;
            this.item = item;
            this.selectable = selectable;
        }

        public GCInventoryDataItem copy() {
            return new GCInventoryDataItem(index, item, selectable);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            GCInventoryDataItem that = (GCInventoryDataItem) o;
            if(item == null && that.item == null) {
                return true;
            } else if(item == null || that.item == null) {
                return false;
            }
            return Objects.equals(item.getUid(), that.item.getUid());
        }

        @Override
        public int hashCode() {
            return Objects.hash(item);
        }
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }
}


