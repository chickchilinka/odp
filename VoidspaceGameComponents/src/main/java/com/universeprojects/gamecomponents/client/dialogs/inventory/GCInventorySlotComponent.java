package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;

public class GCInventorySlotComponent<K, T extends GCInventoryItem, C extends GCInventorySlotComponent<K, T,C>> extends GCInventorySlotBaseComponent<K, T, C> {

    public static final int DEFAULT_ICON_SIZE = 50;
    protected final GCItemIcon<T> itemIcon;
    private final Cell<?> itemIconCell;
    private float iconSize;

    public GCInventorySlotComponent(GCInventoryBase<K, T, C> inventoryBase, K key, SlotConfig slotConfig) {
        this(inventoryBase, "gc-list-blank", key, slotConfig);
    }

    public GCInventorySlotComponent(GCInventoryBase<K, T, C> inventoryBase, String styleName, K key, SlotConfig slotConfig) {
        super(inventoryBase, styleName, key);
        this.itemIcon = new GCItemIcon<T>(inventoryBase.getLayer(), slotConfig) {
            @Override
            protected boolean isSelected() {
                //noinspection unchecked
                return inventoryBase.isSelected((C)GCInventorySlotComponent.this);
            }

            @Override
            protected String getEmptyFrameIconStyle() {
                return GCInventorySlotComponent.this.getEmptyFrameIconStyle();
            }

            @Override
            protected String getEmptySpriteKey() {
                return GCInventorySlotComponent.this.getEmptySpriteKey();
            }

            @Override
            protected String getFrameStyle() {
                return GCInventorySlotComponent.this.getFrameStyle();
            }

            @Override
            protected int getFrameBaseIconSize() {
                return GCInventorySlotComponent.this.getFrameBaseIconSize();
            }
        };
        iconSize = DEFAULT_ICON_SIZE;
        center();
        defaults().center();
        itemIconCell = add(itemIcon);
        itemIconCell.size(iconSize);
    }

    public void setIconSize(float iconSize) {
        this.iconSize = iconSize;
        itemIconCell.size(iconSize);
    }

    @Override
    public float getPrefWidth() {
        return iconSize;
    }

    @Override
    public float getPrefHeight() {
        return iconSize;
    }

    @Override
    public void setItem(T item) {
        super.setItem(item);
        itemIcon.setItem(item);
    }

    @Override
    public void onDragStart() {
        itemIcon.setColor(Color.GRAY);
    }

    @Override
    public void onDragStop() {
        itemIcon.setColor(Color.WHITE);
    }

    @Override
    public void invalidate() {
        super.invalidate();
        if(itemIcon != null) {
            itemIcon.invalidate();
        }
    }

    public String getEmptySpriteKey() {
        return "images/icons/base/bg_empty.png";
    }

    public String getEmptyFrameIconStyle() {
        return "icon-item_frame";
    }

    public String getFrameStyle() {
        return "icon-item_frame";
    }

    public int getFrameBaseIconSize() {
        return GCItemIcon.BASE_FRAME_ICON_SIZE;
    }
}
