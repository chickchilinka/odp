package com.universeprojects.gamecomponents.client;

import java.util.ArrayList;
import java.util.List;

public abstract class GameComponents {
    public static List<String> systemMessages = new ArrayList<>();

    public static void addSystemMessage(String message) {
        systemMessages.add(message);
    }
}
