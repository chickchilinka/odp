package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCEnergyNodeListItem.GCEnergyNodeItemActionHandler;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.GenericEvent;

import java.util.LinkedHashMap;
import java.util.Map;

class GCEnergyNodeList extends H5EContainer {

    private H5ELabel attachedLabel;
    private GCList<GCEnergyNodeListItem> list;

    private final GCEnergyNodeItemActionHandler actionHandler;
    private final Map<String, String> nameByUid = new LinkedHashMap<>();

    public final GenericEvent.GenericEvent0Args onContentUpdate = new GenericEvent.GenericEvent0Args();

    private final int WIDTH;
    private final static int ITEM_H = 25;
    private final static int ITEM_SP = 5;

    public void attachLabel(H5ELabel label) {
        if (attachedLabel != null) {
            detachLabel();
        }
        attachedLabel = label;
    }

    public void detachLabel() {
        attachedLabel = null;
    }

    @Deprecated
    GCEnergyNodeList(H5ELayer layer, int width, String caption, GCEnergyNodeItemActionHandler actionHandler) {
        this(layer, width, actionHandler);
    }

    GCEnergyNodeList(H5ELayer layer, int width, GCEnergyNodeItemActionHandler actionHandler) {
        super(layer);
        this.actionHandler = Dev.checkNotNull(actionHandler);

        setWidth(width);
        setHeight(0);

        WIDTH = width;
    }

    void clearList() {
        if (list != null) {
            list.clear();
            list.remove();
            list = null;
            nameByUid.clear();
        }
        if (attachedLabel != null) {
            attachedLabel.setVisible(false);
        }
    }

    void update(Map<String, String> items) {
        Dev.checkNotNull(items);
        if (nameByUid.equals(items)) {
            return; // no change
        }

        clearList();
        if (items.isEmpty()) {
            setVisible(false);
            if (attachedLabel != null) {
                attachedLabel.setVisible(false);
            }
        } else {
            setVisible(true);
            if (attachedLabel != null) {
                attachedLabel.setVisible(true);
            }

            int numItems = items.size();
            final int listH = ITEM_H * numItems + ITEM_SP * (numItems - 1);

            list = new GCList<>(getLayer(), 2, ITEM_SP, ITEM_SP, true);
            add(list);

            for (String uid : items.keySet()) {
                String name = items.get(uid);

                if (Strings.isEmpty(uid)) {
                    throw new IllegalArgumentException("Blank or null UID with name: " + name);
                }
                if (Strings.isEmpty(name)) {
                    throw new IllegalArgumentException("Blank or null name with UID: " + uid);
                }

                list.addItem(new GCEnergyNodeListItem(getLayer(), actionHandler, uid, name));
                nameByUid.put(uid, name);
            }
        }

        onContentUpdate.fire();
    }

}
