package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.gamecomponents.client.auth.AuthCredentials;
import com.universeprojects.gamecomponents.client.auth.PlatformAuthOption;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.vsdata.shared.SpawnOptionData;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class GCLoginManager<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> {

    public static final String CODE_BAD_SERVER_RESPONSE = "bad_response";
    public static final String CODE_COMMUNICATION ="communication_error";
    public static final String CODE_BAD_GAME = "bad_game";
    protected final H5ELayer layer;

    private GCLoginScreen<L> loginScreen;
    private GCServerSelectScreen<L, S> serverSelectScreen;
    private GCCharacterSelectScreen<L, S, C> characterSelectScreen;
    private GCCharacterCreateScreen<L, S, C> characterCreateScreen;
    private GCMobileCharacterCreateScreen<L, S, C> mobileCharacterCreateScreen;
    private GCCharacterRespawnScreen<L, S, C> characterRespawnScreen;
    private GCMobileCharacterRespawnScreen<L, S, C> mobileCharacterRespawnScreen;
    private GCRealmCharacterCreate<L, S, C> realmCharacterCreateScreen;
    private GCTermsOfService termsOfService;
    private GCPrivacyPolicy privacyPolicy;
    private GCRegisterScreen<L> registerScreen;

    protected GCLoginManager(H5ELayer layer) {
        this.layer = layer;
    }

    public void openLoginScreen() {
        if(loginScreen == null) {
            loginScreen = new GCLoginScreen<>(layer, this);
        }
        loginScreen.open();
    }

    public void openServerSelectScreen(L loginInfo) {
        if(serverSelectScreen == null) {
            serverSelectScreen = new GCServerSelectScreen<>(layer, this);
        }
        serverSelectScreen.open(loginInfo);

    }

    public void openCharacterSelectScreen(L loginInfo, S serverInfo) {
        if(characterSelectScreen == null) {
            characterSelectScreen = new GCCharacterSelectScreen<>(layer, this);
        }
        characterSelectScreen.open(loginInfo, serverInfo);


        closeAndCleanupCharacterCreateScreen();
    }


    public void openCharacterCreateScreen(L loginInfo, S serverInfo, NewCharPreselectionInfo preselectionInfo) {
        characterSelectScreen.close();
        if(characterCreateScreen != null) {
            characterCreateScreen.close();
            characterCreateScreen = null;
        }

        if(mobileCharacterCreateScreen != null){
            mobileCharacterCreateScreen.close();
            mobileCharacterCreateScreen = null;
        }

        layer.getEngine().onResize.registerHandler(this::refreshCharacterCreateScreen);
        if(isMobileMode()) {
            openMobileCharacterCreateScreen(loginInfo, serverInfo, preselectionInfo);
        } else {
            openDesktopCharacterCreateScreen(loginInfo, serverInfo, preselectionInfo);
        }
    }

    protected void openMobileCharacterCreateScreen(L loginInfo, S serverInfo, NewCharPreselectionInfo preselectionInfo) {
        if (mobileCharacterCreateScreen == null) {
            mobileCharacterCreateScreen = new GCMobileCharacterCreateScreen<>(layer, this);
            getDefaultSpawnConfigs();
        }
        mobileCharacterCreateScreen.open(loginInfo, serverInfo, preselectionInfo);
    }

    protected void openDesktopCharacterCreateScreen(L loginInfo, S serverInfo, NewCharPreselectionInfo preselectionInfo) {
        if (characterCreateScreen == null) {
            characterCreateScreen = new GCCharacterCreateScreen<>(layer, this);
        }
        characterCreateScreen.open(loginInfo, serverInfo, preselectionInfo);
    }

    public void closeAndCleanupCharacterCreateScreen() {
        if (characterCreateScreen!=null) {
            layer.getEngine().onResize.removeHandler(this::refreshCharacterCreateScreen);
            characterCreateScreen = null;
        }
    }

    private void refreshCharacterCreateScreen() {
        if(characterSelectScreen != null && characterCreateScreen != null){
            characterSelectScreen.onCreateCharacterButtonClick();
        }
    }

    protected boolean isMobileMode() {
        return layer.getEngine().isMobileMode();
    }

    public void openCharacterRespawnScreen(L loginInfo, S serverInfo, C characterInfo) {
        characterSelectScreen.close();
        if(characterRespawnScreen != null) {
            characterRespawnScreen.close();
            characterRespawnScreen = null;
        }

        if(mobileCharacterRespawnScreen != null){
            mobileCharacterRespawnScreen.close();
            mobileCharacterRespawnScreen = null;
        }

        if(isMobileMode()) {
            openMobileCharacterRespawnScreen(loginInfo, serverInfo, characterInfo);
        } else {
            openDesktopCharacterRespawnScreen(loginInfo, serverInfo, characterInfo);
        }
    }

    protected void openMobileCharacterRespawnScreen(L loginInfo, S serverInfo, C characterInfo) {
        if (mobileCharacterRespawnScreen == null) {
            mobileCharacterRespawnScreen = new GCMobileCharacterRespawnScreen<>(layer, this);
        }
        mobileCharacterRespawnScreen.open(loginInfo, serverInfo, characterInfo);
    }

    protected void openDesktopCharacterRespawnScreen(L loginInfo, S serverInfo, C characterInfo) {
        if (characterRespawnScreen == null) {
            characterRespawnScreen = new GCCharacterRespawnScreen<>(layer, this);
            getDefaultSpawnConfigs();
        }
        characterRespawnScreen.open(loginInfo, serverInfo, characterInfo);
    }

    public void openRealmCharacterSelectScreen(L loginInfo) {
        if (realmCharacterCreateScreen == null) {
            realmCharacterCreateScreen = new GCRealmCharacterCreate<>(layer, this);
            getDefaultSpawnConfigs();
        }
        realmCharacterCreateScreen.open(loginInfo, null, null);
    }

    public void openRegisterWindowWithPolicies() {
        openTermsOfServiceWindow(this::openLoginScreen,
                () -> openPrivacyPolicyWindow(this::openLoginScreen, this::openRegisterWindow));
    }

    public void openTermsOfServiceWindow(Callable0Args onRejected, Callable0Args onAccepted) {
        if(termsOfService == null) {
            termsOfService = new GCTermsOfService(layer, this);
        }
        termsOfService.open(onAccepted, onRejected);
    }

    public void openPrivacyPolicyWindow(Callable0Args onRejected, Callable0Args onAccepted) {
        if(privacyPolicy == null) {
            privacyPolicy = new GCPrivacyPolicy(layer, this);
        }
        privacyPolicy.open(onAccepted, onRejected);
    }

    public void openRegisterWindow() {
        if(registerScreen == null) {
            registerScreen = new GCRegisterScreen<>(layer, this);
        }
        registerScreen.open();
    }

    public void onSuccessfulLogin(L loginInfo) {
        openServerSelectScreen(loginInfo);
    }

    public void onServerSelected(L loginInfo, S serverInfo) {
        openCharacterSelectScreen(loginInfo, serverInfo);
    }

    public void checkServerCompatibility(L loginInfo, S serverInfo, Callable0Args continueLoginCallback) {
        continueLoginCallback.call();
    }


    public List<PlatformAuthOption> getPlatformAuthOptions() {
        return Collections.emptyList();
    }

    protected abstract boolean showAdminControls();

    public abstract void startSingleplayer();

    protected abstract L getSavedToken();

    protected abstract void setSavedToken(L token);

    protected abstract void verify(L token, Callable1Args<L> successCallback, Callable1Args<LoginError> errorCallback);

    protected abstract void login(AuthCredentials credentials, Callable1Args<L> successCallback, Callable1Args<LoginError> errorCallback);

    protected abstract void register(String email, String password, String userName, Callable1Args<L> successCallback, Callable1Args<LoginError> errorCallback);

    protected abstract void loadPrivacyPolicy(Callable1Args<String> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadTermsOfService(Callable1Args<String> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadServers(L loginInfo, Callable1Args<List<S>> successCallback, Callable1Args<String> errorCallback);

    public abstract void onServerChosen(S serverInfo, Callable1Args<S> onDone);

    protected abstract void findRealmServer(L loginInfo, Callable2Args<S, Boolean> successCallback, Callable1Args<String> errorCallback);

    protected abstract void pingServer(S serverInfo, Callable0Args successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadGameCharacters(L loginInfo, S serverInfo, Callable1Args<List<C>> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadUserCharacters(L loginInfo, Callable1Args<List<String>> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadUserFriends(L loginInfo, S serverInfo, Callable1Args<Map<String, Map<String, Long>>> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadUserOrganisations(L loginInfo, S serverInfo, Callable1Args<Map<Long, String>> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadRespawnOptions(L loginInfo, S serverInfo, C characterInfo, Callable1Args<List<SpawnOptionData>> successCallback, Callable1Args<String> errorCallback);

    protected abstract void deleteCharacter(L loginInfo, S serverInfo, C characterInfo, Callable0Args successCallback, Callable1Args<String> errorCallback);

    protected abstract void joinWithNewCharacter(L loginInfo, S serverInfo, String characterName, String spawnType, JSONObject spawnParams, Callable0Args successCallback, Callable1Args<String> errorCallback);

    protected abstract void joinWithExistingCharacter(L loginInfo, S serverInfo, C characterInfo, Callable0Args successCallback, Callable1Args<String> errorCallback);

    protected abstract void joinWithRealmCharacter(L loginInfo, S serverInfo, String characterName, String spawnType, JSONObject spawnParams, Callable0Args successCallback, Callable1Args<String> errorCallback);

    public abstract void respawn(L loginInfo, S serverInfo, C characterInfo, String spawnType, JSONObject spawnParams, Callable0Args successCallback, Callable1Args<String> errorCallback);

    protected abstract GCSpawnSelectDialog.GCSpawnSelectData getDefaultSpawnConfigs();

    public abstract void backToCharacterSelection();
}
