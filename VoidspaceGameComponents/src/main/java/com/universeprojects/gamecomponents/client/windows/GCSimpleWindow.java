package com.universeprojects.gamecomponents.client.windows;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

/**
 * The purpose of this component is to simplify the creation of UI windows in the game,
 * as well as reduce the amount of boilerplate code, via the following features:
 * <p>
 * - reusable declaration of commonly-used elements
 * - relative positioning of elements
 * - element alignment in relation to the window
 * - maintaining the window dimensions based on its contents
 */
public class GCSimpleWindow extends GCWindow {

    public static final String GENERIC_BUTTON_SMALL_KEY = "images/GUI/blue/generic-button-small.9.png";
    public static final String GENERIC_BUTTON_MEDIUM_KEY = "images/GUI/blue/generic-button-medium.9.png";
    public static final String GENERIC_BUTTON_LARGE_KEY = "images/GUI/blue/generic-button-large.png";

    private final Logger log = Logger.getLogger(GCSimpleWindow.class);

    /**
     * Constructs a window with variable dimensions.
     * In this case, the boundaries will stretch as content is added to the window.
     */
    public GCSimpleWindow(H5ELayer layer, String windowId, String title) {
        this(layer, windowId, title, null, null, true);
    }

    /**
     * Constructs a window with fixed dimensions.
     * In this case, content may overflow the window boundaries.
     */
    public GCSimpleWindow(H5ELayer layer, String windowId, String title, int width, int height) {
        this(layer, windowId, title, width, height, true);
    }

    /**
     * Constructs a window with the specified parameters
     *
     * @param layer         The layer to create the window in
     * @param windowId      The ID of the window (useful for debugging purposes only)
     * @param title         The window title, that's visible to the user
     * @param minimumWidth  The initial width of the window, or null for default setting
     * @param minimumHeight The initial height of the window, or null for default setting
     */
    public GCSimpleWindow(H5ELayer layer, String windowId, String title, Integer minimumWidth, Integer minimumHeight, boolean autosizeWindow) {
        this(layer, windowId, title, minimumWidth, minimumHeight, autosizeWindow, "default");
    }

    public GCSimpleWindow(H5ELayer layer, String windowId, String title, Integer minimumWidth, Integer minimumHeight, boolean autosizeWindow, String styleName) {
        super(layer, minimumWidth, minimumHeight, styleName);

        log.debug("Creating window with ID " + Strings.inQuotes(windowId));
        if (Strings.isEmpty(windowId)) {
            throw new IllegalArgumentException("Window ID can't be empty");
        }
        if (Strings.isEmpty(title)) {
            title = "";
        }

        setId(windowId);
        setTitle(title.toUpperCase());
        setPackOnOpen(autosizeWindow);
        defaults().left();
    }

    @Deprecated
    public Cell<H5ELabel> addTextArea(String text, int width, Position position, Alignment alignment, int x, int y) {
        H5ELabel element = new H5ELabel(getLayer());
        element.setText(text);
        element.setAlignment(alignment.getValue());
        return this.add(element);
    }

    /**
     * This internal routine is called for every new element that's added into the window.
     */
    private <T extends Actor & GraphicElement> T processNewElement(T element, Position position, Alignment alignment, float offsetX, float offsetY, boolean growX) {
        if (element == null) {
            throw new IllegalArgumentException("New element reference can't be null");
        }
        if (position == null) {
            position = Position.WINDOW;
        }
        if (alignment == null) {
            alignment = Alignment.LEFT;
        }

        // Add the element to the window content pane
        if(position == Position.NEW_LINE) {
            row();
        }
        Cell<T> cell = add(element);

        cell.align(alignment.getValue());

        // Add the element to a cell, if applicable
        cell.padLeft(offsetX);
        cell.padTop(offsetY);
        if (growX) {
            cell.growX();
        }

        return element;
    }

    /**
     * Disables the window's close button in the top bar (enabled by default)
     */
    public void disableCloseButton() {
        setCloseButtonEnabled(false);
    }


    /**
     * Alternates between "open" and "closed" states
     */
    public void toggle() {
        if (isOpen()) close();
        else open();
    }

    //////////////////////////////////////////////////////////////////////
    // PUBLIC INTERFACE - ELEMENT CREATION
    //

    /**
     * Adds a line break of the specified height, overriding the default line break size
     * The requested value must be greater than zero
     */
    public void addEmptyLine(int height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Custom line break height must be greater than zero: " + height);
        }
        row().padTop(height);
        add();
        row();
    }

    /**
     * Modifies the x/y offset settings of a previously-added element
     */
    @Deprecated
    public <T extends Actor & GraphicElement> void updateElementOffset(T element, float offsetX, float offsetY) {
        //noinspection unchecked
        Cell<T> cell = getCell(element);
        if (cell == null) {
            throw new IllegalStateException("Can't update element settings, because the element is not registered");
        }
        
        cell.padLeft(offsetX);
        cell.padTop(offsetY);
    }

    @Deprecated
    public H5ELabel addH1(String caption, Position position, Alignment align, float offsetX, float offsetY) {
        H5ELabel element = new H5ELabel(getLayer());
        element.setFontScale(1.2f);
        element.setText(caption);

        return processNewElement(element, position, align, offsetX, offsetY, false);
    }

    public Cell<H5ELabel> addH1(String caption) {
        H5ELabel element = new H5ELabel(getLayer());
        element.setStyle(getLayer().getEngine().getSkin().get("label-bold", Label.LabelStyle.class));
        element.setText(caption);
        return add(element);
    }

    @Deprecated
    public H5ELabel addH2(String caption, Position position, Alignment align, float offsetX, float offsetY) {
        H5ELabel element = new H5ELabel(getLayer());
        element.setFontScale(1);
        element.setText(caption);

        return processNewElement(element, position, align, offsetX, offsetY, false);
    }

    public Cell<H5ELabel> addH2(String caption) {
        H5ELabel element = new H5ELabel(getLayer());
        element.setFontScale(1);
        element.setText(caption);
        return add(element);
    }

    @Deprecated
    public H5ELabel addLabel(String caption, Position position, Alignment align, float offsetX, float offsetY) {
        H5ELabel element = new H5ELabel(getLayer());
        element.setText(caption);

        return processNewElement(element, position, align, offsetX, offsetY, false);
    }

    public Cell<H5ELabel> addLabel(String caption) {
        H5ELabel element = new H5ELabel(getLayer());
        element.setText(caption);
        return this.add(element);
    }

    @Deprecated
    public H5EInputBox addInputBox(int width, Position position, Alignment align, float offsetX, float offsetY) {
        H5EInputBox element = new H5EInputBox(getLayer());
        element.setWidth(width);

        return processNewElement(element, position, align, offsetX, offsetY, false);
    }

    public Cell<H5EInputBox> addInputBox(int width) {
        H5EInputBox element = new H5EInputBox(getLayer());
        element.setWidth(width);
        return add(element).minWidth(width);
    }


//    public H5ETextArea addTextArea(String text, int width, Position position, Alignment align, int offsetX, int offsetY) {
//        H5ETextArea element = new H5ETextArea(getLayer());
//        element.setWidth(width);
//        element.setText(text);
//        element.setAutoCalculateHeight(true);
//        element.stabilize();
//
//        return processNewElement(element, position, align, offsetX, offsetY);
//    }
//
//    public H5ETextArea addScrollableTextArea(String text, int width, int height, int maxRawLines, Position position, Alignment align, int offsetX, int offsetY) {
//        H5ETextArea element = new H5ETextArea(getLayer(), "images/btnUpArrow1.png", "images/btnUpEndArrow1.png", maxRawLines);
//        element.setWidth(width);
//        element.setHeight(height);
//        element.setAutoCalculateHeight(false);
//        element.setText(text);
//        element.stabilize();
//
//        return processNewElement(element, position, align, offsetX, offsetY);
//    }

    @Deprecated
    public H5EButton addLargeButton(String caption, Position position, Alignment align, int offsetX, int offsetY) {
        H5EButton button = H5EButton.createSpriteBacked(getLayer(), GENERIC_BUTTON_LARGE_KEY, caption);
        return processNewElement(button, position, align, offsetX, offsetY, true);
    }

    @Deprecated
    public H5EButton addMediumButton(String caption, Position position, Alignment align, int offsetX, int offsetY) {
        H5EButton button = H5EButton.createSpriteBacked(getLayer(), GENERIC_BUTTON_MEDIUM_KEY, caption);
        return processNewElement(button, position, align, offsetX, offsetY, false);
    }

    @Deprecated
    public H5EButton addSmallButton(String caption, Position position, Alignment align, int offsetX, int offsetY) {
        H5EButton button = H5EButton.createSpriteBacked(getLayer(), GENERIC_BUTTON_SMALL_KEY, caption);
        return processNewElement(button, position, align, offsetX, offsetY, false);
    }

    public Cell<H5EButton> addLargeButton(String caption) {
        return addMediumButton(caption);
    }

    public Cell<H5EButton> addMediumButton(String caption) {
        H5EButton button = new H5EButton(caption, getLayer());
        return add(button);
    }

    public Cell<H5EButton> addSmallButton(String caption) {
        H5EButton button = new H5EButton(caption, getLayer());
        return add(button);
    }


    @Deprecated
    public <T extends Actor & GraphicElement> T addElement(T element, Position position, Alignment align, int offsetX, int offsetY, boolean growX) {
        return processNewElement(element, position, align, offsetX, offsetY, growX);
    }

    @Deprecated
    public <T extends Actor & GraphicElement> T addElement(T element, Position position, Alignment align, int offsetX, int offsetY) {
        return addElement(element, position, align, offsetX, offsetY, false);
    }

//    public <T extends H5ECompositeElement> T addElement(T element, Position position, Alignment align, int offsetX, int offsetY) {
//        processNewElement(element.getRootElement(), position, align, offsetX, offsetY);
//        element.onContentUpdate.registerHandler(eventHandler);
//        return element;
//    }
//
//    public <T extends H5EGraphicElement> void setVisibilityCondition(T element, RecursiveVisibilityCondition condition) {
//        window.setVisibilityCondition(element, condition);
//    }

}
