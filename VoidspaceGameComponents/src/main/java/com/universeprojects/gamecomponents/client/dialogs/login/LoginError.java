package com.universeprojects.gamecomponents.client.dialogs.login;

public class LoginError extends Throwable {
    public LoginError() {
        this(null, null);
    }

    public LoginError(String message) {
        this(message, null);
    }

    public LoginError(Throwable cause) {
        this(null, cause);
    }

    public LoginError(String message, Throwable cause) {
        super(message, cause, true, false);
    }
}
