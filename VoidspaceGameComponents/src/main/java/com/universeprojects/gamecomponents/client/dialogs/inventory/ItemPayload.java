package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class ItemPayload<K, T extends GCInventoryItem, C extends GCInventorySlotBaseComponent<K, T, C>> extends DragAndDrop.Payload {

    public static final int ICON_SIZE = 50;
    public static final Color COLOR_INVALID = new Color(0.5f, 0f, 0f, 1f);
    public static final Color COLOR_VALID = new Color(0.6f, 1f, 0.6f, 1f);
    private final T item;
    private final C sourceComponent;

    public ItemPayload(C sourceComponent) {
        this.sourceComponent = sourceComponent;
        this.item = sourceComponent.getItem();
        setObject(item);
        final H5ELayer layer = sourceComponent.getLayer();

        final DragAndDrop dragAndDrop = InventoryManager.getInstance(layer.getEngine()).getDragAndDrop();
        dragAndDrop.setDragActorPosition(ICON_SIZE / 2f, -ICON_SIZE / 2f);

        GCItemIcon<T> dragIcon = new GCItemIcon<>(layer);
        dragIcon.setItem(item);
        dragIcon.setSize(ICON_SIZE, ICON_SIZE);
        setDragActor(dragIcon);
        dragIcon.setStage(null);//Necessary for DragAndDrop to work

        GCItemIcon<T> invalidDragIcon = new GCItemIcon<>(layer);
        invalidDragIcon.setItem(item);
        invalidDragIcon.setColor(COLOR_INVALID);
        invalidDragIcon.setSize(ICON_SIZE, ICON_SIZE);
        setInvalidDragActor(invalidDragIcon);
        invalidDragIcon.setStage(null);//Necessary for DragAndDrop to work

        GCItemIcon<T> validDragIcon = new GCItemIcon<>(layer);
        validDragIcon.setItem(item);
        validDragIcon.setColor(COLOR_VALID);
        validDragIcon.setSize(ICON_SIZE, ICON_SIZE);
        setValidDragActor(validDragIcon);
        validDragIcon.setStage(null);//Necessary for DragAndDrop to work
    }

    public C getSourceComponent() {
        return sourceComponent;
    }

    @Override
    public T getObject() {
        return item;
    }
}
