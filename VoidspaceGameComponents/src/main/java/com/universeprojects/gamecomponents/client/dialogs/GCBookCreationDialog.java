package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.BookChapterData;
import com.universeprojects.vsdata.shared.BookData;

public class GCBookCreationDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 500;
    private final static int WINDOW_H = 600;
    private final H5EStack titleStack;
    private final H5EInputBox titleBox;
    private final H5ELabel titleLabel;
    private final Cell<?> chapterCell;
    private final H5EStack chapterStack;
    private final H5EInputBox chapterBox;
    private final H5ELabel chapterLabel;
    private final GCMediaTextArea chapterEditArea;
    private final H5EButton previousChapterBtn;
    private final H5EInputBox chapterNumberBox;
    private final H5EButton nextChapterBtn;
    private final H5EButton doneBtn;
    private final H5EScrollablePane scrollablePane;
    private final Table buttonTable;
    private final GCLoadingIndicator loadingIndicator;
    private BookData bookData;
    private int curChapter = 1;
    private final Task taskChapterNumber;
    private final Timer timer;

    public GCBookCreationDialog(H5ELayer layer, String windowTitle, Callable1Args<BookData> onSubmit) {
        this(layer, windowTitle);
        newBook();
        setChapter(1);
        doneBtn.addButtonListener(() -> {
            collectChapterContent();
            removeEmptyChapters();
            bookData.name = titleBox.getText();
            close();
            onSubmit.call(bookData);
        });
    }

    public GCBookCreationDialog(H5ELayer layer, String windowTitle) {
        super(layer, "bookCreationDialog", windowTitle, WINDOW_W, WINDOW_H);
        positionProportionally(0.65f, 0.65f);
        setFullscreenOnMobile(true);
        setPackOnOpen(false);

        //<editor-fold desc="Input boxes and area">

        titleLabel = new H5ELabel("(No book title)", layer);
        titleLabel.setAlignment(Align.center);
        titleStack = new H5EStack();
        titleStack.add(titleLabel);
        titleBox = new H5EInputBox(layer);
        titleBox.setAlignment(Align.center);
        add(titleStack).expandX().fillX().center();

        row();

        chapterStack = new H5EStack();
        chapterLabel = new H5ELabel("(No chapter title)", layer);
        chapterLabel.setAlignment(Align.center);
        chapterLabel.setColor(Color.valueOf("#CCCCCC"));
        chapterStack.add(chapterLabel);
        chapterBox = new H5EInputBox(layer);
        chapterCell = add(chapterStack).expandX().fillX().center();
        chapterBox.setAlignment(Align.center);

        row();

        scrollablePane = new H5EScrollablePane(layer);
        scrollablePane.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        scrollablePane.setScrollingDisabled(true, false);
        scrollablePane.setOverscroll(false, true);
        scrollablePane.setForceScroll(false, true);
        chapterEditArea = new GCMediaTextArea(layer);
        chapterEditArea.setReadOnlyMode(false);
        TextFieldStyle areaStyle = new TextFieldStyle(chapterEditArea.getTextArea().getStyle());
        areaStyle.background = null;
        chapterEditArea.getTextArea().setStyle(areaStyle);
        titleLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (bookData.editable) {
                    chapterStack.removeActor(chapterBox);
                    chapterBox.unfocus();
                    chapterLabel.setText(chapterBox.getText().equals("") ? "(No chapter title)" : chapterBox.getText());
                    chapterStack.add(chapterLabel);
                    titleStack.removeActor(titleLabel);
                    titleStack.add(titleBox);
                    chapterEditArea.getTextArea().unfocus();
                    titleBox.focus();
                    event.handle();
                }
            }
        });

        chapterLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (bookData.editable) {
                    titleStack.removeActor(titleBox);
                    titleBox.unfocus();
                    titleLabel.setText(titleBox.getText().equals("") ? "(No title)" : titleBox.getText());
                    titleStack.add(titleLabel);
                    chapterStack.removeActor(chapterLabel);
                    chapterStack.add(chapterBox);
                    chapterEditArea.getTextArea().unfocus();
                    chapterBox.focus();
                    event.handle();
                }
            }
        });

        chapterEditArea.getTextArea().addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                super.keyboardFocusChanged(event, actor, focused);
                if (bookData.editable) {
                    if (titleStack.getChildren().contains(titleBox, true)) {
                        titleStack.removeActor(titleBox);
                        titleBox.unfocus();
                        titleLabel.setText(titleBox.getText().equals("") ? "(No title)" : titleBox.getText());
                        titleStack.add(titleLabel);
                    }
                    if (chapterStack.getChildren().contains(chapterBox, true)) {
                        chapterStack.removeActor(chapterBox);
                        chapterBox.unfocus();
                        chapterLabel.setText(chapterBox.getText().equals("") ? "(No chapter title)" : chapterBox.getText());
                        chapterStack.add(chapterLabel);
                    }
                }
                event.handle();
            }
        });

        chapterEditArea.getTextArea().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                setAreaHeight();
            }
        });

        scrollablePane.add(chapterEditArea).grow();
        add(scrollablePane).padLeft(5).padRight(5).expand().fill().left();

        //</editor-fold>

        row();

        //<editor-fold desc="Chapter control">
        nextChapterBtn = new H5EButton("Next", layer);
        previousChapterBtn = new H5EButton("Previous", layer);
        doneBtn = new H5EButton("Done", layer);
        previousChapterBtn.addButtonListener(this::previousChapter);
        nextChapterBtn.addButtonListener(this::nextChapter);
        chapterNumberBox = new H5EInputBox(layer);
        chapterNumberBox.setAlignment(Align.center);
        timer = new Timer();
        taskChapterNumber = new Task() {
            @Override
            public void run() {
                try {
                    setChapter(Integer.parseInt(chapterNumberBox.getText()));
                } catch (NumberFormatException ignored) {
                }
            }
        };
        chapterNumberBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                timer.clear();
                timer.scheduleTask(taskChapterNumber, 1f);
                event.handle();
            }
        });
        chapterNumberBox.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (titleStack.getChildren().contains(titleBox, true)) {
                    titleStack.removeActor(titleBox);
                    titleBox.unfocus();
                    titleLabel.setText(titleBox.getText().equals("") ? "(No title)" : titleBox.getText());
                    titleStack.add(titleLabel);
                }
                if (chapterStack.getChildren().contains(chapterBox, true)) {
                    chapterStack.removeActor(chapterBox);
                    chapterBox.unfocus();
                    chapterLabel.setText(chapterBox.getText().equals("") ? "(No chapter title)" : chapterBox.getText());
                    chapterStack.add(chapterLabel);
                }
                if (focused) {
                    chapterNumberBox.setText("");
                    chapterNumberBox.setTypeNumber();
                } else {
                    updChapterNumberBox();
                }
                super.keyboardFocusChanged(event, actor, focused);
            }
        });
        chapterNumberBox.setText("1/1");
        row();

        buttonTable = new Table();
        buttonTable.add().width(buttonTable.getWidth() / 3).growX();
        H5ELabel chap = new H5ELabel("Chapter", layer);
        chap.setAlignment(Align.center);
        buttonTable.add(chap).center().padTop(10).width(buttonTable.getWidth() / 3).growX();
        buttonTable.row();
        buttonTable.pad(5);

        buttonTable.add(previousChapterBtn).left().width(buttonTable.getWidth() / 3).growX();
        buttonTable.add(chapterNumberBox).left().padLeft(5).padRight(5).width(buttonTable.getWidth() / 3).growX();
        buttonTable.add(nextChapterBtn).left().width(buttonTable.getWidth() / 3).growX();
        buttonTable.row();


        buttonTable.add().left().width(buttonTable.getWidth() / 3).growX();
        buttonTable.add().left().width(buttonTable.getWidth() / 3).growX();
        buttonTable.add(doneBtn).left().width(buttonTable.getWidth() / 3).growX();
        add(buttonTable).padLeft(5).padRight(5).right().growX();
        loadingIndicator = new GCLoadingIndicator(layer);
        loadingIndicator.activate(this);
        //</editor-fold>
    }

    protected void setAreaHeight() {
        chapterEditArea.getTextArea().setPrefRows(chapterEditArea.getTextArea().getLines() + 3);
        scrollablePane.setContentHeight((int) chapterEditArea.getTextArea().getPrefHeight());
        chapterEditArea.invalidate();
        scrollablePane.layout();
        scrollablePane.setScrollY(-chapterEditArea.getTextArea().getCursorY() - scrollablePane.getHeight() / 2);
    }

    public void processBook(BookData data) {
        loadingIndicator.deactivate();
        bookData = data;
        if (!bookData.editable) {
            if (data.listOfChapters.size() == 1) {
                chapterCell.clearActor();
                removeActor(buttonTable);
            } else {
                if (getCell(buttonTable) == null) {
                    chapterCell.setActor(chapterStack).expandX().fillX().center();
                    row();
                    add(buttonTable).padLeft(5).padRight(5).right().growX();
                }
            }
        }
        curChapter = 1;
        titleLabel.setText(bookData.name);
        titleBox.setText(bookData.name);
        chapterLabel.setText(bookData.listOfChapters.get(curChapter - 1).name);
        chapterEditArea.processContent(bookData.listOfChapters.get(curChapter - 1).content);
        chapterBox.setText(bookData.listOfChapters.get(curChapter - 1).name);
        chapterEditArea.getTextArea().setMessageText("Enter chapter text");
        scheduleAreaHeight();
        updChapterNumberBox();
        chapterEditArea.setReadOnlyMode(!bookData.editable);
        if (bookData.editable) {
            if (buttonTable.getCell(doneBtn) == null) {
                buttonTable.add(doneBtn);
            }
        } else {
            buttonTable.removeActor(doneBtn);
        }
    }

    public void hideLoadingIndicator() {
        loadingIndicator.deactivate();
    }

    @Override
    public void open(boolean pack){
        super.open(pack);
        clearForm();
        loadingIndicator.activate(this);
    }

    public void newBook() {
        curChapter = 1;
        bookData = new BookData();
        bookData.listOfChapters.add(new BookChapterData("Chapter #1", ""));
        titleLabel.setText("(No title)");
        titleBox.setText("(No title)");
        chapterLabel.setText("Chapter #1");
        chapterBox.setText("Chapter #1");
        chapterEditArea.getTextArea().setMessageText("Enter chapter text");
        chapterEditArea.processContent("");
        scheduleAreaHeight();
        updChapterNumberBox();
    }

    private void removeEmptyChapters() {
        for (int i = 0; i < bookData.listOfChapters.size(); i++) {
            if (bookData.listOfChapters.get(i).content.equals("")) {
                bookData.listOfChapters.remove(i);
                i--;
            }
        }
    }

    private void collectChapterContent() {
        bookData.listOfChapters.get(curChapter - 1).name = chapterBox.getText();
        bookData.listOfChapters.get(curChapter - 1).content = chapterEditArea.getText();
    }

    private void nextChapter() {
        if (bookData.editable && curChapter == bookData.listOfChapters.size() && !chapterEditArea.getText().equals("")) {
            bookData.listOfChapters.add(new BookChapterData("Chapter #" + (curChapter + 1), ""));
        }
        setChapter(curChapter + 1);
        updChapterNumberBox();
    }

    private void updChapterNumberBox() {
        chapterNumberBox.setTypeText();
        chapterNumberBox.setText(curChapter + "/" + bookData.listOfChapters.size());
    }

    private void setChapter(int chapter) {
        collectChapterContent();
        if (chapter > bookData.listOfChapters.size()) {
            curChapter = bookData.listOfChapters.size();
        } else if (chapter < 1) {
            curChapter = 1;
        } else {
            curChapter = chapter;
        }
        chapterLabel.setText(bookData.listOfChapters.get(curChapter - 1).name);
        chapterEditArea.getTextArea().setText(bookData.listOfChapters.get(curChapter - 1).content);
        chapterBox.setText(bookData.listOfChapters.get(curChapter - 1).name);
        scheduleAreaHeight();
    }

    private void previousChapter() {
        if (curChapter > 1) {
            setChapter(curChapter - 1);
            updChapterNumberBox();
        }
    }

    public void clearForm() {
        titleLabel.setText("");
        titleBox.setText("");
        chapterLabel.setText("");
        chapterBox.setText("");
        chapterEditArea.clearContent();
        chapterEditArea.getTextArea().setMessageText("");
    }

    private void scheduleAreaHeight() {
        Timer.schedule(new Task() {
            @Override
            public void run() {
                setAreaHeight();
            }
        }, 0.01f);
    }
}