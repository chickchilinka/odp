package com.universeprojects.gamecomponents.client.elements.actionbar.dnd;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarController;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarType;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCOperation;

public class GCActionBarDndTarget extends DragAndDrop.Target {
    private final GCActionBarController controller;
    private final GCActionBarType type;
    private final int index;
    private int row;

    public GCActionBarDndTarget(Actor actor, GCActionBarController controller, GCActionBarType type, int row, int index) {
        super(actor);
        this.controller = controller;
        this.type = type;
        this.row = row;
        this.index = index;
    }

    @Override
    public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        return payload instanceof GCActionBarDndPayload;
    }

    @Override
    public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        if (!(payload instanceof GCActionBarDndPayload)) return;

        GCActionBarDndPayload dndPayload = (GCActionBarDndPayload) payload;

        boolean fromActionBar = source instanceof GCActionBarDndSource;

        if (fromActionBar) {
            GCActionBarDndSource dndSource = (GCActionBarDndSource) source;
            controller.setAction(dndSource.getType(), dndSource.getRow(), dndSource.getIndex(), null, false);
        }

        // If source is action bar, we just "move" the action from one slot
        // else we create a copy of this action (to apply all the necessary changes to target set name)
        GCOperation operation = fromActionBar ? dndPayload.getAction() : dndPayload.getAction().getActionBarCopy();
        controller.setAction(type, row, index, operation, true);
    }

    public void setRow(int row) {
        this.row = row;
    }
}
