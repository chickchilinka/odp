package com.universeprojects.gamecomponents.client.tutorial;

import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialStage;

import java.util.List;

public interface TutorialConfig {
    List<TutorialStage> getTutorialStages();
    void buildTutorialStages(String root);

    void addTutorialElement(String tutorialId, Object data);
}
