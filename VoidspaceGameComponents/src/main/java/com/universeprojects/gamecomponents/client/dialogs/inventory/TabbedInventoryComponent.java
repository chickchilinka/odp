package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TabbedInventoryComponent extends Table {
    private final H5ELayer layer;
    private final HorizontalGroup btnBar;
    private final ButtonGroup<H5EButton> btnGroup;
    private final Stack stack;
    private final List<TabButton> btnArray;
    private GCInventory<?> activeInventory;
    private final Map<GCInventory<?>, H5EButton> buttonsByInventory;

    public TabbedInventoryComponent(H5ELayer layer) {
        this.layer = layer;
        setStage(layer);
        btnBar = new HorizontalGroup();
        btnGroup = new ButtonGroup<>();
        btnArray = new ArrayList<>();
        stack = new Stack();
        buttonsByInventory = new HashMap<>();

        this.add(btnBar).left();
        this.row();
        this.add(stack).grow();
    }

    public GCInventory<?> getActiveInventory () {
        return activeInventory;
    }

    public H5EButton getButton(GCInventory<?> inventory) {
        return buttonsByInventory.get(inventory);
    }

    // client use, for adding a button to the tabbed component
    public void add (String image, GCInventory<?> inventory) {
        H5EButton btn = this.addButton(image);
        buttonsByInventory.put(inventory, btn);

        final DragAndDrop dragAndDrop = InventoryManager.getInstance(inventory.getEngine()).getDragAndDrop();
        final TabButtonTarget btnTarget = new TabButtonTarget(btn, inventory);
        dragAndDrop.addTarget(btnTarget);

        // Start with first button and its inventory displayed
        if (btnArray.size() == 0) {
            TabButton firstTabBtn = new TabButton(btn, image, inventory);
            btnArray.add(firstTabBtn);
            inventory.setVisible(true);
            stack.add(inventory);
            activeInventory = inventory;
        } else {
            TabButton curTabBtn = new TabButton(btn, image, inventory);
            btnArray.add(curTabBtn);
            stack.add(inventory);
        }

        btn.addCheckedButtonListener(() -> {
            activeInventory.setVisible(false);
            // deselect any selected items in the old inventory when switching tabs into a new one
            activeInventory.select(null);
            inventory.setVisible(true);
            activeInventory = inventory;
        });
    }

    // Sets the default inventory to be the inventory associated with the TabButton in btnArray at index
    public void setDefaultInventory (int index) {
        setAllInvisible();
        if(index >= btnArray.size()) {
            return;
        }
        TabButton defaultTabBtn = btnArray.get(index);
        defaultTabBtn.btn.toggle();
        defaultTabBtn.btnInventory.setVisible(true);
        activeInventory = defaultTabBtn.btnInventory;
    }

    // add button to a horizontal group
    private H5EButton addButton (String image) {
        H5EButton btn = ButtonBuilder.inLayer(layer)
                .withStyle("button2-tab")
                .withForegroundSpriteType(image)
                .build();

        final int TAB_BUTTON_WIDTH  = 30;
        final int TAB_BUTTON_HEIGHT = 25;

        btnGroup.add(btn);
        btnBar.addActor(btn);
        btn.getImageCell()
                .width(TAB_BUTTON_WIDTH)
                .height(TAB_BUTTON_HEIGHT)
                .align(Align.center);

        return btn;
    }

    private void setAllInvisible () {
        for (TabButton tabButton : btnArray) {
            tabButton.btnInventory.setVisible(false);
        }
    }

    /**
     * Clears the fields of the TabbedInventoryComponent
     */
    public void clearParts () {
        btnBar.clear();
        btnGroup.clear();
        stack.clearChildren();
        btnArray.clear();
        buttonsByInventory.clear();
    }

    private static class TabButton {
        protected H5EButton btn;
        protected String imageName;
        protected GCInventory<?> btnInventory;

        TabButton (H5EButton btn, String imageName, GCInventory<?> btnInventory) {
            this.btn = btn;
            this.imageName = imageName;
            this.btnInventory = btnInventory;
        }
    }
}