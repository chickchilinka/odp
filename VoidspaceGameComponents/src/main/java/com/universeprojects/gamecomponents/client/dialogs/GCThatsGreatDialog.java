package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCThatsGreatDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 350;
    private final static int WINDOW_H = 350;
    private final H5ELabel text;

    public GCThatsGreatDialog(H5ELayer layer, String displayStoreName, Callable0Args onSubmit, Callable0Args onCancel) {
        super(layer, "thatsGreatDialog", "That's great!", WINDOW_W, WINDOW_H, false);
        positionProportionally(0.5f, 0.5f);
        setModal(true);

        text = new H5ELabel("Wow, that's really great! Would you also consider rating us on " + displayStoreName
                + "? It would really help our development efforts.", layer);
        LabelStyle labelStyle = new LabelStyle(text.getStyle());
        labelStyle.background = StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque;
        text.setWrap(true);
        text.setStyle(labelStyle);
        add(text).left().top().growX().pad(5);
        row();
        H5EButton submit = new H5EButton("Sure!", layer);
        H5EButton cancel = new H5EButton("Close", layer);
        cancel.addButtonListener(() -> {
            onCancel.call();
            close();
        });
        submit.addButtonListener(() -> {
            onSubmit.call();
            close();
        });
        row();
        Table buttonRow = new Table();
        buttonRow.pad(15,10,5,10);
        buttonRow.add(cancel).left();
        buttonRow.add().growX();
        buttonRow.add(submit).right();
        add(buttonRow).right().growX();
    }
}