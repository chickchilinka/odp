package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarController;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndPayload;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndSource;

/**
 * This class allows items to be jettisoned into space
 * and action bar actions to be removed
 */
public class JettisonComponent<K, T extends GCInventoryItem, C extends GCInventorySlotBaseComponent<K, T, C>> extends DragAndDrop.Target {
    private final GCActionBarController controller;

	public JettisonComponent(Actor actor, GCActionBarController controller) {
		super(actor);
        this.controller = controller;
    }

	/**
	 * Determine whether an item can be jettisoned
	 * @param source  The inventory that the item is being dragged from
	 * @param payload The item that is being dragged
	 * @return Whether the item can be jettisoned
	 */
	@Override
	public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        if (payload instanceof GCActionBarDndPayload && source instanceof GCActionBarDndSource) {
            return true;
        } else if (payload instanceof ItemPayload) {
            ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
            ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;
            return canJettison(componentSource, itemPayload);
        } else return false;
	}

	/**
	 * Jettison an item
	 * @param source  The inventory that the item is being dragged from
	 * @param payload The item that is being dragged
	 */
	@Override
	public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        if (payload instanceof GCActionBarDndPayload && source instanceof GCActionBarDndSource) {
            GCActionBarDndSource dndSource = (GCActionBarDndSource) source;
            controller.setAction(dndSource.getType(), dndSource.getRow(), dndSource.getIndex(), null, true);
        } else if (payload instanceof ItemPayload) {
            ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
            ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;
            jettison(componentSource, itemPayload);
        }
	}

	/**
	 * Jettison an item
	 * @param componentSourceUntyped  The inventory that the item is being dragged from
	 * @param itemPayloadUntyped      The item that is being dragged
	 */
	@SuppressWarnings({"unchecked", "ConstantConditions"})
	private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>>
	void jettison(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
		ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
		ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
		final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
		final KT sourceKey = componentSource.getActor().getKey();

		sourceInventory.jettison(sourceKey, itemPayload.getObject());
	}

	/**
	 * Determine whether an item can be jettisoned
	 * @param componentSourceUntyped  The inventory that the item is being dragged from
	 * @param itemPayloadUntyped      The item that is being dragged
	 * @return Whether the item can be jettisoned
	 */
	@SuppressWarnings({"unchecked", "ConstantConditions"})
	private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>>
	boolean canJettison(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
		ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
		ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
		final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
		final KT sourceKey = componentSource.getActor().getKey();
		return sourceInventory.canJettison(sourceKey, itemPayload.getObject());
	}
}
