package com.universeprojects.gamecomponents.client.components;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.ArrayList;
import java.util.List;

public class GCItemInternalTree extends Table {

    protected final H5ELayer layer;
    protected List<GCItemTreeItem> categoryList = new ArrayList<>();

    protected GCItemTreeSubitem selection = null;

    public GCItemInternalTree(H5ELayer layer) {
        this.layer = layer;
    }

    public GCItemTreeSubitem addItem(String name, Callable0Args callable) {
        GCItemTreeSubitem item = add(new GCItemTreeSubitem(layer, this, name, callable)).left().top().getActor();
        row();
        return item;
    }

    public GCItemTreeItem addCategory(String name) {
        GCItemTreeItem item = add(new GCItemTreeItem(layer, this, name)).left().top().getActor();
        categoryList.add(item);
        row();
        return item;
    }

    public void removeCategory(GCItemTreeItem item) {
       removeActor(item);
       categoryList.remove(item);
    }

    public void clearTree() {
        clearChildren();
        selection = null;
        categoryList.clear();
    }

    public GCItemTreeSubitem getSelection() {
        return selection;
    }

}
