package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.html5engine.client.framework.H5EEngine;

public class InventoryManager {
    private static InventoryManager INSTANCE = null;

    public static InventoryManager getInstance(H5EEngine engine) {
        if (INSTANCE == null) {
            INSTANCE = new InventoryManager(engine);
        }
        return INSTANCE;
    }

    public static void cleanup() {
        INSTANCE = null;
    }

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private final H5EEngine engine;
    private final DragAndDrop dragAndDrop;

    public InventoryManager(H5EEngine engine) {
        this.engine = engine;
        this.dragAndDrop = new DragAndDrop();
    }

    public DragAndDrop getDragAndDrop() {
        return dragAndDrop;
    }
}
