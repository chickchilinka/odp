package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;
import com.universeprojects.vsdata.shared.RarityValue;

public enum Rarity {
    JUNK("#7D7D7D"), //Dark Grey
    COMMON("#BCC4CCf"), //Grey
    UNCOMMON("#22993e"), //Green
    RARE("#2f65d6"), //Blue
    EPIC("#7721c4"), //Purple
    LEGENDARY("#c18617"), //Yellow

    INVENTION_PROTOTYPE("#BCC4CCf"),
    INVENTION_SKILL("#BCC4CCf"),
    INVENTION_EXPERIMENT("#BCC4CCf"),
    NONE("#BCC4CCf");


    Rarity(String color) {
        this(Color.valueOf(color));
    }

    Rarity(Color color) {
        this.color = color;
    }

    public static Rarity from(RarityValue rarityValue) {
        return valueOf(rarityValue.name());
    }

    public RarityValue toRarityValue() {
        return RarityValue.valueOf(name());
    }

    public String text() {
        if(text == null) {
            synchronized (this) {
                if(text == null) {
                    text = name().replaceAll("_", " ");
                }
            }
        }
        return text;
    }

    public String getIconStyle() {
        if (this == NONE) return null;
        return "icon-" + name().toLowerCase() + "_grade";
    }

    private final Color color;
    private String text;

    public Color getColor() {
        return color;
    }
}
