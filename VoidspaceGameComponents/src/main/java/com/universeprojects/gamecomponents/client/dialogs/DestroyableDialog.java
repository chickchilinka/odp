package com.universeprojects.gamecomponents.client.dialogs;

public interface DestroyableDialog {
    void destroy();
}
