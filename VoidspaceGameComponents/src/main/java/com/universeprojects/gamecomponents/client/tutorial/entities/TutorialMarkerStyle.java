package com.universeprojects.gamecomponents.client.tutorial.entities;

public enum TutorialMarkerStyle {
    ARROW,
    TOOLTIP
}
