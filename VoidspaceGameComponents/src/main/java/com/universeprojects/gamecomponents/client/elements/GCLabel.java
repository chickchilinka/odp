package com.universeprojects.gamecomponents.client.elements;

import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCLabel extends H5ELabel {
    public GCLabel(H5ELayer layer) {
        super(layer);
    }

    public GCLabel(String text, H5ELayer layer){
        super(text, layer);
    }

    public GCLabel(String text ,H5ELayer layer, String styleName){
        super(text, layer, styleName);
    }

//    private final int maxWidth;
//    private String text = "";
//
//    public GCLabel(H5ELayer layer, int maxWidth) {
//        super(layer);
//        if (maxWidth < 0) {
//            throw new IllegalArgumentException("Maximum width can't be negative: " + maxWidth);
//        }
//
//        this.maxWidth = maxWidth;
//    }
//
//    @Override
//    public void setText(String text) {
//        text = Strings.nullToEmpty(text);
//        if (text.equals(this.text)) {
//            return;
//        }
//        this.text = text;
//
//        super.setText(text);
//        super.stabilize();
//
//        int textW = super.getRenderedTextWidth();
//        if (textW > maxWidth) {
//            // make sure that the label does not go out of bounds by "chomping" it
//
//            double prop = ((double) maxWidth) / textW;
//            int chompedLength = UPMath.max(0, (int)((double)text.length() * prop) - 2);
//            String chompedText = text.substring(0, chompedLength) + "...";
//
//            super.setText(chompedText);
//            super.stabilize();
//        }
//    }
//
//    @Override
//    public String getText() {
//        return text;
//    }

}
