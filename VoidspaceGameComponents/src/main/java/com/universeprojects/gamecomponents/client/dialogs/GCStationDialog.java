package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.common.shared.callable.Callable3Args;
import com.universeprojects.gamecomponents.client.dialogs.inventory.*;
import com.universeprojects.gamecomponents.client.dialogs.station.store.*;
import com.universeprojects.gamecomponents.client.dialogs.station.passengers.*;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ERate;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ETextArea;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.vsdata.shared.CategoryData;
import com.universeprojects.vsdata.shared.CurrencyData;
import com.universeprojects.vsdata.shared.PassengerData;
import com.universeprojects.vsdata.shared.CategorizedStoreItemData;
import com.universeprojects.vsdata.shared.ItemTreeData;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

public class GCStationDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 800;
    private final static int WINDOW_H = 600;
    public final H5ELayer layer;
    public final ButtonGroup<H5EButton> tabButtons;
    public final Table tabButtonsTable;
    public final H5EStack tabWindowsStack;
    //<editor-fold desc="Up panel">
    public final Table upPanel;
    private final H5EButton refuelBtn;
    private final Table refuelButtonContent;
    private final H5EButton rechargeBtn;
    private final Table rechargeButtonContent;
    private final H5EButton undockBtn;
    private final Table fuelPlace;
    private GCItemIcon<GCInventoryItem> fuelItemIcon;
    private List<GCStoreListItemData> fuels;
    private ClickListener fuelClicked;
    private GCStoreListItemData curFuel;
    private Callable1Args onRefuel;
    private Callable0Args onRecharge;
    private Callable0Args onUndock;
    // public final H5EIcon questionMark;
    //public final Table rechargeBtn;
    //public final Table undockBtn;
    //</editor-fold>
    //<editor-fold desc="Store variables">
    private final GCCategoryStoreSplitComponent storeComponent;
    private final Table storeTable;
    private final GCLoadingIndicator loadingIndicator;
    private Callable2Args onStoreItemSelected;
    private ItemTreeData storeTree;
    private List<GCStoreListItemData> itemList;
    private List<CurrencyData> playerBalance;
    private GCItemCountDialog itemCountDialog;
    //</editor-fold>
    //<editor-fold desc="Passenger tab variables">
    private final Table passengersTab;
    private final Table passengersControlPanel;
    private final H5EButton lockCabin;
    private boolean isCabinLocked;
    private final H5EScrollablePane passengersPane;
    private final GCList<GCPassengersListItem> passengersItemList;
    private final GCListItemActionHandler<GCPassengersListItem> passengerHandler;
    private Callable1Args controlBtnClick;
    private Callable2Args onPassengerSelected;
    //</editor-fold>

    private Actor currentActor;

    public GCStationDialog(H5ELayer layer) {
        super(layer, "stationDialog", "Station Dialog", WINDOW_W, WINDOW_H, false);
        this.layer = layer;
        positionProportionally(0.65f, 0.65f);
        setFullscreenOnMobile(true);
        upPanel = new Table();
        upPanel.setBackground(StyleFactory.INSTANCE.panelStyleBlueCornersSemiTransparent);
        Table leftButtons = new Table();
        refuelBtn = new H5EButton("", layer);
        refuelBtn.setStyle(StyleFactory.INSTANCE.buttonStyleBlueYellow);
        refuelButtonContent = new Table();
        refuelBtn.add(refuelButtonContent).grow();
        fuelPlace = new Table();
        fuelPlace.setBackground(StyleFactory.INSTANCE.panelStyleDarkBorderSemiTransparent);
        fuelItemIcon = new GCItemIcon<>(layer);
        fuelClicked = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                GCFuelSelectionDialog dialog = new GCFuelSelectionDialog(layer, (fuel) -> {
                    curFuel = fuel;
                    fuelPlace.clear();
                    refuelButtonContent.clear();
                    fuelItemIcon = new GCItemIcon(layer);
                    fuelItemIcon.setItem(fuel.getItem());
                    fuelItemIcon.addListener(fuelClicked);
                    fuelPlace.add(fuelItemIcon).size(40, 40);
                    refuelButtonContent.add(H5EIcon.fromSpriteType(layer, "GUI/ui2/gauge-gas-gauge1")).size(24, 24).left();
                    refuelButtonContent.add(new H5ELabel(String.valueOf(fuel.getData().price.amount), layer)).right().padLeft(5);
                    refuelButtonContent.add(H5EIcon.fromSpriteType(layer, fuel.getData().price.icon)).padLeft(5).size(24, 24).left();
                    refuelBtn.setTooltip("Refuel for " + fuel.getData().price.amount + " " + fuel.getData().price.name);
                });
                dialog.setData(fuels);
                dialog.open();
            }
        };
        refuelBtn.addButtonListener(() -> {
            if (curFuel == null) {
                fuelClicked.clicked(null, 0, 0);
            } else {
                onRefuel.call(curFuel);
            }
        });
        rechargeBtn = new H5EButton("", layer);
        rechargeBtn.setStyle(StyleFactory.INSTANCE.buttonStyleBlueYellow);
        rechargeButtonContent = new Table();
        rechargeBtn.add(rechargeButtonContent).grow();
        rechargeBtn.addButtonListener(() -> onRecharge.call());
        undockBtn = new H5EButton("", layer);
        undockBtn.add(H5EIcon.fromSpriteType(layer, "aspect-action-icons/exit-ship1")).size(24, 24).padRight(10);
        undockBtn.setStyle(StyleFactory.INSTANCE.buttonStyleBlueYellow);
        undockBtn.setTooltip("Undock from Base");
        undockBtn.addButtonListener(() -> onUndock.call());

        upPanel.add(refuelBtn).padLeft(10);
        upPanel.add(fuelPlace).size(40, 40).spaceLeft(15);
        upPanel.add(rechargeBtn).spaceLeft(15).spaceTop(5);
        upPanel.add().growX();
        upPanel.add(undockBtn).padRight(10).space(10);
        add(upPanel).growX().minHeight(55);
        float screenWidth = layer.getViewport().getScreenWidth();
        row();

        tabButtons = new ButtonGroup<H5EButton>();
        tabButtonsTable = new Table();
        add(tabButtonsTable).growX().padTop(5).left();
        H5EButton storeTabButton = ButtonBuilder.inLayer(layer).withStyle("tab-button2").withText("Store").build();
        tabButtonsTable.add(storeTabButton).left().padBottom(-5).minWidth(100);
        tabButtons.add(storeTabButton);
        storeTabButton.addButtonListener(this::storeTabSelected);
        H5EButton passengersTabButton = ButtonBuilder.inLayer(layer).withStyle("tab-button2").withText("Passengers").build();
        tabButtonsTable.add(passengersTabButton).left().padBottom(-5).minWidth(100);
        tabButtons.add(passengersTabButton);
        passengersTabButton.addButtonListener(this::passengersTabSelected);
        H5EButton adminTabButton = ButtonBuilder.inLayer(layer).withStyle("tab-button2").withText("Admin").build();
        tabButtonsTable.add(adminTabButton).left().padBottom(-5).minWidth(100);
        tabButtons.add(adminTabButton);
        adminTabButton.addButtonListener(this::adminTabSelected);
        tabButtonsTable.add().growX();

        row();

        Table stackTable = new Table();
        stackTable.background(StyleFactory.INSTANCE.panelStyleYellowTopAndBottomBorderSemiTransparent);
        tabWindowsStack = new H5EStack();
        storeTable = new Table();
        storeComponent = new GCCategoryStoreSplitComponent(layer, true, (entryData) -> {
            callbackStore(entryData);
        });
        storeTable.add(storeComponent).grow();

        passengersTab = new Table();
        passengersControlPanel = new Table();
        passengersControlPanel.setBackground(StyleFactory.INSTANCE.panelStyleBlueCornersSemiTransparent);
        passengersPane = new H5EScrollablePane(layer);
        passengersItemList = new GCList<GCPassengersListItem>(layer, 1, 5, 1, true);
        passengerHandler = new GCListItemActionHandler<GCPassengersListItem>() {
            @Override
            protected void onSelectionUpdate(GCPassengersListItem lastSelectedItem) {
                GCPassengersListItem item = this.getSelectedItem();
                for (GCPassengersListItem passenger : passengersItemList.getItems()) {
                    passenger.hideButtons();
                }
                if (item != null) {
                    item.showButtons();
                    onPassengerSelected.call(item.getData(), 0);
                }
            }
        };
        lockCabin = new H5EButton("Lock cabin", layer);
        lockCabin.addButtonListener(() -> controlBtnClick.call(1));
        lockCabin.setStyle(StyleFactory.INSTANCE.buttonStyleBlueYellow);
        passengersControlPanel.add(lockCabin).left().padLeft(10);
        passengersControlPanel.add().growX();
        passengersTab.add(passengersControlPanel).growX().maxHeight(45);
        passengersTab.row();
        passengersTab.add(passengersPane).grow().padTop(5);
        passengersPane.add(passengersItemList).grow();

        currentActor = storeComponent;
        tabWindowsStack.add(storeTable);
        stackTable.add(tabWindowsStack).grow();
        add(stackTable).grow();
        loadingIndicator = new GCLoadingIndicator(layer);
        storeComponent.getLabelTable().clear();
        storeComponent.getLabelTable().add(loadingIndicator);
        loadingIndicator.activate(storeComponent.getLabelTable());
    }

    //<editor-fold desc="Store tab">
    public void processStoreData(ItemTreeData<CategorizedStoreItemData> tree, List<GCStoreListItemData> itemList, List<CurrencyData> playerBalance, Callable2Args<GCStoreListItemData, Integer> callback) {
        this.storeTree = tree;
        this.onStoreItemSelected = callback;
        this.itemList = itemList;
        this.playerBalance = playerBalance;
        loadingIndicator.deactivate();
        List<CategoryData> rootCategories = tree.category.listOfSubcategories;
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        for (CategoryData cat : rootCategories) {
            generateMapRecursively(data, cat);
        }
        List<GCStoreListItemData> unCatList = new ArrayList<GCStoreListItemData>();
        List<CategorizedStoreItemData> unCatData = (List<CategorizedStoreItemData>) tree.getUncatItems();
        for (CategorizedStoreItemData unCatDataEntry : unCatData) {
            for (GCStoreListItemData item : itemList) {
                if (item.getData() == unCatDataEntry) {
                    unCatList.add(item);
                }
            }
        }
        data.put("", unCatList);
        storeComponent.setData(data);
    }

    public void generateMapRecursively(Map<String, Object> map, CategoryData category) {
        if (category.listOfSubcategories != null && category.listOfSubcategories.size() > 0) {
            Map<String, Object> inner = new LinkedHashMap<String, Object>();
            map.put(category.name, inner);
            for (CategoryData cat : category.listOfSubcategories) {
                generateMapRecursively(inner, cat);
            }
        } else {
            List<GCStoreListItemData> list = new ArrayList<GCStoreListItemData>();
            List<CategorizedStoreItemData> dataList = storeTree.getCategoryItems(category.id);
            for (CategorizedStoreItemData dataEntry : dataList) {
                for (GCStoreListItemData item : itemList) {
                    if (item.getData() == dataEntry) {
                        list.add(item);
                    }
                }
            }
            map.put(category.name, list);
        }
    }


    public void callbackStore(GCStoreListItem entry) {
        CurrencyData balance = new CurrencyData();
        for (CurrencyData balanceEntry : playerBalance) {
            if (balanceEntry.name.equals(entry.getEntryData().getData().price.name)) {
                balance = balanceEntry;
            }
        }
        itemCountDialog = new GCItemCountDialog(layer, entry.getEntryData(), balance, (count) -> {
            itemCountDialog.close();
            onStoreItemSelected.call(itemCountDialog.getEntry(), count);
        });
        itemCountDialog.open();
    }

    public void updateStoreItem(GCStoreListItemData itemData) {
        storeComponent.updateItem(itemData);
        CurrencyData balance = new CurrencyData();
        for (GCStoreListItemData product : itemList) {
            if (product.getData().id.equals(itemData.getData().id)) {
                itemList.set(itemList.indexOf(product), itemData);
            }
        }
        for (CurrencyData balanceEntry : playerBalance) {
            if (balanceEntry.name.equals(itemData.getData().price.name)) {
                balance = balanceEntry;
            }
        }
        if (itemCountDialog != null && itemCountDialog.isOpen()) {
            itemCountDialog.updateData(itemData, balance);
        }
    }

    public void updateBalance(List<CurrencyData> playerBalance) {
        this.playerBalance = playerBalance;

        if (itemCountDialog != null && itemCountDialog.isOpen()) {
            CurrencyData balance = new CurrencyData();
            for (CurrencyData balanceEntry : playerBalance) {
                if (balanceEntry.name.equals(itemCountDialog.getEntry().getData().price.name)) {
                    balance = balanceEntry;
                }
            }
            itemCountDialog.updateData(itemCountDialog.getEntry(), balance);
        }
    }
    //</editor-fold>

    //<editor-fold desc="Passengers">
    public void processPassengersData(List<PassengerData> passengers, boolean isCabinLocked, Callable1Args<Integer> controlBtnClick, Callable2Args<PassengerData, Integer> onClick) {
        this.onPassengerSelected = onClick;
        this.controlBtnClick = controlBtnClick;
        setCabinLockMode(isCabinLocked);
        passengersItemList.clear();
        for (PassengerData passenger : passengers) {
            GCPassengersListItem item = new GCPassengersListItem(layer, passengerHandler, (data) -> {
                onPassengerSelected.call(data, 1);
            }, passenger);
            passengersItemList.addItem(item);
        }
    }

    public void setCabinLockMode(boolean mode) {
        this.isCabinLocked = mode;
        if (!isCabinLocked) {
            lockCabin.setText("Lock cabin");
        } else {
            lockCabin.setText("Unlock cabin");
        }
    }

    //</editor-fold>

    //<editor-fold desc="Up panel">
    public void processUpPanel(List<GCStoreListItemData> fuelList, CurrencyData rechargePrice, Callable1Args<GCStoreListItemData> onRefuel, Callable0Args onRecharge, Callable0Args onUndock) {
        this.fuels = fuelList;
        this.onRefuel = onRefuel;
        this.onRecharge = onRecharge;
        this.onUndock = onUndock;
        fuelPlace.clear();
        refuelButtonContent.clear();
        if (fuelList.size() == 1) {
            curFuel = fuelList.get(0);
            fuelItemIcon.setItem(curFuel.getItem());
            fuelPlace.add(fuelItemIcon).size(40, 40);
            refuelButtonContent.add(H5EIcon.fromSpriteType(layer, "GUI/ui2/gauge-gas-gauge1")).size(24, 24).left();
            refuelButtonContent.add(new H5ELabel(String.valueOf(curFuel.getData().price.amount), layer)).right().padLeft(5);
            refuelButtonContent.add(H5EIcon.fromSpriteType(layer, curFuel.getData().price.icon)).padLeft(5).size(24, 24).left();
            refuelBtn.setTooltip("Refuel for " + curFuel.getData().price.amount + " " + curFuel.getData().price.name);
        } else {
            curFuel = null;
            H5ELabel selectFuel = new H5ELabel("?", layer);
            fuelPlace.addListener(fuelClicked);
            fuelPlace.add(selectFuel).padLeft(10).grow();
            refuelButtonContent.add(H5EIcon.fromSpriteType(layer, "GUI/ui2/gauge-gas-gauge1")).size(24, 24).left();
            refuelButtonContent.add(new H5ELabel("?", layer)).right().padLeft(5);
            refuelBtn.setTooltip("Select fuel for refueling");
        }
        rechargeButtonContent.clear();
        rechargeButtonContent.add(H5EIcon.fromSpriteType(layer, "GUI/ui2/gauges-power-levels1")).size(24, 24).left();
        rechargeButtonContent.add(new H5ELabel(String.valueOf(rechargePrice.amount), layer)).left().padLeft(5);
        rechargeButtonContent.add(H5EIcon.fromSpriteType(layer, rechargePrice.icon)).padLeft(5).size(24, 24).left();
        rechargeBtn.setTooltip("Recharge for " + rechargePrice.amount + " " + rechargePrice.name);
    }
    //</editor-fold>

    //<editor-fold desc="Tabs control">
    public void passengersTabSelected() {
        tabWindowsStack.clear();
        tabWindowsStack.add(passengersTab);
    }

    public void storeTabSelected() {
        tabWindowsStack.clear();
        tabWindowsStack.add(storeTable);
    }

    public void adminTabSelected() {
        tabWindowsStack.clear();
    }
    //</editor-fold>
}
