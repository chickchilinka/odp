package com.universeprojects.gamecomponents.client.dialogs.login;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Used as a superclass for the {@link GCLoginScreen} and {@link GCRegisterScreen} . Initializes  core elements for
 * users to enter login information such as email and password text fields.
 */

public abstract class GCLoginInformation {

    protected final GCWindow window;

    protected final H5EInputBox fieldEmail;
    protected final H5EInputBox fieldPassword;
    protected H5ELabel emailLabel;
    protected H5ELabel passwordLabel;
    protected final H5ELabel emailWarningText;
    protected final H5ELabel passwordWarningText;
    protected static final String emailRequirements = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$";
    protected static final Pattern emailPattern = Pattern.compile(emailRequirements);
    protected Matcher emailMatcher;

    protected final Color warningColor;
    protected final Color normalColor;
    public static final int LOGIN_FIELD_W = 350;
    public static final int ICON_SIZE = 64;
    final float warningTextFontSize = 0.75f;
    final float loginInfoFontSize = 1.1f;


    protected final H5ELayer layer;

    public GCLoginInformation(H5ELayer layer) {

        this.layer = layer;
        window = new GCWindow(layer, 444, 546, "login-window");
        window.setId("login-screen");
        window.setTitle("Create your account");
        window.positionProportionally(0.5f, 0.5f);
        window.setPackOnOpen(false);
        window.setMovable(false);
        window.defaults().center();


        window.setCloseButtonEnabled(false);
        warningColor = Color.RED;
        normalColor = Color.WHITE;
        //Align title onto tab at the top
        window.padTop(40);
        window.getTitleTable().padRight(20);

        //Create login tab

        // This field is used in the subclasses
        emailLabel = new H5ELabel(layer);
        emailLabel.setFontScale(loginInfoFontSize);
        emailLabel.setText("Email");

        //Space to enter email
        fieldEmail = new H5EInputBox(layer);
        fieldEmail.setWidth(LOGIN_FIELD_W);

        //Warning text for invalid emails
        emailWarningText = new H5ELabel(layer);
        emailWarningText.setFontScale(warningTextFontSize);
        emailWarningText.setColor(warningColor);
        emailWarningText.setAlignment(Align.center);
        emailWarningText.setVisible(false);

        //Password text
        passwordLabel = new H5ELabel(layer);
        passwordLabel.setFontScale(loginInfoFontSize);
        passwordLabel.setText("Password");

        //Space to enter password
        fieldPassword = new H5EInputBox(layer);
        fieldPassword.setWidth(LOGIN_FIELD_W);
        fieldPassword.setPasswordMode(true);

        //Warning text for invalid passwords
        passwordWarningText = new H5ELabel(layer);
        passwordWarningText.setFontScale(warningTextFontSize);
        passwordWarningText.setColor(warningColor);
        passwordWarningText.setAlignment(Align.center);
        passwordWarningText.setVisible(false);

    }

    public String getPassword() {
        return fieldPassword.getText();
    }

    public String getEmail() {
        return fieldEmail.getText();
    }

    protected boolean isValidEmail() {
        // check if an email was entered
        String email = getEmail();
        if (email.isEmpty()) {
            emailWarningText.setText("Please enter your email.");
            emailWarningText.setVisible(true);
            fieldEmail.setColor(warningColor);
            return false;
        } else {
            //Check if email string is invalid (e.g no whitespace, no @, etc)
            emailMatcher = emailPattern.matcher(email);
            boolean validEmail = emailMatcher.matches();
            if (!validEmail) {
                emailWarningText.setText("Please enter a valid email.");
                emailWarningText.setVisible(true);
                fieldEmail.setColor(warningColor);
                return false;
            }
            return true;
        }
    }

    protected boolean isValidPassword() {
        String password = getPassword();
        if (password.isEmpty()) {
            passwordWarningText.setText("Please enter your password.");
            passwordWarningText.setVisible(true);
            fieldPassword.setColor(warningColor);
            return false;
        }
        return true;

    }

    public GCWindow getWindow() {
        return window;
    }

    public void open() {
        window.open();
        window.positionProportionally(0.5f, 0.5f);
    }


    public void close() {
        window.close();
    }

    public void destroy() {
        window.destroy();
    }

    public boolean isOpen() {
        return window.isOpen();
    }
}
