package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionButton;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCActionsListItem extends GCListItem {
    private GCActionButton actionButton;
    private Label description;
    private Label name;

    public GCActionsListItem(H5ELayer layer, GCListItemActionHandler<?> actionHandlerIn, GCActionButton button, String name, String desc){
        super(layer, actionHandlerIn);
        if(button == null){
            return;
        }
        actionButton = add(button).padRight(10).padLeft(10).center().getActor();
        Table subTable = new Table();
        add(subTable).growX().growY().fillY();
        subTable.left().top();
        subTable.defaults().left().top();


        this.name = subTable.add(new Label(name + ":", getSkin(), "label-electrolize-small")).getActor();
        this.name.setColor(Color.valueOf("#FAFAFA"));
        subTable.row();

        description = subTable.add(new Label(desc,getSkin(), "label-electrolize-small")).growX().fillY().getActor();
        description.setWrap(true);
        description.setText(desc);
        description.setColor(Color.valueOf("#CCCCCC"));
    }

    public static class GCActionsListItemHandler extends GCListItemActionHandler<GCActionsListItem> {

        @Override
        protected void onSelectionUpdate(GCActionsListItem newItemSelection) {

        }
    }

}
