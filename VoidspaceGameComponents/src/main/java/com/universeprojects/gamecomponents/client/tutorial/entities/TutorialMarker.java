package com.universeprojects.gamecomponents.client.tutorial.entities;

public class TutorialMarker {
    private String objectId;
    private TutorialMarkerStyle style = TutorialMarkerStyle.ARROW;
    private String trigger;

    public TutorialMarker() {
    }

    public TutorialMarker(String objectId) {
        this.objectId = objectId;
    }

    public TutorialMarkerStyle getStyle() {
        return style;
    }

    public void setStyle(TutorialMarkerStyle style) {
        this.style = style;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }
}
