package com.universeprojects.gamecomponents.client.elements;

import java.util.HashMap;
import java.util.Map;

public class CreditItemData implements Comparable<CreditItemData> {

    public String name = "";
    public long id;
    public int quantity = 0;
    public String iconKey = "";
    public long sortingField = -1L;
    public String externalId;
    public final Map<String, CreditItemPriceData> processorPriceData = new HashMap<>();

    @Override
    public int compareTo(CreditItemData o) {
        return Long.compare(sortingField, o.sortingField);
    }
}
