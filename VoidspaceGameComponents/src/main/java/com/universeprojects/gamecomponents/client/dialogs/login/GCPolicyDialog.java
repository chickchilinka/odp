package com.universeprojects.gamecomponents.client.dialogs.login;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * UI for an arbitrary policy to be accepted before registration. New policies should extend this class
 * passing the policy that should be accepted as a string upon initialization and set a new window title.
 */
public abstract class GCPolicyDialog {

    protected final H5ELayer layer;
    protected final GCWindow window;
    protected H5EButton btnAccept;
    protected H5EButton btnBack;
    protected Callable0Args onAccepted;
    protected Callable0Args onRejected;
    protected final H5ELabel label;
    private final H5EScrollablePane panel;
    protected final GCLoadingIndicator loadingIndicator;

    /**
     * Initializes all policy UI.
     *
     * @param layer Current window layer
     */
    public GCPolicyDialog(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);
        loadingIndicator = new GCLoadingIndicator(layer);

        final int width = (int) (500);
        final int height = (int) (400);
        window = new GCWindow(layer, width, height, "login-window");
        window.setId("privacy-policy");
        window.setTitle("Privacy Policy");
        window.setCloseButtonEnabled(false);
        window.positionProportionally(0.5f, 0.5f);
        window.setPackOnOpen(false);
        window.setMovable(false);
        window.defaults().left();
        window.setCloseButtonEnabled(false);
        window.padTop(40);
        window.getTitleTable().padRight(20);
        window.row();
        //Create scrollpane
        panel = new H5EScrollablePane(window.getLayer(), "scrollpane-backing-1");
        panel.setScrollingDisabled(true, false);
        panel.setOverscroll(false, false);

        label = new H5ELabel(window.getLayer());
        label.setFontScale(0.6f);
        label.setColor(Color.valueOf("#3399FF"));
        label.setWrap(true);
        label.setAlignment(Align.left);
        panel.getContent().add(label).grow();
        panel.getContent().row();
        panel.getContent().add();
        panel.getContent().row();

        window.add(panel).grow();


        window.row().padTop(20);
        btnAccept = new H5EButton("Accept", layer);
        btnAccept.addButtonListener(this::onAcceptButtonClicked);
        //window.add(btnAccept).size(150, btnAccept.getHeight() ).getActor();
//        btnAccept.setDisabled(true);      // Lets not do this.

        btnBack = new H5EButton("Back", layer);
        btnBack.addButtonListener(this::onRejectedButtonClicked);
        btnBack.getStyle().checked = null;
        //window.add(btnBack).size(150, btnBack.getHeight() ).getActor();

        Table buttonTable = new Table();
        window.add(buttonTable).colspan(2).center();
        final Cell<H5EButton> b1Cell = buttonTable.add(btnAccept);
        final Cell<H5EButton> b2Cell = buttonTable.add(btnBack);
        b1Cell.padTop(10).width(150);
        b2Cell.padTop(10).width(150);

        panel.addListener(new InputListener() {
            @Override
            public boolean handle(Event event) {
                checkAllRead();
                return true;
            }
        });
    }

    private void checkAllRead() {
        if (panel.getScrollPercentY() == 1 || panel.getMaxY() == 0) {
            btnAccept.setDisabled(false);
        }
    }

    public void setText(String value) {
        label.setText(value);
        panel.getContent().validate();
        panel.validate();
        panel.setScrollY(0);
        checkAllRead();
    }

    public GCWindow getWindow() {
        return window;
    }

    public void open(Callable0Args onAccepted, Callable0Args onRejected) {
        this.onAccepted = onAccepted;
        this.onRejected = onRejected;
        btnAccept.setVisible(onAccepted != null);
        window.open();
    }

    public void close() {
        window.close();
        onAccepted = null;
        onRejected = null;
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    protected void onAcceptButtonClicked() {
        if (onAccepted != null) {
            onAccepted.call();
        }
        close();
    }

    protected void onRejectedButtonClicked() {
        if (onRejected != null) {
            onRejected.call();
        }
        close();
    }

}
