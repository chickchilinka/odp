package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.vsdata.shared.BuildQueueEntry;

import java.util.Collections;
import java.util.Map;

public class BuildQueueEntryItemLike implements GCInventoryItem {

    private final BuildQueueEntry buildQueueEntry;

    public BuildQueueEntryItemLike(BuildQueueEntry buildQueueEntry) {
        this.buildQueueEntry = buildQueueEntry;
    }

    @Override
    public String getUid() {
        return String.valueOf(buildQueueEntry.referenceId);
    }

    @Override
    public Object getRawEntity() {
        return buildQueueEntry;
    }

    @Override
    public String getName() {
        return buildQueueEntry.entryName;
    }

    @Override
    public String getItemClass() {
        return buildQueueEntry.entryClass;
    }

    @Override
    public String getDescription() {
        return buildQueueEntry.entryDescription;
    }

    @Override
    public long getQuantity() {
        return buildQueueEntry.repeats;
    }

    @Override
    public boolean isStackable() {
        return buildQueueEntry.repeats > 1;
    }

    @Override
    public String getIconSpriteKey() {
        return buildQueueEntry.iconSpriteType;
    }

    @Override
    public String getRarityIconStyle() {
        return getRarityWithDefault().getIconStyle();
    }

    @Override
    public String getRarityText() {
        return getRarityWithDefault().text();
    }

    @Override
    public Color getRarityColor() {
        return getRarityWithDefault().getColor();
    }

    @Override
    public Map<String, InspectorParameter> getAdditionalProperties() {
        return Collections.emptyMap();
    }

    @Override
    public boolean createAdditionalInspectorComponents(H5ELayer layer, Table inspectorTable) {
        return false;
    }

    @Override
    public GCInternalItemInventory getInternalInventory() {
        return null;
    }

    @Override
    public GCInternalTank getInternalTank() {
        return null;
    }

    @Override
    public String getIconOverlayKey() {
        return null;
    }

    @Override
    public boolean canBeExperimentedOn() {
        return false;
    }

    @Override
    public boolean canBeCustomized() {
        return false;
    }

    private Rarity getRarityWithDefault() {
        Rarity rarity = Rarity.from(buildQueueEntry.rarityValue);
        return rarity != null ? rarity : Rarity.COMMON;
    }
}
