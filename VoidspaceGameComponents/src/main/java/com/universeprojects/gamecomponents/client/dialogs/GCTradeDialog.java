package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventory;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

public class GCTradeDialog extends GCSimpleWindow {
	private static final int WIDTH = 500;
	private static final int HEIGHT = 500;

	public static final int DEFAULT_SIZE = 12;

	private GCInventory player1TradeInventory;
	private GCInventory player2TradeInventory;

	private final Image player1ReadyOverlay;
	private final Image player2ReadyOverlay;

	public GCTradeDialog (H5ELayer layer, String player1Name, String player2Name, GCInventory player1TradeInventory, GCInventory player2TradeInventory) {
		super(layer, "tradeDialogWindow", "Trade Dialog", WIDTH, HEIGHT, true);
		setFullscreenOnMobile(false);

		this.player1TradeInventory = player1TradeInventory;
		this.player2TradeInventory = player2TradeInventory;

		H5EStack player1Stack = new H5EStack();
		H5EStack player2Stack = new H5EStack();

		this.addH2(player1Name).uniform().left();
		this.addH2(player2Name).uniform().right();
		this.row().padTop(20);

		this.add(player1Stack).grow().minWidth(170).left();
		this.add(player2Stack).grow().minWidth(170).right();

		player1Stack.add(player1TradeInventory);
		this.player1TradeInventory.left();

		player2Stack.add(player2TradeInventory);
		this.player2TradeInventory.right();
		this.player2TradeInventory.setTouchable(Touchable.disabled);

		player1ReadyOverlay = new Image(layer.getEngine().getSkin(), "confirm-overlay-rectangle");
		player1Stack.add(player1ReadyOverlay);
		player1ReadyOverlay.setTouchable(Touchable.disabled);
		player1ReadyOverlay.setVisible(false);
		this.player1TradeInventory.setVisible(true);

		player2ReadyOverlay = new Image(layer.getEngine().getSkin(), "confirm-overlay-rectangle");
		player2Stack.add(player2ReadyOverlay);
		player2ReadyOverlay.setTouchable(Touchable.disabled);
		player2ReadyOverlay.setVisible(false);
		this.player2TradeInventory.setVisible(true);

		this.row().padBottom(20);

		H5EButton tradeBtn = new H5EButton("Trade", layer);
		tradeBtn.addButtonListener(this::playerTradeReady);
		tradeBtn.setUserObject("button:trade");
		this.add(tradeBtn).left();

		H5EButton cancelBtn = new H5EButton("Cancel", layer);
		cancelBtn.addButtonListener(this::playerTradeCancel);
		this.add(cancelBtn).right();
	}

	/**
	 * Update the rendering of the inventories
	 */
	public void update() {
		player1TradeInventory.update();
		player2TradeInventory.update();
	}

	/**
	 * This method is called when player 2 is ready; a ready overlay is drawn around player 2's trade inventory when
	 * player 2 is ready to trade
	 */
	public void partnerTradeReady () {
		player2ReadyOverlay.setVisible(true);
	}

	/**
	 * This method is called when player 2 wants to cancel being ready; the ready overlay is removed from around player
	 * 2's trade inventory when player 2 is no longer ready to trade
	 */
	public void partnerTradeUnready () {
		player2ReadyOverlay.setVisible(false);
	}

	/**
	 * This method is called when player 1 is ready; a ready overlay is drawn around player 1's trade inventory when
	 * player 1 is ready to trade
	 */
	public void playerTradeReady () {
		player1ReadyOverlay.setVisible(true);
		Tutorials.trigger("button:trade");
	}

	/**
	 * This method is called when player 1 wants to cancel being ready; the ready overlay is removed from around player
	 * 1's trade inventory when player 1 is no longer ready to trade
	 */
	public void playerTradeUnready () {
		player1ReadyOverlay.setVisible(false);
	}

	/**
	 * This method is called when trading is cancelled
	 */
	public void playerTradeCancel () {

	}
}
