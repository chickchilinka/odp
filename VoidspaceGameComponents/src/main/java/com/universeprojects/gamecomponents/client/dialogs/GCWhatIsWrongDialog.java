package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ETextArea;
import com.universeprojects.vsdata.shared.ProblemData;

public class GCWhatIsWrongDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 450;
    private final static int WINDOW_H = 650;
    private final H5ELabel text;
    private final H5ETextArea reportEditArea;
    private final H5EScrollablePane scrollablePane;
    private final H5EButton submit;

    public GCWhatIsWrongDialog(H5ELayer layer, String message, String closeText, Callable1Args<ProblemData> onSubmit, Callable0Args onClose) {
        super(layer, "whatIsWrongDialog", "Tell us what's wrong", WINDOW_W, WINDOW_H, false);
        positionProportionally(0.5f, 0.5f);
        setFullscreenOnMobile(true);
        setModal(true);
        disableCloseButton();
        text = new H5ELabel(message, layer);
        text.setWrap(true);
        add(text).left().top().growX().pad(5).padLeft(10);
        row();
        scrollablePane = new H5EScrollablePane(layer);
        scrollablePane.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        scrollablePane.setScrollingDisabled(true, false);
        scrollablePane.setOverscroll(false, true);
        scrollablePane.setForceScroll(false, true);
        reportEditArea = new H5ETextArea(layer);
        TextFieldStyle areaStyle = new TextFieldStyle(reportEditArea.getStyle());
        areaStyle.background = null;
        reportEditArea.setStyle(areaStyle);
        reportEditArea.setMessageText("Please, tell us what is wrong");
        reportEditArea.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                submit.setDisabled(reportEditArea.getText().length() <= 0);
                setAreaHeight();
            }
        });
        scrollablePane.add(reportEditArea).grow();
        add(scrollablePane).padLeft(5).padRight(5).expand().fill().left();
        row();
        submit = new H5EButton("Submit", layer);
        H5EButton cancel = new H5EButton(closeText, layer);
        cancel.addButtonListener(()->{
            onClose.call();
            close();
        });
        submit.addButtonListener(()->{
            ProblemData outData = new ProblemData(reportEditArea.getText());
            onSubmit.call(outData);
            close();
        });
        submit.setDisabled(true);
        Table buttonRow = new Table();
        buttonRow.pad(15,10,5,10);
        buttonRow.add(submit).width(120).left();
        buttonRow.add().growX();
        buttonRow.add(cancel).width(120).right();
        add(buttonRow).right().growX();
    }

    protected void setAreaHeight() {
        reportEditArea.setPrefRows(reportEditArea.getLines() + 3);
        scrollablePane.setContentHeight((int) reportEditArea.getPrefHeight());
        reportEditArea.invalidate();
        scrollablePane.layout();
        scrollablePane.setScrollY(-reportEditArea.getCursorY() - scrollablePane.getHeight() / 2);
    }
}