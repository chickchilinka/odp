package com.universeprojects.gamecomponents.client.elements.actionbar;

import com.universeprojects.common.shared.util.ReactiveValue;
import com.universeprojects.html5engine.shared.ActionIconData;
import com.universeprojects.json.shared.serialization.SerializedDataList;

/**
 * An operation that can be placed to the ActionBar
 */
public interface GCOperation {
    void call();

    ReactiveValue<ActionIconData> getIcon();

    SerializedDataList serialize();

    default void activate() {

    }

    default void deactivate() {

    }

    GCOperation getActionBarCopy();

    String getActionHumanName();
}
