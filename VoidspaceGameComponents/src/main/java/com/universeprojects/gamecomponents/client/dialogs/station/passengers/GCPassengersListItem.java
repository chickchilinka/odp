package com.universeprojects.gamecomponents.client.dialogs.station.passengers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.vsdata.shared.PassengerData;


@SuppressWarnings("FieldCanBeLocal")
public class GCPassengersListItem extends GCListItem {

    private final PassengerData entryData;
    private final Button passengerContainer;
    private final H5EIcon passengerIcon;
    private final H5ELabel passengerNameLabel;
    private final Table buttons;
    private final H5EButton trade;
    public static final int ICON_SIZE = 25;

    private float descPrefWidth;
    private float descWidth;

    public GCPassengersListItem(H5ELayer layer, GCListItemActionHandler<GCPassengersListItem> handler, Callable1Args<PassengerData> onTrade, PassengerData entryData) {
        super(layer, handler);
        this.entryData = entryData;
        row();

        passengerContainer = add(new Button(getEngine().getSkin(), "gc-list-item-no-check")).growX().getActor();
        passengerContainer.left().top();
        passengerContainer.defaults().left().top();

        H5EStack stack = new H5EStack();
        passengerContainer.add(stack).size(ICON_SIZE);
        passengerIcon = H5EIcon.fromSpriteType(layer, entryData.icon);
        stack.add(passengerIcon);

        Table subTable = new Table().left().top();
        subTable.defaults().left().top();
        passengerContainer.add(subTable).grow();

        passengerNameLabel = new H5ELabel(layer);
        subTable.add(passengerNameLabel).growX().padLeft(10).getActor();
        passengerNameLabel.setWrap(true);
        passengerNameLabel.setText(entryData.characterName);
        passengerNameLabel.setTouchable(Touchable.disabled);
        buttons = new Table();
        trade = new H5EButton("Trade", layer);
        trade.setStyle(StyleFactory.INSTANCE.buttonStyleBlueYellow);
        trade.addButtonListener(()->{
            setDisabled(true);
            onTrade.call(this.entryData);
            Timer.schedule(new Timer.Task() {
                @Override
                public void run(){
                    setDisabled(false);
                }
            },0.01f);
        });
        row();
        add(buttons);

    }

    public void showButtons() {
        buttons.clear();
        buttons.add(trade).left().pad(5);
    }

    public void hideButtons() {
        buttons.clear();
    }

    public PassengerData getData() {
        return entryData;
    }

}
