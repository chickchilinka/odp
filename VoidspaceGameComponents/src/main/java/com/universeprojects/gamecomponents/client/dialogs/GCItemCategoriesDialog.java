package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.dialogs.GCCharactersListItem;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.vsdata.shared.CategoryData;
import com.universeprojects.vsdata.shared.CategoryItemData;
import com.universeprojects.vsdata.shared.ItemTreeData;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItemData;
import com.universeprojects.gamecomponents.client.dialogs.GCCategoryItemSplitComponent;

import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;

public class GCItemCategoriesDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 550;
    private final static int WINDOW_H = 600;
    private final GCCategoryItemSplitComponent<CategoryItemData> component;
    private ItemTreeData<CategoryItemData> tree;

    public GCItemCategoriesDialog(H5ELayer layer, Callable1Args<List<CategoryItemData>> onDone) {
        super(layer, "categories-dialog", "Categories", WINDOW_W, WINDOW_H, true);
        Table row1 = new Table();
        positionProportionally(0.5f, 0.5f);
        setFullscreenOnMobile(true);
        component = new GCCategoryItemSplitComponent<CategoryItemData>(layer, true);
        component.setScrollProperties();
        add(component).grow();
        row();
        Table buttonRow = new Table();
        H5EButton doneButton = new H5EButton("Done", layer);
        doneButton.addButtonListener(() ->
        {
            onDone.call(component.getSelectedItems());
            close();
        });
        H5EButton cancelButton = new H5EButton("Cancel", layer);
        buttonRow.padTop(15);
        buttonRow.add(cancelButton).left();
        buttonRow.add().growX();
        buttonRow.add(doneButton).right().pad(4);
        add(buttonRow).right().growX();
    }

    @Override
    public void open(boolean pack) {
        super.open(pack);
        component.clearFilter();
    }

    public void processData(ItemTreeData<CategoryItemData> tree) {
        this.tree = tree;
        List<CategoryData> rootCategories = tree.category.listOfSubcategories;
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        for (CategoryData cat : rootCategories) {
            generateMapRecursively(data, cat);
        }
        List<GCGenericINDListItemData<CategoryItemData>> unCatList = new ArrayList<GCGenericINDListItemData<CategoryItemData>>();
        for (CategoryItemData item : tree.getUncatItems()) {
            unCatList.add(new GCGenericINDListItemData<CategoryItemData>(item.name, item.icon, item.description, 0, item));
        }
        data.put("", unCatList);
        component.setMaxCheckCount(tree.items.size());
        component.setData(data);
    }

    public void generateMapRecursively(Map<String, Object> map, CategoryData category) {
        if (category.listOfSubcategories != null && category.listOfSubcategories.size() > 0) {
            Map<String, Object> inner = new LinkedHashMap<String, Object>();
            map.put(category.name, inner);
            for (CategoryData cat : category.listOfSubcategories
            ) {
                generateMapRecursively(inner, cat);
            }
        } else {
            List<GCGenericINDListItemData<CategoryItemData>> list = new ArrayList<GCGenericINDListItemData<CategoryItemData>>();
            for (CategoryItemData item : tree.getCategoryItems(category.id)) {
                list.add(new GCGenericINDListItemData<CategoryItemData>(item.name, item.icon, item.description, 0, item));
            }
            map.put(category.name, list);
        }

    }

    //<editor-fold desc="Demo work">
    public void demoWork() {
        processData(demoCategories());
    }

    public List<CategoryItemData> demoItems() {
        List<CategoryItemData> items = new ArrayList<CategoryItemData>() {{
            add(new CategoryItemData("1", "ships/harbor/harbor-g7-class1a.png", "Harbor g7 1a", "very very very very looooooooooooooooooooong text", "6"));
            add(new CategoryItemData("2", "ships/pass/pass-ship-class1-miner1.png", "Pass ship miner", "awesome Ship", "6"));
            add(new CategoryItemData("3", "slots/character-tool1.png", "Some tool", "awesome tool", "7"));
            add(new CategoryItemData("4", "icons/gearbox1.png", "Some skill", "some skill", "3"));
            add(new CategoryItemData("4", "icons/gearbox1.png", "Uncategorized", "some uncategorized", "12"));
        }};
        return items;
    }

    public ItemTreeData demoCategories() {
        CategoryData ideas = new CategoryData("1", "Ideas", null);
        CategoryData skills = new CategoryData("2", "Skills", null);
        CategoryData someList1 = new CategoryData("3", "Some List1", null);
        CategoryData ships = new CategoryData("4", "Ships", null);
        CategoryData a = new CategoryData("5", "A", null);
        CategoryData b = new CategoryData("6", "B", null);
        CategoryData tools = new CategoryData("7", "Tools", null);

        ItemTreeData treeData = new ItemTreeData();
        treeData.addCategoryToId(treeData.category, ideas, "0");
        treeData.addCategoryToId(treeData.category, skills, "0");
        treeData.addCategoryToId(treeData.category, ships, "1");
        treeData.addCategoryToId(treeData.category, tools, "1");
        treeData.addCategoryToId(treeData.category, someList1, "2");
        treeData.addCategoryToId(treeData.category, a, "4");
        treeData.addCategoryToId(treeData.category, b, "4");
        treeData.items = demoItems();
        return treeData;
    }
    //</editor-fold>
}