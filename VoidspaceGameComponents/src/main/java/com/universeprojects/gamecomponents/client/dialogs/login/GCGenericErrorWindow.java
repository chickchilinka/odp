package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCGenericErrorWindow extends GCSimpleWindow {
    public GCGenericErrorWindow(String title, H5ELayer layer) {
        super(layer, "connect-error", title, 500, 200, false, "login-window");

        setId("login-screen");
        setTitle(title);
        positionProportionally(0.5f, 0.5f);
        setPackOnOpen(false);
        setMovable(false);
        defaults().center();
        setCloseButtonEnabled(false);
        //Align title onto tab at the top
        padTop(40);
        getTitleTable().padRight(20);
    }

    public GCGenericErrorWindow(H5ELayer layer) {
        this("Error", layer);
    }

    @Override
    public void open() {
        pack();
        positionProportionally(0.5f, 0.5f);
        super.open();
    }
}
