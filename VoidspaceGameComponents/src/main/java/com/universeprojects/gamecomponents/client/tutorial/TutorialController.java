package com.universeprojects.gamecomponents.client.tutorial;

import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.utils.Array;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.ReturningCallable1Args;
import com.universeprojects.common.shared.hooks.Hook;
import com.universeprojects.common.shared.hooks.Hooks;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.GameComponents;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCInventionSystemDialog;
import com.universeprojects.gamecomponents.client.selection.GCSelectionMenu;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarker;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialStage;
import com.universeprojects.gamecomponents.client.tutorial.visualization.TutorialObjectTracker;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.*;

/**
 * Main class for tutorial system. Handles tutorial system initialization,
 * manages communications with underlying service and tracks objective
 * execution
 */
public class TutorialController {
    public static final TutorialDismissEvent EMPTY_CHANGE_EVENT = new TutorialDismissEvent();
    public static Hook<Callable1Args<Map<String, Integer>>> HOOK_RequestNearbyObject = Hooks.createHook1Args();
    public static Hook<Callable1Args<String>> HOOK_RequestControlledObject = Hooks.createHook1Args();
    public static final Hook<Callable1Args<List<String>>> HOOK_RequestTutorialObject = Hooks.createHook1Args();
    public static final Hook<Callable1Args<TutorialStage>> HOOK_StageBecameActive = Hooks.createHook1Args();

    private final Logger logger = Logger.getLogger(Tutorials.class);
    private final Set<String> triggerSet = new HashSet<>();
    private TutorialSystemState state = TutorialSystemState.ENABLED;
    private TutorialManager service;
    private Map<String, TutorialStage> tutorialStages;
    private List<TutorialStage> activeStages;
    private final TutorialObjectTracker markerManager;
    private TutorialConfig config;
    private final List<String> objectIDRequests = new ArrayList<>();
    private final Map<String, Integer> objectIDByDistanceRequests = new HashMap<>();

    public TutorialController() {
        this.markerManager = new TutorialObjectTracker(this);
        markerManager.registerCheckerObject("service", this);
    }

    public void replayTutorials() {
        if (this.config!=null)
            setConfig(this.config);
        setActive(tutorialStages.keySet().iterator().next());
    }

    public void setConfig(TutorialConfig config) {
        this.config = config;
        this.tutorialStages = new LinkedHashMap<>();
        for(TutorialStage stage : config.getTutorialStages()) {
            tutorialStages.put(stage.getId(), stage);
        }
    }

    public TutorialConfig getConfig() {
        return config;
    }

    public void setService(TutorialManager service) {
        this.service = service;
        markerManager.registerCheckerObject("controlling", service);
    }

    public void setLayer(H5ELayer layer) {
        markerManager.initMarkerComponent(layer);
    }

    public void onNetworkReady() {
        if (!markerManager.isReady()
                || service == null
                || this.tutorialStages == null)
            throw new IllegalArgumentException("Network ready was fired before the tutorial system was initialized properly (" + markerManager.isReady() + ", " + (service != null) + ", " + (tutorialStages != null) + ")");

        service.requestCompletedStages(
                (enabled) -> setLocalState(enabled ? TutorialSystemState.ENABLED : TutorialSystemState.DISABLED), (list) ->{
                    setCompletedStages(list);
                    precheckActiveObjectives();
                }

        );
    }

    void triggerObjective(String objective) {
        if (Tutorials.watchTutorialCodes) GameComponents.addSystemMessage("TRIGGER: " + objective);
        if (objective == null || activeStages == null || !state.isTrackObjectives() || !triggerSet.contains(objective))
            return;

        List<TutorialStage> newNextStages = new ArrayList<>();
        ListIterator<TutorialStage> iterator = activeStages.listIterator();
        while (iterator.hasNext()) {
            TutorialStage stage = iterator.next();
            if (!stage.getObjectives().contains(objective))
                continue;

            logger.info("Objective {} triggered for stage ID {}", objective, stage.getId());

            iterator.remove();
            service.addCompletedStage(stage.getId());
            if(stage.getMonologue() != null) {
                H5EButton dismissButton = (H5EButton)markerManager.getObject("button:sidebar:dismiss");
                if(dismissButton != null) {
                    Array<EventListener> listeners = dismissButton.getListeners();
                    int size = listeners.size;
                    for(int i=0; i<size; ++i) {
                        EventListener listener = listeners.get(i);
                        if (listener instanceof TutorialDismissListener) {
                            listener.handle(EMPTY_CHANGE_EVENT);
                        }
                    }
                }
            }

            List<String> nextStages = stage.getNextStages();
            if(nextStages != null) {
                List<TutorialStage> list = new ArrayList<>();
                Map<String, TutorialStage> stringTutorialStageMap = tutorialStages;
                for (String nextStage : nextStages) {
                    TutorialStage tutorialStage = stringTutorialStageMap.get(nextStage);
                    if (tutorialStage != null) {
                        list.add(tutorialStage);
                    }
                }
                newNextStages.addAll(list);
            }
        }

        activeStages.clear();
        activeStages.addAll(newNextStages);
        update();
    }

    public TutorialObjectTracker getMarkerManager() {
        return markerManager;
    }

    private void setCompletedStages(List<String> completedObjectives) {
        // Activate child stages of completed stages
        List<TutorialStage> newActiveStages = new ArrayList<>();
        for(String objective : completedObjectives) {
            TutorialStage stage = tutorialStages.get(objective);
            if(stage != null) {
                for(String nextStages : stage.getNextStages()) {
                    newActiveStages.add(tutorialStages.get(nextStages));
                }
            }
        }

        // Activate default stages that have not been
        // completed yet
        for (TutorialStage tutorialStage : tutorialStages.values()) {
            if (tutorialStage.isActiveByDefault()) {
                newActiveStages.add(tutorialStage);
            }
        }

        // Remove all the completed stages
        Iterator<TutorialStage> it = newActiveStages.iterator();
        //noinspection Java8CollectionRemoveIf
        while(it.hasNext()) {
            TutorialStage stage = it.next();
            if(completedObjectives.contains(stage.getId())) {
                it.remove();
            }
        }

        if(activeStages == null) {
            activeStages = new ArrayList<>(newActiveStages);
        }
        else {
            activeStages.addAll(newActiveStages);
        }

        logger.info("Tutorial Network Service synchronization completed: " + activeStages);
        update();
    }

    public void setSystemState(TutorialSystemState state) {
        if (this.state != state) {
            if (!state.isTemporary())
                service.setTutorialToggleStatus(state.isServerSideEnabled());

            setLocalState(state);
        }
    }

    private void setLocalState(TutorialSystemState value) {
        logger.info("Tutorial Service State Update: " + value);
        this.state = value;
        update();
    }

    public void resetTutorialStages() {
        resetTutorialStages(true);
    }

    public void resetTutorialStages(boolean update) {
        setCompletedStages(Collections.emptyList());
        activeStages.clear();
        updateNearbyObjectRequests();
        service.resetCompletedStages(() -> {
            if (update) {
                TutorialConfig config = getConfig();
                setConfig(config);
                if (!tutorialStages.isEmpty())
                    setActive(tutorialStages.keySet().iterator().next());
                update();
            }
        });
    }

    public TutorialSystemState getState() {
        return state;
    }

    void update() {
        if (activeStages == null)
            return;

        triggerSet.clear();

        if (state.isTrackObjectives()) {
            for (TutorialStage stage : activeStages) {
                triggerSet.addAll(stage.getObjectives());
            }
        }

        if(state == TutorialSystemState.ENABLED) {
            for (TutorialStage s : activeStages) {
                Hooks.call(HOOK_StageBecameActive, s);
            }
        }

        markerManager.refreshMarkerList(
                state.isShowVisualization()
                        ? activeStages
                        : Collections.emptyList()
        );
        precheckActiveObjectives();
        triggerWorldRequest();
        updateNearbyObjectRequests(null);
    }

    public void setActive(String tutorialName) {
        setActive(tutorialName, false);
    }

    private String getFirstNonCompleted(List<String> completed, List<String> nextStageIDs) {
        if(nextStageIDs == null) {
            return null;
        }

        for(String nextStageID : nextStageIDs) {
            if(tutorialStages.containsKey(nextStageID)) {
                if(!completed.contains(nextStageID)) {
                    return nextStageID;
                }
                else {
                    return getFirstNonCompleted(completed, tutorialStages.get(nextStageID).getNextStages());
                }
            }
        }

        return null;
    }

    private void setActive(String tutorialName, boolean yielded) {
        if(tutorialStages == null) {
            return;
        }
        if (tutorialStages.containsKey(tutorialName)) {
            TutorialStage stage = tutorialStages.get(tutorialName);
            if (!yielded) {
                service.requestCompletedStages(
                        (enabled) -> {
                        },
                        (completed) -> {
                            if (!completed.contains(stage.getId())) {
                                setActive(tutorialName, true);
                            }
                            else {
                                String firstNonCompleted = getFirstNonCompleted(completed, stage.getNextStages());
                                if(firstNonCompleted != null) {
                                    setActive(firstNonCompleted, true);
                                }
                            }
                        }
                );
            } else {
                if (activeStages == null) {
                    activeStages = new ArrayList<>();
                }
                activeStages.clear();
                activeStages.add(stage);
                update();
            }
        } else {
            Tutorials.changeTutorialBank(tutorialName);
            if (tutorialStages.containsKey(tutorialName)) {
                setActive(tutorialName, false);
            } else {
                updateNearbyObjectRequests();
            }
        }
    }

    private void triggerWorldRequest() {
        objectIDRequests.clear();
        for (TutorialStage s : tutorialStages.values()) {
            if (s.getMarkers() != null) {
                for (TutorialMarker m : s.getMarkers()) {
                    if (m.getObjectId().contains("world:") && !markerManager.hasObject(m.getObjectId())) {
                        objectIDRequests.add(m.getObjectId());
                    }
                }
            }
        }

        if(objectIDRequests.size() > 0) {
            Hooks.call(HOOK_RequestTutorialObject, objectIDRequests);
        }
    }

    /** This is called on startup and every update*/
    public void precheckActiveObjectives(){
        if(activeStages == null) {
            return;
        }
        List<TutorialStage> copyList = new ArrayList<>(activeStages);
        for(TutorialStage stage : copyList){
            if(checkTutorialCheckers(stage)){
                for (String s : stage.getObjectives()) {
                    triggerObjective(s);
                }
            }
        }
    }

    /**
     * This function is going to grab the checkers attached to this stage, if any.
     * Then it will grab the Map of registered objects that have checkable functions (ie. isOpen() on a dialog)
     * If there is a checker and an registered object with the same id it will then execute the callable(check)
     * currently the only registered object is the invention window.
     * a new window must be registered with an id and functionality must be implemented in this function as well
     * */
    @SuppressWarnings("unchecked")
    private <T> boolean checkTutorialCheckers(TutorialStage stage){
        if(stage == null)return false;
        Map<String, Object> objectCheckers = markerManager.getCheckers();
        Map<String, List<ReturningCallable1Args<?,?>>> stageCheckers = stage.getStageCheckers();
        if(objectCheckers == null || stageCheckers == null) return false;
        boolean completed = false;
        mainLoop: for(Map.Entry<String, List<ReturningCallable1Args<?,?>>> entry : stageCheckers.entrySet()){
            String id = entry.getKey();
            Object object = objectCheckers.get(id);

            for (ReturningCallable1Args<?, ?> callable : entry.getValue()) {
                if (id.equals("window:invention")) {
                    if (object instanceof GCInventionSystemDialog) {
                        ReturningCallable1Args<Boolean, GCInventionSystemDialog> castCallable = (ReturningCallable1Args<Boolean, GCInventionSystemDialog>) callable;
                        if (!castCallable.call((GCInventionSystemDialog) object)) {
                            continue mainLoop;
                        }
                        completed = true;
                        break;
                    }
                } else if (id.equals("window:select")) {
                    if (object instanceof GCSelectionMenu) {
                        ReturningCallable1Args<Boolean, GCSelectionMenu> castCallable = (ReturningCallable1Args<Boolean, GCSelectionMenu>) callable;
                        if (!castCallable.call((GCSelectionMenu) object)) {
                            continue mainLoop;
                        }
                        completed = true;
                        break;
                    }
                } else if (id.contains("world:")) {
                    ReturningCallable1Args<Boolean, String> castCallable = (ReturningCallable1Args<Boolean, String>) callable;
                    if (!castCallable.call(id.substring(id.indexOf(':') + 1))) {
                        continue mainLoop;
                    }
                    completed = true;
                    break;
                } else if (id.contains("controlling:")) {
                    ReturningCallable1Args<Boolean, String> castCallable = (ReturningCallable1Args<Boolean, String>) callable;
                    if (!castCallable.call(id.substring(id.indexOf(':') + 1))) {
                        continue mainLoop;
                    }
                    completed = true;
                    break;
                } else if (id.equals("service:inventory")) {
                    if (object != null) {
                        ReturningCallable1Args<Boolean, T> castCallable = (ReturningCallable1Args<Boolean, T>) callable;
                        //noinspection unchecked
                        if (!castCallable.call((T) object)) {
                            continue mainLoop;
                        }
                        completed = true;
                        break;
                    }
                }else if(id.equals("service:invention")){
                    if(object != null){
                        ReturningCallable1Args<Boolean, T> castCallable = (ReturningCallable1Args<Boolean, T>) callable;
                        boolean result = castCallable.call((T) object);
                        if (!result) {
                            continue mainLoop;
                        }
                        completed = true;
                        break;
                    }
                } else if(id.equals("service:anchoring")){
                    if(object != null){
                        ReturningCallable1Args<Boolean, T> castCallable = (ReturningCallable1Args<Boolean, T>)callable;
                        if(!castCallable.call((T)object)){
                            continue mainLoop;
                        }
                        completed = true;
                        break;
                    }
                }else if(id.equals("aspect:enclosed-space")){
                    if(object != null){
                        ReturningCallable1Args<Boolean, T> castCallable = (ReturningCallable1Args<Boolean, T>)callable;
                        if(!castCallable.call((T)object)){
                            continue mainLoop;
                        }
                        completed = true;
                        break;
                    }
                }
                else if (id.contains(":null:")) {
                    if (!checkWindowNull(objectCheckers, id)) {
                        continue mainLoop;
                    }
                    completed = true;
                    break;
                }
                else if (id.equals("window:equipment")) {
                    if(object != null) {
                        ReturningCallable1Args<Boolean, T> castCallable = (ReturningCallable1Args<Boolean, T>)callable;
                        if(!castCallable.call((T) object)) {
                            continue mainLoop;
                        }
                        completed = true;
                        break;
                    }
                }
            }
        }

        return completed;
    }

    public void updateNearbyObjectRequests() {
        objectIDByDistanceRequests.clear();
        for (TutorialStage s : tutorialStages.values()) {
            for (String o : s.getObjectives()) {
                if (o.contains("world:")) {
                    try {
                        String key = o.substring(o.indexOf(':') + 1, o.lastIndexOf(':'));
                        Integer distance = (int) Float.parseFloat(o.substring(o.lastIndexOf(':') + 1));
                        objectIDByDistanceRequests.put(key, distance);
                    } catch (Exception ex) {
                        logger.error("Error parsing tutorial object request "+o, ex);
                    }
                }
            }
        }

        Hooks.call(HOOK_RequestNearbyObject, objectIDByDistanceRequests);
    }

    public static Boolean updateNearbyObjectRequests(@SuppressWarnings("unused") String ignored) {
        Tutorials.updateNearbyObjectRequests();

        return false;
    }

    public void triggerNearbyObject(String worldID) {
        if (activeStages == null) {
            return;
        }
        List<TutorialStage> copyList = new ArrayList<>(activeStages);
        for (TutorialStage stage : copyList) {
            Map<String, List<ReturningCallable1Args<?, ?>>> stageCheckers = stage.getStageCheckers();
            if (stageCheckers != null) {
                for (String entry : stageCheckers.keySet()) {
                    if (entry.equals("world:" + worldID)) {
                        for (String s : stage.getObjectives()) {
                            triggerObjective(s);
                        }
                        return;
                    }
                }
            }
            for(String objective : stage.getObjectives()) {
                if (objective.equals("world:" + worldID)) {
                    for (String s : stage.getObjectives()) {
                        triggerObjective(s);
                    }
                    return;
                }
            }
        }
    }

    public static Boolean checkControlled(String worldID) {
        Hooks.call(HOOK_RequestControlledObject, worldID);
        return false;
    }

    public void triggerControlledObject(String worldID) {
        if(activeStages == null) {
            return;
        }
        List<TutorialStage> copyList = new ArrayList<>(activeStages);
        for(TutorialStage stage : copyList) {
            Map<String, List<ReturningCallable1Args<?,?>>> stageCheckers = stage.getStageCheckers();
            if(stageCheckers != null) {
                for(String entry : stageCheckers.keySet()) {
                    if(entry.equals("controlling:" + worldID)) {
                        for (String s : stage.getObjectives()) {
                            triggerObjective(s);
                        }
                        return;
                    }
                }
            }
        }
    }

    public static boolean checkWindowNull(Map<String, Object> objectCheckers, String windowName) {
        String windowNameActual = windowName.replace(":null:", ":");
        return !objectCheckers.containsKey(windowNameActual);
    }
}
