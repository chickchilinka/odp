package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.utils.RandomNameGenerator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.SpawnOptionData;

import java.util.Random;

public abstract class GCCharacterSpawnMobileBase<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> extends GCCharacterSpawnBase<L, S, C>  {
    protected static final int LOGIN_FIELD_W = 350;
    protected final GCWindow characterNameWindow;
    protected final H5EInputBox fieldCharacterName;
    protected final H5ELabel characterWarningText;
    protected final Color warningColor;
    public static final int ICON_SIZE = 64;
    final float warningTextFontSize = 0.75f;

    public GCCharacterSpawnMobileBase(H5ELayer layer, GCLoginManager<L, S, C> loginManager) {
        super(layer, loginManager, true);

        spawnSelectDialog.positionProportionally(0.5f, 0.5f);

        layer.getEngine().onResize.registerHandler(() -> spawnSelectDialog.positionProportionally(0.5f, 0.5f));

        characterNameWindow = new GCWindow(layer, 444, 300, "login-window");
        characterNameWindow.setId("login-screen");
        characterNameWindow.setTitle("NAME YOUR CHARACTER");
        characterNameWindow.positionProportionally(0.5f, 0.5f);
        characterNameWindow.setPackOnOpen(false);
        characterNameWindow.setMovable(false);
        characterNameWindow.defaults().center();
        characterNameWindow.setCloseButtonEnabled(false);
        warningColor = Color.RED;
        //Align title onto tab at the top
        characterNameWindow.padTop(40);
        characterNameWindow.getTitleTable().padRight(20);

        //Create login tab

//        H5ELabel characterNameLabel = new H5ELabel(layer);
//        characterNameLabel.setFontScale(loginInfoFontSize);
//        characterNameLabel.setText("Character Name");

        //Space to enter email
        fieldCharacterName = new H5EInputBox(layer);
        fieldCharacterName.setWidth(LOGIN_FIELD_W);

        //Warning text for invalid emails
        characterWarningText = new H5ELabel(layer);
        characterWarningText.setFontScale(warningTextFontSize);
        characterWarningText.setColor(warningColor);
        characterWarningText.setAlignment(Align.center);
        characterWarningText.setVisible(false);
        characterWarningText.setWrap(true);

        H5EButton btnNext = new H5EButton("Next", layer);
        btnNext.addButtonListener(this::selectSpawnOption);
        H5EButton btnBack = new H5EButton("Back", layer);
        btnBack.addButtonListener(this::back);

        // Randomize name button (temporary)
        ImageButton btnRandomName = new ImageButton(layer.getEngine().getSkin(), "btn-random");
        btnRandomName.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                String randomName = RandomNameGenerator.generateRandomName(2, 4); 
                fieldCharacterName.setText(randomName);
            }
        });

        Table characterNameTable = new Table();
        characterNameTable.add(fieldCharacterName).width(350);
        characterNameTable.add(btnRandomName);

//        characterNameWindow.add(characterNameLabel);
//        characterNameWindow.row();
        characterNameWindow.add(characterNameTable).colspan(2).width(350);
        characterNameWindow.row();
        characterNameWindow.add(characterWarningText).colspan(2).width(400);
        characterNameWindow.row();
        characterNameWindow.add(btnBack).width(150);
        characterNameWindow.add(btnNext).width(150);

        spawnSelectDialog.onDoubleClick = (option) -> {
            this.selectedSpawnOption = option;
            executeSpawn();
        };
    }

    // reverse the order of the windows
    protected void selectSpawnOption() {
        if (validateSpawn()) {
            spawnSelectDialog.open();
            spawnSelectDialog.positionProportionally(0.5f, 0.5f);
        }
    }

    @Override
    protected void onSpawnSelected(SpawnOptionData option) {
        super.onSpawnSelected(option);
        executeSpawn();
    }

    protected void processError(String error) {
        loadingIndicator.deactivate();
        if ("char_in_use_ingame".equals(error)) {
            characterWarningText.setText("Character name is already in use in this game. \nPlease use a different name.");
        } else if ("char_owned_by_other_user".equals(error)) {
            characterWarningText.setText("Character name is already in use by another player. \nPlease use a different name.");
        } else {
            characterWarningText.setText("An unexpected error happened while creating the character. \nPlease try again later. Error: "+error);
        }
        characterWarningText.setVisible(true);
        fieldCharacterName.setColor(warningColor);
    }

    protected void displayWarning(String text) {
        characterWarningText.setText(text);
        characterWarningText.setVisible(true);
        fieldCharacterName.setColor(warningColor);
    }

    public GCWindow getWindow() {
        return characterNameWindow;
    }

    public void open(L loginInfo, S serverInfo) {
        this.loginInfo = loginInfo;
        this.serverInfo = serverInfo;
        characterNameWindow.open();
        characterNameWindow.positionProportionally(0.5f, 0.5f);
    }

    public void close() {
        characterWarningText.setText("");
        characterWarningText.setVisible(false);
        loadingIndicator.deactivate();
        characterNameWindow.close();
        friendWindow.close();
        characterSpawnWindow.close();
    }

    public boolean isOpen() {
        return characterNameWindow.isOpen();
    }
}
