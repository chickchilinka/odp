package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.utils.Align;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCLoginOrganisationItem extends GCListItem {

    private final String name;
    private final Long orgId;

    public GCLoginOrganisationItem(H5ELayer layer, GCLoginOrganisationListHandler handler, String name, Long charId){
        super(layer, handler);
        if(name == null) {
            name = "";
        }
        this.name = name;
        this.orgId = charId;

        final GCLabel nameLabel = add(new GCLabel(layer)).left().growX().padLeft(5).height(50).getActor();
        nameLabel.setText(this.name);
        nameLabel.setAlignment(Align.left);

    }

    public String getName(){
        return name;
    }

    public Long getOrgId() {
        return orgId;
    }

    public static class GCLoginOrganisationListHandler extends GCListItemActionHandler<GCLoginOrganisationItem>{

        @Override
        protected void onSelectionUpdate(GCLoginOrganisationItem newItemSelection) {

        }
    }
}
