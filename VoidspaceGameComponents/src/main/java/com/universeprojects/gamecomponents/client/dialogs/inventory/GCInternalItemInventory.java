package com.universeprojects.gamecomponents.client.dialogs.inventory;

public interface GCInternalItemInventory {

    <KS, I extends GCInventoryItem> void moveItemToHere(GCInventoryBase<KS, ?, ?> startInventoryBase, KS startKey, I item);

    <I extends GCInventoryItem> boolean canAcceptItem(I item);

    boolean canAutoUnlink();
}
