package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class GCList<T extends GCListItem> extends H5EContainer {
    public static final int DEFAULT_SPACING = 5;

    private final boolean fillRowFirst;

    private int columns;

    private final SnapshotArray<T> itemList;
    private final int colSpacing;
    private final int rowSpacing;

    private final ButtonGroup<T> buttonGroup;

    public GCList(H5ELayer layer, int columns, boolean fillRowFirst) {
        this(layer, columns, DEFAULT_SPACING, DEFAULT_SPACING, fillRowFirst);
    }

    /**
     * Constructs a new "item list" component, capable of displaying lists of items in a grid.
     *
     * @param layer        The graphics layer to place this element in
     * @param columns      The number of columns to use
     * @param rowSpacing   The amount of space between rows
     * @param colSpacing   The amount of space between columns
     * @param fillRowFirst When TRUE, the list will be filled horizontally from left to right, row after row
     */
    public GCList(H5ELayer layer, int columns, int rowSpacing, int colSpacing, boolean fillRowFirst) {
        super(layer);

        left().top();

        if (colSpacing < 0 || rowSpacing < 0) {
            throw new IllegalArgumentException("Row/column spacing values can't be negative: row=" + rowSpacing + ", col=" + colSpacing);
        }

        this.colSpacing = colSpacing;
        this.rowSpacing = rowSpacing;

        this.columns = columns;

        this.fillRowFirst = fillRowFirst;


        this.itemList = new SnapshotArray<>();

        this.buttonGroup = new ButtonGroup<>();
        buttonGroup.setMinCheckCount(0);

        defaults().fill().padLeft(colSpacing).padBottom(rowSpacing);
        clear();
    }

    @Override
    public void clear() {
        itemList.clear();
        buttonGroup.clear();
        invalidateHierarchy();
    }

    @Override
    public void layout() {
        final Array<Cell> cells = getCells();
        if (cells.size < itemList.size) {
            int start = cells.size;
            for (int i = start; i < itemList.size; i++) {
                final Cell cell = super.add((Actor) null);
                cell.padLeft(colSpacing).padBottom(rowSpacing).growX().fillY();
                if (i % columns == columns - 1) {
                    cell.row();
                }
            }
        }
        if (fillRowFirst) {
            for (int i = 0; i < cells.size; i++) {
                cells.get(i).setActor(i < itemList.size ? itemList.get(i) : null);
            }
        } else {
            float height = getHeight();
            float itemHeight = 0;
            for (int i = 0; i < itemList.size; i++) {
                itemHeight = Math.max(itemHeight, itemList.get(i).getHeight() + rowSpacing);
            }
            final int numRows = Math.max(1, (int) Math.floor(height / itemHeight));
            for (int i = 0; i < itemList.size; i++) {
                int col = i / numRows;
                int row = i % numRows;
                int num = row * columns + col;
                cells.get(num).setActor(itemList.get(i));
            }

            for (int i = itemList.size; i < cells.size; i++) {
                int col = i / numRows;
                int row = i % numRows;
                int num = row * columns + col;
                cells.get(num).setActor(null);
            }
        }

        super.layout();
    }

    public int getColumnCount() {
        return columns;
    }

    public void addItem(T item) {
        if (item == null) {
            throw new IllegalArgumentException("Item reference can't be null");
        }
        itemList.add(item);
        buttonGroup.add(item);
        layout();
        invalidateHierarchy();
    }

    public void removeItem(T item) {
        if (item == null) {
            throw new IllegalArgumentException("Item reference can't be null");
        }
        itemList.removeValue(item, true);
        buttonGroup.remove(item);
        layout();
        invalidate();
    }

    @Override
    public float getPrefWidth() {
        validate();
        return super.getPrefWidth();
    }

    @Override
    public float getPrefHeight() {
        validate();
        return super.getPrefHeight();
    }

    public ButtonGroup<T> getButtonGroup() {
        return buttonGroup;
    }

    @Deprecated
    @Override
    public <E extends Actor & GraphicElement> E addChild(E child) {
        throw new IllegalStateException("Please use addItem!");
    }

    @Deprecated
    @Override
    public <E extends Actor> Cell<E> add(E actor) {
        throw new IllegalStateException("Please use addItem!");
    }

    @Deprecated
    @Override
    public Table add(Actor... actors) {
        throw new IllegalStateException("Please use addItem!");
    }

    @Deprecated
    @Override
    public Cell<Label> add(CharSequence text) {
        throw new IllegalStateException("Please use addItem!");
    }

    @Deprecated
    @Override
    public Cell<Label> add(CharSequence text, String labelStyleName) {
        throw new IllegalStateException("Please use addItem!");
    }

    @Deprecated
    @Override
    public Cell<Label> add(CharSequence text, String fontName, Color color) {
        throw new IllegalStateException("Please use addItem!");
    }

    @Deprecated
    @Override
    public Cell<Label> add(CharSequence text, String fontName, String colorName) {
        throw new IllegalStateException("Please use addItem!");
    }

    @Deprecated
    @Override
    public Cell add() {
        throw new IllegalStateException("Please use addItem!");
    }

    public void setColumns(int columns){
        this.columns=columns;
        clearChildren();
        layout();
    }


    public T get(int index) {
        return itemList.get(index);
    }

    public int size() {
        return itemList.size;
    }

    public boolean isEmpty() {
        return itemList.size == 0;
    }

    public SnapshotArray<T> getItems() {
        return itemList;
    }

    public int indexOf(T item) {
        return itemList.indexOf(item, true);
    }

    public int getRowSpacing() {
        return rowSpacing;
    }

    public T getSelectedItem() {
        return buttonGroup.getChecked();
    }

    public void setMaxCheckCount(int count) {
        buttonGroup.setMaxCheckCount(count);
    }

    public Array<T> getSelectedItems() {
        return buttonGroup.getAllChecked();
    }
}
