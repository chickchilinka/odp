package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.html5engine.client.framework.H5EEngine;

public class GraphicsTestLoadingScreen extends LoadingScreenBase {

    private float duration;
    private int currentCount = 0;
    private Image background;
    private Image loadingBackground;
    private Image loadingBar;
    private H5EEngine engine;

    public GraphicsTestLoadingScreen(H5EEngine engine ,float myTime){
        super();
        duration = myTime;
        this.engine = engine;
        background = new Image(engine.getSkin(), "login-background");
        background.setScaling(Scaling.fill);
        background.setScaleX(1.1f);
        Texture tempLoading = new Texture(Gdx.files.internal("images/GUI/ui2/preload/loading-bar-background-borderless.png"));
        Texture tempBar = new Texture(Gdx.files.internal("images/GUI/ui2/preload/loading-bar-borderless.png"));
        loadingBackground = new Image(tempLoading);
        loadingBar = new Image(tempBar);
        initialY = Gdx.graphics.getHeight() / 2 - loadingBarBackground.getHeight() / 2 - 15;
        loadingBackground.setX(initialX);
        loadingBackground.setY(initialY);
        loadingBar.setX(initialX + 25);
        loadingBar.setY(initialY + (loadingBar.getImageHeight() / 2) - 2);

    }

    @Override
    public void render() {
        float currentWidth = calculateBarWidth(currentCount);
        loadingBar.setWidth(currentWidth);

        loadingBar.setX(initialX + 25);
        loadingBar.setY(initialY + (loadingBar.getImageHeight() / 2) - 2);
        currentCount++;
        loadingBackground.toFront();
    }

    @Override
    protected float calculateBarWidth(float elapsedTime) {
        float barWidth;
        barWidth = loadingBackground.getWidth() * (elapsedTime/duration/2);
        return barWidth;
    }

    public Image getBackground(){
        return background;
    }
    public Image getLoadingBackground() {return loadingBackground;}
    public Image getLoadingBar(){return loadingBar;}
}
