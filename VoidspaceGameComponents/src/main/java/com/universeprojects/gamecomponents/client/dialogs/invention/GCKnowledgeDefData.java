package com.universeprojects.gamecomponents.client.dialogs.invention;

import java.util.ArrayList;
import java.util.List;

public class GCKnowledgeDefData {

    public List<GCKnowledgeDefData.GCKnowledgeDefDataItem> children = new ArrayList<>();

    public GCKnowledgeDefData() {
    }

    public static GCKnowledgeDefData createNew() {
        return new GCKnowledgeDefData();
    }

    public GCKnowledgeDefData.GCKnowledgeDefDataItem add(String name, boolean isParentCategory, String description, double maxExperience) {
        int index = children.size();
        GCKnowledgeDefData.GCKnowledgeDefDataItem item = new GCKnowledgeDefData.GCKnowledgeDefDataItem(index, name, isParentCategory, description, maxExperience);
        children.add(item);
        return item;
    }

    public GCKnowledgeDefData.GCKnowledgeDefDataItem get(int index) {
        return children.get(index);
    }

    public int size() {
        return children.size();
    }

    public static class GCKnowledgeDefDataItem extends GCKnowledgeDefData {
        public int index;
        public String name;
        public boolean isParentCategory;
        public String description;
        public double maxExperience;

        public GCKnowledgeDefDataItem(int index, String name, boolean isParentCategory, String description, double maxExperience) {
            this.index = index;
            this.name = name;
            this.isParentCategory = isParentCategory;
            this.description = description;
            this.maxExperience = maxExperience;
        }
    }
}
