package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.vsdata.shared.SpawnOptionData;


public class GCMobileCharacterRespawnScreen<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> extends GCCharacterSpawnMobileBase<L, S, C> {

    protected C characterInfo;

    public GCMobileCharacterRespawnScreen(H5ELayer layer, GCLoginManager<L, S, C> loginManager) {
        super(layer, loginManager);
    }

    @Override
    protected boolean validateSpawn() {
        return true;
    }

    @Override
    protected void spawn(String spawnType, JSONObject spawnParams) {
        loginManager.respawn(loginInfo, serverInfo, characterInfo, spawnType, spawnParams, this::close, this::processError);
    }

    public void open(L loginInfo, S serverInfo, C characterInfo) {
        this.loginInfo = loginInfo;
        this.serverInfo = serverInfo;
        this.characterInfo = characterInfo;
        spawnSelectDialog.open();
        fieldCharacterName.setText(characterInfo.getName());
        spawnSelectDialog.activateLoadingIndicator();
        loginManager.loadRespawnOptions(loginInfo, serverInfo, characterInfo, (options) -> {
            final GCSpawnSelectDialog.GCSpawnSelectData spawnOptions = new GCSpawnSelectDialog.GCSpawnSelectData();
            for (SpawnOptionData option : options) {
                spawnOptions.add(option);
            }
            spawnOptions.add(loginManager.getDefaultSpawnConfigs());


            spawnSelectDialog.populateData(spawnOptions);
            spawnSelectDialog.deactivateLoadingIndicator();
        }, this::processError);
    }

    @Override
    protected void back()  {
        close();
        loginManager.backToCharacterSelection();
    }
}

