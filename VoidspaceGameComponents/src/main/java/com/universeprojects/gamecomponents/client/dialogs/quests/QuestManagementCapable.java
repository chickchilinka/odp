package com.universeprojects.gamecomponents.client.dialogs.quests;

public interface QuestManagementCapable {

    void skipQuest(String questDefKey);

    void markActive(String questDefKey);

    void markCompleted(String questDefKey);
}
