package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class UIFactory {
	private Table table;
	private H5ELayer layer;

	private static final float   DEFAULT_FONT_SCALE = 1.0f;
	public  static final String  DEFAULT_FONT_COLOR = "#CCCCCC";
	private static final int     DEFAULT_ALIGNMENT  = Align.left;
	private static final boolean DEFAULT_WRAP       = true;

	public UIFactory (Table table, H5ELayer layer) {
		this.table = table;
		this.layer = layer;
	}

	/**
	 * Adds a line break of the specified height, overriding the default line break size
	 * The requested value must be greater than zero
	 */
	public UIFactory addEmptyLine (int height) {
		if (height <= 0) {
			throw new IllegalArgumentException("Custom line break height must be greater than zero: " + height);
		}

		table.row().padTop(height);
		table.add();
		table.row();

		return this;
	}

	public UIFactory row () {
		table.row();

		return this;
	}

	/**
	 * Adds a header with a slightly larger font than the default label
	 */
	public UIFactory addH1(String caption) {
		return addLabel(caption, 1.2f, DEFAULT_FONT_COLOR, DEFAULT_ALIGNMENT, DEFAULT_WRAP);
	}

	/**
	 * Adds a label
	 */
	public UIFactory addLabel(String caption, float fontScale, String fontColor, int alignment, boolean doWrap, String styleName, int colspan) {
		H5ELabel element = new H5ELabel(layer);

		element.setStyle(layer.getEngine().getSkin().get(styleName, Label.LabelStyle.class));

		element.setText(caption);
		element.setFontScale(fontScale);
		element.setColor(Color.valueOf(fontColor));
		element.setAlignment(alignment);
		element.setWrap(doWrap);

		table.add(element).colspan(colspan).grow();
		return this;
	}

	public UIFactory addLabel(String caption, float fontScale, String fontColor, int alignment, boolean doWrap) {
		return addLabel(caption, fontScale, fontColor, alignment, doWrap, "label-bold", 1);
	}

	/**
	 * Add a label with the default style
	 */
	public UIFactory addLabel(String caption) {
		return addLabel(caption, DEFAULT_FONT_SCALE, DEFAULT_FONT_COLOR, DEFAULT_ALIGNMENT, DEFAULT_WRAP, "label-bold", 1);
	}
}
