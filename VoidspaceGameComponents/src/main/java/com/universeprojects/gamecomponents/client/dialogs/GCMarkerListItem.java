package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.MapMarkerData;

public class GCMarkerListItem extends GCListItem {

    private MapMarkerData data;
    private ImageButton removeBtn;
    private ImageButton excludeBtn;

    public GCMarkerListItem(H5ELayer layer, GCListItemActionHandler<com.universeprojects.gamecomponents.client.dialogs.GCMarkerListItem> handler, MapMarkerData data, Callable2Args<com.universeprojects.gamecomponents.client.dialogs.GCMarkerListItem, Boolean> onDelete) {
        super(layer, handler);
        this.data = data;
        Table subTable = new Table();
        add(subTable).grow();
        subTable.add(H5EIcon.fromSpriteType(layer, data.icon)).left();
        H5ELabel label = new H5ELabel(data.name, layer);
        subTable.add(label).growX().left().padLeft(10);
        //removeBtn = new ImageButton(layer.getEngine().getSkin(), "btn-remove");
        excludeBtn = new ImageButton(layer.getEngine().getSkin(), "btn-close-window");
        //subTable.add(removeBtn).right().top();
        subTable.add(excludeBtn).right().top();
        //removeBtn.setVisible(false);
        excludeBtn.setVisible(false);
//        removeBtn.addListener(new ClickListener() {
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                onDelete.call(GCMarkerListItem.this, true);
//            }
//        });
        excludeBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                onDelete.call(GCMarkerListItem.this, false);
            }
        });
        subTable.left().top();
    }

    public void showControls(boolean show) {
        //removeBtn.setVisible(show);
        excludeBtn.setVisible(show);
    }

    @Override
    public void setStage(Stage stage) {
        super.setStage(stage);
    }

    public MapMarkerData getData() {
        return data;
    }
}