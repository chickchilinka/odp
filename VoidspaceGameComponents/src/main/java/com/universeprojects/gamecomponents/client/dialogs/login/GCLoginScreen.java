package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.auth.AuthCredentials;
import com.universeprojects.gamecomponents.client.auth.PlatformAuthOption;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Creates the UI for players to login.
 */
public class GCLoginScreen<L extends LoginInfo> extends GCLoginInformation {

    private final GCLoginManager<L, ?, ?> loginManager;
    private final Logger log = Logger.getLogger(GCLoginManager.class);
    private final GCLoadingIndicator loadingIndicator;
    private final H5EButton checkBox;

    public GCLoginScreen(H5ELayer layer, GCLoginManager<L, ?, ?> loginManager) {
        super(layer);
        this.loginManager = loginManager;
        loadingIndicator = new GCLoadingIndicator(layer);
        window.setTitle("Sign In");
        window.setCloseButtonEnabled(false);
        //Add "email" text
        window.row().padTop(10);
        window.add(emailLabel);

        //Space to enter email
        window.row().padTop(10);
        window.add(fieldEmail).width(LOGIN_FIELD_W);

        //Warning text for invalid emails
        window.row().padTop(5);
        window.add(emailWarningText);

        //Add "password" text
        window.row().padTop(5);
        window.add(passwordLabel);

        //Space to enter password
        window.row().padTop(10);
        window.add(fieldPassword).width(LOGIN_FIELD_W);

        //Warning text for invalid passwords
        window.row().padTop(10);
        window.add(passwordWarningText);

        window.row().padTop(10);

        checkBox = ButtonBuilder.inLayer(layer).withStyle("default-checkable").build();
        checkBox.setRawText("Remember me");
        checkBox.setChecked(true);
        window.add(checkBox);

        //Login button
        window.row().padTop(10);
        final H5EButton btnLogin = new H5EButton("login", layer);
        btnLogin.addButtonListener(this::submitLoginInfo);
        btnLogin.getStyle().checked=null;
        btnLogin.setUserObject("button:login");
        window.add(btnLogin).size(200, btnLogin.getHeight() ).getActor();

        //Register Button
        window.row().padTop(5);
        final H5EButton btnRegister = new H5EButton("Register", layer);
        btnRegister.addButtonListener(this::openRegisterWindow);
        btnRegister.getStyle().checked=null;
        window.add(btnRegister).size(200, btnLogin.getHeight() ).getActor();
        //Get default colour from text fields

        if (!loginManager.getPlatformAuthOptions().isEmpty()) {
            window.row();
            window.add(new H5ELabel("Sign in with:", layer));
            window.row();
            for (PlatformAuthOption option : loginManager.getPlatformAuthOptions()) {
                Button button = option.newSignInButton(layer);
                if(button != null) {
                    window.add(button).width(150F).height(60F);
                }
                option.setAuthCallback(this::checkOauthLogin);
                option.setOnErrorCallback(this::oauthOnError);
            }
        }
        //Tie both fields to enter key
        UPUtils.tieButtonToKey(fieldEmail, Input.Keys.ENTER, btnLogin);
        UPUtils.tieButtonToKey(fieldPassword, Input.Keys.ENTER, btnLogin);
    }

    private void oauthOnError(String msg) {
        passwordWarningText.setText(msg);
        passwordWarningText.setVisible(true);
    }

    private void checkOauthLogin(AuthCredentials credentials) {
        if(credentials == null) {
            return;
        }
        loadingIndicator.activate(window);
        loginManager.login(credentials, loginInfo -> {
            loadingIndicator.deactivate();
            close();
            loginManager.onSuccessfulLogin(loginInfo);
            if (checkBox.isChecked()) {
                loginManager.setSavedToken(loginInfo);
            } else {
                loginManager.setSavedToken(null);
            }
        }, error -> {
            loginManager.setSavedToken(null);
            loadingIndicator.deactivate();
            if (GCLoginManager.CODE_COMMUNICATION.equals(error.getMessage())) {
                onLoginError();
            } else {
                log.error("OAuth login failed", error);
                passwordWarningText.setText("Unable to process your authentication request.");
                passwordWarningText.setVisible(true);
                fieldPassword.setColor(warningColor);
                fieldEmail.setColor(warningColor);
            }
        });
    }

    private void submitLoginInfo(){
        Gdx.input.setOnscreenKeyboardVisible(false);
        emailWarningText.setVisible(false);
        passwordWarningText.setVisible(false);

        fieldEmail.setColor(normalColor);
        fieldPassword.setColor(normalColor);


        if(!isValidEmail()) {
            return;
        }
        if(!isValidPassword()){
            return;
        }
        processLogin(getEmail(), getPassword());
    }

    public void onLoginError() {
        passwordWarningText.setText("Error communicating with the server. Please try again later.");
        passwordWarningText.setVisible(true);
    }

    public void onLoginFailed(String error) {
        String message;

        switch (error) {
            case "Authentication error: Bad login":
                message = "Your email or password is incorrect. Please try again.";
                break;

            case "password-reset-required":
                message = "Please reset your password to login to your account from this device.";
                break;

            default:
                message = "Unable to process your login due to an unexpected error.\n[error code: " + error + "]";
                break;
        }

        log.error("Unable to process your login. Error {}", error);
        passwordWarningText.setText(message);
        passwordWarningText.setVisible(true);
        fieldPassword.setColor(warningColor);
        fieldEmail.setColor(warningColor);
    }

    private void processLogin(String email, String password) {
        loadingIndicator.activate(window);
        Map<String, Object> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        AuthCredentials credentials = new AuthCredentials("voidspace", params);
        loginManager.login(credentials, (loginInfo) -> {
            loadingIndicator.deactivate();
            close();
            loginManager.onSuccessfulLogin(loginInfo);
            if (checkBox.isChecked()) {
                loginManager.setSavedToken(loginInfo);
            } else {
                loginManager.setSavedToken(null);
            }
        }, (error) -> {
            loginManager.setSavedToken(null);
            loadingIndicator.deactivate();
            if (GCLoginManager.CODE_COMMUNICATION.equals(error.getMessage())) {
                onLoginError();
            } else {
                onLoginFailed(error.getMessage());
            }
        });
    }

    @Override
    public void close() {
        super.close();
        Gdx.input.setOnscreenKeyboardVisible(false);

        for (PlatformAuthOption authOption : loginManager.getPlatformAuthOptions()) {
            authOption.shutdown();
        }
    }

    private void openRegisterWindow() {
        close();
        loginManager.openRegisterWindowWithPolicies();
    }

}
