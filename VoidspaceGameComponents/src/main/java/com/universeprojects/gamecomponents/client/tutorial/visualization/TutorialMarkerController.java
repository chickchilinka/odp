package com.universeprojects.gamecomponents.client.tutorial.visualization;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.tutorial.TutorialController;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarker;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarkerStyle;
import com.universeprojects.gamecomponents.client.tutorial.visualization.animations.GeneralTooltipAnimation;
import com.universeprojects.gamecomponents.client.tutorial.visualization.animations.TutorialArrowAnimation;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;

import java.util.ArrayList;
import java.util.List;

/**
 * This components received a list of registered actors
 * and corresponding TutorialMarkers from {@link TutorialObjectTracker}
 * and performs further checks to find a visible tutorial marker target with
 * highest priority.
 * <p>
 * Once the target was identified, this classes updates decides which visual
 * clues will be used for this marker and updates their visability (hiding
 * unnecessary objects and showing relevant ones).
 * <p>
 * After visual clues have been initialized, this class is also responsible for
 * tracking target's position and moving visual clues
 */
public class TutorialMarkerController extends Actor {
    private static final float ARROW_OFFSET = 40;
    private static final float ANIMATION_DURATION = 2F;

    @SuppressWarnings("unused")
    private final TutorialController tutorialController;
    private final H5ELayer layer;
    private final H5ESprite arrowActor;

    private final List<ActorMarkerPair> targets;
    private ActorMarkerPair lastActiveActor;
    private boolean forceStateUpdate = false;

    private TutorialArrowAnimation arrowBounceAnimation;
    private GeneralTooltipAnimation generalTooltipAnimation;
    private Vector2 globalCoordsVector = new Vector2();
    private Vector3 tempVec = new Vector3();
    private Vector3 tempVec2 = new Vector3();
    private Vector2 tempVec2D = new Vector2();
    private Vector2 tempVec2D2 = new Vector2();

    public TutorialMarkerController(TutorialController tutorialController, H5ELayer layer) {
        this.tutorialController = tutorialController;
        this.layer = layer;

        arrowBounceAnimation = new TutorialArrowAnimation(ANIMATION_DURATION, ARROW_OFFSET);
        generalTooltipAnimation = new GeneralTooltipAnimation(0.15f);
        targets = new ArrayList<>();

        arrowActor = new H5ESprite(layer, "images/icons/tutorial-marker.png");
        arrowActor.addTo(layer);
        arrowActor.setVisible(false);
        arrowActor.setTouchable(Touchable.disabled);
        arrowActor.setOrigin(Align.center);
    }

    /**
     * Checks if object is visible on the screen
     * and updates globalCoordsVector with its
     * screen coordinates.
     * <p>
     * This method is basically a combination of
     * Actor.isHierarchyVisible() and Actor.localToStageCoordinates()
     * with the intent to combine both of them to minimize the performance
     * drop due to the necessity to travel the layout tree
     */
    protected boolean checkHierarchyFindCoords(Actor actor, boolean worldActor) {
        tempVec2D.set(0, 0);
        Actor currentActor = actor;
        do {
            if (!currentActor.isVisible() || currentActor.getStage() == null) {
                return false;
            }
            currentActor.localToParentCoordinates(tempVec2D);
            currentActor = currentActor.getParent();
        } while (currentActor != null);
        if(worldActor) {
            if(actor.getStage() != null && actor.getStage().getCamera() != null) {
                tempVec.set(tempVec2D.x, tempVec2D.y, 0);
                actor.getStage().getCamera().project(tempVec);
                tempVec2D.set(tempVec.x, tempVec.y);
            }
        }
        globalCoordsVector.set(tempVec2D.x, tempVec2D.y);
        return true;
    }

    protected ActorMarkerPair findFirstVisibleTarget() {
        float centerX = layer.getEngine().getWidth() /2f;
        float centerY = layer.getEngine().getHeight() /2f;
        ActorMarkerPair closestPair = null;
        float closestDist2 = Float.MAX_VALUE;
        Vector2 vec = new Vector2();
        for (ActorMarkerPair pair : targets) {
            // If has no requirements for actor visibility, or
            // target actor is visible
            final Actor actor = pair.getActor();
            if (actor == null) {
                if(closestPair == null) {
                    closestPair = pair;
                }
                continue;
            }

            boolean found = false;

            if (pair.getMarker().getStyle() == TutorialMarkerStyle.ARROW) {
                final Object userObject = actor.getUserObject();
                final boolean isWorldActor = userObject instanceof String && ((String) userObject).contains("world:");
                final boolean foundValidTarget = checkHierarchyFindCoords(actor, isWorldActor);
                if (foundValidTarget) {
                    found = true;
                }
            }

            if(found || (actor.getStage() != null && isHierarchyVisible(actor))) {
                vec.setZero();
                actor.localToScreenCoordinates(vec);
                float dist2 = vec.dst2(centerX, centerY);
                if(dist2 < closestDist2) {
                    closestDist2 = dist2;
                    closestPair = pair;
                }
            }
        }

        return closestPair;
    }

    private boolean isHierarchyVisible(Actor actor){
        Actor currentActor = actor;
        do {
            if (!currentActor.isVisible()) {
                return false;
            }
            currentActor = currentActor.getParent();
        } while (currentActor != null);
        return true;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        ActorMarkerPair target = findFirstVisibleTarget();
        if (target != lastActiveActor || forceStateUpdate) {
            updateState(target);
            lastActiveActor = target;
            forceStateUpdate = false;
        }

        if (target == null)
            return;

        TutorialMarker marker = target.getMarker();

        if (marker.getStyle() == TutorialMarkerStyle.ARROW) {
            updateArrowPosition(delta, target.getActor(), (target.getActor().getUserObject() != null && ((String) target.getActor().getUserObject()).contains("world:")));
        }
    }

    private void updateArrowPosition(float delta, Actor targetActor, boolean worldActor) {
        // Attention! It's critically important that globalCoordsVector has been updated
        // with actual data. At this moment it is being done using checkHierarchyFindCoords method
        float width = targetActor.getWidth() * targetActor.getScaleX();
        if(worldActor) {
            tempVec.set(targetActor.getX()+targetActor.getOriginX(), targetActor.getY(), 0);
            if(targetActor.getStage() != null && targetActor.getStage().getCamera() != null) {
                targetActor.getStage().getCamera().project(tempVec);
            }

            globalCoordsVector.set(tempVec.x, tempVec.y).scl(1f / layer.getEngine().getUiScale());
            width = 0;
        }
        float arrowWidth = arrowActor.getWidth();
        float arrowHeight = arrowActor.getHeight();

        float arrowBaseX = globalCoordsVector.x + (width - arrowWidth) / 2;
        float arrowBaseY = globalCoordsVector.y - ARROW_OFFSET - arrowHeight;

        if(!worldActor) {
            if (layer.getHeight() - arrowBaseY > layer.getHeight()-arrowHeight) {
                arrowBaseY += arrowHeight * 2;
                arrowActor.setRotation(180);
            } else {
                arrowActor.setRotation(0);
            }
        }

        tempVec2D2.set(0, 1);

        if(worldActor) {
            boolean rotate = false;
            if(arrowBaseX < arrowWidth) {
                arrowBaseX = arrowWidth;
                rotate = true;
            }
            else {
                int screenWidth = layer.getViewport().getScreenWidth();
                if(arrowBaseX > screenWidth -(arrowWidth)) {
                    arrowBaseX = screenWidth -(arrowWidth);
                    rotate = true;
                }
            }
            if(arrowBaseY < arrowHeight*2) {
                arrowBaseY = arrowHeight*2;
                rotate = true;
            }
            else {
                int screenHeight = layer.getViewport().getScreenHeight();
                if(arrowBaseY > screenHeight -(arrowHeight*2)) {
                    arrowBaseY = screenHeight -(arrowHeight*2);
                    rotate = true;
                }
            }

            if(rotate) {
                tempVec2.set(arrowBaseX, arrowBaseY, 0); //to object
                tempVec.set(globalCoordsVector.x, globalCoordsVector.y, 0).sub(tempVec2);
                tempVec2D.set(0, 1);
                tempVec2D2.set(tempVec.x, tempVec.y);
                float angle = tempVec2D.angle(tempVec2D2);
                arrowActor.setRotation(angle);
                tempVec2D2.nor();
            }
            else {
                arrowActor.setRotation(0);
            }
        }

        float animatedAmount = arrowBounceAnimation.animate(delta);

        arrowActor.setPosition(arrowBaseX + (tempVec2D2.x * animatedAmount),
                arrowBaseY + (tempVec2D2.y * animatedAmount));
        arrowActor.setZIndex(Integer.MAX_VALUE);

    }

    @SuppressWarnings("Duplicates")
    private void updateState(ActorMarkerPair target) {
        if (target == null) {
            arrowActor.setVisible(false);
            return;
        }

        TutorialMarker marker = target.getMarker();

        // Update tooltips
        if (marker.getStyle() == TutorialMarkerStyle.ARROW) {
            arrowActor.setVisible(true);
            arrowActor.toFront();

        } else if (marker.getStyle() == TutorialMarkerStyle.TOOLTIP) {
            arrowActor.setVisible(false);
            generalTooltipAnimation.reset();
        }
    }

    public void clearTargets() {
        targets.clear();
        lastActiveActor = null;
        forceStateUpdate = true;
    }

    public void addTarget(ActorMarkerPair pair) {
        if (pair.getActor() == null && pair.getMarker().getStyle() == TutorialMarkerStyle.ARROW)
            throw new IllegalArgumentException("TutorialMarker must have a target actor when in ARROW mode");

        targets.add(pair);
    }
}
