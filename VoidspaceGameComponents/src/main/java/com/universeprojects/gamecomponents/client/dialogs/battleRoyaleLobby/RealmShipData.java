package com.universeprojects.gamecomponents.client.dialogs.battleRoyaleLobby;

public class RealmShipData {
    public String name;
    public String description;
    public int    price;
    public String mainIconURL;
    public String topDownURL;
    public boolean owned;

    public RealmShipData(String name, String description, int price, String mainIconURL, String topDownURL, boolean owned) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.mainIconURL = mainIconURL;
        this.topDownURL = topDownURL;
        this.owned = owned;
    }
}