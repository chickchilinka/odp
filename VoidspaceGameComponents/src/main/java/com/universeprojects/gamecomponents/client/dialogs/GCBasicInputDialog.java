package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.gamecomponents.client.elements.GCErrorMessage;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

/**
 * This is an implementation of a very basic input dialog, as follows:
 * <p>
 * - There is one input field
 * - There is one button to submit the dialog
 * - The dialog can be dismissed by hitting the [X] in the corner
 * <p>
 * This implementation uses SimpleWindow.
 */
public abstract class GCBasicInputDialog implements DestroyableDialog {

    private final GCSimpleWindow window;
    private final H5EInputBox inputField;
    private final GCErrorMessage errorMessage;
    private final H5EButton btnOK;

    private final static int MIN_WIDTH = 450;
    private final static int MIN_HEIGHT = 225;
    private final static boolean AUTO_SIZE = true;

    /**
     * Constructs a basic input dialog in the specified layer
     */
    public GCBasicInputDialog(H5ELayer layer, String id) {
        window = new GCSimpleWindow(layer, id, getTitle(), MIN_WIDTH, MIN_HEIGHT, AUTO_SIZE);
        window.onClose.registerHandler(this::reset);
        window.positionProportionally(0.5f, 0.5f);

        window.addH1(getCaption(), Position.NEW_LINE, Alignment.CENTER, 0, 0);
        window.addEmptyLine(2);

        inputField = window.addInputBox(350, Position.NEW_LINE, Alignment.CENTER, 0, 0);
        inputField.setTextFieldListener((textField, c) -> {
            if(c == '\n') {
                doSubmit();
            }
        });
//        inputField.onEnterPressed.registerHandler(this);

        errorMessage = window.addElement(new GCErrorMessage(layer), Position.NEW_LINE, Alignment.CENTER, 0, 0);

        btnOK = window.addMediumButton(getButtonText(), Position.NEW_LINE, Alignment.CENTER, 0, 0);
        btnOK.addButtonListener(this::doSubmit);
    }
//
//    @Override
//    public void handleEvent(H5EEvent event) {
//        if (event == H5EButton.onButtonUnpressEvent && event.getSource() == btnOK) {
//            doSubmit();
//        } else if (event == H5EInputBox.onEnterPressedEvent) {
//            doSubmit();
//        } else if (event == GCSimpleWindow.onCloseEvent) {
//            reset();
//        }
//    }

    private void doSubmit() {
        String errorMsg = validate();
        if (errorMsg != null) {
            errorMessage.error(errorMsg);
        } else {
            handleSubmit();
            close();
        }
    }

    private void reset() {
        inputField.setText(null);
        errorMessage.clearError();
    }

    public void setText(String text) {
        this.inputField.setText(text);
    }

    //////////////////////////////////////////////////////////////////
    // CLIENT SETTINGS - TO BE IMPLEMENTED BY THE CHILD CLASS
    //

    /**
     * To be implemented by the child class. Specifies the title for the dialog window.
     * (NULL for no title)
     */
    public abstract String getTitle();

    /**
     * To be implemented by the child class. Specifies the caption for the dialog.
     */
    public abstract String getCaption();

    /**
     * To be implemented by the child class. Specifies the text for the button.
     */
    public abstract String getButtonText();

    /**
     * To be implemented by the child class. Validates the value provided in the input box.
     *
     * @return a String error message if validation failed, or NULL if validation was successful
     */
    public abstract String validate();

    /**
     * Handles the submission of this dialog, if validation is successful
     */
    public abstract void handleSubmit();


    //////////////////////////////////////////////////////////////////
    // PUBLIC INTERFACE
    //

    /**
     * Opens the dialog
     */
    public void open() {
        window.open();
    }

    /**
     * Closes the dialog
     */
    public void close() {
        window.close();
    }

    /**
     * @return TRUE if the dialog is open, FALSE otherwise
     */
    public boolean isOpen() {
        return window.isOpen();
    }

    /**
     * @return The text in the input field
     */
    protected String getInput() {
        return inputField.getText();
    }

    @Override
    public void destroy() {
        window.destroy();
    }
}
