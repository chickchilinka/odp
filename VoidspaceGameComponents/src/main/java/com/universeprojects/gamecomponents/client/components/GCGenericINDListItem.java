package com.universeprojects.gamecomponents.client.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

@SuppressWarnings("unused")
public class GCGenericINDListItem<T> extends GCListItem {

    public static final int ICON_SIZE = 64;
    private static final String DEFAULT_ICON = "https://i.imgur.com/v3HqseQ.png";
    final GCGenericINDListItemData<T> data;

    private final Table subTable;
    private final H5EScrollLabel nameLabel;
    private final H5ELabel descTextArea;

    private final H5EScrollablePane parentScrollPane;

    private float lastScrollPaneWidth = 100;

    public GCGenericINDListItem(H5ELayer layer, GCListItemActionHandler<GCGenericINDListItem<T>> actionHandler, GCGenericINDListItemData<T> data, H5EScrollablePane parentScrollPane) {
        super(layer, actionHandler);
        this.parentScrollPane = parentScrollPane;

        left().top();
        defaults().left().top();

        this.data = Dev.checkNotNull(data);
        if (Strings.isEmpty(data.name)) {
            throw new IllegalArgumentException("Idea name can't be blank");
        }

        H5EIcon icon = H5EIcon.fromSpriteType(layer, Dev.withDefault(data.iconKey, DEFAULT_ICON));
        if (data.quantity > 0) {
            icon.setFillParent(true);
            H5ELabel quantityLabel = new H5ELabel(Integer.toString(data.quantity), layer);
            quantityLabel.setStyle(getLayer().getEngine().getSkin().get("label-small", Label.LabelStyle.class));
            WidgetGroup iconGroup = new WidgetGroup(icon, quantityLabel) {
                @Override
                public void layout() {
                    icon.validate();
                    quantityLabel.validate();
                    quantityLabel.setY(-2);
                    quantityLabel.setX(icon.getWidth() - quantityLabel.getPrefWidth() - 5);
                }
            };
            add(iconGroup).size(ICON_SIZE).padRight(5);
        } else {
            add(icon).size(ICON_SIZE);
        }

        subTable = new Table();
        subTable.left().top();
        subTable.defaults().left().top();
        add(subTable).growX().fillY();

        nameLabel = new H5EScrollLabel(data.name, layer);
        nameLabel.getLabel().setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));

        String desc = "";
        if (data.description != null) desc = data.description;
        descTextArea = new H5ELabel(desc, layer);
        descTextArea.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        descTextArea.setColor(Color.valueOf("#AAAAAA"));
        descTextArea.setWrap(true);
        descTextArea.setTouchable(Touchable.disabled);

        rebuildActor();
    }

    public GCGenericINDListItemData<T> getData() {
        return data;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (parentScrollPane.getWidth() != lastScrollPaneWidth)
            rebuildActor();

        nameLabel.setAutoScroll(isChecked() || isOver());
    }

    public void rebuildActor() {
        subTable.clear();
        lastScrollPaneWidth = parentScrollPane.getWidth();
        int newWidth = (int) (lastScrollPaneWidth - ICON_SIZE - 22);
        subTable.add(nameLabel).padLeft(5).width(newWidth - 5);
        nameLabel.setWidth(newWidth - 5);
        nameLabel.setupScrolling();
        subTable.row();
        subTable.add(descTextArea).padLeft(5).growX().getActor();
    }
}
