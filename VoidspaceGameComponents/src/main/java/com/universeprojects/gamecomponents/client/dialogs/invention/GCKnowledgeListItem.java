package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCKnowledgeData.GCKnowledgeDataItem;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;

class GCKnowledgeListItem extends GCListItem {

    private final GCLabel nameLabel;
    private final GCLabel expLabel;

    public GCKnowledgeListItem(H5ELayer layer, GCListItemActionHandler<GCKnowledgeListItem> actionHandler, GCKnowledgeDataItem dataItem) {
        super(layer, actionHandler);

        if (Strings.isEmpty(dataItem.name)) {
            throw new IllegalArgumentException("Item name can't be blank");
        }
        if (dataItem.exp < 0) {
            throw new IllegalArgumentException("XP for "+dataItem.name+" must be >= 0");
        }

        nameLabel = add(new GCLabel(layer)).left().growX().padLeft(5).getActor();
        nameLabel.setColor(Color.valueOf("#EEEEEE"));
        nameLabel.setText(dataItem.name);
        nameLabel.setTouchable(Touchable.disabled);
        nameLabel.setAlignment(Align.left);

        expLabel = add(new GCLabel(layer)).right().bottom().growX().padRight(5).getActor();
        expLabel.setColor(Color.valueOf("#CCCCCC"));
        expLabel.setText(dataItem.exp + " XP");
        expLabel.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        expLabel.setTouchable(Touchable.disabled);
        expLabel.setAlignment(Align.right);

    }

}
