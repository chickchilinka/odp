package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;

public class GCLoadingIndicator extends H5ESprite {
    private final Action action = new Action() {
        @Override
        public boolean act(float delta) {
            target.rotateBy(-6f * 60f * delta);
            return false;
        }
    };
    private boolean active = false;

    public GCLoadingIndicator(H5ELayer layer) {
        super(layer, "images/GUI/ui2/preload/loading1.png");
        this.setVisible(false);
        this.setOrigin(Align.center);
        this.setTouchable(Touchable.disabled);
    }

    public void activate(Group componentToOverlay) {
        if (!active) {
            active = true;
            addAction(action);
        }
        this.setRotation(0);

        this.setVisible(true);
        componentToOverlay.addActor(this);

        float x = (componentToOverlay.getWidth() / 2) - this.getWidth() / 2;
        float y = (componentToOverlay.getHeight() / 2) - this.getHeight() / 2;

        this.setPosition(x, y);
    }

    public void deactivate() {
        if (active) {
            removeAction(action);
            active = false;
        }
        this.setVisible(false);
    }
}
