package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.Input;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.shared.UPUtils;

import java.util.List;

public class CharacterListSpawnSelection<C extends CharacterInfo> extends SimpleSelectionWindow {

    private List<? extends CharacterInfo> characterList;
    private final CharacterLoader<C> charLoader;
    private final Callable1Args<Long> onSelected;
    private final GCListItemActionHandler<GCCharacter<CharacterInfo>> handler;
    private final H5EButton btnSpawn;

    public CharacterListSpawnSelection(H5ELayer layer, CharacterLoader<C> charLoader, Callable1Args<Long> onSelected) {
        super(layer, "spawn-character-select", "Select Character", 500, 500);
        this.setSize(500,500);
        this.charLoader = charLoader;
        this.onSelected = onSelected;
        handler = new GCListItemActionHandler.SimpleHandler<>(()-> {});

        btnSpawn = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Spawn").build();
        btnSpawn.addButtonListener(this::spawnButton);
        H5EButton btnCancel = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Cancel").build();
        btnCancel.addButtonListener(this::cancelButton);
        btnTable.add(btnCancel).center().growX();
        btnTable.add(btnSpawn).center().growX();
    }

    @Override
    protected void loadItemList() {
        itemList.clear();
        itemList.remove();
        itemList = null;
        itemList = new GCList<>(getLayer(), 1, 5, 5, true);
        itemList.top().left();
        mainPane.getContent().clear();
        mainPane.add(itemList).top().left().grow();

        if(characterList.isEmpty()) {
            return;
        }

        for (CharacterInfo characterInfo : characterList) {
            GCCharacter<CharacterInfo> item = new GCCharacter<>(getLayer(), handler, characterInfo);
            itemList.addItem(item);
        }

        //noinspection unchecked
        GCCharacter<CharacterInfo> item = (GCCharacter<CharacterInfo>) itemList.getItems().get(0);
        item.setChecked(true);
    }

    private void spawnButton(){
        GCCharacter<CharacterInfo> selectedItem =  handler.getSelectedItem();
        if(selectedItem != null){
            onSelected.call(selectedItem.getCharacterInfo().getId());
        }else{
            messageLabel.setText("*Please select a character");
        }
    }

    private void cancelButton(){
        this.close();
    }

    protected void loadCharacterDataAndOpenDialog() {
        try {
            charLoader.loadGameCharacters((response) -> {
                characterList = response;
                this.open();
            }, (error) ->
                Log.error("Error while loading friends: " + error));
        } catch (Throwable ex) {
            Log.error("Error while loadng friend data:", ex);
        }


    }


    @Override
    public void open() {
        super.open();
        handler.reset();
        loadItemList();
        this.setSize(400,500);
        positionProportionally(0.5f, 0.5f);
        UPUtils.tieButtonToKey(this, Input.Keys.ENTER, btnSpawn);
        getLayer().setKeyboardFocus(this);
    }

    public interface CharacterLoader<C extends CharacterInfo> {
        void loadGameCharacters(Callable1Args<List<C>> successCallback, Callable1Args<String> errorCallback);
    }
}
