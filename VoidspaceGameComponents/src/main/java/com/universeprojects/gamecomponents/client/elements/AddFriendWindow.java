package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.shared.UPUtils;

public abstract class AddFriendWindow extends GCSimpleWindow {
    protected H5EInputBox input;
    protected H5EButton btnAdd;
    protected Table mainTable;

    public AddFriendWindow(H5ELayer layer){
        super(layer, "social-add-friend", "Add Friend",300  , 300);
        mainTable = add(new Table(layer.getEngine().getSkin())).left().top().grow().getActor();
        input = mainTable.add(new H5EInputBox(layer)).growX().top().center().colspan(2).getActor();
//        input.addListener(new InputListener(){
//            @Override
//            public boolean keyDown(InputEvent event, int keycode) {
//                if(keycode == Input.Keys.ENTER){
//                    addFriend();
//                }
//                event.handle();
//                return super.keyDown(event, keycode);
//            }
//        });
        input.focus();
        mainTable.row();
        btnAdd = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Add").build();
        btnAdd.addButtonListener(() -> {
            this.addFriend();
            input.focus();
            input.selectAll();
        });

        mainTable.add(btnAdd).center().top();

        UPUtils.tieButtonToKey(input, Input.Keys.ENTER, btnAdd);
    }

    protected abstract void addFriend();

    public void highlightAll(){
        input.selectAll();
    }

    @Override
    public void open() {
        super.open();
        input.focus();
        this.positionProportionally(0.5f, 0.5f);
    }

    @Override
    public void close() {
        super.close();
//            dialog.SERVICE_Friend.get().openFriendsTabSocialDialog();
    }
}
