package com.universeprojects.gamecomponents.client.dialogs.station.store;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.vsdata.shared.CategorizedStoreItemData;


@SuppressWarnings("FieldCanBeLocal")
public class GCStoreListItem extends GCListItem {

    private final GCStoreListItemData entryData;
    private final Button itemContainer;
    protected final GCItemIcon<GCInventoryItem> itemIcon;
    private final H5ELabel itemNameLabel;
    public static final int ICON_SIZE = 64;

    private float descPrefWidth;
    private float descWidth;

    public GCStoreListItem(H5ELayer layer, GCListItemActionHandler<GCStoreListItem> handler, GCStoreListItemData entryData) {
        super(layer, handler);
        this.entryData = entryData;
        row();

        itemContainer = add(new Button(getEngine().getSkin(), "gc-list-item-no-check")).growX().getActor();
        itemContainer.left().top();
        itemContainer.defaults().left().top();

        H5EStack stack = new H5EStack();
        itemContainer.add(stack).size(ICON_SIZE);
        itemIcon = new GCItemIcon<>(layer);
        itemIcon.setItem(entryData.getItem());
        stack.add(itemIcon);

        Table subTable = new Table().left().top();
        subTable.defaults().left().top();
        itemContainer.add(subTable).grow();

        itemNameLabel = new H5ELabel(layer);
        subTable.add(itemNameLabel).growX().padLeft(10).getActor();
        itemNameLabel.setWrap(true);
        itemNameLabel.setText(entryData.getData().name);
        //itemNameLabel.setColor(Color.valueOf("#FFFFFF"));
        itemNameLabel.setTouchable(Touchable.disabled);
        subTable.row();
        if(!entryData.getData().sellerNickname.equals("")) {
            H5ELabel sellerNickLabel = new H5ELabel(entryData.getData().sellerNickname, layer);
            sellerNickLabel.setFontScale(0.8f);
            subTable.add(sellerNickLabel).left().padLeft(10);
            subTable.row();
        }
        Table priceTable = new Table();
        H5ELabel priceLabel = new H5ELabel(String.valueOf(entryData.getData().price.amount), layer);
        priceTable.add(priceLabel).left().padLeft(10);
        priceTable.add(H5EIcon.fromSpriteType(layer, entryData.getData().price.icon)).width(24).height(24).left().padLeft(10);
        priceLabel.setFontScale(0.8f);
        subTable.add(priceTable);
    }

    public GCStoreListItemData getEntryData() {
        return entryData;
    }

}
