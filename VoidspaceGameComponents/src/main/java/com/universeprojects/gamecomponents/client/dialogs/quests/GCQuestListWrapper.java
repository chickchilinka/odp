package com.universeprojects.gamecomponents.client.dialogs.quests;

import java.util.ArrayList;
import java.util.List;

/**
 * Used as a container for all the quests. {@link GCQuestListItem}
 */
public class GCQuestListWrapper {
    /**
     * Lists of quests
     */
    private final List<GCQuestListItem> activeQuests = new ArrayList<>();
    private final List<GCQuestListItem> completedQuests = new ArrayList<>();
    private final List<GCQuestListItem> skippedQuests = new ArrayList<>();
    private final List<GCQuestListItem> abandonedQuests = new ArrayList<>();

    /**
     * Constructor for the quest list wrapper
     */
    GCQuestListWrapper() {
    }

    // Getters begin here ----------------------------------------------------------------------------------------------

    /**
     * @return The list of active quests
     */
    List<GCQuestListItem> getActiveQuests() {
        return activeQuests;
    }

    /**
     * @return The list of completed quests
     */
    List<GCQuestListItem> getCompletedQuests() {
        return completedQuests;
    }

    /**
     * @return The list of skipped quests
     */
    List<GCQuestListItem> getSkippedQuests() {
        return skippedQuests;
    }

    /**
     * @return The list of abandoned quests
     */
    List<GCQuestListItem> getAbandonedQuests() {
        return abandonedQuests;
    }

    // Getters end here ----------------------------------------------------------------------------------------------

    /**
     * Clear all quests
     */
    public void clear() {
        activeQuests.clear();
        completedQuests.clear();
        skippedQuests.clear();
        abandonedQuests.clear();
    }
}





