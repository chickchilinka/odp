package com.universeprojects.gamecomponents.client.dialogs.battleRoyaleLobby;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.util.VoidspaceNumberFormat;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.inputs.H5EButtonInvalidateListener;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class NewRealmPlayerListItem extends Button {

    private final H5ELayer layer;
    private final H5EIcon shipIcon;
    private final RealmPlayerData data;

    public NewRealmPlayerListItem(H5ELayer layer, RealmPlayerData data) {
        super(layer.getEngine().getSkin(), "gc-list-item");
        addListener(new H5EButtonInvalidateListener(this));
        this.layer = layer;
        this.data = data;

        // Ship Icon
        H5EContainer shipContainer = add(new H5EContainer(layer)).size(48).pad(2).getActor();
        shipContainer.setBackground("purchase-dialog-subpanel");

        if (data.ship.mainIconURL == null || data.ship.mainIconURL.length() == 0) {
            data.ship.mainIconURL = "voidspace"; // just need something here so it doesn't crash
        }
        H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(data.ship.mainIconURL);
        shipIcon = new H5EIcon(layer,spriteType);
        shipIcon.setScaling(Scaling.fit);
        shipContainer.add(shipIcon).grow();
        spriteType.subscribeToLoadStateChange((state)-> shipIcon.setDrawable(new TextureRegionDrawable(spriteType.getGraphicData())));

        // Text
        Table textTable = add(new Table()).grow().pad(5).getActor();

        final H5ELabel nameLabel = textTable.add(new H5ELabel(data.name, layer)).left().growX().colspan(2).getActor();
        nameLabel.setAlignment(Align.left);
        if (data.isYou) nameLabel.setColor(0.5f, 0.7f, 1.0f, 1.0f);

        textTable.row();

        final H5ELabel titleLabel = textTable.add(new H5ELabel(data.title, layer)).left().growX().getActor();
        titleLabel.setAlignment(Align.left);
        titleLabel.setFontScale(0.75f);
        titleLabel.setColor(Color.valueOf("#959595"));

        final H5ELabel rankLabel = textTable.add(new H5ELabel("Rank: " + VoidspaceNumberFormat.format(data.rank), layer)).right().getActor();
        rankLabel.setAlignment(Align.left);
        rankLabel.setFontScale(0.75f);
        rankLabel.setColor(Color.valueOf("#959595"));
    }

    public void changeShip(String shipURL) {
        if (shipURL == null || shipURL.length() == 0) shipURL = "voidspace";
        H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(shipURL);
        spriteType.subscribeToLoadStateChange((state)-> shipIcon.setDrawable(new TextureRegionDrawable(spriteType.getGraphicData())));
    }

    public RealmPlayerData getData() {
        return data;
    }
}