package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.components.GCItemInternalTree;
import com.universeprojects.gamecomponents.client.components.GCItemTreeItem;
import com.universeprojects.gamecomponents.client.components.GCItemTreeSubitem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeasData.GCIdeaDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

class GCInventionIdeasTab extends GCInventionTab {

    private final H5EScrollablePane listContainer;
    private GCList<GCIdeaListItem> ideasList;
    private final GCListItemActionHandler<GCIdeaListItem> listHandler;
    public static final int LIST_SPACING = 5;

    private final GCItemInternalTree leftTree;
    private GCIdeasData allIdeas;
    private GCIdeasData currentIdeas;
    private float paneWidth = 0;

    protected GCInventionIdeasTab(GCInventionSystemDialog dialog) {
        super(dialog, "Ideas");

        listContainer = rightContent.add(new H5EScrollablePane(layer) {
            @Override
            public void act(float delta) {
                super.act(delta);
                if (UPMath.abs(getWidth() - paneWidth) > 5) {
                    paneWidth = getWidth();
                    if (ideasList == null) return;
                    for (GCIdeaListItem item : ideasList.getItems()) {
                        item.rebuildActor();
                    }
                }
            }
        }).left().top().grow().getActor();
        listContainer.getContent().top().left();

        listHandler = new GCListItemActionHandler<GCIdeaListItem>() {
            @Override
            protected void onSelectionUpdate(GCIdeaListItem lastSelectedItem) {
                GCIdeaListItem selectedItem = this.getSelectedItem();
                Tutorials.trigger("window:idea:select-item" + (selectedItem != null ? ":" + selectedItem.data.name : ""));
            }
        };

        H5EScrollablePane treeContainer = leftContent.add(new H5EScrollablePane(layer)).padBottom(12).grow().getActor();
        treeContainer.getContent().top().left();
        leftTree = treeContainer.add(new GCItemInternalTree(layer)).top().left().getActor();

        final H5EButton btnBeginPrototyping = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Design").withTutorialId("button:invention:start-prototype").build();
        lowerButtonRow.add(btnBeginPrototyping).grow();
        btnBeginPrototyping.addButtonListener(() -> {
            GCIdeaListItem selectedIdea = listHandler.getSelectedItem();
            Long selectedId = selectedIdea == null ? null : selectedIdea.data.id;
            Tutorials.trigger("process:start:idea");
            dialog.onBeginPrototypingBtnPressed(selectedId);
        });
    }

    @Override
    protected void clearTab() {
        if (ideasList != null) {
            listHandler.reset();
            for (GCIdeaListItem listItem : ideasList.getItems()) {
                Tutorials.removeObject("button:invention:item:" + listItem.getName());
            }
            ideasList.clear();
            ideasList.remove();
            ideasList = null;
            listContainer.getContent().clear();
        }
    }

    void populateData(GCIdeasData ideasData) {
        clearTab();
        ideasData.sort();
        currentIdeas = ideasData;
        ideasList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        ideasList.left().top();
        listContainer.add(ideasList).left().top().grow();
        for (int index = 0; index < ideasData.size(); index++) {
            GCIdeaDataItem dataItem = ideasData.get(index);
            GCIdeaListItem listItem;
            try {
                listItem = new GCIdeaListItem(layer, listHandler, dataItem, listContainer);
            } catch (Exception ex) {
                Log.error("Error parsing idea data " + dataItem);
                continue;
            }
            if (listItem.getName().toLowerCase().contains(filterText.toLowerCase()) || listItem.data.description.contains(filterText.toLowerCase())) {
                ideasList.addItem(listItem);
            }
            Tutorials.registerObject("button:invention:item:" + listItem.getName(), listItem);
        }
    }

    void populateCategories(GCIdeasData ideaData, GCIdeaCategoryData categoryData) {
        ideaData.sort();
        leftTree.clearTree();
        allIdeas = GCIdeasData.createNew();
        GCItemTreeSubitem leafAll = leftTree.addItem("All", () -> {
            titleLabel.setText("Ideas: All");
            populateData(allIdeas);
        });
        for (GCIdeaCategoryData.GCIdeaCategoryDataItem item : categoryData.children) {
            extractCategoryData(item, leftTree, ideaData);
        }
        for (int index = ideaData.size() - 1; index >= 0; index--) {
            GCIdeasData.GCIdeaDataItem dataItem = ideaData.get(index);
            allIdeas.add(dataItem.id, dataItem.name, dataItem.description, dataItem.iconKey, dataItem.parentId, dataItem.createdTime, dataItem.durationSecs);
        }
        leafAll.click();
    }

    private boolean extractCategoryData(GCIdeaCategoryData.GCIdeaCategoryDataItem item, GCItemInternalTree currentTree, GCIdeasData ideaData) {
        ideaData.sort();
        boolean hasChildren = false;
        if (item.isParentCategory) {
            GCItemTreeItem category = currentTree.addCategory(item.name);
            boolean newHasChildren = false;
            for (GCIdeaCategoryData.GCIdeaCategoryDataItem subitem : item.children) {
                boolean extractionResult = extractCategoryData(subitem, category, ideaData);
                if (extractionResult) newHasChildren = true;
            }
            if (!newHasChildren) {
                currentTree.removeCategory(category);
            }
            hasChildren = newHasChildren;
        } else {
            // Category is a clickable leaf
            GCIdeasData leafData = GCIdeasData.createNew();
            for (int index = ideaData.size() - 1; index >= 0; index--) {
                GCIdeasData.GCIdeaDataItem dataItem = ideaData.get(index);
                if (dataItem.parentId != null && dataItem.parentId.equals(item.id)) {
                    hasChildren = true;
                    leafData.add(dataItem.id, dataItem.name, dataItem.description, dataItem.iconKey, dataItem.parentId, dataItem.createdTime, dataItem.durationSecs);
                    allIdeas.add(dataItem.id, dataItem.name, dataItem.description, dataItem.iconKey, dataItem.parentId, dataItem.createdTime, dataItem.durationSecs);
                    ideaData.remove(index);
                }
            }
            if (hasChildren) {
                currentTree.addItem(item.name, () -> {
                    titleLabel.setText("Ideas: " + item.name);
                    populateData(leafData);
                });
            }
        }
        return hasChildren;
    }

    public String getSelectedItemName() {
        if (listHandler == null) return "";
        GCIdeaListItem selected = listHandler.getSelectedItem();
        if (selected == null || selected.data == null || selected.data.name == null) return "";
        return selected.data.name;
    }

    @Override
    protected void onFilter() {
        super.onFilter();
        populateData(currentIdeas);
    }
}
