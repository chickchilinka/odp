package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;


@SuppressWarnings("FieldCanBeLocal")
public class GCExperimentsListItem extends GCListItem {

    public final GCInventoryDataItem data;

    private final GCItemIcon<GCInventoryItem> icon;
    private final H5ELabel quantityLabel;
    private final Table nameTable;
    private final H5EScrollLabel nameLabel;
    public static final int ICON_SIZE = 64;

    private final H5EScrollablePane parentScrollPane;

    public GCExperimentsListItem(H5ELayer layer, GCListItemActionHandler<GCExperimentsListItem> actionHandler, GCInventoryDataItem data, H5EScrollablePane parentScrollPane) {
        super(layer, actionHandler);
        this.parentScrollPane = parentScrollPane;

        if (data.index < 0) {
            throw new IllegalArgumentException("Index can't be negative");
        }
        if (Strings.isEmpty(data.item.getName())) {
            throw new IllegalArgumentException("Item name can't be blank");
        }
        if (data.item.getQuantity() <= 0) {
            throw new IllegalArgumentException("Quantity must be greater than 0");
        }

        this.data = data;

        if (Strings.isEmpty(data.item.getIconSpriteKey())) {
            icon = null;
        } else {
            H5EStack stack = new H5EStack();
            add(stack).left().top().padRight(5).size(ICON_SIZE);
            icon = new GCItemIcon<>(layer);
            icon.setItem(data.item);
            stack.add(icon);
            icon.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    actionHandler.onInfoButtonClicked(GCExperimentsListItem.this);
                    event.handle();
                }
            });
        }

        if (data.item.getQuantity() == 1) {
            quantityLabel = null;
        } else {
            quantityLabel = add(new H5ELabel(layer)).left().padRight(10).getActor();
            quantityLabel.setText(Long.toString(data.item.getQuantity()));
            quantityLabel.setColor(Color.valueOf("#FFFFFF"));
        }

        nameTable = new Table();
        nameTable.left().top();
        nameTable.defaults().left().top();
        add(nameTable).growX().fillY();

        nameLabel = new H5EScrollLabel(data.item.getName(), layer);

        rebuildActor();
    }

    @Override
    public String getName() {
        return data.item.getName() ;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        nameLabel.setAutoScroll(isChecked() || isOver());
    }

    public void rebuildActor() {
        nameTable.clear();
        float quantityWidth = quantityLabel == null ? 5 : quantityLabel.getPrefWidth() + 15;
        int newWidth;
        if(icon != null){
             newWidth = (int) (parentScrollPane.getWidth() - ICON_SIZE - quantityWidth - 15);

        }else{
            newWidth = (int) (parentScrollPane.getWidth()  - quantityWidth - 15);
        }
        nameTable.add(nameLabel).width(newWidth);
        nameLabel.setWidth(newWidth);
        nameLabel.setupScrolling();
    }
}
