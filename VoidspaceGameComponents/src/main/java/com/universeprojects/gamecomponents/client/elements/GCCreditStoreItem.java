package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.login.GCCreditStoreComponent;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.HashMap;
import java.util.Map;

public class GCCreditStoreItem extends GCListItem {

    private final Map<String, CreditItemPriceData> priceData = new HashMap<>();
    private final H5ELabel priceLabel;
    private int quantity;
    private final String iconKey;

    private H5EIcon icon;
    private Cell<H5EIcon> iconCell;

    private CreditItemData itemData;
    protected GCCreditStoreComponent<?> dialog;

    public GCCreditStoreItem(H5ELayer layer, GCCreditStoreItemHandler handler, CreditItemData data, GCCreditStoreComponent<?> dialog){
        super(layer, handler);
        this.dialog = dialog;
        this.itemData = data;
        iconKey = data.iconKey;
        quantity = data.quantity;
        priceData.putAll(data.processorPriceData);

        if (iconKey != null && !iconKey.isEmpty()) {
            icon = H5EIcon.fromSpriteType(layer, iconKey, Scaling.fit, Align.center);
            iconCell = add(icon).top().pad(5).height(200).width(150);
        } else {
            add().top().pad(5);
        }
        row();
//        add(new H5ELabel(quantity + " Credits", layer)).center().pad(5).getActor();
//        row();
        priceLabel = add(new H5ELabel(layer)).center().pad(5).getActor();
//        row();
//        selectBtn = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Select").build();
//        selectBtn.addButtonListener(this::select);
//        add(selectBtn).center().pad(5);
    }

    public void setPixelIconPixelHeight(int height) {
        iconCell.height(height).width((int)(((double)height)*0.75d));
    }


    protected void select(){
        dialog.updatedSelected(itemData, this);
    }

    public void setPriceForPaymentMethod(String method) {
        CreditItemPriceData priceData = this.priceData.get(method);
        long price = priceData.price + priceData.fees;
        String dollars = Long.toString(price / 100L);
        String cents = Long.toString(price % 100);
        if(cents.length() < 2) {
            cents = "0"+cents;
        }
        String priceString = dollars+"."+cents;
        priceLabel.setText("$"+ priceString + " USD");
    }

    public static class GCCreditStoreItemHandler extends GCListItemActionHandler<GCCreditStoreItem>{

        @Override
        protected void onSelectionUpdate(GCCreditStoreItem newItemSelection) {
            GCCreditStoreItem item = getSelectedItem();
            if(item == null)return;
            item.select();
        }
    }
}
