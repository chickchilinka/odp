package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCLoginOrganisationItem;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.HashMap;
import java.util.Map;

public class OrganisationSpawnSelectionWindow extends SimpleSelectionWindow {

    private final Map<Long, String> orgMap = new HashMap<>();
    private final OrganisationLoader organisationLoader;
    private final Callable1Args<Long> onSelected;

    @SuppressWarnings("rawtypes")
    public OrganisationSpawnSelectionWindow(H5ELayer layer, OrganisationLoader organisationLoader, Callable1Args<Long> onSelected) {
        super(layer, "spawn-organisation-select", "Select Organisation", 500, 500);
        this.setSize(600, 500);
        this.organisationLoader = organisationLoader;
        this.onSelected = onSelected;
        this.handler = new GCLoginOrganisationItem.GCLoginOrganisationListHandler();

        H5EButton btnSpawn = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Spawn").build();
        btnSpawn.addButtonListener(this::spawnButton);
        H5EButton btnRefresh = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Refresh").build();
        btnRefresh.addButtonListener(this::loadOrganisationData);
        H5EButton btnCancel = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Cancel").build();
        btnCancel.addButtonListener(this::cancelButton);
        btnTable.add(btnCancel).center().growX();
        btnTable.add(btnRefresh).center().growX();
        btnTable.add(btnSpawn).center().growX();

//        friendMap = dialog.friendMap;
    }

    @Override
    protected void loadItemList() {
        itemList.clear();
        itemList.remove();
        itemList = null;
        itemList = new GCList<>(getLayer(), 1, 5, 5, true);
        itemList.top().left();
        mainPane.getContent().clear();
        mainPane.add(itemList).top().left().grow();

//        friendMap = dialog.friendMap;
        for (Map.Entry<Long, String> entry : orgMap.entrySet()) {
            long orgId = entry.getKey();
            String name = entry.getValue();
            GCLoginOrganisationItem item = new GCLoginOrganisationItem(getLayer(), (GCLoginOrganisationItem.GCLoginOrganisationListHandler) handler, name, orgId);
            itemList.addItem(item);
        }
    }

    private void spawnButton() {
        GCLoginOrganisationItem selectedItem = (GCLoginOrganisationItem) handler.getSelectedItem();
        if (selectedItem != null) {
            close();
            onSelected.call(selectedItem.getOrgId());
        } else {
            messageLabel.setText("*Please select an organisation");
        }
    }

    private void cancelButton() {
        this.close();
    }

    protected void loadOrganisationData() {
        try {
            organisationLoader.loadUserOrganisations((response) -> {
                orgMap.clear();
                orgMap.putAll(response);
                if (isOpen()) {
                    loadItemList();
                } else {
                    this.open();
                }
            }, (error) ->
                Log.error("Error while loading friends: " + error));
        } catch (Throwable ex) {
            Log.error("Error while loadng friend data:", ex);
        }


    }

    @Override
    public void open() {
        super.open();
        loadItemList();
        this.setSize(400, 500);
        handler.reset();
        positionProportionally(0.5f, 0.5f);
    }

    public interface OrganisationLoader {
        void loadUserOrganisations(Callable1Args<Map<Long, String>> successCallback, Callable1Args<String> errorCallback);
    }
}
