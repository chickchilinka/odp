package com.universeprojects.gamecomponents.client.dialogs.inventory;

public class GCInventoryItemSlotComponent<K, T extends GCInventoryItem> extends GCInventorySlotComponent<K, T, GCInventoryItemSlotComponent<K, T>> {
    public GCInventoryItemSlotComponent(GCInventoryBase<K, T, GCInventoryItemSlotComponent<K, T>> inventoryBase, K key, SlotConfig slotConfig) {
        super(inventoryBase, key, slotConfig);
    }

    public GCInventoryItemSlotComponent(GCInventoryBase<K, T, GCInventoryItemSlotComponent<K, T>> inventoryBase, String styleName, K key, SlotConfig slotConfig) {
        super(inventoryBase, styleName, key, slotConfig);
    }

    @Override
    public String getEmptyFrameIconStyle() {
        return null;
    }

    @Override
    public String getName() {
        return this.getItem().getName();
    }
}
