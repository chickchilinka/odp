package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.universeprojects.html5engine.shared.abstractFramework.MarkerRectangle;
import com.universeprojects.vsdata.shared.SlotLockState;

public class GCEquipmentSlotComponent<S extends Comparable<S>, T extends GCInventoryItem> extends GCInventorySlotComponent<EquipmentSlotKey<S>, T, GCEquipmentSlotComponent<S, T>> {

    private final MarkerRectangle marker;
    private final SlotLockState slotLockState;

    public GCEquipmentSlotComponent(GCInventoryBase<EquipmentSlotKey<S>, T, GCEquipmentSlotComponent<S, T>> inventoryBase, EquipmentSlotKey<S> key, MarkerRectangle marker, SlotConfig slotConfig, SlotLockState slotLockState) {
        super(inventoryBase, key, slotConfig);
        this.marker = marker;
        this.slotLockState = slotLockState;
        updateSize();
    }

    protected void updateSize() {
        setWidth(getPrefWidth());
        setHeight(getPrefHeight());
    }

    public MarkerRectangle getMarker() {
        return marker;
    }

    @Override
    public void setIconSize(float iconSize) {
        super.setIconSize(iconSize);
        updateSize();
    }

    @Override
    public boolean checkDisconnectLocked() {
        if(!slotLockState.allowDisconnect) {
            inventoryBase.showLockedSlotWarning();
            return true;
        }
        return false;
    }

    @Override
    public boolean needsSetOnlyCheck() {
        return slotLockState == SlotLockState.SET_ONLY;
    }

    @Override
    public String getEmptySpriteKey() {
        return "images/icons/base/junk_grade_2.png";
    }

    @Override
    public String getEmptyFrameIconStyle() {
        return "icon-equipment_item_frame";
    }

    @Override
    public String getFrameStyle() {
        return "icon-equipment_item_frame";
    }

    @Override
    public int getFrameBaseIconSize() {
        return GCItemIcon.BASE_EMPTY_FRAME_ICON_SIZE;
    }
}