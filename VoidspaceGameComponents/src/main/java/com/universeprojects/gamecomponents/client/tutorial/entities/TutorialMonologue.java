package com.universeprojects.gamecomponents.client.tutorial.entities;

public class TutorialMonologue {
    private String text;
    private String icon;

    public TutorialMonologue(String text, String icon){
        this.text = text;
        this.icon = icon;
    }

    public String getText(){
        return text;
    }

    public String getIcon(){
        return icon;
    }
}
