package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.common.shared.callable.ReturningCallable2Args;
import com.universeprojects.common.shared.util.CollectionUtils;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.vsdata.shared.RecipeCategoryData;
import com.universeprojects.vsdata.shared.RecipeConstructionData;
import com.universeprojects.vsdata.shared.RecipeConstructionSlotData;
import com.universeprojects.vsdata.shared.RecipeData;
import com.universeprojects.vsdata.shared.RecipeSlotData;
import com.universeprojects.vsdata.shared.RecipeSlotType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GCRecipeData {

    public enum RecipeMode {
        ITEM_IDEA, STRUCTURE_IDEA, ITEM_SKILL, STRUCTURE_SKILL
    }

    private final RecipeData data;
    private final RecipeMode mode;
    private final Long referenceId;
    private final List<GCRecipeSlotDataItem> slots = new ArrayList<>();
    private final Map<String, String> userFieldValueConfigs = new LinkedHashMap<>();

    private GCRecipeData(RecipeData data, RecipeMode mode, Long referenceId) {
        if (data == null) {
            data = new RecipeData();
        }
        if (mode == null) {
            throw new IllegalArgumentException("Mode can't be null");
        }
        if (referenceId == null) {
            throw new IllegalArgumentException("Reference id can't be null");
        }
        this.data = data;
        this.mode = mode;
        this.referenceId = referenceId;
    }

    public void addUserFieldValueConfig(String key, String gcConfig) {
        userFieldValueConfigs.put(key, gcConfig);
    }

    public void addUserFieldValueConfigs(Map<String, String> values) {
        userFieldValueConfigs.putAll(values);
    }

    public String getName() {
        return data.name;
    }

    public RecipeMode getMode() {
        return mode;
    }

    public Long getReferenceId() {
        return referenceId;
    }

    public static GCRecipeData createNew(RecipeData data, RecipeMode mode, Long referenceId) {
        return new GCRecipeData(data, mode, referenceId);
    }

    public GCRecipeData copy() {
        GCRecipeData copy = new GCRecipeData(data, mode, referenceId);
        for (GCRecipeSlotDataItem slot : slots) {
            copy.slots.add(slot.copy());
        }
        for (Map.Entry<String, String> entry : userFieldValueConfigs.entrySet()) {
            copy.userFieldValueConfigs.put(entry.getKey(), entry.getValue());
        }
        return copy;
    }

    public GCRecipeData generateSlots(RecipeConstructionData constructionData, ReturningCallable2Args<GCInventoryData, RecipeCategoryData, RecipeSlotData> itemFinder) {
        slots.clear();
        final Map<String, RecipeConstructionSlotData> slotConstructionMap = CollectionUtils.mapAsMapValue(constructionData.getSlotData(), s -> s.slotId);
        for (RecipeCategoryData category : data.categories) {
            for (RecipeSlotData slot : category.getSlots()) {
                int index = slots.size();
                final RecipeConstructionSlotData recipeConstructionSlotData = slotConstructionMap.get(slot.slotId);
                String selectedUid = recipeConstructionSlotData != null ? recipeConstructionSlotData.objectUid : null;
                final GCInventoryData items = itemFinder.call(category, slot);
                slots.add(new GCRecipeSlotDataItem(index, slot, category.toSlotType(), category.required, selectedUid, items));
            }
        }
        return this;
    }

    public GCRecipeData addSlot(String id, String name, RecipeSlotType slotType, Boolean required, String selectedItemUid, GCInventoryData items) {
        RecipeSlotData slotData = new RecipeSlotData();
        slotData.name = name;
        slotData.slotId = id;
        int index = slots.size();
        slots.add(new GCRecipeSlotDataItem(index, slotData, slotType, required, selectedItemUid, items));
        return this;
    }

    public GCRecipeSlotDataItem getSlot(int index) {
        return slots.get(index);
    }

    public List<GCRecipeSlotDataItem> getSlots() {
        return Collections.unmodifiableList(slots);
    }

    public Map<String, String> getUserFieldValueData() {
        return Collections.unmodifiableMap(userFieldValueConfigs);
    }

    public RecipeData getData() {
        return data;
    }

    public int size() {
        return slots.size();
    }

    public static class GCRecipeSlotDataItem {
        public final int index;

        public RecipeSlotData data;
        public final RecipeSlotType slotType;
        /**
         * When this field is null, it means that this is not specified
         */
        public final Boolean required;

        private GCInventoryData items;
        private String selectedItemUid;

        private GCRecipeSlotDataItem(GCRecipeSlotDataItem copyFrom) {
            this.index = copyFrom.index;

            this.data = copyFrom.data;
            this.slotType = copyFrom.slotType;
            this.required = copyFrom.required;
            this.items = copyFrom.items.copy();

            this.selectedItemUid = copyFrom.selectedItemUid;
        }

        GCRecipeSlotDataItem(int index, RecipeSlotData data, RecipeSlotType slotType, Boolean required, String selectedItemUid, GCInventoryData items) {
            this.index = index;

            this.data = data;
            this.slotType = slotType;
            this.required = required;
            this.items = items;

            this.selectedItemUid = selectedItemUid;
        }


        public GCRecipeSlotDataItem copy() {
            return new GCRecipeSlotDataItem(this);
        }

        public void clearSelectedItem() {
            selectedItemUid = null;
        }

        public void selectItem(String uid) {
            if (!items.hasUid(uid)) {
                throw new IllegalStateException("uid not found: " + uid);
            }
            Log.info("Selected item uid: " + uid);
            selectedItemUid = uid;
        }

        public String getNameWithQuantity() {
            if(data.options != null && !data.options.isEmpty()) {
                int quantity = data.options.get(0).requiredQuantity;
                if(quantity > 1) {
                    return getName()+" x"+quantity;
                }
            }
            return getName();
        }

        public boolean isItemSelected() {
            return selectedItemUid != null && items.containsItemForUid(selectedItemUid);
        }

        public GCInventoryDataItem getSelectedItem() {
            if (selectedItemUid == null) {
                return null;
            } else {
                return items.getByUid(selectedItemUid);
            }
        }

        public GCInventoryData getItems() {
            return this.items.copy();
        }

        public void replaceItems(GCInventoryData items) {
            Set<String> departingUids = this.items.getUids();
            departingUids.removeAll(items.getUids());

            if (departingUids.contains(selectedItemUid)) {
                selectedItemUid = null;
            }

            this.items = items;
        }

        public String getName() {
            return data.name;
        }

    }

}


