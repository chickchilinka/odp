package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.SpawnOptionData;

import java.util.ArrayList;
import java.util.List;

public class GCSpawnSelectDialog extends GCLoginInformation {

    protected final H5ELayer layer;
    protected final GCWindow window;

    private final H5EScrollablePane listContainer;
    private GCList<GCSpawnSelectItem> spawnOptionList;
    public static final int LIST_SPACING = 5;
    private float paneWidth = 0;
    private final GCListItemActionHandler<GCSpawnSelectItem> listHandler;

    private SpawnOptionData chosenSpawnObject;

    public Callable1Args<SpawnOptionData> onChooseSpawn;
    public Callable1Args<SpawnOptionData> onDoubleClick;
    private final GCLoadingIndicator loadingIndicator;

    public GCSpawnSelectDialog(H5ELayer layer, boolean mobileMode) {
        super(layer);
        this.layer = Dev.checkNotNull(layer);
        this.loadingIndicator = new GCLoadingIndicator(layer);

        window = new GCWindow(layer, 500, 550);
        window.setId("spawn-setting");
        window.setTitle("Spawn Setting");
        window.setCloseButtonEnabled(false);
        window.setPackOnOpen(false);
        window.setMovable(false);
        window.defaults().left().top();

        listContainer = window.add(new H5EScrollablePane(layer, "scrollpane-backing-1") {
            @Override
            public void act(float delta) {
                super.act(delta);
                if (UPMath.abs(getWidth() - paneWidth) > 5) {
                    paneWidth = getWidth();
                    if (spawnOptionList == null) return;
                    for (GCSpawnSelectItem item : spawnOptionList.getItems()) {
                        item.rebuildActor();
                    }
                }
            }
        }).colspan(2).left().top().grow().getActor();
        listContainer.getContent().top().left();

        listHandler = new GCListItemActionHandler<GCSpawnSelectItem>() {
            @Override
            protected void onSelectionUpdate(GCSpawnSelectItem lastSelectedItem) {
                if (lastSelectedItem != null){
                    chosenSpawnObject = lastSelectedItem.data.spawnObject;
                    if(!mobileMode)
                        onChooseSpawn.call(chosenSpawnObject);
                }
            }

            @Override
            protected void onDoubleClick(GCSpawnSelectItem item) {
                if(onDoubleClick != null && item != null) {
                    onDoubleClick.call(item.data.spawnObject);
                }
            }
        };

        window.row();

        if (mobileMode) {
            H5EButton backButton = new H5EButton("Back", layer);
            H5EButton nextButton = new H5EButton("Next", layer);

            window.add(backButton).align(Align.left).width(150);
            window.add(nextButton).align(Align.right).width(150);

            backButton.addButtonListener(window::close);
            nextButton.addButtonListener(() -> {
                onChooseSpawn.call(chosenSpawnObject);
                close();
            });
        }
    }

    public void preselectSpawnType(String spawnType) {
        if(spawnOptionList == null || spawnType == null) {
            return;
        }
        for (GCSpawnSelectItem item : spawnOptionList.getItems()) {
            if(spawnType.equals(item.data.spawnObject.spawnType)) {
                item.setChecked(true);
                listHandler.setSelection(item);
                return;
            }
        }
    }

    public void activateLoadingIndicator() {
        loadingIndicator.activate(window);
    }

    public void deactivateLoadingIndicator() {
        loadingIndicator.deactivate();
    }

    protected void clearTab() {
        if (spawnOptionList != null) {
            listHandler.reset();
            spawnOptionList.clear();
            spawnOptionList.remove();
            spawnOptionList = null;
            listContainer.getContent().clear();
        }
    }

    public void populateData(GCSpawnSelectData spawnOptions) {
        if(spawnOptions.size() == 0) {
            return;
        }
        populateData(spawnOptions, spawnOptions.get(0));
    }

    public void populateData(GCSpawnSelectData spawnOptions, GCSpawnOptionData preselectData) {
        clearTab();

        GCSpawnSelectItem preselectItem = null;

        spawnOptionList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        spawnOptionList.left().top();
        listContainer.add(spawnOptionList).left().top().grow();
        for (int index = 0; index < spawnOptions.size(); index++) {
            GCSpawnOptionData dataItem = spawnOptions.get(index);
            GCSpawnSelectItem listItem = new GCSpawnSelectItem(layer, listHandler, dataItem, listContainer);
            spawnOptionList.addItem(listItem);
            if (preselectData != null && preselectData.equals(dataItem)) {
                preselectItem = listItem;
            }
        }

        if (preselectItem != null) {
            preselectItem.setChecked(true);
            listHandler.setSelection(preselectItem);
        }
    }

    public GCWindow getWindow() {
        return window;
    }

    public void close() {
        window.close();
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    public void open() {
        window.open();
    }

    public void setPosition(float x, float y){
        window.setPosition(x, y);
    }

    public void positionProportionally(float x, float y){
        window.positionProportionally(x, y);
    }

    public float getHeight(){
        return window.getHeight();
    }

    public static class GCSpawnSelectData {

        private final List<GCSpawnOptionData> items = new ArrayList<>();

        public GCSpawnSelectData() {
        }

        public GCSpawnSelectData add(GCSpawnSelectData other) {
            for (GCSpawnOptionData item : other.items) {
                // we call the add() method sequentially in order for new indices to be assigned
                add(item.spawnObject);
            }
            return this;
        }

        public GCSpawnSelectData add(SpawnOptionData spawnObject) {
            int index = items.size();
            items.add(new GCSpawnOptionData(index, spawnObject, spawnObject.name, spawnObject.description, spawnObject.iconKey));
            return this;
        }

        public GCSpawnOptionData get(int index) {
            return items.get(index);
        }

        public void remove(int index) {
            items.remove(index);
        }

        public int size() {
            return items.size();
        }

        public GCSpawnSelectData copy() {
            GCSpawnSelectData copy = new GCSpawnSelectData();
            copy.items.addAll(items);
            return copy;
        }
    }

    @SuppressWarnings("FieldCanBeLocal")
    static class GCSpawnSelectItem extends GCListItem {

        public static final int ICON_SIZE = 64;
        final GCSpawnOptionData data;

        private final H5EIcon icon;
        private final Table subTable;
        private final H5EScrollLabel nameLabel;
        private final H5ELabel descTextArea;

        private final H5EScrollablePane parentScrollPane;

        public GCSpawnSelectItem(H5ELayer layer, GCListItemActionHandler<GCSpawnSelectItem> actionHandler, GCSpawnOptionData data, H5EScrollablePane parentScrollPane) {
            super(layer, actionHandler);
            this.parentScrollPane = parentScrollPane;

            left().top();
            defaults().left().top();

            this.data = Dev.checkNotNull(data);
            if (data.index < 0) {
                throw new IllegalArgumentException("Index can't be negative");
            }
            if (Strings.isEmpty(data.name)) {
                throw new IllegalArgumentException("Idea name can't be blank");
            }
            if (Strings.isEmpty(data.description)) {
                throw new IllegalArgumentException("Idea description can't be blank");
            }
            if (Strings.isEmpty(data.iconKey)) {
                throw new IllegalArgumentException("Idea icon key can't be blank");
            }

            icon = add(H5EIcon.fromSpriteType(layer, data.iconKey)).size(ICON_SIZE).padRight(5).getActor();

            subTable = new Table();
            subTable.left().top();
            subTable.defaults().left().top();
            add(subTable).growX().fillY();

            nameLabel = new H5EScrollLabel(data.name, layer);
            nameLabel.getLabel().setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));

            descTextArea = new H5ELabel(data.description, layer);
            descTextArea.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
            descTextArea.setColor(Color.valueOf("#AAAAAA"));
            descTextArea.setWrap(true);
            descTextArea.setTouchable(Touchable.disabled);

            rebuildActor();
        }

        @Override
        public void act(float delta) {
            super.act(delta);
            nameLabel.setAutoScroll(isChecked() || isOver());
        }

        public void rebuildActor() {
            subTable.clear();
            int newWidth = (int) (parentScrollPane.getWidth() - icon.getWidth() - 17);
            subTable.add(nameLabel).width(newWidth);
            nameLabel.setWidth(newWidth);
            nameLabel.setupScrolling();
            subTable.row();
            subTable.add(descTextArea).growX().getActor();
        }
    }

    public static class GCSpawnOptionData {
        final int index;
        public final SpawnOptionData spawnObject;
        public final String name;
        public final String description;
        public final String iconKey;

        GCSpawnOptionData(int index, SpawnOptionData spawnObject, String name, String description, String iconKey) {
            this.index = index;

            this.spawnObject = spawnObject;
            this.name = name;
            this.description = description;
            this.iconKey = iconKey;
        }

        @Override
        public String toString() {
            return "index: " + index + ", " +
                    "spawnType: " + spawnObject + ", " +
                    "name: " + name + ", " +
                    "description: " + description + ", " +
                    "iconKey: " + iconKey;
        }
    }
}
