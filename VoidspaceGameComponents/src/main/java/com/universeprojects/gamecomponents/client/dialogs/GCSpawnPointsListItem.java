package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.SpawnOptionData;

@SuppressWarnings("FieldCanBeLocal")
public class GCSpawnPointsListItem extends GCListItem {

    public static final int ICON_SIZE = 64;
    public final SpawnOptionData data;
    private final H5EIcon icon;
    private final Table nameTable;
    private final H5EScrollLabel nameLabel;
    private final H5ELabel descriptionLabel;

    public GCSpawnPointsListItem(H5ELayer layer, GCListItemActionHandler<GCSpawnPointsListItem> actionHandler, SpawnOptionData data) {
        super(layer, actionHandler);

        this.data = data;
        H5EStack stack = new H5EStack();
        add(stack).left().top().padRight(5).size(ICON_SIZE);
        icon = new H5EIcon(layer, data.iconKey, ICON_SIZE, ICON_SIZE);
        stack.add(icon);
        nameTable = new Table();
        nameTable.left().top();
        nameTable.defaults().left().top();
        add(nameTable).growX().fillY();
        nameLabel = new H5EScrollLabel(data.name, layer);
        descriptionLabel = new H5ELabel(data.description, layer);
        descriptionLabel.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        descriptionLabel.setColor(Color.valueOf("#AAAAAA"));
        descriptionLabel.setWrap(true);
        rebuildActor();
    }

    @Override
    public String getName() {
        return data.name;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        nameLabel.setAutoScroll(isChecked() || isOver());
    }

    @Override
    public void layout() {
        nameLabel.setupScrolling();
        super.layout();
    }

    public void rebuildActor() {
        nameTable.clear();
        nameTable.add(nameLabel).fillX().expandX();
        nameTable.row();
        nameTable.add(descriptionLabel).fillX().expandX();
    }
}
