package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.gamecomponents.client.utils.RandomNameGenerator;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

public abstract class GCCharacterSpawnDesktopBase<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> extends GCCharacterSpawnBase<L, S, C>  {
    protected static final int LOGIN_FIELD_W = 350;
    protected final Table characterNameWindow;
    protected final H5EInputBox fieldCharacterName;
    protected final ImageButton btnRandomName;
    protected final H5ELabel characterWarningText;
    protected final H5EIcon characterImage;
    protected final Color warningColor;
    protected final H5EButton btnBack;
    protected final Cell<H5ELabel> warningTextCell;
    final float warningTextFontSize = 0.75f;
    final float loginInfoFontSize = 1.1f;

    protected C characterInfo;

    protected boolean createIsActive = true;

    public GCCharacterSpawnDesktopBase(H5ELayer layer, GCLoginManager<L, S, C> loginManager) {
        super(layer, loginManager, false);

        characterNameWindow = new Table();
        characterNameWindow.defaults().center();
        warningColor = Color.RED;
        //Align title onto tab at the top
        characterNameWindow.setVisible(false);

        layer.addActorToTop(characterNameWindow);

        //Create login tab

        H5ELabel characterNameLabel = new H5ELabel(layer);
        characterNameLabel.setFontScale(loginInfoFontSize);
        characterNameLabel.setText("Character Name");

        //Space to enter email
        fieldCharacterName = new H5EInputBox(layer);
        fieldCharacterName.setWidth(LOGIN_FIELD_W);

        //Warning text for invalid emails
        characterWarningText = new H5ELabel(layer);
        characterWarningText.setWrap(true);
        characterWarningText.setWidth(250);
        characterWarningText.setFontScale(warningTextFontSize);
        characterWarningText.setColor(warningColor);
        characterWarningText.setAlignment(Align.center);
        characterWarningText.setVisible(false);

        // Randomize name button (temporary)
        btnRandomName = new ImageButton(layer.getEngine().getSkin(), "btn-random");
        btnRandomName.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                String randomName = RandomNameGenerator.generateRandomName(2, 4);
                fieldCharacterName.setText(randomName);
            }
        });

        Table characterNameTable = new Table();
        characterNameTable.add(fieldCharacterName).width(350);
        characterNameTable.add(btnRandomName);

        characterNameWindow.add(characterNameLabel);
        characterNameWindow.row();
        characterNameWindow.add(characterNameTable);
        characterNameWindow.row();
        warningTextCell = characterNameWindow.add(characterWarningText).width(350);
        characterNameWindow.row();
        Table buttonTable = new Table();
        characterNameWindow.add(buttonTable);

        btnBack = new H5EButton(getBackButtonText(), layer);
        btnBack.addButtonListener(this::back);
        buttonTable.add(btnBack);

        H5EButton btnNext = new H5EButton(getJoinText(), layer);
        btnNext.addButtonListener(this::next);
        buttonTable.add(btnNext);

        characterNameWindow.pack();

        characterImage = new H5EIcon(layer, layer.getEngine().getSkin().getDrawable("character-equipment2"), Scaling.fit, Align.center);
        layer.addActorToTop(characterImage);
        characterImage.toBack();
        characterImage.setHeight(layer.getHeight() / 2);
        characterImage.setPosition(layer.getWidth() / 2 - characterImage.getWidth() / 2, layer.getHeight() / 2 - characterImage.getHeight() / 2);

        UPUtils.tieButtonToKey(fieldCharacterName, Input.Keys.ENTER, btnNext);
    }

    protected String getBackButtonText() {
        return "Back";
    }

    protected String getJoinText() {
        return "Join with new Character";
    }

    protected void next() {
        if (!createIsActive) {
            return;
        }
        createIsActive = false;

        fieldCharacterName.unfocus();
        executeSpawn();
    }


    @Override
    protected boolean validateSpawn() {
        return true;
    }


    protected void processError(String error) {
        loadingIndicator.deactivate();
        if ("char_in_use_ingame".equals(error)) {
            characterWarningText.setText("Character name is already in use in this game. \nPlease use a different name.");
        } else if ("char_owned_by_other_user".equals(error)) {
            characterWarningText.setText("Character name is already in use by another player. \nPlease use a different name.");
        } else {
//            characterWarningText.setText("An unexpected error happened while creating the character. \nPlease try again later.");
            characterWarningText.setText(error);
        }
        characterWarningText.setVisible(true);
        fieldCharacterName.setColor(warningColor);
        createIsActive = true;
    }

    protected void displayWarning(String text) {
        characterWarningText.setText(text);
        characterWarningText.setVisible(true);
        fieldCharacterName.setColor(warningColor);
        createIsActive = true;
    }

    protected void openSpawnTypeSelector() {
        spawnSelectDialog.open();

        spawnSelectDialog.setPosition(0.0f, layer.getHeight() / 2 - spawnSelectDialog.getHeight() / 2);

        layer.getEngine().onResize.registerHandler(() -> spawnSelectDialog.setPosition(0.0f, layer.getHeight() / 2 - spawnSelectDialog.getHeight() / 2));
    }

    public void close() {
        characterWarningText.setText("");
        characterWarningText.setVisible(false);
        loadingIndicator.deactivate();
        //characterNameWindow.close();
        characterNameWindow.setVisible(false);
        spawnSelectDialog.close();
        btnBack.setVisible(false);
        characterImage.setVisible(false);
        friendWindow.close();
        characterSpawnWindow.close();
    }

    protected void open(L loginInfo, S serverInfo) {
        this.loginInfo = loginInfo;
        this.serverInfo = serverInfo;
        this.createIsActive = true;
        //characterNameWindow.open();

        characterNameWindow.setVisible(true);

        characterNameWindow.setPosition(layer.getWidth() / 2 - characterNameWindow.getWidth() / 2, 15.0f);

        layer.getEngine().onResize.registerHandler(() -> characterNameWindow.setPosition(layer.getWidth() / 2 - characterNameWindow.getWidth() / 2, 15.0f));

        openSpawnTypeSelector();
        characterImage.setVisible(true);
        characterImage.setHeight(layer.getHeight() / 2);

        btnBack.setVisible(true);
    }

    public boolean isOpen() {
        return characterNameWindow.isVisible();
    }
}
