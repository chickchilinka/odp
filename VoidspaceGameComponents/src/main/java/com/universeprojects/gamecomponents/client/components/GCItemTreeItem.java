package com.universeprojects.gamecomponents.client.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;

// A category of an item tree
public class GCItemTreeItem extends GCItemInternalTree {

    protected final H5ELayer layer;
    private final ImageTextButton.ImageTextButtonStyle categoryUnopenedStyle;
    private final ImageTextButton.ImageTextButtonStyle categoryOpenedStyle;

    protected final GCItemInternalTree itemTree;

    private Table listTable;
    protected List<GCItemTreeSubitem> itemList = new ArrayList<>();

    public GCItemTreeItem(H5ELayer layer, GCItemInternalTree itemTree, String name) {
        super(layer);
        this.layer = layer;
        this.itemTree = itemTree;

        categoryUnopenedStyle = layer.getEngine().getSkin().get("button-tree-item-unopened", ImageTextButton.ImageTextButtonStyle.class);
        categoryOpenedStyle = layer.getEngine().getSkin().get("button-tree-item-opened", ImageTextButton.ImageTextButtonStyle.class);

        listTable = new Table();

        H5EButton catButton = ButtonBuilder.inLayer(layer).withStyle("button-tree-item-unopened").build();
        catButton.addCheckedButtonListener(() -> {
            if (catButton.getStyle() == categoryOpenedStyle) {
                catButton.setStyle(categoryUnopenedStyle);
                this.clear();
                this.add(catButton).left().padBottom(1).padTop(1);
                this.row();
            } else {
                catButton.setStyle(categoryOpenedStyle);
                this.add(listTable).left().padLeft(10).width(getWidth() - 10).fillX();

            }
            catButton.setChecked(false);
        });
        H5ELabel catLabel = catButton.add(new H5ELabel(name, layer)).padTop(-2).padLeft(25).left().growX().getActor();
        catLabel.setColor(Color.valueOf("#89cdfe"));
        catLabel.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        add(catButton).left().padBottom(1).padTop(1);
        row();

    }

    @Override
    public GCItemTreeSubitem addItem(String name, Callable0Args callable) {
        GCItemTreeSubitem item = listTable.add(new GCItemTreeSubitem(layer, itemTree, name, callable)).left().getActor();
        listTable.row();
        itemList.add(item);
        return item;
    }

    @Override
    public GCItemTreeItem addCategory(String name) {
        GCItemTreeItem item = listTable.add(new GCItemTreeItem(layer, itemTree, name)).left().getActor();
        listTable.row();
        return item;
    }


}
