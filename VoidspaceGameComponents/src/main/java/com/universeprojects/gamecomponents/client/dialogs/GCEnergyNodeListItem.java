package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

class GCEnergyNodeListItem extends GCListItem {

    private final String uid;

    private final H5EButton actionBtn;

    GCEnergyNodeListItem(H5ELayer layer, GCEnergyNodeItemActionHandler actionHandler, String uid, String name) {
        super(layer, actionHandler);

        this.uid = Dev.checkNotEmpty(uid);
        setName(name);

        GCLabel label = add(new GCLabel(layer)).grow().getActor();
        label.setAlignment(Align.left);
        label.setFontScale(0.8f);
        label.setColor(Color.valueOf("#CCCCCC"));
        label.setText(name);

        actionBtn = add(new H5EButton("open", layer)).right().getActor();
        actionBtn.setColor(Color.valueOf("#AAAAAA"));
        actionBtn.getLabel().setColor(Color.valueOf("#FFFFFF"));
        actionBtn.getLabel().setFontScale(0.6f);
        actionBtn.setVisible(false);

        actionBtn.addButtonListener(() -> {
            if (actionHandler != null) {
                actionHandler.handleActionBtn(GCEnergyNodeListItem.this);
            }
        });
    }

    void hideActionButton() {
        actionBtn.setVisible(false);
    }

    void showActionButton() {
        actionBtn.setVisible(true);
    }

    String getUid() {
        return uid;
    }

    static abstract class GCEnergyNodeItemActionHandler extends GCListItemActionHandler<GCEnergyNodeListItem> {

        @Override
        protected void onSelectionUpdate(GCEnergyNodeListItem lastSelectedItem) {
            if (lastSelectedItem != null) {
                lastSelectedItem.hideActionButton();
            }

            GCEnergyNodeListItem selectedItem = getSelectedItem();
            if (selectedItem != null) {
                selectedItem.showActionButton();
            }
        }

        abstract void handleActionBtn(GCEnergyNodeListItem item);
    }


}
