package com.universeprojects.gamecomponents.client.dialogs;


import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.MapMarkerGroupData;

public class GCMarkersGroupItem extends GCListItem {

    private final H5EInputBox inputBox;
    private final H5EStack textStack;
    private final H5ELabel nameLabel;
    private final Callable2Args onRename;
    private final ImageButton deleteBtn;
    private MapMarkerGroupData data;
    private ButtonStyle normalStyle;
    private ButtonStyle overStyle;


    public GCMarkersGroupItem(H5ELayer layer, GCListItemActionHandler<GCMarkersGroupItem> handler, MapMarkerGroupData data, Callable2Args<String, GCMarkersGroupItem> onRename, Callable1Args<GCMarkersGroupItem> onDelete) {
        super(layer, handler);
        this.data = data;
        this.onRename = onRename;
        textStack = new H5EStack();
        nameLabel = new H5ELabel(data.name, layer);
        textStack.add(nameLabel);
        nameLabel.setWrap(true);
        Table subTable = new Table();
        subTable.left();
        subTable.add(textStack).growX().left().padLeft(10);
        left();
        add(subTable).grow();
        normalStyle = getStyle();
        overStyle = new ButtonStyle(normalStyle);
        overStyle.up = overStyle.over;
        overStyle.checked = overStyle.over;
        inputBox = new H5EInputBox(layer);
        inputBox.setText(data.name);
        inputBox.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ENTER) {
                    String oldName = new String(data.name);
                    disableEditMode();
                    onRename.call(oldName, GCMarkersGroupItem.this);
                }
                return super.keyDown(event, keycode);
            }
        });
        deleteBtn = new ImageButton(layer.getEngine().getSkin(), "btn-close-window");
        if (!data.name.equals("All"))
            add(deleteBtn).right();
        deleteBtn.setVisible(false);
        deleteBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                onDelete.call(GCMarkersGroupItem.this);
            }
        });

    }

    public void toggleEditMode() {
        textStack.clear();
        textStack.add(inputBox);
        inputBox.setText(data.name);
        inputBox.focus();
        setChecked(true);
        setDisabled(true);
        showControls(false);
    }

    public void disableEditMode() {
        textStack.clear();
        if (!inputBox.getText().equals("")) {
            data.name = inputBox.getText();
            nameLabel.setText(data.name);
        }
        textStack.add(nameLabel);
        setDisabled(false);
    }

    public void showControls(boolean show) {
        deleteBtn.setVisible(show);
    }

    public String getGroupName() {
        return data.name;
    }

    public MapMarkerGroupData getData() {
        return data;
    }

    public ButtonStyle getNormalStyle() {
        return normalStyle;
    }

    public ButtonStyle getOverStyle() {
        return overStyle;
    }

    public void setName(String newName) {
        data.name = newName;
        inputBox.setText(data.name);
        nameLabel.setText(data.name);
    }
}
