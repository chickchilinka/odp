package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.GenericEvent;
import com.universeprojects.html5engine.shared.UPUtils;

/**
 * Creates a generic UI for players to confirm a decision. UI components that need a confirmation should implement
 * {@link Confirmable} as this screen will call the implemented method after the user confirms/denies their decision.
 */
public class GCConfirmationScreen {

    private GCWindow confirmationWindow;
    private boolean confirmed;
    private boolean callConfirmedOnExit = false;
    private Confirmable thingToConfirm;
    private H5ELabel warningText;

    public GCConfirmationScreen(H5ELayer layer, Confirmable thingToConfirm,String confirmationText ) {
        this.thingToConfirm=thingToConfirm;
        confirmationWindow = new GCWindow(layer, 444, 300);
        confirmationWindow.positionProportionally(0.5f,0.5f);
        confirmationWindow.setTitle("Are You Sure?");
        confirmationWindow.open();
        confirmationWindow.close();

        Table mainTable = new Table();
        confirmationWindow.add(mainTable).grow().colspan(2);
        mainTable.background(layer.getEngine().getSkin().getDrawable("inventory-background"));


        H5EButton btnConfirm = new H5EButton("Yes",layer);
        btnConfirm.addButtonListener(this::confirmed);
        H5EButton btnBack=new H5EButton("No",layer);
        btnBack.addButtonListener(this::openConfirmedWindow);
        warningText = new H5ELabel(confirmationText,layer);
        mainTable.add(warningText).expandY();
        confirmationWindow.row().padTop(20);
        confirmationWindow.add(btnConfirm);
        confirmationWindow.add(btnBack);
        confirmationWindow.onClose.registerHandler(this::onClose);
        confirmationWindow.setPackOnOpen(true);
    }

    public boolean isCallConfirmedOnExit() {
        return callConfirmedOnExit;
    }

    public void setCallConfirmedOnExit(boolean callConfirmedOnExit) {
        this.callConfirmedOnExit = callConfirmedOnExit;
    }

    /**
     * Called when the user clicks "yes" to confirm their decision.
     */
    public void  confirmed(){
        confirmed=true;
        openConfirmedWindow();
    }
    public void openConfirmedWindow (){
        thingToConfirm.confirmed();
        this.close();
    }

    /**
     * This method should be called after the user has selected an option.
     * @return true if the user selected yes, otherwise returns false.
     */
    public boolean isConfirmed(){
        return confirmed;
    }

    /**
     * Change the message of the confirmation screen.
     * @param newText
     */
    public void setText(String newText){
        warningText.setText(newText);
    }
    public void setTitle(String newText){
        confirmationWindow.setTitle(newText);
    }

    public void open() {
        confirmed =false;
        confirmationWindow.open();
    }

    public void close() {
        confirmationWindow.close();
    }

    protected void onClose() {
        if (isCallConfirmedOnExit()) {
            openConfirmedWindow();
        }
    }

    public boolean isOpen() {
        return confirmationWindow.isOpen();
    }

    public void positionProportionally(float propX, float propY) {
        confirmationWindow.positionProportionally(propX, propY);
    }

}
