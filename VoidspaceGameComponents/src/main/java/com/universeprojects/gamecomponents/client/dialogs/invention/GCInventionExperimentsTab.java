package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.SnapshotArray;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.components.GCItemInternalTree;
import com.universeprojects.gamecomponents.client.components.GCItemTreeSubitem;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

import java.util.List;

public class GCInventionExperimentsTab extends GCInventionTab {

    private final H5EScrollablePane listContainer;
    private final Cell<?> repeatsCell;
    private final H5ELabel fixedRepeats;
    private GCList<GCExperimentsListItem> itemsList;
    private final GCListItemActionHandler<GCExperimentsListItem> listHandler;
    private final H5EInputBox txtRepeats;
    private final H5EScrollablePane treeContainer;
    public static final int LIST_SPACING = 5;

    private final GCItemInternalTree leftTree;

    private float paneWidth = 0;
    private GCInventoryData currentData;

    GCInventionExperimentsTab(final GCInventionSystemDialog dialog) {
        super(dialog, "Experimentation");

        listContainer = rightContent.add(new H5EScrollablePane(layer) {
            @Override
            public void act(float delta) {
                super.act(delta);
                if (UPMath.abs(getWidth() - paneWidth) > 5) {
                    paneWidth = getWidth();
                    if (itemsList == null) return;
                    for (GCExperimentsListItem item : itemsList.getItems()) {
                        item.rebuildActor();
                    }
                }
            }
        }).left().top().grow().getActor();
        listContainer.getContent().top().left();

        listHandler = new GCListItemActionHandler<GCExperimentsListItem>() {
            @Override
            protected void onSelectionUpdate(GCExperimentsListItem lastSelectedItem) {
                //Reset the repeats text box
                GCExperimentsListItem selectedItem = this.getSelectedItem();
                if (selectedItem != null && !selectedItem.equals(lastSelectedItem)) {
                    resetRepeats();
                }
                Tutorials.trigger("window:experiment:select-item"+ (selectedItem != null ? ":"+selectedItem.data.item.getName() : ""));
            }

            @Override
            public void onInfoButtonClicked(GCExperimentsListItem item) {
                dialog.onItemIconInfoClicked(item, item.data);
            }
        };

        filterCell.colspan(2);
        treeContainer = leftContent.add(new H5EScrollablePane(layer)).grow().padBottom(12).colspan(2).getActor();
        treeContainer.getContent().top().left();
        leftTree = treeContainer.add(new GCItemInternalTree(layer)).top().left().getActor();

        //Two columns are needed to get the correct spacing of the label and input box
        //leftContent.add().top().left().grow().colspan(2);
        leftContent.row().padBottom(5);

        final H5ELabel txtRepeatsLabel = leftContent.add(new H5ELabel(layer)).fill().padLeft(5).getActor();
        txtRepeatsLabel.setText("Repeats: ");

        repeatsCell = leftContent.add().growX().padRight(5);
        fixedRepeats = new H5ELabel(layer);
        fixedRepeats.setAlignment(Align.center);

        txtRepeats = new H5EInputBox(layer);
        txtRepeats.setTypeNumber();
        txtRepeats.setAlignment(Align.right);
        txtRepeats.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                txtRepeats.selectAll();
            }
        });
        resetRepeats();

        final H5EButton btnBeginExperiments = ButtonBuilder.inLayer(layer).withStyle("button2").withTutorialId("button:invention:start-experiment").withText("Study Item").build();
        lowerButtonRow.add(btnBeginExperiments).grow();
        btnBeginExperiments.addButtonListener(() -> {
            int repeats = 1;
            try {
                repeats = Integer.parseInt(txtRepeats.getText());
            } catch (NumberFormatException e) {
                //Ignore
            }

            GCExperimentsListItem selectedItem = listHandler.getSelectedItem();
            String selectedItemUid = selectedItem != null ? selectedItem.data.item.getUid() : null;
            Tutorials.trigger("process:start:experiment");
            dialog.onBeginExperimentsBtnPressed(selectedItemUid, repeats);
        });

        UPUtils.tieButtonToKey(txtRepeats, Input.Keys.ENTER, btnBeginExperiments);
    }

    /**
     * Reset the repeats input box
     */
    public void resetRepeats () {
        txtRepeats.setText("1");
        txtRepeats.selectAll();
        txtRepeats.focus();
        repeatsCell.setActor(txtRepeats);
    }

    public void setFixedRepeats(String value) {
        fixedRepeats.setText(value);
        repeatsCell.setActor(fixedRepeats);
    }

    public void setSelectedItem(String uid) {
        SnapshotArray<GCExperimentsListItem> items = itemsList.getItems();
        for (GCExperimentsListItem item : items) {
            if (!item.data.item.getUid().equals(uid))
                continue;

            item.setChecked(true);
            listHandler.setSelection(item);
        }
    }

    public String getSelectedItemName(){
        if(listHandler == null)return "";
        GCExperimentsListItem selected = listHandler.getSelectedItem();
        if(selected == null || selected.data == null || selected.data.item.getName() == null)return "";
        return selected.data.item.getName();
    }

    @Override
    public void show () {
        super.show();

        txtRepeats.selectAll();
        txtRepeats.focus();
    }

    @Override
    protected void clearTab() {
        if (itemsList != null) {
            listHandler.reset();
            for(GCExperimentsListItem listItem : itemsList.getItems()) {
                Tutorials.removeObject("button:invention:item:" + listItem.getName());
            }
            itemsList.clear();
            itemsList.remove();
            itemsList = null;
            listContainer.getContent().clear();
        }
    }

    void populateData(GCInventoryData inventoryData) {
        clearTab();
        this.currentData=inventoryData;
        itemsList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        listContainer.add(itemsList).left().top().grow();
        for (int index = 0; index < inventoryData.size(); index++) {
            GCInventoryDataItem dataItem = inventoryData.get(index);
            if(dataItem.item.getName().toLowerCase().contains(filterText.toLowerCase())) {
                GCExperimentsListItem listItem = new GCExperimentsListItem(layer, listHandler, dataItem, listContainer);
                itemsList.addItem(listItem);
                Tutorials.registerObject("button:invention:item:" + listItem.getName(), listItem);
            }
        }
    }

    void populateCategories(List<GCInventoryData> inventoryDataList) {
        leftTree.clearTree();
        GCInventoryData allItems = GCInventoryData.createNew();
        GCItemTreeSubitem leafAll = leftTree.addItem("All",() -> {
            titleLabel.setText("Experimentation: All");
            populateData(allItems);
        });
        for (GCInventoryData invData : inventoryDataList) {
            allItems.add(invData);
            leftTree.addItem(invData.getInventoryName(),() -> {
                titleLabel.setText("Experimentation: "+invData.getInventoryName());
                populateData(invData);
            });
        }
        leafAll.click();
    }

    @Override
    public void windowStateChanged(GCInventionSystemDialog.WindowState windowState) {
        super.windowStateChanged(windowState);
        leftContent.setBackground(rightTitleArea.getBackground());
        rightContent.setBackground(rightTitleArea.getBackground());
        treeContainer.setStyle(layer.getEngine().getSkin().get("default", ScrollPane.ScrollPaneStyle.class));
        switch (windowState){
            case DESKTOP:
                break;
            case PORTRAIT:
                treeContainer.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
                rightContent.setBackground(StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque);
                dialog.mainTable.getCell(dialog.leftContent).height(300);
                dialog.mainTable.getCell(dialog.rightContent).maxHeight(300);
                break;
            case LANDSCAPE:
                break;
        }
    }

    @Override
    protected void onFilter() {
        super.onFilter();
        if(currentData!=null) {
            populateData(currentData);
        }
    }
}
