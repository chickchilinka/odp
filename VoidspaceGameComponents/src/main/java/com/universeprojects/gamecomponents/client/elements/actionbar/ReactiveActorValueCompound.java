package com.universeprojects.gamecomponents.client.elements.actionbar;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.HierarchyReachableEvent;
import com.badlogic.gdx.scenes.scene2d.HierarchyReachableListener;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.util.ReactiveValue;
import com.universeprojects.common.shared.util.ReactiveValueListener;

/**
 * This class handles
 *
 * @param <T>
 */
public class ReactiveActorValueCompound<T> {

    private ReactiveValue<T> currentReactiveValue;
    private ReactiveValueListener<T> currentReactiveListener;
    private Callable1Args<T> callback;
    private T value;


    public ReactiveActorValueCompound(Actor actor) {
        actor.addListener(new HierarchyReachableListener() {
            @Override
            public void handle(HierarchyReachableEvent event) {
                if (event.becameReachable()) {
                    if (currentReactiveValue != null && currentReactiveListener == null)
                        resumeListening();
                } else {
                    if (currentReactiveValue != null && currentReactiveListener != null) {
                        pauseListening();
                    }
                }
            }
        });
    }

    public void setValue(ReactiveValue<T> value) {
        if (currentReactiveListener != null) {
            pauseListening();
            currentReactiveValue = null;
        }

        this.currentReactiveValue = value;
        if (this.currentReactiveValue != null)
            resumeListening();
        else
            this.callback.call(null);
    }

    public boolean hasReactiveValue() {
        return currentReactiveValue != null;
    }

    public void pauseListening() {
        if (this.currentReactiveListener == null
                || this.currentReactiveValue == null)
            throw new IllegalStateException("Attempted to pause listening when listening is not in progress");

        this.currentReactiveValue.removeListener(currentReactiveListener);
        currentReactiveListener = null;
    }

    public void resumeListening() {
        if (this.currentReactiveListener != null)
            throw new IllegalStateException("Attempted to register listener over existing listener");

        if (this.currentReactiveValue == null)
            throw new IllegalStateException("Attempted to register listener over an not existing value");

        this.currentReactiveListener = this.currentReactiveValue.getAndListen(
                (oldValue, newValue) -> this.callback.call(newValue)
        );
    }

    public void setCallback(Callable1Args<T> callback) {
        this.callback = callback;
    }
}
