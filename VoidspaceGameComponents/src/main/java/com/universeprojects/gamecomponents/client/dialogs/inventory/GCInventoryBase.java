package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class GCInventoryBase<K, T extends GCInventoryItem, C extends GCInventorySlotBaseComponent<K, T, C>> extends Table implements GraphicElement {
    private final Map<K, C> components = new LinkedHashMap<>();
    private final Map<K, C> components_view = Collections.unmodifiableMap(components);

    C highlightedComponent;
    C selectedComponent;

    private boolean itemsInvalid = true;
    private boolean componentsInvalid = true;

    public void showLockedSlotWarning() {

    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        if(getLayer() != null) {
            return getLayer().getEngine();
        }
        else {
            return null;
        }
    }

    public GCInventoryBase(H5ELayer layer) {
        super(layer.getEngine().getSkin());
        setStage(layer);
    }

    public boolean isHighlighted(C component) {
        return component == highlightedComponent;
    }

    public boolean isSelected(C component) {
        return component == selectedComponent;
    }

    public Map<K, C> getComponents() {
        return components_view;
    }

    public void select(C component) {
        if(component != null && component.isEmpty()) {
            component = null;
        }
        C oldSelected = selectedComponent;
        if(component == oldSelected) component = null; //Deselect on repeated selection
        this.selectedComponent = component;
        if(oldSelected != null) {
            oldSelected.invalidate();
        }
        if(selectedComponent != null)
            selectedComponent.invalidate();
        onSelected(selectedComponent);
    }

    public void highlight(C component) {
        C oldHighlighted = highlightedComponent;
        this.highlightedComponent = component;
        if(oldHighlighted != null)
            oldHighlighted.invalidate();
        if(highlightedComponent != null)
            highlightedComponent.invalidate();

    }

    protected void invalidateComponents() {
        componentsInvalid = true;
    }

    protected void invalidateItems() {
        itemsInvalid = true;
        invalidateHierarchy();
    }

    protected void updateItems() {
        if(!itemsInvalid) return;
        itemsInvalid = false;
        String inventoryType = this instanceof GCEquipmentDialog ? "equipment" : this instanceof GCInventory ? "inventory" : "";

        for (Map.Entry<K, C> entry : components.entrySet()) {
            if(entry.getValue().getItem() != null) {
                Tutorials.removeObject(inventoryType + ":item:" + entry.getValue().getItem().getName());
            }
            T item = getItem(entry.getKey());
            entry.getValue().setItem(item);
            if(item != null) {
                Tutorials.registerObject(inventoryType + ":item:" + item.getName(), entry.getValue());
            }
        }
    }

    protected void updateComponents() {
        if(!componentsInvalid) return;
        componentsInvalid = false;
        doUpdateComponents();
    }

    protected void doUpdateComponents() {
        final Collection<K> slotKeys = getSlots();
        final Set<K> componentKeys = components.keySet();
        List<K> slotsWithoutComponent = new ArrayList<>(slotKeys);
        slotsWithoutComponent.removeAll(componentKeys);
        List<K> componentsWithoutSlot = new ArrayList<>(componentKeys);
        componentsWithoutSlot.removeAll(slotKeys);

        for(K key : componentsWithoutSlot) {
            final C component = components.remove(key);
            removeDragAndDrop(component);
            component.remove();
        }

        for(K key : slotsWithoutComponent) {
            final C component = createComponent(key);
            setupDragAndDrop(component);
            components.put(key, component);
        }
    }

    protected void setupDragAndDrop(C component) {
        component.dragAndDropSource = new ComponentSource<>(component);
        component.dragAndDropTarget = new ComponentTarget<>(component);
        final DragAndDrop dragAndDrop = InventoryManager.getInstance(getEngine()).getDragAndDrop();
        dragAndDrop.addSource(component.dragAndDropSource);
        dragAndDrop.addTarget(component.dragAndDropTarget);
    }

    protected void removeDragAndDrop(C component) {
        final DragAndDrop dragAndDrop = InventoryManager.getInstance(getEngine()).getDragAndDrop();
        if(component.dragAndDropSource != null) {
            dragAndDrop.removeSource(component.dragAndDropSource);
        }
        if(component.dragAndDropTarget != null) {
            dragAndDrop.removeTarget(component.dragAndDropTarget);
        }
        component.dragAndDropSource = null;
        component.dragAndDropTarget = null;
    }


    public void clearComponents() {
        String dialogType = this instanceof GCEquipmentDialog ? "equipment" : this instanceof GCInventory ? "inventory" : "";
        for(C component : components.values()) {
            if(component.dragAndDropTarget != null) {
                removeDragAndDrop(component);
                if(component.getItem() != null) {
                    Tutorials.removeObject(dialogType + ":item:" + component.getItem().getName());
                }
            }
        }
        components.clear();
        clearChildren();
    }

    public C getSelectedComponent() {
        return selectedComponent;
    }

    protected abstract C createComponent(K key);
    protected abstract Collection<K> getSlots();
    protected abstract T getItem(K key);
    protected abstract List<InventoryAction> getActions(K key);
    protected abstract <KT> void onCrossInventoryDropFromHere(K startKey, T item, GCInventoryBase<KT, ?, ?> targetInventory, boolean willBeUnlinkedLater);
    protected abstract <KS, I extends GCInventoryItem> void onCrossInventoryDropToHere(GCInventoryBase<KS,?, ?> startInventory, KS startKey, I item, K targetKey);
    protected abstract void onSameInventoryDrop(K startKey, T item, K targetKey);
    protected abstract <KT> boolean canCrossInventoryDropFromHere(K startKey, T item, GCInventoryBase<KT, ?, ?> targetInventory);
    protected abstract <KS, I extends GCInventoryItem> boolean canCrossInventoryDropToHere(GCInventoryBase<KS,?, ?> startInventory, KS startKey, I item, K targetKey);
    protected abstract boolean canSameInventoryDrop(K startKey, T item, K targetKey);
    protected abstract void onSelected(C component);
    protected abstract void jettison (K startKey, T item);
    protected abstract boolean canJettison (K startKey, T item);
}
