package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCSkillsData.GCSkillDataItem;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

class GCItemsListItem extends GCListItem {

    public static final int ICON_SIZE = 64;
    final GCSkillDataItem data;

    private final H5EIcon icon;
    private final Table subTable;
    private final H5EScrollLabel nameLabel;
    private final H5ELabel descTextArea;
    private final Table buttons;
    private final H5EButton updateBtn;
    private final Cell<Table> buttonsCell;

    private final H5EScrollablePane parentScrollPane;

    public GCItemsListItem(H5ELayer layer, GCListItemActionHandler<GCItemsListItem> actionHandler, GCSkillDataItem data, H5EScrollablePane parentScrollPane, Callable0Args onSkillUpgradeButtonPress) {
        super(layer, actionHandler);
        this.parentScrollPane = parentScrollPane;

        this.data = Dev.checkNotNull(data);
        if (data.index < 0) {
            throw new IllegalArgumentException("Index can't be negative");
        }
        if (Strings.isEmpty(data.name)) {
            throw new IllegalArgumentException("Skill name can't be blank");
        }
        if (Strings.isEmpty(data.description)) {
            throw new IllegalArgumentException("Skill description can't be blank");
        }
        if (Strings.isEmpty(data.iconKey)) {
            throw new IllegalArgumentException("Skill icon key can't be blank");
        }

        if (Strings.isEmpty(data.iconKey)) {
            icon = null;
        } else {
            H5EStack stack = new H5EStack();
            add(stack).left().top().padRight(5).size(ICON_SIZE);
            icon = H5EIcon.fromSpriteType(layer, data.iconKey);
            stack.add(icon);
            icon.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    actionHandler.onInfoButtonClicked(GCItemsListItem.this);
                    event.handle();
                }
            });

        }

        subTable = new Table();
        subTable.left().top();
        subTable.defaults().left().top();
        add(subTable).growX().fillY();

        nameLabel = new H5EScrollLabel(data.name, layer);
        nameLabel.getLabel().setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        nameLabel.getLabel().setColor(data.rarityColor);

        String finalDescription = "[" + UPUtils.secondsToTimeShortString(data.baseSeconds) + "] " + data.description;
        descTextArea = new H5ELabel(finalDescription, layer);
        descTextArea.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        descTextArea.setColor(Color.valueOf("#AAAAAA"));
        descTextArea.setWrap(true);
        row();
        buttons=new Table();
        add();
        //noinspection unchecked
        buttonsCell=add();
        updateBtn = new H5EButton("Upgrade", layer, StyleFactory.INSTANCE.buttonStyleBlueYellow);
        updateBtn.addButtonListener(onSkillUpgradeButtonPress);
        buttons.add(updateBtn).left().top().pad(5).padLeft(0);
        //descTextArea.setTouchable(Touchable.disabled);

        rebuildActor();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        nameLabel.setAutoScroll(isChecked() || isOver());
    }

    public void rebuildActor() {
        subTable.clear();
        int newWidth = (int) (parentScrollPane.getWidth() - ICON_SIZE - 17);
        subTable.add(nameLabel).width(newWidth);
        nameLabel.setWidth(newWidth);
        nameLabel.setupScrolling();
        subTable.row();
        subTable.add(descTextArea).growX().getActor();
    }

    public void hideButtons(){
        buttonsCell.clearActor();
    }

    public void showButtons(){
        buttonsCell.setActor(buttons);
    }
}
