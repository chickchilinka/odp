package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.common.shared.util.TextBuilder;
import com.universeprojects.gamecomponents.client.common.UIFactory;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;

import java.util.ArrayList;
import java.util.List;

public class GCChangeLogDialog {
	private GCSimpleWindow window;
	private H5EScrollablePane scrollablePane;
	private GCLoadingIndicator loadingIndicator;

	private final List<GCChangeLogVersionLog> versionList = new ArrayList<>();

	private static final String[] COLOR_LOOKUP_TABLE = {
			"#CCCCCC",  //default
			"#2D64AD",  //code-low
			"#4982CC",  //code-medium
			"#85AEE5",  //code-high
			"#2DAD2F",  //content-low
			"#56CC48",  //content-medium
			"#85E588"   //content-high
	};

	public GCChangeLogDialog (H5ELayer layer) {
		final int WIDTH = 600;
		final int HEIGHT = 500;

		loadingIndicator = new GCLoadingIndicator(layer);

		window = new GCSimpleWindow(layer, "changeLogWindow", "VoidSpace Change Log", WIDTH, HEIGHT, false);
		window.setFullscreenOnMobile(true);
		scrollablePane = new H5EScrollablePane(layer, "scrollpane-backing-1");
		scrollablePane.getContent().pad(5);

		window.add(scrollablePane).grow();
	}

	public void open() {
		window.positionProportionally(.5f, .5f);
		window.open();
	}

	public void close() {
		window.close();
	}

	public void destroy() {
		window.destroy();
	}

	public boolean isOpen() {
		return window.isOpen();
	}

	public void activate() {
		window.activate();
	}

	public void positionProportionally(float propX, float propY) {
		window.positionProportionally(propX, propY);
	}

	public void flagLoading() {
		loadingIndicator.activate(window);
	}

	public void clear () {
		scrollablePane.getContent().clear();
		versionList.clear();
	}

	/**
	 * Add an entry to the changelog
	 * @param gameVersion
	 * @param interest
	 * @param timestamp
	 * @param type
	 * @param logItems
	 */
	public void addChangeLogEntry (String gameVersion, String interest, List<String> logItems, String timestamp, String type) {
		final int ENTRY_SPACING = 5;

		GCChangeLogVersionLog versionLog = null;
		for (GCChangeLogVersionLog log : versionList) {
			if (gameVersion.equals(log.getVersion())) {
				versionLog = log;
				break;
			}
		}
		if (versionLog == null) {
			versionLog = new GCChangeLogVersionLog(window.getLayer(), gameVersion);
			versionList.add(versionLog);
			scrollablePane.add(versionLog).grow().padBottom(20);
			scrollablePane.getContent().row();
		}

		//Style the labels
		int colorIndex = 0;

		if (type.equalsIgnoreCase("Code")) {
			colorIndex += 1;
		} else if (type.equalsIgnoreCase("Content")) {
			colorIndex += 4;
		}

		if (interest.equalsIgnoreCase("Low")) {
			colorIndex += 0;
		} else if (interest.equalsIgnoreCase("Medium")) {
			colorIndex += 1;
		} else if (interest.equalsIgnoreCase("High")) {
			colorIndex += 2;
		}

		for (String logItem : logItems) {
			versionLog.addChangeLogEntry(logItem, interest, COLOR_LOOKUP_TABLE[colorIndex]);
		}
	}

	/**
	 * Stop displaying the loading icon
	 */
	public void finishLoading () {
		TextBuilder text = new TextBuilder();

		for (int i = 0; i < 20; i++) {
			text.appendLine();
		}

		text.appendLine(" Congratulations! You've found an easter egg!");

		new UIFactory(scrollablePane.getContent(), window.getLayer())
				.addLabel(text.toString());

		loadingIndicator.deactivate();
	}
}
