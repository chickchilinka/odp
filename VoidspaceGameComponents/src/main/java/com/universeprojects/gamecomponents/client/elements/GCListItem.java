package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.html5engine.client.framework.DoubleClickListener;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.inputs.H5EButtonInvalidateListener;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public abstract class GCListItem extends Button implements GraphicElement {

    private final GCListItemActionHandler actionHandler;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    protected GCListItem(H5ELayer layer, GCListItemActionHandler<?> actionHandlerIn) {
        this(layer, actionHandlerIn, "gc-list-item");
    }

    protected GCListItem(H5ELayer layer, GCListItemActionHandler<?> actionHandlerIn, String style) {
        super(layer.getEngine().getSkin(), style);
        addListener(new H5EButtonInvalidateListener(this));
        setStage(layer);

        left().top();
        defaults().left().top();

        this.actionHandler = actionHandlerIn;
        if (actionHandler != null) {
            setTouchable(Touchable.enabled);
            addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    if (isChecked()) {
                        //noinspection unchecked
                        actionHandler.setSelection(GCListItem.this);
                    } else {
                        actionHandler.clearSelection();
                    }
                    event.handle();
                }
            });
            addListener(new DoubleClickListener() {
                @Override
                public boolean onDoubleClick(InputEvent event, float x, float y) {
                    //noinspection unchecked
                    actionHandler.onDoubleClick(GCListItem.this);
                    return true;
                }
            });
        }
    }

    /**
     * For internal use only.
     * Destroys the graphical representation of this item, such that it is not longer rendered by the engine.
     * This is intended to be done only when the item is no longer needed.
     */
    public void destroy() {
        remove();
    }
}
