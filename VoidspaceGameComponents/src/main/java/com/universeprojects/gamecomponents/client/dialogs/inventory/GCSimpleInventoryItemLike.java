package com.universeprojects.gamecomponents.client.dialogs.inventory;

/**
 * Used so we can store something that is similar to an inventory item in a component that normally
 * displays inventory items.
 */
public class GCSimpleInventoryItemLike extends GCSimpleInventoryItem {

    public GCSimpleInventoryItemLike(String uid, String name, String icon, int quantity, Rarity rarity) {
        this.uid = uid;
        this.stackable = true;
        this.name = name;
        this.iconSpriteKey = icon;
        this.quantity = quantity;
        this.rarity = rarity;
        this.canBeCustomized = false;
    }

    public GCSimpleInventoryItemLike(Object rawEntity, String name, String icon, String secondName, Rarity rarity) {
        this.rawEntity = rawEntity;
        this.name = name;
        this.iconSpriteKey = icon;
        this.itemClass = secondName;
        this.rarity = rarity;
        this.canBeCustomized = false;
    }

    public void setQuantity(int newQuantity) {
        this.quantity = newQuantity;
    }
}
