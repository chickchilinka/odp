package com.universeprojects.gamecomponents.client.dialogs.login;


import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EProgressBar;
import com.universeprojects.json.shared.JSONObject;

public class GCRealmCharacterCreate<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> extends GCCharacterCreateScreen<L, S, C> {
    private final Logger log = Logger.getLogger(GCRealmCharacterCreate.class);
    private final int[] ATTEMPT_DELAYS = new int[]{1000, 2000, 5000};
    private final H5EProgressBar progressBar;

    private boolean isWaitingToRetry = false;
    private Timer.Task currentTask;
    private int attempts;
    private String characterName;
    private S currentConnectingServer;

    public GCRealmCharacterCreate(H5ELayer layer, GCLoginManager<L, S, C> loginManager) {
        super(layer, loginManager);

        progressBar = new H5EProgressBar(layer) {
            @Override
            protected void syncText() {
                if (isWaitingToRetry) {
                    updateProgressText("Waiting to reconnect");
                    return;
                }
                RealmJoinState state = RealmJoinState.getById((int) getValue());
                if (state == null) {
                    updateProgressText("Connecting...");
                } else {
                    updateProgressText(state.getStateName());
                }
            }
        };
        progressBar.setOrigin(Align.center);
        progressBar.setDisplayMode(H5EProgressBar.DisplayMode.LABEL_PERCENTAGE);
        progressBar.setRange(0, RealmJoinState.getMaxStateId());

        warningTextCell.width(characterNameWindow.getWidth()).height(25F);
    }

    @Override
    protected void spawn(String spawnType, JSONObject spawnParams) {
        throw new RuntimeException("This method should not be called for GCRealmCharacterCreate (it doesnt care about spawnType)");
    }

    @Override
    protected void back() {
        close();
        loginManager.openServerSelectScreen(loginInfo);
        stopTasks();
    }

    public void startConnection() {
        stopTasks();
        loadingIndicator.activate(characterNameWindow);
        this.attempts = 0;
        allocateRealmServer((server, isReady) -> {
            log.info("Allocated server: ready - {}", isReady);
            currentConnectingServer = server;
            if (isReady) {
                pingBattleRoyale();
            } else {
                setJoinState(RealmJoinState.WAITING_SERVER_START);
                scheduleAttemptIn(1000);
            }
        });
    }

    protected void allocateRealmServer(Callable2Args<S, Boolean> successCallback) {
        warningTextCell.setActor(progressBar);
        setJoinState(RealmJoinState.LOOKING_FOR_SERVER);
        loginManager.findRealmServer(loginInfo, successCallback, (error) -> {
            loadingIndicator.deactivate();
            log.error("Unable to allocate realm server: {}", error);

            switch (error) {
                case GCLoginManager.CODE_BAD_SERVER_RESPONSE:
                    displayWarning("Unable to find game: invalid response from the server.");
                    break;
                case GCLoginManager.CODE_COMMUNICATION:
                    displayWarning("Unable to find game: connection to control server failed.");
                    break;
                case "no_instance_created":
                    displayWarning("Unable to find game: simulation servers are down.");
                    break;
                case "bad_connection_url":
                    displayWarning("Unable to find game: malformed game instance encountered.");
                    break;
                default:
                    displayWarning("Unable to find game: unknown reason.");
                    break;
            }
        });
    }

    protected void pingBattleRoyale() {
        isWaitingToRetry = false;
        switch (attempts) {
            case 0:
                setJoinState(RealmJoinState.CONNECTING_1);
                break;
            case 1:
                setJoinState(RealmJoinState.CONNECTING_2);
                break;
            case 2:
                setJoinState(RealmJoinState.CONNECTING_3);
                break;
            case 3:
                setJoinState(RealmJoinState.CONNECTING_4);
                break;
            default:
                log.warn("Unknown connection state: " + attempts);
                setJoinState(RealmJoinState.CONNECTING_OTHER);
                break;
        }

        loginManager.pingServer(currentConnectingServer, this::onConnectionSuccess, (e) -> {
            attempts++;
            if (attempts - 1 < ATTEMPT_DELAYS.length) {
                isWaitingToRetry = true;
                progressBar.setValue(progressBar.getValue());
                scheduleAttemptIn(ATTEMPT_DELAYS[attempts - 1]);
            } else onConnectionGaveUp(e);
        });
    }

    public void scheduleAttemptIn(long time) {
        currentTask = Timer.instance().scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                currentTask = null;
                pingBattleRoyale();
            }
        }, time / 1000F);
    }

    public void onConnectionSuccess() {
        setJoinState(RealmJoinState.CHECKING_SERVER_VERSION);
        loginManager.checkServerCompatibility(loginInfo, currentConnectingServer, () -> {
            setJoinState(RealmJoinState.JOINING);
            loginManager.joinWithRealmCharacter(
                        loginInfo,
                        currentConnectingServer,
                        characterName,
                selectedSpawnOption.spawnType, selectedSpawnOption.spawnParams,
                this::onSuccessfulLogin,
                this::onLoginError);
        });
    }

    private void setJoinState(RealmJoinState state) {
        progressBar.setValue(state.getStateId());
    }

    @Override
    protected void next() {
        if (validateSpawn()) {
            this.characterName = fieldCharacterName.getText();
            startConnection();
        }
    }

    public void onSuccessfulLogin() {
        setJoinState(RealmJoinState.JOINED);
        loadingIndicator.deactivate();
        characterWarningText.setText("");
        characterWarningText.setVisible(false);
        loadingIndicator.deactivate();
        //characterNameWindow.close();
        characterNameWindow.setVisible(false);
        spawnSelectDialog.close();
        btnBack.setVisible(false);
        characterImage.setVisible(false);
    }

    @Override
    protected void openSpawnTypeSelector() {
        // We don't need spawn type selector for Realms
    }

    public void onLoginError(String error) {
        loadingIndicator.deactivate();
        super.processError(error);
    }

    @Override
    protected void displayWarning(String text) {
        warningTextCell.setActor(characterWarningText);
        super.displayWarning(text);
    }

    public void onConnectionGaveUp(String error) {
        loadingIndicator.deactivate();
        displayWarning("Unable to test the game server: " + error);
        log.error("Unable to join realm server: "+error);
    }

    public void stopTasks() {
        loadingIndicator.deactivate();
        if (currentTask != null) {
            currentTask.cancel();
            currentTask = null;
        }
    }

    @Override
    public void close() {
        characterWarningText.setText("");
        characterWarningText.setVisible(false);
        loadingIndicator.deactivate();
        //characterNameWindow.close();
        characterNameWindow.setVisible(false);
        spawnSelectDialog.close();
        btnBack.setVisible(false);
        characterImage.setVisible(false);

        loginManager.openServerSelectScreen(loginInfo);
    }

    @Override
    public void open(L loginInfo, S serverInfo, NewCharPreselectionInfo preselectionInfo) {
        warningTextCell.setActor(characterWarningText);
        super.open(loginInfo, serverInfo, preselectionInfo);
    }

    private enum RealmJoinState {
        LOOKING_FOR_SERVER(0, "Looking for server"),
        WAITING_SERVER_START(1, "Starting the server"),
        CONNECTING_1(2, "Connecting to server (1/4)"),
        CONNECTING_2(3, "Connecting to server (2/4)"),
        CONNECTING_3(4, "Connecting to server (3/4)"),
        CONNECTING_4(5, "Connecting to server (4/4)"),
        CONNECTING_OTHER(6, "Connecting to server"),
        CHECKING_SERVER_VERSION(7, "Checking server version"),
        JOINING(8, "Joining"),
        JOINED(9, "Joined");

        private final int stateId;
        private final String stateName;

        RealmJoinState(int stateId, String stateName) {
            this.stateId = stateId;
            this.stateName = stateName;
        }

        public int getStateId() {
            return stateId;
        }

        public String getStateName() {
            return stateName;
        }

        public static RealmJoinState getById(int id) {
            for (RealmJoinState value : RealmJoinState.values()) {
                if (value.stateId == id)
                    return value;
            }
            return null;
        }

        public static int getMaxStateId() {
            RealmJoinState[] values = RealmJoinState.values();
            return values[values.length - 1].getStateId();
        }
    }
}

