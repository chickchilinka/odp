package com.universeprojects.gamecomponents.client.selection;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

abstract class GCSelectionMenuClickable extends H5EContainer {

    private final Logger log = Logger.getLogger(GCSelectionMenuClickable.class);

    private boolean pressed = false;
    private boolean selected = false;

    public GCSelectionMenuClickable(H5ELayer layer) {
        super(layer);
    }

    /**
     * @param body      This element subscribes to pointer events
     */
    <T extends Actor & GraphicElement> void setupClickable(T body) {
        log.debug("Setting up clickable");

        this.pressed = false;
        this.selected = false;

        body.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                log.debug("pointer:press");
                pressed = true;
                updateVisualState();
                event.handle();
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                log.debug("pointer:unpress");

                super.touchUp(event, x, y, pointer, button);
                pressed = false;
                updateVisualState();
                event.handle();
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onClick();
                event.handle();
            }

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                log.debug("pointer:enter");

                onPointerEnter();
                super.enter(event, x, y, pointer, fromActor);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                log.debug("pointer:exit");

                onPointerExit();
                super.exit(event, x, y, pointer, toActor);
            }
        });
    }

    protected void updateVisualState() {
        if (pressed) {
            setScale(0.95f);
        } else if (selected) {
            setScale(1.1f);
        } else { // neutral state
            setScale(1);
        }
    }

    void select() {
        pressed = false;
        selected = true;
        updateVisualState();
    }

    void unselect() {
        pressed = false;
        selected = false;
        updateVisualState();
    }


    /**
     * Override this
     */
    void onClick() {

    }

    /**
     * Override this
     */
    void onPointerEnter() {

    }

    /**
     * Override this
     */
    void onPointerExit() {

    }

}
