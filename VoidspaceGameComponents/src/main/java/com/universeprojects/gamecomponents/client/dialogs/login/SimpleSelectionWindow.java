package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.elements.GCLoginFriendItem;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public abstract class SimpleSelectionWindow extends GCSimpleWindow {

    protected Color warningColor = Color.RED;
    final float warningTextFontSize = 0.75f;

    protected H5EScrollablePane mainPane;
    protected H5ELabel messageLabel;
    protected Table btnTable;
    protected GCList<GCListItem> itemList;
    protected GCListItemActionHandler handler;

    public SimpleSelectionWindow(H5ELayer layer, String windowId, String title){
        this(layer, windowId, title, null, null);
    }

    public SimpleSelectionWindow(H5ELayer layer, String windowId, String title, Integer width, Integer height){
        super(layer, windowId, title, width, height);
        messageLabel = new H5ELabel("",layer);
        messageLabel.setFontScale(warningTextFontSize);
        messageLabel.setColor(warningColor);
        add(messageLabel);
        row();
        mainPane = add(new H5EScrollablePane(layer, "scrollpane-backing-4")).grow().top().left().getActor();
        itemList = new GCList<>(layer, 1, 5, 5, true);
        row();
        btnTable = new Table();
        add(btnTable).center().growX();
    }

    protected abstract void loadItemList();
}
