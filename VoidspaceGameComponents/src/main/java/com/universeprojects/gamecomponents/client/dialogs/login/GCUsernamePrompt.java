package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.auth.AuthCredentials;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCUsernamePrompt<L extends LoginInfo> extends GCSimpleWindow {
    private final Logger log = Logger.getLogger(GCUsernamePrompt.class);
    private final GCLoginManager<L, ?, ?> loginManager;
    private final H5EInputBox usernameText;
    private final H5ELabel warningLabel;
    private final GCLoadingIndicator loadingIndicator;
    private AuthCredentials credentials;

    public GCUsernamePrompt(GCLoginManager<L, ?, ?> loginManager, H5ELayer layer) {
        super(layer, "oauth-username-prompt", "Pick your username", 450, 150, false, "login-window");
        this.loginManager = loginManager;
        this.loadingIndicator = new GCLoadingIndicator(layer);

        positionProportionally(0.5f, 0.5f);
        setPackOnOpen(false);
        setMovable(false);
        defaults().center();

        setCloseButtonEnabled(false);
        setTitle("Pick your username");


        usernameText = new H5EInputBox(layer);
        warningLabel = new H5ELabel(layer);
        warningLabel.setVisible(false);
        warningLabel.setColor(Color.RED);
        warningLabel.setFontScale(0.75f);
        warningLabel.setAlignment(Align.center);

        row().padTop(10F);
        add(new H5ELabel("Username:", layer));

        row().pad(10F);
        add(usernameText).width(350L);

        row();
        add(warningLabel);

        row();
        add(new H5EButton("Create Account", layer)).getActor().addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                processLogin();
            }
        });
    }

    public void open(AuthCredentials credentials) {
        this.credentials = credentials;
        open();
    }

    private void processLogin() {
        String username = usernameText.getText();
        if (Strings.isEmpty(username)) {
            warningLabel.setText("Please provide the username");
            warningLabel.setVisible(true);
        }
        warningLabel.setVisible(false);
        if(username != null) {
            credentials.getParams().put("username", username);
        }

        loadingIndicator.activate(this);
        loginManager.login(credentials, (loginInfo) -> {
            log.info("OAuth registration successful");
            loadingIndicator.deactivate();
            close();
            loginManager.setSavedToken(loginInfo);
            loginManager.onSuccessfulLogin(loginInfo);
        }, error -> {
            log.error("OAuth registration error:", error);
            loginManager.setSavedToken(null);
            loadingIndicator.deactivate();
            if (GCLoginManager.CODE_COMMUNICATION.equals(error.getMessage())) {
                warningLabel.setText("Error communicating with the server. Please try again later.");
                warningLabel.setVisible(false);
            } else {
                warningLabel.setText("Unable to process your authentication request.");
                warningLabel.setVisible(true);
            }
        });

    }
}
