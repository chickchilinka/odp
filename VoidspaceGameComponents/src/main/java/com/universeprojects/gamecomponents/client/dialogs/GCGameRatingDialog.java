package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ERate;
import com.universeprojects.vsdata.shared.GameRatingData;
import com.universeprojects.vsdata.shared.ProblemData;
import com.universeprojects.vsdata.shared.RatingClientInfo;

public class GCGameRatingDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 450;
    private final static int WINDOW_H = 525;
    private static final String PROBLEM_MESSAGE =
        "We are very sorry to hear that it's not going that well. If you have a moment we would really love to try to fix some of the issues you've been having." +
        "\n\nIf you can, please tell us what's going on below.";
    private static final String PROBLEM_CLOSE_TEXT = "No thanks";
    private final H5ERate rate;
    private final H5ELabel text;
    private final H5EButton neverAskAganCheckbox;
    private final H5EButton submitBtn;
    private final H5ELayer layer;
    private final Callable1Args<GameRatingData> onSubmit;
    private final Callable0Args onSureClick;
    private final Callable1Args<ProblemData> onProblemSubmit;
    private final RatingClientInfo ratingClientInfo;
    private GameRatingData outData;

    public GCGameRatingDialog(H5ELayer layer, RatingClientInfo ratingClientInfo, Callable1Args<GameRatingData> onSubmit, Callable0Args onSureClick, Callable1Args<ProblemData> onProblemSubmit) {
        super(layer, "gameReviewDialog", "How is it going?", WINDOW_W, WINDOW_H, false);
        this.layer = layer;
        this.onSureClick = onSureClick;
        this.onSubmit = onSubmit;
        this.onProblemSubmit = onProblemSubmit;
        this.ratingClientInfo = ratingClientInfo;
        setModal(true);
        positionProportionally(0.5f, 0.5f);
        setFullscreenOnMobile(true);
        text = new H5ELabel("How are we doing so far? The game is in early access but we need to know when things are going right, and then are going wrong." +
                "\n\nLet us know how things are going with a rating below!", layer);
        LabelStyle labelStyle = new LabelStyle(text.getStyle());
        labelStyle.background = StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque;
        text.setWrap(true);
        text.setStyle(labelStyle);
        add(text).left().top().growX().pad(5);
        row();

        rate = new H5ERate(layer, false, "How you rate the game so far:");
        rate.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                checkEnableSubmitButton();
                event.handle();
            }
        });
        add(rate).padLeft(10).left();
        row();
        neverAskAganCheckbox = ButtonBuilder.inLayer(layer).withStyle("default-checkable").build();
        neverAskAganCheckbox.setRawText(" Never show me this again");
        neverAskAganCheckbox.setChecked(false);
        neverAskAganCheckbox.addButtonListener(this::checkEnableSubmitButton);
        add(neverAskAganCheckbox).padTop(25).padBottom(20).padLeft(15);
        row();
        submitBtn = new H5EButton("Submit", layer);
        submitBtn.setDisabled(true);
        H5EButton cancel = new H5EButton("Close", layer);
        cancel.addButtonListener(this::close);
        submitBtn.addButtonListener(this::collectForm);
        row();
        add().grow();
        row();
        Table buttonRow = new Table();
        buttonRow.pad(15,10,5,10);
        buttonRow.add(submitBtn).width(120).fillX().left();
        buttonRow.add().growX();
        buttonRow.add(cancel).width(120).fillX().right();
        add(buttonRow).right().growX();
    }

    private void checkEnableSubmitButton() {
        submitBtn.setDisabled(!canSubmit());
    }

    private boolean canSubmit() {
        return rate.getValue() > 0 || neverAskAganCheckbox.isChecked();
    }

    public void collectForm() {
        if(!canSubmit()) {
            return;
        }
        final boolean neverAskAgain = neverAskAganCheckbox.isChecked();
        if(rate.getValue() == 0 && neverAskAgain) {
            outData = new GameRatingData(null, true);
            submit();
            return;
        }
        outData = new GameRatingData((int) rate.getValue(), neverAskAgain);
        if (rate.getValue() > 3) {
            if(ratingClientInfo.canRate) {
                GCThatsGreatDialog thatsGreatDialog = new GCThatsGreatDialog(layer, ratingClientInfo.displayStoreName,
                    () -> {
                        onSureClick.call();
                        submit();
                    }, this::submit);
                thatsGreatDialog.open(false);
            } else {
                submit();
            }
        } else {
            GCWhatIsWrongDialog whatIsWrongDialog = new GCWhatIsWrongDialog(layer, PROBLEM_MESSAGE, PROBLEM_CLOSE_TEXT, (problemData) -> {
                onProblemSubmit.call(problemData);
                submit();
            }, this::submit);
            whatIsWrongDialog.open();
        }
    }

    public void submit() {
        onSubmit.call(outData);
        close();
    }
}