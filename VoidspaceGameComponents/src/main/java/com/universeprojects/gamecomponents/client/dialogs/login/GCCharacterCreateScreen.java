package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.json.shared.JSONObject;


public class GCCharacterCreateScreen<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> extends GCCharacterSpawnDesktopBase<L, S, C> {
    public GCCharacterCreateScreen(H5ELayer layer, GCLoginManager<L, S, C> loginManager) {
        super(layer, loginManager);

        spawnSelectDialog.populateData(loginManager.getDefaultSpawnConfigs());
    }

    @Override
    protected void spawn(String spawnType, JSONObject spawnParams) {
        final String characterName = fieldCharacterName.getText();
        loginManager.joinWithNewCharacter(loginInfo, serverInfo, characterName, spawnType, spawnParams, this::close, this::processError);
    }

    @Override
    protected boolean validateSpawn() {
        String characterName = fieldCharacterName.getText();
        if (characterName.isEmpty()) {
            displayWarning("Please enter your character name.");
            fieldCharacterName.focus();
            return false;
        }
        return true;
    }

    public void open(L loginInfo, S serverInfo, NewCharPreselectionInfo preselectionInfo) {
        super.open(loginInfo, serverInfo);
        fieldCharacterName.setText("");
        fieldCharacterName.focus();

        if(preselectionInfo != null) {
            spawnSelectDialog.preselectSpawnType(preselectionInfo.spawnType);
        }
    }

    @Override
    protected void back() {
        close();
    }

    @Override
    public void close() {
        super.close();
        loginManager.openCharacterSelectScreen(loginInfo, serverInfo);
    }
}

