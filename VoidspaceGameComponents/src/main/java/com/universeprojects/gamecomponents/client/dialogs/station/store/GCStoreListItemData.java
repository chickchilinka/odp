package com.universeprojects.gamecomponents.client.dialogs.station.store;

import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.vsdata.shared.CurrencyData;
import com.universeprojects.vsdata.shared.CategorizedStoreItemData;

public class GCStoreListItemData {
    private CategorizedStoreItemData data;
    private GCInventoryItem item;

    public GCStoreListItemData(CategorizedStoreItemData data, GCInventoryItem item) {
        this.data = data;
        this.item = item;
    }

    public GCStoreListItemData(String offerId, String currencyId, String currencyIcon, int price, String sellerNickname, String categoryId, GCInventoryItem item) {
        this.item = item;
        CurrencyData currency = new CurrencyData(currencyId, currencyIcon, price);
        this.data = new CategorizedStoreItemData(offerId, item.getUid(), item.getName(), currency, sellerNickname, categoryId);
    }

    public CategorizedStoreItemData getData() {
        return data;
    }

    public GCInventoryItem getItem() {
        return item;
    }

    public void setData(CategorizedStoreItemData data) {
        this.data = data;
    }

    public void setItem(GCInventoryItem item) {
        this.item = item;
    }
}