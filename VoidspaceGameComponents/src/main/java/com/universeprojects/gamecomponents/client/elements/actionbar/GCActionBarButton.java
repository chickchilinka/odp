package com.universeprojects.gamecomponents.client.elements.actionbar;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.ActivateDeactivateWrapper;
import com.universeprojects.common.shared.util.ReactiveValue;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndSource;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndTarget;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.ActionIconData;

import static com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBar.MISSING_ACTION_ICON;

class GCActionBarButton extends Table {
    protected final H5ELayer layer;
    private final Logger log = Logger.getLogger(GCActionBarButton.class);
    protected H5ELabel buttonLabel;
    protected H5EIcon multiplicityIcon;
    protected GCActionBarDndTarget dndTarget;
    protected GCActionBarDndSource dndSource;
    protected Cell iconCell;
    private ReactiveActorValueCompound<ActionIconData> icon;
    private ActivateDeactivateWrapper<H5EIcon> primaryIcon;
    private ActivateDeactivateWrapper<H5EIcon> secondaryIcon;

    public GCActionBarButton(GCActionBarType type, H5ELayer layer, String keyBinding, GCActionBar actionBar, GCActionBarController controller, int index) {
        super();

        this.layer = layer;

        multiplicityIcon = H5EIcon.fromSpriteType(layer, "images/GUI/ui2/action-multiple-targets.png", Scaling.fit, Align.center);
        multiplicityIcon.setVisible(false);
        addActor(multiplicityIcon);

        buttonLabel = new H5ELabel(keyBinding, layer);
        buttonLabel.setPosition(6, 8);
        buttonLabel.setStyle(layer.getEngine().getSkin().get("label-small", Label.LabelStyle.class));
        addActor(buttonLabel);

        setTouchable(Touchable.enabled);

        addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (event.getTarget() == primaryIcon.get()
                        || event.getTarget() == secondaryIcon.get()) {
                    try {
                        controller.callAction(type, actionBar.getCurrentRow(), index);
                    } catch (Throwable ex) {
                        log.error("Unable to call action", ex);
                    }
                }
            }
        });

        iconCell = add();
        icon = new ReactiveActorValueCompound<>(this);
        icon.setCallback(iconData -> {
            if (icon.hasReactiveValue()) {
                applyIcon(iconData, false);
            } else {
                // No actions are currently assigned to this button
                applyIcon(null, true);
            }
        });

        primaryIcon = new ActivateDeactivateWrapper<>(null, icon -> {
            removeActor(icon);
            iconCell.setActor(null);
        }, actor -> iconCell.setActor(actor).grow());

        secondaryIcon = new ActivateDeactivateWrapper<>(null, this::removeActor, actor -> {
            addActor(actor);
            actor.setPosition(getWidth() - actor.getWidth(), 0);
        });
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();

        multiplicityIcon.setSize(getWidth() * 0.39F, getHeight() * 0.34F);
        multiplicityIcon.setPosition(getWidth() - this.multiplicityIcon.getWidth(), getHeight() - this.multiplicityIcon.getHeight() - 2);
        if (secondaryIcon.get() != null) {
            H5EIcon icon = secondaryIcon.get();
            icon.setSize(getWidth() * 0.52F, getHeight() * 0.5F);
            icon.setPosition(getWidth() - icon.getWidth(), 0);
        }
        H5ELayer.invalidateBufferForActor(this);
    }

    private void applyIcon(ActionIconData iconData, boolean hide) {
        if (!hide) {
            Color color = iconData != null ? iconData.color : Color.WHITE;
            String primaryImage;
            String secondaryImage;

            if (iconData == null || (iconData.actionIcon == null && iconData.itemIcon == null)) {
                primaryImage = MISSING_ACTION_ICON;
                secondaryImage = null;
            } else if (iconData.actionIcon != null && iconData.itemIcon != null) {
                primaryImage = iconData.itemIcon;
                secondaryImage = iconData.actionIcon;
            } else if (iconData.actionIcon != null) {
                primaryImage = iconData.actionIcon;
                secondaryImage = null;
            } else {
                primaryImage = iconData.itemIcon;
                secondaryImage = null;
            }

            changePrimaryImage(primaryImage, color);
            changeSecondaryImage(secondaryImage, color);
            multiplicityIcon.setVisible(iconData != null && iconData.multipleTargets);
        } else {
            changePrimaryImage(null, null);
            changeSecondaryImage(null, null);
            multiplicityIcon.setVisible(false);
        }
        H5ELayer.invalidateBufferForActor(this);
    }

    public void setReactiveIcon(ReactiveValue<ActionIconData> newIcon) {
        icon.setValue(newIcon);
    }

    public void changePrimaryImage(String image, Color color) {
        if (image == null) {
            primaryIcon.set(null);
        } else {
            H5EIcon icon = H5EIcon.fromSpriteType(layer, image, Scaling.fit, Align.center);
            if (color != null) {
                icon.setColor(color);
            }
            primaryIcon.set(icon);
        }
    }

    public void changeSecondaryImage(String image, Color color) {
        if (image == null) {
            secondaryIcon.set(null);
        } else {
            H5EIcon icon = H5EIcon.fromSpriteType(layer, image, Scaling.fit, Align.center);
            if (color != null) {
                icon.setColor(color);
            }
            icon.setWidth(getWidth() * 0.52F);
            icon.setHeight(getHeight() * 0.5F);
            secondaryIcon.set(icon);
        }
    }

    public void changeLabel(String keyBinding) {
        removeActor(buttonLabel);
        buttonLabel = new H5ELabel(keyBinding, layer);
        addActor(buttonLabel);
        buttonLabel.setPosition(6, 8);
        buttonLabel.setStyle(layer.getEngine().getSkin().get("label-small", Label.LabelStyle.class));
    }

    /* Changes the position of the label */
    public void setLabelPosition(int x, int y) {
        if (buttonLabel != null) {
            buttonLabel.setPosition(x, y);
        }
    }
}

