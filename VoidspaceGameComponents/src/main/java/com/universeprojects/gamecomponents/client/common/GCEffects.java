package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.VisibleAction;
import com.badlogic.gdx.utils.Pools;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

/**
 * This class contains various methods used for visual effects on various UI components
 */
public class GCEffects {

    public static final float FADE_TIME = 0.2f;

    public static <T extends Actor & GraphicElement> void addFadeAction(T element, float targetAlpha, boolean visibleTarget, float time) {
        addAlphaAction(element, targetAlpha, time);
        addVisibleAction(element, visibleTarget, time);
    }

    public static <T extends Actor & GraphicElement> void addVisibleAction(T element, boolean visibleTarget, float time) {
        final DelayAction delayAction = Pools.obtain(DelayAction.class);
        final VisibleAction visibleAction = Pools.obtain(VisibleAction.class);
        visibleAction.setVisible(visibleTarget);
        delayAction.setDuration(time);
        delayAction.setAction(visibleAction);
        element.addAction(delayAction);
    }

    public static <T extends Actor & GraphicElement> void addAlphaAction(T element, float targetAlpha, float time) {
        final AlphaAction alphaAction = new InvalidatingAlphaAction();
        alphaAction.setAlpha(targetAlpha);
        alphaAction.setDuration(time);
        element.addAction(alphaAction);
    }

    public static <T extends Actor & GraphicElement> void show(T element, float fadeTime, float targetAlpha) {
        if (element.isVisible() && element.getColor().a > (0.9f * targetAlpha)) {
            element.getColor().a = targetAlpha;
            return; // already visible
        }
        element.setVisible(true);
        addAlphaAction(element, targetAlpha, fadeTime);
    }

    /**
     * If the element is hidden, sets it to fade-in
     */
    public static <T extends Actor & GraphicElement> void show(T element) {
        show(element, FADE_TIME, 1f);
    }

    public static <T extends Actor & GraphicElement> void show(T element, float targetAlpha) {
        show(element, FADE_TIME, targetAlpha);
    }

    public static <T extends Actor & GraphicElement> void hide(T element, float fadeTime) {
        if (!element.isVisible() || element.getColor().a < 0.1f) {
            element.setVisible(false);
            element.getColor().a = 0;
            return; // already hidden
        }
        addFadeAction(element, 0.0f, false, fadeTime);
    }

    /**
     * If the element is visible, sets it to fade-out
     */
    public static <T extends Actor & GraphicElement> void hide(T element) {
        hide(element, FADE_TIME);
    }

    /**
     * Hides an element instantly, without the fade-out effect
     */
    public static <T extends Actor & GraphicElement> void hideInstantly(T element) {
        element.clearActions();
        element.getColor().a = 1;
        element.setVisible(false);
    }

    /**
     * Shows an element instantly, without the fade-out effect
     */
    public static <T extends Actor & GraphicElement> void showInstantly(T element) {
        element.clearActions();
        element.getColor().a = 1;
        element.setVisible(true);
    }

    /**
     * @return TRUE if the given element is visible, FALSE if it is hidden
     */
    public static <T extends Actor & GraphicElement> boolean isVisible(T element) {
        return element.isVisible() && (element.getColor() == null || element.getColor().a > 0);
    }

}
