package com.universeprojects.gamecomponents.client.dialogs.inventory;

import java.util.Map;

public interface GCInternalTank {

    String getFluidType();

    String getForcedType();

    boolean isFull();

    float getCurrentVolume();

    float getMaxVolume();

    void transferFrom(GCInternalTank tank, float amount);

    void transferFrom(Map<GCInternalTank, Float> sourceTanks, String fluidType);

    void openTransferDialogTo(GCInternalTank targetTank);
}
