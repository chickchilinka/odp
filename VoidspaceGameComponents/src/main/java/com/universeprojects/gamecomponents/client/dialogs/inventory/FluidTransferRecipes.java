package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.universeprojects.common.shared.util.Log;

import java.util.*;

public class FluidTransferRecipes {
    public static final FluidTransferRecipes INSTANCE = new FluidTransferRecipes();
    private List<FluidTransferRecipe> recipes = new ArrayList<>();

    static {
        INSTANCE.addRecipe(new HashMap<String, Float>() {
            {
                put("Hydrogen", 1f);
                put("Oxygen", 1f);
            }
        }, "Hydrolox");

        INSTANCE.addRecipe(new HashMap<String, Float>() {
            {
                put("Hydrogen", 1f);
                put("Oxygen", 1f);
                put("Hydrolox", 1f);
            }
        }, "Hydrolox");
    }

    private FluidTransferRecipes() {

    }

    public void addRecipe(Map<String, Float> proportionsBySourceIngredients, String result) {
        if(proportionsBySourceIngredients == null || result == null || proportionsBySourceIngredients.isEmpty()) {
            Log.error("Cannot add a null Fluid recipe.");
            return;
        }
        else {
            for(Map.Entry<String, Float> ingredient : proportionsBySourceIngredients.entrySet()) {
                if(ingredient.getKey() == null || ingredient.getValue() == null) {
                    Log.error("Cannot add a null Fluid recipe.");
                    return;
                }
            }

            String currentResult = getResultFor(proportionsBySourceIngredients.keySet());

            if(currentResult != null) {
                Log.error("Cannot add a Fluid recipe that already exists.");
            }
        }

        float largestProportion = Float.NEGATIVE_INFINITY;
        for(Float proportion : proportionsBySourceIngredients.values()) {
            if(proportion > largestProportion) {
                largestProportion = proportion;
            }
        }

        Set<String> keySet = proportionsBySourceIngredients.keySet();
        float totalProportion = 0f;
        for(String ingredient : keySet) {
            float finalProportion = proportionsBySourceIngredients.get(ingredient) / largestProportion;
            proportionsBySourceIngredients.put(ingredient, finalProportion);
            totalProportion += finalProportion;
        }

        for(String ingredient : keySet) {
            proportionsBySourceIngredients.put(ingredient, proportionsBySourceIngredients.get(ingredient) / totalProportion);
        }

        FluidTransferRecipe recipe = new FluidTransferRecipe();
        recipe.ingredients = proportionsBySourceIngredients;
        recipe.result = result;

        recipes.add(recipe);
    }

    public String getResultFor(Collection<String> sourceIngredients) {
        for (FluidTransferRecipe recipe : recipes) {
            boolean matches = true;
            for (String ingredient : sourceIngredients) {
                if (!recipe.contains(ingredient)) {
                    matches = false;
                    break;
                }
            }

            if (matches) {
                return recipe.result;
            }
        }

        return null;
    }

    public Map<String, Float> getIngredientProportionsForRecipe(Collection<String> sourceIngredients, String expectedResult) {
        for(FluidTransferRecipe recipe : recipes) {
            boolean matches = true;
            if(sourceIngredients.size() != recipe.ingredients.size()) {
                matches = false;
            }
            else {
                for (String ingredient : sourceIngredients) {
                    if (!recipe.contains(ingredient)) {
                        matches = false;
                        break;
                    }
                }
                if(matches && recipe.result.equals(expectedResult)) {
                    return recipe.ingredients;
                }
            }
        }

        return null;
    }

    private static class FluidTransferRecipe {
        Map<String, Float> ingredients;
        String result;

        public boolean contains(String ingredient) {
            for(String sourceIngredient : ingredients.keySet()) {
                if(ingredient.equals(sourceIngredient)) {
                    return true;
                }
            }

            return false;
        }
    }
}
