package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates the UI for players to login.
 */
public class GCServerSelectScreen<L extends LoginInfo, S extends ServerInfo> {
    final H5ELayer layer;
    final GCWindow window;
    private final GCList<GCServerSelectorItem> serverListing;
    private final GCListItemActionHandler<GCServerSelectorItem> actionHandler;
    private final GCLoadingIndicator loadingIndicator;
    private final GCLoginManager<L, S, ?> loginManager;
    private final H5EButton btnConnect;
    private final H5ELabel warningText;
    protected Logger log = Logger.getLogger(GCServerSelectScreen.class);
    private L loginInfo;

    public GCServerSelectScreen(H5ELayer layer, GCLoginManager<L, S, ?> loginManager) {
        this.layer = Dev.checkNotNull(layer);
        window = new GCWindow(layer, 500, 600, "login-window");
        this.loginManager = loginManager;
        loadingIndicator = new GCLoadingIndicator(layer);
        //window.debug();
        window.setId("server-select");
        window.setTitle("Server Select");
        window.defaults().left().top();
        window.setCloseButtonEnabled(false);
        window.setMovable(false);
        window.padTop(40);
        window.getTitleTable().padRight(20);

        warningText = new H5ELabel(layer);
        warningText.setFontScale(0.9F);
        warningText.setColor(Color.RED);
        warningText.setAlignment(Align.center);
        warningText.setVisible(false);

        final Table labelTable = window.add(new Table()).left().top().height(75).fillX().getActor();
        H5ELabel serverName = new H5ELabel("Server Name", layer);

//        H5ELabel population = new H5ELabel("Population", layer);

        labelTable.add(serverName).left().expand().padLeft(20);
//        labelTable.add(population).right().expandX().padRight(20);
        window.row();

        final H5EScrollablePane serverScrollPane = new H5EScrollablePane(window.getLayer(), "scrollpane-backing-1");

        serverScrollPane.setScrollingDisabled(true, false);
        window.add(serverScrollPane).width(450).expandY().fillY();
        serverListing = new GCList<>(layer, 1, 5, 5, true);
        serverScrollPane.add(serverListing).width(425).expandY().fillY();

        final H5EButton btnBack = new H5EButton("Logout", layer);
        btnConnect = new H5EButton("Connect", layer);
        btnConnect.addButtonListener(this::connect);
        btnConnect.setDisabled(true);
        final H5EButton btnRefresh = new H5EButton("Refresh", layer);
        btnRefresh.addButtonListener(this::refreshServerList);
        btnBack.addButtonListener(this::handleLogout);
        Table buttonTable = new Table();
        window.row();
        window.add(warningText).center();
        window.row();
        window.add(buttonTable).colspan(2).center();

        buttonTable.add(btnBack).width(150);
        buttonTable.add(btnRefresh).width(150);
        buttonTable.add(btnConnect).width(150);

        this.actionHandler = new GCListItemActionHandler.SimpleHandler<>(this::handleSelection, this::onChoiceMade);
//        window.pack();
        window.positionProportionally(0.5f, 0.5f);
    }

    public synchronized void refreshServerList() {
        serverListing.clear();
        loadingIndicator.activate(window);
        loginManager.loadServers(loginInfo, (servers) -> {
            loadingIndicator.deactivate();
            if (servers.isEmpty()) {
                warningText.setText("No servers supporting your game version were found.\nPlease update your Voidspace application.");
                warningText.setVisible(true);
            } else {
                warningText.setVisible(false);
            }
            synchronized (GCServerSelectScreen.this) {
                serverListing.clear();
                for (S server : servers) {
                    addServer(server);
                }
                addAdditionalModes();
                resizeListItems();
            }
        }, (error) -> {
            loadingIndicator.deactivate();
            log.error("Unable to refresh server list: {}", error);
            warningText.setText("Unable to refresh server list.\nPlease try again later.");
            warningText.setVisible(true);
            serverListing.clear();
            addAdditionalModes();
            resizeListItems();
        });
    }

    private void resizeListItems() {
        if (!layer.getEngine().isMobileMode()) {
            return;
        }

        int count = serverListing.size();
        if (count == 0) {
            return;
        }

        float minPxPerItem = serverListing.get(0).getPrefHeight();
        float prefPxPerItem = (serverListing.getHeight() - count * serverListing.getRowSpacing()) / count;
        float maxPxPerItem = serverListing.getHeight() / 5;

        float height;
        if (maxPxPerItem < minPxPerItem) {
            height = minPxPerItem;
        } else if (prefPxPerItem < minPxPerItem) {
            height = minPxPerItem;
        } else if (prefPxPerItem > maxPxPerItem) {
            height = maxPxPerItem;
        } else {
            height = prefPxPerItem;
        }


        for (Cell cell : serverListing.getCells()) {
            cell.height(height);
        }
    }

    protected void onChoiceMade(GCServerSelectorItem selection) {
        if (selection instanceof GCServer) {
            //noinspection unchecked
            loginManager.onServerChosen(((GCServer<S>) selection).getServerInfo(), this::handleConnect);
        } else if (selection instanceof GCAdditionalGameMode) {
            loadingIndicator.deactivate();
            close();
            GCAdditionalGameMode.AdditionalMode mode = ((GCAdditionalGameMode) selection).getMode();
            switch (mode) {
                case BATTLE_ROYALE:

                    loginManager.openRealmCharacterSelectScreen(loginInfo);
                    break;
                case SINGLE_PLAYER:
                    loginManager.startSingleplayer();
                    break;
                default:
                    throw new IllegalArgumentException("Unable to start additional game mode: " + selection);
            }
        }
    }

    public synchronized void addServer(S serverInfo) {
        GCServer<S> server = new GCServer<>(layer, actionHandler, serverInfo);
        serverListing.addItem(server);
    }

    public synchronized void addAdditionalModes() {
//        serverListing.addItem(new GCAdditionalGameMode<>(layer, actionHandler, GCAdditionalGameMode.AdditionalMode.BATTLE_ROYALE));
        serverListing.addItem(new GCAdditionalGameMode<>(layer, actionHandler, GCAdditionalGameMode.AdditionalMode.SINGLE_PLAYER));
    }

    private void connect() {
        final GCServerSelectorItem server = serverListing.getButtonGroup().getChecked();
        onChoiceMade(server);
    }

    @SuppressWarnings("unused")
    private void handleSelection(GCServerSelectorItem lastSelectedItem) {
        boolean anySelected = serverListing.getButtonGroup().getChecked() != null;
        btnConnect.setDisabled(!anySelected);
    }

    protected void handleConnect(S serverInfo) {
        loadingIndicator.activate(window);
        loginManager.pingServer(serverInfo, () -> {
            loadingIndicator.deactivate();
            close();
            warningText.setVisible(false);
            loginManager.onServerSelected(loginInfo, serverInfo);
        }, (error) -> {
            loadingIndicator.deactivate();
            warningText.setText("Unable to connect to this server.\nPlease try again later.");
            warningText.setVisible(true);
            log.error("Unable to join to the server: "+error);
            //TODO
        });
    }

    protected void handleLogout() {
        loginManager.setSavedToken(null);
        close();
        loginManager.openLoginScreen();
    }

    public void open(L loginInfo) {
        this.loginInfo = loginInfo;
        refreshServerList();
        window.open();
        window.positionProportionally(0.5f, 0.5f);
    }

    public void close() {
        window.close();
    }

    public void destroy() {
        window.destroy();
    }

    public boolean isOpen() {
        return window.isOpen();
    }
}





