package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCSlider;
import com.universeprojects.gamecomponents.client.dialogs.inventory.*;
import com.universeprojects.gamecomponents.client.dialogs.station.store.GCStoreListItemData;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ERate;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.vsdata.shared.CategorizedStoreItemData;
import com.universeprojects.vsdata.shared.CurrencyData;

public class GCItemCountDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 300;
    private final static int WINDOW_H = 300;
    private final static int ICON_SIZE = 64;

    private GCStoreListItemData entry;
    private CurrencyData playerCurrency;
    private GCSlider slider;
    private H5ELayer layer;
    private H5ELabel priceLabel;
    private H5EIcon currencyIcon;
    private H5ELabel sellerLabel;
    private GCItemIcon<GCInventoryItem> itemIcon;
    private H5ELabel itemNameLabel;
    private Table sliderRow;
    private H5EInputBox sliderBox;
    private H5EButton buyBtn;
    private H5EIcon btnIcon;
    private H5ELabel buttonText;

    public GCItemCountDialog(H5ELayer layer, GCStoreListItemData entry, CurrencyData playerCurrency, Callable1Args<Integer> onBuy) {
        super(layer, "itemBuyDialog", "Buy Item", WINDOW_W, WINDOW_H, false);
        positionProportionally(0.5f, 0.5f);
        this.layer = layer;
        this.entry = entry;
        this.playerCurrency = playerCurrency;
        setModal(true);

        Table itemContainer = add(new Table()).left().fillX().expandX().getActor();

        H5EStack stack = new H5EStack();
        itemContainer.add(stack).left().pad(5).size(ICON_SIZE);
        itemIcon = new GCItemIcon<>(layer);
        itemIcon.setItem(entry.getItem());
        stack.add(itemIcon);

        Table subTable = new Table().top();
        itemContainer.add(subTable).left().pad(5).growX();
        itemContainer.background(StyleFactory.INSTANCE.panelStyleBlueCornersSemiTransparent);
        itemNameLabel = new H5ELabel(layer);
        subTable.add(itemNameLabel).left().growX().getActor();
        itemNameLabel.setText(entry.getData().name);
        itemNameLabel.setWrap(true);

        subTable.row();
        sellerLabel = new H5ELabel(entry.getData().sellerNickname, layer);
        sellerLabel.setFontScale(0.8f);
        subTable.add(sellerLabel).left();
        Table priceTable = new Table();
        subTable.row();
        currencyIcon = H5EIcon.fromSpriteType(layer, entry.getData().price.icon);
        priceLabel = new H5ELabel(String.valueOf(entry.getData().price.amount), layer);
        priceTable.add(priceLabel).left();
        priceTable.add(currencyIcon).padLeft(10).left().width(24).height(24);
        priceLabel.setFontScale(0.8f);
        subTable.add(priceTable).left();
        row();
        sliderRow = new Table();
        if (entry.getItem().getQuantity() > 1) {
            slider = new GCSlider(layer);
            slider.setRange(1, entry.getItem().getQuantity(), 1f);
            slider.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    buttonText.setText(String.valueOf((long)slider.getValue() * entry.getData().price.amount));
                    buyBtn.setDisabled(playerCurrency.amount < slider.getValue() * entry.getData().price.amount);
                }
            });
            sliderRow.add(slider).fillX();
            H5EInputBox countBox = new H5EInputBox(layer);
            slider.attachInputBox(countBox);
            slider.setValue(0);
            countBox.setTypeNumber();
            sliderRow.add(countBox).width(100);
        }
        add(sliderRow).growX().padTop(10);
        row();
        Table buttonRow = new Table();
        buyBtn = new H5EButton("", layer);
        buttonText = new H5ELabel("", layer);
        buyBtn.add(new H5ELabel("Buy for ", layer));
        btnIcon = H5EIcon.fromSpriteType(layer, entry.getData().price.icon);
        buyBtn.add(buttonText).left();
        buyBtn.add(btnIcon).left().width(24).height(24);
        buyBtn.addButtonListener(() -> {
            if (entry.getItem().getQuantity() > 1) {
                onBuy.call((int) slider.getValue());
            } else {
                onBuy.call(1);
            }
        });
        if (entry.getItem().getQuantity() > 1) {
            buttonText.setText(String.valueOf((long)slider.getValue() * entry.getData().price.amount));
        } else {
            buttonText.setText(String.valueOf(entry.getData().price.amount));
        }
        buyBtn.setDisabled(playerCurrency.amount < entry.getData().price.amount);
        buttonRow.add(buyBtn).padTop(10).center().growX();
        add(buttonRow).growX();
    }

    public void updateData(GCStoreListItemData entry, CurrencyData playerCurrency) {
        if (entry.getData().id.equals(this.entry.getData().id)) {
            if (entry.getItem().getQuantity() == 0) {
                close();
                return;
            }
            this.entry=entry;
            this.playerCurrency = playerCurrency;
            currencyIcon.setDrawable(new TextureRegionDrawable(layer.getEngine().getResourceManager().getSpriteType(entry.getData().price.icon).getGraphicData()));
            btnIcon.setDrawable(new TextureRegionDrawable(layer.getEngine().getResourceManager().getSpriteType(entry.getData().price.icon).getGraphicData()));
            priceLabel.setText(String.valueOf(entry.getData().price.amount));
            sellerLabel.setText(entry.getData().sellerNickname);
            itemNameLabel.setText(entry.getData().name);
            itemIcon.setItem(entry.getItem());
            sliderRow.clear();
            if (entry.getItem().getQuantity() > 1) {
                slider = new GCSlider(layer);
                slider.setRange(1, entry.getItem().getQuantity(), 1f);
                slider.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        buttonText.setText(String.valueOf((long)slider.getValue() * entry.getData().price.amount));
                        buyBtn.setDisabled(playerCurrency.amount < slider.getValue() * entry.getData().price.amount);
                    }
                });
                sliderRow.add(slider).fillX();
                H5EInputBox countBox = new H5EInputBox(layer);
                slider.attachInputBox(countBox);
                slider.setValue(0);
                countBox.setTypeNumber();
                sliderRow.add(countBox).width(100);
            }
            if (entry.getItem().getQuantity() > 1) {
                buttonText.setText(String.valueOf((long)slider.getValue() * entry.getData().price.amount));
            } else {
                buttonText.setText(String.valueOf(entry.getData().price.amount));
            }
            buyBtn.setDisabled(playerCurrency.amount < entry.getData().price.amount);
        }
    }

    public GCStoreListItemData getEntry() {
        return entry;
    }

}