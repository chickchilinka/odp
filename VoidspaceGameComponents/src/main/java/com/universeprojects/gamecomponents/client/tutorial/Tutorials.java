package com.universeprojects.gamecomponents.client.tutorial;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Main public API for Tutorial System.
 *
 * Handles objective handling and registering graphics component.
 * Uses internal TutorialController to process all the logic.
 */
@SuppressWarnings("UnusedReturnValue")
public class Tutorials {
    public static boolean watchTutorialCodes = false;

    private static TutorialController INSTANCE;

    /**
     * Scan through all active stages and find those
     * that have provided objective name if their objective
     * list. These stages will be marked as completed and
     * their child stages will be added to currently active
     * stages.
     * <p>
     * If no stages were listening to this objective ID,
     * nothing will happen
     *
     * @param objective Objective ID as it was defined in tutorial
     *                  configuration file
     */
    public static void trigger(String objective) {
        if(INSTANCE == null) {
            return;
        }
        INSTANCE.triggerObjective(objective);
    }

    public static void replayTutorials() {
        if (INSTANCE == null) return;

        INSTANCE.replayTutorials();
    }

    public static void setActive(String tutorialName) {
        if(INSTANCE == null) {
            return;
        }
        INSTANCE.setActive(tutorialName);
    }

    public static void setInstance(TutorialController instance) {
        Tutorials.INSTANCE = instance;
    }

    public static <T extends Actor> T registerObject(String objectId, T actor) {
        if (INSTANCE == null)
            return actor;
        INSTANCE.getMarkerManager().registerObject(objectId, actor);
        return actor;
    }

    public static <T extends Actor> T registerObject(T actor) {
        if (INSTANCE == null)
            return actor;
        INSTANCE.getMarkerManager().registerObject(actor);
        return actor;
    }

    public static void registerCheckerObject(String id, Object obj){
        if(INSTANCE == null)return;
        INSTANCE.getMarkerManager().registerCheckerObject(id, obj);
    }

    public static void removeObject(String objectId) {
        if (INSTANCE == null)
            return;
        INSTANCE.getMarkerManager().removeObject(objectId);
    }

    public static void removeObject(String objectId, Actor actor) {
        if (INSTANCE == null)
            return;
        INSTANCE.getMarkerManager().removeObject(objectId, actor);
    }

    public static void changeTutorialBank(String tutorialName) {
        if(INSTANCE == null) {
            return;
        }
        INSTANCE.getConfig().buildTutorialStages(tutorialName);
        INSTANCE.setConfig(INSTANCE.getConfig());
    }

    public static void update() {
        if(INSTANCE == null) {
            return;
        }

        INSTANCE.update();
    }

    public static void triggerNearbyObject(String worldID) {
        if(INSTANCE == null) {
            return;
        }

        INSTANCE.triggerNearbyObject(worldID);
    }

    public static void triggerControlledObject(String worldID) {
        if (INSTANCE == null) {
            return;
        }

        INSTANCE.triggerControlledObject(worldID);
    }

    public static void updateNearbyObjectRequests() {
        if (INSTANCE == null) {
            return;
        }

        INSTANCE.updateNearbyObjectRequests();
    }

    public static void addTutorialElements(String tutorialId, Object tutorialElementsData) {
        if (INSTANCE == null) {
            return;
        }

        INSTANCE.getConfig().addTutorialElement(tutorialId, tutorialElementsData);
    }
}
