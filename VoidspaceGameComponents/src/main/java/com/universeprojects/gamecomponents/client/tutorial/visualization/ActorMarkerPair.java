package com.universeprojects.gamecomponents.client.tutorial.visualization;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarker;

class ActorMarkerPair {
    private Actor actor;
    private TutorialMarker marker;

    public ActorMarkerPair(Actor actor, TutorialMarker marker) {
        this.actor = actor;
        this.marker = marker;
    }

    public Actor getActor() {
        return actor;
    }

    public TutorialMarker getMarker() {
        return marker;
    }
}
