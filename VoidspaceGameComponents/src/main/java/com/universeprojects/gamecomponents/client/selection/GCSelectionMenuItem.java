package com.universeprojects.gamecomponents.client.selection;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
//import com.universeprojects.voidspace.shared.services.effects.SoundEffect;

class GCSelectionMenuItem extends GCSelectionMenuClickable {

    private static final Logger log = Logger.getLogger(GCSelectionMenuItem.class);

    final static float FONT_SCALE = 1.0f;

    private final GCSelectionMenu menu;
    private final Image selectionArrow;

    private final GCSelectionMenuNode node;
    private final H5ELabel itemLabel;

    GCSelectionMenuItem(GCSelectionMenu menu, GCSelectionMenuNode node) {
        super(menu.getLayer());
        defaults().left();
        this.menu = Dev.checkNotNull(menu);

        this.node = node;

        final H5ELayer layer = Dev.checkNotNull(menu.getLayer());

        selectionArrow = new Image(layer.getEngine().getSkin(), "selection-menu-arrow");
        add(selectionArrow).left();

        itemLabel = new H5ELabel(node.getName(), layer);
        add(itemLabel).left();
        itemLabel.setAlignment(Align.left);
        itemLabel.setFontScale(FONT_SCALE);
        itemLabel.setColor(GCSelectionMenu.FONT_COLOR);

        Image enterArrow = new Image(layer.getEngine().getSkin(), "selection-menu-submenu-arrow");
        enterArrow.setVisible(node.hasChildren());
        add(enterArrow).left().padLeft(5);
        align(Align.left);

        setupClickable(this);
        unselect();
    }

    @Override
    public void setScale(float scaleXY) {
        super.setScale(scaleXY);
        itemLabel.setFontScale(FONT_SCALE * scaleXY);
    }

    @Override
    void onClick() {

        activate();
    }

    @Override
    void onPointerEnter() {
        menu.setSelection(this);
    }

    GCSelectionMenuNode getNode() {
        return node;
    }

    @Override
    void select() {
        super.select();
        selectionArrow.setVisible(true);
        getColor().a = 1;

    }

    @Override
    void unselect() {
        super.unselect();
        selectionArrow.setVisible(false);
        getColor().a = 0.6f;
    }

    void activate() {
        if (node.hasChildren()) {
            menu.enter(node);
        } else {
            log.debug("Triggered menu action: " + node.getName());
            GCSelectionMenuActionHandler actionHandler = menu.getActionHandler();
            if (actionHandler == null) {
                throw new IllegalStateException("Action handler is not setup");
            }
            actionHandler.executeAction(node.getPath());
        }
    }

    @Override
    public String getName() {
        return node.getName();
    }
}
