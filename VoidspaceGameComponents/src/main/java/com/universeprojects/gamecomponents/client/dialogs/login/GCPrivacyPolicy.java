package com.universeprojects.gamecomponents.client.dialogs.login;


import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.html5engine.client.framework.H5ELayer;


public class GCPrivacyPolicy extends GCPolicyDialog {

    private final GCLoginManager<?, ?, ?> loginManager;

    public GCPrivacyPolicy(H5ELayer layer, GCLoginManager<?, ?, ?> loginManager){
        super(layer);
        this.loginManager = loginManager;
        window.setTitle("Privacy Policy");
    }

    @Override
    public void open(Callable0Args onAccepted, Callable0Args onRejected) {
        super.open(onAccepted, onRejected);
        loadingIndicator.activate(window);
        loginManager.loadPrivacyPolicy((text) -> {
            loadingIndicator.deactivate();
            setText(text);
        }, (error) -> {
            loadingIndicator.deactivate();
            Log.error("Unable to download Privacy Policy: " + error);
            //TODO
        });
    }

}
