package com.universeprojects.gamecomponents.client.dialogs.battleRoyaleLobby;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.*;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class ShipSelectorDialog extends GCWindow {

    private final H5ELayer layer;

    private final H5EScrollablePane listPane;
    private final ButtonGroup<RealmShipListItem> buttonGroup;

    private final H5EContainer shipPane;
    private final Table rightShipTable;
    private final H5ELabel shipName;
    private final H5ELabel shipDescription;
    private final H5EIcon mainShipIcon;
    private final H5EIcon topShipIcon;
    private final GCLoadingIndicator shipLoadingIndicator;
    private boolean loadedOneSprite = false;

    private final H5ELabel priceLabel;
    private final H5EButton btnSelect;

    private RealmShipData currentShip;

    private enum WindowState {DESKTOP, LANDSCAPE, PORTRAIT}
    private WindowState lastState = WindowState.DESKTOP;

    public ShipSelectorDialog(H5ELayer layer, Callable1Args<RealmShipData> onSelectShipButtonPressed) {
        super(layer,600, 600);
        this.layer = layer;

        setTitle("Select a Ship");
        setId("realm-ship-selection");
        positionProportionally(0.5f, 0.5f);
        setKeepWithinStage(true);
        setFullscreenOnMobile(true);
        shipLoadingIndicator = new GCLoadingIndicator(layer);

        H5EContainer mainTable = add(new H5EContainer(layer)).grow().getActor();
        mainTable.setBackground("purchase-dialog-subpanel");

        // Ship Pane
        shipPane = mainTable.add(new H5EContainer(layer)).grow().pad(5).getActor();
        shipPane.setBackground("scrollpane-backing-2-drawable");
        mainTable.row();

        topShipIcon = new H5EIcon(layer, new BaseDrawable());
        topShipIcon.setScaling(Scaling.fit);
        shipPane.add(topShipIcon).grow().uniformX().center().pad(25,25,25,0);

        rightShipTable = shipPane.add(new Table()).grow().uniformX().pad(15,0,25,15).getActor();

        shipName = rightShipTable.add(new H5ELabel("Ship Name",layer)).align(Align.left).getActor();
        rightShipTable.row();

        shipDescription = rightShipTable.add(new H5ELabel("ship description ship description ship description ship description ship description",layer)).align(Align.left).growX().getActor();
        shipDescription.setWrap(true);
        shipDescription.setFontScale(0.75f);
        rightShipTable.row();

        mainShipIcon = new H5EIcon(layer, new BaseDrawable());
        mainShipIcon.setScaling(Scaling.fit);
        rightShipTable.add(mainShipIcon).grow().center().padTop(15);

        // Ship List Pane
        Table listTable = mainTable.add(new Table()).growX().pad(5).getActor();
        mainTable.row();

        H5EButton leftButton = ButtonBuilder.inLayer(layer).withStyle("button-minimap-zoom-out").build();
        listTable.add(leftButton).size(64);

        listPane = listTable.add(new H5EScrollablePane(layer, "scrollpane-backing-1")).growX().height(90).getActor();
        listPane.setOverscroll(false, false);
        listPane.setScrollingDisabled(false, true);
        listPane.getContent().center();
        buttonGroup = new ButtonGroup<>();
        buttonGroup.setMinCheckCount(1);

        H5EButton rightButton = ButtonBuilder.inLayer(layer).withStyle("button-minimap-zoom-in").build();
        listTable.add(rightButton).size(64);

        leftButton.addButtonListener(() -> {
            buttonGroup.getButtons().get(UPMath.max(buttonGroup.getCheckedIndex() - 1, 0)).setChecked(true);
            listPane.setScrollX(buttonGroup.getChecked().getX() + buttonGroup.getChecked().getWidth()/2f - listPane.getWidth()/2f);
        });

        rightButton.addButtonListener(() -> {
            buttonGroup.getButtons().get(UPMath.min(buttonGroup.getCheckedIndex() + 1, buttonGroup.getButtons().size - 1)).setChecked(true);
            listPane.setScrollX(buttonGroup.getChecked().getX() + buttonGroup.getChecked().getWidth()/2f - listPane.getWidth()/2f);
        });

        // Price and Select Button
        Table bottomTable = mainTable.add(new Table()).growX().pad(5).getActor();

        priceLabel = bottomTable.add(new H5ELabel("Price: $999.99",layer)).align(Align.left).growX().padLeft(10).getActor();
        priceLabel.setFontScale(1.5f);

        btnSelect = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Select This Ship").build();
        bottomTable.add(btnSelect).width(200).align(Align.right);
        btnSelect.addButtonListener(() -> {
            if (currentShip.owned) {
                // Button currently says "SELECT THIS SHIP"
                onSelectShipButtonPressed.call(currentShip);
                this.close();
            } else {
                // Button currently says "BUY NOW"
                // not currently implemented
            }
        });



        // Test data
        addShip(new RealmShipListItem(layer, new RealmShipData(
                "Harbor G7 Class 1A",
                "ship description ship description ship description ship description ship description",
                299,
                "images/ships/harbor/harbor-g7-class1a-icon",
                "images/ships/harbor/harbor-g7-class1a",
                true)));
        addShip(new RealmShipListItem(layer, new RealmShipData(
                "Infected Ship Class 1A",
                "ship description ship description ship description ship description ship description",
                99,
                "images/ships/infected/infected-ship-class1a-icon",
                "images/ships/infected/infected-ship-class1a",
                true)));
        addShip(new RealmShipListItem(layer, new RealmShipData(
                "Infected Ship Class 1B",
                "ship description ship description ship description ship description ship description",
                149,
                "images/ships/infected/infected-ship-class1b-icon",
                "images/ships/infected/infected-ship-class1b",
                true)));
        addShip(new RealmShipListItem(layer, new RealmShipData(
                "Infected Ship Class 2A",
                "ship description ship description ship description ship description ship description",
                299,
                "images/ships/infected/infected-ship-class2a-icon",
                "images/ships/infected/infected-ship-class2a",
                true)));
        addShip(new RealmShipListItem(layer, new RealmShipData(
                "Infected Ship Class 3A",
                "ship description ship description ship description ship description ship description",
                499,
                "images/ships/infected/infected-ship-class3a-icon",
                "images/ships/infected/infected-ship-class3a",
                true)));
        addShip(new RealmShipListItem(layer, new RealmShipData(
                "Infected Ship Class 4A",
                "ship description ship description ship description ship description ship description",
                899,
                "images/ships/infected/infected-ship-class4a-icon",
                "images/ships/infected/infected-ship-class4a",
                false)));

        addShip(new RealmShipListItem(layer, new RealmShipData(
                "Harbor G7 Class 1A",
                "ship description ship description ship description ship description ship description",
                299,
                "images/ships/harbor/harbor-g7-class1a-icon",
                "images/ships/harbor/harbor-g7-class1a",
                false)));
        addShip(new RealmShipListItem(layer, new RealmShipData(
                "Harbor G7 Class 1A",
                "ship description ship description ship description ship description ship description",
                299,
                "images/ships/harbor/harbor-g7-class1a-icon",
                "images/ships/harbor/harbor-g7-class1a",
                false)));

        changeShip(buttonGroup.getChecked().getDataItem());
        // end of test data
    }

    // Changes the main ship that is being displayed
    private void changeShip(RealmShipData data) {
        currentShip = data;
        shipLoadingIndicator.activate(shipPane);
        shipName.setText(data.name);

        if (data.mainIconURL == null || data.mainIconURL.length() == 0) data.mainIconURL = "voidspace";
        if (data.topDownURL == null || data.topDownURL.length() == 0) data.topDownURL = "voidspace";

        H5ESpriteType topSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(data.topDownURL);
        topSpriteType.subscribeToLoadStateChange((state)-> {
            topShipIcon.setDrawable(new TextureRegionDrawable(topSpriteType.getGraphicData()));
            onSpriteLoaded();
        });

        H5ESpriteType mainSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(data.mainIconURL);
        mainSpriteType.subscribeToLoadStateChange((state)-> {
            mainShipIcon.setDrawable(new TextureRegionDrawable(mainSpriteType.getGraphicData()));
            onSpriteLoaded();
        });

        if (!data.owned) {
            shipPane.setColor(1.0f, 0.4f, 0.4f, 1.0f);
            btnSelect.setText("Buy Now");
            priceLabel.setText("Price: $" + (float) data.price / 100.0f);
        } else {
            shipPane.setColor(1,1,1,1);
            btnSelect.setText("Select this ship");
            priceLabel.setText("Ship Owned");
        }
    }

    private void onSpriteLoaded() {
        if (!loadedOneSprite) {
            loadedOneSprite = true;
        } else {
            shipLoadingIndicator.deactivate();
            loadedOneSprite = false;
        }
    }

    // Adds a ship to the list
    private void addShip(RealmShipListItem ship) {
        listPane.getContent().add(ship);
        buttonGroup.add(ship);
        ship.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (ship.isChecked()) {
                    changeShip(ship.getDataItem());
                }
                event.handle();
            }
        });
    }

    // Removes a ship from the list
    private void removeShip(RealmShipListItem ship) {
        buttonGroup.remove(ship);
        listPane.getContent().clearChildren();
        for (RealmShipListItem newShip : buttonGroup.getButtons()) {
            listPane.getContent().add(newShip);
        }
    }

    @Override
    protected void positionOnResize() {
        resetLayout();
        super.positionOnResize();
    }

    private void resetLayout() {
        // Reconstruct the window based on device and orientation
        WindowState currentState = WindowState.DESKTOP;
        if (isFullscreen()) {
            currentState = getEngine().getWidth() > getEngine().getHeight() ? WindowState.LANDSCAPE : WindowState.PORTRAIT;
        }
        if (currentState == lastState) return;
        lastState = currentState;

        shipPane.clearChildren();
        rightShipTable.clearChildren();
        switch (currentState) {
            case DESKTOP:
                // Default window
                shipPane.add(topShipIcon).grow().uniformX().center().pad(25,25,25,0);
                shipPane.add(rightShipTable).grow().uniformX().pad(15,0,25,15);
                rightShipTable.add(shipName).align(Align.left);
                rightShipTable.row();
                rightShipTable.add(shipDescription).align(Align.left).growX().getActor();
                rightShipTable.row();
                rightShipTable.add(mainShipIcon).grow().center().padTop(15);
                break;

            case LANDSCAPE:
                // Fullscreen, similar layout to desktop
                shipPane.add(topShipIcon).grow().uniformX().center().pad(20,25,20,0);
                shipPane.add(mainShipIcon).grow().uniformX().pad(20,10,20,10);
                shipPane.add(rightShipTable).grow().uniformX().pad(15,0,25,15);
                rightShipTable.add(shipName).align(Align.left);
                rightShipTable.row();
                rightShipTable.add(shipDescription).align(Align.left).growX().getActor();
                break;

            case PORTRAIT:
                // Fullscreen, different layout from desktop
                shipPane.add(topShipIcon).grow().uniformY().center().pad(25,10,0,125);
                shipPane.row();
                shipPane.add(mainShipIcon).grow().uniformY().center().pad(-50, 100, 5, 10);
                shipPane.row();
                shipPane.add(rightShipTable).growX().pad(0,25,25,25);
                rightShipTable.add(shipName).align(Align.left);
                rightShipTable.row();
                rightShipTable.add(shipDescription).align(Align.left).growX().getActor();
                rightShipTable.row();
                break;
        }
    }
}
