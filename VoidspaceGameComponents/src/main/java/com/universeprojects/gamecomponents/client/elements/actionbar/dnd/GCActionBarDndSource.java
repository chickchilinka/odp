package com.universeprojects.gamecomponents.client.elements.actionbar.dnd;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarType;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCOperation;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCActionBarDndSource extends GCActionDndSource {
    private final GCActionBarType type;
    private final Integer index;
    private Integer row;

    public GCActionBarDndSource(Actor actor, GCOperation action, H5ELayer layer, GCActionBarType type, Integer index) {
        super(actor, action, layer);
        this.type = type;
        this.index = index;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getIndex() {
        return index;
    }

    public GCActionBarType getType() {
        return type;
    }
}
