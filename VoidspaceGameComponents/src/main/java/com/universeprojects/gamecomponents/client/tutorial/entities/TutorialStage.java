package com.universeprojects.gamecomponents.client.tutorial.entities;

import com.universeprojects.common.shared.callable.ReturningCallable1Args;
import com.universeprojects.gamecomponents.client.tutorial.TutorialDialogData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class TutorialStage implements Comparable<TutorialStage> {
    private String id;
    private int priority;
    private TutorialMonologue monologue;
    private TutorialDialogData dialog;

    private final List<TutorialMarker> markers = new ArrayList<>();
    private final Set<String> objectives = new HashSet<>();
    private final List<String> nextStages = new ArrayList<>();
    private final Map<String, List<ReturningCallable1Args<?, ?>>> stageCheckers = new HashMap<>();
    private boolean activeByDefault = false;

    public void setInfoDialog(TutorialDialogData dialog) {
        this.dialog = dialog;
    }

    public TutorialDialogData getDialog() {
        return dialog;
    }

    public boolean isActiveByDefault() {
        return activeByDefault;
    }

    public void setActiveByDefault(boolean activeByDefault) {
        this.activeByDefault = activeByDefault;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TutorialMarker> getMarkers() {
        return markers;
    }

    public void addMarkers(List<TutorialMarker> markers) {
        this.markers.addAll(markers);
    }

    public void addMarker(TutorialMarker marker) {
        this.markers.add(marker);
    }

    public TutorialMonologue getMonologue() {
        return monologue;
    }

    public void setMonologue(TutorialMonologue monos) {
        this.monologue = monos;
    }

    public Set<String> getObjectives() {
        return objectives;
    }

    public void setObjectives(Set<String> objectives) {
        this.objectives.clear();
        this.objectives.addAll(objectives);
    }

    public boolean hasObjective() {
        return !objectives.isEmpty();
    }

    public void addObjective(String objective) {
        this.objectives.add(objective);
    }

    public List<String> getNextStages() {
        return nextStages;
    }

    public void setNextStages(List<String> nextStages) {
        this.nextStages.clear();
        this.nextStages.addAll(nextStages);
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }


    /**
     * registers a lambda on this stage for prechecking conditions
     * */
    public void addChecker(String id, ReturningCallable1Args<?, ?> callable){
        if(id == null || callable == null)return;
        List<ReturningCallable1Args<?, ?>> callables = stageCheckers.get(id);
        if(callables == null) {
            callables = new ArrayList<>();
        }
        callables.add(callable);
        stageCheckers.put(id, callables);
    }

    public Map<String, List<ReturningCallable1Args<?,?>>> getStageCheckers(){
        return stageCheckers;
    }

    @Override
    public String toString() {
        return "TutorialStage{" +
                "id='" + id + '\'' +
                ", priority=" + priority +
                ", objectives='" + objectives + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TutorialStage that = (TutorialStage) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(priority, that.priority);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, priority);
    }

    @Override
    @SuppressWarnings("NullableProblems")
    public int compareTo(TutorialStage o) {
        if(o == null) {
            return 1;
        }
        int priorityComparison = Integer.compare(priority, o.priority);
        if(priorityComparison != 0) {
            return priorityComparison;
        }
        return id.compareTo(o.id);
    }
}
