package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.GCUtils;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCSlider;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EProgressBar;
import com.universeprojects.html5engine.shared.UPUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public abstract class GCFluidTransfer<T extends GCInventoryItem> extends GCInventoryBase<GCFluidTransfer.Slot, T, GCFluidContainerSlotComponent<T>> {

    public enum Slot {
        SOURCE_1, SOURCE_2, SOURCE_3, TARGET
    }

    public static final List<Slot> SLOT_LIST = Collections.unmodifiableList(Arrays.asList(Slot.values()));

    public static final List<Slot> SOURCE_SLOTS = Collections.unmodifiableList(Arrays.asList(Slot.SOURCE_1, Slot.SOURCE_2, Slot.SOURCE_3));

    private final Map<Slot, T> slots = new HashMap<>();
    private final Map<Slot, SlotGraphics> slotGraphics = new HashMap<>();
    private final Map<Slot, Float> tankChanges = new HashMap<>();
    private final Map<Slot, Float> currentRatios = new HashMap<>();
    private boolean reverseAllowed;

    private final H5EProgressBar progressBar;

    private final GCSlider quantitySlider;

    public GCFluidTransfer(H5ELayer layer) {
        super(layer);
        int windowWidth = 500;


        H5EContainer barBox = add(new H5EContainer(layer)).colspan(3).growX().getActor();
        barBox.setBackground("purchase-dialog-subpanel");

        Table sourceContainersTable = barBox.add(new Table()).getActor();
        for(Slot slot : SOURCE_SLOTS) {
            createSlot(layer, sourceContainersTable, slot);
        }

        Table middleContainerTable = new Table();

        quantitySlider = new GCSlider(layer);
        quantitySlider.setValue(0f);
        disableSlider();

        quantitySlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onChangeQuantity();
            }
        });

        middleContainerTable.add(new H5EIcon(layer, "images/GUI/ui2/transfer-fluid-direction1.png", Scaling.none, Align.center, 77, 48)).width(77).height(48);
        middleContainerTable.row();
        middleContainerTable.add(quantitySlider).growX();

        middleContainerTable.row();

        H5ELabel noteLabel = new H5ELabel("Note: If you want to transfer fluids the other way, you need to drag the item in the right slot to the left slot first.", layer);
        noteLabel.setWrap(true);
        noteLabel.setStyle(StyleFactory.INSTANCE.labelSmallDefault);
        noteLabel.setColor(0.7f, 0.7f, 0.7f, 1);
        middleContainerTable.add(noteLabel).grow().pad(2).padTop(10);

        middleContainerTable.row();

        progressBar = new H5EProgressBar(layer, "default-horizontal-green");
        progressBar.setVisible(false);
        middleContainerTable.add(progressBar).grow().pad(2).pad(5);

        barBox.add(middleContainerTable);


        Table targetContainersTable = barBox.add(new Table()).getActor();
        createSlot(layer, targetContainersTable, Slot.TARGET);

        row().padTop(10);
        add();
        row();
        setWidth(windowWidth);
        add(ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Cancel").withButtonListener(this::onCancel).build()).width(140).left();
        add(ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Transfer").withButtonListener(this::executeTransfer).build()).width(140).right();

        GCUtils.preventSchematicWindowClosingFor(this);

//        this.debugAll();
    }

    private void triggerFluidTransferAnimation() {
        progressBar.setVisible(true);
        progressBar.setRange(0, 20);
        progressBar.setValue(0);

        UPUtils.simpleTimer(0, 40f, 20, () -> {
            float progress = progressBar.getValue();
            progressBar.setValue(progress+1);
            if (progress==20) {
                progressBar.setVisible(false);
                return false;
            }
            return true;
        });
    }

    public T getSourceTank1() {
        return slots.get(Slot.SOURCE_1);
    }

    public T getSourceTank2() {
        return slots.get(Slot.SOURCE_2);
    }

    public T getSourceTank3() {
        return slots.get(Slot.SOURCE_3);
    }

    public T getTargetTank() {
        return slots.get(Slot.TARGET);
    }

    private void createSlot(H5ELayer layer, Table table, Slot slot) {
        final SlotGraphics graphics = new SlotGraphics(slot);
        slotGraphics.put(slot, graphics);
        Table containerTable = table.add(new Table()).getActor();

        H5EProgressBar containerBar = new H5EProgressBar(layer);
        containerBar.setDisplayMode(H5EProgressBar.DisplayMode.NO_LABEL);

        H5ELabel amount = new H5ELabel("", layer);

        Stack progressBarStack = new Stack();
        progressBarStack.add(containerBar);
        progressBarStack.add(amount);

        H5ELabel containerLabel = new H5ELabel("", layer);

        GCFluidContainerSlotComponent<T> component = new GCFluidContainerSlotComponent<>(this, slot, null);

        containerTable.add(containerLabel);
        containerTable.row();
        containerTable.add(component);
        containerTable.row();
        containerTable.add(progressBarStack);
        table.row();

        graphics.bar = containerBar;
        graphics.label = containerLabel;
        graphics.component = component;
    }

    protected void executeTransfer() {
        final Float targetDelta = tankChanges.get(Slot.TARGET);
        final T targetItem = getItem(Slot.TARGET);
        if(targetItem == null || targetDelta == null || targetDelta == 0f) {
            return;
        }
        final Map<T, Float> sourceTankDeltas = new HashMap<>();
        for (Slot slot : SOURCE_SLOTS) {
            final T item = getItem(slot);
            if (item != null) {
                final Float change = tankChanges.get(slot);
                sourceTankDeltas.put(item, change);
            }
        }
        executeTransfer(sourceTankDeltas, targetItem, targetDelta);

        triggerFluidTransferAnimation();
    }

    protected abstract void executeTransfer(Map<T, Float> sourceTankDeltas, T targetTank, float targetDelta);

    protected abstract String determineCurrentRecipeTargetType();

    protected abstract void onCancel();

    protected abstract Map<String, Float> determineCurrentRecipeRatios();

    protected abstract boolean isReverseTransferAllowed();

    private void onChangeQuantity() {
        updateTankChanges();
    }

    protected void updateTankChanges() {
        final float value = quantitySlider.getValue();
        final float targetChange = reverseAllowed || value >= 0 ? value : 0;
        tankChanges.put(Slot.TARGET, targetChange);
        for (Slot slot : SOURCE_SLOTS) {
            final Float ratio = currentRatios.get(slot);
            if(ratio != null) {
                T tank = slots.get(slot);
                tankChanges.put(slot, - Math.min(tank.getInternalTank().getCurrentVolume(), ratio * targetChange));
            }
        }
        updateBars();
    }

    private void updateBars() {
        for (Slot slot : SLOT_LIST) {
            final T item = getItem(slot);
            if(item != null) {
                final SlotGraphics slotGraphics = this.slotGraphics.get(slot);
                final float change = Dev.withDefault(tankChanges.get(slot), 0f);
                slotGraphics.bar.setValue(item.getInternalTank().getCurrentVolume() + change);
            }
        }
    }

    @Override
    protected GCFluidContainerSlotComponent<T> createComponent(Slot key) {
        return slotGraphics.get(key).component;
    }

    @Override
    protected Collection<Slot> getSlots() {
        return SLOT_LIST;
    }

    @Override
    protected T getItem(Slot key) {
        return slots.get(key);
    }

    public void setSlot(Slot key, T item) {
        slots.put(key, item);
        resetGraphics();
    }

    public void resetSlotGraphics() {
        for (Slot slot : getSlots()) {
            resetSlotGraphics(slot);
        }
    }

    private void resetSlotGraphics(Slot key) {
        final T item = slots.get(key);
        final SlotGraphics slotGraphics = this.slotGraphics.get(key);
        slotGraphics.component.setItem(item);
        if(item != null) {
            final GCInternalTank tank = item.getInternalTank();
            final String fluidType = Strings.nullToEmpty(tank.getFluidType());
            slotGraphics.label.setText(fluidType);
            slotGraphics.bar.setRange(0, tank.getMaxVolume());
            slotGraphics.bar.setValue(tank.getCurrentVolume());
        } else {
            slotGraphics.label.setText("");
            slotGraphics.bar.setRange(0, 100);
            slotGraphics.bar.setValue(0);
        }
    }

    @Override
    protected List<InventoryAction> getActions(Slot key) {
        return Collections.emptyList();
    }

    @Override
    protected <KT> void onCrossInventoryDropFromHere(Slot startKey, T item, GCInventoryBase<KT, ?, ?> targetInventory, boolean willBeUnlinkedLater) {
    }

    @Override
    protected void onSameInventoryDrop(Slot startKey, T item, Slot targetKey) {
        T oldTargetItem = getItem(targetKey);
        slots.put(startKey, oldTargetItem);
        slots.put(targetKey, item);
        resetGraphics();
    }

    private void resetGraphics() {
        resetSlotGraphics();
        updateItems();
        final T targetItem = getItem(Slot.TARGET);
        if(targetItem == null) {
            disableSlider();
            return;
        }
        final GCInternalTank targetTank = targetItem.getInternalTank();
        String targetType = determineCurrentRecipeTargetType();
        if(targetType == null) {
            disableSlider();
            return;
        }
        if(targetTank.getFluidType() == null) {
            if (targetTank.getForcedType() == null || targetType.equals(targetTank.getForcedType())) {
                slotGraphics.get(Slot.TARGET).label.setText(targetType);
                setupSlider();
            } else {
                disableSlider();
            }
        } else {
            if(targetType.equals(targetTank.getFluidType())) {
                setupSlider();
            } else {
                disableSlider();
            }
        }
    }

    private void setupSlider() {
        for (Slot slot : getSlots()) {
            tankChanges.put(slot, 0f);
        }
        currentRatios.clear();
        final Map<String, Float> ratios = determineCurrentRecipeRatios();
        if(ratios == null) {
            return;
        }
        final GCInternalTank targetTank = getTargetTank().getInternalTank();
        float maxToAdd = targetTank.getMaxVolume() - targetTank.getCurrentVolume();
        float maxToRemove = targetTank.getCurrentVolume();
        for (Slot slot : SOURCE_SLOTS) {
            final T item = getItem(slot);
            if (item == null) {
                continue;
            }
            GCInternalTank sourceTank = item.getInternalTank();
            if(sourceTank == null) {
                continue;
            }
            final String fluidType = sourceTank.getFluidType();
            final Float ratio = ratios.get(fluidType);
            if(ratio == null) {
                continue;
            }
            currentRatios.put(slot, ratio);

            maxToAdd = Math.min(maxToAdd, sourceTank.getCurrentVolume() / ratio);
            final float sourceTankFreeVolume = Math.max(0f, sourceTank.getMaxVolume() - sourceTank.getCurrentVolume());
            maxToRemove = Math.min(maxToRemove, sourceTankFreeVolume / ratio);

        }
        reverseAllowed = isReverseTransferAllowed();

        if(reverseAllowed) {
            quantitySlider.setRange(-maxToRemove, maxToAdd);
            quantitySlider.setValue(0);
        } else {
            quantitySlider.setRange(0, maxToAdd);
            quantitySlider.setValue(0);
        }
        enableSlider();
    }

    @Override
    protected <KS, I extends GCInventoryItem> void onCrossInventoryDropToHere(GCInventoryBase<KS, ?, ?> startInventory, KS startKey, I item, Slot targetKey) {
        List<Slot> slotsToClear = new ArrayList<>();
        for (Map.Entry<Slot, T> slotEntry : slots.entrySet()) {
            if(slotEntry.getValue() != null && slotEntry.getValue().equals(item) && slotEntry.getKey() != targetKey) {
                slotsToClear.add(slotEntry.getKey());
            }
        }
        for (Slot slot : slotsToClear) {
            setSlot(slot, null);
        }


        //noinspection unchecked
        setSlot(targetKey, (T) item);
    }

    @Override
    protected <KT> boolean canCrossInventoryDropFromHere(Slot startKey, T item, GCInventoryBase<KT, ?, ?> targetInventory) {
        return false;
    }

    @Override
    protected void onSelected(GCFluidContainerSlotComponent<T> component) {
    }

    @Override
    protected void jettison(Slot startKey, T item) {
        setSlot(startKey, null);
    }

    @Override
    protected boolean canJettison(Slot startKey, T item) {
        return true;
    }

    private void disableSlider() {
        quantitySlider.setDisabled(true);
        quantitySlider.setColor(Color.DARK_GRAY);
    }

    private void enableSlider() {
        quantitySlider.setDisabled(false);
        quantitySlider.setColor(Color.WHITE);
    }

    private class SlotGraphics {
        final Slot slot;
        GCFluidContainerSlotComponent<T> component;
        H5EProgressBar bar;
        H5ELabel label;

        public SlotGraphics(Slot slot) {
            this.slot = slot;
        }
    }
}
