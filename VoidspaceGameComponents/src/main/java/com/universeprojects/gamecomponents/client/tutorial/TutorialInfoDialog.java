package com.universeprojects.gamecomponents.client.tutorial;

import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public abstract class TutorialInfoDialog extends GCWindow {
    protected final H5ELayer layer;
    private final String closeTrigger;

    public TutorialInfoDialog(H5ELayer layer, String closeTrigger) {
        super(layer, 600, 400);
        this.layer = layer;
        this.closeTrigger = closeTrigger;
    }

    @Override
    protected void onClose() {
        super.onClose();
        if(closeTrigger != null) {
            Tutorials.trigger(closeTrigger);
        }
    }

    @Override
    public void open() {
        if(isPackOnOpen()) {
            pack();
        }
        positionProportionally(0.5f, 0.5f);
        open(false);
    }
}
