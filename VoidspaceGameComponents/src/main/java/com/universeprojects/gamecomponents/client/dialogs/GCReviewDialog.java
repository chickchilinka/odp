package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ERate;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ETextArea;
import com.universeprojects.vsdata.shared.OrganizationReviewData;

public class GCReviewDialog extends GCSimpleWindow {
    private final static int WINDOW_W = 450;
    private final static int WINDOW_H = 450;
    private final H5ERate rate;
    private Callable1Args<OrganizationReviewData> onSubmit;
    private OrganizationReviewData data;
    private final H5ETextArea reviewArea;
    private final H5ELabel organizationTitle;

    public GCReviewDialog(H5ELayer layer, OrganizationReviewData data, Callable1Args<OrganizationReviewData> onSubmit) {
        this(layer);
        setReviewData(data);
        this.onSubmit = onSubmit;
    }

    public GCReviewDialog(H5ELayer layer) {
        super(layer, "reviewDialog", "Rate and Review", WINDOW_W, WINDOW_H, false);
        positionProportionally(0.65f, 0.65f);
        setFullscreenOnMobile(true);

        organizationTitle = addH2("").padLeft(5).center().getActor();

        row();
        addH2("Review").padLeft(5).left();

        row();
        reviewArea = new H5ETextArea(layer);
        add(reviewArea).padLeft(5).expand().fill();

        row();
        rate = new H5ERate(layer, false);
        add(rate).padLeft(5).left();

        H5EButton submit = new H5EButton("Submit", layer);
        H5EButton cancel = new H5EButton("Cancel", layer);
        cancel.addButtonListener(this::close);
        submit.addButtonListener(this::collectFormAndCallback);

        row();
        Table buttonRow = new Table();
        buttonRow.padTop(15).padRight(4).padBottom(4);
        buttonRow.add(submit).right();
        buttonRow.add(cancel).right();
        add(buttonRow).right();
    }


    public void openAndSet(OrganizationReviewData curData) {
        open();
        setReviewData(curData);
    }

    public void setReviewData(OrganizationReviewData curData) {
        data = curData;
        organizationTitle.setText(data.name);
        rate.setValue(data.rating);
        reviewArea.setText(data.review);
    }

    public void collectFormAndCallback() {
        if (data != null) {
            OrganizationReviewData outData = new OrganizationReviewData(data.id, data.name, "", 0);
            outData.rating = rate.getValue();
            outData.review = reviewArea.getText();
            onSubmit.call(outData);
        }
        close();
    }
}