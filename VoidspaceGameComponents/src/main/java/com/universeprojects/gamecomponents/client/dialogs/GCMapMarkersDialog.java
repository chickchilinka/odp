package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.*;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.MapMarkerData;
import com.universeprojects.vsdata.shared.MapMarkerGroupData;

import java.util.ArrayList;
import java.util.List;

public class GCMapMarkersDialog extends GCSimpleWindow {
    public static final int WINDOW_W = 600;
    public static final int WINDOW_H = 400;
    public final H5ELayer layer;
    public final Table groupsTable;
    public final Table markersTable;
    public final GCList<GCMarkersGroupItem> groupList;
    public final GCList<GCMarkerListItem> markerList;

    private final GCListItemActionHandler<GCMarkersGroupItem> groupHandler;
    private final GCListItemActionHandler<GCMarkerListItem> markersHandler;
    private final H5EScrollablePane groupsContainer;
    private final H5EScrollablePane markersContainer;
    private final H5EButton addGroupBtn;
    private final H5EButton shareGroup;
    private final H5EButton shareMarker;
    private MapMarkerGroupData currentGroup;
    private DragAndDrop dragAndDrop;

    public GCMapMarkersDialog(H5ELayer layer, Callable1Args<MapMarkerData> onShareMarker, Callable1Args<MapMarkerGroupData> onShareGroup) {
        super(layer, "mapmarkers", "Map Markers", WINDOW_W, WINDOW_H, false);
        this.layer = layer;
        positionProportionally(0.5f, 0.5f);
        setFullscreenOnMobile(true);
        dragAndDrop = new DragAndDrop();
        groupsContainer = new H5EScrollablePane(layer);
        groupsTable = groupsContainer.getContent();
        groupsContainer.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        groupList = new GCList<GCMarkersGroupItem>(layer, 1, 5, 1, true);
        groupHandler = new GCListItemActionHandler<GCMarkersGroupItem>() {
            @Override
            protected void onSelectionUpdate(GCMarkersGroupItem newItemSelection) {
                for (GCMarkersGroupItem groupItem : groupList.getItems()) {
                    groupItem.showControls(false);
                }
                if (isItemSelected()) {
                    if(!getSelectedItem().getGroupName().equals("All"))
                        shareGroup.setDisabled(false);
                    else
                        shareGroup.setDisabled(true);
                    currentGroup = getSelectedItem().getData();
                    getSelectedItem().showControls(true);
                    populateMarkersList(getSelectedItem().getData());
                } else {
                    shareGroup.setDisabled(true);
                }
            }

            @Override
            protected void onDoubleClick(GCMarkersGroupItem item) {
                if (!item.getGroupName().equals("All")) {
                    for (int i = 0; i < groupList.size(); i++) {
                        if (item != groupList.get(i)) {
                            groupList.get(i).disableEditMode();
                            groupList.get(i).setDisabled(true);
                        }
                    }
                    addGroupBtn.setDisabled(true);
                    item.toggleEditMode();
                    super.onDoubleClick(item);
                }
            }
        };
        markerList = new GCList<GCMarkerListItem>(layer, 1, 5, 1, true);
        markersHandler = new GCListItemActionHandler<GCMarkerListItem>() {
            @Override
            protected void onSelectionUpdate(GCMarkerListItem lastSelectedItem) {
                for (GCMarkerListItem item : markerList.getItems()) {
                    item.showControls(false);
                }
                if (isItemSelected()) {
                    shareMarker.setDisabled(false);
                    getSelectedItem().showControls(true);
                }else{
                    shareMarker.setDisabled(true);
                }
            }
        };
        markersContainer = new H5EScrollablePane(layer);
        markersTable = markersContainer.getContent();
        markersContainer.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        Table leftContent = new Table();
        Table rightContent = new Table();
        leftContent.add(groupsContainer).grow();
        rightContent.add(markersContainer).grow().colspan(2);
        add(leftContent).grow().padRight(100).left();
        add(rightContent).grow().left().padLeft(-100);
        addGroupBtn = new H5EButton("", layer);
        addGroupBtn.add(new H5ELabel(" + Add group", layer)).growX();
        addGroupBtn.addButtonListener(() -> {
            boolean found = true;
            int groupId = 1;
            while (found) {
                found = false;
                for (GCMarkersGroupItem group : groupList.getItems()) {
                    if (group.getGroupName().equals("New group " + groupId)) {
                        found = true;
                    }
                }
                if (found)
                    groupId++;
            }
            addGroup("New group " + groupId);
        });

        rightContent.row();
        leftContent.row();
        addGroupBtn.setStyle(layer.getEngine().getSkin().get("gc-list-button", ImageTextButton.ImageTextButtonStyle.class));
        groupsContainer.add(groupList).top().growX();
        groupsContainer.getContent().top();
        markersContainer.add(markerList).grow();
        groupsTable.row();
        groupsTable.add(addGroupBtn).left().top().growX();
        shareGroup = new H5EButton("Share group", layer);
        shareGroup.addButtonListener(() -> {
            onShareGroup.call(currentGroup);
        });
        shareMarker = new H5EButton("Share marker", layer);
        shareMarker.addButtonListener(()->{
            onShareMarker.call(markersHandler.getSelectedItem().getData());
        });
        shareMarker.setDisabled(true);
        rightContent.add().growX();
        rightContent.add(shareMarker).pad(5).right();
        leftContent.add(shareGroup).pad(5).left();
    }

    public void processData(List<MapMarkerGroupData> data) {
        groupList.clear();
        markerList.clear();
        MapMarkerGroupData allGroup = new MapMarkerGroupData("all", "All", new ArrayList<>());
        for (MapMarkerGroupData groupData : data) {
            allGroup.markers.addAll(groupData.markers);
        }
        GCMarkersGroupItem allGroupItem = processGroup(allGroup);
        groupList.addItem(allGroupItem);
        allGroupItem.toggle();
        for (MapMarkerGroupData groupData : data) {
            groupList.addItem(processGroup(groupData));
        }
    }

    public void renameGroup(String oldName, GCMarkersGroupItem renamedItem) {
        if (!oldName.equals(renamedItem.getGroupName())) {
            boolean foundSame = false;
            for (GCMarkersGroupItem groupItem : groupList.getItems()) {
                if (groupItem != renamedItem && groupItem.getGroupName().equals(renamedItem.getGroupName())) {
                    foundSame = true;
                }
                if (foundSame) {
                    renamedItem.setName(oldName);
                }
            }
        }
        populateMarkersList(renamedItem.getData());
        for (GCMarkersGroupItem group : groupList.getItems()) {
            group.setDisabled(false);
        }
        renamedItem.showControls(true);
        addGroupBtn.setDisabled(false);
    }

    public GCMarkersGroupItem processGroup(MapMarkerGroupData data) {
        GCMarkersGroupItem groupItem = new GCMarkersGroupItem(layer, groupHandler, data, this::renameGroup, (item) -> {
            groupList.removeItem(item);
            populateMarkersList(groupList.get(0).getData());
            groupHandler.clearSelection();
        });
        DragAndDrop.Target target = new DragAndDrop.Target(groupItem) {
            @Override
            public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float v, float v1, int i) {
                groupItem.setStyle(groupItem.getOverStyle());
                return true;
            }

            @Override
            public void reset(DragAndDrop.Source source, DragAndDrop.Payload payload) {
                groupItem.setStyle(groupItem.getNormalStyle());
                super.reset(source, payload);
            }

            @Override
            public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float v, float v1, int i) {
                if (!data.markers.contains(((GCMarkerListItem) source.getActor()).getData())) {
                    data.markers.add(((GCMarkerListItem) source.getActor()).getData());
                }
                populateMarkersList(currentGroup);
            }
        };
        dragAndDrop.addTarget(target);
        return groupItem;
    }

    public void addGroup(String name) {
        MapMarkerGroupData newGroup = new MapMarkerGroupData(name, name, new ArrayList<MapMarkerData>());
        GCMarkersGroupItem groupItem = processGroup(newGroup);
        groupList.addItem(groupItem);
    }

    public void deleteMarker(GCMarkerListItem item, boolean fromAll) {
        if(!currentGroup.name.equals("All")) {
            currentGroup.markers.remove(item.getData());
            if (fromAll) {
                for (GCMarkersGroupItem group : groupList.getItems()) {
                    group.getData().markers.remove(item.getData());
                }
            }
        }
        populateMarkersList(currentGroup);
    }

    public void populateMarkersList(MapMarkerGroupData group) {
        markersTable.clear();
        markersTable.add(markerList).grow();
        markersContainer.setFlickScroll(true);
        markerList.clear();
        currentGroup = group;
        for (MapMarkerData entry : group.markers) {
            GCMarkerListItem item = new GCMarkerListItem(layer, markersHandler, entry, this::deleteMarker);
            DragAndDrop.Source source = new DragAndDrop.Source(item) {
                @Override
                public DragAndDrop.Payload dragStart(InputEvent inputEvent, float v, float v1, int i) {
                    DragAndDrop.Payload payload = new DragAndDrop.Payload();
                    H5EIcon icon = H5EIcon.fromSpriteType(layer, entry.icon);
                    icon.setStage(null);
                    payload.setDragActor(icon);
                    dragAndDrop.setDragActorPosition(icon.getWidth() / 2, -icon.getHeight() / 2);
                    return payload;
                }
            };
            dragAndDrop.addSource(source);
            markerList.addItem(item);
        }
        if (markerList.size() == 0) {
            H5ELabel noMarkers = new H5ELabel("No markers", layer);
            noMarkers.setAlignment(Alignment.LEFT.getValue());
            noMarkers.setColor(Color.valueOf("#CCCCCC"));
            markersTable.add(noMarkers).grow();
        }
    }
}


