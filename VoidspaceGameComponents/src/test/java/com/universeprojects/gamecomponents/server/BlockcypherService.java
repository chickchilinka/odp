package com.universeprojects.gamecomponents.server;

import com.badlogic.gdx.utils.Timer;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.vsdata.shared.CryptoCurrency;
import com.universeprojects.vsdata.shared.CryptoTransactionData;
import com.universeprojects.vsdata.shared.CryptoTransactionInput;
import com.universeprojects.vsdata.shared.CryptoTransactionOutput;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class BlockcypherService implements BlockchainService {
    public CryptoCurrency type;

    public BlockcypherService(CryptoCurrency type) {
        this.type = type;
    }

    public List<CryptoTransactionData> getTransactions(String address, int count) {
        List<CryptoTransactionData> data = new ArrayList<>();
        String url = "https://api.blockcypher.com/v1/" + type.getText() + "/main/addrs/" + address + "/full?limit=" + count;
        String received = "";
        try {
            URL query = new URL(url);
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(query.openStream(), "UTF-8"))) {
                JsonObject list;
                list = JsonParser.parseReader(reader).getAsJsonObject();
                JsonArray array = list.getAsJsonArray("txs");
                for (Object entry : array) {
                    JsonObject transaction = (JsonObject) entry;
                    String dateTime = transaction.get("confirmed").getAsString().replace('T', ' ');
                    dateTime = dateTime.substring(0, dateTime.length() - 1);
                    data.add(new CryptoTransactionData(type, transaction.get("total").getAsBigDecimal().divide(new BigDecimal(100000000d)).doubleValue(), transaction.get("total").getAsString(), getInputs(transaction), getOutputs(transaction), transaction.get("hash").getAsString(), dateTime));
                    System.err.println("Inserted new transaction data");
                }
            }
        } catch (MalformedURLException exception) {
            return null;
        } catch (IOException exception) {
            return null;
        }
        return data;
    }

    public void getTransactions(String address, int count, Callable1Args<List<CryptoTransactionData>> onLoad) {
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                List<CryptoTransactionData> result = getTransactions(address, count);
                onLoad.call(result);
            }
        }, 0);
    }

    public List<CryptoTransactionData> getTransactions(String address) {
        return getTransactions(address, 10);
    }

    private CryptoTransactionInput[] getInputs(JsonObject transaction) {
        System.err.println(transaction.getAsJsonArray("inputs").get(0).isJsonObject());
        JsonArray inputsList = transaction.getAsJsonArray("inputs");
        CryptoTransactionInput[] inputs = new CryptoTransactionInput[inputsList.size()];
        for (int i = 0; i < inputs.length; i++) {
            double amount = 0;
            String amountText = "0";
            if (inputsList.get(i).getAsJsonObject().keySet().contains("output_value")) {
                amount = inputsList.get(i).getAsJsonObject().get("output_value").getAsBigDecimal().divide(new BigDecimal(100000000d)).doubleValue();
                amountText = inputsList.get(i).getAsJsonObject().get("output_value").getAsString();
            }
            String[] addresses = new String[inputsList.get(i).getAsJsonObject().getAsJsonArray("addresses").size()];
            for (int j = 0; j < addresses.length; j++) {
                addresses[j] = inputsList.get(i).getAsJsonObject().getAsJsonArray("addresses").get(j).getAsString();
            }
            long age = 0;
            if (inputsList.get(i).getAsJsonObject().keySet().contains("age")) {
                age = inputsList.get(i).getAsJsonObject().get("age").getAsLong();
            }
            inputs[i] = new CryptoTransactionInput(amount, amountText, addresses, age);
        }
        return inputs;
    }

    private CryptoTransactionOutput[] getOutputs(JsonObject transaction) {
        JsonArray outputsList = transaction.getAsJsonArray("outputs");
        CryptoTransactionOutput[] outputs = new CryptoTransactionOutput[outputsList.size()];
        for (int i = 0; i < outputs.length; i++) {
            double amount = outputsList.get(i).getAsJsonObject().get("value").getAsBigDecimal().divide(new BigDecimal(100000000d)).doubleValue();
            String amountText = outputsList.get(i).getAsJsonObject().get("value").getAsString();
            String[] addresses = new String[outputsList.get(i).getAsJsonObject().getAsJsonArray("addresses").size()];
            for (int j = 0; j < addresses.length; j++) {
                addresses[j] = outputsList.get(i).getAsJsonObject().getAsJsonArray("addresses").get(j).getAsString();
            }
            String spentBy = "";
            if (outputsList.get(i).getAsJsonObject().keySet().contains("spent_by")) {
                spentBy = outputsList.get(i).getAsJsonObject().get("spent_by").getAsString();
            }
            outputs[i] = new CryptoTransactionOutput(amount, amountText, addresses, spentBy);
        }
        return outputs;
    }
}
