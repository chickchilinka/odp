package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.dialogs.GCEnergyNodeControlDialog;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class EnergySystemControlDemo extends Demo {

    private final Logger log = Logger.getLogger(EnergySystemControlDemo.class);

    private final H5ELayer layer;
    private final GCSimpleWindow controlWindow;
    private final H5EButton btnAddSource;
    private final H5EButton btnRemoveSource;
    private final H5EButton btnAddConsumer;
    private final H5EButton btnRemoveConsumer;

    private final GCEnergyNodeControlDialog dialog;

    private final static Map<String, String> SOURCES = new HashMap<>();

    static {
        SOURCES.put("obj-001", "Main Generator");
        SOURCES.put("obj-002", "Backup Generator");
        SOURCES.put("obj-003", "AAA battery in the corner");
    }

    private int sourceCount = 0;
    private int consumerCount = 0;

    private final static Map<String, String> CONSUMERS = new HashMap<>();

    static {
        CONSUMERS.put("obj-101", "Energy Shield");
        CONSUMERS.put("obj-102", "Plasma gun");
        CONSUMERS.put("obj-103", "Laser cannon L");
        CONSUMERS.put("obj-104", "Laser cannon R");
    }

    public EnergySystemControlDemo(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);
        layer.addListener(new InputListener() {
            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (keycode == Input.Keys.I) {
                    removeSource();
                } else if (keycode == Input.Keys.O) {
                    addSource();
                } else if (keycode == Input.Keys.K) {
                    removeConsumer();
                } else if (keycode == Input.Keys.L) {
                    addConsumer();
                }
                return false;
            }
        });

        controlWindow = new GCSimpleWindow(layer, "energy-system-control-demo", "Energy System Control Demo");
        controlWindow.onClose.registerHandler(this::cleanup);
        controlWindow.positionProportionally(.5f, .15f);

        controlWindow.row().padTop(20).padBottom(5).growX();
        btnAddSource = controlWindow.add(createButton("Source +")).getActor();
        btnAddConsumer = controlWindow.add(createButton("Output +")).getActor();
        controlWindow.row().padTop(5).padBottom(20).growX();
        btnRemoveSource = controlWindow.add(createButton("Source -")).getActor();
        btnRemoveConsumer = controlWindow.add(createButton("Output -")).getActor();

        dialog = new GCEnergyNodeControlDialog(layer) {
            @Override
            protected void onDismiss() {
                log.info("Dialog dismissed by user");
            }

            @Override
            protected void onSwitchOnOff(boolean newState) {
                log.info("Switch state changed to: " + (newState ? "ON" : "OFF"));
            }

            @Override
            protected void onOpenBtn(String uid) {
                log.info("Open button pressed on item with UID: " + uid);
            }
        };

        dialog.positionProportionally(.5f, .4f);
        dialog.setName("Main Energy Storage");
        dialog.setCapacity(10000);
        dialog.setEnergyLevel(5578);

        addSource();
        addSource();

        addConsumer();
        addConsumer();
        addConsumer();
    }

    private H5EButton createButton(String caption) {
        H5EButton btn = new H5EButton(caption, layer);
        btn.getLabel().setFontScale(0.6f);
        btn.addButtonListener(() -> onButton(btn));
        return btn;
    }

    private void onButton(H5EButton button) {
        if (button == btnAddSource) {
            addSource();
        } else if (button == btnRemoveSource) {
            removeSource();
        } else if (button == btnAddConsumer) {
            addConsumer();
        } else if (button == btnRemoveConsumer) {
            removeConsumer();
        }
    }

    private void addSource() {
        if (sourceCount < SOURCES.size()) {
            dialog.setSources(subset(SOURCES, ++sourceCount));
        }
    }

    private void removeSource() {
        if (sourceCount > 0) {
            dialog.setSources(subset(SOURCES, --sourceCount));
        }
    }

    private void addConsumer() {
        if (consumerCount < CONSUMERS.size()) {
            dialog.setConsumers(subset(CONSUMERS, ++consumerCount));
        }
    }

    private void removeConsumer() {
        if (consumerCount > 0) {
            dialog.setConsumers(subset(CONSUMERS, --consumerCount));
        }
    }

    private Map<String, String> subset(Map<String, String> data, int size) {
        Map<String, String> subset = new HashMap<>();
        Iterator<String> iterator = data.keySet().iterator();
        while (size-- > 0) {
            String key = iterator.next();
            String value = data.get(key);
            subset.put(key, value);
        }
        return subset;
    }

    @Override
    public void open() {
        controlWindow.open();
        dialog.open();
    }

    @Override
    public void close() {
        controlWindow.close();
    }

    private void cleanup() {
        dialog.close();
    }

    @Override
    public boolean isOpen() {
        return dialog.isOpen();
    }

}
