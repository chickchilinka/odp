package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCSlider;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.BlendingMode;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

public class BlendingModeDemo extends Demo {

    public static final int INPUT_WIDTH = 200;
    private final H5ELayer layer;

    private final GCSimpleWindow window;

    final ChangeListener inputChangeListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            handleInput();
        }
    };
    final ChangeListener selectBoxChangeListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            handleSelect();
        }
    };
    final ChangeListener sliderChangeListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            handleSlider();
        }
    };

    final BlendingValue defaultSrcColor = BlendingValue.GL_SRC_ALPHA;
    final BlendingValue defaultDstColor = BlendingValue.GL_ONE_MINUS_SRC_ALPHA;
    final BlendingValue defaultSrcAlpha = BlendingValue.GL_SRC_ALPHA;
    final BlendingValue defaultDstAlpha = BlendingValue.GL_ONE_MINUS_SRC_ALPHA;

    final String defaultSprite1SpriteType = "images/ships/Artist2Ship1.png";
    final String defaultSprite2SpriteType = "images/effects/infected-effect1.jpg";
    final String defaultSprite3SpriteType = "base-silo1";

    @SuppressWarnings("unused")
    enum BlendingValue {
        GL_ONE(GL20.GL_ONE),
        GL_ZERO(GL20.GL_ZERO),
        GL_SRC_ALPHA(GL20.GL_SRC_ALPHA),
        GL_SRC_COLOR(GL20.GL_SRC_COLOR),
        GL_DST_ALPHA(GL20.GL_DST_ALPHA),
        GL_DST_COLOR(GL20.GL_DST_COLOR),
        GL_ONE_MINUS_SRC_ALPHA(GL20.GL_ONE_MINUS_SRC_ALPHA),
        GL_ONE_MINUS_SRC_COLOR(GL20.GL_ONE_MINUS_SRC_COLOR),
        GL_ONE_MINUS_DST_ALPHA(GL20.GL_ONE_MINUS_DST_ALPHA),
        GL_ONE_MINUS_DST_COLOR(GL20.GL_ONE_MINUS_DST_COLOR)
        ;
        public final int value;

        BlendingValue(int value) {
            this.value = value;
        }
    }

    private final SelectBox<BlendingValue> selectSprite1SrcColor, selectSprite1DstColor, selectSprite1SrcAlpha, selectSprite1DstAlpha;
    private final SelectBox<BlendingValue> selectSprite2SrcColor, selectSprite2DstColor, selectSprite2SrcAlpha, selectSprite2DstAlpha;
    private final SelectBox<BlendingValue> selectSprite3SrcColor, selectSprite3DstColor, selectSprite3SrcAlpha, selectSprite3DstAlpha;
    private final H5EInputBox inputSprite1Type, inputSprite2Type, inputSprite3Type;
    private final GCSlider sprite1TransparencySlider, sprite2TransparencySlider, sprite3TransparencySlider;

    private H5ESprite sprite1, sprite2, sprite3;

    public BlendingModeDemo(H5ELayer layer) {
        this.layer = layer;

        window = new GCSimpleWindow(layer, "switch-demo", "Blending Mode Demo");
        window.positionProportionally(.5f, .15f);

        window.addH2("SpriteType:").padRight(10);
        inputSprite1Type = window.addInputBox(INPUT_WIDTH).fill().getActor();
        inputSprite1Type.setText(defaultSprite1SpriteType);
        inputSprite1Type.addListener(inputChangeListener);

        sprite1TransparencySlider = window.add(new GCSlider(layer)).colspan(2).fill().getActor();
        sprite1TransparencySlider.setRange(0, 100, 1);
        sprite1TransparencySlider.setValue(100);
        sprite1TransparencySlider.addListener(sliderChangeListener);

        window.row();
        window.addH2("Color:");
        selectSprite1SrcColor = window.add(selectBox(defaultSrcColor)).getActor();
        selectSprite1DstColor = window.add(selectBox(defaultDstColor)).getActor();
        window.row();
        window.addH2("Alpha:");
        selectSprite1SrcAlpha = window.add(selectBox(defaultSrcAlpha)).getActor();
        selectSprite1DstAlpha = window.add(selectBox(defaultDstAlpha)).getActor();
        window.row().padTop(20);

        window.addH2("SpriteType:");
        inputSprite2Type = window.addInputBox(INPUT_WIDTH).fill().getActor();
        inputSprite2Type.setText(defaultSprite2SpriteType);
        inputSprite2Type.addListener(inputChangeListener);
        sprite2TransparencySlider = window.add(new GCSlider(layer)).colspan(2).fill().getActor();
        sprite2TransparencySlider.setRange(0, 100, 1);
        sprite2TransparencySlider.setValue(100);
        sprite2TransparencySlider.addListener(sliderChangeListener);
        window.row();
        window.addH2("Color:");
        selectSprite2SrcColor = window.add(selectBox(defaultSrcColor)).getActor();
        selectSprite2DstColor = window.add(selectBox(defaultDstColor)).getActor();
        window.row();
        window.addH2("Alpha:");
        selectSprite2SrcAlpha = window.add(selectBox(defaultSrcAlpha)).getActor();
        selectSprite2DstAlpha = window.add(selectBox(defaultDstAlpha)).getActor();
        window.row().padTop(20);

        window.addH2("SpriteType:");
        inputSprite3Type = window.addInputBox(INPUT_WIDTH).fill().getActor();
        inputSprite3Type.setText(defaultSprite3SpriteType);
        inputSprite3Type.addListener(inputChangeListener);
        sprite3TransparencySlider = window.add(new GCSlider(layer)).colspan(2).fill().getActor();
        sprite3TransparencySlider.setRange(0, 100, 1);
        sprite3TransparencySlider.setValue(100);
        sprite3TransparencySlider.addListener(sliderChangeListener);
        window.row();
        window.addH2("Color:");
        selectSprite3SrcColor = window.add(selectBox(defaultSrcColor)).getActor();
        selectSprite3DstColor = window.add(selectBox(defaultDstColor)).getActor();
        window.row();
        window.addH2("Alpha:");
        selectSprite3SrcAlpha = window.add(selectBox(defaultSrcAlpha)).getActor();
        selectSprite3DstAlpha = window.add(selectBox(defaultDstAlpha)).getActor();
        window.row();

        window.add(ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Reset").withButtonListener(this::reset).build()).fill();
        window.row();

        H5EStack stack = new H5EStack();
        window.add(stack).colspan(3).padTop(20);
        sprite1 = new H5ESprite(layer, getSpriteType(inputSprite1Type));
        sprite1.setBlendingMode(getBlendingMode(selectSprite1SrcColor, selectSprite1DstColor, selectSprite1SrcAlpha, selectSprite1DstAlpha));
        stack.add(sprite1);
        sprite2 = new H5ESprite(layer, inputSprite2Type.getText());
        sprite1.setBlendingMode(getBlendingMode(selectSprite2SrcColor, selectSprite2DstColor, selectSprite2SrcAlpha, selectSprite2DstAlpha));
        stack.add(sprite2);
        sprite3 = new H5ESprite(layer, inputSprite3Type.getText());
        sprite1.setBlendingMode(getBlendingMode(selectSprite3SrcColor, selectSprite3DstColor, selectSprite3SrcAlpha, selectSprite3DstAlpha));
        stack.add(sprite3);

        close();
    }

    private void reset() {
        inputSprite1Type.setText(defaultSprite1SpriteType);
        inputSprite2Type.setText(defaultSprite2SpriteType);
        inputSprite3Type.setText(defaultSprite3SpriteType);

        selectSprite1SrcColor.setSelected(defaultSrcColor);
        selectSprite1SrcAlpha.setSelected(defaultSrcAlpha);
        selectSprite1DstColor.setSelected(defaultDstColor);
        selectSprite1DstAlpha.setSelected(defaultDstAlpha);

        selectSprite2SrcColor.setSelected(defaultSrcColor);
        selectSprite2SrcAlpha.setSelected(defaultSrcAlpha);
        selectSprite2DstColor.setSelected(defaultDstColor);
        selectSprite2DstAlpha.setSelected(defaultDstAlpha);

        selectSprite3SrcColor.setSelected(defaultSrcColor);
        selectSprite3SrcAlpha.setSelected(defaultSrcAlpha);
        selectSprite3DstColor.setSelected(defaultDstColor);
        selectSprite3DstAlpha.setSelected(defaultDstAlpha);

        sprite1TransparencySlider.setValue(100);
        sprite2TransparencySlider.setValue(100);
        sprite3TransparencySlider.setValue(100);
    }

    private SelectBox<BlendingValue> selectBox(BlendingValue defaultValue) {
        final SelectBox<BlendingValue> selectBox = new SelectBox<>(layer.getEngine().getSkin());
        selectBox.setItems(BlendingValue.values());
        selectBox.setSelected(defaultValue);
        selectBox.addListener(selectBoxChangeListener);
        return selectBox;
    }

    private void handleSelect() {
        sprite1.setBlendingMode(getBlendingMode(selectSprite1SrcColor, selectSprite1DstColor, selectSprite1SrcAlpha, selectSprite1DstAlpha));
        sprite2.setBlendingMode(getBlendingMode(selectSprite2SrcColor, selectSprite2DstColor, selectSprite2SrcAlpha, selectSprite2DstAlpha));
        sprite3.setBlendingMode(getBlendingMode(selectSprite3SrcColor, selectSprite3DstColor, selectSprite3SrcAlpha, selectSprite3DstAlpha));
    }

    private void handleSlider() {
        sprite1.getColor().a = sprite1TransparencySlider.getValue() / 100f;
        sprite2.getColor().a = sprite2TransparencySlider.getValue() / 100f;
        sprite3.getColor().a = sprite3TransparencySlider.getValue() / 100f;
    }

    private void handleInput() {
        updateSpriteType(inputSprite1Type, sprite1);
        updateSpriteType(inputSprite2Type, sprite2);
        updateSpriteType(inputSprite3Type, sprite3);
    }

    private void updateSpriteType(H5EInputBox inputBox, H5ESprite sprite) {
        final H5ESpriteType spriteType = getSpriteType(inputBox);
        if(spriteType != null) {
            sprite.setSpriteType(spriteType);
        }
    }

    private H5ESpriteType getSpriteType(H5EInputBox inputBox) {
        String key = inputBox.getText();
        return (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(key);
    }

    private BlendingMode getBlendingMode(SelectBox<BlendingValue> srcColInput, SelectBox<BlendingValue> dstColInput, SelectBox<BlendingValue> srcAlInput, SelectBox<BlendingValue> dstAlInput) {
        return new BlendingMode(
            srcColInput.getSelected().value,
            dstColInput.getSelected().value,
            srcAlInput.getSelected().value,
            dstAlInput.getSelected().value
            );
    }

    @Override
    public void open() {
        window.open();
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }

}
