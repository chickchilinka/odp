package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.utils.Timer;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.vsdata.shared.CategoryData;
import com.universeprojects.vsdata.shared.CurrencyData;
import com.universeprojects.vsdata.shared.PassengerData;
import com.universeprojects.vsdata.shared.CategorizedStoreItemData;
import com.universeprojects.vsdata.shared.ItemTreeData;
import com.universeprojects.gamecomponents.client.dialogs.GCStationDialog;
import com.universeprojects.gamecomponents.client.dialogs.station.store.*;
import com.universeprojects.gamecomponents.client.dialogs.inventory.*;

import java.util.ArrayList;
import java.util.List;

public class StationDialogDemo extends Demo {

    private H5ELayer layer;
    private GCStationDialog dialog;
    private List<GCStoreListItemData> items;
    private List<CurrencyData> playerBalance;

    public StationDialogDemo(H5ELayer layer) {
        this.layer = layer;
    }

    //<editor-fold desc="Store demo work">
    public void demoStore() {
        ItemTreeData treeData = demoCategories();
        List<GCStoreListItemData> items = demoItems();
        for (GCStoreListItemData item : items) {
            treeData.items.add(item.getData());
        }
        playerBalance = new ArrayList<CurrencyData>();
        playerBalance.add(new CurrencyData("doge coin", "GUI/ui2/button-bar-buttons/coins1.png", 2000));
        playerBalance.add(new CurrencyData("gold coin", "icons/gold-coil1.png", 100));
        dialog.processStoreData(treeData, items, playerBalance, (data, count) -> {
            System.err.println("Buyed " + data.getData().name + "x" + String.valueOf(count));
            ((GCSimpleInventoryItemLike) data.getItem()).setQuantity(((int) data.getItem().getQuantity()) - count);
            for (CurrencyData curBalance : this.playerBalance) {
                if (curBalance.name.equals(data.getData().price.name)) {
                    curBalance.amount -= count * data.getData().price.amount;
                }
            }
            dialog.updateBalance(this.playerBalance);
            dialog.updateStoreItem(data);
        });
    }

    public List<GCStoreListItemData> demoItems() {
        items = new ArrayList<GCStoreListItemData>();
        for (int i = 0; i <= 2; i++) {
            items.add(new GCStoreListItemData(String.valueOf(i), "doge coin", "GUI/ui2/button-bar-buttons/coins1.png", (i + 1) * 500, "seller #" + String.valueOf(i + 1), "6",
                    new GCSimpleInventoryItemLike("1", "Harbor g7 1a", "ships/harbor/harbor-g7-class1a.png", (i + 1), Rarity.RARE)));
        }
        items.add(new GCStoreListItemData("3", "doge coin", "GUI/ui2/button-bar-buttons/coins1.png", 1000, "chickchilinka", "6",
                new GCSimpleInventoryItemLike("2", "Pass ship miner", "ships/pass/pass-ship-class1-miner1.png", 1, Rarity.COMMON)));
        items.add(new GCStoreListItemData("4", "gold coin", "icons/gold-coil1.png", 1000, "Nik", "7",
                new GCSimpleInventoryItemLike("3", "Some tool", "slots/character-tool1.png", 1, Rarity.JUNK)));
        return items;
    }

    public ItemTreeData<CategorizedStoreItemData> demoCategories() {
        CategoryData ideas = new CategoryData("1", "Ideas", null);
        CategoryData ships = new CategoryData("4", "Ships", null);
        CategoryData a = new CategoryData("5", "A", null);
        CategoryData b = new CategoryData("6", "B", null);
        CategoryData tools = new CategoryData("7", "Tools", null);
        ItemTreeData<CategorizedStoreItemData> treeData = new ItemTreeData<CategorizedStoreItemData>();
        treeData.addCategoryToId(treeData.category, ideas, "0");
        treeData.addCategoryToId(treeData.category, ships, "1");
        treeData.addCategoryToId(treeData.category, tools, "1");
        treeData.addCategoryToId(treeData.category, a, "4");
        treeData.addCategoryToId(treeData.category, b, "4");
        return treeData;
    }
    //</editor-fold>

    //<editor-fold desc="Passengers tab demo work">
    public void demoPassengers() {
        List<PassengerData> passengerDataList = new ArrayList<PassengerData>();
        passengerDataList.add(new PassengerData("0", "Some character 1", "some nickname 1", "icons/hello-kitty-icon.png"));
        passengerDataList.add(new PassengerData("1", "Some character 2", "some nickname 2", "map-indicators/character-indicator1.png"));
        dialog.processPassengersData(passengerDataList, false, (btn) -> {
            if (btn == 1) {
                System.err.println("Lock/Unlock cabin button pressed");
                dialog.setCabinLockMode(true);
            }
        }, (passenger, option) -> {
            if (option == 0) {
                System.err.println("Selected:" + passenger.characterName);
            } else if (option == 1) {
                System.err.println("Trade:" + passenger.characterName);
            }
        });
    }
    //</editor-fold>

    public void demoStation() {
        List<GCStoreListItemData> data = new ArrayList<GCStoreListItemData>();
        data.add(new GCStoreListItemData("3", "doge coin", "GUI/ui2/button-bar-buttons/coins1.png", 1000, "", "6",
                new GCSimpleInventoryItemLike("2", "Some fuel 1", "ships/pass/pass-ship-class1-miner1.png", 1, Rarity.COMMON)));
        data.add(new GCStoreListItemData("3", "doge coin", "GUI/ui2/button-bar-buttons/coins1.png", 500, "", "6",
                new GCSimpleInventoryItemLike("2", "Some fuel 2", "ships/harbor/harbor-g7-class1a.png", 1, Rarity.COMMON)));
        CurrencyData price = new CurrencyData("doge coin", "GUI/ui2/button-bar-buttons/coins1.png", 1000);
        dialog.processUpPanel(data, price, (fuel) -> {
            System.err.println("Refuel with " + fuel.getData().name);
        }, () -> {
            System.err.println("Recharge");
        }, () -> {
            System.err.println("Undock");
        });

    }

    @Override
    public void open() {
        dialog = new GCStationDialog(layer);
        demoStation();
        demoPassengers();
        dialog.open(false);
        Timer.instance().scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                demoStore();
            }
        }, 3f);
        Timer.instance().scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                items.set(1, new GCStoreListItemData("1", "doge coin", "GUI/ui2/button-bar-buttons/coins1.png", 1000, "seller #2", "6",
                        new GCSimpleInventoryItemLike("2", "Pass ship miner", "ships/pass/pass-ship-class1-miner1.png", 3, Rarity.COMMON)));
                items.get(4).getData().price = new CurrencyData("doge coin", "GUI/ui2/button-bar-buttons/coins1.png", 500);
                items.get(4).getData().name = "aaa";
                items.get(4).setItem(new GCSimpleInventoryItemLike("3", "Some tool", "slots/character-tool1.png", 3, Rarity.JUNK));
                items.get(2).setItem(new GCSimpleInventoryItemLike("1", "Harbor g7 1a", "ships/harbor/harbor-g7-class1a.png", 0, Rarity.RARE));
                dialog.updateStoreItem(items.get(1));
                dialog.updateStoreItem(items.get(2));
                dialog.updateStoreItem(items.get(4));
            }
        }, 8f);
        Timer.instance().scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                playerBalance.get(0).amount = 500;
                dialog.updateBalance(playerBalance);
            }
        }, 12f);

    }

    @Override
    public void close() {
        dialog.close();
    }

    @Override
    public boolean isOpen() {
        if (dialog != null)
            return dialog.isOpen();
        else
            return false;
    }

}