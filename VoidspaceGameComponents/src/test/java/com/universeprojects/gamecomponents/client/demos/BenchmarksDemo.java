package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class BenchmarksDemo extends Demo {

    private final Logger log = Logger.getLogger(BenchmarksDemo.class);

    public BenchmarksDemo() {

    }

    @Override
    public void open() {
        // TODO: introduce window
    }

    @Override
    public void close() {
        // TODO: introduce window
    }

    @Override
    public boolean isOpen() {
        // TODO: introduce window
        return false;
    }

    public void run() {
        int NUM_VALUES = 100000;

        RetardedlyPrimitiveTimer t = new RetardedlyPrimitiveTimer();
        Double[] keys = new Double[NUM_VALUES];
        Double[] values = new Double[NUM_VALUES];
        log.info("Double[]-allocation: " + t.elapsed() / 2);
        for (int i = 0; i < NUM_VALUES; i++) {
            keys[i] = Math.random();
            values[i] = Math.random();
        }

        Map<Double, Double> hashMap = new HashMap<>();
        Map<Double, Double> linkedHashMap = new LinkedHashMap<>();
        Map<Double, Double> treeMap = new TreeMap<>();

        t.reset();
        for (int i = 0; i < NUM_VALUES; i++) {
            treeMap.put(keys[i], values[i]);
        }
        log.info("TreeMap-put: " + t.elapsed());
        log.info("TreeMap-size: " + treeMap.size());

        t.reset();
        for (int i = 0; i < NUM_VALUES; i++) {
            linkedHashMap.put(keys[i], values[i]);
        }
        log.info("LinkedHashMap-put: " + t.elapsed());
        log.info("LinkedHashMap-size: " + linkedHashMap.size());

        t.reset();
        for (int i = 0; i < NUM_VALUES; i++) {
            hashMap.put(keys[i], values[i]);
        }
        log.info("HashMap-put: " + t.elapsed());
        log.info("HashMap-size: " + hashMap.size());

        @SuppressWarnings("unused")
        double d = 0;

        t.reset();
        Object[] arr = treeMap.keySet().toArray();
        List<Object> list = Arrays.asList(arr);
        log.info("TreeMap-keyListExtraction: " + t.elapsed());

        t.reset();
        for (Object o : list) {
            d += (Double) o;
        }
        log.info("list-iteration: " + t.elapsed());

        t.reset();
        for (Double key : treeMap.keySet()) {
            d += key;
        }
        log.info("TreeMap-keyIterator: " + t.elapsed());

        t.reset();
        for (Double key : linkedHashMap.keySet()) {
            d += key;
        }
        log.info("LinkedHashMap-keyIterator: " + t.elapsed());

        t.reset();
        for (Double key : hashMap.keySet()) {
            d += key;
        }
        log.info("HashMap-keyIterator: " + t.elapsed());


        t.reset();
        for (Double key : treeMap.values()) {
            d += key;
        }
        log.info("TreeMap-valueIterator: " + t.elapsed());

        t.reset();
        for (Double key : linkedHashMap.values()) {
            d += key;
        }
        log.info("LinkedHashMap-valueIterator: " + t.elapsed());

        t.reset();
        for (Double key : hashMap.values()) {
            d += key;
        }
        log.info("HashMap-valueIterator: " + t.elapsed());
    }

    private static class RetardedlyPrimitiveTimer {
        private long startTime = System.currentTimeMillis();

        void reset() {
            startTime = System.currentTimeMillis();
        }

        long elapsed() {
            return System.currentTimeMillis() - startTime;
        }
    }

}
