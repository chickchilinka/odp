package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.selection.GCSelectionMenu;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

public class SelectionMenuDemo extends Demo {

    private final GCSimpleWindow controlWindow;

    private final GCSelectionMenu menu;


    public SelectionMenuDemo(H5ELayer layer) {

        controlWindow = new GCSimpleWindow(layer, "selection-menu-demo", "Selection Menu Demo");
        controlWindow.onClose.registerHandler(this::cleanup);
        controlWindow.positionProportionally(.5f, .85f);
        controlWindow.defaults().left();

        menu = new GCSelectionMenu(layer, 300, 350);
        layer.addActorToTop(menu);
        menu.setX(650);
        menu.setY(250);
        menu.hide();

        final H5EButton btnClose = controlWindow.addLargeButton("Close menu").getActor();
        btnClose.addButtonListener(menu::hide);

        final H5EButton btnInRange = controlWindow.addLargeButton("Open / in range").getActor();
        btnInRange.addButtonListener(() -> menu.show(true));

        final H5EButton btnOutOfRange = controlWindow.addLargeButton("Open / out of range").getActor();
        btnOutOfRange.addButtonListener(() -> menu.show(false));


        populateMenu(menu);
    }

    public static void populateMenu(GCSelectionMenu menu) {
        menu.addItem("/Inspect");

        menu.addItem("/Ship Hull/Inspect");

        menu.addItem("/Cargo Hold/Inspect");
        menu.addItem("/Cargo Hold/Show Contents");

        menu.addItem("/Energy Generators/Fusion Generator 1/Inspect");
        menu.addItem("/Energy Generators/Fusion Generator 2/Inspect");

        menu.addItem("/Energy Storage Units/Energy Storage 1/Inspect");
        menu.addItem("/Energy Storage Units/Energy Storage 2/Inspect");

        menu.addItem("/Engines/Main Propulsion Engine/Inspect");
        menu.addItem("/Engines/Reverse Propulsion Engine 1/Inspect");
        menu.addItem("/Engines/Reverse Propulsion Engine 2/Inspect");
        menu.addItem("/Engines/Left Propulsion Engine 1/Inspect");
        menu.addItem("/Engines/Left Propulsion Engine 2/Inspect");
        menu.addItem("/Engines/Right Propulsion Engine 1/Inspect");
        menu.addItem("/Engines/Right Propulsion Engine 2/Inspect");

        menu.addItem("/Weapon Systems/Plasma Gun 1/Inspect");
        menu.addItem("/Weapon Systems/Plasma Gun 2/Inspect");
        menu.addItem("/Weapon Systems/Laser Cannon 1/Inspect");
        menu.addItem("/Weapon Systems/Laser Cannon 2/Inspect");
        menu.addItem("/Weapon Systems/Missile Launcher/Inspect");
        menu.addItem("/Weapon Systems/Mine Dropper/Inspect");

        menu.addItem("/Defense Systems/Energy Shield/Inspect");

        menu.addItem("/Equipment/Radar/Inspect");
        menu.addItem("/Equipment/Mine Detector/Inspect");
        menu.addItem("/Equipment/Trade Module/Inspect");
        menu.addItem("/Equipment/Trade Module/Send Trade Request");

        menu.addItem("/Equipment/Nano Bots/Inspect");
        menu.addItem("/Equipment/Nano Bots/Build Silo");
        menu.addItem("/Equipment/Nano Bots/Build Territory Marker");
        menu.addItem("/Equipment/Nano Bots/Build Missile Factory");
        menu.addItem("/Equipment/Nano Bots/Build Turret/Build Gun Turret");
        menu.addItem("/Equipment/Nano Bots/Build Turret/Build Laser Turret");
        menu.addItem("/Equipment/Nano Bots/Build Turret/Build Missile Turret");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Colibri MK1");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Hawk Class");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Wraith MK1");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Adder Class");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Commander MK1");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Turtle Class");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Eagle Class");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Mosquito Class");
        menu.addItem("/Equipment/Nano Bots/Build Ship/Build Viper MK1");

        menu.addItem("/AI Module/Inspect");
        menu.addItem("/AI Module/Follow");
        menu.addItem("/AI Module/Navigate to");
        menu.addItem("/AI Module/Attack");
        menu.addItem("/AI Module/Stop");
        menu.addItem("/AI Module/Activate Autopilot");
        menu.addItem("/AI Module/Set Stance/Evasive");
        menu.addItem("/AI Module/Set Stance/Passive");
        menu.addItem("/AI Module/Set Stance/Defensive");
        menu.addItem("/AI Module/Set Stance/Sentry");
        menu.addItem("/AI Module/Set Stance/Aggressive");

        menu.addItem("/Launch Escape Pod");
    }

    @Override
    public void open() {
        controlWindow.open();

        menu.show(true);
    }

    @Override
    public void close() {
        controlWindow.close();
        menu.hide();
    }

    private void cleanup() {
        menu.hide();
    }

    @Override
    public boolean isOpen() {
        return controlWindow.isVisible();
    }
}
