package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.graphics.Color;
import com.universeprojects.gamecomponents.client.elements.GCLevelGauge;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class LevelGaugeDemo extends Demo {

    private final GCSimpleWindow window;

    private final GCLevelGauge liveGauge1, liveGauge2;
    private float liveValue1, liveValue2;
    private final double liveDelta1, liveDelta2;
    private double liveFactor1, liveFactor2;

    public LevelGaugeDemo(H5ELayer layer) {

        window = new GCSimpleWindow(layer, "level-gauge-demo", "Level Gauges", 400, 500, false);
        window.positionProportionally(.5f, .5f);

        window.addH2("Default configuration:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        final GCLevelGauge gauge1 = new GCLevelGauge(layer);
        window.addElement(gauge1, Position.NEW_LINE, Alignment.LEFT, 0, 0, true);
        gauge1.setValue(65);

        window.addEmptyLine(25);
        window.addH2("Custom dimensions and font size:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        GCLevelGauge gauge2 = window.addElement(new GCLevelGauge(layer), Position.NEW_LINE, Alignment.LEFT, 0, 0, true);
        gauge2.setFontScale(0.5f);
        gauge2.setDimensions(350, 18);
        gauge2.setValue(100);

        window.addEmptyLine(25);
        window.addH2("Value below maximum:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        GCLevelGauge gauge3 = window.addElement(new GCLevelGauge(layer), Position.NEW_LINE, Alignment.LEFT, 0, 0, true);
        gauge3.setDimensions(350, 25);
        gauge3.setLabel("Available Energy");
        gauge3.setValue(0);

        window.addEmptyLine(25);
        window.addH2("Live value modification:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        liveGauge1 = window.addElement(new GCLevelGauge(layer), Position.NEW_LINE, Alignment.LEFT, 0, 0, true);
        liveGauge1.setDimensions(350, 25);
        liveGauge1.setLabel("Available Energy");
        liveGauge1.setRange(1000, 0.1f);

        window.addEmptyLine(25);
        window.addH2("Custom colors:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        liveGauge2 = window.addElement(new GCLevelGauge(layer), Position.NEW_LINE, Alignment.LEFT, 0, 0, true);
        liveGauge2.setDimensions(350, 25);
        liveGauge2.setLabel("Available Energy");
        liveGauge2.setRange(1000, 0.1f);
        liveGauge2.setEmptyColor(Color.valueOf("#333333"));
        liveGauge2.setFullColor(Color.valueOf("#FF00FF"));
        liveGauge2.setFontColor(Color.valueOf("#FFFF00"));

        liveValue1 = 1000;
        liveDelta1 = 0.031;
        liveFactor1 = 1 - liveDelta1;
        liveValue2 = 1000;
        liveDelta2 = 0.025;
        liveFactor2 = 1 - liveDelta2;
    }

    @Override
    public void open() {
        window.open();
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }

    @Override
    public void animate() {
        liveValue1 *= liveFactor1;
        if (liveValue1 <= 100 && liveFactor1 < 1) {
            liveValue1 = 100;
            liveFactor1 += 2 * liveDelta1;
        } else if (liveValue1 >= 950 && liveFactor1 > 1) {
            liveValue1 = 950;
            liveFactor1 -= 2 * liveDelta1;
        }
        liveGauge1.setValue(liveValue1);


        liveValue2 *= liveFactor2;
        if (liveValue2 <= 100 && liveFactor2 < 1) {
            liveValue2 = 100;
            liveFactor2 += 2 * liveDelta2;
        } else if (liveValue2 >= 950 && liveFactor2 > 1) {
            liveValue2 = 950;
            liveFactor2 -= 2 * liveDelta2;
        }
        liveGauge2.setValue(liveValue2);
    }
}
