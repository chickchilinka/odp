package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.demos.Demo;
import com.universeprojects.gamecomponents.server.BlockcypherService;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EDropDown;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.vsdata.shared.CryptoCurrency;
import com.universeprojects.vsdata.shared.CryptoTransactionData;

import java.util.ArrayList;
import java.util.List;

public class WalletTransactionsDemo extends Demo {
    private final static int WINDOW_W = 900;
    private final static int WINDOW_H = 1000;
    private final Table transactionsTable;
    private final H5ELayer layer;
    private final H5EInputBox box;
    private final H5EButton checkBtn;
    private final H5EDropDown dropDown;
    private final GCSimpleWindow window;

    public WalletTransactionsDemo(H5ELayer layer) {
        window = new GCSimpleWindow(layer, "transactionsDialog", "Check Transactions", WINDOW_W, WINDOW_H, false);
        window.positionProportionally(0.65f, 0.65f);
        window.setFullscreenOnMobile(true);
        this.layer = layer;
        Table row = new Table();
        box = window.add(new H5EInputBox(layer)).left().top().pad(5).growX().getActor();
        box.setMessageText("Wallet address");
        window.row();
        List<String> currencies = new ArrayList<>();
        for (CryptoCurrency value : CryptoCurrency.values()) {
            currencies.add(value.toString());
        }
        dropDown = row.add(new H5EDropDown(currencies, layer)).left().top().pad(5).getActor();
        row.add().growX();
        checkBtn = row.add(new H5EButton("Check", layer)).left().top().pad(5).padTop(0).getActor();
        checkBtn.addButtonListener(() -> {
            processWallet(box.getText());
        });
        window.add(row).growX();
        window.row();
        H5EScrollablePane pane = window.add(new H5EScrollablePane(layer)).grow().pad(5).getActor();
        transactionsTable = pane.getContent();
        transactionsTable.setBackground(StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque);
    }

    public void processWallet(String walletAddress) {
        CryptoCurrency type = CryptoCurrency.valueOf(dropDown.getSelected());
        BlockcypherService service = new BlockcypherService(type);
        service.getTransactions(walletAddress, 10, (list) -> {
            processTransactions(list);
        });
    }

    public void processTransactions(List<CryptoTransactionData> transactions) {
        transactionsTable.clearChildren();
        H5ELabel from = new H5ELabel("From", layer);
        from.setStyle(StyleFactory.INSTANCE.labelSmallElectrolizeBorders);
        H5ELabel to = new H5ELabel("To", layer);
        to.setStyle(StyleFactory.INSTANCE.labelSmallElectrolizeBorders);
        H5ELabel amount = new H5ELabel("Amount", layer);
        amount.setStyle(StyleFactory.INSTANCE.labelSmallElectrolizeBorders);
        H5ELabel time = new H5ELabel("Time", layer);
        time.setStyle(StyleFactory.INSTANCE.labelSmallElectrolizeBorders);
        transactionsTable.add(from).left().top().growX();
        transactionsTable.add(to).left().top().growX();
        transactionsTable.add(amount).left().top().fill();
        transactionsTable.add(time).left().top().fill();
        transactionsTable.row();
        for (int i = 0; i < transactions.size(); i++) {
            for (int j = 0; j < transactions.get(i).outputs.length; j++) {
                String datetime = transactions.get(i).timestamp;
                String inputAddress = transactions.get(i).inputs[0].addresses[0];
                addTableRow(inputAddress, transactions.get(i).outputs[j].addresses[0], String.valueOf(transactions.get(i).outputs[j].value), datetime);
            }
        }
        transactionsTable.row();
        transactionsTable.add().grow();
    }

    public void addTableRow(String from, String to, String amount, String time) {
        H5ELabel fromLabel = new H5ELabel(from, layer);
        fromLabel.setStyle(StyleFactory.INSTANCE.labelSmallElectrolizeBorders);
        fromLabel.setWrap(true);
        H5ELabel toLabel = new H5ELabel(to, layer);
        toLabel.setStyle(StyleFactory.INSTANCE.labelSmallElectrolizeBorders);
        toLabel.setWrap(true);
        H5ELabel amountLabel = new H5ELabel(amount, layer);
        amountLabel.setStyle(StyleFactory.INSTANCE.labelSmallElectrolizeBorders);
        H5ELabel timeLabel = new H5ELabel(time, layer);
        timeLabel.setStyle(StyleFactory.INSTANCE.labelSmallElectrolizeBorders);
        transactionsTable.add(fromLabel).left().top().fill();
        transactionsTable.add(toLabel).left().top().fill();
        transactionsTable.add(amountLabel).left().top().fill();
        transactionsTable.add(timeLabel).left().top().fill().padRight(2);
        transactionsTable.row();
    }

    @Override
    public void open() {
        window.open(false);
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }
}
