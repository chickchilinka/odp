package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBar;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarController;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarType;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCOperation;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.Map;

public class ActionBarDemo extends Demo {

    private GCActionBar actionBar;

    public ActionBarDemo(H5ELayer layer) {

        actionBar = new GCActionProgressBarDemoImpl(layer);
        layer.addActorToTop(actionBar);
    }

    @Override
    public void open() {
        actionBar.show();
    }

    @Override
    public void close() {
        //actionBar.hide();
    }

    @Override
    public boolean isOpen() {
        return actionBar.isVisible();
    }

    private class GCActionProgressBarDemoImpl extends GCActionBar {

        public GCActionProgressBarDemoImpl(H5ELayer layer) {
            super(layer, new GCActionBarController() {
                @Override
                public void callAction(GCActionBarType type, int row, int index) {

                }

                @Override
                public void clear() {

                }

                @Override
                public GCOperation getAction(GCActionBarType type, int row, int index) {
                    return null;
                }

                @Override
                public void setAction(GCActionBarType type, int row, int index, GCOperation myAction, boolean save) {

                }

                @Override
                public void refreshIcons() {

                }

                @Override
                public Map<RowIndexPair, GCOperation> getOperations() {
                    return null;
                }
            }, null);
        }
    }
}
