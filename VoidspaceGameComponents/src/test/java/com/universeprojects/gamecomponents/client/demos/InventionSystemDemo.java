package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeaCategoryData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeasData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCInventionSystemDialog;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCKnowledgeData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCKnowledgeDefData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData.GCRecipeSlotDataItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCSkillsData;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.gefcommon.shared.elements.UserFieldValueConfigurationType;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.resourceloader.AtlasResourceLoader;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.vsdata.shared.BuildQueueEntry;
import com.universeprojects.vsdata.shared.RarityValue;
import com.universeprojects.vsdata.shared.RecipeData;
import com.universeprojects.vsdata.shared.RecipeSlotType;
import com.universeprojects.vsdata.shared.UserFieldValueConfigurationData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class InventionSystemDemo extends Demo {

    public static final String UNKNOWN_ICON_KEY = AtlasResourceLoader.UNKNOWN_ICON_KEY;
    private final GCSimpleWindow controlWindow;
    private final H5EButton btnToggleAnimation;
    private boolean animate = false;

    private final InventionDialogDemoImpl inventionDialog;

    public InventionSystemDemo(H5ELayer layer) {
        controlWindow = new GCSimpleWindow(layer, "invention-system-demo", "Invention Dialog Demo");
        controlWindow.onClose.registerHandler(this::cleanup);
        controlWindow.positionProportionally(.6f, .15f);

        btnToggleAnimation = ButtonBuilder.inLayer(layer).withFilledRectBackground().withText("Turn animation " + (animate ? "OFF" : "ON")).build();
        btnToggleAnimation.addButtonListener(() -> {
            animate = !animate;
            btnToggleAnimation.setText("Turn animation " + (animate ? "OFF" : "ON"));
            controlWindow.activate();
        });
        //noinspection deprecation
        controlWindow.addElement(btnToggleAnimation, Position.SAME_LINE, Alignment.LEFT, 0, 0);

        inventionDialog = new InventionDialogDemoImpl(layer);
        inventionDialog.positionProportionally(.65f, .6f);
        inventionDialog.loadDataForQueue();
    }

    @Override
    public void open() {
        controlWindow.open();
        inventionDialog.open();
    }

    @Override
    public void close() {
        controlWindow.close();
    }

    private void cleanup() {
        inventionDialog.close();
    }

    @Override
    public boolean isOpen() {
        return inventionDialog.isOpen();
    }

    @Override
    public void animate() {
        if (animate) {
            inventionDialog.animate();
        }
    }

    private static class InventionDialogDemoImpl extends GCInventionSystemDialog {

        private final Logger log = Logger.getLogger(InventionDialogDemoImpl.class);
        private Map<BuildQueueEntry, GCRecipeData> queueRecipesMap;

        public InventionDialogDemoImpl(H5ELayer layer) {
            super(layer);
        }

        private long lastAnimationFrame = System.currentTimeMillis();

        @Override
        public void loadDataForKnowledgeTab() {
            flagLoading();
            GCKnowledgeData knowledgeData = GCKnowledgeData.createNew()
                .add("Hydrogen", 155, null)
                .add("Oxygen", 389, null)
                .add("Nitrogen", 38, null)
                .add("Argon", 3, null)
                .add("Titanium", 7, null)
                .add("Aluminium", 11, null)
                .add("Duralumin", 96, null)
                .add("Stainless Steel", 128, null)
                .add("Metal Debris", 552, null)
                .add("Space Garbage", 993, null)
                .add("Asteroid Rock", 430, null)
                .add("Space Dust", 430, null)
                .add("Standard Missile", 21, null)
                .add("Plasma Blaster", 107, null)
                .add("Missile Launcher", 90, null)
                .add("Escape Pod", 3, null);

            openKnowledgeTab(knowledgeData, GCKnowledgeDefData.createNew());
        }

        @Override
        public void loadDataForExperimentsTab() {
            flagLoading();
            GCInventoryData experimentsData = GCInventoryData.createNew()
                .add(createItem("1.hydrogen", "Hydrogen", "hydrogen-icon", 1))
                .add(createItem("2.nitrogen", "Nitrogen", "hydrogen-icon", 1))
                .add(createItem("3.titanium", "Titanium", "metal-icon", 1))
                .add(createItem("4.aluminium", "Aluminium", "metal-icon", 1))
                .add(createItem("5.duralumin", "Duralumin", "metal-icon", 1))
                .add(createItem("6.standard.missile", "Standard Missile", "missile1-icon", 1))
                .add(createItem("7.plasma.blaster", "Plasma Blaster", "turret1-icon", 1))
                .add(createItem("8.escape.pod", "Escape Pod", "drone1-icon", 1))
                .add(createItem("9.space.garbage", "Space Garbage", "drone1-icon", 1))
                .add(createItem("10.metal.debris", "Metal Debris", "drone1-icon", 1));

            List<GCInventoryData> experimentDataList = new ArrayList<>();
            experimentDataList.add(experimentsData);
            openExperimentsTab(experimentDataList);
        }

        @Override
        public void loadDataForIdeasTab() {
            flagLoading();
            GCIdeasData ideasData = GCIdeasData.createNew()
                .add(100L, "Timed-activation Mine", "An explosive device with an activation timer", UNKNOWN_ICON_KEY, 0L, 0L, 5L)
                .add(101L, "Proximity Mine", "An explosive device with a proximity sensor to trigger the explosion", UNKNOWN_ICON_KEY, 0L, 0L, 5L)
                .add(102L, "Bozo Mine", "An explosive device with a speaker, that plays a circus song for 5 seconds before the explosion is triggered", UNKNOWN_ICON_KEY, 0L, 0L, 5L);

            openIdeasTab(ideasData, GCIdeaCategoryData.createNew());
        }

        @Override
        public void loadDataForItemsTab() {
            flagLoading();
            GCSkillsData skillsData = GCSkillsData.createNew()
                .add(100L, "Timed-activation Mine", "An explosive device with an activation timer", UNKNOWN_ICON_KEY, 0L, 5L, Color.WHITE)
                    .add(101L, "Some item", "Some description", UNKNOWN_ICON_KEY, 0L, 8L, Color.BLUE)
            .add(101L, "Some item", "Some description", UNKNOWN_ICON_KEY, 0L, 8L, Color.BLUE).add(101L, "Some item", "Some description", UNKNOWN_ICON_KEY, 0L, 8L, Color.BLUE).add(101L, "Some item", "Some description", UNKNOWN_ICON_KEY, 0L, 8L, Color.BLUE).add(101L, "Some item", "Some description", UNKNOWN_ICON_KEY, 0L, 8L, Color.BLUE).add(101L, "Some item", "Some description", UNKNOWN_ICON_KEY, 0L, 8L, Color.BLUE).add(101L, "Some item", "Some description", UNKNOWN_ICON_KEY, 0L, 8L, Color.BLUE);

            openItemsTab(skillsData, GCIdeaCategoryData.createNew());
        }

        @Override
        public void loadDataForQueue() {
            flagLoading();
            List<BuildQueueEntry> queueEntries=new ArrayList<>();
            queueRecipesMap=new HashMap<>();
            BuildQueueEntry entry1=new BuildQueueEntry();
            entry1.entryName="Something 1";
            entry1.rarityValue = RarityValue.EPIC;
            entry1.repeats=2;
            entry1.iconSpriteType = "ships/Artist1GreenShip.png";
            RecipeData recipeData1=new RecipeData();
            recipeData1.name = "Something 1";
            GCRecipeData recipe1 = GCRecipeData.createNew(recipeData1,GCRecipeData.RecipeMode.ITEM_IDEA,100L)

                    .addSlot("0.container",
                            "Container",
                            RecipeSlotType.ITEM,
                            true,
                            null,
                            GCInventoryData.createNew()
                                    .add(createItem("1.gas.cylinder", "Gas cylinder", "drone1-icon", quantity()))
                                    .add(createItem("2.shoe.box", "Shoe box", "drone1-icon", quantity()))
                                    .add(createItem("3.beer.bottle", "Beer bottle", "drone1-icon", quantity())))

                    .addSlot("1.flammable.gas",
                            "A flammable gas",
                            RecipeSlotType.ITEM,
                            true,
                            null,
                            GCInventoryData.createNew()
                                    .add(createItem("1.hydrogen", "Hydrogen", "hydrogen-icon", quantity()))
                                    .add(createItem("2.nitrogen", "Nitrogen", "hydrogen-icon", quantity()))
                                    .add(createItem("3.dihydrogen.sulfide", "Dihydrogen sulfide", "hydrogen-icon", quantity())));
            queueRecipesMap.put(entry1,recipe1);
            BuildQueueEntry entry2=new BuildQueueEntry();
            entry2.entryName="Something 2";
            entry2.rarityValue = RarityValue.COMMON;
            entry2.iconSpriteType = "ships/Artist2Ship1.png";
            RecipeData recipeData2=new RecipeData();
            recipeData2.name = "Something 2";
            GCRecipeData recipe2 = GCRecipeData.createNew(recipeData2,GCRecipeData.RecipeMode.ITEM_IDEA,100L)

                    .addSlot("0.container",
                            "Container",
                            RecipeSlotType.ITEM,
                            true,
                            null,
                            GCInventoryData.createNew()
                                    .add(createItem("1.gas.cylinder", "Gas cylinder", "drone1-icon", quantity()))
                                    .add(createItem("2.shoe.box", "Shoe box", "drone1-icon", quantity()))
                                    .add(createItem("3.beer.bottle", "Beer bottle", "drone1-icon", quantity())))

                    .addSlot("1.firestarter",
                            "Any fire-starter",
                            RecipeSlotType.ITEM,
                            true,
                            null,
                            GCInventoryData.createNew()
                                    .add(createItem("1.lighter", "Lighter", "drone1-icon", quantity()))
                                    .add(createItem("2.box.of.matches", "Box of matches", "drone1-icon", quantity())));
            queueRecipesMap.put(entry2,recipe2);
            BuildQueueEntry entry3=new BuildQueueEntry();
            entry3.entryName="Something 3";
            entry3.rarityValue = RarityValue.INVENTION_SKILL;
            entry3.iconSpriteType = "cloutWhite.png";
            RecipeData recipeData3=new RecipeData();
            recipeData3.name = "Something 3";
            GCRecipeData recipe3 = GCRecipeData.createNew(recipeData3,GCRecipeData.RecipeMode.ITEM_IDEA,100L)

                    .addSlot("0.container",
                            "Container",
                            RecipeSlotType.ITEM,
                            true,
                            null,
                            GCInventoryData.createNew()
                                    .add(createItem("1.gas.cylinder", "Gas cylinder", "drone1-icon", quantity()))
                                    .add(createItem("2.shoe.box", "Shoe box", "drone1-icon", quantity()))
                                    .add(createItem("3.beer.bottle", "Beer bottle", "drone1-icon", quantity())))

                    .addSlot("1.spices",
                            "Some spice to taste",
                            RecipeSlotType.ITEM,
                            true,
                            null,
                            GCInventoryData.createNew()
                                    .add(createItem("0.sea.salt", "Sea salt", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("1.black.pepper", "Black pepper", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("2.cumin", "Cumin", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("3.paprica", "Paprica", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("4.tabasco", "Tabasco", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("5.garlic.powder", "Garlic powder", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("6.red.curry.paste", "Red curry paste", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("7.dried.basil", "Dried basil", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("8.oregano", "Oregano", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("9.dried.basil", "Dried basil", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("10.clove", "Clove", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("11.anise.seed", "Anise seed", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("12.green.cardamom.seed", "Green cardamom seed", UNKNOWN_ICON_KEY, quantity()))
                                    .add(createItem("13.black.cardamom.seed", "Black cardamom seed", UNKNOWN_ICON_KEY, quantity())));
            queueRecipesMap.put(entry3,recipe3);
            UserFieldValueConfigurationData userFieldData=new UserFieldValueConfigurationData();
            userFieldData.description="Some description";
            userFieldData.targetFieldName="Some field";
            userFieldData.type = UserFieldValueConfigurationType.STRING;
            userFieldData.required=false;
            recipe3.getData().userFieldValueConfigurations=new ArrayList<>();
            recipe3.getData().getUserFieldValueConfigurationsSafe().add(userFieldData);

            queueEntries.add(entry1);
            queueEntries.add(entry2);
            queueEntries.add(entry3);
            initQueueData(queueEntries);
        }

        private GCInventoryItem createItem(String name, String itemClass, String icon, int quantity) {
            return new InventoryV2Demo.GCInventoryDemoItem(name, itemClass, quantity, icon);
        }

        void animate() {
            if (System.currentTimeMillis() - lastAnimationFrame < 1000) {
                return;
            }
            lastAnimationFrame = System.currentTimeMillis();

            if (!isCurrentTab(recipeTab)) {
                return;
            }

            for (GCRecipeSlotDataItem slot : recipe.getSlots()) {
                GCInventoryData updatedItems = GCInventoryData.createNew();
                GCInventoryData existingItems = slot.getItems();
                if(Gdx.app.getType() == Application.ApplicationType.iOS) {
                    Map<Integer, GCInventoryData.GCInventoryDataItem> itemsByIndex = existingItems.getItemsByIndex();
                    for(GCInventoryData.GCInventoryDataItem item : itemsByIndex.values()) {
                        if(item.item.getQuantity() > 1) {
                            updatedItems.add(new InventoryV2Demo.GCInventoryDemoItem(item.item.getUid(), item.item.getName(), item.item.getItemClass(), item.item.getQuantity()-1, item.item.getIconSpriteKey()));
                        }
                    }
                }
                else {
                    existingItems.forEach((item) -> {
                        if (item.item.getQuantity() > 1) {
                            updatedItems.add(new InventoryV2Demo.GCInventoryDemoItem(item.item.getUid(), item.item.getName(), item.item.getItemClass(), item.item.getQuantity()-1, item.item.getIconSpriteKey()));
                        }
                    });
                }
                slot.replaceItems(updatedItems);
            }

            updateRecipeTab(recipe);
        }


        private int quantity() {
            return random(3, random(7, 45));
        }

        private int random(int min, int max) {
            return min + rnd.nextInt(max + 1);
        }

        private Random rnd = new Random();
        private GCRecipeData recipe = null;

        private void initRecipeData() {
            final boolean REQUIRED = true, OPTIONAL = false;
            RecipeData recipeData=new RecipeData();
            recipeData.name = "Demo Idea";
            recipe = GCRecipeData.createNew(recipeData,GCRecipeData.RecipeMode.ITEM_IDEA,100L)

                .addSlot("0.container",
                    "Container",
                    RecipeSlotType.ITEM,
                    REQUIRED,
                    null,
                    GCInventoryData.createNew()
                        .add(createItem("1.gas.cylinder", "Gas cylinder", "drone1-icon", quantity()))
                        .add(createItem("2.shoe.box", "Shoe box", "drone1-icon", quantity()))
                        .add(createItem("3.beer.bottle", "Beer bottle", "drone1-icon", quantity())))

                .addSlot("1.flammable.gas",
                    "A flammable gas",
                        RecipeSlotType.ITEM,
                    REQUIRED,
                    null,
                    GCInventoryData.createNew()
                        .add(createItem("1.hydrogen", "Hydrogen", "hydrogen-icon", quantity()))
                        .add(createItem("2.nitrogen", "Nitrogen", "hydrogen-icon", quantity()))
                        .add(createItem("3.dihydrogen.sulfide", "Dihydrogen sulfide", "hydrogen-icon", quantity())))


                .addSlot("2.firestarter",
                    "Any fire-starter",
                        RecipeSlotType.ITEM,
                    REQUIRED,
                    null,
                    GCInventoryData.createNew()
                        .add(createItem("1.lighter", "Lighter", "drone1-icon", quantity()))
                        .add(createItem("2.box.of.matches", "Box of matches", "drone1-icon", quantity())))

                .addSlot("3.sticker",
                    "A funny sticker",
                        RecipeSlotType.ITEM,
                    OPTIONAL,
                    null,
                    GCInventoryData.createNew()
                        .add(createItem("1.hello.kitty.sticker", "Hello Kitty Sticker", "icons/hello-kitty-icon.png", quantity())))

                .addSlot("4.spices",
                    "Some spice to taste",
                        RecipeSlotType.ITEM,
                    OPTIONAL,
                    null,
                    GCInventoryData.createNew()
                        .add(createItem("0.sea.salt", "Sea salt", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("1.black.pepper", "Black pepper", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("2.cumin", "Cumin", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("3.paprica", "Paprica", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("4.tabasco", "Tabasco", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("5.garlic.powder", "Garlic powder", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("6.red.curry.paste", "Red curry paste", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("7.dried.basil", "Dried basil", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("8.oregano", "Oregano", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("9.dried.basil", "Dried basil", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("10.clove", "Clove", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("11.anise.seed", "Anise seed", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("12.green.cardamom.seed", "Green cardamom seed", UNKNOWN_ICON_KEY, quantity()))
                        .add(createItem("13.black.cardamom.seed", "Black cardamom seed", UNKNOWN_ICON_KEY, quantity())));
        }


        @Override
        public void onBeginExperimentsBtnPressed(String selectionUid, int repeats) {
            log.info("Begin-Experiments button pressed with selection UID " + selectionUid + " and repeats " + repeats + " time(s).");
        }

        @Override
        public void onBeginPrototypingBtnPressed(Long ideaId) {
            log.info("Begin-Prototyping button pressed with selection index " + ideaId);
            if (ideaId == null) {
                log.info("Please make a selection");
                return;
            }
            initRecipeData();
            openRecipeTab(recipe);
        }

        @Override
        public void onBuildItemBtnPressed(Long skillId) {
            log.info("Build-Item button pressed with skill id: " + skillId);
            initRecipeData();
            openRecipeTab(recipe);
        }

        @Override
        public void onDeleteSkillBtnPressed(Long skillId) {
            log.info("Delete-Skill button pressed with skill id: " + skillId);
        }

        @Override
        public void onBuildBuildingBtnPressed(Long skillId) {
            log.info("Build-Building button pressed with skill id: " + skillId);
        }

        @Override
        public void onRecipeOkayBtnPressed(GCRecipeData recipe, int repeats) {
            log.info("Recipe-Okay button pressed with recipe state: " + recipe.getReferenceId() + " and " + repeats + " repeats");
        }

        @Override
        public void onItemIconInfoClicked(Actor actor, GCInventoryData.GCInventoryDataItem data) {
            log.info("Info icon button pressed with data: " + data);
        }

        @Override
        public void onSkillIconInfoClicked(Actor actor, GCSkillsData.GCSkillDataItem skillDataItem) {
            log.info("Skill info icon clicked with skill "+ skillDataItem);
        }

        @Override
        public void onCancelBtnPressed() {
            log.info("Cancel button pressed");
        }

        @Override
        public void showCancelBtn() {

        }

        @Override
        public void hideCancelBtn() {

        }

        @Override
        public void clearEmbeddedProgressBar() {

        }

        @Override
        public void onFinishExperiment(List<String> icons) {

        }

        @Override
        public void onFinishDesignItem(String icon) {

        }

        @Override
        public void onFinishBuildItem(String icon) {

        }

        @Override
        public void onFinishDesignBuilding(String icon) {

        }

        @Override
        public void onQueueEntrySelected(BuildQueueEntry buildQueueEntry, boolean isFirst) {
            openRecipeTab(queueRecipesMap.get(buildQueueEntry), isFirst);
        }

        @Override
        public void onSkillUpgradeButtonPressed(GCSkillsData.GCSkillDataItem skill) {

        }

        @Override
        public void onQueueCancelPressed(){
            openRecipeTab();
        }

        @Override
        public boolean validateRecipeSlotsFilled(GCRecipeData data) {
            return true;
        }

        @Override
        public boolean validateUserFieldValues(GCRecipeData data) {
            return true;
        }
    }
}
