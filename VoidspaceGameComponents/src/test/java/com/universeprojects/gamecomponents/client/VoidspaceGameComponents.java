package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.universeprojects.common.shared.log.LogLevel;
import com.universeprojects.common.shared.log.LoggerConfigManager;
import com.universeprojects.common.shared.log.LoggerFactory;
import com.universeprojects.common.shared.log.SimpleJULLogProcessor;
import com.universeprojects.common.shared.profiler.ProfilerServer;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.services.MockTutorialConfig;
import com.universeprojects.gamecomponents.client.services.MockTutorialManager;
import com.universeprojects.gamecomponents.client.tutorial.TutorialController;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EResourceManager;
import com.universeprojects.html5engine.client.framework.H5ESprite;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.resourceloader.DownloadingResourceLoader;
import com.universeprojects.html5engine.client.framework.resourceloader.GdxNetAssetLoader;

public class VoidspaceGameComponents {
    private AssetManager assetManager;

    private ProfilerServer profiler = new ProfilerServer();
    private GameComponentsApplicationManager application;
    private TextureAtlas atlas;
    private H5EResourceManager rm;

    public static void main(String[] args) {
        LoggerFactory.getInstance().setProcessor(new SimpleJULLogProcessor());
        final LoggerConfigManager loggerConfigManager = LoggerFactory.getInstance().getLoggerConfigManager();
        loggerConfigManager.findLoggerConfig(LoggerFactory.ROOT_LOGGER_NAME).setMinLevel(LogLevel.DEBUG);
        GdxNetAssetLoader.initialize();
        new VoidspaceGameComponents().run();
    }

    /**
     * This is the GWT entry point method
     */
    private void run() {
        profiler.enable();
        application = new GameComponentsApplicationManager();
//        engine.setDebugMode(true);

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1600;
        config.height = 900;
        config.title = "VoidspaceGameComponents";
        config.useGL30 = false;
        config.addIcon("images/GUI/ui2/preload/voidspace-icon-16.png", Files.FileType.Internal);
        config.addIcon("images/GUI/ui2/preload/voidspace-icon-32.png", Files.FileType.Internal);
        config.addIcon("images/GUI/ui2/preload/voidspace-icon-64.png", Files.FileType.Internal);
        config.addIcon("images/GUI/ui2/preload/voidspace-icon-128.png", Files.FileType.Internal);

        application.onCreate.registerHandler(() -> {
            assetManager = new AssetManager();
            profiler.start("Atlas-load");
            assetManager.load("images/voidspace.atlas", TextureAtlas.class);
        });
        application.onLoadRender.registerHandler(this::onAtlasLoad);
        application.onInitialize.registerHandler(this::onInitialize);

        new LwjglApplication(application, config);
    }

    private void onAtlasLoad() {
        boolean done = assetManager.update();
        if (done) {
            onAtlasLoadComplete();
        }
    }

    private void onAtlasLoadComplete() {
        profiler.end("Atlas-load");
        atlas = assetManager.get("images/voidspace.atlas", TextureAtlas.class);
        profiler.start("ResourceManager-prep");
        DownloadingResourceLoader resourceLoader = new DownloadingResourceLoader(atlas);
        this.rm = new H5EResourceManager(resourceLoader);
        // Tell the resource manager to load all the resources defined in the .h5l file
        rm.readLibraryFile(Gdx.files.internal("voidspace.h5l").readString());
        rm.readLibraryFile(Gdx.files.internal("audio.h5l").readString());

        profiler.end("ResourceManager-prep");
        profiler.start("ResourceManager-load");
        rm.loadResources();
        application.setGameState(GameComponentsApplicationManager.GameState.INITIALIZE);
    }

    private void onInitialize(H5EEngine engine) {
        profiler.end("ResourceManager-load");
        profiler.start("Engine-start");
        StyleFactory.INSTANCE = new StyleFactory(rm, atlas);
        engine.setSkin(StyleFactory.INSTANCE.generateSkin());
        engine.setupDebugInfoViewer();
        engine.setResourceManager(rm);

        final H5ELayer backgroundLayer = engine.getLayer("default");

        createBackground(backgroundLayer);


        Cursor cursor = engine.getSkin().get(Cursor.class);
        Gdx.graphics.setCursor(cursor);

        H5ELayer foregroundLayer = engine.newUILayer("foreground");

        TutorialController tutorialController = new TutorialController();
        tutorialController.setConfig(new MockTutorialConfig());
        tutorialController.setLayer(engine.addLayer(new H5ELayer(engine, "tutorial", 2, 0, engine.getUiViewport())));
        tutorialController.setService(new MockTutorialManager());
        tutorialController.onNetworkReady();
        Tutorials.setInstance(tutorialController);

        MainMenu mainMenu = new MainMenu(foregroundLayer, tutorialController);
        mainMenu.open();
        profiler.end("Engine-start");
        profiler.printAndReset();

        application.setGameState(GameComponentsApplicationManager.GameState.RUNNING);
    }

    /**
     * Initializes the images in the background layer
     */
    private void createBackground(H5ELayer layer) {
        H5ESpriteType bgSpriteType = rm.getSpriteType("Game-Background4");
        H5ESprite bgSprite = new H5ESprite(layer, bgSpriteType);
        bgSprite.addTo(layer);
//        final int bgWidth = bgSpriteType.getWidth();
//        final int bgHeight = bgSpriteType.getHeight();
//
//        H5ESprite bgTopLeft = new H5ESprite(backgroundLayer, bgSpriteType);
//        bgTopLeft.setX(0);
//        bgTopLeft.setY(0);

//        H5ESprite bgTopRight = new H5ESprite(backgroundLayer, bgSpriteType);
//        bgTopRight.setX(bgWidth);
//        bgTopRight.setY(0);
//
//        H5ESprite bgbottomLeft = new H5ESprite(backgroundLayer, bgSpriteType);
//        bgbottomLeft.setX(0);
//        bgbottomLeft.setY(bgHeight);
//
//        H5ESprite bgBottomRight = new H5ESprite(backgroundLayer, bgSpriteType);
//        bgBottomRight.setX(bgWidth);
//        bgBottomRight.setY(bgHeight);
    }

}
