package com.universeprojects.gamecomponents.client.services;

import com.universeprojects.gamecomponents.client.tutorial.TutorialConfig;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarker;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarkerStyle;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialStage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MockTutorialConfig implements TutorialConfig {

    @Override
    public List<TutorialStage> getTutorialStages() {
        TutorialStage mainMenuTrade = new TutorialStage();
        mainMenuTrade.setActiveByDefault(true);
        TutorialMarker mainMenuMarkerTradeDialog = new TutorialMarker("button:menu:Trade Dialog");
        mainMenuMarkerTradeDialog.setTrigger("button:menu:Trade Dialog");
        mainMenuTrade.addMarker(mainMenuMarkerTradeDialog);
        mainMenuTrade.setId("1");
        mainMenuTrade.addObjective("button:menu:Trade Dialog");
        mainMenuTrade.setNextStages(Arrays.asList("2", "3"));

        TutorialStage mainMenuSplit = new TutorialStage();
        TutorialMarker marker = new TutorialMarker("button:menu:Stack Splitter");
        marker.setTrigger("button:menu:Stack Splitter");
        mainMenuSplit.addMarker(marker);
        mainMenuSplit.setId("2");
        mainMenuSplit.addObjective("button:menu:Stack Splitter");
        mainMenuSplit.setNextStages(Collections.singletonList("4"));


        TutorialStage tradeBtnStage = new TutorialStage();
        TutorialMarker tradeTutorialMarker = new TutorialMarker("button:trade");
        tradeTutorialMarker.setStyle(TutorialMarkerStyle.TOOLTIP);
        tradeTutorialMarker.setTrigger("button:trade");
        tradeBtnStage.addMarker(tradeTutorialMarker);
        tradeBtnStage.setId("3");
        tradeBtnStage.setPriority(1);
        tradeBtnStage.addObjective("button:trade");

        TutorialStage splitBtnStage = new TutorialStage();
        TutorialMarker o = new TutorialMarker("button:split");
        o.setTrigger("button:split");
        splitBtnStage.addMarker(o);
        splitBtnStage.setId("4");
        splitBtnStage.setPriority(1);
        splitBtnStage.addObjective("button:split");

        return Arrays.asList(mainMenuSplit, mainMenuTrade, tradeBtnStage, splitBtnStage);
    }

    @Override
    public void buildTutorialStages(String root) {

    }

    @Override
    public void addTutorialElement(String tutorialId, Object data) {

    }
}
