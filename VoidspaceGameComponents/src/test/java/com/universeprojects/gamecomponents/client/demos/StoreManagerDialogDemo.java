package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.GCStoreManagerDialog;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class StoreManagerDialogDemo extends Demo {
    private GCStoreManagerDialog storeDialog;

    public StoreManagerDialogDemo (H5ELayer layer) {
        storeDialog = new GCStoreManagerDialog(layer);

        storeDialog.positionProportionally(0.7f, 0.5f);

//        GCStoreOrderComponent storeOrderComponent = new GCStoreOrderComponent(layer, "Test", "3d-printer2", 1337, "test", "missile1-icon", 42);
//        for (int i = 0; i < 13; i++) {
//            storeDialog.addStoreOrderToDialog(storeOrderComponent);
//        }
    }

    @Override
    public void open () {
        storeDialog.open();
    }

    @Override
    public void close () {
        storeDialog.close();
    }

    @Override
    public boolean isOpen () {
        return storeDialog.isOpen();
    }
}
