package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.dialogs.GCInfoDialog;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.Deque;
import java.util.LinkedList;

@SuppressWarnings("SpellCheckingInspection")
public class CachedContainerDemo extends Demo {

    private final Logger log = Logger.getLogger(CachedContainerDemo.class);

    private final H5ELayer layer;
    private final GCSimpleWindow controlWindow;


    private Deque<GCInfoDialog> openDialogs = new LinkedList<>();

    private int nextDialogIndex = 0;

    public CachedContainerDemo(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);

        controlWindow = new GCSimpleWindow(layer, "cached-container-demo", "Cached Container Demo");

//        H5EGraphicElement infectedSprite = createInfectedSprite(layer);
//        controlWindow.addElement(infectedSprite, Position.SAME_LINE, Alignment.LEFT, 0, 0);
//        H5EGraphicElement outlineSprite = createOutlineSprite(layer);
//        controlWindow.addElement(outlineSprite, Position.SAME_LINE, Alignment.LEFT, infectedSprite.getWidth() + 10, 0);
//        H5EGraphicElement coloredBackgroundSprite = createedColorBackgroundSprite(layer);
//        controlWindow.addElement(coloredBackgroundSprite, Position.SAME_LINE, Alignment.LEFT, 0, infectedSprite.getHeight() + 10);
//        H5EGraphicElement singleColorSprite = createSingleColorSprite(layer);
//        controlWindow.addElement(singleColorSprite, Position.SAME_LINE, Alignment.LEFT, infectedSprite.getWidth() + 10, infectedSprite.getHeight() + 10);
//
//
//        controlWindow.positionProportionally(.5, .5);
    }

//    private H5EGraphicElement createInfectedSprite(H5ELayer layer) {
//        H5ECachedContainer cachedContainer = new H5ECachedContainer(layer);
//        H5ESpriteType imageSpriteType = layer.getEngine().getResourceManager().getSpriteType("shipyard1");
//        H5ESpriteType infectedSpriteType = layer.getEngine().getResourceManager().getSpriteType("infected-effect1");
//        H5ESprite bottomLayer = new H5ESprite(layer, imageSpriteType);
//        H5ESprite middleLayer = new H5ESprite(layer, infectedSpriteType);
//        middleLayer.setBlendingMode(BlendingMode.SOURCE_IN);
//        H5ESprite topLayer = new H5ESprite(layer, imageSpriteType);
//        topLayer.setTransparency(0.9);
//        topLayer.setBlendingMode(BlendingMode.MULTIPLY);
//        cachedContainer.addChild(bottomLayer);
//        cachedContainer.addChild(middleLayer);
//        cachedContainer.addChild(topLayer);
//        return cachedContainer;
//    }
//
//    private H5EGraphicElement createOutlineSprite(H5ELayer layer) {
//        H5ECachedContainer cachedContainer = new H5ECachedContainer(layer);
//        H5ESpriteType imageSpriteType = layer.getEngine().getResourceManager().getSpriteType("shipyard1");
//        H5ESprite bottomLayer = new H5ESprite(layer, imageSpriteType);
//        bottomLayer.setShadowColor("red");
//        bottomLayer.setShadowBlur(3);
//        H5ESprite topLayer = new H5ESprite(layer, imageSpriteType);
//        topLayer.setBlendingMode(BlendingMode.XOR);
//        cachedContainer.addChild(bottomLayer);
//        cachedContainer.addChild(topLayer);
//        return cachedContainer;
//    }
//
//    private H5EGraphicElement createedColorBackgroundSprite(H5ELayer layer) {
//        H5ESpriteType imageSpriteType = layer.getEngine().getResourceManager().getSpriteType("shipyard1");
//        H5ESprite sprite = new H5ESprite(layer, imageSpriteType);
//        sprite.setShadowColor("red");
//        sprite.setShadowBlur(2);
//        sprite.setTransparency(0.5);
//        return sprite;
//    }
//
//    private H5EGraphicElement createSingleColorSprite(H5ELayer layer) {
//        H5ECachedContainer cachedContainer = new H5ECachedContainer(layer);
//        H5ESpriteType imageSpriteType = layer.getEngine().getResourceManager().getSpriteType("shipyard1");
//        H5ESprite bottomLayer = new H5ESprite(layer, imageSpriteType);
//        H5ERectangle topLayer = new H5ERectangle(layer);
//        topLayer.setColor(Color.valueOf("red"));
//        topLayer.setWidth(bottomLayer.getWidth());
//        topLayer.setHeight(bottomLayer.getHeight());
//        topLayer.setBlendingMode(BlendingMode.SOURCE_ATOP);
//        cachedContainer.addChild(bottomLayer);
//        cachedContainer.addChild(topLayer);
//        return cachedContainer;
//    }

    @Override
    public void open() {
        controlWindow.open();
    }

    @Override
    public void close() {
        controlWindow.close();
    }

    private void cleanup() {
        while (!openDialogs.isEmpty()) {
            openDialogs.pollFirst().close();
        }
    }

    @Override
    public boolean isOpen() {
        return controlWindow.isOpen();
    }

}
