package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.GCMapMarkersDialog;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.vsdata.shared.MapMarkerData;
import com.universeprojects.vsdata.shared.MapMarkerGroupData;

import java.util.ArrayList;

public class MapMarkersDemo extends Demo {
    public GCMapMarkersDialog dialog;

    public MapMarkersDemo(H5ELayer layer) {
        dialog = new GCMapMarkersDialog(layer,(marker)->{},(group)->{});
        demoData();
    }

    public void demoData() {
        MapMarkerGroupData group1 = new MapMarkerGroupData("group1","Group 1", new ArrayList<MapMarkerData>());
        MapMarkerGroupData group2 = new MapMarkerGroupData("group2","Group 2", new ArrayList<MapMarkerData>());
        group1.markers.add(new MapMarkerData("some id", "Marker 1", "icons/copper-coil1.png"));
        group1.markers.add(new MapMarkerData("some id", "Marker 2", "null icon"));
        group1.markers.add(new MapMarkerData("some id", "Marker 3", "null icon"));
        group2.markers.add(new MapMarkerData("some id", "Marker 4", "null icon"));
        group2.markers.add(new MapMarkerData("some id", "Marker 5", "null icon"));
        dialog.processData(new ArrayList<MapMarkerGroupData>() {{
            add(group1);
            add(group2);
        }});
    }

    @Override
    public boolean isOpen() {
        return dialog.isOpen();
    }

    @Override
    public void open() {
        dialog.open();
    }

    @Override
    public void close() {
        dialog.close();
    }
}
