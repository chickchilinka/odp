package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionProgressBar;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class ActionProgressBarDemo extends Demo {

    private final Logger log = Logger.getLogger(ActionProgressBarDemo.class);

    private GCActionProgressBar progressBar;

    private int progress = 100;

    public ActionProgressBarDemo(H5ELayer layer) {
        progressBar = new GCActionProgressBarDemoImpl(layer);
        layer.addActorToTop(progressBar);
        progressBar.setCaption("Something is in progress...");
        progressBar.setProgress(0);
        progressBar.setMaximum(1000);
        progressBar.positionProportionally(.5f, .5f);
    }

    @Override
    public void open() {
        progressBar.show();
    }

    @Override
    public void close() {
        progressBar.hide();
    }

    @Override
    public boolean isOpen() {
        return progressBar.isVisible();
    }

    @Override
    public void animate() {
        if (++progress > 1000) {
            progress = 0;
        }
        progressBar.setProgress(progress);
    }

    private class GCActionProgressBarDemoImpl extends GCActionProgressBar {

        public GCActionProgressBarDemoImpl(H5ELayer layer) {
            super(layer, "Left", "Right");
        }

        @Override
        public void onLeftBtnPressed() {
            log.info("Left button was pressed");
        }

        @Override
        public void onRightBtnPressed() {
            log.info("Right button was pressed");
        }

        @Override
        public void onMaximumReached() {
            log.info("Progress maximum was reached");
        }

    }

}
