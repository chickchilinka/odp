package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.universeprojects.gamecomponents.client.windows.GCWindowManager;
import com.universeprojects.gamecomponents.client.windows.WindowManager;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.shared.GenericEvent;

public class GameComponentsApplicationManager implements ApplicationListener {

    public final GenericEvent.GenericEvent0Args onCreate = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent2Args<GameState, GameState> onStateChange = new GenericEvent.GenericEvent2Args<>();
    public final GenericEvent.GenericEvent1Args<H5EEngine> onInitialize = new GenericEvent.GenericEvent1Args<>();
    public final GenericEvent.GenericEvent0Args onLoadRender = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onPausedRender = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onPause = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onResume = new GenericEvent.GenericEvent0Args();

    public enum GameState {
        LOAD, INITIALIZE, RUNNING
    }

    private boolean paused;

    private GameState currentGameState = GameState.LOAD;

    private H5EEngine graphicsEngine;


    @Override
    public void create() {
        onCreate.fire();
    }

    @Override
    public void resize(int width, int height) {
        if(graphicsEngine != null) {
            graphicsEngine.resize(width, height);
        }
        WindowManager windowManager = WindowManager.getInstance(graphicsEngine);
        if(windowManager instanceof GCWindowManager) {
            ((GCWindowManager)windowManager).setMobileModeFromResize(width < 500);
        }
    }

    @Override
    public void render() {
        if(paused) {
            onPausedRender.fire();
            return;
        }
        switch (currentGameState) {
            case RUNNING:
                graphicsEngine.render();
                break;
            case LOAD:
                onLoadRender.fire();
                break;
            case INITIALIZE:
                initializeGraphicEngine();
                onInitialize.fire(graphicsEngine);
                break;
        }
    }

    private void initializeGraphicEngine() {
        if(graphicsEngine != null) {
            return;
        }
        graphicsEngine = new H5EEngine();
        graphicsEngine.create(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0);
    }

    @Override
    public void pause() {
        paused = true;
        onPause.fire();
    }

    @Override
    public void resume() {
        paused = false;
        onResume.fire();
    }

    public boolean isPaused() {
        return paused;
    }

    public void setGameState(GameState newState) {
        if(newState == null || currentGameState == newState) return;
        GameState lastGameState = currentGameState;
        currentGameState = newState;
        onStateChange.fire(lastGameState, newState);
    }

    public GameState getGameState() {
        return currentGameState;
    }

    @Override
    public void dispose() {
        graphicsEngine.dispose();
    }
}
