package com.universeprojects.gamecomponents.client.dialogs.quests;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.dialogs.Confirmable;
import com.universeprojects.gamecomponents.client.dialogs.GCConfirmationScreen;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates the UI for player Quest.
 */
public class GCQuestDialog implements Confirmable {
    private GCSimpleWindow window;
    private H5ELayer layer;
    private GCQuestListWrapper quests;
    private GCList<GCQuestListItem> questListing;

    //Active quest assets
    private H5EScrollablePane questScrollPane;

    //Description side assets
    private H5ELabel questDescriptionLabel;
    private H5EScrollablePane questDescriptionScrollpane;
    private H5ELabel questEmptyWarning;


    //Tab Buttons (active/completed/skipped/abandoned)

    private H5EButton btnActiveQuest;
    private H5EButton btnCompletedQuest;
    private H5EButton btnSkippedQuest;
    private H5EButton btnAbandonedQuest;
    private H5EButton btnAbandonSelectedQuest;
    private H5EButton btnSkipSelectedQuest;
    private ButtonGroup questButtonGroup;

    //side tracker window
    private GCQuestSidebar questSidebar;
    //End of side tracking window

    //confirm screens
    private GCConfirmationScreen confirmationScreen;
    private boolean skippingQuest; //Booleans used to set the right text for confirmation screen
    private boolean abandoningQuest;//


    public GCQuestDialog(H5ELayer layer) {
        //TODO: Move this to another class, Add a potential way to close/minimize this
        //**************************START OF QUICK VIEW****************************************
        questSidebar = new GCQuestSidebar(layer);
        questSidebar.open();
        questSidebar.refresh();
        //**************************END OF QUICK VIEW****************************************

        this.layer = layer;
        window = new GCQuestManager(null, layer, 650, 500, questSidebar);
        window.setTitle("Quest");
        window.defaults().left().top();

        //Active Quest Button
        btnActiveQuest = new H5EButton("Active Quest", layer);
        btnActiveQuest.getLabel().setFontScale(0.8f);
        btnActiveQuest.setChecked(true);
        btnActiveQuest.setColor(Color.YELLOW);
        window.add(btnActiveQuest).colspan(1);

        //complete Quest Button
        btnCompletedQuest = new H5EButton("Completed Quest", layer);
        btnCompletedQuest.getLabel().setFontScale(0.7f);
        window.add(btnCompletedQuest).colspan(1);

        //Skipped Quest Button
        btnSkippedQuest = new H5EButton("Skipped Quest", layer);
        btnSkippedQuest.getLabel().setFontScale(0.7f);
        window.add(btnSkippedQuest).colspan(1);
        // window.debug();

        //abandon Quest Button
        btnAbandonedQuest = new H5EButton("Abandoned Quest", layer);
        btnAbandonedQuest.getLabel().setFontScale(0.7f);
        window.add(btnAbandonedQuest).colspan(1);
        window.row();

        //Make buttongroup with buttons
        questButtonGroup = new ButtonGroup<>(btnActiveQuest, btnCompletedQuest, btnSkippedQuest, btnAbandonedQuest);
        questButtonGroup.setMaxCheckCount(1);
        questButtonGroup.setMinCheckCount(0);
        questButtonGroup.setUncheckLast(true);
        //Added listeners here because adding them earlier breaks things
        btnActiveQuest.addButtonListener(this::refreshQuestListing);
        btnCompletedQuest.addButtonListener(this::refreshQuestListing);
        btnSkippedQuest.addButtonListener(this::refreshQuestListing);
        btnAbandonedQuest.addButtonListener(this::refreshQuestListing);

        //****LEFT Side*****

        //Quest Listing i.e left listing

        quests = new GCQuestListWrapper();
        questListing = new GCList<>(layer, 1, 5, 5, true);
        for (GCQuestListItem currentQuest : quests.getActiveQuests()) {
            questListing.addItem(currentQuest);
        }

        //Quest list scroll pane
        questScrollPane = new H5EScrollablePane(layer, "scrollpane-backing-2");
        questScrollPane.setScrollingDisabled(true, false);
        //add quest listing to scrollpane. questlisting-->quest scrollpane
        questScrollPane.add(questListing).grow().left();

        questEmptyWarning = new H5ELabel("No Quest Available", layer);
        questEmptyWarning.setAlignment(Align.center);
        questEmptyWarning.setFontScale(0.7f);

        //add quest scrollpane to window. quest scrollpane-->window
        window.add(questScrollPane).colspan(2).left().fill();
        // window.add(questScrollPane).width(300).left();
        //********End of Left********

        //*******Right side*******

        //Quest description right scrollpane
        questDescriptionScrollpane = new H5EScrollablePane(layer, "scrollpane-backing-2");
        questDescriptionScrollpane.setScrollingDisabled(false, false);
        //quest description label
        questDescriptionLabel = new H5ELabel("", layer);
        questDescriptionLabel.setWrap(true);
        questDescriptionLabel.setAlignment(Align.topLeft);
        questDescriptionLabel.setFontScale(0.7f);
        //Button to actually skip a quest (bottom right buttons)
        btnSkipSelectedQuest = new H5EButton("Skip Quest", layer);
        btnSkipSelectedQuest.getLabel().setFontScale(0.7f);
        btnSkipSelectedQuest.addButtonListener(this::openSkipDialogue);

        //Button to actually abandon a quest
        btnAbandonSelectedQuest = new H5EButton("Abandon Quest", layer);
        btnAbandonSelectedQuest.getLabel().setFontScale(0.7f);
        btnAbandonSelectedQuest.addButtonListener(this::openAbandonDialogue);

        Table table = new Table();
        table.add(questDescriptionLabel).grow();
        table.row();
        table.add(btnSkipSelectedQuest).height(30).pad(5);
        table.row();
        table.add(btnAbandonSelectedQuest).height(30).pad(5);
        questDescriptionScrollpane.add(table).top().left().grow().padLeft(5);
        window.add(questDescriptionScrollpane).grow().colspan(2);
        //******End of right side***********

        window.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                refreshDescriptionText();
            }
        });

        confirmationScreen = new GCConfirmationScreen(layer, this, "Are you sure you want to skip this quest?");
        //TODO: Test the all the  adding quest methods
    }

    /**
     * Cycle through all active quest held by this dialog to
     * see if they have achieved their objective and setCompletion the dialog
     * to reflect changes
     */
    public void updateQuestValues() {
        refresh();
    }

    /**
     * Disable/enable the skip and abandon buttons. Typically used while the user is looking at completed/skipped/abandoned quest.
     *
     * @param enable True if you want to disable the buttons. Otherwise pass false to enable them.
     */
    public void setDisabledSkipButtons(boolean enable) {
        btnSkipSelectedQuest.setDisabled(enable);
        btnAbandonSelectedQuest.setDisabled(enable);
    }


    /**
     * Set the Text to No active quest if there are no quest in the currently selected Tab
     */
    public void emptyQuestText() {
        if (questListing.size() < 1) {
            questScrollPane.setActor(questEmptyWarning);
        }

    }

    //****************REFRESH METHODS********************

    /**
     * Clears The current listing. Used when listings are changed to prevent errors from
     * adding and removing elements
     */

    public void clearListing() {
        questListing.clear();
        questListing.getButtonGroup().clear();
    }

    /**
     * Clears and displays the quest listing based which listing you're trying to view with the selected button(active, completed
     * abandoned etc)
     */
    private void refreshQuestListing() {
        if (questButtonGroup.getCheckedIndex() == 0) {
            clearListing();
            for (GCQuestListItem currentQuest : quests.getActiveQuests()) {
                questListing.addItem(currentQuest);
            }
            questScrollPane.setActor(questListing);
            setDisabledSkipButtons(false);
        }
        if (questButtonGroup.getCheckedIndex() == 1) {
            clearListing();
            for (GCQuestListItem currentQuest : quests.getCompletedQuests()) {
                questListing.addItem(currentQuest);
            }
            questScrollPane.setActor(questListing);
            setDisabledSkipButtons(true);
        }
        if (questButtonGroup.getCheckedIndex() == 2) {
            clearListing();
            for (GCQuestListItem currentQuest : quests.getSkippedQuests()) {
                questListing.addItem(currentQuest);
            }
            questScrollPane.setActor(questListing);
            setDisabledSkipButtons(true);
        }
        if (questButtonGroup.getCheckedIndex() == 3) {
            clearListing();
            for (GCQuestListItem currentQuest : quests.getAbandonedQuests()) {
                questListing.addItem(currentQuest);
            }
            questScrollPane.setActor(questListing);
            setDisabledSkipButtons(true);
        }
        emptyQuestText();
    }

    /**
     * Updates the description text based on the currently selected quest.
     * Description text is held within the GCQuestListItem component.
     */
    public void refreshDescriptionText() {
        int button = questListing.getButtonGroup().getCheckedIndex();
        if (button != -1) {
            //Get active Quest descriptions if the active Quest button is selected
            if (questButtonGroup.getCheckedIndex() == 0) {
                questDescriptionLabel.setText(quests.getActiveQuests().get(button).getDescription());
            }
            //Get completed Quest descriptions if the completed Quest button is selected
            if (questButtonGroup.getCheckedIndex() == 1) {
                questDescriptionLabel.setText(quests.getCompletedQuests().get(button).getDescription());
            }
            //Get Abandoned Quest descriptions if the skipped Quest button is selected
            if (questButtonGroup.getCheckedIndex() == 2) {
                questDescriptionLabel.setText(quests.getSkippedQuests().get(button).getDescription());
            }
            //Get Abandoned Quest descriptions if the abandoned Quest button is selected
            if (questButtonGroup.getCheckedIndex() == 3) {
                questDescriptionLabel.setText(quests.getAbandonedQuests().get(button).getDescription());
            }
        }
        // refreshQuestListing();
    }

    /**
     * Refresh both quest views
     */
    public void refresh() {
        refreshQuestListing();
        questSidebar.refresh();
    }
    //****************END OF REFRESH METHODS********************


    //****************SKIP AND ABANDON DIALOGUE METHODS********************

    /**
     * This method is called after the user confirms or denies to skip/abandon quest. Used To call the
     * appropriate method after confirmation
     */
    @Override
    public void confirmed() {
        if (skippingQuest) {
            if (confirmationScreen.isConfirmed()) {
                skipQuest();
            }
        }
        if (abandoningQuest) {
            if (confirmationScreen.isConfirmed()) {
                abandonQuest();
            }
        }
        this.open();
    }

    /**
     * Skip the currently selected quest
     */
    public void skipQuest() {
        int button = questListing.getButtonGroup().getCheckedIndex();
        GCQuestListItem questToSkip = quests.getActiveQuests().get(button);//Get selected Quest
        quests.getSkippedQuests().add(questToSkip);//Add that selected quest instance to the list of skip quest
        quests.getActiveQuests().remove(questToSkip);//Remove that quest from the user's active quest
        refresh();//Update the quest list to reflect the changes
        //Refresh quick view incase the user skips a quest that in their quick view
        refreshDescriptionText();

    }

    /**
     * Abandon the currently selected quest
     */
    public void abandonQuest() {
        int button = questListing.getButtonGroup().getCheckedIndex();
        GCQuestListItem questToAbandon = quests.getActiveQuests().get(button);//Get selected Quest
        quests.getAbandonedQuests().add(questToAbandon);//Add that selected quest instance to the list of abandoned quest
        quests.getActiveQuests().remove(questToAbandon);//Remove that quest from the user's active quest
        refreshQuestListing();//Update the quest list to reflect the changes
        refreshDescriptionText();
        questSidebar.refresh(); //Refresh quick view incase the user abandons a quest that in their quick view
    }

    /**
     * Checks if a quest in the current listing is selected. May be depreciated
     *
     * @return
     */
    public boolean isButtonSelected() {
        return questListing.getButtonGroup().getCheckedIndex() != -1;
    }

    //TODO: Add some kind of warning text if the user has nothing selected/force something to be selected.
    public void openSkipDialogue() {
        if (isButtonSelected()) {
            confirmationScreen.setText("Are you sure you want to skip this quest?");
            confirmationScreen.setTitle("Skip Quest");
            skippingQuest = true;
            abandoningQuest = false;
            confirmationScreen.open();
            this.close();
        }
    }

    public void openAbandonDialogue() {
        if (isButtonSelected()) {
            confirmationScreen.setText("Are you sure you want to Abandon this quest?");
            confirmationScreen.setTitle("Abandon Quest");
            skippingQuest = false;
            abandoningQuest = true;
            confirmationScreen.open();
            this.close();
        }
    }
    //****************END OF SKIP AND ABANDON DIALOGUE METHODS********************

    public void open() {
        window.open();
    }


    public void close() {
        window.close();
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    public void positionProportionally(float propX, float propY) {
        window.positionProportionally(propX, propY);
    }
}
