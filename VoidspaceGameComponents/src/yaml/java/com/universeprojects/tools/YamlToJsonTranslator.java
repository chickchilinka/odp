package com.universeprojects.tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.*;

public class YamlToJsonTranslator {
    public static void main(String[] args) throws IOException {
        File inputYaml = new File(args[0]);

        File outputDir = new File(args[1]);
        if (!outputDir.exists()) {
            if (!outputDir.mkdirs()) {
                throw new IOException("Unable to create resource output directory");
            }
        }
        File outputJson = new File(outputDir, replaceFileExtension(inputYaml.getName()));

        InputStream inputStream = new FileInputStream(inputYaml);

        ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
        Object obj = yamlReader.readValue(inputStream, Object.class);

        ObjectMapper jsonWriter = new ObjectMapper();
        String jsonContents = jsonWriter.writeValueAsString(obj);

        try (FileWriter writer = new FileWriter(outputJson)) {
            writer.write(jsonContents);
        }
    }

    public static String replaceFileExtension(String name) {
        return name.substring(0, name.length() - 4) + ".json";
    }
}
