package com.universeprojects.vsdata.shared;

public class CryptoTransactionData {
    public CryptoCurrency type;
    public double amount;
    public String amountText;
    public CryptoTransactionInput[] inputs;
    public CryptoTransactionOutput[] outputs;
    public String hash;
    public String timestamp;

    public CryptoTransactionData() {
    }

    public CryptoTransactionData(CryptoCurrency type, double amount, String amountText, CryptoTransactionInput[] inputs, CryptoTransactionOutput[] outputs, String hash, String timestamp) {
        this.type = type;
        this.amount = amount;
        this.amountText = amountText;
        this.inputs = inputs;
        this.outputs = outputs;
        this.hash = hash;
        this.timestamp = timestamp;
    }
}
