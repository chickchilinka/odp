package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.math.CoordinateEncoder;
import com.universeprojects.common.shared.math.UPVector;

import java.util.Objects;

@AutoSerializable({"cellX", "cellY"})
public class ServerCell implements Comparable<ServerCell> {
    public static final long SIZE = 4800;
    public long cellX;
    public long cellY;
    private Long encodedCoordinate;

    public ServerCell(long cellX, long cellY) {
        this.cellX = cellX;
        this.cellY = cellY;
    }

    public ServerCell(CellLocation cellLocation) {
        this(cellLocation.getCellX(), cellLocation.getCellY());
    }

    public ServerCell() {
    }

    public long getEncodedCoordinate() {
        if(encodedCoordinate == null) {
            synchronized (this) {
                if (encodedCoordinate == null) {
                    encodedCoordinate = CoordinateEncoder.encoderSigned.encodeLocHash((int)cellX, (int)cellY);
                }
            }
        }
        return encodedCoordinate;
    }

    public long getMinX() {
        return cellX * SIZE;
    }

    public long getMaxX() {
        return (cellX + 1) * SIZE - 1;
    }

    public long getMinY() {
        return cellY * SIZE;
    }

    public long getMaxY() {
        return (cellY + 1) * SIZE - 1;
    }

    public long getCenterX() {
        return (getMaxX() + getMinX()) / 2;
    }

    public long getCenterY() {
        return (getMaxY() + getMinY()) / 2;
    }

    @Override
    public String toString() {
        return "ServerCell{" +
            "cellX=" + cellX +
            ", cellY=" + cellY +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServerCell that = (ServerCell) o;
        return cellX == that.cellX &&
            cellY == that.cellY;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cellX, cellY);
    }

    @Override
    public int compareTo(ServerCell o) {
        int xCompare = Long.compare(cellX, o.cellX);
        if(xCompare != 0) {
            return xCompare;
        }
        return Long.compare(cellY, o.cellY);
    }

    public static ServerCell forLocation(UPVector location) {
        return new ServerCell((long)Math.floor(location.x / SIZE), (long)Math.floor(location.y / SIZE));
    }
}
