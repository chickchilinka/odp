package com.universeprojects.vsdata.shared;

public enum RecipeSlotType {
    ITEM, TOOL, TARGET
}
