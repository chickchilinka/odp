package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;

@SuppressWarnings("unused")
@AutoSerializable(value = {
    "entryId", "entryName", "entryClass", "entryDescription", "iconSpriteType", "rarityValue",
    "action", "repeats", "inventionObjectUid", "referenceId", "constructionData"},
    serializationType = SerializationType.MAP)
public class BuildQueueEntry {
    public Long entryId;
    public String entryName;
    public String entryClass;
    public String entryDescription;
    public String iconSpriteType;
    public RarityValue rarityValue;

    public InventionAction action;
    public int repeats = 1;
    public String inventionObjectUid;
    public Long referenceId;
    public RecipeConstructionData constructionData;
    public QueueEntryState state;
    enum QueueEntryState{
        Setup, Error, Approved
    }
}
