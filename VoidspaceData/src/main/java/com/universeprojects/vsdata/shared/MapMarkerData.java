package com.universeprojects.vsdata.shared;


import com.universeprojects.common.shared.annotations.AutoSerializable;

@AutoSerializable({"id","name","icon"})
public class MapMarkerData {
    public String id;
    public String name;
    public String icon;

    public MapMarkerData() {
    }

    public MapMarkerData(String id, String name, String icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }
}
