package com.universeprojects.vsdata.shared;

public enum OrgQueryMembershipType {
    NONE, PENDING, MEMBER, ADMIN
}
