package com.universeprojects.vsdata.shared;

public class CryptoTransactionInput {
    public double value;
    public String valueText;
    public String[] addresses;
    public long age;

    public CryptoTransactionInput(double value, String valueText, String[] addresses, long age) {
        this.value = value;
        this.valueText=valueText;
        this.addresses = addresses;
        this.age = age;
    }
}
