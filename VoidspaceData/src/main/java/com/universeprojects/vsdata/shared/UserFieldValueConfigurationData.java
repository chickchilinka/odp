package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.UserFieldValueConfiguration;
import com.universeprojects.gefcommon.shared.elements.UserFieldValueConfigurationType;

@AutoSerializable(value = {"description", "type", "typeConfiguration", "targetFieldName", "minLength", "maxLength", "required", "formattingErrorMessage"}, serializationType = SerializationType.MAP)
public class UserFieldValueConfigurationData implements UserFieldValueConfiguration {

    public String description;
    public UserFieldValueConfigurationType type;
    public String typeConfiguration;
    public String targetFieldName;
    public Integer minLength;
    public Integer maxLength;
    public Boolean required;
    public String formattingErrorMessage;

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public UserFieldValueConfigurationType getType() {
        return type;
    }

    @Override
    public String getTypeConfiguration() {
        return typeConfiguration;
    }

    @Override
    public String getTargetFieldName() {
        return targetFieldName;
    }

    @Override
    public Integer getMinLength() {
        return minLength;
    }

    @Override
    public Integer getMaxLength() {
        return maxLength;
    }

    @Override
    public Boolean isRequired() {
        return required;
    }

    @Override
    public String getFormattingErrorMessage() {
        return formattingErrorMessage;
    }

    public boolean isRequiredSafe() {
        return required == null || required == Boolean.TRUE;
    }

    public static UserFieldValueConfigurationData fromConfig(UserFieldValueConfiguration config) {
        UserFieldValueConfigurationData configData = new UserFieldValueConfigurationData();
        configData.description = config.getDescription();
        configData.formattingErrorMessage = config.getFormattingErrorMessage();
        configData.maxLength = config.getMaxLength();
        configData.minLength = config.getMinLength();
        configData.required = config.isRequired();
        configData.type = config.getType();
        configData.targetFieldName = config.getTargetFieldName();
        configData.typeConfiguration = config.getTypeConfiguration();
        return configData;
    }
}
