package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializableMap;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.RecipeConstruction;
import com.universeprojects.gefcommon.shared.elements.RecipeConstructionSlot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@AutoSerializable(value = {"slotData", "userFieldValues"}, serializationType = SerializationType.MAP)
public final class RecipeConstructionData implements RecipeConstruction {
    @SerializableList(elementClass = RecipeConstructionSlotData.class)
    public List<RecipeConstructionSlotData> slotData;
    @SerializableMap(keyClass = String.class, valueClass = String.class)
    public Map<String, String> userFieldValues;

    @Override
    public List<RecipeConstructionSlotData> getSlotData() {
        if(slotData == null) {
            return Collections.emptyList();
        }
        return slotData;
    }

    @Override
    public Map<String, String> getUserFieldValues() {
        if(userFieldValues == null) {
            return Collections.emptyMap();
        }
        return userFieldValues;
    }

    public void addSlot(RecipeConstructionSlotData slot) {
        if (this.slotData == null) {
            this.slotData = new ArrayList<>();
        }
        this.slotData.add(slot);
    }

    public void addUserFieldValue(String targetFieldName, String value) {
        if(this.userFieldValues == null) {
            this.userFieldValues = new LinkedHashMap<>();
        }
        this.userFieldValues.put(targetFieldName, value);
    }

    public RecipeConstructionData copy() {
        RecipeConstructionData constructionDataCopy = new RecipeConstructionData();
        if (slotData != null) {
            constructionDataCopy.slotData = new ArrayList<>();
            for (RecipeConstructionSlotData slot : slotData) {
                RecipeConstructionSlotData slotDataCopy = slot.copy();
                constructionDataCopy.slotData.add(slotDataCopy);
            }
        }
        if(userFieldValues != null) {
            constructionDataCopy.userFieldValues = new LinkedHashMap<>(userFieldValues);
        }
        return constructionDataCopy;
    }

    public static RecipeConstructionData fromRecipeConstruction(RecipeConstruction construction) {
        if (construction instanceof RecipeConstructionData) return (RecipeConstructionData) construction;
        RecipeConstructionData constructionData = new RecipeConstructionData();
        if (construction.getSlotData() != null) {
            constructionData.slotData = new ArrayList<>();
            for (RecipeConstructionSlot slot : construction.getSlotData()) {
                RecipeConstructionSlotData slotData = RecipeConstructionSlotData.fromRecipeConstructionSlot(slot);
                constructionData.slotData.add(slotData);
            }
        }
        if(construction.getUserFieldValues() != null) {
            constructionData.userFieldValues = new LinkedHashMap<>(construction.getUserFieldValues());
        }
        return constructionData;
    }
}
