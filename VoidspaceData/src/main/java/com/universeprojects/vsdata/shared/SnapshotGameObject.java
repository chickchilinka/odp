package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.gefcommon.shared.elements.GameObject;
import com.universeprojects.json.shared.serialization.SerializedDataList;
import com.universeprojects.json.shared.serialization.SerializedDataMap;

import java.util.Set;

public interface SnapshotGameObject<K> extends GameObject<K> {
    SerializedDataList getRunningEvents();

    SerializedDataMap<String, SerializedDataList> getEventSubscriptions();

    Long getCreationTick();

    String getCreatorUid();

    boolean isDestroyed();

    UPVector getActualLocation();

    Set<String> getTags();
}
