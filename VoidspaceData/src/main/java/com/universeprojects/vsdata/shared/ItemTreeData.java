package com.universeprojects.vsdata.shared;

import java.util.List;
import java.util.ArrayList;

public class ItemTreeData<T extends CategoryItemData> {
    public CategoryData category;
    public List<T> items;

    public ItemTreeData() {
        category = new CategoryData("0", "root", null);
        items = new ArrayList<T>();
    }

    public List<T> getCategoryItems(String id) {
        List<T> result = new ArrayList<T>();
        for (T item : items) {
            if (item.parentCategoryId.equals(id))
                result.add(item);
        }
        return result;
    }

    public void addCategoryToId(CategoryData entry, CategoryData cat, String parentId) {
        if (entry.id.equals(parentId)) {
            if (entry.listOfSubcategories == null)
                entry.listOfSubcategories = new ArrayList<CategoryData>();
            entry.listOfSubcategories.add(cat);
            return;
        }
        if (entry.listOfSubcategories != null)
            for (CategoryData _entry : entry.listOfSubcategories) {
                addCategoryToId(_entry, cat, parentId);
            }
    }

    public List<T> getUncatItems() {
        List<T> unCatItems = new ArrayList<T>();
        for (T item : items) {
            if (!containsCategoryId(category, item.parentCategoryId)) {
                unCatItems.add(item);
            }
        }
        return unCatItems;
    }

    private boolean containsCategoryId(CategoryData entry, String id) {
        if (entry.id.equals(id)) {
            return true;
        } else {
            boolean sum = false;
            if (entry.listOfSubcategories != null)
                for (CategoryData cat : entry.listOfSubcategories) {
                    sum = sum || containsCategoryId(cat, id);
                }
            return sum;
        }
    }
}