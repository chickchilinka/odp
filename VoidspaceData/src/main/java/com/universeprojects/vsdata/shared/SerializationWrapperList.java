package com.universeprojects.vsdata.shared;

import com.universeprojects.json.shared.serialization.SerializationWrapper;

import java.util.ArrayList;
import java.util.List;

public abstract class SerializationWrapperList {
    protected static <O> List<SerializationWrapper<O>> toSerializationWrapper (List<O> objectList) {
        List<SerializationWrapper<O>> wrapped = new ArrayList<>();
        for(O object : objectList) {
            wrapped.add(new SerializationWrapper<>(object));
        }

        return wrapped;
    }

    protected static <O> List<O> fromSerializationWrapper (List<SerializationWrapper<O>> wrappedObjectList) {
        List<O> unwrappedObjectList = new ArrayList<>();
        for (SerializationWrapper<O> wrappedObject : wrappedObjectList) {
            unwrappedObjectList.add(wrappedObject.getValue());
        }

        return unwrappedObjectList;
    }
}
