package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.reflection.InstantiationHelper;
import com.universeprojects.common.shared.reflection.ReflectionException;
import com.universeprojects.json.shared.GeneratedClassInitializationException;
import com.universeprojects.json.shared.GeneratedClassInitializer;
import com.universeprojects.json.shared.serialization.AutoSerializer;


public class VoidspaceDataClassInitializer extends GeneratedClassInitializer {

    @Override
    protected void initialize() throws GeneratedClassInitializationException {
        try {
            AutoSerializer serializer = InstantiationHelper.INSTANCE.instantiate("com.universeprojects.vsdata.shared.VoidspaceDataAutoSerializerGenerated", AutoSerializer.class);
            serializer.initializeSerializer();
        } catch (ReflectionException ex) {
            throw new GeneratedClassInitializationException(ex);
        }
    }
}
