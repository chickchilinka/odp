package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;

import java.util.Objects;

@AutoSerializable({"id", "characterName", "nickname"})
public class CharacterData{
    public String id;
    public String characterName;
    public String nickname;

    public CharacterData(){
    }

    public CharacterData(String id, String characterName, String nickname){
        this.id=id;
        this.characterName=characterName;
        this.nickname=nickname;
    }
}