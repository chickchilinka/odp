package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;

import java.util.List;
import java.util.ArrayList;

@AutoSerializable({"id", "name", "description", "parentCategoryId", "sellItemId", "price", "sellerNickname"})
public class CategorizedStoreItemData extends CategoryItemData {
    public String sellItemId;
    public CurrencyData price;
    public String sellerNickname;

    public CategorizedStoreItemData(String id, String sellItemId, String sellItemName, CurrencyData price, String sellerNickname, String categoryId) {
        super(id, "", sellItemName, "", categoryId);
        this.sellItemId = sellItemId;
        this.sellerNickname = sellerNickname;
        this.price = price;
    }

    public CategorizedStoreItemData() {
        super();
    }
}