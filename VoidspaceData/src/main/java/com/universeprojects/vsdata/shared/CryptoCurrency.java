package com.universeprojects.vsdata.shared;

public enum CryptoCurrency {
    Dogecoin("doge"), Bitcoin("btc"), Litecoin("ltc"), Ethereum("eth"), Dash("dash");

    private String text;

    CryptoCurrency(String text){
        this.text=text;
    }

    public String getText(){
        return text;
    }
}

