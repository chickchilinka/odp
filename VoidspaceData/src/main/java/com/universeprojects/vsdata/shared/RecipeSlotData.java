package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.RecipeSlot;
import com.universeprojects.gefcommon.shared.elements.RecipeSlotOption;

import java.util.ArrayList;
import java.util.List;

@AutoSerializable(value = {"slotId", "name", "description", "options"}, serializationType = SerializationType.MAP)
public final class RecipeSlotData implements RecipeSlot {
    public String slotId;
    public String name;
    public String description;
    @SerializableList(elementClass = RecipeSlotOptionData.class)
    public List<RecipeSlotOptionData> options;

    @Override
    public String getSlotId() {
        return slotId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<RecipeSlotOptionData> getOptions() {
        return options;
    }

    public static RecipeSlotData fromRecipeSlot(RecipeSlot slot) {
        if (slot instanceof RecipeSlotData) return (RecipeSlotData) slot;
        RecipeSlotData slotData = new RecipeSlotData();
        slotData.name = slot.getName();
        slotData.slotId = slot.getSlotId();
        slotData.description = slot.getDescription();
        if (slot.getOptions() != null) {
            slotData.options = new ArrayList<>();
            for (RecipeSlotOption option : slot.getOptions()) {
                RecipeSlotOptionData optionData = RecipeSlotOptionData.fromRecipeSlotOption(option);
                slotData.options.add(optionData);
            }
        }
        return slotData;
    }
}
