package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;

import java.util.Objects;

@AutoSerializable({"id", "icon", "name", "description", "parentCategoryId"})
public class CategoryItemData {
    public String id;
    public String icon;
    public String name;
    public String description;
    public String parentCategoryId;

    public CategoryItemData() {
    }

    public CategoryItemData(String id, String icon, String name, String description, String parentCategoryId) {
        this.id = id;
        this.icon = icon;
        this.name = name;
        this.description = description;
        this.parentCategoryId = parentCategoryId;
    }
}