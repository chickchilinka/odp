package com.universeprojects.vsdata.shared;

import com.universeprojects.gefcommon.shared.elements.SkillType;

public enum VoidspaceSkillType implements SkillType {
    ItemOrStructure,
    Item,
    Structure,
    Knowledge
}
