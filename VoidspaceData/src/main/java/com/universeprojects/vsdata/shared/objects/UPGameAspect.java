package com.universeprojects.vsdata.shared.objects;

import com.universeprojects.gefcommon.shared.elements.GameAspect;
import com.universeprojects.gefcommon.shared.elements.GameObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public final class UPGameAspect implements GameAspect<String> {

    final String aspectName;
    UPGameObject gameObject;

    final Map<String, Object> properties = new TreeMap<>();
    final Map<String, Object> changedProperties = new TreeMap<>();
    final Set<String> allPropertyNames = new TreeSet<>();
    final Map<String, GameObject<String>> singleSubObjects = new TreeMap<>();
    final Map<String, List<GameObject<String>>> listSubObjects = new TreeMap<>();
    final Map<String, Map<?, GameObject<String>>> mapSubObjects = new TreeMap<>();

    Integer version;

    public UPGameAspect(String aspectName) {
        this.aspectName = aspectName;
    }

    public void setPropertyRaw(String fieldName, Object value) {
        properties.put(fieldName, value);
        allPropertyNames.add(fieldName);
    }

    public void removeProperty(String name) {
        properties.remove(name);
        changedProperties.remove(name);
        allPropertyNames.remove(name);
        singleSubObjects.remove(name);
        listSubObjects.remove(name);
        mapSubObjects.remove(name);
    }

    public Object getPropertyRaw(String fieldName) {
        return properties.get(fieldName);
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Map<String, Object> getPropertyMap() {
        return properties;
    }

    @Override
    public Object getProperty(String fieldName) {
        if (changedProperties.containsKey(fieldName)) {
            return changedProperties.get(fieldName);
        }
        return properties.get(fieldName);
    }

    @Override
    public void setProperty(String fieldName, Object value) {
        changedProperties.put(fieldName, value);
        allPropertyNames.add(fieldName);
    }

    @Override
    public String getName() {
        return aspectName;
    }

    @Override
    public UPGameObject getGameObject() {
        return gameObject;
    }

    public Map<String, Object> getChangedProperties() {
        return changedProperties;
    }

    public Set<String> getPropertyNames() {
        return allPropertyNames;
    }

    @Override
    public void setSubObjectSingle(String name, GameObject<String> object) {
        singleSubObjects.put(name, UPGameObject.fromGameObjectWithLinks(object));
    }

    @Override
    public void setSubObjectList(String name, List<GameObject<String>> objectList) {
        if(objectList == null || objectList.isEmpty()) return;
        List<GameObject<String>> targetList = new ArrayList<>();
        for(GameObject<String> object : objectList) {
            targetList.add(UPGameObject.fromGameObjectWithLinks(object));
        }
        listSubObjects.put(name, targetList);
    }

    @Override
    public void setSubObjectMap(String name, Map<?, GameObject<String>> objectMap) {
        if(objectMap == null || objectMap.isEmpty()) return;
        Map<Object, GameObject<String>> targetMap = new LinkedHashMap<>();
        for (Map.Entry<?, GameObject<String>> entry : objectMap.entrySet()) {
            targetMap.put(entry.getKey(), UPGameObject.fromGameObjectWithLinks(entry.getValue()));
        }
        mapSubObjects.put(name, targetMap);
    }

    @Override
    public Map<String, GameObject<String>> getSubObjectsSingle() {
        return singleSubObjects;
    }

    @Override
    public Map<String, List<GameObject<String>>> getSubObjectsList() {
        return listSubObjects;
    }

    @Override
    public Map<String, Map<?, GameObject<String>>> getSubObjectsMap() {
        return mapSubObjects;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @SuppressWarnings("unused")
    public static UPGameAspect fromGameAspect(com.universeprojects.gefcommon.shared.elements.GameAspect<String> source) {
        return fromGameAspect(source, false);
    }

    @SuppressWarnings("unused")
    public static UPGameAspect fromGameAspect(com.universeprojects.gefcommon.shared.elements.GameAspect<String> source, boolean copyLinks) {
        if (source instanceof UPGameAspect) {
            return (UPGameAspect) source;
        }
        return copyFromGameAspect(source, copyLinks);
    }

    @SuppressWarnings("unused")
    public static UPGameAspect copyFromGameAspect(com.universeprojects.gefcommon.shared.elements.GameAspect<String> source) {
        return copyFromGameAspect(source, false);
    }

    public static UPGameAspect copyFromGameAspect(com.universeprojects.gefcommon.shared.elements.GameAspect<String> source, boolean copyLinks) {
        UPGameAspect target = new UPGameAspect(source.getName());
        target.version = source.getVersion();
        for (String propertyName : source.getPropertyNames()) {
            Object value = source.getProperty(propertyName);
            target.setPropertyRaw(propertyName, value);
        }
        if (copyLinks) {
            copyLinks(source, target);
        }
        return target;
    }

    private static void copyLinks(GameAspect<String> source, UPGameAspect target) {
        final Map<String, GameObject<String>> subObjectsSingle = source.getSubObjectsSingle();
        if(subObjectsSingle != null) {
            for (Map.Entry<String, GameObject<String>> entry : subObjectsSingle.entrySet()) {
                target.setSubObjectSingle(entry.getKey(), UPGameObject.fromGameObjectWithLinks(entry.getValue()));
            }
        }
        final Map<String, List<GameObject<String>>> subObjectsList = source.getSubObjectsList();
        if (subObjectsList != null) {
            for (Map.Entry<String, List<GameObject<String>>> entry : subObjectsList.entrySet()) {
                final List<GameObject<String>> list = entry.getValue();
                final List<GameObject<String>> upGameObjectList = new ArrayList<>();
                for(GameObject<String> gameObject : list) {
                    upGameObjectList.add(UPGameObject.fromGameObjectWithLinks(gameObject));
                }
                target.setSubObjectList(entry.getKey(), upGameObjectList);
            }
        }
        final Map<String, Map<?, GameObject<String>>> subObjectsMap = source.getSubObjectsMap();
        if (subObjectsMap != null) {
            for (Map.Entry<String, Map<?, GameObject<String>>> entry : subObjectsMap.entrySet()) {
                final Map<?, GameObject<String>> map = entry.getValue();
                final Map<Object, GameObject<String>> upGameObjectMap = new LinkedHashMap<>();
                for (Map.Entry<?, GameObject<String>> mapEntry : map.entrySet()) {
                    upGameObjectMap.put(mapEntry.getKey(), UPGameObject.fromGameObjectWithLinks(mapEntry.getValue()));
                }
                target.setSubObjectMap(entry.getKey(), upGameObjectMap);
            }
        }
    }

    @Override
    public String toString() {
        return "UPGameAspect{" +
            "aspectName='" + aspectName + '\'' +
            ", gameObject=" + getUid() +
            ", properties=" + properties +
            ", changedProperties=" + changedProperties +
            '}';
    }

    public String getUid() {
        if(gameObject == null) {
            return null;
        }
        return gameObject.getUid();
    }
}
