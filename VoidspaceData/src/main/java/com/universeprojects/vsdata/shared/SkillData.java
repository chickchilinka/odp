package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.gefcommon.shared.elements.Skill;
import com.universeprojects.vsdata.shared.objects.UPGameObject;

import java.util.Objects;

@AutoSerializable({"id", "name", "description", "icon", "baseTimeToRunSecs", "baseSuccessRate", "gameObject", "skillType","parentCategoryId"})
public final class SkillData implements Skill {
    public Long id;
    public String name;
    public String description;
    public String icon;
    public Long baseTimeToRunSecs;
    public Double baseSuccessRate;
    public UPGameObject gameObject;
    public VoidspaceSkillType skillType;
    public Long parentCategoryId;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getIcon() {
        return icon;
    }

    public Long getBaseTimeToRunSecs() {
        return baseTimeToRunSecs;
    }

    @Override
    public Double getBaseSuccessRate() {
        return baseSuccessRate;
    }

    @Override
    public String toString() {
        return "SkillData{" +
                "name='" + name + '\'' +
                ", descr='" + description + '\'' +
                ", icon='" + icon + '\'' +
                ", secs=" + baseTimeToRunSecs +
                '}';
    }

    public static SkillData fromSkill(Skill skill) {
        if (skill instanceof SkillData) return (SkillData) skill;
        SkillData skillData = new SkillData();
        skillData.id = skill.getId();
        skillData.name = skill.getName();
        skillData.description = skill.getDescription();
        skillData.icon = skill.getIcon();
        skillData.baseTimeToRunSecs = skill.getBaseTimeToRunSecs();
        skillData.baseSuccessRate = skill.getBaseSuccessRate();
        if (skill.getGameObject()!=null)
            skillData.gameObject = UPGameObject.fromGameObjectWithLinks(skill.getGameObject());
        if(skill.getSkillType() != null) {
            skillData.skillType = VoidspaceSkillType.valueOf(skill.getSkillType().name());
        } else {
            skillData.skillType = VoidspaceSkillType.Item;
        }
        skillData.parentCategoryId = skill.getParentCategoryId();
        return skillData;
    }

    @Override
    public UPGameObject getGameObject() {
        return gameObject;
    }

    @Override
    public VoidspaceSkillType getSkillType() {
        return skillType;
    }

    @Override
    public Long getParentCategoryId() {
        return parentCategoryId;
    }


    public String getShortName() {
        return name.substring(name.indexOf("'s ")+3, name.indexOf(" technique #"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SkillData skillData = (SkillData) o;
        return Objects.equals(id, skillData.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
