package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;

@AutoSerializable({"id", "name", "review", "rating"})
public class OrganizationReviewData {
    public Object id;
    public String name;
    public String review;
    public float rating;

    public OrganizationReviewData() {
    }

    public OrganizationReviewData(Object id, String name, String review, float rating) {
        this.id = id;
        this.name = name;
        this.review = review;
        this.rating = rating;
    }

    //for debug
    @Override
    public String toString() {
        String output = id.toString() + "\n";
        output += name + "\n";
        output += review + "\n";
        output += Float.toString(rating);
        return output;
    }
}