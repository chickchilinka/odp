package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.RecipeFieldRequirementOperator;
import com.universeprojects.gefcommon.shared.elements.RecipeFieldRequirementOption;
import com.universeprojects.json.shared.serialization.SerializationWrapper;

@AutoSerializable(value = {"aspect", "field", "operator", "internalValue"}, serializationType = SerializationType.MAP)
public final class RecipeFieldRequirementOptionData implements RecipeFieldRequirementOption {
    public String aspect;
    public String field;
    public RecipeFieldRequirementOperator operator;
    public SerializationWrapper<?> internalValue;

    @Override
    public String getAspect() {
        return aspect;
    }

    @Override
    public String getField() {
        return field;
    }

    @Override
    public RecipeFieldRequirementOperator getOperator() {
        return operator;
    }

    @Override
    public Object getValue() {
        if (internalValue == null) return null;
        return internalValue.getValue();
    }

    public void setValue(Object value) {
        this.internalValue = new SerializationWrapper<>(value);
    }

    public static RecipeFieldRequirementOptionData fromRecipeFieldRequirementOption(RecipeFieldRequirementOption option) {
        if (option instanceof RecipeFieldRequirementOptionData) return (RecipeFieldRequirementOptionData) option;
        RecipeFieldRequirementOptionData fieldRequirementData = new RecipeFieldRequirementOptionData();
        fieldRequirementData.aspect = option.getAspect();
        fieldRequirementData.field = option.getField();
        fieldRequirementData.operator = option.getOperator();
        fieldRequirementData.internalValue = new SerializationWrapper<>(option.getValue());
        return fieldRequirementData;
    }

}
