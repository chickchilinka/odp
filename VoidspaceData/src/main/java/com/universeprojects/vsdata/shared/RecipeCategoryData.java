package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.RecipeCategory;
import com.universeprojects.gefcommon.shared.elements.RecipeSlot;

import java.util.ArrayList;
import java.util.List;

@AutoSerializable(value = {"name", "required", "slots"}, serializationType = SerializationType.MAP)
public final class RecipeCategoryData implements RecipeCategory {
    public String name;
    public boolean required;
    @SerializableList(elementClass = RecipeSlotData.class)
    public List<RecipeSlotData> slots;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public List<RecipeSlotData> getSlots() {
        return slots;
    }

    public static RecipeCategoryData fromRecipeCategory(RecipeCategory category) {
        if (category instanceof RecipeCategoryData) return (RecipeCategoryData) category;
        RecipeCategoryData categoryData = new RecipeCategoryData();
        categoryData.name = category.getName();
        categoryData.required = category.isRequired();
        if (category.getSlots() != null) {
            categoryData.slots = new ArrayList<>();
            for (RecipeSlot slot : category.getSlots()) {
                RecipeSlotData slotData = RecipeSlotData.fromRecipeSlot(slot);
                categoryData.slots.add(slotData);
            }
        }
        return categoryData;
    }

    public RecipeSlotType toSlotType(){
        RecipeSlotType type;
        if (name.contains("Materials")) {
            type = RecipeSlotType.ITEM;
        } else if (name.contains("Tools")) {
            type = RecipeSlotType.TOOL;
        } else if (name.contains("Target")) {
            type = RecipeSlotType.TARGET;
        } else {
            throw new IllegalStateException("Unexpected category name: " + name);
        }
        return type;
    }

}
