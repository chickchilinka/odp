package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;

@AutoSerializable({"platform", "displayStoreName", "canRate"})
public class RatingClientInfo {
    public String platform;
    public String displayStoreName;
    public boolean canRate;

    public RatingClientInfo(String platform, boolean canRate) {
        this(platform, platform, canRate);
    }

    public RatingClientInfo(String platform, String displayStoreName, boolean canRate) {
        this.platform = platform;
        this.displayStoreName = displayStoreName;
        this.canRate = canRate;
    }

    public RatingClientInfo() {
    }
}
