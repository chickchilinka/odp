package com.universeprojects.vsdata.shared;

public enum RarityValue {
    JUNK, //Dark Grey
    COMMON, //Grey
    UNCOMMON, //Green
    RARE, //Blue
    EPIC, //Purple
    LEGENDARY, //Yellow

    INVENTION_PROTOTYPE,
    INVENTION_SKILL,
    INVENTION_EXPERIMENT,
    NONE
}
