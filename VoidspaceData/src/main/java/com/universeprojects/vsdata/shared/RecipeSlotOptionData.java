package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.RecipeFieldRequirement;
import com.universeprojects.gefcommon.shared.elements.RecipeSlotOption;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@AutoSerializable(value = {"name", "requiredQuantity", "requiredAspects", "fieldRequirements", "toolUsageTimeFraction"}, serializationType = SerializationType.MAP)
public final class RecipeSlotOptionData implements RecipeSlotOption {
    public String name;
    @SerializableList(elementClass = String.class)
    public List<String> requiredAspects;
    @SerializableList(elementClass = RecipeFieldRequirementData.class)
    public List<RecipeFieldRequirementData> fieldRequirements;
    public int requiredQuantity;
    public Float toolUsageTimeFraction;

    @Override
    public List<String> getRequiredAspects() {
        return requiredAspects;
    }

    public void setRequiredAspects(List<String> requiredAspects) {
        this.requiredAspects = requiredAspects;
    }

    @Override
    public List<RecipeFieldRequirementData> getFieldRequirements() {
        return fieldRequirements;
    }

    @Override
    public Float getToolUsageTimeFraction() {
        return toolUsageTimeFraction;
    }

    public void setFieldRequirements(List<RecipeFieldRequirementData> fieldRequirements) {
        this.fieldRequirements = fieldRequirements;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getRequiredQuantity() {
        return requiredQuantity;
    }

    public static RecipeSlotOptionData fromRecipeSlotOption(RecipeSlotOption option) {
        if (option instanceof RecipeSlotOptionData) return (RecipeSlotOptionData) option;
        RecipeSlotOptionData optionData = new RecipeSlotOptionData();
        optionData.name = option.getName();
        optionData.requiredQuantity = option.getRequiredQuantity();
        optionData.toolUsageTimeFraction = option.getToolUsageTimeFraction();
        final Collection<String> requiredAspects = option.getRequiredAspects();
        optionData.requiredAspects = new ArrayList<>();
        if (requiredAspects != null) {
            optionData.requiredAspects.addAll(requiredAspects);
        }
        final Collection<? extends RecipeFieldRequirement> fieldRequirements = option.getFieldRequirements();
        optionData.fieldRequirements = new ArrayList<>();
        if (fieldRequirements != null) {
            for (RecipeFieldRequirement requirement : fieldRequirements) {
                RecipeFieldRequirementData fieldRequirementData =
                        RecipeFieldRequirementData.fromRecipeFieldRequirement(requirement);
                optionData.fieldRequirements.add(fieldRequirementData);
            }
        }
        return optionData;
    }
}
