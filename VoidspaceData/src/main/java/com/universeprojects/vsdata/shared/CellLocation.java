package com.universeprojects.vsdata.shared;

public interface CellLocation {
    Long getCellX();
    Long getCellY();
}
