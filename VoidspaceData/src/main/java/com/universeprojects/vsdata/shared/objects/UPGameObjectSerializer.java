package com.universeprojects.vsdata.shared.objects;

import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.common.shared.types.UPList;
import com.universeprojects.common.shared.types.UPMap;
import com.universeprojects.gefcommon.shared.elements.GameAspect;
import com.universeprojects.gefcommon.shared.elements.GameObject;
import com.universeprojects.json.shared.JSONArray;
import com.universeprojects.json.shared.JSONAware;
import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.json.shared.parser.JSONParserFactory;
import com.universeprojects.json.shared.parser.ParseException;
import com.universeprojects.json.shared.serialization.SerializationException;
import com.universeprojects.json.shared.serialization.SerializedDataList;
import com.universeprojects.json.shared.serialization.SerializedDataMap;
import com.universeprojects.json.shared.serialization.Serializer;
import com.universeprojects.json.shared.serialization.SerializerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UPGameObjectSerializer implements Serializer {

    private static boolean initialized = false;

    public synchronized static void initialize() {
        if (initialized) {
            return;
        }
        UPGameObjectSerializer serializer = new UPGameObjectSerializer();
        SerializerFactory.registerSerializer(UPGameObject.class, serializer);
        SerializerFactory.registerSerializer(UPGameAspect.class, serializer);
        SerializerFactory.registerSerializer(GameObject.class, serializer);
        SerializerFactory.registerSerializer(GameAspect.class, serializer);
        initialized = true;
    }

    @Override
    public Object serialize(Object instance) {
        return serialize(instance, false);
    }

    public Object serialize(Object instance, boolean humanReadable) {
        if (instance instanceof UPGameObject) {
            final UPGameObject gameObject = (UPGameObject) instance;
            JSONArray array = new JSONArray();
            array.add(gameObject.getUid());
            array.add(gameObject.getCreatorUid());
            array.add(gameObject.getCreationTick());
            array.add(gameObject.isDestroyed());
            array.add(serializeValue(gameObject.getActualLocation(), humanReadable));
            array.add(SerializerFactory.serialize(gameObject.getRunningEvents()));
            array.add(SerializerFactory.serialize(gameObject.getEventSubscriptions()));
            JSONArray aspectData = new JSONArray();
            for (String aspectClass : UPGameObject.coreAspectClasses) {
                final UPGameAspect coreAspect = gameObject.getAspect(aspectClass);
                if(coreAspect != null) {
                    aspectData.add(serialize(coreAspect, humanReadable));
                }
            }
            for (UPGameAspect aspect : gameObject.getAspects()) {
                if(UPGameObject.coreAspectClasses.contains(aspect.aspectName)) {
                    continue;
                }
                aspectData.add(serialize(aspect, humanReadable));
            }
            array.add(aspectData);
            final Set<String> tags = gameObject.getTags();
            if (tags != null) {
                array.add(new JSONArray(tags));
            } else {
                array.add(null);
            }
            final Map<String, Object> additionalProperties = gameObject.getAdditionalProperties();
            if (additionalProperties != null && !additionalProperties.isEmpty()) {
                array.add(SerializerFactory.createSerializedDataMap(additionalProperties));
            } else {
                array.add(null);
            }
            array.add(new JSONArray(gameObject.getAddedAspectNames()));

            return array;
        } else if (instance instanceof UPGameAspect) {
            final UPGameAspect gameAspect = (UPGameAspect) instance;
            JSONArray array = new JSONArray();
            array.add(gameAspect.getName());
            JSONObject propertyData = new JSONObject();
            for (Map.Entry<String, Object> entry : gameAspect.properties.entrySet()) {
                Object value = entry.getValue();
                Object serialized = serializeValue(value, humanReadable);
                propertyData.put(entry.getKey(), serialized);
            }
            array.add(propertyData);
            JSONObject changedPropertyData = new JSONObject();
            for (Map.Entry<String, Object> entry : gameAspect.changedProperties.entrySet()) {
                Object value = entry.getValue();
                Object serialized = serializeValue(value, humanReadable);
                changedPropertyData.put(entry.getKey(), serialized);
            }
            array.add(changedPropertyData);
            JSONObject singleLinkData = new JSONObject();
            for (Map.Entry<String, GameObject<String>> entry : gameAspect.singleSubObjects.entrySet()) {
                singleLinkData.put(entry.getKey(), serialize(entry.getValue(), humanReadable));
            }
            array.add(singleLinkData);
            JSONObject listLinkData = new JSONObject();
            for (Map.Entry<String, List<GameObject<String>>> entry : gameAspect.listSubObjects.entrySet()) {
                JSONArray listEntries = new JSONArray();
                for (GameObject<String> object : entry.getValue()) {
                    listEntries.add(serialize(object, humanReadable));
                }
                listLinkData.put(entry.getKey(), listEntries);
            }
            array.add(listLinkData);
            JSONObject mapLinkData = new JSONObject();
            for (Map.Entry<String, Map<?, GameObject<String>>> entry : gameAspect.mapSubObjects.entrySet()) {
                JSONObject mapEntries = new JSONObject();
                for (Map.Entry<?, GameObject<String>> mapEntry : entry.getValue().entrySet()) {
                    mapEntries.put(String.valueOf(SerializerFactory.serialize(mapEntry.getKey())),
                        serialize(mapEntry.getValue(), humanReadable));
                }
                mapLinkData.put(entry.getKey(), mapEntries);
            }
            array.add(mapLinkData);
            array.add(gameAspect.getVersion());
            return array;
        }
        throw new IllegalArgumentException("Cannot find serializer for class " + instance.getClass());
    }

    protected Object serializeValue(Object value, boolean humanReadable) {
        if (humanReadable) {
            if (value instanceof UPVector) {
                JSONObject json = new JSONObject();
                final UPVector vec = (UPVector) value;
                json.put("x", vec.x);
                json.put("y", vec.y);
                return json;
            }
        }
        if (isNumberOrBoolean(value)) {
            return value;
        } else if (value instanceof Map && !(value instanceof JSONAware) && !(value instanceof UPMap)) {
            //noinspection unchecked,rawtypes
            return SerializerFactory.createSerializedDataMap((Map) value);
        } else if (value instanceof List && !(value instanceof JSONAware) && !(value instanceof UPList)) {
            //noinspection rawtypes
            return SerializerFactory.createSerializedDataList(((List) value).toArray());
        } else {
            return SerializerFactory.serialize(value);
        }
    }

    private boolean isNumberOrBoolean(Object value) {
        return value instanceof Long || value instanceof Integer || value instanceof Double || value instanceof Float || value instanceof Boolean;
    }

    @Override
    @SuppressWarnings({"unchecked", "OverlyLongMethod"})
    public <T> T deserialize(Object element, Class<T> type) {
        if (type == UPGameObject.class) {
            JSONArray array = getJSONArray(element);
            if (array == null) {
                throw new NullPointerException();
            }
            int i = -1;
            String uid = (String) array.get(++i);
            String creatorUid = (String) array.get(++i);
            Long creationTick = (Long) array.get(++i);
            Boolean destroyed = (Boolean) array.get(++i);
            UPVector actualLocation = SerializerFactory.deserialize(array.get(++i), UPVector.class);
            SerializedDataList runningEvents = SerializerFactory.deserialize(array.get(++i), SerializedDataList.class);
            //noinspection rawtypes
            SerializedDataMap eventSubscriptions = SerializerFactory.deserialize(array.get(++i), SerializedDataMap.class);
            JSONArray aspectData = (JSONArray) array.get(++i);
            JSONArray tagsJson = (JSONArray) getSafe(array, ++i);
            JSONObject additionalPropertiesJson = (JSONObject) getSafe(array, ++i);
            JSONArray addedAspects = (JSONArray) getSafe(array, ++i);
            List<UPGameAspect> aspects = new ArrayList<>();
            for (Object aspectObj : aspectData) {
                aspects.add(deserialize(aspectObj, UPGameAspect.class));
            }
            UPGameObject gameObject = new UPGameObject(uid, aspects);
            gameObject.setCreatorUid(creatorUid);
            gameObject.setCreationTick(creationTick);
            gameObject.setActualLocation(actualLocation);
            gameObject.setRunningEvents(runningEvents);
            gameObject.setEventSubscriptions(eventSubscriptions);
            if (destroyed != null) {
                gameObject.setDestroyed(destroyed);
            }
            if (tagsJson != null) {
                //noinspection rawtypes
                gameObject.setTags(new HashSet<>((List) tagsJson));
            }
            if (additionalPropertiesJson != null) {
                //noinspection rawtypes
                final SerializedDataMap additionalPropertiesSerializedMap = SerializerFactory.deserialize(additionalPropertiesJson, SerializedDataMap.class);
                if(additionalPropertiesSerializedMap != null) {
                    gameObject.setAdditionalProperties(additionalPropertiesSerializedMap.deserializeData(String.class, Object.class));
                }
            }
            if(addedAspects != null && !addedAspects.isEmpty()) {
                //noinspection rawtypes
                Set<String> addedAspectNames = new LinkedHashSet<>((List) addedAspects);
                gameObject.setAddedAspectNames(addedAspectNames);
            }
            return (T) gameObject;
        } else if (type == UPGameAspect.class) {
            JSONArray array = getJSONArray(element);
            if (array == null) {
                throw new NullPointerException();
            }
            String name = (String) array.get(0);
            Map<String, Object> properties = (JSONObject) array.get(1);
            Map<String, Object> changedProperties = (JSONObject) array.get(2);
            UPGameAspect gameAspect = new UPGameAspect(name);
            gameAspect.properties.putAll(properties);
            gameAspect.changedProperties.putAll(changedProperties);
            gameAspect.allPropertyNames.addAll(properties.keySet());
            gameAspect.allPropertyNames.addAll(changedProperties.keySet());
            if (array.size() > 5) {
                Map<String, JSONObject> singleLinkData = (Map<String, JSONObject>) array.get(3);
                for (Map.Entry<String, JSONObject> entry : singleLinkData.entrySet()) {
                    gameAspect.setSubObjectSingle(entry.getKey(), deserialize(entry.getValue(), UPGameObject.class));
                }
                Map<String, JSONArray> listLinkData = (Map<String, JSONArray>) array.get(4);
                for (Map.Entry<String, JSONArray> entry : listLinkData.entrySet()) {
                    List<GameObject<String>> list = new ArrayList<>();
                    for (Object gameObjectData : entry.getValue()) {
                        list.add(deserialize(gameObjectData, UPGameObject.class));
                    }
                    gameAspect.setSubObjectList(entry.getKey(), list);
                }
                Map<String, JSONObject> mapLinkData = (Map<String, JSONObject>) array.get(5);
                for (Map.Entry<String, JSONObject> entry : mapLinkData.entrySet()) {
                    Map<Object, GameObject<String>> map = new LinkedHashMap<>();
                    for (Map.Entry<String, Object> gameObjectEntry : entry.getValue().entrySet()) {
                        map.put(gameObjectEntry.getKey(), deserialize(gameObjectEntry.getValue(), UPGameObject.class));
                    }
                    gameAspect.setSubObjectMap(entry.getKey(), map);
                }
            }
            if(array.size() > 6) {
                Object versionObj = array.get(6);
                if(versionObj instanceof Number) {
                    gameAspect.version = ((Number) versionObj).intValue();
                } else {
                    gameAspect.version = null;
                }
            }
            return (T) gameAspect;
        } else if (type == GameObject.class) {
            return (T) deserialize(element, UPGameObject.class);
        } else if (type == GameAspect.class) {
            return (T) deserialize(element, UPGameAspect.class);
        }
        throw new IllegalArgumentException("Cannot find deserializer for class " + type);
    }

    private Object getSafe(JSONArray array, int index) {
        return index < array.size() ? array.get(index) : null;
    }

    private JSONArray getJSONArray(Object element) {
        if (element instanceof JSONArray) {
            return (JSONArray) element;
        } else if (element instanceof String) {
            try {
                return (JSONArray) JSONParserFactory.getParser().parse((String) element);
            } catch (ParseException e) {
                throw new SerializationException(e);
            }
        } else {
            return null;
        }
    }
}
