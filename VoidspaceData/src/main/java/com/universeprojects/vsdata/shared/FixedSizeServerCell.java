package com.universeprojects.vsdata.shared;

public class FixedSizeServerCell extends ServerCell {
    private final long minX;
    private final long maxX;
    private final long minY;
    private final long maxY;

    public FixedSizeServerCell(long minX, long maxX, long minY, long maxY) {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
    }

    public FixedSizeServerCell(long width, long height) {
        this.minX = -width / 2;
        this.maxX = width / 2;
        this.minY = -height / 2;
        this.maxY = height / 2;
    }

    public FixedSizeServerCell() {
        this.minX = Long.MIN_VALUE;
        this.maxX = Long.MAX_VALUE;
        this.minY = Long.MIN_VALUE;
        this.maxY = Long.MAX_VALUE;
    }

    @Override
    public long getMinX() {
        return minX;
    }

    @Override
    public long getMaxX() {
        return maxX;
    }

    @Override
    public long getMinY() {
        return minY;
    }

    @Override
    public long getMaxY() {
        return maxY;
    }
}
