package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializationType;

import java.util.ArrayList;
import java.util.List;

@AutoSerializable(value = {"items"}, serializationType = SerializationType.MAP)
public class OrgListData {

    @SerializableList(elementClass = OrgListDataItem.class)
    public List<OrgListDataItem> items = new ArrayList<>();

    public OrgListData() {
    }

    public static OrgListData createNew() {
        return new OrgListData();
    }

    public OrgListData add(OrgListData other) {
        for (OrgListDataItem item : other.items) {
            add(item.name, item.orgID, item.tag, item.member, item.admin);
        }
        return this;
    }

    public OrgListData add(String name, long orgID, String tag) {
        return add(name, orgID, tag, false, false);
    }

    public OrgListData add(String name, long orgID, String tag, boolean member, boolean admin) {
        int index = items.size();
        items.add(new OrgListDataItem(index, name, orgID, tag, member, admin));
        return this;
    }

    public OrgListDataItem get(int index) {
        return items.get(index);
    }

    public int size() {
        return items.size();
    }

    @AutoSerializable(value = {"index","name","orgID","tag", "member", "admin"}, serializationType = SerializationType.MAP)
    public static class OrgListDataItem {
        public int index;
        public String name;
        public long orgID;
        public String tag;
        public boolean member;
        public boolean admin;

        public OrgListDataItem(int index, String name, long orgID, String tag, boolean member, boolean admin) {
            this.index = index;
            this.name = name;
            this.orgID = orgID;
            this.tag = tag;
            this.member = member;
            this.admin = admin;
        }

        public OrgListDataItem() {
        }

        public String getName() {
            return name;
        }

        public long getOrgID() {
            return orgID;
        }

        public String getTag() {
            return tag;
        }

        public boolean isAdmin() {
            return admin;
        }
    }
}
