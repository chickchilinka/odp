package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;

@AutoSerializable({"rating", "neverShowAgain"})
public class GameRatingData {
    public Integer rating;
    public boolean neverShowAgain;

    public GameRatingData(){ }

    public GameRatingData(Integer rating, boolean neverShowAgain) {
        if(rating != null && rating > 0) {
            this.rating = rating;
        }
        this.neverShowAgain = neverShowAgain;
    }

    @Override
    public String toString() {
        return "GameRatingData{" +
            "rating=" + rating +
            ", neverShowAgain=" + neverShowAgain +
            '}';
    }
}

