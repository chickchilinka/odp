package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;

import java.util.List;
import java.util.ArrayList;

@AutoSerializable({"name","content"})
public class BookChapterData{
    public String name;
    public String content;

    public BookChapterData() {
    }

    public BookChapterData(String name, String content) {
        this.name = name;
        this.content = content;
    }

}
