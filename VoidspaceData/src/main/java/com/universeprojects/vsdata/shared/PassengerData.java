package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;

@AutoSerializable({"id", "characterName", "nickname", "icon"})
public class PassengerData extends CharacterData{
    public String icon;

    public PassengerData(){
        super();
    }

    public PassengerData(String id, String characterName, String nickname, String icon) {
        this.id=id;
        this.characterName=characterName;
        this.nickname=nickname;
        this.icon = icon;
    }
}