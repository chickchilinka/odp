package com.universeprojects.vsdata.shared;

public enum BookEditableMode {
    EVERYONE, NONE
}
