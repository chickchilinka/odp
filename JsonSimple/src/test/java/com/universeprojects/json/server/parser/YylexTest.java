package com.universeprojects.json.server.parser;

import com.universeprojects.json.shared.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;

public class YylexTest {

    @Test
	public void testYylex() throws Exception{
		String s="\"\\/\"";
		System.out.println(s);
		StringReader in = new StringReader(s);
		Yylex lexer=new Yylex(in);
		Yytoken token=lexer.yylex();
		Assert.assertEquals(Yytoken.TYPE_VALUE,token.type);
		Assert.assertEquals("/",token.value);
		
		s="\"abc\\/\\r\\b\\n\\t\\f\\\\\"";
		System.out.println(s);
		in = new StringReader(s);
		lexer=new Yylex(in);
		token=lexer.yylex();
		Assert.assertEquals(Yytoken.TYPE_VALUE,token.type);
		Assert.assertEquals("abc/\r\b\n\t\f\\",token.value);
		
		s="[\t \n\r\n{ \t \t\n\r}";
		System.out.println(s);
		in = new StringReader(s);
		lexer=new Yylex(in);
		token=lexer.yylex();
		Assert.assertEquals(Yytoken.TYPE_LEFT_SQUARE,token.type);
		token=lexer.yylex();
		Assert.assertEquals(Yytoken.TYPE_LEFT_BRACE,token.type);
		token=lexer.yylex();
		Assert.assertEquals(Yytoken.TYPE_RIGHT_BRACE,token.type);
		
		s="\b\f{";
		System.out.println(s);
		in = new StringReader(s);
		lexer=new Yylex(in);
		ParseException err=null;
		try{
			token=lexer.yylex();
		}
		catch(ParseException e){
			err=e;
			System.out.println("error:"+err);
			Assert.assertEquals(ParseException.ERROR_UNEXPECTED_CHAR, e.getErrorType());
			Assert.assertEquals(0,e.getPosition());
			Assert.assertEquals(new Character('\b'),e.getUnexpectedObject());
		}
		catch(IOException ie){
			throw ie;
		}
		Assert.assertTrue(err!=null);
		
		s="{a : b}";
		System.out.println(s);
		in = new StringReader(s);
		lexer=new Yylex(in);
		err=null;
		try{
			lexer.yylex();
			token=lexer.yylex();
		}
		catch(ParseException e){
			err=e;
			System.out.println("error:"+err);
			Assert.assertEquals(ParseException.ERROR_UNEXPECTED_CHAR, e.getErrorType());
			Assert.assertEquals(new Character('a'),e.getUnexpectedObject());
			Assert.assertEquals(1,e.getPosition());
		}
		catch(IOException ie){
			throw ie;
		}
		Assert.assertTrue(err!=null);
	}

}
