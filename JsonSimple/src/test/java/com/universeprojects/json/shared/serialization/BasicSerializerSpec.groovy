package com.universeprojects.json.shared.serialization

import com.universeprojects.common.shared.math.UPVector
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

/**
 * This is a test suite for the BasicSerializer component
 */
@Stepwise
// <-- MUST BE A SEQUENTIAL SUITE (see last test)
class BasicSerializerSpec extends Specification {

    @Shared
    def serializer = new BasicSerializer()

    @Shared
    def reportedTypes = new HashSet<Class>()

    /**
     * Each stand-alone test targets a specific grid type, and it must report that type using this method
     */
    def reportType(Class type) {
        assert type != null, "argument can't be null"
        reportedTypes.add(type)
        return type
    }

    def "null is serialized to null (type-agnostic)"() {
        expect:
        serializer.serialize(null) == null
    }

    def "null is de-serialized to null, for any data type"() {
        expect:
        serializer.deserialize(null, (Class) type) == null

        where:
        type << BasicSerializer.supportedTypes
    }

    def "String serialize/deserialize"() {
        given:
        def type = reportType(String.class)

        expect:
        serializer.serialize(original) == original
        serializer.deserialize(original, type) == original

        where:
        original                || _
        ""                      || _
        "         "             || _
        "a"                     || _
        "abc xyz 123"           || _
        "12345"                 || _
        "xyz" * Short.MAX_VALUE || _
    }

    def "Integer serialize/deserialize"() {
        given:
        def type = reportType(Integer.class)

        expect:
        serializer.serialize(original) == original              // serialize (int/int)
        serializer.deserialize(original, type) == original      // <-- de-serialize (int/int)
        serializer.deserialize(str_version, type) == original    // <-- de-serialize (str/int)

        where:
        original          || str_version
        0                 || "0"
        1                 || "1"
        -1                || "-1"
        12345             || "12345"
        -12345            || "-12345"
        Integer.MIN_VALUE || "" + Integer.MIN_VALUE
        Integer.MAX_VALUE || "" + Integer.MAX_VALUE
    }

    def "Long serialize/deserialize"() {
        given:
        def type = reportType(Long.class)

        expect:
        serializer.serialize(original) == original              // serialize (long > long)
        serializer.deserialize(original, type) == original      // de-serialize (long > long)
        serializer.deserialize(str_version, type) == original    // de-serialize (str > long)

        where:
        original       || str_version
        0L             || "0"
        1L             || "1"
        -1L            || "-1"
        12345L         || "12345"
        -12345L        || "-12345"
        Long.MIN_VALUE || "" + Long.MIN_VALUE
        Long.MAX_VALUE || "" + Long.MAX_VALUE
    }

    def "Float serialize/deserialize"() {
        given:
        def type = reportType(Float.class)

        expect:
        serializer.serialize(original) == serialized
        serializer.deserialize(serialized, type) == original

        where:
        original        || serialized
        0f              || "D0"
        1f              || "D3vs0000000000"
        -1f             || "D-4040000000000"
        123.45f         || "D40nmspj000000"
        -123.45f        || "D-3v8936d000000"
        Float.MIN_VALUE || "D3d80000000000"
        Float.MAX_VALUE || "D4frvvvvg00000"
    }

    def "Double serialize/deserialize"() {
        given:
        def type = reportType(Double.class)

        expect:
        serializer.serialize(original) == serialized
        serializer.deserialize(serialized, type) == original

        where:
        original        || serialized
        0d              || "D0"
        1d              || "D3vs0000000000"
        -1d             || "D-4040000000000"
        123.45d         || "D40nmspj6cpj6d"
        -123.45d        || "D-3v8936cpj6cpj"
        Float.MIN_VALUE || "D3d80000000000"
        Float.MAX_VALUE || "D4frvvvvg00000"
    }

    @SuppressWarnings("GroovyPointlessBoolean")
    def "Boolean serialize"() {
        given:
        reportType(Boolean.class)

        expect:
        serializer.serialize(original) == serialized

        where:
        original || serialized
        true     || true
        false    || false
    }

    @SuppressWarnings("GroovyPointlessBoolean")
    def "Boolean deserialize"() {
        given:
        def type = reportType(Boolean.class)

        expect:
        serializer.deserialize(original, type) == deserialized

        where:
        original || deserialized
        true     || true
        false    || false
        "true"   || true
        "false"  || false
    }

    def "Boolean deserialize (invalid input)"() {
        given:
        def type = reportType(Boolean.class)

        when:
        serializer.deserialize(input, type)

        then:
        thrown SerializationException

        where:
        input    || _
        ""       || _
        "   "    || _
        "abc"    || _
        "abc 23" || _

    }

    private enum Color {
        RED, GREEN, BLUE
    }

    def "Enum serialize"() {
        given:
        reportType(Enum.class)

        expect:
        serializer.serialize(original) == original.name()

        where:
        original << [
                Color.BLUE,
                Color.GREEN,
                Color.RED,
        ]
    }

    def "Enum deserialize"() {
        given:
        reportType(Enum.class)

        expect:
        serializer.deserialize(original, Color.class) == deserialized

        where:
        original || deserialized
        "BLUE"   || Color.BLUE
        "GREEN"  || Color.GREEN
        "RED"    || Color.RED
    }

    def "Enum deserialize (invalid input)"() {
        given:
        reportType(Enum.class)

        when:
        serializer.deserialize(input, Color.class)

        then:
        thrown SerializationException

        where:
        input || _
        // non-integer-convertible inputs
        "" || _
        "   " || _
        "abc" || _
        "123x" || _
        // invalid indices (out of bounds)
        "-1" || _
        "-99999" || _
        "3" || _
        "99999" || _
        -1 || _
        -99999 || _
        3 || _
        99999 || _
    }

    //TODO: proper tests
    def "Dummy-Test to prevent errors"() {
        reportType(SerializedDataList.class);
        reportType(SerializedDataMap.class);
        reportType(UPVector.class);
    }

    /**
     * At the end, we verify that the types that were tested correspond with the serializer's supported types.
     * This is put place, to protect against future addition of supported types without test coverage.
     */
    def "check that all supported types have been tested"() {
        def untestedTypes = new HashSet<>();
        untestedTypes.addAll(BasicSerializer.supportedTypes);
        untestedTypes.removeAll(reportedTypes);
        assert untestedTypes.isEmpty(), "The following types don't have test coverage:\n" + untestedTypes.join("\n") + "\n"

        expect:
        1 == 1 //<-- this is just to identify this method as a test
    }
    // THIS MUST BE THE LAST TEST IN THE SEQUENCE!
    // NOTE: using Spock's cleanupSpec() is not ideal for this purpose, because it doesn't seem to play well with intentional exceptions

}
