package com.universeprojects.json.shared.parser;

import com.universeprojects.json.server.parser.JSONServerParser;

import java.util.List;
import java.util.Map;

/**
 * Container factory for creating containers for JSON object and JSON array.
 *
 * @author FangYidong<fangyidong   @   yahoo.com.cn>
 * @see JSONServerParser#parse(java.io.Reader, ContainerFactory)
 */
@SuppressWarnings("rawtypes")
public interface ContainerFactory {
    /**
     * @return A Map instance to store JSON object, or null if you want to use com.universeprojects.json.shared.JSONObject.
     */
    Map createObjectContainer();

    /**
     * @return A List instance to store JSON array, or null if you want to use com.universeprojects.json.shared.JSONArray.
     */
    List creatArrayContainer();
}
