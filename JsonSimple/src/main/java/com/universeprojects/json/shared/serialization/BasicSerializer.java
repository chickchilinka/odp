package com.universeprojects.json.shared.serialization;

import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.common.shared.types.UPList;
import com.universeprojects.common.shared.types.UPListImpl;
import com.universeprojects.common.shared.types.UPMap;
import com.universeprojects.common.shared.types.UPMapImpl;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.json.shared.JSONArray;
import com.universeprojects.json.shared.JSONAware;
import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.json.shared.parser.JSONParserFactory;
import com.universeprojects.json.shared.parser.ParseException;

import java.util.Map;

/**
 * This serializer handles the basic data types, provided by the language / libraries
 */
public class BasicSerializer implements Serializer {

    static final Class<?>[] supportedTypes = new Class<?>[]{
        String.class,
        Boolean.class,
        Integer.class,
        Long.class,
        Float.class,
        Double.class,
        Enum.class,
        Class.class,
        UPList.class,
        UPListImpl.class,
        UPMap.class,
        UPMapImpl.class,
        SerializedDataList.class,
        SerializedDataMap.class,
        UPVector.class,
        SerializationWrapper.class,
    };
    public static final int LONG_RADIX = 32;

    @Override
    public Object serialize(Object element) {
        if (element == null || element instanceof JSONAware) {
            return element;
        } else if (element instanceof String) {
            return element;
        } else if (element instanceof Enum) {
            return ((Enum<?>) element).name();
        } else if (element instanceof Double) {
            return serializeDoubleLongBits((Number) element);
        } else if (element instanceof Float) {
            return serializeFloatTeIntBits((Number) element);
        } else if (element instanceof Integer) {
            return element;
        } else if (element instanceof Long) {
            return element;
        } else if (element instanceof Boolean) {
            return element;
        } else if (element instanceof Class) {
            return SerializerFactory.getSimpleName((Class<?>) element);
        } else if (element instanceof UPVector) {
            UPVector coord = (UPVector) element;
            JSONArray arr2 = new JSONArray();
            arr2.add(serialize(coord.x));
            arr2.add(serialize(coord.y));
            return arr2;
        } else if (element instanceof SerializationWrapper) {
            //noinspection rawtypes
            SerializationWrapper wrapper = (SerializationWrapper) element;

            final Object value = wrapper.getValue();
            JSONArray arr2 = new JSONArray();
            if(value == null) {
                arr2.add(null);
                arr2.add(null);
            } else {
                arr2.add(SerializerFactory.serialize(value));
                arr2.add(SerializerFactory.getSimpleClassName(value));
            }
            return arr2;
        } else if (element instanceof UPList) {
            UPList<?> list = (UPList<?>) element;
            Class<?> elementClass = list.getElementClass();
            JSONArray arr = new JSONArray();
            arr.add(getSimpleName(elementClass));
            for (Object o : list) {
                arr.add(SerializerFactory.serialize(o));
            }
            return arr;
        } else if (element instanceof UPMap) {
            UPMap<?, ?> map = (UPMap<?, ?>) element;
            Class<?> keyClass = map.getKeyClass();
            Class<?> valClass = map.getValueClass();
            JSONArray arr = new JSONArray();
            arr.add(getSimpleName(keyClass));
            arr.add(getSimpleName(valClass));
            JSONObject jo = new JSONObject();
            for (Map.Entry<?, ?> en : map.entrySet()) {
                Object key = en.getKey();
                if(key == null) {
                    Log.warn("Detected null key in UPMap "+element);
                    continue;
                }
                jo.put(String.valueOf(SerializerFactory.serialize(key)), SerializerFactory.serialize(en.getValue()));
            }
            arr.add(jo);
            return arr;
        } else {
            throw new SerializationException("Unsupported data type: " + element.getClass().getCanonicalName());
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> T deserialize(Object element, Class<T> clazz) {
        if (clazz == null) {
            throw new SerializationException("Type cannot be null");
        }

        if (element == null) {
            return null;
        } else if (clazz == String.class) {
            return (T) element;
        } else if (clazz.isEnum()) {
            String name = element.toString();
            try {
                return (T) Enum.valueOf((Class<? extends Enum>) clazz, name);
            } catch (IllegalArgumentException ex) {
                throw new SerializationException(element, clazz, ex);
            }
        } else if (clazz == Boolean.class) {
            if (element instanceof Boolean) {
                return (T) element;
            } else if (element instanceof String) {
                String str = (String) element;
                if ("true".equals(str)) {
                    return (T) Boolean.TRUE;
                }
                if ("false".equals(str)) {
                    return (T) Boolean.FALSE;
                }
                // Boolean.valueOf() is not used here for a reason:
                // it is bug-prone because it does not explicitly recognize "false"
                throw new SerializationException("Can't convert value " + Strings.inQuotes(str) + " to a Boolean");
            }
        } else if (clazz == Double.class) {
            if (element instanceof Number) {
                return (T) Double.valueOf(((Number) element).doubleValue());
            } else if (element instanceof String) {
                String str = (String) element;
                if (str.startsWith("D")) {
                    return (T) deserializeDoubleLongBits(str);
                } else if (str.startsWith("F")) {
                    Float fl = deserializeFloatTeIntBits(str);
                    return (T) Double.valueOf(fl.doubleValue());
                } else {
                    return (T) Double.valueOf(str);
                }
            }
        } else if (clazz == Float.class) {
            if (element instanceof Number) {
                return (T) Float.valueOf(((Number) element).floatValue());
            } else if (element instanceof String) {
                String str = (String) element;
                if (str.startsWith("F")) {
                    Float fl = deserializeFloatTeIntBits(str);
                    return (T) fl;
                } else if(str.startsWith("D")) {
                    Double dbl = deserializeDoubleLongBits(str);
                    return (T) Float.valueOf(dbl.floatValue());
                } else {
                    return (T) Float.valueOf(str);
                }
            }
        } else if (clazz == Long.class) {
            if (element instanceof Number) {
                return (T) Long.valueOf(((Number) element).longValue());
            } else if (element instanceof String) {
                return (T) Long.valueOf((String) element);
            }
        } else if (clazz == Integer.class) {
            if (element instanceof Number) {
                return (T) Integer.valueOf(((Number) element).intValue());
            } else if (element instanceof String) {
                return (T) Integer.valueOf((String) element);
            }
        } else if (clazz == Class.class) {
            return (T) SerializerFactory.getCompatibleClassForName((String) element);
        } else if (clazz == UPVector.class) {
            JSONArray arr = getJSONArray(element);
            Float x = SerializerFactory.deserialize(arr.get(0), Float.class);
            Float y = SerializerFactory.deserialize(arr.get(1), Float.class);
            return (T) new UPVector(x, y);
        } else if (clazz == SerializedDataList.class) {
            if (element instanceof String)
                return (T) new SerializedDataList((String) element, null, null);
            JSONArray arr = (JSONArray) element;
            return (T) new SerializedDataList(null, null, arr);
        } else if (clazz == SerializedDataMap.class) {
            if (element instanceof String)
                return (T) new SerializedDataMap((String) element, null, null);
            JSONObject obj = (JSONObject) element;
            return (T) new SerializedDataMap(null, null, obj);
        } else if (clazz == SerializationWrapper.class) {
            JSONArray json = getJSONArray(element);
            Object serializedValue = json.get(0);
            Class wrapperClass = deserialize(json.get(1), Class.class);
            Object value = null;
            if (wrapperClass != null) {
                value = SerializerFactory.deserialize(serializedValue, wrapperClass);
            }
            return (T) new SerializationWrapper<>(value);
        } else if (clazz == UPList.class || clazz == UPListImpl.class) {
            JSONArray arr = getJSONArray(element);

            boolean first = true;
            Class<?> elementClass = null;
            UPList list = null;
            for (Object val : arr) {
                if (first) {
                    first = false;
                    elementClass = SerializerFactory.getCompatibleClassForSimpleName((String) val);
                    list = new UPListImpl(elementClass);
                } else {
                    Object listVal = SerializerFactory.deserialize(val, elementClass);
                    list.add(listVal);
                }
            }
            return (T) list;
        } else if (clazz == UPMap.class || clazz == UPMapImpl.class) {
            JSONArray arr = (JSONArray) element;

            Class<?> keyClass = SerializerFactory.getCompatibleClassForSimpleName((String) arr.get(0));
            Class<?> valClass = SerializerFactory.getCompatibleClassForSimpleName((String) arr.get(1));

            JSONObject jsonMap = (JSONObject) arr.get(2);
            UPMap map = new UPMapImpl(keyClass, valClass);

            for (Map.Entry en : jsonMap.entrySet()) {
                Object rawKey = en.getKey();
                if(rawKey == null) {
                    Log.warn("Null key detected for UPMap on deserialization");
                    continue;
                }
                Object key = SerializerFactory.deserialize(rawKey, keyClass);
                Object val = SerializerFactory.deserialize(en.getValue(), valClass);
                map.put(key, val);
            }
            return (T) map;
        } else {
            throw new SerializationException("Unsupported data type: " + clazz.getCanonicalName());
        }

        throw new SerializationException(element, clazz);
    }

    public static Double deserializeDoubleLongBits(String s) {
        return Double.longBitsToDouble(Long.valueOf(s.substring(1), LONG_RADIX));
    }

    public static String serializeDoubleLongBits(Number n) {
        return "D" + Long.toString(Double.doubleToLongBits(n.doubleValue()), LONG_RADIX);
    }

    public static Float deserializeFloatTeIntBits(String s) {
        return Float.intBitsToFloat(Integer.valueOf(s.substring(1), LONG_RADIX));
    }

    public static String serializeFloatTeIntBits(Number n) {
        return "F" + Integer.toString(Float.floatToIntBits(n.floatValue()), LONG_RADIX);
    }

    private JSONArray getJSONArray(Object element) {
        if (element instanceof JSONArray)
            return (JSONArray) element;
        else if (element instanceof String) {
            try {
                return (JSONArray) JSONParserFactory.getParser().parse((String) element);
            } catch (ParseException e) {
                throw new SerializationException(e);
            }
        } else
            return null;
    }

    private static String getSimpleName(Class<?> clazz) {
        Dev.checkNotNull(clazz);
        String name = clazz.getName();
        int dotIndex = name.lastIndexOf(".");
        if (dotIndex == -1) return name;
        return name.substring(dotIndex + 1);
    }

}
