package com.universeprojects.json.shared;


import java.util.LinkedHashSet;
import java.util.Set;

public abstract class GeneratedClassInitializer {

    private static final Set<Class<? extends GeneratedClassInitializer>> initializedClasses = new LinkedHashSet<>();

    public void runInitialization() {
        synchronized (initializedClasses) {
            if (!initializedClasses.contains(getClass())) {
                initialize();
                initializedClasses.add(getClass());
            }
        }
    }

    protected abstract void initialize() throws GeneratedClassInitializationException;
}
