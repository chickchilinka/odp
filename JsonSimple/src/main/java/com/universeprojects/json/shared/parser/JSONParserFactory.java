package com.universeprojects.json.shared.parser;

import com.universeprojects.common.shared.util.GwtIncompatible;
import com.universeprojects.json.server.parser.JSONServerParser;

public class JSONParserFactory {

    private static ParserHelper parserHelper;

    @GwtIncompatible
    public static void initServerParser() {
        if (parserHelper == null) {
            parserHelper = new ServerParserHelper();
        }
    }

    public static void setParserHelper(ParserHelper parserHelper) {
        JSONParserFactory.parserHelper = parserHelper;
    }

    public static JSONParser getParser() {
        return parserHelper.getParser();
    }

    @GwtIncompatible
    public static JSONServerParser getServerParser() {
        if (parserHelper == null) parserHelper = new ServerParserHelper();
        return (JSONServerParser) parserHelper.getParser();
    }


    public interface ParserHelper {
        JSONParser getParser();
    }

    @SuppressWarnings("NonJREEmulationClassesInClientCode")
    @GwtIncompatible
    private static class ServerParserHelper implements ParserHelper {
        final ThreadLocal<JSONServerParser> serverParser = new ThreadLocal<JSONServerParser>() {
            @Override
            protected JSONServerParser initialValue() {
                return new JSONServerParser();
            }
        };

        @Override
        public JSONParser getParser() {
            return serverParser.get();
        }


    }
}
