package com.universeprojects.common.shared.callable;

public interface ReturningCallable0Args<R> extends ReturningCallable<R> {
    R call();
}
