package com.universeprojects.common.shared.callable;

public interface ReturningCallable2Args<R, A1, A2> extends ReturningCallable<R> {
    R call(A1 arg1, A2 arg2);
}
