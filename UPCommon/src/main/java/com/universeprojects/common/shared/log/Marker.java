package com.universeprojects.common.shared.log;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;


public class Marker implements org.slf4j.Marker {
    protected final Map<String, Marker> childMarkers = new TreeMap<>();
    protected Marker parent;
    protected MarkerFilter filter;
    public String name;

    protected Marker(String name) {
        this.name = name;
    }

    public Marker() {
    }

    public MarkerFilter findFilter() {
        if (filter != null && filter.getFilterType() != null) {
            return filter;
        } else if (parent != null) {
            return parent.findFilter();
        } else {
            return null;
        }
    }

    public MarkerFilter getFilter() {
        return filter;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void add(org.slf4j.Marker reference) {
        addChildMarker((Marker) reference);
    }

    @Override
    public boolean remove(org.slf4j.Marker reference) {
        return false;
    }

    @Override
    public boolean hasChildren() {
        return !childMarkers.isEmpty();
    }

    @Override
    public boolean hasReferences() {
        return parent != null;
    }

    @Override
    public Iterator iterator() {
        return Collections.emptyIterator();
    }

    @Override
    public boolean contains(org.slf4j.Marker other) {
        //noinspection SuspiciousMethodCalls
        return childMarkers.containsValue(other);
    }

    @Override
    public boolean contains(String name) {
        return childMarkers.containsKey(name);
    }

    public void setFilter(MarkerFilter filter) {
        this.filter = filter;
    }

    protected void addChildMarker(Marker child) {
        childMarkers.put(child.name, child);
        child.parent = this;
    }

    @SuppressWarnings("unused")
    public synchronized Marker getChildMarker(String name) {
        Marker marker = childMarkers.get(name);
        if (marker == null) {
            marker = MarkerFactory.getInstance().getMarker(this, name);
        }
        return marker;
    }


}
