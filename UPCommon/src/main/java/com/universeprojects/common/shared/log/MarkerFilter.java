package com.universeprojects.common.shared.log;

import java.io.Serializable;


public class MarkerFilter implements Serializable {
    private String markerName;
    private MarkerFilterType filterType;

    @SuppressWarnings("unused")
    public MarkerFilter() {

    }

    public MarkerFilter(String markerName) {
        this.markerName = markerName;
    }

    @SuppressWarnings("unused")
    public String getMarkerName() {
        return markerName;
    }

    public MarkerFilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(MarkerFilterType filterType) {
        this.filterType = filterType;
    }
}
