package com.universeprojects.common.shared.reflection;

import com.universeprojects.common.shared.util.GwtIncompatible;

@GwtIncompatible
public class SimpleInstantiationHelper extends InstantiationHelper {

    public static void initialize() {
        if (InstantiationHelper.INSTANCE == null) {
            InstantiationHelper.INSTANCE = new SimpleInstantiationHelper();
        }
    }

    @Override
    public <T> T instantiate(String className, Class<T> targetClass) throws ReflectionException {
        try {
            final Class<?> clazz = Class.forName(className);
            //noinspection unchecked
            return (T) clazz.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            throw new ReflectionException(ex);
        }
    }
}
