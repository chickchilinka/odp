package com.universeprojects.common.shared.profiler;

public class ProfilerNull implements Profiler {

    private ProfilerNull() {
    }

    public static ProfilerNull instance = new ProfilerNull();

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public void enable() {
    }

    @Override
    public void disable() {
    }

    @Override
    public ProfilerAutoCloseable start(String name) {
        return null;
    }

    @Override
    public void end(String name) {
    }

    @Override
    public void incCounter(String counterKey) {
    }

    @Override
    public void clearAll() {
    }

    @Override
    public void observeTickStart() {

    }

    @Override
    public void printAndReset() {
    }

    public void ensureRoot() {
    }
}
