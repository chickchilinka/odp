package com.universeprojects.common.shared.profiler;

public interface Profiler {

    boolean isEnabled();

    void enable();

    void disable();

    ProfilerAutoCloseable start(String name);

    void end(String name);

    void incCounter(String counterKey);

    void clearAll();

    void observeTickStart();

    void printAndReset();

    void ensureRoot();

}