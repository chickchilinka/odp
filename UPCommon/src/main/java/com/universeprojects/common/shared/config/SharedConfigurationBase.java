package com.universeprojects.common.shared.config;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class SharedConfigurationBase extends ConfigurationBase {

    protected final Map<String, String> dataMap = new TreeMap<>();
    protected final Map<String, String> dataMap_view = Collections.unmodifiableMap(dataMap);

    public SharedConfigurationBase() {

    }

    protected SharedConfigurationBase(SharedConfigurationBase base) {
        dataMap.putAll(base.dataMap);
    }

    @Override
    public void writeProperty(String propertyName, String propertyValue) {
        dataMap.put(propertyName, propertyValue);
    }

    public void writeProperty(String propertyName, Object propertyValue) {
        writeProperty(propertyName, String.valueOf(propertyValue));
    }

    @Override
    protected String readProperty(String propertyName, String defaultValue) {
        String result = dataMap.get(propertyName);
        if(result != null || dataMap.containsKey(propertyName)) {
            return result;
        } else {
            return defaultValue;
        }
    }

    @Override
    public void loadConfiguration() {
        super.loadConfiguration();
    }
}
