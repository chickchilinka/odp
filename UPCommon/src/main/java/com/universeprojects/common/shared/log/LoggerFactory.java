package com.universeprojects.common.shared.log;

import org.slf4j.ILoggerFactory;

import java.util.Map;
import java.util.TreeMap;


public class LoggerFactory implements ILoggerFactory {
    private static final LoggerFactory instance = new LoggerFactory();
    private LogProcessor processor;
    private final Map<String, Logger> loggers = new TreeMap<>(new LoggerNameComparator());

    public static final String ROOT_LOGGER_NAME = "root";

    private LoggerFactory() {
    }

    public static LoggerFactory getInstance() {
        return instance;
    }

    public Logger getRootLogger() {
        return getLogger(ROOT_LOGGER_NAME);
    }

    public LoggerConfigManager getLoggerConfigManager() {
        return processor.getLoggerConfigManager();
    }

    public Map<String, Logger> getLoggers() {
        return loggers;
    }

    public void setProcessor(LogProcessor processor) {
        this.processor = processor;
    }

    public LogProcessor getProcessor() {
        return processor;
    }

    @Override
    public Logger getLogger(String name) {
        if (name == null) {
            name = ROOT_LOGGER_NAME;
        }
        synchronized (loggers) {
            Logger logger = loggers.get(name);
            if (logger == null) {
                logger = createLogger(name);
                loggers.put(name, logger);
            }
            return logger;
        }
    }

    protected Logger createLogger(String name) {
        if (processor == null) {
            processor = new SimpleJULLogProcessor();
        }
        LoggerConfig config = getLoggerConfigManager().findLoggerConfig(name);
        Logger logger = processor.createLogger(name);
        logger.setLoggerConfig(config);
        return logger;
    }

    public Logger getLogger(Class clazz) {
        String loggerName = ROOT_LOGGER_NAME;
        if (clazz != null) {
            loggerName = clazz.getCanonicalName();
        }
        return getLogger(loggerName);
    }
}
