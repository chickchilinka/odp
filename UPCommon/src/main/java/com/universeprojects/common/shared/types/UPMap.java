package com.universeprojects.common.shared.types;

import java.util.Map;
import java.util.Set;

public interface UPMap<K, V> extends Map<K, V> {
    Class<K> getKeyClass();

    Class<V> getValueClass();

    Set<Map.Entry<K, V>> modifiableEntrySet();
}
