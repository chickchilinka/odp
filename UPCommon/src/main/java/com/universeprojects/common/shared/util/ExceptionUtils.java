package com.universeprojects.common.shared.util;

public final class ExceptionUtils {

    /**
     * Converts the stack trace of an exception to a String
     */
    public static String getStackTrace(Throwable th) {
        String ret = "";
        while (th != null) {
            if (ret != "")
                ret += "\nCaused by: ";
            ret += th.toString();
            for (StackTraceElement sTE : th.getStackTrace())
                ret += "\n  at " + sTE;
            th = th.getCause();
        }

        return ret;
    }
}
