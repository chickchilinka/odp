package com.universeprojects.common.shared.util;

public class Check {

    public static boolean equal(Integer a, Integer b) {
        return equalGeneric(a, b);
    }

    public static boolean equal(Double a, Double b) {
        return equalGeneric(a, b);
    }

    public static boolean equal(Float a, Float b) {
        return equalGeneric(a, b);
    }

    // hiding generic signature for now
    private static boolean equalGeneric(Object a, Object b) {
        return (a == null && b == null) || (a != null && a.equals(b));
    }

}
