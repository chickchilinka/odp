package com.universeprojects.common.shared.callable;

public interface ReturningCallable3Args<R, A1, A2, A3> extends ReturningCallable<R> {
    R call(A1 arg1, A2 arg2, A3 arg3);
}
