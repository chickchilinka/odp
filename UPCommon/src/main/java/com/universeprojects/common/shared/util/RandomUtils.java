package com.universeprojects.common.shared.util;

import java.util.Random;

@SuppressWarnings("unused")
public class RandomUtils {

	private static Random rnd = new Random(System.currentTimeMillis());
	
	public static Random getRandom() {
		return rnd;
	}

	public static boolean randomBoolean()
	{
		return random(0,1)==0;
	}

	public static int random(int to) {
		return random(rnd, 0, to);
	}

	/**
	 * Returns a random integer within 0 and a specified upper bound, INCLUSIVE of bounds.
	 */
	public static int random(Random rnd, int to) {
		return random(rnd, 0, to);
	}
	
	/**
	 * Returns a random integer within the specified lower and upper bounds, INCLUSIVE of bounds.
	 */
	public static int random(Random random, int from, int to) 	{
		if(from == to) {
			return from;
		}
		if (from > to) {
			Dev.fail("requested upper bound " + to + " is less than lower bound " + from);
		}
		
		int span = to - from;
		int value = random.nextInt(span+1);
		return value + from;
	}

	/**
	 * Returns a random integer within the specified lower and upper bounds, INCLUSIVE of bounds.
	 */
	public static int random(int from, int to) 	{
		return random(rnd, from, to);
	}

	/**
	 * Returns a random double within the specified lower and upper bounds.
	 */
	public static double random(double from, double to) {
		if(from == to) {
			return from;
		}
		if (from > to) {
			Dev.fail("requested upper bound " + to + " is less than lower bound " + from);
		}

		double span = to - from;
		return span * rnd.nextDouble() + from;
	}

	public static float random(float from, float to) {
		return random(rnd, from, to);
	}

	/**
	 * Returns a random double within the specified lower and upper bounds.
	 */
	public static float random(Random rnd, float from, float to) {
		if(from == to) {
			return from;
		}
		if (from > to) {
            float hold = to;
            to = from;
            from = hold;
		}
		float span = to - from;
		return span * rnd.nextFloat() + from;
	}

	/**
	 * Returns a random string within the specified length boundaries, with characters from a default char set
     */
	public static String randomString(int minLength, int maxLength) {
		return randomString(rnd, minLength, maxLength, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890");
	}

	/**
	 * Returns a random string within the specified length boundaries, with characters from a specified char set
     */
	public static String randomString(int minLength, int maxLength, String chars) {
		return randomString(rnd, minLength, maxLength, chars);
	}


	/**
	 * Returns a random string within the specified length boundaries, with characters from a default char set
	 */
	public static String randomString(Random rnd, int minLength, int maxLength) {
		return randomString(rnd, minLength, maxLength, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890");
	}

	/**
	 * Returns a random string within the specified length boundaries, with characters from a specified char set
	 */
	public static String randomString(Random rnd, int minLength, int maxLength, String chars) {
		StringBuilder res = new StringBuilder();
		int length = minLength + rnd.nextInt(maxLength);
		for (int i = 0; i < length; i++) {
			res.append(chars.charAt(rnd.nextInt(chars.length())));
		}
		return res.toString();
	}

	public static String randomString(int length) {
		return randomString(length, length);
	}
}
