package com.universeprojects.common.shared.annotations;

public enum SerializationType {
    ARRAY, MAP
}
