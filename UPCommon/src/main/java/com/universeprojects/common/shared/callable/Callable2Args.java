package com.universeprojects.common.shared.callable;

public interface Callable2Args<A1, A2> extends Callable {
    void call(A1 arg1, A2 arg2);
}
