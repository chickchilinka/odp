package com.universeprojects.common.shared.profiler;

import com.universeprojects.common.shared.log.Logger;

public class ProfilerClient extends ProfilerBase {
    protected ProfilerClient() {
        super(Logger.getLogger("Profiler Client"));
    }

    @Override
    public long getTimeMicro() {
        return System.currentTimeMillis() * 1000;
    }

    @Override
    public String format(long number) {
        return (number / 1000) + "." + ((number / 10) % 100);
    }

    public static final Profiler instance = new ProfilerClient();
}
