package com.universeprojects.common.shared.util;

import com.universeprojects.common.shared.callable.Callable1Args;

import java.util.Objects;

public class ActivateDeactivateWrapper<T> {
    private T value;
    private Callable1Args<T> deactivateListener;
    private Callable1Args<T> activateListener;

    public ActivateDeactivateWrapper(T value) {
        this(value, null, null);
    }

    public ActivateDeactivateWrapper() {
    }

    public ActivateDeactivateWrapper(T value, Callable1Args<T> deactivateListener, Callable1Args<T> activateListener) {
        this.deactivateListener = deactivateListener;
        this.activateListener = activateListener;
        set(value);
    }

    public void setDeactivateListener(Callable1Args<T> deactivateListener) {
        this.deactivateListener = deactivateListener;
    }

    public void setActivateListener(Callable1Args<T> activateListener) {
        this.activateListener = activateListener;
    }

    public T get() {
        return value;
    }

    public void set(T value) {
        if (Objects.equals(value, this.value))
            return;

        if (this.value != null && deactivateListener != null) {
            deactivateListener.call(this.value);
            this.value = null;
        }
        this.value = value;
        if (this.value != null && activateListener != null) {
            activateListener.call(this.value);
        }
    }
}
