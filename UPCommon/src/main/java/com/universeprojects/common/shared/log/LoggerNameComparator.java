package com.universeprojects.common.shared.log;

import java.util.Comparator;


public class LoggerNameComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        if (o1.equals(o2)) {
            return 0;
        } else if (o1.equals(LoggerFactory.ROOT_LOGGER_NAME)) {
            return 1;
        } else if (o2.equals(LoggerFactory.ROOT_LOGGER_NAME)) {
            return -1;
        }
        int lengthComparison = o2.length() - o1.length();
        if (lengthComparison != 0) {
            return lengthComparison;
        }
        return o1.compareTo(o2);
    }
}
