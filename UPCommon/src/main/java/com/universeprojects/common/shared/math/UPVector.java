package com.universeprojects.common.shared.math;

import com.universeprojects.common.shared.util.Strings;

@SuppressWarnings("unused")
public class UPVector {

    public static UPVector ZERO_VECTOR = new UPVector();

    public final float x, y;

    public UPVector() {
        this(0.f,0.f);
    }

    public UPVector(float x, float y) {
        if(Float.isInfinite(x) || Float.isInfinite(y)) {
            throw new IllegalArgumentException("Argument is not finite x="+x+", y="+y);
        }
        this.x = x;
        this.y = y;
    }

    public UPVector(float x, float y, float factor) {
        this(x*factor, y*factor);
    }

    public UPVector copy() {
        return new UPVector(x, y);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public UPVector norm() {
        float norm = UPMath.sqrt(x * x + y * y);
        float newX = x / norm;
        float newY = y / norm;
        return new UPVector(newX, newY);
    }

    public UPVector add(float x, float y) {
        if(x == 0f && y == 0f) {
            return this;
        }
        return new UPVector(this.x + x, this.y + y);
    }

    public float length() {
        if(isZero()) {
            return 0f;
        }
        return UPMath.distance(0, 0, x, y);
    }

    public float length2() {
        if(isZero()) {
            return 0f;
        }
        return UPMath.distance2(0, 0, x, y);
    }

    public UPVector add(UPVector p) {
        if(p.isZero()) {
            return this;
        }
        return add(p.x, p.y);
    }

    public UPVector subtract(UPVector p) {
        if(p.isZero()) {
            return this;
        }
        return add(-p.x, -p.y);
    }

    public UPVector multiply(float v) {
        if(v == 1d) {
            return this;
        }
        return new UPVector(x * v, y * v);
    }

    public UPVector divide(float v) {
        if(v == 1d) {
            return this;
        }
        return new UPVector(x / v, y / v);
    }

    public float dot(UPVector p) {
        return this.x * p.x + this.y * p.y;
    }

    public UPVector rotate(float angleRad) {
        return rotateDeg(UPMath.toDegrees(angleRad));
    }

    public UPVector rotateDeg(float angleDeg) {
        if(isZero() || angleDeg == 0f) {
            return this;
        }
        float newX = x;
        float newY = y;
        if (angleDeg % 360 != 0) {
            float rad = UPMath.toRadians(angleDeg);
            float sin = UPMath.sin(rad);
            float cos = UPMath.cos(rad);
            newX = x * cos - y * sin;
            newY = x * sin + y * cos;
        }
        return new UPVector(newX, newY);
    }

    public UPVector scale(float sc) {
        return scale(sc, sc);
    }

    public UPVector scale(float scaleX, float scaleY) {
        return new UPVector(x * scaleX, y * scaleY);
    }

    public float distance(UPVector p) {
        return distance(p.x, p.y);
    }

    public float distance(float x, float y) {
        return UPMath.distance(this.x, this.y, x, y);
    }

    public float distance2(UPVector p) {
        return distance2(p.x, p.y);
    }

    public float distance2(float x, float y) {
        return UPMath.distance2(this.x, this.y, x, y);
    }

    public float angle(UPVector p) {
        return UPMath.angle(this.x, this.y, p.x, p.y);
    }

    public float angleDeg(UPVector p) {
        return UPMath.angleDeg(this.x, this.y, p.x, p.y);
    }

    @Override
    public String toString() {
        String sx = Strings.fromDouble(x, 4);
        String sy = Strings.fromDouble(y, 4);

        return "( " + sx + " , " + sy + " )";
    }


    @Override
    @SuppressWarnings("WrapperTypeMayBePrimitive")
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        Float floatX = x;
        Float floatY = y;
        result = prime * result + floatX.hashCode();
        result = prime * result + floatY.hashCode();
        return result;
    }

    public boolean equals(float x, float y) {
        return x == this.x && y == this.y;
    }

    public boolean equalsWithinEpsilon(float x, float y, float epsilon) {
        float dx = x - this.x;
        float dy = y - this.y;
        return dx * dx + dy * dy <= epsilon * epsilon;
    }

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UPVector)) {
            return false;
        }
        UPVector other = (UPVector) obj;
        if (!Float.valueOf(x).equals(other.x)) {
            return false;
        }
        if (!Float.valueOf(y).equals(other.y)) {
            return false;
        }
        return true;
    }

    public boolean equalsWithinEpsilon(Object obj, float epsilon) {
        if (equals(obj)) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UPVector)) {
            return false;
        }
        UPVector other = (UPVector) obj;
        return distance2(other) <= epsilon * epsilon;
    }

    public boolean isZero() {
        return x == 0f && y == 0f;
    }
}
