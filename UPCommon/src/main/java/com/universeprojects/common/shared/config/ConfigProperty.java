package com.universeprojects.common.shared.config;

public class ConfigProperty<T> {

    protected final String name;
    protected final ConfigPropertyType type;
    protected final T defaultValue;
    protected final boolean notNull;
    private final ConfigurationBase configurationBase;
    private T value = null;
    private boolean manuallySet = false;

    /**
     * Constructs a configuration property object, which describes the property
     * and has the ability to hold a value resolved for this property
     *
     * @param name The name of the property, for example "voidspace.database.url"
     *
     * @param type The data type of the property - {@link ConfigPropertyType}
     *
     * @param defaultValue The default value to use when a configured value is not available
     *                     When this is NULL, it implies that the property is REQUIRED!
     *                     This is because the check() method would fail whenever it encounters a null value.
     * @param  notNull if true this property will reject null values
     */
    ConfigProperty(ConfigurationBase configurationBase, String name, ConfigPropertyType type, T defaultValue, boolean notNull) {
        this.configurationBase = configurationBase;
        this.name = name;
        this.type = type;
        this.defaultValue = defaultValue;
        this.notNull = notNull;
    }

    /**
     * Sets a value for this property. Only intended to be used by framework.
     */
    protected void setValueInternal(T value) {
        this.value = value;
        manuallySet = false;
    }

    public void setValue(T value) {
        this.value = value;
        manuallySet = true;
        configurationBase.onManualWrite(this);
    }

    protected void reset() {
        this.value = defaultValue;
        this.manuallySet = false;
    }

    /**
     * Returns the value that's currently set for this property
     */
    public T getValue() {
        return value != null ? value : defaultValue;
    }

    public String getName() {
        return name;
    }

    /**
     * Checks the value - intended to be called when the configuration has been loaded.
     * Throws an exception if the value is not set.
     */
    protected void check() {
        if (notNull && value == null) {
            throw new RuntimeException("Required configuration property not set: " + name);
        }
    }

    public boolean isManuallySet() {
        return manuallySet;
    }
}
