package com.universeprojects.common.shared.util;

public class NumberFormat {

	private static final NumberFormat defaultInstance = new NumberFormat();
	
	public static NumberFormat getDefaultInstance() {
		return defaultInstance;
	}
	
	public static NumberFormat newInstance() {
		return new NumberFormat();
	}
	
	private NumberFormat() {
	}
	
	public String format(double val) {
		if(Double.isInfinite(val) || Double.isNaN(val))
			return Double.toString(val);
		return format(Double.toString(val));
	}
	
	public String format(float val) {
		if(Float.isInfinite(val) || Float.isNaN(val))
			return Float.toString(val);
		return format(Float.toString(val));
	}

	public String decimalFormat(float val, int decimals){
		if(Float.isInfinite(val) || Float.isNaN(val)){
			return Float.toString(val);
		}
		return decimalFormat(Float.toString(val), decimals);
	}
	
	private String format(String baseStr) {
		
		//Log.info("formatting "+baseStr);
		String prefix = "";
		if(baseStr.startsWith("-")) {
			prefix = "-";
			baseStr = baseStr.substring(1);
		}
		else if(baseStr.startsWith("+")) {
			baseStr = baseStr.substring(1);
		}
		int dotIndex = baseStr.indexOf(".");
		//Log.info("dotIndex = "+dotIndex);
		int eIndex = baseStr.indexOf("E");
		if(eIndex == -1)
			eIndex = baseStr.indexOf('e');
		//Log.info("eIndex = "+eIndex);
		int eNum = 0;
		int lastNumIndex = baseStr.length()-1;
		if(eIndex != -1) {
			String eNumStr = baseStr.substring(eIndex+1);
			if(eNumStr.startsWith("+"))
				eNumStr = eNumStr.substring(1);
			//Log.info("eNumStr = "+eNumStr);
			eNum = Integer.parseInt(eNumStr);
			lastNumIndex = eIndex-1;
		}
		//Log.info("lastNumIndex = "+lastNumIndex);
		//Log.info("eNum = "+eNum);
		
		if(dotIndex == -1) {
			dotIndex = lastNumIndex+1;
		}
		
		String beforeDot = baseStr.substring(0, dotIndex);
		//Log.info("beforeDot = "+beforeDot);
		String afterDot = baseStr.substring(dotIndex+1, lastNumIndex+1);
		if(afterDot.equalsIgnoreCase("E")) afterDot = "";
		//Log.info("afterDot = "+beforeDot);
		
		boolean beforeDotNumIs0 = is0(beforeDot);
		//Log.info("beforeDotNumIs0 = "+beforeDotNumIs0);
		boolean afterDotNumIs0 = is0(afterDot);
		//Log.info("beforeDotNumIs0 = "+beforeDotNumIs0);
		
		if(beforeDotNumIs0 && afterDotNumIs0)
			return "0.E0";
		if(!beforeDotNumIs0 && beforeDot.length() == 1)
			return prefix+trimLeading0(beforeDot)+"."+trimTrailing0(afterDot)+"E"+eNum;
		
		if(beforeDotNumIs0) {
			//do *10
			int newENum = eNum;
			String newBeforeDot = "";
			String newAfterDot = afterDot;
			boolean numN0 = false;
			for(int i=0;i<afterDot.length();i++) {
				char nextChar = afterDot.charAt(i);
				newENum--;
				if(nextChar != '0') {
					newBeforeDot += nextChar;
					newAfterDot = afterDot.substring(i+1);
					break;
				}
			}
			return prefix+trimLeading0(newBeforeDot)+"."+trimTrailing0(newAfterDot)+"E"+newENum;
		} else {
			//do /10
			int newENum = eNum+beforeDot.length()-1;
			String newBeforeDot = beforeDot.substring(0, 1);
			String newAfterDot = beforeDot.substring(1)+afterDot;
			return prefix+trimLeading0(newBeforeDot)+"."+trimTrailing0(newAfterDot)+"E"+newENum;
		}
	}
	
	private String trimLeading0(String s) {
		if(is0(s)) return "0";
		if(!s.startsWith("0")) return s;
		int index = 0;
		for(int i=0;i<s.length();i++) {
			if(s.charAt(i) == '0')
				index++;
			else 
				break;
		}
		return s.substring(index);
	}
	
	private String trimTrailing0(String s) {
		if(is0(s)) return "0";
		if(!s.endsWith("0")) return s;
		int index = s.length()-1;
		for(int i=s.length()-1;i>=0;i--) {
			if(s.charAt(i) == '0')
				index--;
			else 
				break;
		}
		return s.substring(0,index);
	}
		
	private boolean is0(String s) {
		if(s == null || s.isEmpty()) return true;
		for(int i=0;i<s.length();i++) {
			if(s.charAt(i) != '0') 
				return false;
		}
		return true;
	}

	/**formats number string to x decimal points
	 * WARNING: May not detect exponents(untested)
	 * */
	private String decimalFormat(String num, int decimals){
		if(num == null || num.isEmpty())return "";
		StringBuilder builder = new StringBuilder();
		int count = 0;
		boolean hit = false;
		for(int i = 0; i < num.length(); i++){
			char c = num.charAt(i);

			if(c == '.'){
				hit = true;
			}
			if(c == 'E'){
				break;
			}

			builder.append(c);
			if(hit){
				count++;
			}
			if(count == decimals + 1){
				break;
			}
		}
		if(decimals == 0){
			builder.deleteCharAt(builder.indexOf("."));
		}
		if(count < decimals + 1){
			if(!hit){
				builder.append(".");
			}
			while(count < decimals + 1){
				builder.append("0");
				count++;
			}
		}
		return builder.toString();
	}

}
