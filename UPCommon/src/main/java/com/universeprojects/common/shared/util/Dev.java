package com.universeprojects.common.shared.util;

import com.universeprojects.common.shared.log.Logger;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * TODO: The name 'Dev' is misleading here.
 * Ideally, code that's labeled 'dev' should only be useful for development purposes.
 * Any code from this class that's used permanently should be re-branded as something else.
 */
public class Dev {

    private final static Logger log = Logger.getLogger(Dev.class);

    /**
     * Throws an exception if the given argument is NULL
     */
    public static <T> T checkNotNull(T obj) {
        return checkNotNull(obj, "Argument is null");
    }

    /**
     * Throws an exception with a  specified message if the given argument is NULL
     */
    public static <T> T checkNotNull(T obj, String message) {
        if (obj == null) {
            throw new DevException(message);
        }
        return obj;
    }


    /**
     * Throws an exception if the given string is NULL, "empty" or "blank"
     */
    public static String checkNotEmpty(String str) {
        if (Strings.isEmpty(str)) {
            throw new DevException("String argument is null or empty");
        }
        return str;
    }

    public static boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotNullOrEmpty(Collection<?> collection) {
        return !isNullOrEmpty(collection);
    }

    public static boolean isNullOrEmpty(Map<?,?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotNullOrEmpty(Map<?, ?> map) {
        return !isNullOrEmpty(map);
    }

    public static void assertThat(boolean condition) {
        check(condition);
    }

    /**
     * Throws an exception if the given boolean condition is FALSE
     */
    public static void check(boolean condition) {
        check(condition, "Condition does not hold");
    }

    /**
     * Throws an exception with a specified message, if the given boolean condition is FALSE
     */
    public static void check(boolean condition, String message) {
        if (!condition) {
            throw new DevException(message);
        }
    }

    /**
     * Throws an exception if the given objects aren't "equal"
     * (This method is NULL-safe)
     */
    public static void checkEqual(Object value1, Object value2) {
        check(Objects.equals(value1, value2), "Objects not equal " + value1 + " vs " + value2);
    }

    /**
     * Throws an exception if the given reference in NULL.
     * Intended to be used as a fuse to detect un-initialized component references at class level.
     *
     * @return The reference, if the check was successful
     */
    public static <T> T checkWiring(T ref) {
        if (ref == null) {
            throw new DevException("Reference wiring issue (requested reference is NULL)");
        }
        return ref;
    }

    /**
     * Throws an exception if this line in the code was reached.
     * Intended to be used as a fuse to detect unexpected flow in non-trivial logic.
     */
    public static void unreachableLine() {
        throw new DevException("Should not have reached this line");
    }

    /**
     * Throws an exception with a specified message.
     * Intended as a general-purpose fuse to blow up in unexpected situations.
     */
    public static void fail(String message) {
        throw new DevException(message);
    }

    public static <T> T withDefault(T value, T defaultValue) {
        if(value != null) {
            return value;
        } else {
            return defaultValue;
        }
    }

    /**
     * Outputs a log message via the DEV logger. Intended to be used for dev-purposes only!
     */
    public static void log(String message) {
        log.info("[DEV LOG] " + message);
    }

    /**
     * Converts a double value to human-readable format (only a few decimal places).
     * This is intended to be used for logging purposes only.
     */
    public static String readable(double d) {
        return Double.toString(d);
//        return NumberFormat.getFormat("0.00").format(d);
    }

    /**
     * Intended to wrap temporary dev code in an IF statement, to be easily located and cleaned-up.<br/>
     * If you find any code using this hook, it was committed by error, and you can safely get rid of it.<br/>
     *
     * Example use:
     *
     * <pre>
     * if (Dev.temp()) {
     *     // temp code here
     *     ...
     *     ...
     * }
     * </pre>
     */
    public static boolean temp() {
        return true;
    }

    public static final SortedSet EMPTY_SORTED_SET = Collections.unmodifiableSortedSet(new TreeSet<>());

    public static <T> SortedSet<T> emptySortedSet() {
        //noinspection unchecked
        return EMPTY_SORTED_SET;
    }

    public static <T> Class<T> getClass(T object) {
        //noinspection unchecked
        return (Class<T>) object.getClass();
    }
}
