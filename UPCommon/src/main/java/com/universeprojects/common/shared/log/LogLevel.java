package com.universeprojects.common.shared.log;


import org.slf4j.spi.LocationAwareLogger;

import java.util.Map;
import java.util.TreeMap;

public enum LogLevel {
    FATAL(60, "FATAL"), ERROR(50, "ERROR"), WARN(40, "WARN "), INFO(30, "INFO "), DEBUG(20, "DEBUG"), TRACE(10, "TRACE");

    public final int level;
    private final String text;

    LogLevel(int level, String text) {
        this.level = level;
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

    private static final Map<Integer, LogLevel> upIntToLevelMap;
    private static final Map<Integer, LogLevel> slf4jIntToLevelMap;

    static {
        upIntToLevelMap = new TreeMap<>();
        upIntToLevelMap.put(TRACE.level, TRACE);
        upIntToLevelMap.put(DEBUG.level, DEBUG);
        upIntToLevelMap.put(INFO.level, INFO);
        upIntToLevelMap.put(WARN.level, WARN);
        upIntToLevelMap.put(ERROR.level, ERROR);
        upIntToLevelMap.put(FATAL.level, FATAL);

        slf4jIntToLevelMap = new TreeMap<>();
        slf4jIntToLevelMap.put(LocationAwareLogger.TRACE_INT, TRACE);
        slf4jIntToLevelMap.put(LocationAwareLogger.DEBUG_INT, DEBUG);
        slf4jIntToLevelMap.put(LocationAwareLogger.INFO_INT, INFO);
        slf4jIntToLevelMap.put(LocationAwareLogger.WARN_INT, WARN);
        slf4jIntToLevelMap.put(LocationAwareLogger.ERROR_INT, ERROR);
    }

    public static LogLevel forUPLevel(int level) {
        return upIntToLevelMap.get(level);
    }

    public static LogLevel forSlf4JLevel(int level) {
        return slf4jIntToLevelMap.get(level);
    }
}
