package com.universeprojects.common.shared.util;

import com.universeprojects.common.shared.math.UPMath;

public class VoidspaceNumberFormat {

    public static final String[] HIGH_PREFIXES = new String[]{"", "k", "M", "G", "T"};
    public static final String[] LOW_PREFIXES = new String[]{"", "m", "micro", "n"};
    protected static final int HIGH_UNIT_CHANGE = 10_000;
    protected static final double LOW_UNIT_CHANGE = 0.1;

    public VoidspaceNumberFormat() {}

    public static String format(float x) {
        // Sign
        String sign = x < 0 ? "-" : "";
        x = UPMath.abs(x);

        // Start with just the integer value
        String s = Float.toString(x);

        // If .toString() already put the number in scientific notation, leave it at that
        int eIndex = s.indexOf("E");
        if(eIndex != -1) return s;

        // Remove decimals
        int dotIndex = s.indexOf(".");
        if (dotIndex != -1) {
            s = s.substring(0, dotIndex);
        }

        // Put in commas
        if (s.length() > 3) {
            for (int i = 1; i < s.length() + 1; i++) {
                if (i % 4 == 0) {
                    s = s.substring(0, s.length() - i + 1) + "," + s.substring(s.length() - i + 1);
                }
            }
        }

        // Decimals (up to 2 digits)
        int decimals = (int) (100f * (x - UPMath.floor(x)));
        if (decimals != 0 && s.length()<=2) {
            if (decimals<10)
                s += ".0" + decimals;
            else
                s += "." + decimals;
        }



        return sign + s;

    }

    public static String prefixed(float x) {
        return formatValue(x).numberWithPrefix();
    }

    public static FormattedValue formatValue(float x) {
        if(UPMath.abs(x) >= HIGH_UNIT_CHANGE) {
            return formatValueHigh(x);
        } else if (UPMath.abs(x) <= LOW_UNIT_CHANGE) {
            return formatValueLow(x);
        } else {
            return new FormattedValue(x, 1, "");
        }
    }

    public static FormattedValue formatSame(float x, FormattedValue formattedValue) {
        return new FormattedValue(x, formattedValue.prefixFactor, formattedValue.prefix);
    }

    public static FormattedValue formatValueLow(float x) {
        int n = 0;
        float factor = 1;
        while (UPMath.abs(x * factor) <= LOW_UNIT_CHANGE && n < LOW_PREFIXES.length) {
            n++;
            factor *= 1000.0;
        }

        if (n >= 4) {
            return new FormattedValue(0, 1, "");
        }

        return new FormattedValue(x, factor, LOW_PREFIXES[n]);
    }

    public static FormattedValue formatValueHigh(float x) {
        int n = 0;
        float factor = 1;
        while (UPMath.abs(x * factor) >= HIGH_UNIT_CHANGE && n < HIGH_PREFIXES.length) {
            n++;
            factor /= 1000.0;
        }
        return new FormattedValue(x, factor, HIGH_PREFIXES[n]);
    }

    public static class FormattedValue {
        public final float rawValue;
        public final float prefixFactor;
        public final String prefix;
        private String numberOnly;
        private String numberWithPrefix;

        private FormattedValue(float rawValue, float prefixFactor, String prefix) {
            this.rawValue = rawValue;
            this.prefixFactor = prefixFactor;
            this.prefix = prefix;
        }

        public String numberOnly() {
            if(numberOnly == null) {
                numberOnly = format(rawValue * prefixFactor);
            }
            return numberOnly;
        }

        public String numberWithPrefix() {
            if(numberWithPrefix == null) {
                numberWithPrefix = numberOnly()+" "+ prefix();
            }
            return numberWithPrefix;
        }

        private String prefix() {
            return prefix;
        }
    }
}
