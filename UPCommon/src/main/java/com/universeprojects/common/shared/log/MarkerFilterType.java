package com.universeprojects.common.shared.log;


public enum MarkerFilterType {
    ACCEPT, DENY, CONTINUE
}
