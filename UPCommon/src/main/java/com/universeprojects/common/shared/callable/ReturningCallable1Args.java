package com.universeprojects.common.shared.callable;

public interface ReturningCallable1Args<R, A1> extends ReturningCallable<R> {
    R call(A1 arg1);
}
