package com.universeprojects.common.shared.log;

import org.slf4j.IMarkerFactory;

import java.util.Map;
import java.util.TreeMap;


public class MarkerFactory implements IMarkerFactory {
    protected final Map<String, Marker> markers = new TreeMap<>();
    private static MarkerFactory instance = new MarkerFactory();

    private MarkerFactory() {

    }

    public static MarkerFactory getInstance() {
        return instance;
    }

    public Marker getMarker(String name) {
        return getMarker(null, name);
    }

    @Override
    public boolean exists(String name) {
        return name != null && markers.containsKey(name);
    }

    @Override
    public boolean detachMarker(String name) {
        return name != null && markers.remove(name) != null;
    }

    @Override
    public org.slf4j.Marker getDetachedMarker(String name) {
        if (name == null) {
            return null;
        }
        return new Marker(name);
    }

    public synchronized Marker getMarker(Marker parent, String name) {
        if (name == null) {
            return null;
        }
        Marker marker = markers.get(name);
        if (marker == null) {
            MarkerFilter filter = LoggerFactory.getInstance().getProcessor().getLoggerConfigManager().findMarkerFilter(name);
            marker = LoggerFactory.getInstance().getProcessor().createMarker(name);
            if (filter != null) {
                marker.setFilter(filter);
            }
            if (parent != null) {
                parent.addChildMarker(marker);
            }
            markers.put(name, marker);
        }
        return marker;
    }
}
