package com.universeprojects.common.shared.log;

import org.slf4j.spi.LocationAwareLogger;

@SuppressWarnings("unused")
public class Logger implements org.slf4j.Logger, LocationAwareLogger {
    private LoggerConfig loggerConfig;
    private final LogProcessor processor;
    private final String name;

    protected Logger(String name, LogProcessor processor) {
        this.name = name;
        this.processor = processor;
    }

    public void setLoggerConfig(LoggerConfig loggerConfig) {
        this.loggerConfig = loggerConfig;
    }

    public LoggerConfig getLoggerConfig() {
        return loggerConfig;
    }

    public static Logger getLogger(Class cls) {
        return LoggerFactory.getInstance().getLogger(cls);
    }

    public static Logger getLogger(String name) {
        return LoggerFactory.getInstance().getLogger(name);
    }

    public String getName() {
        return name;
    }

    public boolean isLoggingActive(LogLevel level) {
        return isLoggingActive(level, null);
    }

    public boolean isLoggingActive(LogLevel level, Marker marker) {
        if (marker != null) {
            MarkerFilter filter = marker.findFilter();
            if (filter != null) {
                if (filter.getFilterType() == MarkerFilterType.ACCEPT) {
                    return true;
                } else if (filter.getFilterType() == MarkerFilterType.DENY) {
                    return false;
                }
            }
        }
        return level.level >= loggerConfig.getMinLevel().level;
    }

    @Override
    public boolean isErrorEnabled() {
        return isLoggingActive(LogLevel.ERROR, null);
    }

    @Override
    public boolean isWarnEnabled() {
        return isLoggingActive(LogLevel.WARN, null);
    }

    @Override
    public boolean isInfoEnabled() {
        return isLoggingActive(LogLevel.INFO, null);
    }

    @Override
    public boolean isDebugEnabled() {
        return isLoggingActive(LogLevel.DEBUG, null);
    }

    @Override
    public boolean isTraceEnabled() {
        return isLoggingActive(LogLevel.TRACE, null);
    }

    @Override
    public boolean isErrorEnabled(org.slf4j.Marker marker) {
        return isLoggingActive(LogLevel.ERROR, (Marker) marker);
    }

    @Override
    public boolean isWarnEnabled(org.slf4j.Marker marker) {
        return isLoggingActive(LogLevel.WARN, (Marker) marker);
    }

    @Override
    public boolean isInfoEnabled(org.slf4j.Marker marker) {
        return isLoggingActive(LogLevel.INFO, (Marker) marker);
    }

    @Override
    public boolean isDebugEnabled(org.slf4j.Marker marker) {
        return isLoggingActive(LogLevel.DEBUG, (Marker) marker);
    }

    @Override
    public boolean isTraceEnabled(org.slf4j.Marker marker) {
        return isLoggingActive(LogLevel.TRACE, (Marker) marker);
    }

    protected static String buildMessage(String message, Object... parameters) {
        if (parameters == null || parameters.length == 0) {
            return message;
        }
        StringBuilder builder = new StringBuilder();
        String[] parts = message.split("\\{\\}");
        for (int i = 0; i < parts.length; i++) {
            builder.append(parts[i]);
            if (i < parameters.length) {
                builder.append(parameters[i]);
            }
        }
        return builder.toString();
    }


    @Override
    public void log(org.slf4j.Marker marker, String fqcn, int level, String message, Object[] parameters, Throwable throwable) {
        processLog(LogLevel.forSlf4JLevel(level), (Marker) marker, throwable, buildMessage(message, parameters));
    }

    private void processLog(LogLevel level, Marker marker, Throwable throwable, String message) {
        processor.processLogEntry(System.currentTimeMillis(), name, level, marker, message, throwable);
    }

    private void doLog(LogLevel level, Marker marker, Throwable throwable, String message) {
        if (!isLoggingActive(level, marker)) {
            return;
        }
        processLog(level, marker, throwable, message);
    }

    private void doLog(LogLevel level, Marker marker, Throwable throwable, String message, Object arg) {
        if (!isLoggingActive(level, marker)) {
            return;
        }
        processLog(level, marker, throwable, buildMessage(message, arg));
    }

    private void doLog(LogLevel level, Marker marker, Throwable throwable, String message, Object arg1, Object arg2) {
        if (!isLoggingActive(level, marker)) {
            return;
        }
        processLog(level, marker, throwable, buildMessage(message, arg1, arg2));
    }

    private void doLog(LogLevel level, Marker marker, Throwable throwable, String message, Object... args) {
        if (!isLoggingActive(level, marker)) {
            return;
        }
        processLog(level, marker, throwable, buildMessage(message, args));
    }

    public void log(LogLevel level, Marker marker, String message, Throwable throwable) {
        doLog(level, marker, throwable, message);
    }

    public void log(LogLevel level, String message, Throwable throwable) {
        doLog(level, null, throwable, message);
    }

    public void log(LogLevel level, String message) {
        doLog(level, null, null, message);
    }

    public void log(LogLevel level, Marker marker, String message) {
        doLog(level, marker, null, message);
    }

    public void fatal(String message, Throwable throwable) {
        doLog(LogLevel.FATAL, null, throwable, message);
    }

    public void fatal(String message) {
        doLog(LogLevel.FATAL, null, null, message);
    }

    public void fatal(org.slf4j.Marker marker, String message, Throwable throwable) {
        doLog(LogLevel.FATAL, (Marker) marker, throwable, message);
    }

    public void fatal(org.slf4j.Marker marker, String message) {
        doLog(LogLevel.FATAL, (Marker) marker, null, message);
    }

    @Override
    public void error(String message, Throwable throwable) {
        doLog(LogLevel.ERROR, null, throwable, message);
    }

    @Override
    public void error(org.slf4j.Marker marker, String msg) {
        doLog(LogLevel.ERROR, (Marker) marker, null, msg);
    }

    @Override
    public void error(org.slf4j.Marker marker, String format, Object arg) {
        doLog(LogLevel.ERROR, (Marker) marker, null, format, arg);
    }

    @Override
    public void error(org.slf4j.Marker marker, String format, Object arg1, Object arg2) {
        doLog(LogLevel.ERROR, (Marker) marker, null, format, arg1, arg2);
    }

    @Override
    public void error(org.slf4j.Marker marker, String format, Object[] argArray) {
        doLog(LogLevel.ERROR, (Marker) marker, null, format, argArray);
    }

    @Override
    public void error(org.slf4j.Marker marker, String msg, Throwable throwable) {
        doLog(LogLevel.ERROR, (Marker) marker, throwable, msg);
    }

    @Override
    public void error(String message) {
        doLog(LogLevel.ERROR, null, null, message);
    }

    @Override
    public void error(String format, Object arg) {
        doLog(LogLevel.ERROR, null, null, format, arg);
    }

    @Override
    public void error(String format, Object arg1, Object arg2) {
        doLog(LogLevel.ERROR, null, null, format, arg1, arg2);
    }

    @Override
    public void error(String format, Object[] argArray) {
        doLog(LogLevel.ERROR, null, null, format, argArray);
    }


    @Override
    public void warn(String message, Throwable throwable) {
        doLog(LogLevel.WARN, null, throwable, message);
    }

    @Override
    public void warn(org.slf4j.Marker marker, String msg) {
        doLog(LogLevel.WARN, (Marker) marker, null, msg);
    }

    @Override
    public void warn(org.slf4j.Marker marker, String format, Object arg) {
        doLog(LogLevel.WARN, (Marker) marker, null, format, arg);
    }

    @Override
    public void warn(org.slf4j.Marker marker, String format, Object arg1, Object arg2) {
        doLog(LogLevel.WARN, (Marker) marker, null, format, arg1, arg2);
    }

    @Override
    public void warn(org.slf4j.Marker marker, String format, Object[] argArray) {
        doLog(LogLevel.WARN, (Marker) marker, null, format, argArray);
    }

    @Override
    public void warn(org.slf4j.Marker marker, String msg, Throwable throwable) {
        doLog(LogLevel.WARN, (Marker) marker, throwable, msg);
    }

    @Override
    public void warn(String message) {
        doLog(LogLevel.WARN, null, null, message);
    }

    @Override
    public void warn(String format, Object arg) {
        doLog(LogLevel.WARN, null, null, format, arg);
    }

    @Override
    public void warn(String format, Object arg1, Object arg2) {
        doLog(LogLevel.WARN, null, null, format, arg1, arg2);
    }

    @Override
    public void warn(String format, Object[] argArray) {
        doLog(LogLevel.WARN, null, null, format, argArray);
    }


    @Override
    public void info(String message, Throwable throwable) {
        doLog(LogLevel.INFO, null, throwable, message);
    }

    @Override
    public void info(org.slf4j.Marker marker, String msg) {
        doLog(LogLevel.INFO, (Marker) marker, null, msg);
    }

    @Override
    public void info(org.slf4j.Marker marker, String format, Object arg) {
        doLog(LogLevel.INFO, (Marker) marker, null, format, arg);
    }

    @Override
    public void info(org.slf4j.Marker marker, String format, Object arg1, Object arg2) {
        doLog(LogLevel.INFO, (Marker) marker, null, format, arg1, arg2);
    }

    @Override
    public void info(org.slf4j.Marker marker, String format, Object[] argArray) {
        doLog(LogLevel.INFO, (Marker) marker, null, format, argArray);
    }

    @Override
    public void info(org.slf4j.Marker marker, String msg, Throwable throwable) {
        doLog(LogLevel.INFO, (Marker) marker, throwable, msg);
    }

    @Override
    public void info(String message) {
        doLog(LogLevel.INFO, null, null, message);
    }

    @Override
    public void info(String format, Object arg) {
        doLog(LogLevel.INFO, null, null, format, arg);
    }

    @Override
    public void info(String format, Object arg1, Object arg2) {
        doLog(LogLevel.INFO, null, null, format, arg1, arg2);
    }

    @Override
    public void info(String format, Object[] argArray) {
        doLog(LogLevel.INFO, null, null, format, argArray);
    }


    @Override
    public void debug(String message, Throwable throwable) {
        doLog(LogLevel.DEBUG, null, throwable, message);
    }

    @Override
    public void debug(org.slf4j.Marker marker, String msg) {
        doLog(LogLevel.DEBUG, (Marker) marker, null, msg);
    }

    @Override
    public void debug(org.slf4j.Marker marker, String format, Object arg) {
        doLog(LogLevel.DEBUG, (Marker) marker, null, format, arg);
    }

    @Override
    public void debug(org.slf4j.Marker marker, String format, Object arg1, Object arg2) {
        doLog(LogLevel.DEBUG, (Marker) marker, null, format, arg1, arg2);
    }

    @Override
    public void debug(org.slf4j.Marker marker, String format, Object[] argArray) {
        doLog(LogLevel.DEBUG, (Marker) marker, null, format, argArray);
    }

    @Override
    public void debug(org.slf4j.Marker marker, String msg, Throwable throwable) {
        doLog(LogLevel.DEBUG, (Marker) marker, throwable, msg);
    }

    @Override
    public void debug(String message) {
        doLog(LogLevel.DEBUG, null, null, message);
    }

    @Override
    public void debug(String format, Object arg) {
        doLog(LogLevel.DEBUG, null, null, format, arg);
    }

    @Override
    public void debug(String format, Object arg1, Object arg2) {
        doLog(LogLevel.DEBUG, null, null, format, arg1, arg2);
    }

    @Override
    public void debug(String format, Object[] argArray) {
        doLog(LogLevel.DEBUG, null, null, format, argArray);
    }


    @Override
    public void trace(String message, Throwable throwable) {
        doLog(LogLevel.TRACE, null, throwable, message);
    }

    @Override
    public void trace(org.slf4j.Marker marker, String msg) {
        doLog(LogLevel.TRACE, (Marker) marker, null, msg);
    }

    @Override
    public void trace(org.slf4j.Marker marker, String format, Object arg) {
        doLog(LogLevel.TRACE, (Marker) marker, null, format, arg);
    }

    @Override
    public void trace(org.slf4j.Marker marker, String format, Object arg1, Object arg2) {
        doLog(LogLevel.TRACE, (Marker) marker, null, format, arg1, arg2);
    }

    @Override
    public void trace(org.slf4j.Marker marker, String format, Object[] argArray) {
        doLog(LogLevel.TRACE, (Marker) marker, null, format, argArray);
    }

    @Override
    public void trace(org.slf4j.Marker marker, String msg, Throwable throwable) {
        doLog(LogLevel.TRACE, (Marker) marker, throwable, msg);
    }

    @Override
    public void trace(String message) {
        doLog(LogLevel.TRACE, null, null, message);
    }

    @Override
    public void trace(String format, Object arg) {
        doLog(LogLevel.TRACE, null, null, format, arg);
    }

    @Override
    public void trace(String format, Object arg1, Object arg2) {
        doLog(LogLevel.TRACE, null, null, format, arg1, arg2);
    }

    @Override
    public void trace(String format, Object[] argArray) {
        doLog(LogLevel.TRACE, null, null, format, argArray);
    }

}
