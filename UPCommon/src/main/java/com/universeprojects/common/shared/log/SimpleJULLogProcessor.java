package com.universeprojects.common.shared.log;

import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class SimpleJULLogProcessor extends LogProcessor {

    protected void processLogEntry(long timestamp, String loggerName, LogLevel level, Marker marker, String message, Throwable throwable) {
        Level convertedLevel = convertLogLevel(level);
        Logger.getLogger(loggerName).log(convertedLevel, message, throwable);
    }

    private SimpleLoggerConfigManager simpleLoggerConfigManager = new SimpleLoggerConfigManager();

    @Override
    public LoggerConfigManager getLoggerConfigManager() {
        return simpleLoggerConfigManager;
    }

    public static Level convertLogLevel(LogLevel level) {
        if (level == LogLevel.FATAL || level == LogLevel.ERROR) {
            return Level.SEVERE;
        } else if (level == LogLevel.WARN) {
            return Level.WARNING;
        } else if (level == LogLevel.INFO) {
            return Level.INFO;
        } else if (level == LogLevel.DEBUG) {
            return Level.FINE;
        }
        return Level.FINEST;

    }

}
