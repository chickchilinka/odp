package com.universeprojects.common.shared.log;

import java.util.Date;


@SuppressWarnings("unused")
public class SysOutLogProcessor extends LogProcessor {

    protected void processLogEntry(long timestamp, String loggerName, LogLevel level, Marker marker, String message, Throwable throwable) {
        StringBuilder sb = new StringBuilder();
        sb.append(new Date());
        sb.append(" ");
        sb.append(loggerName);
        sb.append(" ");
        sb.append(level);
        sb.append(" ");
        sb.append(message);
        if(throwable != null) {
            sb.append(" ");
            sb.append(throwable.getMessage());
        }
        if(level.level >= LogLevel.ERROR.level) {
            System.err.println(sb.toString());
        } else {
            System.out.println(sb.toString());
        }
    }

    private SimpleLoggerConfigManager simpleLoggerConfigManager = new SimpleLoggerConfigManager();

    @Override
    public LoggerConfigManager getLoggerConfigManager() {
        return simpleLoggerConfigManager;
    }

}
