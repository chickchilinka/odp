package com.universeprojects.common.shared.networking;

public interface ConnectionListener {
	void onConnect();
	void onDisconnect();
	void onPacketMessage(String message);
}
