package com.universeprojects.common.shared.util;

/**
 * This exception type is intended to be used in scenarios, when it's clearly a "code issue".
 * <p>
 * A "code issue" is something that can only be fixed by a programmer changing the code.
 * Though it can occur as an indirect result of the user's actions, it is never the user's fault.
 * <p>
 * This is mostly useful when a system gets big enough for its components no longer "trusting"
 * their callers.
 *
 */
@SuppressWarnings("serial")
public class DevException extends RuntimeException {

    public DevException(String message) {
        super("[Dev] " + message);
    }

    public DevException(String message, Throwable cause) {
        super("[Dev] " + message, cause);
    }

    public DevException() {
    }
}
