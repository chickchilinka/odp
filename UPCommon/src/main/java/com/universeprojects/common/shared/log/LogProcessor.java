package com.universeprojects.common.shared.log;

public abstract class LogProcessor {
    public Logger createLogger(String name) {
        return new Logger(name, this);
    }

    public Marker createMarker(String name) {
        return new Marker(name);
    }

    protected abstract void processLogEntry(long timestamp, String loggerName, LogLevel level, Marker marker, String message, Throwable throwable);

    public abstract LoggerConfigManager getLoggerConfigManager();

}
