package com.universeprojects.common.shared.profiler;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.GwtIncompatible;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@GwtIncompatible
@SuppressWarnings("NonJREEmulationClassesInClientCode")
public class ProfilerServer extends ProfilerBase {
    public ProfilerServer() {
        super(Logger.getLogger("Profiler"));
    }

    @Override
    public String format(long number) {
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMinimumFractionDigits(3);
        nf.setMaximumFractionDigits(3);
        return nf.format(number / 1000.);
    }

    @Override
    public long getTimeMicro() {
        return System.nanoTime() / 1000;
    }

    private static final Map<String, Profiler> instances = new HashMap<>();

    public synchronized static Profiler getInstance(String id) {
        Profiler pr = instances.get(id);
        if (pr == null) {
            pr = new ProfilerServer();
            instances.put(id, pr);
        }
        return pr;
    }

    public synchronized static List<String> findInstanceNames(String start) {
        List<String> list = new ArrayList<>();
        for (String key : instances.keySet()) {
            if(start == null || key.startsWith(start)) {
                list.add(key);
            }
        }
        return list;
    }

}
