package com.universeprojects.common.shared.util;

import com.universeprojects.common.shared.callable.Callable1Args;

public class Ref<T> {
    private T value;

    public boolean isSet() {
        return value != null;
    }

    public void clear() {
        value = null;
    }

    public Ref() {
    }

    public Ref(T value) {
        this.value = value;
    }

    public T get() {
        return value;
    }

    public void getNotNull(Callable1Args<T> callable) {
        if(value != null) {
            callable.call(value);
        }
    }

    public void set(T value) {
        this.value = value;
    }
}
