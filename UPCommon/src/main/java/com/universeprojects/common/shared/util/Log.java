package com.universeprojects.common.shared.util;

import com.universeprojects.common.shared.log.LoggerFactory;
import com.universeprojects.common.shared.math.UPMath;


public class Log {
    public static boolean debugToSystemOut = false;

    @SuppressWarnings("unused")
    public static String[] splitLogString(String original) {
        return splitLogString(original, 1000);
    }

    public static String[] splitLogString(String original, int partSize) {
        int numParts = (int)UPMath.ceil((double)original.length()/partSize);
        String[] parts = new String[numParts];
        for(int i=0;i<numParts-1;i++) {
            parts[i] = original.substring(i*partSize,(i+1)*partSize);
        }
        parts[numParts-1] = original.substring((numParts-1)*partSize,original.length());
        return parts;
    }

    public static void info(String text) {
        LoggerFactory.getInstance().getRootLogger().info(text);
    }

    public static void warn(String text) {
        LoggerFactory.getInstance().getRootLogger().warn(text);
    }

    public static void warn(String text, Throwable thr) {
        LoggerFactory.getInstance().getRootLogger().warn(text, thr);
    }

    public static void error(String text) {
        LoggerFactory.getInstance().getRootLogger().error(text);
    }

    public static void error(String text, Throwable thr) {
        LoggerFactory.getInstance().getRootLogger().error(text, thr);
    }

    public static void debug(String text) {
        LoggerFactory.getInstance().getRootLogger().debug(text);
        if(debugToSystemOut) {
            System.out.println(text);
        }
    }

    public static boolean isDebugEnabled() {
        return LoggerFactory.getInstance().getRootLogger().isDebugEnabled();
    }

}
