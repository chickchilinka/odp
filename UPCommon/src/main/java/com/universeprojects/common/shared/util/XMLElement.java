package com.universeprojects.common.shared.util;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class XMLElement {
	public final String name;
	protected XMLElement parent;
	protected String value = null;
	protected Map<String,String> attributes = new LinkedHashMap<String,String>();
	protected Set<XMLElement> children = new LinkedHashSet<XMLElement>();
	protected Map<String,LinkedList<XMLElement>> childrenMap = new LinkedHashMap<String,LinkedList<XMLElement>>();
	
	public XMLElement(String name) {
		Dev.checkNotNull(name);
		this.name = name;
	}
	
	public void addAttribute(String key, Object value) {
		addAttribute(key, value.toString());
	}
	
	public void addAttribute(String key, String value) {
		Dev.checkNotNull(key);
		Dev.checkNotNull(value);
		attributes.put(key,value);
	}
	
	public void setValue(String value) {
		Dev.checkNotNull(value);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public Set<XMLElement> getChildren() {
		return Collections.unmodifiableSet(children);
	}
	
	public void appendChild(XMLElement el) {
		Dev.checkNotNull(el);
		if(el.parent != null)
			throw new RuntimeException("Has to be removed from the old parent");
		el.parent = this;
		boolean success = children.add(el);
		if(!success)
			throw new RuntimeException("This element "+name+" already contains this child "+el.name);
		LinkedList<XMLElement> list = childrenMap.get(el.name);
		if(list == null) {
			list = new LinkedList<XMLElement>();
			childrenMap.put(el.name, list);
		}
		list.add(el);
	}
	
	public void removeChild(XMLElement el) {
		Dev.checkNotNull(el);
		boolean success = children.remove(el);
		if(success == false)
			throw new RuntimeException("The element "+el.name+" is not a child of this element "+name);
		el.parent = null;
		LinkedList<XMLElement> list = childrenMap.get(el.name);
		if(list != null)
			list.remove(el);
	}
	
	public boolean containes(XMLElement el) {
		return children.contains(el);
	}
	
	public String getAttributeValue(String key) {
		return attributes.get(key);
	}
	
	public Map<String,String> getAttributes() {
		return attributes;
	}
	
	public XMLElement getFirstChildElement(String key) {
		LinkedList<XMLElement> list = childrenMap.get(key);
		if(list == null) return null;
		return list.getFirst();
	}
	
	public List<XMLElement> getChildElements(String key) {
		LinkedList<XMLElement> list = childrenMap.get(key);
		if(list == null) return Collections.emptyList();
		return Collections.unmodifiableList(list);
	}
	
	public static final int INDENT=2;
	
	@Override
	public String toString() {
		return toFormattedString();
	}
	
	public String toFormattedString() {
		StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		appendObject(sb,null, true);
		return sb.toString();
	}
	
	private void appendObject(StringBuilder sb, String indent, boolean pretty) {
		if(indent == null)
			indent = "";
		else if(pretty) {
			for(int i=0;i<INDENT;i++)
				indent += " ";
		}
		sb.append(indent).append("<").append(name);
		for(String key : attributes.keySet()) {
			sb.append(" ").append(key).append("=\"").append(attributes.get(key)).append("\"");
		}
		if(children.isEmpty() && value == null) {
			sb.append("/>");
			if(pretty)
				sb.append("\n");
		} else {
			sb.append(">");
			if(pretty)
				sb.append("\n");
			for(XMLElement el : children) {
				el.appendObject(sb,indent,pretty);
			}
			if(value != null)
				sb.append(value);
			sb.append(indent).append("</").append(name).append(">");
			if(pretty)
				sb.append("\n");
		}
	}
}