package com.universeprojects.common.shared.log;

public class LoggerConfig {
    private final String name;
    private LogLevel minLevel = LogLevel.INFO;

    public LoggerConfig(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public LogLevel getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(LogLevel minLevel) {
        this.minLevel = minLevel;
    }
}
