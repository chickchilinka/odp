package com.universeprojects.common.shared.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("unused")
public class Strings {

    public static boolean isEmpty(String str) {
        return (str == null || str.isEmpty() || str.trim().isEmpty());
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static String inQuotes(String str) {
        return "\"" + str + "\"";
    }

    public static String inSingleQuotes(String str) {
        return "'" + str + "'";
    }

    public static String inParentheses(String str) {
        return "(" + str + ")";
    }

    public static String fromDouble(double d) {
        return fromDouble(d, 3);
    }

    public static String fromDouble(double d, int numberOfZeros) {
        String s = Double.toString(d);
        if (s.indexOf('.') > -1 && s.indexOf('.') < s.length() - 4) {
            s = s.substring(0, s.indexOf('.') + numberOfZeros + 1);
        }
        return s;
    }

    public static String join(Object[] items, String delimiter) {
        return join(Arrays.asList(items), delimiter);
    }

    public static String join(Collection<?> items, String delimiter) {
        Dev.checkNotNull(items);
        Dev.checkNotNull(delimiter);

        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Object obj : items) {
            if (!first) {
                sb.append(delimiter);
            }
            sb.append(obj != null ? obj.toString() : "null");
            first = false;
        }

        return sb.toString();
    }

    public static String nullToEmpty(String str) {
        return str == null ? "" : str;
    }

    public static String emptyToDefault(String str, String defaultValue) {
        return Strings.isEmpty(str) ? defaultValue : str;
    }

    public static String splitCamelCase(String str) {
        return str
            .replaceAll("([A-Z][a-z]+)", " $1") // Words beginning with UC
            .replaceAll("([A-Z][A-Z]+)", " $1") // "Words" of only UC
            .replaceAll("([^A-Za-z ]+)", " $1") // "Words" of non-letters
            .trim();
    }

    /**
     * (Borrowed from Apache Commons WordUtils since I couldn't use that directly due to GWT)
     *
     * <p>Wraps a single line of text, identifying words by the space character.</p>
     *
     * <p>Leading spaces on a new line are stripped. Trailing spaces are not stripped.</p>
     *
     * @param str        the String to be word wrapped, may be NULL
     * @param wrapLength the column to wrap the words at, must be greater than 0
     * @return An array with the wrapped lines, or an empty array for NULL input
     */
    public static String[] wrap(String str, int wrapLength) {
        if (str == null) {
            return new String[0];
        }
        if (wrapLength < 1) {
            throw new IllegalArgumentException("wrap length must be greater than zero");
        }

        int inputLineLength = str.length();
        int offset = 0;
        List<String> lines = new ArrayList<>();

        while ((inputLineLength - offset) > wrapLength) {
            if (str.charAt(offset) == ' ') {
                offset++;
                continue;
            }
            int spaceToWrapAt = str.lastIndexOf(' ', wrapLength + offset);

            if (spaceToWrapAt >= offset) {
                // normal case
                lines.add(str.substring(offset, spaceToWrapAt));
                offset = spaceToWrapAt + 1;
            } else {
                // really long word or URL; wrap one line at a time
                lines.add(str.substring(offset, wrapLength + offset));
                offset += wrapLength;
            }
        }

        // Whatever is left is short enough to just pass through
        String remainder = str.substring(offset);
        if (!isEmpty(remainder)) {
            lines.add(remainder);
        }

        return lines.toArray(new String[0]);
    }

    public static String capitalize(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() <= 1) {
            return str.toUpperCase();
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    }
}
