package com.universeprojects.common.shared.math;

import java.math.BigDecimal;
import java.util.Random;

public class UPMath {
    public static double sin(double rad) {
        return Math.sin(rad);
    }

    public static double cos(double rad) {
        return Math.cos(rad);
    }

    public static double tan(double rad) {
        return Math.tan(rad);
    }

    public static double sinDeg(double deg) {
        return Math.sin(Math.toRadians(deg));
    }

    public static double cosDeg(double deg) {
        return Math.cos(Math.toRadians(deg));
    }

    public static double tanDeg(double deg) {
        return Math.tan(Math.toRadians(deg));
    }

    public static double asin(double val) {
        return Math.asin(val);
    }

    public static double acos(double val) {
        return Math.acos(val);
    }

    public static double atan(double val) {
        return Math.atan(val);
    }

    public static double atan2(double y, double x) {
        return Math.atan2(y, x);
    }

    public static float sin(float rad) {
        return (float) Math.sin(rad);
    }

    public static float cos(float rad) {
        return (float) Math.cos(rad);
    }

    public static float tan(float rad) {
        return (float) Math.tan(rad);
    }

    public static float sinDeg(float deg) {
        return (float) Math.sin(Math.toRadians(deg));
    }

    public static float cosDeg(float deg) {
        return (float) Math.cos(Math.toRadians(deg));
    }

    public static float tanDeg(float rad) {
        return (float) Math.tan(Math.toRadians(rad));
    }

    public static float asin(float val) {
        return (float) Math.asin(val);
    }

    public static float acos(float val) {
        return (float) Math.acos(val);
    }

    public static float atan(float val) {
        return (float) Math.atan(val);
    }

    public static float atan2(float y, float x) {
        return (float) Math.atan2(y, x);
    }

    public static final double PI = Math.PI;
    public static final double PI_UNDER_180 = 180.0 / PI;
    public static final double PI_OVER_180 = PI / 180.0;
    public static final double E = Math.E;

    public static double toDegrees(double val) {
        return PI_UNDER_180 * val;
    }

    public static float toDegrees(float val) {
        return ((float) PI_UNDER_180) * val;
    }

    public static double toRadians(double val) {
        return PI_OVER_180 * val;
    }

    public static float toRadians(float val) {
        return ((float) PI_OVER_180) * val;
    }

    public static double sqrt(double val) {
        return Math.sqrt(val);
    }

    public static float sqrt(float val) {
        return (float) Math.sqrt(val);
    }

    public static double hypotenuse(int a, int b) {
        return sqrt(a * a + b * b);
    }

    public static double hypotenuse(double a, double b) {
        return sqrt(a * a + b * b);
    }

    public static double log(double val) {
        return Math.log(val);
    }

    public static int abs(int val) {
        return Math.abs(val);
    }

    public static double abs(double val) {
        return Math.abs(val);
    }

    public static float abs(float val) {
        return Math.abs(val);
    }

    public static long round(double d) {
        return Math.round(d);
    }

    public static long max(long a, long b) {
        return Math.max(a, b);
    }

    public static int max(int a, int b) {
        return Math.max(a, b);
    }

    public static double max(double a, double b) {
        return Math.max(a, b);
    }

    public static float max(float a, float b) {
        return Math.max(a, b);
    }

    public static long min(long a, long b) {
        return Math.min(a, b);
    }

    public static int min(int a, int b) {
        return Math.min(a, b);
    }

    public static double min(double a, double b) {
        return Math.min(a, b);
    }

    public static float min(float a, float b) {
        return Math.min(a, b);
    }

    public static double ceil(double a) {
        return Math.ceil(a);
    }

    public static double floor(double a) {
        return Math.floor(a);
    }

    public static float floor(float a) {
        return (float) Math.floor(a);
    }

    public static double signum(double a) {
        return Math.signum(a);
    }

    public static float signum(float a) {
        return Math.signum(a);
    }

    public static double pow(double a, double b) {
        return Math.pow(a, b);
    }

    public static float pow(float a, float b) {
        return (float) Math.pow(a, b);
    }

    public static int cap(int num, int min, int max) {
        if (num < min) return min;
        if (num > max) return max;
        return num;
    }

    public static double cap(double num, double min, double max) {
        if (num < min) return min;
        if (num > max) return max;
        return num;
    }

    public static float cap(float num, float min, float max) {
        if (num < min) return min;
        if (num > max) return max;
        return num;
    }

    public static BigDecimal cap(BigDecimal num, BigDecimal min, BigDecimal max) {
        if (num.compareTo(min) < 0) return min;
        if (num.compareTo(max) > 0) return max;
        return num;
    }

    public static int capMin(int num, int min) {
        if (num < min) return min;
        return num;
    }

    public static double capMin(double num, double min) {
        if (num < min) return min;
        return num;
    }

    public static float capMin(float num, float min) {
        if (num < min) return min;
        return num;
    }

    public static BigDecimal capMin(BigDecimal num, BigDecimal min) {
        if (num.compareTo(min) < 0) return min;
        return num;
    }

    public static int capMax(int num, int max) {
        if (num > max) return max;
        return num;
    }

    public static double capMax(double num, double max) {
        if (num > max) return max;
        return num;
    }

    public static float capMax(float num, float max) {
        if (num > max) return max;
        return num;
    }

    public static long capMax(long num, long max) {
        if (num > max) return max;
        return num;
    }

    public static BigDecimal capMax(BigDecimal num, BigDecimal max) {
        if (num.compareTo(max) > 0) return max;
        return num;
    }

    public static double random() {
        return Math.random();
    }

    public static final double FULL_CIRCLE_ANGLE = toRadians(360);

    /**
     * Calculates the squared distance from 2D point P1(x1, y1) to P2(x2, y2).
     */
    public static double distance(double x1, double y1, double x2, double y2) {
        return sqrt(distance2(x1, y1, x2, y2));
    }
    public static float distance(float x1, float y1, float x2, float y2) {
        return sqrt(distance2(x1, y1, x2, y2));
    }

    /**
     * Calculates the squared distance from 2D point P1(x1, y1) to P2(x2, y2).
     * It is squared, because the SQRT operation is not applied at the end.
     * To be used for optimization purposes.
     */
    public static double distance2(double x1, double y1, double x2, double y2) {
        double x = x2 - x1;
        double y = y2 - y1;
        return (x * x) + (y * y);
    }

    public static float distance2(float x1, float y1, float x2, float y2) {
        float x = x2 - x1;
        float y = y2 - y1;
        return (x * x) + (y * y);
    }

    /**
     * Calculates the angle of a line represented by points P1(x1, y1) and P2(x2, y2).
     *
     * @return the angle in RADIANS
     */
    public static double angle(double x1, double y1, double x2, double y2) {
        double a = Math.atan2(x2 - x1, y2 - y1);
        if (a < 0)
            a += 2 * PI;
        return a;
    }

    public static float cross(float x1, float y1, float x2, float y2) {
        return x1 * y2 - y1 * x2;
    }

    public static float dot(float x1, float y1, float x2, float y2) {
        return x1*x2 + y1*y2;
    }

    public static float angle(float x1, float y1, float x2, float y2) {
        float a = (float) Math.atan2(cross(x1, y1, x2, y2), dot(x1, y1, x2, y2));
        if (a < 0)
            a += 2 * PI;
        return a;
    }

    /**
     * Calculates the angle of a line represented by points P1(x1, y1) and P2(x2, y2).
     *
     * @return the angle in DEGREES
     */
    public static double angleDeg(double x1, double y1, double x2, double y2) {
        return toDegrees(angle(x1, y1, x2, y2));
    }

    public static float angleDeg(float x1, float y1, float x2, float y2) {
        return toDegrees(angle(x1, y1, x2, y2));
    }

    public static String doubleToRadixString(double d, int radix) {
        return Long.toString(Double.doubleToLongBits(d), radix);
    }

    public static double doubleFromRadixString(String s, int radix) {
        return Double.longBitsToDouble(Long.parseLong(s, radix));
    }

    public static int findNextPower2(int v) {
        v--;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v++;
        return v;
    }

    public static int log2(int v) {
        int r = 0;
        while ((v >>= 1) != 0) {
            r++;
        }
        return r;
    }

    public static int generatePoissonRndValue(double mean, Random rng) {
        int x = 0;
        double t = 0.0;
        while (true) {
            t -= log(rng.nextDouble()) / mean;
            if (t > 1.0) {
                break;
            }
            ++x;
        }
        return x;
    }

    public static float lerp(float a, float b, float t) {
        t = t < 0f ? 0f : t > 1f ? 1f : t;
        return a + t*(b-a);
    }

    public static int lerp(int a, int b, float t) {
        t = t < 0f ? 0f : t > 1f ? 1f : t;
        return (int)(a + t*(b-a));
    }

    public static UPVector lerp(UPVector a, UPVector b, float t) {
        t = t < 0f ? 0f : t > 1f ? 1f : t;
        return a.add(b.subtract(a).multiply(t));
    }
}
