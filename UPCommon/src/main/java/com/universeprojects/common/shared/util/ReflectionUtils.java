package com.universeprojects.common.shared.util;

public class ReflectionUtils {

    public static ReflectionUtils INSTANCE = new ReflectionUtils();

    protected ReflectionUtils() {
    }

    public String getSimpleName(Class<?> clazz) {
        return clazz.getSimpleName();
    }
}
