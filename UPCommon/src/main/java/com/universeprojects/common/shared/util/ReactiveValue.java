package com.universeprojects.common.shared.util;

import java.util.ArrayList;
import java.util.List;

public class ReactiveValue<T> {
    private final List<ReactiveValueListener<T>> listeners = new ArrayList<>();
    private T value;

    public ReactiveValue(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        T oldValue = this.value;
        this.value = value;
        for (ReactiveValueListener<T> listener : listeners) {
            listener.onChange(oldValue, value);
        }
    }

    public void addListener(ReactiveValueListener<T> listener) {
        this.listeners.add(listener);
    }

    public ReactiveValueListener<T> getAndListen(ReactiveValueListener<T> listener) {
        addListener(listener);
        listener.onChange(value, value);

        return listener;
    }

    public void removeListener(ReactiveValueListener<T> listener) {
        this.listeners.remove(listener);
    }

    public void clear() {
        listeners.clear();
    }
}
