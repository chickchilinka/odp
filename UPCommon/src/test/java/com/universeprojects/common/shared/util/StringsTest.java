package com.universeprojects.common.shared.util;

import com.universeprojects.common.server.test.UnitTestBase;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

public class StringsTest extends UnitTestBase {
	
	@Test
	public void inQuotes_lowercase() {
		Assert.assertEquals("\"abc\"", Strings.inQuotes("abc"));
	}
	
	@Test
	public void inQuotes_uppercase() {
		Assert.assertEquals("\"ABC\"", Strings.inQuotes("ABC"));
	}
	
	@Test
	public void inQuotes_emptyString() {
		Assert.assertEquals("\"\"", Strings.inQuotes(""));
	}
	
	@Test
	public void inQuotes_null() {
		Assert.assertEquals("\"null\"", Strings.inQuotes(null));
	}
	
	@Test
	public void join_Strings() {
		Collection<String> strings = Arrays.asList("abc", "def", "xyz");
		String joined = Strings.join(strings, ", ");
		Assert.assertEquals("abc, def, xyz", joined);
	}
	
	@Test
	public void join_Objects() {
		
		Object obj1 = new Object() {
			@Override
			public String toString() {
				return "abc";
			}
		};
		
		Object obj2 = new Object() {
			@Override
			public String toString() {
				return "def";
			}
		};
		
		Object obj3 = new Object() {
			@Override
			public String toString() {
				return "xyz";
			}
		};
		
		
		Collection<Object> objects = Arrays.asList(obj1, obj2, obj3);
		String joined = Strings.join(objects, ", ");
		Assert.assertEquals("abc, def, xyz", joined);
	}

	@Test
	public void wrap_nullEmptyBlank_returnsEmptyArray() {
		Assert.assertArrayEquals(new String[0], Strings.wrap(null, 10));
		Assert.assertArrayEquals(new String[0], Strings.wrap("", 10));
		Assert.assertArrayEquals(new String[0], Strings.wrap("         ", 10));
	}

	@Test
	public void wrap_oneWord_underWrapLength_returnsArrayWithOneElement() {
		Assert.assertArrayEquals(new String[]{"abcde"}, Strings.wrap("abcde", 5));
	}

	@Test
	public void wrap_oneWord_overWrapLength_returnsArrayWithTwoElements() {
		Assert.assertArrayEquals(new String[]{"abc", "de"}, Strings.wrap("abcde", 3));
	}

	@Test
	public void wrap_twoWords_underWrapLength_returnsArrayWithOneElement() {
		Assert.assertArrayEquals(new String[]{"abcd xyz"}, Strings.wrap("abcd xyz", 10));
	}

	@Test
	public void wrap_twoWords_overWrapLength_returnsArrayWithTwoElements() {
		Assert.assertArrayEquals(new String[]{"abcd", "xyz"}, Strings.wrap("abcd xyz", 6));
	}

	@Test
	public void splitCamelCase() {
		Assert.assertEquals("lowercase", Strings.splitCamelCase("lowercase"));
		Assert.assertEquals("Class", Strings.splitCamelCase("Class"));
		Assert.assertEquals("My Class", Strings.splitCamelCase("MyClass"));
		Assert.assertEquals("HTML", Strings.splitCamelCase("HTML"));
		Assert.assertEquals("PDF Loader", Strings.splitCamelCase("PDFLoader"));
		Assert.assertEquals("A String", Strings.splitCamelCase("AString"));
		Assert.assertEquals("Simple XML Parser", Strings.splitCamelCase("SimpleXMLParser"));
		Assert.assertEquals("GL 11 Version", Strings.splitCamelCase("GL11Version"));
	}

	
}
