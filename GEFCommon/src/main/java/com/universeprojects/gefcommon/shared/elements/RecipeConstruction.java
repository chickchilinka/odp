package com.universeprojects.gefcommon.shared.elements;

import java.util.Collection;
import java.util.Map;

public interface RecipeConstruction {
    Collection<? extends RecipeConstructionSlot> getSlotData();
    Map<String, String> getUserFieldValues();
}
