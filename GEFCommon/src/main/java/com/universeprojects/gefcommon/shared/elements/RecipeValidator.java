package com.universeprojects.gefcommon.shared.elements;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("unused")
public class RecipeValidator {
    public static RecipeValidator INSTANCE = new RecipeValidator();

    @SuppressWarnings("RegExpRedundantEscape")
    private static final Pattern fieldSubstitutionPattern = Pattern.compile("\\$\\{(.*)\\}");

    private RecipeValidator() {
    }

    public boolean validateRecipeConstruction(Recipe recipe, RecipeConstruction recipeConstruction) {
        checkRecipeAndConstructionValid(recipe, recipeConstruction);

        if (!validateRecipeSlotObjects(recipe, recipeConstruction)) {
            return false;
        }
        return validateRecipeUserFieldValues(recipe, recipeConstruction);
    }

    private void checkRecipeAndConstructionValid(Recipe recipe, RecipeConstruction recipeConstruction) {
        if (recipe == null) throw new IllegalArgumentException("Recipe must not be null");
        if (recipeConstruction == null) throw new IllegalArgumentException("RecipeConstruction must not be null");
        if (recipeConstruction.getSlotData() == null) throw new IllegalArgumentException("RecipeConstruction.getSlotData() must not be null");
    }

    public boolean validateRecipeSlotObjects(Recipe recipe, RecipeConstruction recipeConstruction) {
        checkRecipeAndConstructionValid(recipe, recipeConstruction);

        Map<String, RecipeConstructionSlot> slotMap = createConstructionSlotMap(recipeConstruction);
        Set<Object> usedObjectKeys = new HashSet<>();
        for (RecipeCategory category : recipe.getCategories()) {
            for (RecipeSlot slot : category.getSlots()) {
                RecipeConstructionSlot slotData = slotMap.get(slot.getSlotId());
                if (slotData == null) {
                    if (category.isRequired()) {
                        return false;
                    }
                } else {
                    if (matchesSlot(slot, slotData.getObject()) != MatchType.MATCH) {
                        return false;
                    }
                    boolean added = usedObjectKeys.add(slotData.getObject().getKey());
                    if(!added) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean validateRecipeUserFieldValues(Recipe recipe, RecipeConstruction recipeConstruction) {
        checkRecipeAndConstructionValid(recipe, recipeConstruction);
        return validateRecipeUserFieldValues(recipe.getUserFieldValueConfigurations(), recipeConstruction.getUserFieldValues());
    }

    public boolean validateRecipeUserFieldValues(List<? extends UserFieldValueConfiguration> userFieldValueConfigs, Map<String, String> userFieldValues) {
        if(userFieldValueConfigs == null) {
            return true;
        }
        for (UserFieldValueConfiguration config : userFieldValueConfigs) {
            if(userFieldValues == null) {
                if(isRequired(config)) {
                    return false;
                }
            } else {
                String value = userFieldValues.get(config.getTargetFieldName());
                return validateUserFieldValue(config, value);
            }
        }
        return true;
    }

    private boolean isRequired(UserFieldValueConfiguration config) {
        return config.isRequired() == null || config.isRequired() == Boolean.TRUE;
    }

    @SuppressWarnings("RedundantIfStatement")
    public boolean validateUserFieldValue(UserFieldValueConfiguration config, String value) {
        if(value == null || value.isEmpty()) {
            return !isRequired(config);
        }
        if(config.getType() == UserFieldValueConfigurationType.STRING) {
            Integer minLength = config.getMinLength();
            if(minLength != null && value.length() < minLength) {
                return false;
            }
            Integer maxLength = config.getMaxLength();
            if(maxLength != null && value.length() > maxLength) {
                return false;
            }
            String typeConfig = config.getTypeConfiguration();
            if(typeConfig != null && !value.matches(typeConfig)) {
                return false;
            }
            return true;
        } else {
            throw new IllegalArgumentException("Unrecognized USerFieldValueConfigurationType: "+config.getType());
        }

    }

    public Map<String, RecipeSlotCombination> findRecipeSlotCombinations(Recipe recipe, RecipeConstruction recipeConstruction) {
        checkRecipeAndConstructionValid(recipe, recipeConstruction);
        Map<String, RecipeConstructionSlot> slotMap = createConstructionSlotMap(recipeConstruction);
        Map<String, RecipeSlotCombination> combinationMap = new HashMap<>();
        for (RecipeCategory category : recipe.getCategories()) {
            for (RecipeSlot slot : category.getSlots()) {
                RecipeConstructionSlot slotData = slotMap.get(slot.getSlotId());
                if (slotData == null) {
                    if (category.isRequired()) {
                        return null;
                    }
                } else {
                    GameObject<?> object = slotData.getObject();
                    if(object == null) {
                        throw new NullPointerException("Object not set for slot-id"+slot.getSlotId());
                    }
                    RecipeSlotOption matchingSlotOption = findMatchingSlotOption(slot, object);
                    if (matchingSlotOption == null) {
                        return null;
                    }
                    RecipeSlotCombination combination = new RecipeSlotCombination();
                    combination.constructionSlot = slotData;
                    combination.recipeSlot = slot;
                    combination.recipeSlotOption = matchingSlotOption;
                    combinationMap.put(slot.getSlotId(), combination);
                }
            }
        }
        return combinationMap;
    }

    private Map<String, RecipeConstructionSlot> createConstructionSlotMap(RecipeConstruction recipeConstruction) {
        Map<String, RecipeConstructionSlot> slotMap = new LinkedHashMap<>();
        Collection<? extends RecipeConstructionSlot> slotData = recipeConstruction.getSlotData();
        if(slotData != null) {
            for (RecipeConstructionSlot slot : slotData) {
                slotMap.put(slot.getSlotId(), slot);
            }
        }
        return slotMap;
    }

    @SuppressWarnings("rawtypes")
    public Map<GameObject, MatchType> findMatchingObjects(RecipeSlot slot, Iterable<GameObject> objects) {
        Map<GameObject, MatchType> map = new LinkedHashMap<>();
        for (GameObject<?> object : objects) {
            MatchType matchType = matchesSlot(slot, object);
            if (matchType == MatchType.MATCH || matchType == MatchType.MATCH_INSUFFICIENT_QUANTITY) {
                map.put(object, matchType);
            }
        }
        return map;
    }

    /**
     * Overload for a single game object
     */
    @SuppressWarnings("rawtypes")
    public Map<GameObject, MatchType> findMatchingObjects(RecipeSlot slot, GameObject object) {
        Map<GameObject, MatchType> map = new LinkedHashMap<>();
        MatchType matchType = matchesSlot(slot, object);
        if (matchType == MatchType.MATCH || matchType == MatchType.MATCH_INSUFFICIENT_QUANTITY) {
            map.put(object, matchType);
        }
        return map;
    }

    public MatchType matchesSlot(RecipeSlot slot, GameObject<?> object) {
        for (RecipeSlotOption option : slot.getOptions()) {
            boolean success = matchesSlotOptionWithoutQuantity(option, object);
            if (success) {
                if(matchesRequiredQuantity(object, option.getRequiredQuantity())) {
                    return MatchType.MATCH;
                } else {
                    return MatchType.MATCH_INSUFFICIENT_QUANTITY;
                }
            }
        }
        return MatchType.NO_MATCH;
    }

    public RecipeSlotOption findMatchingSlotOption(RecipeSlot slot, GameObject<?> object) {
        for (RecipeSlotOption option : slot.getOptions()) {
            boolean success = matchesSlotOptionWithoutQuantity(option, object);
            if (success) {
                if(matchesRequiredQuantity(object, option.getRequiredQuantity())) {
                    return option;
                }
            }
        }
        return null;
    }

    public boolean matchesSlotOptionWithoutQuantity(RecipeSlotOption option, GameObject<?> object) {
        return matchRequiredAspects(object, option.getRequiredAspects()) &&
            matchFields(object, option.getFieldRequirements());
    }

    private boolean matchRequiredAspects(GameObject<?> object, Collection<String> requiredAspects) {
       if(requiredAspects == null || requiredAspects.size() == 0) return true;
        return object.getAspectNames().containsAll(requiredAspects);
    }

    private boolean matchesRequiredQuantity(GameObject<?> object, int amount) {
        if(amount < 1) throw new IllegalStateException("Required amount cannot be < 1, is "+amount);
        if(amount == 1) return true;
        final GameAspect<?> aspect = object.getAspect("SimpleStackableAspect");
        if(aspect == null) return false;
        Object quantity = aspect.getProperty("FIELD_Quantity");
        return matchValues(quantity, amount, RecipeFieldRequirementOperator.GE);
    }

    private boolean matchFields(GameObject<?> object, Collection<? extends RecipeFieldRequirement> fieldRequirements) {
        for (RecipeFieldRequirement requirement : fieldRequirements) {
            boolean matches = matchFieldOptions(object, requirement.getFieldOptions());
            if (!matches) return false;
        }
        return true;
    }

    private boolean matchFieldOptions(GameObject<?> object, Collection<? extends RecipeFieldRequirementOption> fieldRequirementOptions) {
        for (RecipeFieldRequirementOption requirement : fieldRequirementOptions) {
            final String aspectCode = requirement.getAspect();
            if(aspectCode != null) {
                GameAspect<?> aspect = object.getAspect(aspectCode);
                if (aspect != null) {
                    Object fieldValue = aspect.getProperty(requirement.getField());
                    if (matchValues(fieldValue, resolveFieldValue(object, requirement.getValue()), requirement.getOperator())) {
                        return true;
                    }
                }
            } else {
                Object fieldValue = object.getProperty(requirement.getField());
                if (matchValues(fieldValue, requirement.getValue(), requirement.getOperator())) {
                    return true;
                }
            }

        }
        return false;
    }

    private Object resolveFieldValue(GameObject<?> object, Object fieldValue) {
        if (fieldValue==null) return null;

        // Check if there is anything special about this field value, otherwise just return it
        // Also note: Currently we only support replacing the ENTIRE value with the replacement.
        // Future iterations may support multiple substitutions inside a value.

        if (fieldValue instanceof String) {
            Matcher matcher = fieldSubstitutionPattern.matcher((String) fieldValue);

            // The only special replacement cases we support at the moment are  field value
            // substitutions. We get field values from another field instead of using a static
            // value...
            // Field value substitution implementation
            if(matcher.matches()) {
                String newFieldName = matcher.group(1);
                // It's for an aspect field
                if (newFieldName.contains(":")) {
                    String[] parts = newFieldName.split(":");
                    String aspectName = parts[0];
                    String aspectFieldName = parts[1];
                    return object.getAspect(aspectName).getProperty(aspectFieldName);
                }
                // It's for an object field
                else {
                    return object.getProperty(newFieldName);
                }
            }

        }
        return fieldValue;
    }


    @SuppressWarnings("OverlyComplexMethod")
    private boolean matchValues(Object fieldValue, Object requirementValue, RecipeFieldRequirementOperator operator) {
        switch (operator) {
            case EQ:
                return matchEquals(fieldValue, requirementValue);
            case NE:
                return !matchEquals(fieldValue, requirementValue);
            case LT:
                return matchLessThan(fieldValue, requirementValue, false);
            case LE:
                return matchLessThan(fieldValue, requirementValue, true);
            case GT:
                return matchLessThan(requirementValue, fieldValue, false);
            case GE:
                return matchLessThan(requirementValue, fieldValue, true);
            case CONTAINS:
                return matchContains(fieldValue, requirementValue);
            default:
                throw new IllegalArgumentException("Invalid operator: " + operator);
        }
    }

    @SuppressWarnings("OverlyComplexMethod")
    private boolean matchEquals(Object fieldValue, Object requirementValue) {
        if (fieldValue == null && requirementValue == null) return true;
        if (fieldValue == null || requirementValue == null) return false;
        if (fieldValue instanceof Number && requirementValue instanceof Number) {
            if (fieldValue instanceof Double || fieldValue instanceof Float || requirementValue instanceof Double || requirementValue instanceof Float) {
                return ((Number) fieldValue).doubleValue() == ((Number) requirementValue).doubleValue();
            } else {
                return ((Number) fieldValue).longValue() == ((Number) requirementValue).longValue();
            }
        }
        if (fieldValue.getClass() == requirementValue.getClass()) {
            return fieldValue.equals(requirementValue);
        }
        final String fieldValueString = objectToString(fieldValue);
        final String requirementValueString = objectToString(requirementValue);
        return fieldValueString.equals(requirementValueString);
    }

    private boolean matchLessThan(Object value1, Object value2, boolean includeEquals) {
        if (!(value1 instanceof Number) || !(value2 instanceof Number)) {
            return false;
        }
        double number1 = ((Number) value1).doubleValue();
        double number2 = ((Number) value2).doubleValue();
        if (includeEquals) {
            return number1 <= number2;
        } else {
            return number1 < number2;
        }
    }

    private boolean matchContains(Object fieldValue, Object requirementValue) {
        return (fieldValue instanceof String && requirementValue instanceof String) &&
                ((String) fieldValue).contains((String) requirementValue);
    }

    protected String objectToString(Object object) {
        return object.toString();
    }

    public static class RecipeSlotCombination {
        public RecipeSlot recipeSlot;
        public RecipeSlotOption recipeSlotOption;
        public RecipeConstructionSlot constructionSlot;
    }
}
