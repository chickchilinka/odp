package com.universeprojects.gefcommon.shared.elements;

import java.util.Collection;

public interface GameObject<K> {
    String getSchemaKind();
	K getKey();

    // Special field access..
    String getName();
    
    String getItemClass();

    Long getQuantity();

    void setQuantity(Long value);

    Long getDurability();

    void setDurability(Long value);

    Double getMass();

    void setMass(Double mass);

    Double getVolume();

    void setVolume(Double volume);

    // Standard field access..
    Object getProperty(String fieldName);

    void setProperty(String fieldName, Object value);

    Collection<String> getPropertyNames();

    // Aspect related access..
    Collection<String> getAspectNames();

    GameAspect<K> addAspect(String aspectName);

    GameAspect<K> getAspect(String aspectName);

    boolean hasAspect(String aspectName);

    void removeAspect(String aspectName);
}
