package com.universeprojects.html5engine.server;

import com.universeprojects.html5engine.shared.abstractFramework.AnimatedSpriteTypeFrame;

/**
 * @author Crokoking
 */
public class ServerAnimatedSpriteTypeFrame extends AnimatedSpriteTypeFrame {

    public ServerSpriteType spriteType;

    protected ServerAnimatedSpriteTypeFrame() {
        this(null);
    }

    protected ServerAnimatedSpriteTypeFrame(ServerSpriteType spriteType) {
        this(spriteType, 100);
    }

    protected ServerAnimatedSpriteTypeFrame(ServerSpriteType spriteType, int duration) {
        this(spriteType, duration, 0, 0, 0);
    }

    protected ServerAnimatedSpriteTypeFrame(ServerSpriteType spriteType, int duration, int adjustedX, int adjustedY, float adjustedScale) {
        this.spriteType = spriteType;
        this.duration = duration;
        this.adjustedX = adjustedX;
        this.adjustedY = adjustedY;
        this.adjustedScale = adjustedScale;
    }

    public ServerSpriteType getSpriteType() {
        return spriteType;
    }

    @Override
    public String getSpriteTypeKey() {
        return spriteType.label;
    }
}
