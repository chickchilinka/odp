package com.universeprojects.html5engine.server;

import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.shared.abstractFramework.AbstractResourceManager;
import com.universeprojects.html5engine.shared.abstractFramework.AnimatedSpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.Audio;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerCircle;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerRectangle;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerVector;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Crokoking
 */
@SuppressWarnings("unused")
public class ServerResourceManager extends AbstractResourceManager {

    private Map<String, ServerImage> images = new HashMap<>();
    private List<String> imageLabels = new ArrayList<>();
    private Map<String, ServerSpriteType> spriteTypes = new HashMap<>();
    private List<String> spriteTypeLabels = new ArrayList<>();
    private Map<String, ServerAnimatedSpriteType> animatedSpriteTypes = new HashMap<>();
    private List<String> animatedSpriteTypeLabels = new ArrayList<>();
    private List<String> audioLinkLabels = new ArrayList<>();

    @Override
    public void clear() {
        images.clear();
        imageLabels.clear();
        spriteTypes.clear();
        spriteTypeLabels.clear();
        animatedSpriteTypes.clear();
        animatedSpriteTypeLabels.clear();
        audioLinkLabels.clear();
    }

    public void addImage(ServerImage img) {
        Dev.checkNotNull(img);
        if (!imageLabels.contains(img.label)) {
            imageLabels.add(img.label);
        }
        images.put(img.label, img);
    }

    public boolean changeImageLabel(String oldLabel, String newLabel, boolean force) {
        if (imageLabels.contains(newLabel)) {
            if (!force) {
                return false;
            }
            removeImage(newLabel);
        }
        ServerImage img = images.get(oldLabel);
        int index = imageLabels.indexOf(oldLabel);
        images.remove(oldLabel);
        imageLabels.remove(index);
        img.label = newLabel;
        imageLabels.add(index, newLabel);
        images.put(newLabel, img);
        return true;
    }

    @Override
    public ServerImage getImage(String label) {
        return images.get(label);
    }

    @Override
    public void removeImage(String label) {
        removeImage(label, true);
    }

    /**
     * @return null if it was successful or the SpriteType that caused it to stop
     */
    public ServerSpriteType removeImage(String label, boolean force) {
        ServerImage img = images.get(label);
        for (String spriteTypeLabel : spriteTypeLabels.toArray(new String[0])) {
            ServerSpriteType sprite = getSpriteType(spriteTypeLabel);
            if (sprite.image == img) {
                if (force) {
                    removeSpriteType(spriteTypeLabel);
                } else {
                    return sprite;
                }
            }
        }
        imageLabels.remove(label);
        images.remove(label);
        return null;
    }

    public void addSpriteType(ServerSpriteType st) {
        Dev.checkNotNull(st);
        if (!spriteTypeLabels.contains(st.label)) {
            spriteTypeLabels.add(st.label);
        }
        spriteTypes.put(st.label, st);
    }

    public boolean changeSpriteTypeLabel(String oldLabel, String newLabel, boolean force) {
        if (spriteTypeLabels.contains(newLabel)) {
            if (!force) {
                return false;
            }
            removeSpriteType(newLabel);
        }
        ServerSpriteType sp = spriteTypes.get(oldLabel);
        int index = spriteTypeLabels.indexOf(oldLabel);
        spriteTypes.remove(oldLabel);
        spriteTypeLabels.remove(index);
        sp.label = newLabel;
        spriteTypeLabels.add(index, newLabel);
        spriteTypes.put(newLabel, sp);
        return true;
    }

    @Override
    public ServerSpriteType getSpriteType(String key) {
        final ServerAnimatedSpriteType animatedSpriteType = getAnimatedSpriteType(key);
        if(animatedSpriteType != null) {
            return animatedSpriteType;
        }
        return getNonAnimatedSpriteType(key);
    }

    @Override
    public ServerSpriteType getNonAnimatedSpriteType(String key) {
        return spriteTypes.get(key);
    }

    @Override
    public void removeSpriteType(String label) {
        removeSpriteType(label, true);
    }

    /**
     * @param label Label
     * @param force Remove frames that use the sprite-type
     * @return null if it was successful or the animatedSpriteFrame that caused it to stop
     */
    public ServerAnimatedSpriteType removeSpriteType(String label, boolean force) {
        ServerSpriteType sprite = getSpriteType(label);
        for (String animatedSpriteTypeLabel : animatedSpriteTypeLabels) {
            ServerAnimatedSpriteType anim = getAnimatedSpriteType(animatedSpriteTypeLabel);
            for (Iterator<ServerAnimatedSpriteTypeFrame> frameIt = anim.frames.iterator(); frameIt.hasNext(); ) {
                ServerAnimatedSpriteTypeFrame frame = frameIt.next();
                if (frame.spriteType == sprite) {
                    if (force) {
                        frameIt.remove();
                    } else {
                        return anim;
                    }
                }
            }
        }
        spriteTypeLabels.remove(label);
        spriteTypes.remove(label);
        return null;
    }

    public void addAnimatedSpriteType(ServerAnimatedSpriteType ast) {
        Dev.checkNotNull(ast);
        if (!animatedSpriteTypeLabels.contains(ast.label)) {
            animatedSpriteTypeLabels.add(ast.label);
        }
        animatedSpriteTypes.put(ast.label, ast);
    }

    public boolean changeAnimatedSpriteTypeLabel(String oldLabel, String newLabel, boolean force) {
        if (animatedSpriteTypeLabels.contains(newLabel)) {
            if (!force) {
                return false;
            }
            removeAnimatedSpriteType(newLabel);
        }
        ServerAnimatedSpriteType anim = animatedSpriteTypes.get(oldLabel);
        int index = animatedSpriteTypeLabels.indexOf(oldLabel);
        animatedSpriteTypes.remove(oldLabel);
        animatedSpriteTypeLabels.remove(index);
        anim.label = newLabel;
        animatedSpriteTypeLabels.add(index, newLabel);
        animatedSpriteTypes.put(newLabel, anim);
        return true;
    }

    @Override
    public ServerAnimatedSpriteType getAnimatedSpriteType(String label) {
        return animatedSpriteTypes.get(label);
    }

    @Override
    public void removeAnimatedSpriteType(String label) {
        animatedSpriteTypeLabels.remove(label);
        animatedSpriteTypes.remove(label);
    }

    @Override
    public void addAnimatedSpriteType(String animatedSpriteTypeKey, AnimatedSpriteType animatedSpriteType) {
        ServerAnimatedSpriteType ast = (ServerAnimatedSpriteType) animatedSpriteType;
        ast.label = animatedSpriteTypeKey;
        addAnimatedSpriteType(ast);
    }

    @Override
    public void addAudio(String key, String src) {
        if (!audioLinkLabels.contains(key)) {
            audioLinkLabels.add(key);
        }
    }

    @Override
    public void addImage(String key, String src) {
        addImage(new ServerImage(key, src, this));
    }

    @Override
    public void addSpriteType(String spriteTypeKey, SpriteType spriteType) {
        ServerSpriteType st = (ServerSpriteType) spriteType;
        st.label = spriteTypeKey;
        addSpriteType(st);
    }

    @Override
    public Collection<String> getAnimatedSpriteTypeKeys() {
        return Collections.unmodifiableCollection(animatedSpriteTypeLabels);
    }

    @Override
    public Collection<String> getAudioKeys() {
        return Collections.unmodifiableCollection(audioLinkLabels);
    }

    @Override
    public Collection<String> getImageKeys() {
        return Collections.unmodifiableCollection(imageLabels);
    }

    @Override
    public Collection<String> getSpriteTypeKeys() {
        return Collections.unmodifiableCollection(spriteTypeLabels);
    }

    @Override
    public void loadResources() {
        throw new IllegalStateException("You cannot load images on the server currently");
    }

    @Override
    public AnimatedSpriteType newAnimatedSpriteType(String animatedSpriteTypeKey) {
        return new ServerAnimatedSpriteType(this, animatedSpriteTypeKey);
    }

    @Override
    public ServerSpriteType newSpriteType(String imageKey, String spriteTypeKey) {
        ServerImage img = getImage(imageKey);
        Dev.checkNotNull(img);
        return new ServerSpriteType(spriteTypeKey, img);
    }

    @Override
    protected MarkerCircle newMarkerCircle() {
        return new ServerMarkerCircle();
    }

    @Override
    protected MarkerRectangle newMarkerRectangle() {
        return new ServerMarkerRectangle();
    }

    @Override
    protected MarkerVector newMarkerVector() {
        return new ServerMarkerVector();
    }

    @Override
    public void removeAllAudio() {
    }

    @Override
    public void removeAllImages() {
        images.clear();
        imageLabels.clear();
    }

    @Override
    public void removeAllSpriteTypes() {
        spriteTypes.clear();
        spriteTypeLabels.clear();
    }

    @Override
    public void removeAudio(String key) {
        audioLinkLabels.remove(key);
    }

    @Override
    public Audio getAudio(String key) {
        return null;
    }
}
