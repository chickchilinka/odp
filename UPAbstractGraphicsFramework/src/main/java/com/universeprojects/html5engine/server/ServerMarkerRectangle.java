package com.universeprojects.html5engine.server;

import com.universeprojects.html5engine.shared.abstractFramework.MarkerRectangle;


/**
 * @author Crokoking
 */
public class ServerMarkerRectangle extends MarkerRectangle {

    public ServerMarkerRectangle copy() {
        ServerMarkerRectangle mr = new ServerMarkerRectangle();
        mr.label = label;
        mr.x = x;
        mr.y = y;
        mr.setTypeLabel(getTypeLabel());
        mr.width = width;
        mr.height = height;
        mr.rotation = rotation;
        return mr;
    }

}
