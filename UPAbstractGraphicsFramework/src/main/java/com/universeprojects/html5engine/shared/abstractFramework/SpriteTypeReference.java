package com.universeprojects.html5engine.shared.abstractFramework;

import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;

import java.util.Objects;

public class SpriteTypeReference {
    public final String key;
    private SpriteType spriteType;

    public SpriteTypeReference(String key) {
        this.key = Dev.checkNotEmpty(key);
    }

    public static SpriteType getSpriteType(SpriteTypeReference spriteTypeRef, ResourceManager resourceManager) {
        if(spriteTypeRef == null) {
            return resourceManager.getSpriteType(null);
        } else {
            return spriteTypeRef.getSpriteType(resourceManager);
        }
    }

    private synchronized void clearCache() {
        spriteType = null;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        return "SpriteTypeReference{" +
            key +
            '}';
    }

    public synchronized SpriteType getSpriteType(ResourceManager resourceManager) {
        if(spriteType == null) {
            SpriteType spriteType = resourceManager.getSpriteType(key);
            if(spriteType instanceof H5ESpriteType) {
                ((H5ESpriteType) spriteType).subscribeToLoadStateChange((state) -> clearCache());
            }
            this.spriteType = spriteType;
            return spriteType;
        }
        return spriteType;
    }

    public synchronized SpriteType getSpriteType(H5EEngine engine) {
        if(engine == null) {
            return null;
        }
        return getSpriteType(engine.getResourceManager());
    }

    public synchronized SpriteType getSpriteType(H5ELayer layer) {
        if(layer == null) {
            return null;
        }
        return getSpriteType(layer.getEngine().getResourceManager());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SpriteTypeReference that = (SpriteTypeReference) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
