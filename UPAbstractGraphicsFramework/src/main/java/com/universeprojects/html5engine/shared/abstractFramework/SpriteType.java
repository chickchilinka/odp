package com.universeprojects.html5engine.shared.abstractFramework;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.List;

/**
 * @author Crokoking
 */
public interface SpriteType extends GraphicSource<TextureRegion> {

    int getAreaHeight();

    int getAreaWidth();

    int getAreaX();

    int getAreaY();

    /**
     * Returns the height for this sprite type. This will return the dimensions of the final sprite and not necessarily
     * the dimensions of the image data used.
     *
     * @return The sprite type's height as it would be if it were drawn on a layer
     */
    int getHeight();

    /**
     * Returns the image being used for this sprite type. If the image does not exist or is not loaded, this method will
     * return null.
     *
     * @return The H5EImage currently being used for this sprite type's image data
     */
    Image getImage();

    String getImageKey();

    /**
     * Returns the width for this sprite type. This will return the dimensions of the final sprite and not necessarily
     * the dimensions of the image data used.
     *
     * @return The sprite type's width as it would be if it were drawn on a layer
     */
    int getWidth();

    void setAreaHeight(int areaHeight);

    void setAreaWidth(int areaWidth);

    void setAreaX(int areaX);

    void setAreaY(int areaY);

    /**
     * This method will change the image key to the image the sprite is using. The next time this sprite is drawn, the
     * new image will be displayed.
     *
     * @param imageKey The image key that will be used for sprite type's image data acquisition
     */
    void setImage(String imageKey);

    /**
     * Call this method to ensure that all variables are valid, given the dimensions of the image.
     *
     * @throws RuntimeException image is not available
     */
    void stabilize();

    void addMarker(Marker e);

    void removeMarker(Marker m);

    Marker getMarker(int index);

    void addMarker(int index, Marker element);

    void removeMarker(int index);

    List<Marker> getMarkers();
}
