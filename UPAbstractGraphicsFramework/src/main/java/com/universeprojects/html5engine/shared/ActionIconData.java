package com.universeprojects.html5engine.shared;

import com.badlogic.gdx.graphics.Color;

import java.util.Objects;

public class ActionIconData {
    private static final Color COLOR_DARKEN = new Color(0.5f, 0.5f, 0.5f, 1f);
    public final String actionIcon;
    public final String itemIcon;
    public final boolean multipleTargets;
    public final Color color;

    public ActionIconData(String actionIcon, String itemIcon, Color color, boolean multipleTargets) {
        this.actionIcon = actionIcon;
        this.itemIcon = itemIcon;
        this.color = color != null ? color : Color.WHITE;
        this.multipleTargets = multipleTargets;
    }

    public ActionIconData(String actionIcon, String itemIcon, boolean darken, boolean multipleTargets) {
        this(actionIcon, itemIcon, darken ? COLOR_DARKEN : Color.WHITE, multipleTargets);
    }

    public static ActionIconData enabled(String actionIcon) {
        return new ActionIconData(actionIcon, null, false, false);
    }

    public static ActionIconData disabled(String actionIcon) {
        return new ActionIconData(actionIcon, null, true, false);
    }

    @Override
    public String toString() {
        return "ActionIconData{" +
                "actionIcon='" + actionIcon + '\'' +
                ", itemIcon='" + itemIcon + '\'' +
                ", color=" + color +
                ", multipleTargets=" + multipleTargets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActionIconData iconData = (ActionIconData) o;
        return Objects.equals(color, iconData.color) &&
                Objects.equals(actionIcon, iconData.actionIcon) &&
                Objects.equals(itemIcon, iconData.itemIcon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actionIcon, itemIcon, color);
    }
}
