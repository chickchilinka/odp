package com.universeprojects.html5engine.shared.abstractFramework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Crokoking
 */
public abstract class BaseSpriteType implements SpriteType {

    /**
     * When this sprite is going to be drawn, we use the area (areaX, areaY, areaWidth..etc) to specify what part of the
     * image to use
     */
    protected int areaHeight = 0;
    /**
     * When this sprite is going to be drawn, we use the area (areaX, areaY, areaWidth..etc) to specify what part of the
     * image to use
     */
    protected int areaWidth = 0;
    /**
     * When this sprite is going to be drawn, we use the area (areaX, areaY, areaWidth..etc) to specify what part of the
     * image to use
     */
    protected int areaX = 0;
    /**
     * When this sprite is going to be drawn, we use the area (areaX, areaY, areaWidth..etc) to specify what part of the
     * image to use
     */
    protected int areaY = 0;

    protected boolean isStable;
    protected AbstractResourceManager resourceManager = null;

    protected final List<Marker> markers = new ArrayList<>();
    protected List<Marker> markers_view;

    @Override
    public int getAreaHeight() {
        return areaHeight;
    }

    @Override
    public int getAreaWidth() {
        return areaWidth;
    }

    @Override
    public int getAreaX() {
        return areaX;
    }

    @Override
    public int getAreaY() {
        return areaY;
    }

    /**
     * Returns the height for this sprite type. This will return the dimensions of the final sprite and not necessarily
     * the dimensions of the image data used.
     *
     * @return The sprite type's height as it would be if it were drawn on a layer
     */
    @Override
    public int getHeight() {
        return getAreaHeight();
    }

    /**
     * Returns the image being used for this sprite type. If the image does not exist or is not loaded, this method will
     * return null.
     *
     * @return The H5EImage currently being used for this sprite type's image data
     */
    @Override
    public abstract Image getImage();

    @Override
    public abstract String getImageKey();

    /**
     * Returns the width for this sprite type. This will return the dimensions of the final sprite and not necessarily
     * the dimensions of the image data used.
     *
     * @return The sprite type's width as it would be if it were drawn on a layer
     */
    @Override
    public int getWidth() {
        return getAreaWidth();
    }

    @Override
    public void setAreaHeight(int areaHeight) {
        this.areaHeight = areaHeight;
    }

    @Override
    public void setAreaWidth(int areaWidth) {
        this.areaWidth = areaWidth;
    }

    @Override
    public void setAreaX(int areaX) {
        this.areaX = areaX;
    }

    @Override
    public void setAreaY(int areaY) {
        this.areaY = areaY;
    }

    /**
     * This method will change the image key to the image the sprite is using. The next time this sprite is drawn, the
     * new image will be displayed.
     *
     * @param imageKey The image key that will be used for sprite type's image data acquisition
     */
    @Override
    public abstract void setImage(String imageKey);

    /**
     * Call this method to ensure that all variables are valid, given the dimensions of the image.
     *
     * @throws RuntimeException image is not available
     */
    @Override
    public abstract void stabilize();

    @Override
    public void addMarker(Marker e) {
        markers.add(e);
    }

    @Override
    public void removeMarker(Marker m) {
        markers.remove(m);
    }

    @Override
    public Marker getMarker(int index) {
        return markers.get(index);
    }

    @Override
    public void addMarker(int index, Marker element) {
        markers.add(index, element);
    }

    @Override
    public void removeMarker(int index) {
        markers.remove(index);
    }

    @Override
    public List<Marker> getMarkers() {
        if (markers_view == null) {
            markers_view = Collections.unmodifiableList(markers);
        }
        return markers_view;
    }
}
