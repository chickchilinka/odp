package com.universeprojects.html5engine.shared.abstractFramework;

/**
 * @author Crokoking
 */
public abstract class Image {

    /**
     * This is the original url used to find the image. It is likely a relative path
     */
    protected String url = null;
    protected AbstractResourceManager resourceManager = null;

    /**
     * The height of the loaded image data. If no image is loaded, the return will be 0.
     *
     * @return The height of the loaded image data. If no image is loaded, the return will be 0.
     */
    public abstract int getHeight();

    /**
     * The original url used to load the image. It is likely a relative url.
     *
     * @return The original url string used when loadImage() was called
     */
    public String getOriginalUrl() {
        return url;
    }

    /**
     * The width of the loaded image data. If no image is loaded, the return will be 0.
     *
     * @return The width of the loaded image data. If no image is loaded, the return will be 0.
     */
    public abstract int getWidth();
}
