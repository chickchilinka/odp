package com.universeprojects.html5engine.shared.abstractFramework;

import java.util.Collection;

public interface ResourceManager {

    /**
     * This method will accept a spriteType object as a parameter and add it to this resource manager's list of sprite
     * types. This method is similar to newSpriteType() except newSpriteType() will actually create the spriteType
     * object for you. The purpose of this method is to allow extended sprite types to be stored by the resource
     * manager.
     *
     * @param spriteTypeKey The key used to access this sprite type object later
     * @param spriteType    The sprite type object you wish to be stored in the resource manager
     */
    void addSpriteType(String spriteTypeKey, SpriteType spriteType);

    /**
     * This method inserts a sprite type and sets areaWidth/Height to the width/height of the image. <br/>
     * The image has to be loaded at this point - otherwise a runtime-exception is thrown <br/>
     * This method is a convenience method intended for fast-adding of new Sprites without editing the h5l
     */
    void insertSpriteType(String imageKey, String spriteTypeKey);

    /**
     * This method will accept an animatedSpriteType object as a parameter and add it to this resource manager's list of
     * animated sprite types. This method is similar to newAnimatedSpriteType() except newAnimatedSpriteType() will
     * actually create the animatedSpriteType object for you. The purpose of this method is to allow extended animated
     * sprite types to be stored by the resource manager.
     *
     * @param animatedSpriteTypeKey The key used to access this animated sprite type object later
     * @param animatedSpriteType    The animated sprite type object you wish to be stored in the resource manager
     */
    void addAnimatedSpriteType(String animatedSpriteTypeKey, AnimatedSpriteType animatedSpriteType);

    /**
     * This method will add a single audio file into the resource manager's queue for loading.
     * <p/>
     * Note: Actual loading will not start until .load() is called.
     *
     * @param key A string identifier that will be used to get the audio file from the resourceManager again later. You
     *            may use the url for this
     * @param src The url pointing to the audio file to load
     */
    void addAudio(String key, String src);

    /**
     * This method will add a list of audio files into the resource manager's queue of audio files to load. The url used
     * to locate the audio file will also be used as a key to access it.
     *
     * @param audioList An array of urls that locate the audio files you wish to load in. It will also be used as the
     *                  audioKey
     */
    void addAudioFiles(String[] audioList);

    /**
     * This method will add a single image into the resource manager's queue for loading.
     * <p/>
     * Note: Actual loading will not start until .load() is called.
     *
     * @param key A string identifier that will be used to get the image from the resourceManager again later. You may
     *            use the url for this
     * @param src The url pointing to the image to load
     */
    void addImage(String key, String src);

    /**
     * This method will add a list of images into the resource manager's queue of images to load. The url used to locate
     * the image will also be used as a key to access it.
     *
     * @param imageList An array of urls that locate the images you wish to load in. It will also be used as the
     *                  imageKey
     */
    void addImages(String[] imageList);

    /**
     * Returns the (potentially animated) sprite type that is associted with the given spriteTypeKey.
     *
     * @param spriteTypeKey A key (string) that is used to identify a particular spriteType within the resource manager
     * @return The associated SpriteType or null if the sprite type could not be found
     */
    BaseSpriteType getSpriteType(String spriteTypeKey);

    /**
     * Returns the sprite type that is associted with the given spriteTypeKey.
     *
     * @param spriteTypeKey A key (string) that is used to identify a particular spriteType within the resource manager
     * @return The associated SpriteType or null if the sprite type could not be found
     */
    BaseSpriteType getNonAnimatedSpriteType(String spriteTypeKey);

    /**
     * Returns the animated sprite type that is associted with the given animatedSpriteTypeKey.
     *
     * @param animatedSpriteTypeKey A key (string) that is used to identify a particular animatedSpriteType within the
     *                              resource manager
     * @return The associated AnimatedSpriteType or null if the animated sprite type could not be found
     */
    AnimatedSpriteType getAnimatedSpriteType(String animatedSpriteTypeKey);

    /**
     * Returns all keys associated with SpriteType objects, stored in this resource manager, in an Array.
     *
     * @return An array of Strings; all keys that are stored against animated sprite types in this resource manager
     */
    Collection<String> getAnimatedSpriteTypeKeys();

    /**
     * TODO: javadoc
     */
    Audio getAudio(String key);

    /**
     * TODO: javadoc
     */
    Collection<String> getAudioKeys();

    /**
     * Gets an image stored in this resource manager by the given key.
     *
     * @param key The string identifier that was used when loading the image
     * @return The image associated with the given key
     */
    Image getImage(String key);

    /**
     * Returns all keys associated with Image objects, stored in this resource manager, in an Array.
     *
     * @return An array of Strings; all keys that are stored against images in this resource manager
     */
    Collection<String> getImageKeys();

    /**
     * Returns all keys associated with SpriteType objects, stored in this resource manager, in an Array.
     *
     * @return An array of Strings; all keys that are stored against sprite types in this resource manager
     */
    Collection<String> getSpriteTypeKeys();

    /**
     * Creates a new animated sprite type and stores it in the resource manager under the given animatedSpriteTypeKey.
     *
     * @param animatedSpriteTypeKey The key used to identify this animatedSpriteType so it can be used later.
     * @return The new AnimatedSpriteType object
     */
    AnimatedSpriteType newAnimatedSpriteType(String animatedSpriteTypeKey);

    /**
     * Creates a new sprite type using the given imageKey and stores it in the resource manager under the given
     * spriteTypeKey.
     *
     * @param imageKey      The key used to identify a loaded image that will be used as the graphic for the spriteType
     * @param spriteTypeKey The key used to identify this spriteType so it can be used later. This parameter can be
     *                      skipped and the imageKey will be used instead.
     * @return The new SpriteType object
     */
    BaseSpriteType newSpriteType(String imageKey, String spriteTypeKey);

    /**
     * This starts the process of loading all resources. Once the load is complete the onComplete event will fire. If
     * there was an error, the onError event will fire. If the load was aborted, the onError event will fire. Every time
     * a single resource (image, audio..etc) is finished, the onProgress event will fire.
     */
    void loadResources();


    /**
     * TODO: javadoc
     */
    void removeAllSpriteTypes();

    /**
     * Removes the animated sprite type that is associated with the given animatedSpriteTypeKey.
     *
     * @param animatedSpriteTypeKey A key (string) that is used to identify a particular animatedSpriteType within the
     *                              resource manager
     */
    void removeAnimatedSpriteType(String animatedSpriteTypeKey);

    /**
     * Sets an audio file stored in this resource manager to null according to the given key. The data should be garbage
     * collected when it is appropriate by the browser.
     *
     * @param key The string identifier that was used when loading the audio file
     */
    void removeAudio(String key);

    /**
     * Sets an image stored in this resource manager to null according to the given key.
     *
     * @param key The string identifier that was used when loading the image
     */
    void removeImage(String key);

    /**
     * Removes the sprite type that is associated with the given spriteTypeKey.
     *
     * @param spriteTypeKey A key (string) that is used to identify a particular spriteType within the resource manager
     */
    void removeSpriteType(String spriteTypeKey);

    /**
     * TODO: javadoc
     */
    void removeAllAudio();

    /**
     * TODO: javadoc
     */
    void removeAllImages();

    /**
     * TODO: javadoc
     */
    void removeAllResources();

    /**
     * TODO; javadoc
     */
    void clear();
}
