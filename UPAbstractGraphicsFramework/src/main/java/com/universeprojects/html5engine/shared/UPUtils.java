package com.universeprojects.html5engine.shared;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.universeprojects.common.shared.callable.ReturningCallable0Args;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.VoidspaceNumberFormat;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class UPUtils {

    private static final float FACTOR_3D_SHIFT = 20f;

    @SuppressWarnings("Convert2Lambda")
    public static Comparator<Class<?>> classComparator = new Comparator<Class<?>>() {
        @Override
        public int compare(Class<?> o1, Class<?> o2) {
            return o1.getCanonicalName().compareTo(o2.getCanonicalName());
        }
    };

    public static boolean isServer() {
        //noinspection SimplifiableIfStatement
        if (Gdx.app == null) return true; //Test
        return Gdx.app.getType() == Application.ApplicationType.HeadlessDesktop;
    }

    public static boolean isClient() {
        return !isServer();
    }

    public static void setAlpha(Actor container, float value) {
        container.getColor().a = value;
    }

    public static void clientSideOnly() {
        if (isServer()) {
            throw new RuntimeException("This code is only allowed to be used on the client side");
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> Class<T> castClass(Class<?> aClass) {
        return (Class<T>) aClass;
    }


    public static String createRandom8CharString() {
        String value = Long.toString(Double.doubleToLongBits(UPMath.random()), 32);
        return padStringTo8(value);
    }

    public static String padStringTo8(String value) {
        if (value.length() > 8) {
            return value.substring(0, 8);
        } else if (value.length() < 8) {
            return "00000000".substring(value.length()) + value;
        } else {
            return value;
        }
    }

    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean equal(Object a, Object b) {
        if (a == b) {
            return true;
        }

        if (a == null || b == null) {
            return false;
        }

        return a.equals(b);
    }

    public static boolean isSubclassOf(Class<?> subClass, Class<?> superClass) {
        if (superClass.isInterface()) {
            //cant test interfaces in gwt
            return true;
        }
        while (!subClass.equals(Object.class)) {
            if (subClass.equals(superClass)) {
                return true;
            }
            subClass = subClass.getSuperclass();
        }
        return false;
    }

    /**
     * Class.getSimpleName() is not supported in GWT 2.5
     */
    public static String getSimpleName(Class<?> clazz) {
        Dev.checkNotNull(clazz);
        String name = clazz.getName();
        int dotIndex = name.lastIndexOf(".");
        if (dotIndex == -1) return name;
        return name.substring(dotIndex + 1);
    }

    /**
     * Class.getSimpleName() is not supported in GWT 2.5
     */
    public static String getSimpleClassName(Object obj) {
        Dev.checkNotNull(obj);
        return getSimpleName(obj.getClass());
    }

    public static void strokeRect(ShapeRenderer shapeRenderer, int stroke, int x, int y, int width, int height) {
        shapeRenderer.rect(x, y, width, stroke);
        shapeRenderer.rect(x, y + stroke, stroke, height - stroke);
        shapeRenderer.rect(x + stroke, y + height - stroke, width - stroke, stroke);
        shapeRenderer.rect(x + width - stroke, y + stroke, stroke, height - 2 * stroke);
    }

    public static UPVector toUPVector(Vector2 location) {
        return new UPVector(location.x, location.y);
    }

    @SuppressWarnings("UnusedReturnValue")
    public static Vector2 calculate3DShift(OrthographicCamera camera, float targetX, float targetY, float zIndex, Vector2 target) {
        float shiftFactor = (1 / (FACTOR_3D_SHIFT * camera.zoom)) * zIndex;
        float shiftX = (targetX - camera.position.x) * shiftFactor;
        float shiftY = (targetY - camera.position.y) * shiftFactor;

        return target.set(shiftX, shiftY);
    }

    public static void tieButtonToKey(H5EInputBox txt, int key, H5EButton btn) {
        txt.addCaptureListener(new InputListener() {
            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (keycode == key) {
                    Gdx.app.postRunnable(() -> btn.setChecked(!btn.isChecked()));
                    return true;
                }
                return false;
            }
        });
    }

    public static void tieButtonToKey(Window window, int key, H5EButton btn) {
        window.addListener(new InputListener() {
            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (window.isVisible() && keycode == key) {
                    Gdx.app.postRunnable(() -> btn.setChecked(!btn.isChecked()));
                    return true;
                }
                return false;
            }
        });
    }


    public static void positionProportionally(Actor actor, Float screenProportionX, Float screenProportionY) {
        actor.setOrigin(Math.round(actor.getWidth() / 2f), Math.round(actor.getHeight() / 2f));
        /*
         *	Implementation note:
         *
         *  Though H5EGraphicElement supports proportional positioning, we do not use that mechanism
         *  as it introduces undesired side-effects on the rendering of sprites that assemble the
         *
         *  More specifically, due to round-off errors "seams" between the sprites become visible.
         *  This implementation makes sure to only use int precision, and simply overrides the existing
         *  coordinates.
         */

        if (screenProportionX != null && (screenProportionX < 0 || screenProportionX > 1)) {
            throw new IllegalArgumentException("Proportional position value must be a fraction between 0 and 1, or NULL");
        }
        if (screenProportionY != null && (screenProportionY < 0 || screenProportionY > 1)) {
            throw new IllegalArgumentException("Proportional position value must be a fraction between 0 and 1, or NULL");
        }
        final H5EEngine engine = ((H5ELayer)actor.getStage()).getEngine();
        int newX = screenProportionX == null ? 0 : (int)(screenProportionX * engine.getWidth());
        int newY = screenProportionY == null ? 0 : (int)(screenProportionY * engine.getHeight());

//        // Limit the top of the dialog box to the top of the screen
//        if (screenProportionY != null) {
//            float engineHeight = getEngine().getHeight();
//            newY = Math.min((int)(engineHeight - getHeight() / 2f), (int)(screenProportionY * engineHeight));
//        }

        // override the pre-existing coordinates with the new coordinates
        actor.setX(Math.round(newX), Align.center);
        actor.setY(Math.round(newY), Align.center);

        int cappedY = Math.min((int)(engine.getHeight() - actor.getHeight() / 2f), (int)(actor.getY() + actor.getHeight() / 2f));
        actor.setY(Math.round(cappedY), Align.center);
    }

    public static String denomFormatString(String num){
        StringBuilder builder = new StringBuilder(num);

        int count = 0;
        for(int i = num.length(); i > 0; i--) {
            count++;
            if(count == 4){
                builder.insert(i, ",");
                count = 1;
            }
        }
        return builder.toString();
    }

    public static Vector2 setVector2(Vector2 vector2, UPVector upVector) {
        return vector2.set(upVector.x, upVector.y);
    }

    public static Vector2 addVector2(Vector2 vector2, UPVector upVector) {
        return vector2.add(upVector.x, upVector.y);
    }

    public static boolean equalVector(UPVector upVector, Vector2 vec2) {
        return upVector.equals(vec2.x, vec2.y);
    }

    public static boolean equalVector(Vector2 vec2, UPVector upVector) {
        return upVector.equals(vec2.x, vec2.y);
    }

    public static float min(float a1, float a2, float a3, float a4) {
        return Math.min(Math.min(a1, a2), Math.min(a3, a4));
    }


    public static String secondsToTimeLongString(int seconds) {
        if (seconds >= 86400) {
            return (seconds / 86400) + " days " + ((seconds % 86400) / 3600) + " hours ";
        } else if (seconds >= 3600) {
            return (seconds / 3600) + " hours " + ((seconds % 3600) / 60) + " minutes ";
        } else if (seconds >= 60) {
            return (seconds / 60) + " minutes " + (seconds % 60) + " seconds";
        } else if (seconds >= 0) {
            return seconds + " seconds";
        } else
            return "";
    }

    public static String secondsToTimeShortString(long seconds) {
        if (seconds >= 86400) {
            return (seconds / 86400) + " days " + ((seconds % 86400) / 3600) + " hrs ";
        } else if (seconds >= 3600) {
            return (seconds / 3600) + " hrs " + ((seconds % 3600) / 60) + " min ";
        } else if (seconds >= 60) {
            return (seconds / 60) + " min " + (seconds % 60) + " sec";
        } else if (seconds >= 0) {
            return seconds + " sec";
        } else
            return "";
    }

    public static float max(float a1, float a2, float a3, float a4) {
        return Math.max(Math.max(a1, a2), Math.max(a3, a4));
    }

    public static <T> Iterable<T> fastIterable(Collection<T> collection) {
        if(collection == null || collection.isEmpty()) {
            return Collections.emptyList();
        } else {
            return collection;
        }
    }

    public static String capString(String string, int length) {
        if(string.length() <= length) {
            return string;
        }
        return string.substring(0, length);
    }

    public static String[] mergeArrays(String[] arr1, String... arr2) {
        final String[] merged = new String[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, merged, 0, arr1.length);
        System.arraycopy(arr2, 0, merged, arr1.length, arr2.length);
        return merged;
    }

    public static boolean equalsWithinDelta(float val1, float val2, float delta) {
        return UPMath.abs(val1-val2) < delta;
    }

    private static String audioFiletype = null;
    public static String GetAudioDotFiletype() {
        if (audioFiletype == null) {
            if (Gdx.app.getType() == Application.ApplicationType.iOS) {
                audioFiletype = ".mp3";
            } else {
                audioFiletype = ".ogg";
            }
        }

        return audioFiletype;
    }

    @Deprecated
    public static <T> T nullToDefault(T value, T defaultValue) {
        return Dev.withDefault(value, defaultValue);
    }


    public static String ccToHumanReadable(float cc) {
        if (cc<10000) {
            return cc+" cm\u00B3";
        }

        // Using cubic meters now
        float cM = cc/1000000f;

        // Cubic meters with 2 decimal points
        if (cM<10) {
            return VoidspaceNumberFormat.format(cM) + " m\u00B3";
        }

        // Cubic meters without decimals
        return VoidspaceNumberFormat.format(Math.round(cM)) + " m\u00B3";
    }

    public static float fixAngle(float angle) {
        while(angle < -180) {
            angle += 360;
        }
        while(angle > 180) {
            angle -= 360;
        }
        return angle;
    }

    /**
     * Make sure to return true in the task if you want the timer to continue running.
     */
    public static void simpleTimer(float delaySeconds, float executionsPerSecond, int totalExecutions, ReturningCallable0Args<Boolean> task) {
        Timer.instance().scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                Boolean success = task.call();
                if (success == Boolean.FALSE) {
                    this.cancel();
                }
            }
        }, delaySeconds, 1/executionsPerSecond, totalExecutions);
    }

    public static String capitalize(String s) {
        if(s == null) {
            return "";
        }
        if(s.length() < 2) {
            return s;
        }
        String firstChar = s.substring(0, 1);
        String otherChars = s.substring(1);
        return firstChar.toUpperCase()+otherChars.toLowerCase();
    }
}
