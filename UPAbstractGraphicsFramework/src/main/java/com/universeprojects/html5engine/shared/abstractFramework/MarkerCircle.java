package com.universeprojects.html5engine.shared.abstractFramework;

/**
 * @author Crokoking
 */
public class MarkerCircle extends Marker {

    public static final String markerClassString = "MarkerCircle";
    public int radius = 10;

    @Override
    public String getMarkerClassString() {
        return markerClassString;
    }

    // See TODO on parent class
    /*
    @Override
    public Object clone() throws CloneNotSupportedException {
        H5EAbstractMarkerCircle mc = (H5EAbstractMarkerCircle) super.clone();
        mc.radius = radius;
        return mc;
    }
    */
}
