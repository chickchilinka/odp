package com.universeprojects.html5engine.shared.abstractFramework;

/**
 * @param <T> This is the type that holds the raw data for the graphics.
 */
public interface GraphicSource<T> {
    T getGraphicData();
}
