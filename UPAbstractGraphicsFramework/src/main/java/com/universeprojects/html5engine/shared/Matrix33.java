package com.universeprojects.html5engine.shared;

/**
 * This class provides convenience methods for working with 3x3 matrices,
 * where such matrices are represented using simple 9-element arrays.
 */
@Deprecated
public class Matrix33 {

    /**
     * These constants map "logical" 3x3 matrix indices to indices in the underlying array
     */
    public static final int
        I11 = 0, I21 = 1, I31 = 2,
        I12 = 3, I22 = 4, I32 = 5,
        I13 = 6, I23 = 7, I33 = 8;


    /**
     * Convenience method to check the validity of an input matrix
     */
    private static void checkSrc(double[] src) {
        if (src == null) {
            throw new IllegalArgumentException("Expected a non-null reference to a double[] array");
        }
        if (src.length != 9) {
            throw new IllegalArgumentException("Expected a double[] array of length 9");
        }
    }

    /**
     * Convenience method to check the validity of a destination matrix
     */
    private static double[] checkDest(double[] dest) {
        if (dest == null) {
            return new double[9];
        }
        if (dest.length != 9) {
            throw new IllegalArgumentException("Expected a double[] array of length 9");
        }
        return dest;
    }

    /**
     * Writes the specified values to a destination matrix
     *
     * @param dest the destination matrix (if NULL, a new one will be created)
     * @return the updated destination matrix
     */
    public static double[] setMatrix(
        double m11, double m21, double m31,
        double m12, double m22, double m32,
        double m13, double m23, double m33,
        double[] dest) {

        dest = checkDest(dest);

        dest[I11] = m11;
        dest[I21] = m21;
        dest[I31] = m31;

        dest[I12] = m12;
        dest[I22] = m22;
        dest[I32] = m32;

        dest[I13] = m13;
        dest[I23] = m23;
        dest[I33] = m33;

        return dest;
    }

    /**
     * Cross-multiplies the provided values, and writes the results to a destination matrix
     *
     * @param dest the destination matrix (if NULL, a new one will be created)
     * @return the updated destination matrix
     */
    public static double[] matmul(
        double a11, double a21, double a31,
        double a12, double a22, double a32,
        double a13, double a23, double a33,
        double b11, double b21, double b31,
        double b12, double b22, double b32,
        double b13, double b23, double b33,
        double[] dest) {

        dest = checkDest(dest);

        double m11 = a11 * b11 + a21 * b12 + a31 * b13;
        double m21 = a11 * b21 + a21 * b22 + a31 * b23;
        double m31 = a11 * b31 + a21 * b32 + a31 * b33;

        double m12 = a12 * b11 + a22 * b12 + a32 * b13;
        double m22 = a12 * b21 + a22 * b22 + a32 * b23;
        double m32 = a12 * b31 + a22 * b32 + a32 * b33;

        double m13 = a13 * b11 + a23 * b12 + a33 * b13;
        double m23 = a13 * b21 + a23 * b22 + a33 * b23;
        double m33 = a13 * b31 + a23 * b32 + a33 * b33;

        return setMatrix(
            m11, m21, m31,
            m12, m22, m32,
            m13, m23, m33,
            dest);
    }

    /**
     * Cross-multiplies the provided matrices 'a' and 'b', and writes the results to a destination matrix
     *
     * @param dest the destination matrix (if NULL, a new one will be created)
     * @return the updated destination matrix
     */
    public static double[] matmul(double[] a, double[] b, double[] dest) {

        checkSrc(a);
        checkSrc(b);

        return matmul(
            a[I11], a[I21], a[I31],
            a[I12], a[I22], a[I32],
            a[I13], a[I23], a[I33],
            b[I11], b[I21], b[I31],
            b[I12], b[I22], b[I32],
            b[I13], b[I23], b[I33],
            dest);
    }

    /**
     * Cross-multiplies the provided values with a matrix 'b', and writes the results to a destination matrix
     *
     * @param dest the destination matrix (if NULL, a new one will be created)
     * @return the updated destination matrix
     */
    public static double[] matmul(
        double a11, double a21, double a31,
        double a12, double a22, double a32,
        double a13, double a23, double a33,
        double[] b, double[] dest) {

        checkSrc(b);

        return matmul(
            a11, a21, a31,
            a12, a22, a32,
            a13, a23, a33,
            b[I11], b[I21], b[I31],
            b[I12], b[I22], b[I32],
            b[I13], b[I23], b[I33],
            dest);
    }

    /**
     * Cross-multiplies a matrix 'a' with the provided values, and writes the results to a destination matrix
     *
     * @param dest the destination matrix (if NULL, a new one will be created)
     * @return the updated destination matrix
     */
    public static double[] matmul(double[] a,
                                  double b11, double b21, double b31,
                                  double b12, double b22, double b32,
                                  double b13, double b23, double b33,
                                  double[] dest) {
        checkSrc(a);

        return matmul(
            a[I11], a[I21], a[I31],
            a[I12], a[I22], a[I32],
            a[I13], a[I23], a[I33],
            b11, b21, b31,
            b12, b22, b32,
            b13, b23, b33,
            dest);
    }

    /**
     * Computes the inverse of a matrix represented by the specified values, and writes the results to a destination matrix
     *
     * @param dest the destination matrix (if NULL, a new one will be created)
     * @return the updated destination matrix, or NULL if the original matrix is not invertible
     */
    public static double[] invert(
        double m11, double m21, double m31,
        double m12, double m22, double m32,
        double m13, double m23, double m33,
        double[] dest) {

        // matrix determinant
        double det =
            m11 * (m22 * m33 - m32 * m23) -
                m12 * (m21 * m33 - m23 * m31) +
                m13 * (m21 * m32 - m22 * m31);

        if (det == 0) {
            // if the determinant is 0, the matrix is not invertible
            return null;
        }

        // the determinant of the inverse of an invertible matrix is the inverse of the determinant
        double invdet = 1 / det;

        if (dest == null) {
            dest = new double[9];
        }
        double d11 = (m22 * m33 - m32 * m23) * invdet;
        double d12 = (m13 * m32 - m12 * m33) * invdet;
        double d13 = (m12 * m23 - m13 * m22) * invdet;
        double d21 = (m23 * m31 - m21 * m33) * invdet;
        double d22 = (m11 * m33 - m13 * m31) * invdet;
        double d23 = (m21 * m13 - m11 * m23) * invdet;
        double d31 = (m21 * m32 - m31 * m22) * invdet;
        double d32 = (m31 * m12 - m11 * m32) * invdet;
        double d33 = (m11 * m22 - m21 * m12) * invdet;

        return setMatrix(
            d11, d21, d31,
            d12, d22, d32,
            d13, d23, d33,
            dest);
    }

    /**
     * Computes the inverse of a matrix 'm', and writes the results to a destination matrix
     *
     * @param dest the destination matrix (if NULL, a new one will be created)
     * @return the updated destination matrix, or NULL if the original matrix is not invertible
     */
    public static double[] invert(double[] m, double[] dest) {
        checkSrc(m);

        return invert(
            m[I11], m[I21], m[I31],
            m[I12], m[I22], m[I32],
            m[I13], m[I23], m[I33],
            dest);
    }


}
