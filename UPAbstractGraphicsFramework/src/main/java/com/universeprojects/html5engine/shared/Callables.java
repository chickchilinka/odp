package com.universeprojects.html5engine.shared;

import com.universeprojects.common.shared.callable.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class Callables {
    private Callables() {
    }

    private static Callable0Args emptyCallable0Args = () -> {
    };
    private static Callable1Args emptyCallable1Args = (a1) -> {
    };
    private static Callable2Args emptyCallable2Args = (a1, a2) -> {
    };
    private static Callable3Args emptyCallable3Args = (a1, a2, a3) -> {
    };

    private static ReturningCallable0Args emptyReturningCallable0Args = () -> null;
    private static ReturningCallable1Args emptyReturningCallable1Args = (a1) -> null;
    private static ReturningCallable2Args emptyReturningCallable2Args = (a1, a2) -> null;
    private static ReturningCallable3Args emptyReturningCallable3Args = (a1, a2, a3) -> null;

    public static Callable0Args emptyCallable0Args() {
        return emptyCallable0Args;
    }

    @SuppressWarnings("unchecked")
    public static <A1> Callable1Args<A1> emptyCallable1Args() {
        return emptyCallable1Args;
    }

    @SuppressWarnings("unchecked")
    public static <A1, A2> Callable2Args<A1, A2> emptyCallable2Args() {
        return emptyCallable2Args;
    }

    @SuppressWarnings("unchecked")
    public static <A1, A2, A3> Callable3Args<A1, A2, A3> emptyCallable3Args() {
        return emptyCallable3Args;
    }

    @SuppressWarnings("unchecked")
    public static <R> ReturningCallable0Args<R> emptyReturningCallable0Args() {
        return emptyReturningCallable0Args;
    }

    @SuppressWarnings("unchecked")
    public static <R, A1> ReturningCallable1Args<R, A1> emptyReturningCallable1Args() {
        return emptyReturningCallable1Args;
    }

    @SuppressWarnings("unchecked")
    public static <R, A1, A2> ReturningCallable2Args<R, A1, A2> emptyReturningCallable2Args() {
        return emptyReturningCallable2Args;
    }

    @SuppressWarnings("unchecked")
    public static <R, A1, A2, A3> ReturningCallable3Args<R, A1, A2, A3> emptyReturningCallable3Args() {
        return emptyReturningCallable3Args;
    }

    @SuppressWarnings("unchecked")
    public static void call(Callable callable, Object[] args) {
        if (callable == null) {
           throw new NullPointerException("Callables.call() called with null callable");
        }
        if (callable instanceof ReturningCallable) {
            call((ReturningCallable) callable, args);
            return;
        }
        if (callable instanceof Callable0Args) {
            //no check here
            ((Callable0Args) callable).call();
        } else if (callable instanceof Callable1Args) {
            checkArgs(1, args);
            ((Callable1Args) callable).call(args[0]);
        } else if (callable instanceof Callable2Args) {
            checkArgs(2, args);
            ((Callable2Args) callable).call(args[0], args[1]);
        } else if (callable instanceof Callable3Args) {
            checkArgs(3, args);
            ((Callable3Args) callable).call(args[0], args[1], args[2]);
        } else {
            throw new RuntimeException("Invalid Callable class: " + callable.getClass().getCanonicalName());
        }
    }

    @SuppressWarnings("unchecked")
    public static <R> R call(ReturningCallable<R> callable, Object[] args) {
        if (callable instanceof ReturningCallable0Args) {
            //no check here
            return ((ReturningCallable0Args<R>) callable).call();
        } else if (callable instanceof ReturningCallable1Args) {
            checkArgs(1, args);
            return (R) ((ReturningCallable1Args) callable).call(args[0]);
        } else if (callable instanceof ReturningCallable2Args) {
            checkArgs(2, args);
            return (R) ((ReturningCallable2Args) callable).call(args[0], args[1]);
        } else if (callable instanceof ReturningCallable3Args) {
            checkArgs(3, args);
            return (R) ((ReturningCallable3Args) callable).call(args[0], args[1], args[2]);
        } else {
            throw new RuntimeException("Invalid event handler class: " + callable.getClass().getCanonicalName());
        }
    }

    private static void checkArgs(int num, Object[] args) {
        if (num > 0 && args == null) {
            throw new WrongArgumentsException("Attempted to call callable with too few arguments: callable=" + num + " args=null");
        } else if (args != null && num != args.length) {
            throw new WrongArgumentsException("Attempted to call callable with the wrong number of arguments: callable=" + num + " args=" + args.length);
        }
    }

    public static <T, R, C extends Collection<R>> C collectMutable(Collection<T> collection, C target, ReturningCallable1Args<R, T> transformer) {
        if (collection.isEmpty()) return target;
        for (T element : collection) {
            target.add(transformer.call(element));
        }
        return target;
    }

    public static <T, R> List<R> collect(Collection<T> collection, ReturningCallable1Args<R, T> transformer) {
        if (collection.isEmpty()) return Collections.emptyList();
        return collectMutable(collection, new ArrayList<>(collection.size()), transformer);
    }

    public static <K, V, R, C extends Collection<R>> C collectMutable(Map<K, V> map, C target, ReturningCallable2Args<R, K, V> transformer) {
        if (map.isEmpty()) return target;
        for (Map.Entry<K, V> entry : map.entrySet()) {
            target.add(transformer.call(entry.getKey(), entry.getValue()));
        }
        return target;
    }

    public static <K, V, R> List<R> collect(Map<K, V> map, List<R> list, ReturningCallable2Args<R, K, V> transformer) {
        if (map.isEmpty()) return Collections.emptyList();
        return collectMutable(map, new ArrayList<>(map.size()), transformer);
    }

    public static <G, T, C extends Collection<T>> Map<G, C> group(Collection<T> collection, Map<G, C> groupMap, ReturningCallable0Args<C> collectionCreator, ReturningCallable1Args<G, T> transformer) {
        if (collection.isEmpty()) return Collections.emptyMap();
        for (T element : collection) {
            G group = transformer.call(element);
            C groupData = groupMap.get(group);
            if (groupData == null) {
                groupData = collectionCreator.call();
                groupMap.put(group, groupData);
            }
            groupData.add(element);
        }
        return groupMap;
    }

    public static <G, T> Map<G, List<T>> group(Collection<T> collection, ReturningCallable1Args<G, T> transformer) {
        if (collection.isEmpty()) return Collections.emptyMap();
        return group(collection, new LinkedHashMap<>(), ArrayList::new, transformer);
    }

    public static <T> T find(List<T> collection, ReturningCallable1Args<Boolean, T> transformer) {
        if (collection.isEmpty()) return null;
        for (T element : collection) {
            if (transformer.call(element)) {
                return element;
            }
        }
        return null;
    }

    public static <T> List<T> filter(List<T> collection, ReturningCallable1Args<Boolean, T> transformer) {
        if (collection.isEmpty()) return Collections.emptyList();
        List<T> list = new ArrayList<>();
        for (T element : collection) {
            if (transformer.call(element)) {
                list.add(element);
            }
        }
        return list;
    }

    @SuppressWarnings("Java8CollectionRemoveIf")
    public static <T, C extends Collection<T>> C filterMutable(C collection, ReturningCallable1Args<Boolean, T> transformer) {
        if (collection.isEmpty()) return collection;
        for (Iterator<T> iterator = collection.iterator(); iterator.hasNext(); ) {
            T element = iterator.next();
            if (!transformer.call(element)) {
                iterator.remove();
            }
        }
        return collection;
    }

    public static <K, V> Map<K, V> filter(Map<K, V> collection, ReturningCallable2Args<Boolean, K, V> transformer) {
        if (collection.isEmpty()) return Collections.emptyMap();
        Map<K, V> map = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : collection.entrySet()) {
            if (transformer.call(entry.getKey(), entry.getValue())) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map;
    }

    @SuppressWarnings("Java8CollectionRemoveIf")
    public static <K, V, M extends Map<K, V>> M filterMutable(M collection, ReturningCallable2Args<Boolean, K, V> transformer) {
        if (collection.isEmpty()) return collection;
        for (Iterator<Map.Entry<K, V>> iterator = collection.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<K, V> entry = iterator.next();
            if (!transformer.call(entry.getKey(), entry.getValue())) {
                iterator.remove();
            }
        }
        return collection;
    }

    public static <T, K, V, M extends Map<K, V>> M collectEntriesMutable(Collection<T> collection, M target, ReturningCallable1Args<KeyValuePair<K, V>, T> transformer) {
        if (collection.isEmpty()) return target;
        for (T element : collection) {
            KeyValuePair<K, V> pair = transformer.call(element);
            if (pair != null) {
                target.put(pair.key, pair.value);
            }
        }
        return target;
    }

    public static <T, K, V> Map<K, V> collectEntries(Collection<T> collection, ReturningCallable1Args<KeyValuePair<K, V>, T> transformer) {
        if (collection.isEmpty()) return Collections.emptyMap();
        return collectEntriesMutable(collection, new LinkedHashMap<>(), transformer);
    }

    public static <KI, VI, KO, VO, M extends Map<KO, VO>> M collectEntriesMutable(Map<KI, VI> collection, M target, ReturningCallable2Args<KeyValuePair<KO, VO>, KI, VI> transformer) {
        if (collection.isEmpty()) return target;
        for (Map.Entry<KI, VI> entry : collection.entrySet()) {
            KeyValuePair<KO, VO> pair = transformer.call(entry.getKey(), entry.getValue());
            if (pair != null) {
                target.put(pair.key, pair.value);
            }
        }
        return target;
    }

    public static <KI, VI, KO, VO> Map<KO, VO> collectEntriesMutable(Map<KI, VI> collection, ReturningCallable2Args<KeyValuePair<KO, VO>, KI, VI> transformer) {
        if (collection.isEmpty()) return Collections.emptyMap();
        return collectEntriesMutable(collection, new LinkedHashMap<>(), transformer);
    }

    public static class WrongArgumentsException extends RuntimeException {
        public WrongArgumentsException(String message) {
            super(message);
        }
    }
}
