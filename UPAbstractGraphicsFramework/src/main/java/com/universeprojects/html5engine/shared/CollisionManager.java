/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.universeprojects.html5engine.shared;

import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.html5engine.shared.abstractFramework.Marker;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerCircle;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerRectangle;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerVector;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteType;

/**
 * @author Crokoking
 */
@Deprecated
public class CollisionManager {

    public static final double epsilon = 0.0001;

    private CollisionManager() throws Exception {
        throw new Exception("This is a static helper-class only");
    }

    public static boolean isColliding(UPVector location1, Transformation transformation1, SpriteType spriteType1, int markerIndex1,
                                      UPVector location2, Transformation transformation2, SpriteType spriteType2, int markerIndex2) {
        Marker marker1 = spriteType1.getMarker(markerIndex1);
        Marker marker2 = spriteType2.getMarker(markerIndex2);
        return isColliding(location1, transformation1, spriteType1, marker1, location2, transformation2, spriteType2, marker2);
    }

    public static boolean isColliding(UPVector location1, Transformation transformation1, SpriteType spriteType1, Marker marker1,
                                      UPVector location2, Transformation transformation2, SpriteType spriteType2, Marker marker2) {
        if (!collidePreCheck(location1, spriteType1, marker1, location2, spriteType2, marker2)) {
            return false;
        }
        if (marker1 instanceof MarkerCircle && marker2 instanceof MarkerCircle) {
            return isColliding(transformation1, (MarkerCircle) marker1, transformation2, (MarkerCircle) marker2);
        } else if (marker1 instanceof MarkerRectangle && marker2 instanceof MarkerRectangle) {
            return isColliding(transformation1, (MarkerRectangle) marker1, transformation2, (MarkerRectangle) marker2);
        } else if (marker1 instanceof MarkerVector && marker2 instanceof MarkerVector) {
            return isColliding(transformation1, (MarkerVector) marker1, transformation2, (MarkerVector) marker2);
        } else if (marker1 instanceof MarkerRectangle && marker2 instanceof MarkerCircle) {
            return isColliding(transformation1, (MarkerRectangle) marker1, transformation2, (MarkerCircle) marker2);
        } else if (marker1 instanceof MarkerCircle && marker2 instanceof MarkerRectangle) {
            return isColliding(transformation1, (MarkerRectangle) marker2, transformation2, (MarkerCircle) marker1);
        } else if (marker1 instanceof MarkerVector && marker2 instanceof MarkerCircle) {
            return isColliding(transformation1, (MarkerVector) marker1, transformation2, (MarkerCircle) marker2);
        } else if (marker1 instanceof MarkerCircle && marker2 instanceof MarkerVector) {
            return isColliding(transformation2, (MarkerVector) marker2, transformation1, (MarkerCircle) marker1);
        } else if (marker1 instanceof MarkerVector && marker2 instanceof MarkerRectangle) {
            return isColliding(transformation1, (MarkerVector) marker1, transformation2, (MarkerRectangle) marker2);
        } else if (marker1 instanceof MarkerRectangle && marker2 instanceof MarkerVector) {
            return isColliding(transformation2, (MarkerVector) marker2, transformation1, (MarkerRectangle) marker1);
        }
        return false;
    }

    private static boolean isColliding(Transformation trVec, MarkerVector mv, Transformation trCirc, MarkerCircle mc) {
        Vector p1 = new Vector(mv.x, mv.y);
        Vector p2 = new Vector(mv.x + mv.deltaX, mv.y + mv.deltaX);
        Transformation tr = new Transformation();
        tr.multiply(trVec);
        tr.multiplyInverse(trCirc);
        tr.transformLocal(p1);
        tr.transformLocal(p2);
        return getRayCircleCollision(p1, p2, new Vector(mc.x, mc.y), mc.radius);
    }

    private static boolean isColliding(Transformation trVec, MarkerVector mv, Transformation trRect, MarkerRectangle mr) {
        Vector p1 = new Vector(mv.x, mv.y);
        Vector p2 = new Vector(mv.x + mv.deltaX, mv.y + mv.deltaX);
        Transformation tr = trRect.inverse().multiply(trVec);
        tr.multiply(new Transformation(mr.x, mr.y, 0, 0, mr.rotation, 1, 1));
        tr.transformInverseLocal(p1);
        tr.transformInverseLocal(p2);

        Vector pr[] = new Vector[4];
        pr[0] = new Vector(-mr.width / 2., -mr.height / 2.);
        pr[1] = new Vector(+mr.width / 2., -mr.height / 2.);
        pr[2] = new Vector(+mr.width / 2., +mr.height / 2.);
        pr[3] = new Vector(-mr.width / 2., +mr.height / 2.);
        if (p1.getX() >= -mr.width / 2. && p1.getX() <= mr.width / 2. && p1.getY() >= -mr.height / 2. && p1.getY() <= mr.height / 2.) {
            return true;
        }
        if (p2.getX() >= -mr.width / 2. && p2.getX() <= mr.width / 2. && p2.getY() >= -mr.height / 2. && p2.getY() <= mr.height / 2.) {
            return true;
        }
        for (int i = 0; i < 4; i++) {
            Vector q1 = pr[i];
            Vector q2 = pr[(i + 1) % 4];
            if (getRayLineCollision(p1, p2, q1, q2)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isColliding(Transformation tr1, MarkerVector mv1, Transformation tr2, MarkerVector mv2) {
        Vector p1 = new Vector(mv1.x, mv1.y);
        Vector p2 = new Vector(mv1.x + mv1.deltaX, mv1.y + mv2.deltaX);
        Vector q1 = new Vector(mv2.x, mv2.y);
        Vector q2 = new Vector(mv2.x + mv2.deltaX, mv2.y + mv2.deltaY);
        tr1.transformLocal(p1);
        tr1.transformLocal(p2);
        tr2.transformLocal(q1);
        tr2.transformLocal(q2);
        return getRayLineCollision(p1, p2, q1, q2);
    }

    private static boolean isColliding(Transformation trRect, MarkerRectangle mr, Transformation trCirc, MarkerCircle mc) {
        Vector pr[] = new Vector[4];
        pr[0] = new Vector(-mr.width / 2., -mr.height / 2.);
        pr[1] = new Vector(+mr.width / 2., -mr.height / 2.);
        pr[2] = new Vector(+mr.width / 2., +mr.height / 2.);
        pr[3] = new Vector(-mr.width / 2., +mr.height / 2.);
        // transform circle into the rect's coordinate system
        double r = trCirc.transformDeltaScale(mc.radius);
        Vector pc = new Vector(mc.x, mc.y);
        trCirc.transformLocal(pc);
        trRect.transformInverseLocal(pc);
        pc.translate(-mr.x, -mr.y);
        pc.rotateDeg(-mr.rotation);
        if (pc.getX() >= -mr.width / 2. && pc.getX() <= mr.width / 2. && pc.getX() >= -mr.height / 2. && pc.getY() <= mr.height / 2.) {
            return true;
        }
        for (int i = 0; i < 4; i++) {
            Vector p1 = pr[i];
            Vector p2 = pr[(i + 1) % 4];
            if (getRayCircleCollision(p1, p2, pc, r)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isColliding(Transformation tr1, MarkerCircle mc1, Transformation tr2, MarkerCircle mc2) {
        Vector p1 = new Vector(mc1.x, mc1.y);
        Vector p2 = new Vector(mc2.x, mc2.y);
        tr1.transformLocal(p1);
        tr2.transformLocal(p2);
        double rad = tr1.transformDeltaScale(mc1.radius) + tr2.transformDeltaScale(mc2.radius);
        return p1.distance2(p2) <= rad * rad;
    }

    private static boolean isColliding(Transformation tr1, MarkerRectangle mr1, Transformation tr2, MarkerRectangle mr2) {
        Vector pr1[] = new Vector[4];
        pr1[0] = new Vector(-mr1.width / 2., -mr1.height / 2.);
        pr1[1] = new Vector(+mr1.width / 2., -mr1.height / 2.);
        pr1[2] = new Vector(+mr1.width / 2., +mr1.height / 2.);
        pr1[3] = new Vector(-mr1.width / 2., +mr1.height / 2.);
        Vector pr2[] = new Vector[4];
        pr2[0] = new Vector(-mr2.width / 2., -mr2.height / 2.);
        pr2[1] = new Vector(+mr2.width / 2., -mr2.height / 2.);
        pr2[2] = new Vector(+mr2.width / 2., +mr2.height / 2.);
        pr2[3] = new Vector(-mr2.width / 2., +mr2.height / 2.);
        Transformation trans = new Transformation();
        trans.rotateDeg(-mr1.rotation);
        trans.translate(-mr1.x, -mr1.y);
        trans.multiplyInverse(tr1);
        trans.multiply(tr2);
        trans.translate(mr2.x, mr2.y);
        trans.rotateDeg(mr2.rotation);

        //Transform the second rect into the coordinate system of the first rect
        for (Vector p : pr2) {
            trans.transformLocal(p);
            if (p.getX() >= -mr1.width / 2. && p.getX() <= mr1.width / 2. && p.getY() >= -mr1.height / 2. && p.getY() <= mr1.height / 2.) {
                // point is inside the first rect
                return true;
            }
        }
        for (int i = 0; i < 4; i++) {
            Vector p1 = pr2[i];
            Vector p2 = pr2[(i + 1) % 4];
            //TODO: Can probably be optimized using the special position of mr1
            for (int j = 0; j < 4; j++) {
                Vector q1 = pr1[j];
                Vector q2 = pr1[(j + 1) % 4];
                if (getRayLineCollision(p1, p2, q1, q2)) {
                    // rect-lines intersect
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isColliding(double pointX, double pointY, Transformation tr, SpriteType spriteType, int markerIndex) {
        Marker marker = spriteType.getMarker(markerIndex);
        return isColliding(pointX, pointY, tr, marker);
    }

    public static boolean isColliding(double pointX, double pointY, Transformation tr, Marker marker) {
        Vector p = new Vector(pointX, pointY);
        tr.transformInverseLocal(p);
        if (marker instanceof MarkerCircle) {

            MarkerCircle mc = (MarkerCircle) marker;
            return p.distance2(mc.x, mc.y) <= mc.radius * mc.radius;
        } else if (marker instanceof MarkerRectangle) {
            MarkerRectangle mr = (MarkerRectangle) marker;
            p.translate(-mr.x, -mr.y);
            p.rotateDeg(-mr.rotation);
            return (p.getX() >= -mr.width / 2. && p.getX() <= mr.width / 2. && p.getY() >= -mr.height / 2. && p.getY() < mr.height / 2.);
        } else {
            MarkerVector mv = (MarkerVector) marker;
            if (mv.deltaX != 0) {
                return UPMath.abs(mv.x - p.getX()) < epsilon;
            } else if (UPMath.abs(mv.deltaY) < epsilon) {
                return (UPMath.abs(mv.y - p.getY()) < epsilon);
            }
            return UPMath.abs((p.getX() - mv.x) / mv.deltaX - (p.getY() - mv.y) / mv.deltaY) < epsilon;
        }
    }

    public static boolean isColliding(double centerX, double centerY, double radius, Transformation transformation, SpriteType spriteType, int markerIndex) {
        Marker marker = spriteType.getMarker(markerIndex);
        return isColliding(centerX, centerY, radius, transformation, marker);
    }

    public static boolean isColliding(double centerX, double centerY, double radius, Transformation transformation, Marker marker) {
        Vector p = new Vector(centerX, centerY);
        transformation.transformInverseLocal(p);
        if (marker instanceof MarkerCircle) {
            MarkerCircle mc = (MarkerCircle) marker;
            return p.distance2(mc.x, mc.y) < (radius + mc.radius) * (radius + mc.radius);
        } else if (marker instanceof MarkerRectangle) {
            MarkerRectangle mr = (MarkerRectangle) marker;
            p.translate(-mr.x, -mr.y);
            p.rotateDeg(-mr.rotation);
            if (p.getX() >= -mr.width / 2. && p.getX() <= mr.width / 2. && p.getY() >= -mr.height / 2. && p.getY() <= mr.height / 2.) {
                return true;
            }

            Vector rect[] = new Vector[4];
            rect[0] = new Vector(-mr.width / 2., -mr.height / 2.);
            rect[1] = new Vector(+mr.width / 2., -mr.height / 2.);
            rect[2] = new Vector(+mr.width / 2., +mr.height / 2.);
            rect[3] = new Vector(-mr.width / 2., +mr.height / 2.);
            for (int i = 0; i < 4; i++) {
                Vector q1 = rect[i];
                Vector q2 = rect[(i + 1) % 4];
                if (getRayCircleCollision(q1, q2, p, radius)) {
                    return true;
                }
            }
            return false;
        } else {
            MarkerVector mv = (MarkerVector) marker;
            return getRayCircleCollision(new Vector(mv.x, mv.y), new Vector(mv.x + mv.deltaX, mv.y + mv.deltaX), p, radius);
        }
    }


    /**
     * Calculates if the line segment defined by the first four parameters and the marker defined by the last two collide
     */
    public static boolean isColliding(double rayStartX, double rayStartY, double rayEndX, double rayEndY, Transformation tr, SpriteType spriteType, int markerIndex) {
        return getCollisionPoint(rayStartX, rayStartY, rayEndX, rayEndY, tr, spriteType, markerIndex) != null;
    }

    public static Vector getCollisionPoint(double rayStartX, double rayStartY, double rayEndX, double rayEndY, Transformation tr, SpriteType spriteType, int markerIndex) {
        Marker marker = spriteType.getMarker(markerIndex);
        return getCollisionPoint(rayStartX, rayStartY, rayEndX, rayEndY, tr, marker);
    }

    /**
     * Calculates the collision point between the line segment defined by the first four parameters and the marker defined by the last two
     */
    public static Vector getCollisionPoint(double rayStartX, double rayStartY, double rayEndX, double rayEndY, Transformation tr, Marker marker) {
        Vector rayStart = new Vector(rayStartX, rayStartY);
        Vector p1 = rayStart.copy();
        Vector p2 = new Vector(rayEndX, rayEndY);
        Vector collPoint = null;
        tr.transformInverseLocal(p1);
        tr.transformInverseLocal(p2);
        if (marker instanceof MarkerCircle) {
            MarkerCircle mc = (MarkerCircle) marker;
            collPoint = getRayCircleCollisionPoint(p1, p2, new Vector(mc.x, mc.y), mc.radius);
        } else if (marker instanceof MarkerRectangle) {
            MarkerRectangle mr = (MarkerRectangle) marker;
            p1.translate(-mr.x, -mr.y);
            p1.rotateDeg(-mr.rotation);
            p2.translate(-mr.x, -mr.y);
            p2.rotateDeg(-mr.rotation);

            if (p1.getX() >= -mr.width / 2. && p1.getX() <= mr.width / 2. && p1.getY() >= -mr.height / 2. && p1.getY() <= mr.height / 2.) {
                return rayStart;
            }

            Vector rect[] = new Vector[4];
            rect[0] = new Vector(-mr.width / 2., -mr.height / 2.);
            rect[1] = new Vector(+mr.width / 2., -mr.height / 2.);
            rect[2] = new Vector(+mr.width / 2., +mr.height / 2.);
            rect[3] = new Vector(-mr.width / 2., +mr.height / 2.);
            for (int i = 0; i < 4; i++) {
                Vector q1 = rect[i];
                Vector q2 = rect[(i + 1) % 4];
                Vector coll = getRayLineCollisionPoint(p1, p2, q1, q2);
                if (coll != null && (collPoint == null || coll.distance2(p1) < collPoint.distance2(p1))) {
                    collPoint = coll;
                }
            }
            if (collPoint != null) {
                collPoint.rotateDeg(mr.rotation);
                collPoint.translate(mr.x, mr.y);
            }
        } else {
            MarkerVector mv = (MarkerVector) marker;
            collPoint = getRayLineCollisionPoint(p1, p2, new Vector(mv.x, mv.y),
                new Vector(mv.x + mv.deltaX, mv.y + mv.deltaX));
        }
        if (collPoint != null) {
            tr.transformLocal(collPoint);
        }
        return collPoint;
    }

    public static boolean getRayLineCollision(double line1StartX, double line1StartY, double line1EndX, double line1EndY,
                                              double line2StartX, double line2StartY, double line2EndX, double line2EndY) {
        return getRayLineCollision(
            new Vector(line1StartX, line1StartY),
            new Vector(line1EndX, line1EndY),
            new Vector(line2StartX, line2StartY),
            new Vector(line2EndX, line2EndY));
    }

    public static boolean getRayLineCollision(
        Vector rayStart, Vector rayEnd,
        Vector testLineStart, Vector testLineEnd) {
        return getRayLineCollisionPoint(rayStart, rayEnd, testLineStart, testLineEnd) != null;
    }

    public static Vector getRayLineCollisionPoint(
        double line1StartX, double line1StartY, double line1EndX, double line1EndY,
        double line2StartX, double line2StartY, double line2EndX, double line2EndY) {
        return getRayLineCollisionPoint(
            new Vector(line1StartX, line1StartY),
            new Vector(line1EndX, line1EndY),
            new Vector(line2StartX, line2StartY),
            new Vector(line2EndX, line2EndY));
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public static Vector getRayLineCollisionPoint(
        //After http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
        Vector rayStart, Vector rayEnd,
        Vector testLineStart, Vector testLineEnd) {
        if (UPMath.max(rayStart.getX(), rayEnd.getX()) < UPMath.min(testLineStart.getX(), testLineEnd.getX())) {
            return null;
        }
        if (UPMath.max(rayStart.getY(), rayEnd.getY()) < UPMath.min(testLineStart.getY(), testLineEnd.getY())) {
            return null;
        }
        if (UPMath.max(testLineStart.getX(), testLineEnd.getX()) < UPMath.min(rayStart.getX(), rayEnd.getX())) {
            return null;
        }
        if (UPMath.max(testLineStart.getY(), testLineEnd.getY()) < UPMath.min(rayStart.getY(), rayEnd.getY())) {
            return null;
        }
        Vector p = rayStart;
        Vector q = testLineStart;
        Vector r = rayEnd.sub(rayStart);
        Vector s = testLineEnd.sub(testLineStart);
        Vector qmp = q.sub(p);
        double rcs = crossAbs(r, s);
        if (UPMath.abs(rcs) < epsilon) {
            //parallel
            if (UPMath.abs(crossAbs(qmp, r)) < epsilon) {
                //collinear
                double tTestLineStart;
                double tTestLineEnd;
                if (r.getX() != 0) {
                    tTestLineStart = (testLineStart.getX() - p.getX()) / r.getX();
                    tTestLineEnd = (testLineEnd.getY() - p.getY()) / r.getX();
                } else {
                    tTestLineStart = (testLineStart.getY() - p.getY()) / r.getY();
                    tTestLineEnd = (testLineEnd.getY() - p.getY()) / r.getY();
                }
                if ((tTestLineStart <= 0 && tTestLineEnd >= 0) || (tTestLineEnd <= 0 && tTestLineStart >= 0)) {
                    return rayStart;
                } else if (tTestLineStart <= 1 && tTestLineEnd > 1) {
                    return testLineStart;
                } else if (tTestLineEnd <= 1 && tTestLineStart > 1) {
                    return testLineEnd;
                }
            }
            return null;
        }
        double t = crossAbs(qmp, s.mul(1. / rcs));
        double u = crossAbs(qmp, r.mul(1. / rcs));
        if (t >= 0 && t <= 1 && u >= 0 && u <= 1) {
            return p.add(r.mul(t));
        }
        return null;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    private static double getRayCircleCollisionT(Vector rayStart, Vector rayEnd,
                                                 Vector circleCenter, double radius) {
        //After http://blog.csharphelper.com/2010/03/28/determine-where-a-line-intersects-a-circle-in-c.aspx
        Vector point1 = rayStart;
        Vector point2 = rayEnd;
        if (circleCenter.distance2(rayStart) < radius * radius) {
            return 0;
        }
        double dx = point2.getX() - point1.getX();
        double dy = point2.getY() - point1.getY();
        double cx = circleCenter.getX();
        double cy = circleCenter.getY();

        double A = dx * dx + dy * dy;
        double B = 2 * (dx * (point1.getX() - cx) + dy * (point1.getY() - cy));
        double C = (point1.getX() - cx) * (point1.getX() - cx) + (point1.getY() - cy) * (point1.getY() - cy) - radius * radius;

        double det = B * B - 4 * A * C;
        if ((A <= epsilon) || (det < 0)) {
            // No real solutions.
            return -1;
        } else if (det == 0) {
            // One solution.
            return -B / (2 * A);
        } else {
            // Two solutions.
            double t1 = ((-B + UPMath.sqrt(det)) / (2 * A));
            double t2 = ((-B - UPMath.sqrt(det)) / (2 * A));
            if (t1 >= 0 && t1 <= 1 && t2 >= 0 && t2 <= 1) {
                return UPMath.min(t1, t2);
            }
            if (t1 >= 0 && t1 <= 1) {
                return t1;
            }
            if (t2 >= 0 && t2 <= 1) {
                return t2;
            }
            return -1;
        }
    }

    public static boolean getRayCircleCollision(Vector rayStart, Vector rayEnd,
                                                Vector circleCenter, double radius) {
        if (rayEnd.distance2(circleCenter) <= radius * radius) {
            return true;
        }
        double t = getRayCircleCollisionT(rayStart, rayEnd, circleCenter, radius);
        return (t >= 0 && t <= 1);
    }

    public static Vector getRayCircleCollisionPoint(Vector rayStart, Vector rayEnd,
                                                    Vector circleCenter, double radius) {
        double t = getRayCircleCollisionT(rayStart, rayEnd, circleCenter, radius);
        if (t >= 0 && t <= 1) {
            Vector delta = rayEnd.sub(rayStart);
            return rayStart.add(delta.mul(t));
        }
        return null;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public static boolean getRayPointCollision(Vector rayStart, Vector rayEnd, Vector point) {
        if (rayStart.equals(point)) {
            return true;
        }
        if (rayEnd.equals(point)) {
            return true;
        }
        Vector a = rayStart;
        Vector b = rayEnd;
        Vector c = point;


        double crossproduct = (c.getY() - a.getY()) * (b.getX() - a.getX()) - (c.getX() - a.getX()) * (b.getY() - a.getY());
        if (UPMath.abs(crossproduct) > epsilon) {
            return false;
        }

        double dotproduct = (c.getX() - a.getX()) * (b.getX() - a.getX()) + (c.getY() - a.getY()) * (b.getY() - a.getY());
        if (dotproduct < 0) {
            return false;
        }

        double squaredlengthba = (b.getX() - a.getX()) * (b.getX() - a.getX()) + (b.getY() - a.getY()) * (b.getY() - a.getY());
        return dotproduct <= squaredlengthba;

    }

    public static boolean getRayPointCollision(double startX, double startY, double endX, double endY, double pointX, double pointY) {
        return getRayPointCollision(new Vector(startX, startY), new Vector(endX, endY), new Vector(pointX, pointY));
    }

    private static double crossAbs(Vector p1, Vector p2) {
        return p1.getX() * p2.getY() - p1.getY() * p2.getX();
    }

    @SuppressWarnings("UnusedParameters")
    private static boolean collidePreCheck(UPVector location1, SpriteType sprityType1, Marker marker1, UPVector location2, SpriteType sprityType2, Marker marker2) {
        double radiusSq1 = getScaledRadiusSq(sprityType1);
        double radiusSq2 = getScaledRadiusSq(sprityType2);
        //Should only return false if the sprites are far from oneanother
        return location1.distance2(location2) <= 2 * (radiusSq1 + radiusSq2);
    }

    private static double getScaledRadiusSq(SpriteType spriteType) {
        double widthHalf = spriteType.getWidth() / 2.;
        double heightHalf = spriteType.getHeight() / 2.;
        return UPMath.distance2(0, 0, widthHalf, heightHalf);
    }
}
