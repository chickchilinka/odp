package com.universeprojects.html5engine.shared.abstractFramework;

import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.client.framework.H5EResourceType;

/**
 * @author Crokoking
 */
public abstract class AbstractResourceManager implements ResourceManager {

    /**
     * Keeps track of the resource library file version we are using. This field is updated when a new library is loaded
     * in. This allows for understanding of how to handle older file types.
     */
    protected String fileVersion = "1";

    @Override
    public void addAudioFiles(String[] audioList) {
        for (String src : audioList) {
            addAudio(src, src);
        }
    }

    @Override
    public void addImages(String[] imageList) {
        for (String src : imageList) {
            addImage(src, src);
        }
    }

    @Override
    public void insertSpriteType(String imageKey, String spriteTypeKey) {
        SpriteType spr = newSpriteType(imageKey, spriteTypeKey);
        Image img = getImage(imageKey);
        if (img == null) {
            throw new RuntimeException("Tried to insert spriteType " + spriteTypeKey + " while the image " + imageKey + " was not loaded");
        }
        spr.setAreaWidth(img.getWidth());
        spr.setAreaHeight(img.getHeight());

        addSpriteType(spriteTypeKey, spr);
    }


    protected abstract MarkerCircle newMarkerCircle();

    protected abstract MarkerRectangle newMarkerRectangle();

    protected abstract MarkerVector newMarkerVector();

    /**
     * Loads all resources from the given file. The file itself is to be in the format created by the resource manager
     * when it saves.
     *
     * @param fileData The textual file containing all data necessary for the resource manager to load all resources in
     */
    public void readLibraryFile(String fileData) {
        readLibraryFile(fileData, null, null);
    }

    public void readLibraryFile(String fileData, String keyToReplace, String replaceValue) {

        String[] lines = fileData.split("\r?\n");

        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if (Strings.isEmpty(line)) {
                continue; // skip empty lines
            }

            // Determine the resource type
            if (!line.matches(".*:")) {
                throw new RuntimeException("Expected to find H5E resource type definition on line " + i+". Found "+line);
            }
            String resourceTypeStr = line.substring(0, line.length() - 1);
            H5EResourceType resourceType = H5EResourceType.fromString(resourceTypeStr);
            if (resourceType == null) {
                throw new RuntimeException("Unrecognized H5E resource type: " + resourceTypeStr);
            }
            i++;

            // Read the data...
            switch (resourceType) {
                case FILE_VERSION:
                    fileVersion = lines[i];
                    break;

                case IMAGE:
                    String imageKey = withOverride(lines[i], keyToReplace, replaceValue);
                    i++;
                    String imageSrc = withOverride(lines[i], keyToReplace, replaceValue);
                    addImage(imageKey, imageSrc);
                    break;

                case AUDIO:
                    String audioKey = withOverride(lines[i], keyToReplace, replaceValue);
                    i++;
                    String audioSrc = lines[i];
                    addAudio(audioKey, audioSrc);
                    break;

                case SPRITE_TYPE:
                    String spriteTypeKey = withOverride(lines[i], keyToReplace, replaceValue);
                    i++;
                    imageKey = withOverride(lines[i], keyToReplace, replaceValue);
                    i++;

                    String areaX = lines[i];
                    i++;
                    String areaY = lines[i];
                    i++;
                    String areaWidth = lines[i];
                    i++;
                    String areaHeight = lines[i];
                    //i++;
                    BaseSpriteType spriteType = newSpriteType(imageKey, spriteTypeKey);
                    spriteType.areaX = Integer.parseInt(areaX);
                    spriteType.areaY = Integer.parseInt(areaY);
                    spriteType.areaWidth = Integer.parseInt(areaWidth);
                    spriteType.areaHeight = Integer.parseInt(areaHeight);
                    addSpriteType(spriteTypeKey, spriteType);
                    break;

                case ANIMATED_SPRITE_TYPE:
                    String animSpriteTypeKey = withOverride(lines[i], keyToReplace, replaceValue);
                    i++;
                    Integer frameCount = Integer.valueOf(lines[i]);

                    AnimatedSpriteType animSpriteType = newAnimatedSpriteType(animSpriteTypeKey);
                    addAnimatedSpriteType(animSpriteTypeKey, animSpriteType);

                    for (int j = 0; j < frameCount; j++) {
                        i++;
                        String spriteTypeFrameKey = withOverride(lines[i], keyToReplace, replaceValue);
                        i++;
                        Integer duration = Integer.valueOf(lines[i]);
                        i++;
                        Integer adjustedX = Integer.valueOf(lines[i]);
                        i++;
                        Integer adjustedY = Integer.valueOf(lines[i]);
                        i++;
                        Float adjustedScale = Float.valueOf(lines[i]);

                        // Now create the frame...
                        AnimatedSpriteTypeFrame frame = animSpriteType.newFrame(spriteTypeFrameKey, duration);
                        frame.adjustedScale = adjustedScale;
                        frame.adjustedX = adjustedX;
                        frame.adjustedY = adjustedY;
                    }
                    break;

                case SPRITE_TYPE_MARKER:
                    String markerKey = lines[i];
                    i++;
                    String markerType = lines[i];
                    i++;
                    String markerSprite = withOverride(lines[i], keyToReplace, replaceValue);
                    i++;
                    String markerClass = lines[i];
                    i++;
                    if (markerClass.matches(MarkerCircle.markerClassString)) {
                        MarkerCircle mc = newMarkerCircle();
                        Integer x = Integer.valueOf(lines[i]);
                        i++;
                        Integer y = Integer.valueOf(lines[i]);
                        i++;
                        Integer radius = Integer.valueOf(lines[i]);
                        mc.label = markerKey;
                        mc.setTypeLabel(markerType);
                        spriteType = getNonAnimatedSpriteType(markerSprite);
                        mc.x = x;
                        mc.y = y;
                        mc.radius = radius;
                        spriteType.addMarker(mc);
                    } else if (markerClass.matches(MarkerRectangle.markerClassString)) {
                        MarkerRectangle mr = newMarkerRectangle();
                        Integer x = Integer.valueOf(lines[i]);
                        i++;
                        Integer y = Integer.valueOf(lines[i]);
                        i++;
                        Integer width = Integer.valueOf(lines[i]);
                        i++;
                        Integer height = Integer.valueOf(lines[i]);
                        i++;
                        Integer rotation = Integer.valueOf(lines[i]);
                        mr.label = markerKey;
                        mr.setTypeLabel(markerType);
                        spriteType = getNonAnimatedSpriteType(markerSprite);
                        mr.x = x;
                        mr.y = y;
                        mr.width = width;
                        mr.height = height;
                        mr.rotation = rotation;
                        spriteType.addMarker(mr);
                    } else if (markerClass.matches(MarkerVector.markerClassString)) {
                        MarkerVector mv = newMarkerVector();
                        Integer x = Integer.valueOf(lines[i]);
                        i++;
                        Integer y = Integer.valueOf(lines[i]);
                        i++;
                        Integer destX = Integer.valueOf(lines[i]);
                        i++;
                        Integer destY = Integer.valueOf(lines[i]);
                        mv.label = markerKey;
                        mv.setTypeLabel(markerType);
                        spriteType = getNonAnimatedSpriteType(markerSprite);
                        mv.x = x;
                        mv.y = y;
                        mv.deltaX = destX;
                        mv.deltaY = destY;
                        spriteType.addMarker(mv);
                    } else {
                        throw new IllegalStateException("Marker type not recognized: " + markerClass);
                    }
                    break;

                default:
                    throw new RuntimeException("Unhandled H5E resource type: " + resourceType.toString());
            }
        }
    }

    private String withOverride(String line, String keyToReplace, String replaceValue) {
        if(keyToReplace == null) {
            return line;
        }
        if(line.equals(keyToReplace)) {
            return replaceValue;
        } else {
            return line;
        }
    }

    @Override
    public void removeAllResources() {
        removeAllImages();
        removeAllAudio();
        removeAllSpriteTypes();
    }

    /**
     * This method will generate a String that can be later saved as a h5l file (Html5Engine Library file).
     *
     * @return The contents of the h5l file.
     */
    public String writeLibraryFile() {
        StringBuilder file = new StringBuilder();
        // Save filetype version...
        file.append("file version:" + '\n');
        file.append("1" + '\n');
        // Save images...
        for (String key : getImageKeys()) {
            Image img = getImage(key);
            String url = img.url;
            file.append("image:" + '\n');
            file.append(key).append('\n');
            file.append(url).append('\n');
        }
        // Save audio...
        for (String key : getAudioKeys()) {
            Audio aud = getAudio(key);
            String url = aud.getURL();
            file.append("audio:" + '\n');
            file.append(key).append('\n');
            file.append(url).append('\n');
        }
        // Save SpriteTypes...
        for (String key : getSpriteTypeKeys()) {
            BaseSpriteType sprite = getNonAnimatedSpriteType(key);
            file.append("H5ESpriteType:\n");

            // Save the sprite type key as it is in the resource manager
            file.append(key).append("\n");

            // Save the sprite type's IMAGE key
            file.append(sprite.getImageKey()).append("\n");

            // Save the area this sprite type uses on the image
            file.append(sprite.areaX).append("\n");
            file.append(sprite.areaY).append("\n");
            file.append(sprite.getAreaWidth()).append("\n");
            file.append(sprite.areaHeight).append("\n");
        }
        // Save AnimatedSpriteTypes...
        for (String key : getAnimatedSpriteTypeKeys()) {
            AnimatedSpriteType anim = getAnimatedSpriteType(key);
            file.append("H5EAnimatedSpriteType:\n");

            // Save the sprite type key as it is in the resource manager
            file.append(key).append("\n");

            // Save the frame count
            file.append(anim.getFrameNumber()).append("\n");

            // Save each frame...
            for (int j = 0; j < anim.getFrameNumber(); j++) {
                file.append(anim.getFrame(j).getSpriteTypeKey()).append("\n");
                file.append(anim.getFrame(j).duration).append("\n");
                file.append(anim.getFrame(j).adjustedX).append("\n");
                file.append(anim.getFrame(j).adjustedY).append("\n");
                file.append(anim.getFrame(j).adjustedScale).append("\n");
            }
        }
        // Save Markers
        for (String spriteKey : getSpriteTypeKeys()) {
            SpriteType sprite = getNonAnimatedSpriteType(spriteKey);
            for (Marker marker : sprite.getMarkers()) {
                file.append("H5ESpriteTypeMarker:\n");

                // Save the marker type key as it is in the resource manager
                file.append(marker.label).append("\n");

                // Save the marker type label
                file.append(marker.getTypeLabel()).append("\n");

                // Save the marker sprite label
                file.append(spriteKey).append("\n");

                // Save the marker-class
                file.append(marker.getMarkerClassString()).append("\n");

                // Save the class-data
                file.append(marker.x).append("\n");
                file.append(marker.y).append("\n");
                if (marker instanceof MarkerCircle) {
                    MarkerCircle mc = (MarkerCircle) marker;
                    file.append(mc.radius).append("\n");
                } else if (marker instanceof MarkerRectangle) {
                    MarkerRectangle mr = (MarkerRectangle) marker;
                    file.append(mr.width).append("\n");
                    file.append(mr.height).append("\n");
                    file.append(mr.rotation).append("\n");
                } else if (marker instanceof MarkerVector) {
                    MarkerVector mv = (MarkerVector) marker;
                    file.append(mv.deltaX).append("\n");
                    file.append(mv.deltaY).append("\n");
                } else {
                    throw new IllegalStateException("Could not write Marker-Class: " + marker.getClass().getName());
                }
            }
        }
        return file.toString();
    }

}
