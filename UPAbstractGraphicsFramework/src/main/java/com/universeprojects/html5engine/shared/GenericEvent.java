package com.universeprojects.html5engine.shared;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unused")
public abstract class GenericEvent {
    protected final Set<GenericEventHandler> handlers = new LinkedHashSet<>();

    private List<Change> changes;

    private boolean useChangeList = false;

    /**
     * Makes any handler-change use the change-list instead
     */
    synchronized void begin() {
        useChangeList = true;
    }

    /**
     * Applies the change-list
     */
    synchronized void end() {
        useChangeList = false;
        if(changes != null) {
            for (Change change : changes) {
                if(change.remove) {
                    handlers.remove(change.handler);
                } else {
                    handlers.add(change.handler);
                }
            }
            changes.clear();
        }
    }

    synchronized void add(GenericEventHandler handler) {
        if(useChangeList) {
            getOrCreateChanges().add(new Change(handler, false));
        } else {
            handlers.add(handler);
        }
    }

    synchronized void remove(GenericEventHandler handler) {
        if(useChangeList) {
            getOrCreateChanges().add(new Change(handler, true));
        } else {
            handlers.remove(handler);
        }
    }

    private synchronized List<Change> getOrCreateChanges() {
        if(changes == null) {
            changes = new ArrayList<>();
        }
        return changes;
    }


    private class Change {
        boolean remove;
        GenericEventHandler handler;

        public Change(GenericEventHandler handler, boolean remove) {
            this.handler = handler;
            this.remove = remove;
        }
    }

    public static class GenericEvent0Args extends GenericEvent {

        public void registerHandler(GenericEventHandler0Args handler) {
            add(handler);
        }

        public void removeHandler(GenericEventHandler0Args onRenderHandler) {
            remove(onRenderHandler);
        }

        public synchronized void fire() {
            begin();
            try {
                for (GenericEventHandler handler : UPUtils.fastIterable(handlers)) {
                    if (handler != null) {
                        ((GenericEventHandler0Args) handler).handleEvent();
                    }
                }
            } finally {
                end();
            }
        }
    }

    public static class GenericEvent1Args<T1> extends GenericEvent {

        public void registerHandler(GenericEventHandler1Args<T1> handler) {
            add(handler);
        }

        public void removeHandler(GenericEventHandler1Args<T1> onRenderHandler) {
            remove(onRenderHandler);
        }

        @SuppressWarnings("unchecked")
        public synchronized void fire(T1 arg1) {
            begin();
            try {
                for (GenericEventHandler handler : handlers) {
                    ((GenericEventHandler1Args<T1>) handler).handleEvent(arg1);
                }
            } finally {
                end();
            }
        }
    }

    public static class GenericEvent2Args<T1, T2> extends GenericEvent {

        public void registerHandler(GenericEventHandler2Args<T1, T2> handler) {
            add(handler);
        }

        public void removeHandler(GenericEventHandler2Args<T1, T2> onRenderHandler) {
            remove(onRenderHandler);
        }

        @SuppressWarnings("unchecked")
        public synchronized void fire(T1 arg1, T2 arg2) {
            begin();
            try {
                for (GenericEventHandler handler : UPUtils.fastIterable(handlers)) {
                    ((GenericEventHandler2Args<T1, T2>) handler).handleEvent(arg1, arg2);
                }
            } finally {
                end();
            }
        }
    }

    public static class GenericEvent3Args<T1, T2, T3> extends GenericEvent {

        public void registerHandler(GenericEventHandler3Args<T1, T2, T3> handler) {
            add(handler);
        }

        public void removeHandler(GenericEventHandler3Args<T1, T2, T3> onRenderHandler) {
            remove(onRenderHandler);
        }

        @SuppressWarnings("unchecked")
        public synchronized void fire(T1 arg1, T2 arg2, T3 arg3) {
            begin();
            try {
                for (GenericEventHandler handler : UPUtils.fastIterable(handlers)) {
                    ((GenericEventHandler3Args<T1, T2, T3>) handler).handleEvent(arg1, arg2, arg3);
                }
            } finally {
                end();
            }
        }
    }

    public static class GenericEvent4Args<T1, T2, T3, T4> extends GenericEvent {

        public void registerHandler(GenericEventHandler4Args<T1, T2, T3, T4> handler) {
            add(handler);
        }

        public void removeHandler(GenericEventHandler4Args<T1, T2, T3, T4> onRenderHandler) {
            remove(onRenderHandler);
        }

        @SuppressWarnings("unchecked")
        public synchronized void fire(T1 arg1, T2 arg2, T3 arg3, T4 arg4) {
            begin();
            try {
                for (GenericEventHandler handler : UPUtils.fastIterable(handlers)) {
                    ((GenericEventHandler4Args<T1, T2, T3, T4>) handler).handleEvent(arg1, arg2, arg3, arg4);
                }
            } finally {
                end();
            }
        }
    }

    private interface GenericEventHandler {
    }

    public interface GenericEventHandler0Args extends GenericEventHandler {
        void handleEvent();
    }

    public interface GenericEventHandler1Args<T1> extends GenericEventHandler {
        void handleEvent(T1 arg1);
    }

    public interface GenericEventHandler2Args<T1, T2> extends GenericEventHandler {
        void handleEvent(T1 arg1, T2 arg2);
    }

    public interface GenericEventHandler3Args<T1, T2, T3> extends GenericEventHandler {
        void handleEvent(T1 arg1, T2 arg2, T3 arg3);
    }

    public interface GenericEventHandler4Args<T1, T2, T3, T4> extends GenericEventHandler {
        void handleEvent(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    }
}
