package com.universeprojects.html5engine.shared;

import com.universeprojects.common.shared.math.UPMath;

import java.util.Arrays;

import static com.universeprojects.html5engine.shared.Matrix33.I11;
import static com.universeprojects.html5engine.shared.Matrix33.I12;
import static com.universeprojects.html5engine.shared.Matrix33.I13;
import static com.universeprojects.html5engine.shared.Matrix33.I21;
import static com.universeprojects.html5engine.shared.Matrix33.I22;
import static com.universeprojects.html5engine.shared.Matrix33.I23;
import static com.universeprojects.html5engine.shared.Matrix33.I31;
import static com.universeprojects.html5engine.shared.Matrix33.I32;
import static com.universeprojects.html5engine.shared.Matrix33.I33;
import static com.universeprojects.html5engine.shared.Matrix33.invert;
import static com.universeprojects.html5engine.shared.Matrix33.matmul;
import static com.universeprojects.html5engine.shared.Matrix33.setMatrix;

@SuppressWarnings("unused")
@Deprecated
public class Transformation {

    /**
     * The forward transformation matrix
     * (a square 3x3 matrix represented using a 9-element array, to comply with Matrix33)
     */
    private final double[] mat = new double[]{
        1, 0, 0,
        0, 1, 0,
        0, 0, 1,
    };

    /**
     * The inverse transformation matrix (cached snapshot)
     * (a square 3x3 matrix represented using a 9-element array, to comply with Matrix33)
     */
    private final double[] inv = new double[]{
        1, 0, 0,
        0, 1, 0,
        0, 0, 1,
    };

    /**
     * When this flag is TRUE, it indicates that the cached inverse matrix needs to be re-calculated
     */
    private boolean inverseInvalid = false;

    /**
     * A mapping from the 3x3 representation to 'dx' measure for the 2x2 representation
     */
    public static final int IDX = I31;
    /**
     * A mapping from the 3x3 representation to 'dy' measure for the 2x2 representation
     */
    public static final int IDY = I32;

    /**
     * Constructs a "clean-slate" transformation object
     */
    public Transformation() {

    }

    /**
     * Constructs a transformation objects and initializes it with the given parameters
     */
    public Transformation(double x, double y, double originX, double originY, double rotation, double scaleX, double scaleY) {
        initialize(x, y, originX, originY, rotation, scaleX, scaleY);
    }

    /**
     * Initializes the transformation object with the given parameters
     */
    public void initialize(double x, double y, double originX, double originY, double rotation, double scaleX, double scaleY) {
        toUnit();
        translate(x, y);
        rotateDeg(rotation);
        scale(scaleX, scaleY);
        translate(-originX, -originY);
    }

    /**
     * Updates the cached inverse (if an update is required)
     */
    private void updateCachedInverse() {
        if (!inverseInvalid) {
            return;
        }
        invert(mat, inv);
    }

    /**
     * Sets all values of this transformation object, from another transformation object
     */
    public void set(Transformation t) {
        System.arraycopy(t.mat, 0, mat, 0, 9);
        inverseInvalid = t.inverseInvalid;
        if (!t.inverseInvalid) {
            System.arraycopy(t.inv, 0, inv, 0, 9);
        }
    }

    /**
     * Re-sets this transformation object to its clean state (unit matrix)
     */
    public void toUnit() {
        setMatrix(
            1, 0, 0,
            0, 1, 0,
            0, 0, 1,
            mat);

        setMatrix(
            1, 0, 0,
            0, 1, 0,
            0, 0, 1,
            inv);

        inverseInvalid = false;
    }

    /**
     * Returns a new transformation object, an exact copy of this transformation
     */
    public Transformation copy() {
        Transformation t = new Transformation();
        t.set(this);
        return t;
    }

    /**
     * Returns a new transformation object, representing the inverse of this transformation
     */
    public Transformation inverse() {
        updateCachedInverse();
        Transformation t = new Transformation();
        System.arraycopy(inv, 0, t.mat, 0, 9);
        System.arraycopy(mat, 0, t.inv, 0, 9);
        return t;
    }

    /**
     * Multiplies this transformation in-place with another transformation
     *
     * @return this transformation (modified)
     */
    public Transformation multiply(Transformation tr) {
        if (tr == null) return this;
        return multiply(tr.mat);
    }

    /**
     * Multiplies this transformation in-place with a matrix
     *
     * @return this transformation (modified)
     */
    public Transformation multiply(double[] m) {
        if (m == null) return this;
        matmul(mat, m, mat);

        inverseInvalid = true;
        return this;
    }

    /**
     * Multiplies this transformation in-place with a matrix represented by the given values
     *
     * @return this transformation (modified)
     */
    public Transformation multiply(
        double m11, double m21, double m31,
        double m12, double m22, double m32,
        double m13, double m23, double m33) {

        matmul(
            mat,
            m11, m21, m31,
            m12, m22, m32,
            m13, m23, m33,
            mat);

        inverseInvalid = true;
        return this;
    }

    /**
     * Reverse-multiplies this transformation in-place with another transformation
     *
     * @return this transformation (modified)
     */
    public Transformation multiplyReverse(Transformation tr) {
        if (tr == null) return this;
        return multiplyReverse(tr.mat);
    }

    /**
     * Reverse-multiplies this transformation in-place with a matrix
     *
     * @return this transformation (modified)
     */
    public Transformation multiplyReverse(double[] m) {
        if (m == null) return this;
        matmul(m, mat, mat);

        inverseInvalid = true;
        return this;
    }

    /**
     * Reverse-multiplies this transformation in-place with a matrix represented by the given values
     *
     * @return this transformation (modified)
     */
    public Transformation multiplyReverse(
        double m11, double m21, double m31,
        double m12, double m22, double m32,
        double m13, double m23, double m33) {

        matmul(m11, m21, m31,
            m12, m22, m32,
            m13, m23, m33,
            mat, mat);

        inverseInvalid = true;
        return this;
    }

    public Transformation multiplyInverse(Transformation t) {
        if (t == null) return this;
        return multiply(t.inv);
    }

    /**
     * Translates this transformation along the x-axis and/or y-axis
     *
     * @param x translation along the x-axis (0 for no translation)
     * @param y translation along the y-axis (0 for no translation)
     * @return this transformation (modified)
     */
    public Transformation translate(double x, double y) {
        if (x != 0 || y != 0) {
            multiply(
                1, 0, x,
                0, 1, y,
                0, 0, 1
            );
        }
        return this;
    }

    /**
     * Reverse-translates this transformation along the x-axis and/or y-axis
     *
     * @param x translation along the x-axis (0 for no translation)
     * @param y translation along the y-axis (0 for no translation)
     * @return this transformation (modified)
     */
    public Transformation translateReverse(double x, double y) {
        if (x != 0 || y != 0) {
            multiplyReverse(
                1, 0, x,
                0, 1, y,
                0, 0, 1);
        }
        return this;
    }

    /**
     * Scales this transformation by the same factor on both axis
     *
     * @param a the scale factor affecting the x-axis and y-axis
     * @return this transformation (modified)
     */
    public Transformation scale(double a) {
        return scale(a, a);
    }

    /**
     * Scales this transformation by (potentially) different factors on the axis
     *
     * @param sx the scale factor affecting the x-axis
     * @param sy the scale factor affecting the y-axis
     * @return this transformation (modified)
     */
    public Transformation scale(double sx, double sy) {
        if (sx != 1 || sy != 1) {
            multiply(
                sx, 0, 0,
                0, sy, 0,
                0, 0, 1);
        }
        return this;
    }

    /**
     * Rotates this transformation by a specified value
     *
     * @param rad the rotation value in radians
     * @return this transformation (modified)
     */
    public Transformation rotate(double rad) {
        if (rad != 0) {
            double sin = UPMath.sin(rad);
            double cos = UPMath.cos(rad);
            multiply(
                cos, -sin, 0,
                sin, cos, 0,
                0, 0, 1);
        }
        return this;
    }

    /**
     * Reverse-rotates this transformation by a specified value
     *
     * @param rad the rotation value in radians
     * @return this transformation (modified)
     */
    public Transformation rotateReverse(double rad) {
        if (rad != 0) {
            double sin = UPMath.sin(rad);
            double cos = UPMath.cos(rad);
            multiplyReverse(
                cos, -sin, 0,
                sin, cos, 0,
                0, 0, 1);
        }
        return this;
    }

    /**
     * Rotates this transformation by a specified value
     *
     * @param deg the rotation value in degrees
     * @return this transformation (modified)
     */
    public Transformation rotateDeg(double deg) {
        if (deg % 360 != 0) {
            double sin = UPMath.sinDeg(deg);
            double cos = UPMath.cosDeg(deg);
            multiply(
                cos, -sin, 0,
                sin, cos, 0,
                0, 0, 1);
        }
        return this;
    }

    /**
     * Reverse-rotates this transformation by a specified value
     *
     * @param deg the rotation value in degrees
     * @return this transformation (modified)
     */
    public Transformation rotateDegReverse(double deg) {
        if (deg % 360 != 0) {
            double sin = UPMath.sinDeg(deg);
            double cos = UPMath.cosDeg(deg);
            multiplyReverse(
                cos, -sin, 0,
                sin, cos, 0,
                0, 0, 1);
        }
        return this;
    }


    /**
     * Apply this transformation completely to the specified vector
     * A new vector is created and the old vector is not being modified
     *
     * @param src the vector to be transformed
     * @return the new, transformed vector
     */
    public Vector transform(Vector src) {
        return new Vector(applyX(mat, src.x, src.y), applyY(mat, src.x, src.y));
    }

    /**
     * Apply the inverse of this transformation completely to the specified vector
     * A new vector is created and the old vector is not being modified
     *
     * @param src the vector to be transformed
     * @return the new, transformed vector
     */
    public Vector transformInverse(Vector src) {
        updateCachedInverse();
        return new Vector(applyX(inv, src.x, src.y), applyY(inv, src.x, src.y));
    }

    /**
     * Apply this transformation to the specified vector while ignoring translations.
     * This can be used to transform ie sizes of objects.
     * A new vector is created and the old vector is not being modified
     *
     * @param src the vector to be transformed
     * @return the new, transformed vector
     */
    public Vector transformDelta(Vector src) {
        return new Vector(applyDeltaX(mat, src.x, src.y), applyDeltaY(mat, src.x, src.y));
    }

    /**
     * Apply the inverse of this transformation to the specified vector while ignoring translations.
     * This can be used to transform ie sizes of objects.
     * A new vector is created and the old vector is not being modified
     *
     * @param src the vector to be transformed
     * @return the new, transformed vector
     */
    public Vector transformInverseDelta(Vector src) {
        updateCachedInverse();
        return new Vector(applyDeltaX(inv, src.x, src.y), applyDeltaY(inv, src.x, src.y));
    }


    /**
     * Apply this transformation completely to the specified vector
     * The given vector is transformed and returned for convenience
     *
     * @param src the vector to be transformed
     * @return the vector from the parameter
     */
    public Vector transformLocal(Vector src) {
        double x = src.x;
        double y = src.y;
        src.x = applyX(mat, x, y);
        src.y = applyY(mat, x, y);
        return src;
    }

    /**
     * Apply the inverse of this transformation completely to the specified vector
     * The given vector is transformed and returned for convenience
     *
     * @param src the vector to be transformed
     * @return the vector from the parameter
     */
    public Vector transformInverseLocal(Vector src) {
        updateCachedInverse();
        double x = src.x;
        double y = src.y;
        src.x = applyX(inv, x, y);
        src.y = applyY(inv, x, y);
        return src;
    }

    /**
     * Apply this transformation to the specified vector while ignoring translations.
     * This can be used to transform ie sizes of objects.
     * The given vector is transformed and returned for convenience
     *
     * @param src the vector to be transformed
     * @return the vector from the parameter
     */
    public Vector transformDeltaLocal(Vector src) {
        double x = src.x;
        double y = src.y;
        src.x = applyDeltaX(mat, x, y);
        src.y = applyDeltaY(mat, x, y);
        return src;
    }


    public double transformDeltaScale(double coord) {
        return mat[I11] * coord;
    }

    /**
     * Apply the inverse of this transformation to the specified vector while ignoring translations.
     * This can be used to transform ie sizes of objects.
     * The given vector is transformed and returned for convenience
     *
     * @param src the vector to be transformed
     * @return the vector from the parameter
     */
    public Vector transformInverseDeltaLocal(Vector src) {
        updateCachedInverse();
        double x = src.x;
        double y = src.y;
        src.x = applyDeltaX(inv, x, y);
        src.y = applyDeltaY(inv, x, y);
        return src;
    }


    /**
     * Calculates the x-coordinate of the vector resulting from applying the matrix mat to the vector (vecX,vecY)
     */
    private static double applyX(double[] mat, double vecX, double vecY) {
        return mat[I11] * vecX + mat[I21] * vecY + mat[I31] * 1;
    }

    /**
     * Calculates the y-coordinate of the vector resulting from applying the matrix mat to the vector (vecX,vecY)
     */
    private static double applyY(double[] mat, double vecX, double vecY) {
        return mat[I12] * vecX + mat[I22] * vecY + mat[I32] * 1;
    }

    /**
     * Calculates the x-coordinate of the vector resulting from applying the matrix mat to the vector (vecX,vecY) while ignoring translations.
     */
    private static double applyDeltaX(double[] mat, double vecX, double vecY) {
        return mat[I11] * vecX + mat[I21] * vecY;
    }

    /**
     * Calculates the y-coordinate of the vector resulting from applying the matrix mat to the vector (vecX,vecY) while ignoring translations.
     */
    private static double applyDeltaY(double[] mat, double vecX, double vecY) {
        return mat[I12] * vecX + mat[I22] * vecY;
    }


    @Override
    public String toString() {
        return "Transformation: {\n" +
            mat[I11] + "," + mat[I21] + "," + mat[I31] + ",\n" +
            mat[I12] + "," + mat[I22] + "," + mat[I32] + ",\n" +
            mat[I13] + "," + mat[I23] + "," + mat[I33] + "}";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(mat);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Transformation other = (Transformation) obj;
        return Arrays.equals(mat, other.mat);
    }


}
