package com.universeprojects.html5engine.shared.abstractFramework;

public interface LevelAwareGraphicsElement extends GraphicElement{

    /**
     * Only called in {@link com.universeprojects.html5engine.client.framework.H5ELayer}
     */
    void setLevel(Integer level);

    /**
     *  Meaningful usage:
     *  - from H5EGraphicsElement (effectively H5EAnimatedSprite) by H5EParticleEmitterParticleEmitterBehaviour.createParticle()
     *  - from H5ESprite by SpriteGraphicComponent.update()
     *  - from H5ESprite by ShipThrusterGraphicComponent.addThruster()
     */
    int getLevel();
}
