package com.universeprojects.html5engine.client.framework;

import com.universeprojects.html5engine.shared.abstractFramework.AnimatedSpriteTypeFrame;

public class H5EAnimatedSpriteTypeFrame extends AnimatedSpriteTypeFrame {

    /**
     * The sprite type that is being used for the graphics for this frame.
     */
    private H5ESpriteType spriteType;
    /**
     * The sprite type key that is used to identify a sprite type object in the resource manager.
     */
    private String spriteTypeKey;

    public H5EAnimatedSpriteTypeFrame(H5EResourceManager resourceManager, String spriteTypeKey, Integer durationInMillis) {
        this.spriteTypeKey = spriteTypeKey;
        if (durationInMillis == null) {
            durationInMillis = 50;
        }
        this.duration = durationInMillis;


        spriteType = resourceManager.getNonAnimatedSpriteType(spriteTypeKey);
        if (spriteType == null) {
            throw new RuntimeException("The spriteTypeKey '" + spriteTypeKey + "' is not associated with any sprite type");
        }
    }

    public H5ESpriteType getSpriteType() {
        return spriteType;
    }

    @Override
    public String getSpriteTypeKey() {
        return spriteTypeKey;
    }

    public void setSpriteTypeKey(String spriteTypeKey) {
        this.spriteTypeKey = spriteTypeKey;
    }
}
