package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.StageDebugAccessor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.shared.abstractFramework.LevelAwareGraphicsElement;

public class H5ELayer extends Stage {

    /**
     * A reference to the engine so we can do work inside this class and can be self sufficient
     */
    protected final H5EEngine engine;

    protected final Group[] levels;
    protected final ShaderProgram[] levelShaders;

    private ShaderProgram layerShader;

    private final int numLevels;
    private final int defaultLevel;

    private boolean eventTransparent = false;
    private H5ELayerProxy layerProxy;

    final InputProcessor inputProcessor = new LayerInputDelegator(this);

    protected final String name;

    public H5ELayer(H5EEngine engine, String name, UPViewport viewport) {
        this(engine, name, 1, 0, viewport);
    }

    public H5ELayer(H5EEngine engine, String name, int numLevels, int defaultLevel, UPViewport viewport) {
        this(engine, name, numLevels, defaultLevel, viewport, true);
    }

    public H5ELayer(H5EEngine engine, String name, int numLevels, int defaultLevel, UPViewport viewport, boolean registerInputProcessor) {
        super(viewport == null ? new ScalingViewport(Scaling.stretch, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()) : viewport,
                viewport == null ? new SpriteBatch() : viewport.getSpriteBatch());
        this.engine = Dev.checkNotNull(engine);
        this.name = name;
        getRoot().setName("Layer " + name);
        getRoot().setTransform(false);

        if (numLevels < 1) {
            throw new IllegalArgumentException("Invalid number of levels (must be 1 or higher): " + numLevels);
        }

        if (defaultLevel < 0 || defaultLevel >= numLevels) {
            throw new IllegalArgumentException("Invalid default level (must be 0 to " + numLevels + "): " + defaultLevel);
        }

        this.numLevels = numLevels;
        this.defaultLevel = defaultLevel;
        this.levels = new Group[numLevels];
        this.levelShaders = new ShaderProgram[numLevels];

        for (int i = 0; i < numLevels; i++) {
            final Group level = createLevel(i);
            if (level == null)
                continue;
            level.setName(level.getClass().getSimpleName() + " Level " + i);
            level.setTransform(false);
            levels[i] = level;
            addActor(level);
        }

        if (registerInputProcessor) {
            engine.inputMultiplexer.addProcessor(0, inputProcessor);
        }

    }

    @Override
    @Deprecated //Please use addActorToTop
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void addActor(Actor actor) {
        super.addActor(actor);
    }

    public Group getLevel(int index) {
        return levels[index];
    }

    protected Group createLevel(int level) {
        return new Group() {
            @Override
            public void draw(Batch batch, float parentAlpha) {
                onBeforeLevelDraw(batch, level);
                try {
                    super.draw(batch, parentAlpha);
                } finally {
                    onAfterLevelDraw(batch, level);
                }
            }
        };
    }

    protected void onBeforeLevelDraw(Batch batch, int level) {
        final ShaderProgram levelShader = levelShaders[level];
        if(levelShader != null) {
            batch.setShader(levelShader);
        }
    }

    protected void onAfterLevelDraw(Batch batch, int level) {
        final ShaderProgram levelShader = levelShaders[level];
        if(levelShader != null) {
            batch.setShader(layerShader);
        }
    }

    @Override
    public void draw() {
        if (!getRoot().isVisible()) {
            return;
        }
        Batch batch = getBatch();
        if (layerShader != null) {
            batch.setShader(layerShader);
        }
        try {
            if (StageDebugAccessor.isStageDebugged()) {
                batch.end();
                try {
                    super.draw();
                } finally {
                    batch.begin();
                }
            } else {
                getRoot().draw(batch, 1);
            }
        } finally {
            if (layerShader != null) {
                batch.setShader(null);
            }
        }
    }

    @Override
    public void clear() {
        removeAllElements();
    }

    public void setLevelEventTransparent(int level, boolean transparent) {
        levels[level].setTouchable(transparent ? Touchable.disabled : Touchable.enabled);
    }

    public int translateLevelName(String name) {
        throw new IllegalStateException("Not implemented");
    }

    public String getName() {
        return name;
    }

    public int getDefaultLevel() {
        return defaultLevel;
    }

    public void setEventTransparent(boolean value) {
        this.eventTransparent = value;
    }

    public boolean isEventTransparent() {
        return eventTransparent;
    }

    protected boolean receivesInputs() {
        return !isEventTransparent() && isVisible();
    }

    /**
     * Shows the layer (sets the layer's visible property to TRUE)
     */
    public void show() {
        setVisible(true);
    }

    /**
     * Hides the layer (sets the layer's visible property to FALSE)
     */
    public void hide() {
        setVisible(false);
    }

    /**
     * Hides/Shows the layer
     *
     * @return TRUE    - if the layer is shown as a result of this operation
     * FALSE   - if the layer is hidden as a result of this operation
     */
    public boolean toggleVisibility() {
        if (isVisible()) {
            hide();
            return false;
        } else {
            show();
            return true;
        }
    }

    /**
     * Sets the layer's visible property. An invisible layer is simply not drawn.
     */
    public void setVisible(boolean newVisible) {
        if (newVisible == isVisible()) return;
        getRoot().setVisible(newVisible);
        invalidateBuffer();
    }

    /**
     * Gets the visible state of this layer.
     */
    public boolean isVisible() {
        return getRoot().isVisible();
    }

    private int resolveStageLevel(Integer customLevelIndex) {
        if (customLevelIndex == null) {
            return defaultLevel;
        }

        if (customLevelIndex < 0 || customLevelIndex > numLevels - 1) {
            throw new IllegalArgumentException("Invalid level: " + customLevelIndex);
        }
        return customLevelIndex;
    }

    public void removeAllElements() {
        for (Group level : levels) {
            level.clear();
        }
    }

    public H5EEngine getEngine() {
        return engine;
    }

    public void addActorToTop(Actor el) {
        internalAddActorToTop(el, defaultLevel);
    }

    public void addActorToTop(Actor el, Integer levelIndex) {
        int realLevel = resolveStageLevel(levelIndex);
        internalAddActorToTop(el, realLevel);
    }

    private void internalAddActorToTop(Actor el, int levelIndex) {
        if (getActors().contains(el, true)) {
            throw new IllegalStateException("The layer already contains this element");
        }
        final Group level = levels[levelIndex];
        if (el.getParent() == level) {
            return;
        }

        if (el instanceof LevelAwareGraphicsElement) {
            ((LevelAwareGraphicsElement) el).setLevel(levelIndex);
        }

        level.addActor(el);
        level.setVisible(true);
    }

    public void setLevelShader(int level, ShaderProgram shader) {
        levelShaders[level] = shader;
    }

    public void clearLevelShader(int level) {
        levelShaders[level] = null;
    }

    public ShaderProgram getLevelShader(int level) {
        return levelShaders[level];
    }

    public ShaderProgram getLayerShader() {
        return layerShader;
    }

    public void setLayerShader(ShaderProgram layerShader) {
        this.layerShader = layerShader;
    }

    public void invalidateBuffer() {
        if (this.layerProxy != null) {
            this.layerProxy.wipeCache();
        }
    }

    public static void invalidateBufferForActor(Actor actor) {
        Stage stage = actor.getStage();
        if (stage instanceof H5ELayer) {
            ((H5ELayer) stage).invalidateBuffer();
        }
    }

    public void setLayerProxy(H5ELayerProxy layerProxy) {
        this.layerProxy = layerProxy;
    }
}
