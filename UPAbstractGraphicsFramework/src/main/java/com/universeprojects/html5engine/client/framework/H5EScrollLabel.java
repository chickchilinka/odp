package com.universeprojects.html5engine.client.framework;

import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class H5EScrollLabel extends H5EScrollablePane {

    private String text;
    private final H5ELabel label;

    private float textWidth;

    private boolean autoScroll = false;

    private float speed = 1.0f;

    public H5EScrollLabel(String text, H5ELayer layer) {
        super(layer);
        this.text = text;

        setScrollingDisabled(false, true);
        setSmoothScrolling(false);
        getContent().left();

        label = add(new H5ELabel(text, layer)).left().getActor();
    }

    public H5ELabel getLabel() {
        return label;
    }

    public void setupScrolling() {
        label.setText(text);
        textWidth = label.getPrefWidth();
        if (textWidth > getWidth()) {
            label.setText(text+"            ");
            textWidth = label.getPrefWidth();
            label.setText(text+"            "+text);
        }
    }

    public boolean getAutoScroll() {
        return autoScroll;
    }

    public void setAutoScroll(boolean autoScroll) {
        if (this.autoScroll == autoScroll) {
            return;
        }

        this.autoScroll = autoScroll;
        H5ELayer.invalidateBufferForActor(this);
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (autoScroll) {
            setSmoothScrolling(false);
            float pos = getScrollPercentX();
            float loopPos = textWidth / (label.getPrefWidth() - getWidth());
            if (pos > loopPos) pos -= loopPos;
            pos += delta * speed * 0.1f;
            setScrollPercentX(pos);
            H5ELayer.invalidateBufferForActor(this);
        } else {
            setSmoothScrolling(true);
            setScrollPercentX(0);
        }
    }

    public void setText(String text) {
        this.text = text;
        label.setText(text);
    }

    public String getText() {
        return text;
    }
}
