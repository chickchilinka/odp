package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.InputProcessor;

public class LayerInputDelegator implements InputProcessor {
    private final H5ELayer layer;

    public LayerInputDelegator(H5ELayer layer) {
        this.layer = layer;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return layer.receivesInputs() && layer.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return layer.receivesInputs() && layer.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return layer.receivesInputs() && layer.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return layer.receivesInputs() && layer.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return layer.receivesInputs() && layer.scrolled(amountX, amountY);
    }

    @Override
    public boolean keyDown(int keyCode) {
        return layer.receivesInputs() && layer.keyDown(keyCode);
    }

    @Override
    public boolean keyUp(int keyCode) {
        return layer.receivesInputs() && layer.keyUp(keyCode);
    }

    @Override
    public boolean keyTyped(char character) {
        return layer.receivesInputs() && layer.keyTyped(character);
    }
}
