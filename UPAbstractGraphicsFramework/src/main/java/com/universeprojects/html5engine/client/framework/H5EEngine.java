package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.ReturningCallable0Args;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Check;
import com.universeprojects.common.shared.util.GWTDebug;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.client.framework.proxy.UILayerProxy;
import com.universeprojects.html5engine.shared.GenericEvent;
import com.universeprojects.html5engine.shared.UPUtils;
import com.universeprojects.html5engine.shared.abstractFramework.ResourceManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@SuppressWarnings("unused")
public class H5EEngine {
    private final static float EMA_COEFFICIENT = 2F / (60F + 1F);
    private static final float MIN_ZOOM = 0.5f;

    private static boolean DisplayDoubleSized = false;

    private final Logger log = Logger.getLogger(H5EEngine.class);

    // Major objects...
    /**
     * The resource manager for this engine. Accessible via the getResourceManager() method.
     */
    private ResourceManager resourceManager;

    private boolean behavioursAreProcessing = false;
    private final Set<H5EBehaviour> allBehaviours = new HashSet<>();
    private final Set<H5EBehaviour> newBehaviours = new HashSet<>();
    private final Set<H5EBehaviour> deleteBehaviours = new HashSet<>();

    /**
     * The viewport object controls how the layers get displayed. Viewports can be zoomed, translated and possibly rotated. A viewport can be thought of as a camera looking at the layers.
     */
    private UPViewport<ExtendViewport> uiViewport;
    private UPViewport<?> gameViewport;
    private float uiScale = 1.0f;
    private boolean fixUiResolution = false;
    private int uiResolution = 576;
    private int minimumUiResolution = 224; // absolute minimum for the game, separate from minimum mobile resolution

    private OrthographicCamera uiCamera;
    private OrthographicCamera gameCamera;
    private int uiRenderingCalls = 0;
    private int gameRenderingCalls = 0;
    private boolean showRenderCalls = false;
    private float meanFrameTime = 0;

    public GenericEvent.GenericEvent1Args<Boolean> onToggleFullscreen = new GenericEvent.GenericEvent1Args<>();

    final InputMultiplexer inputMultiplexer = new SafeInputMultiplexer();

    /**
     * A list of graphical layers (H5ELayer) that this engine maintains
     */
    private final List<H5ELayer> layers = new ArrayList<>();
    private final List<H5ELayer> layers_view = Collections.unmodifiableList(layers);
    private final Map<String, H5ELayer> layersByName = new HashMap<>();

    private final Map<String, Object> engineBoundObjects = new TreeMap<>();

    /**
     * The FPS we would like to achieve
     */
    private int targetFps = 60;
    /**
     * The ticks per second we would like to achieve
     */
    private int targetTps = 50;
    /**
     * Enables or disables the 'frameskip' feature. Frame skip will attempt to execute the onTick event at the requested target frame rate
     * and skip drawing graphical frames to do it. Making this value false will make the game operations dependant on frame rate.
     */
    private boolean frameSkip = true;

    /**
     * If this is turned on, a line of text will appear across the top of the canvas with some debugging details
     */
    private boolean debugMode = false;

    private Skin skin;
    private int tickCount;//TODO: update
    private long lastFrameDraw;

    private int fpsFrameCount = 0;
    private double fps = 0;
    private long lastFpsCalcTime;

    public int tpt = 0;
    public int objCount = 0;

    Stage debugInfoStage;
    Label debugInfoLabel;
    public final GenericEvent.GenericEvent0Args preRender = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args postRender = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onResize = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent1Args<Float> onZoom = new GenericEvent.GenericEvent1Args<>();
    public final GenericEvent.GenericEvent0Args onTick = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onPause = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onResume = new GenericEvent.GenericEvent0Args();

    private boolean rotationMode;
    private float rotation;
    private int inputPriority;

    public static void setDisplayDoubleSized(boolean displayDoubleSized) {
        DisplayDoubleSized = displayDoubleSized;
    }


    public void clearBehaviours() {
        allBehaviours.clear();
        deleteBehaviours.clear();
        newBehaviours.clear();
    }

    public void create(int screenWidth, int screenHeight, int inputPriority) {
        uiCamera = new OrthographicCamera();
        gameCamera = new OrthographicCamera();
        uiViewport = new UPViewport<>(new ExtendViewport(screenWidth, screenHeight, uiCamera));
        gameViewport = new UPViewport<>(new ScreenViewport(gameCamera));
        uiViewport.update(screenWidth, screenHeight, true);
        gameViewport.update(screenWidth, screenHeight, true);

        this.inputPriority = inputPriority;

        setupInput();

        // Create the default layer (layer 0 - commonly used as background)
        newUILayer("default");

        TooltipManager.getInstance().initialTime = 0.5f;
    }

    public static boolean isForceMobileMode() {
        return GdxUtils.isMobileWeb();
    }

    public void setupDebugInfoViewer() {
        debugInfoStage = new Stage(getUiViewport());
        debugInfoLabel = new Label("", skin);
        debugInfoLabel.setFillParent(true);
        debugInfoStage.addActor(debugInfoLabel);
        debugInfoLabel.setAlignment(Align.bottomRight);
        debugInfoLabel.setFontScale(0.7f);
    }

    public <T> void setEngineBoundObject(String name, T object) {
        engineBoundObjects.put(name, object);
    }

    @SuppressWarnings("unchecked")
    public <T> T getEngineBoundObject(String name) {
        return (T) engineBoundObjects.get(name);
    }

    @SuppressWarnings("unchecked")
    public <T> T getOrCreateEngineBoundObject(String name, ReturningCallable0Args<T> generator) {
        T obj = (T) engineBoundObjects.get(name);
        if (obj != null) {
            return obj;
        }
        obj = generator.call();
        engineBoundObjects.put(name, obj);
        return obj;
    }

    public void addInputProcessor(InputProcessor processor) {
        inputMultiplexer.addProcessor(processor);
    }

    public void addInputProcessor(int index, InputProcessor processor) {
        inputMultiplexer.addProcessor(index, processor);
    }

    public void removeInputProcessor(InputProcessor processor) {
        inputMultiplexer.removeProcessor(processor);
    }

    private boolean fullscreen = false;

    public boolean toggleFullscreen() {
        if (Gdx.graphics.supportsDisplayModeChange()) {
            Graphics.Monitor currMonitor = Gdx.graphics.getMonitor();
            Graphics.DisplayMode displayMode = Gdx.graphics.getDisplayMode(currMonitor);
            if (fullscreen) {
                if (GdxUtils.isType(Application.ApplicationType.Desktop)) {
                    Gdx.graphics.setUndecorated(false);
                    final float windowFactor = 0.8f;
                    Gdx.graphics.setWindowedMode((int) (displayMode.width * windowFactor), (int) (displayMode.height * windowFactor));
                } else {
                    Gdx.graphics.setFullscreenMode(displayMode);
                }
                fullscreen = false;
            } else {
                Gdx.graphics.setUndecorated(true);
                Gdx.graphics.setWindowedMode(displayMode.width, displayMode.height);
                fullscreen = true;
            }
            if (onToggleFullscreen != null) {
                onToggleFullscreen.fire(fullscreen);
            }
        }
        return fullscreen;
    }

    public float getAbsoluteWidth() {
        return getWidth() * uiScale;
    }

    public float getAbsoluteHeight() {
        return getHeight() * uiScale;
    }

    public Boolean getFullscreen() {
        return Gdx.graphics.isFullscreen();
    }

    public void setupInput() {
        Gdx.input.setInputProcessor(MasterInputProcessor.INSTANCE);
        MasterInputProcessor.INSTANCE.addProcessor(inputPriority, inputMultiplexer);
    }

    public void removeInput() {
        Gdx.input.setInputProcessor(MasterInputProcessor.INSTANCE);
        MasterInputProcessor.INSTANCE.removeProcessor(inputPriority);
    }

    public Skin getSkin() {
        return skin;
    }

    public int getTickCount() {
        return tickCount;
    }

    public void setResourceManager(ResourceManager resourceManager) {
        this.resourceManager = resourceManager;
    }

    public void render() {
        try {
            doRender();
        } catch (Throwable ex) {
            Log.error("Exception on render-level", ex);
        }
    }

    protected void doRender() {
        long frameTime = System.currentTimeMillis();

        preRender.fire();
        if (frameSkip) {

            if (lastFrameDraw == 0) {
                // initialize for frameskip mode
                lastFrameDraw = System.currentTimeMillis();
            }

            // Determine how many ticks we need to execute
            double millisPerFrame = UPMath.floor(1000f / this.targetTps);
            long timeDiff = System.currentTimeMillis() - lastFrameDraw;
            double ticksToProcess = UPMath.floor(timeDiff / (millisPerFrame));

            int ticksProcessed = 0;
            for (int i = 0; i < ticksToProcess; i++) {
                // First clear the debugValuesPerTick
                GWTDebug.debugValuesPerTick.clear();

                this.onTick.fire();

                ticksProcessed++;
                tickCount++;

                // This will ensure the GUI doesn't lock up by exiting the method every
                // 200 ticks of "catch-up"
                if (ticksProcessed > 200) {
                    break;
                }
            }

            // We are only going to remember the last frame draw time if we've actually caught up
            // If we haven't caught up yet, we're going to only do 100 operation cycles before we
            // allow the browser to execute any events it needs.
            // Instead, we will simply add on the number of cycles we did end up doing
            // to the lastFrameDraw time.
            lastFrameDraw += ticksToProcess * millisPerFrame;

            // Now exit early to ensure we don't lock up the browser and instead of drawing the graphics,
            // this method will be executed again for further catch-up
            if (ticksProcessed > 100) {
                return;
            }
        } else    //Otherwise, turn it off
        {
            onTick.fire();
        }

        // keep track of FPS
        measureFPS();

        int uiRenderingCalls = uiViewport.getSpriteBatch().totalRenderCalls;
        int gameRenderingCalls = gameViewport.getSpriteBatch().totalRenderCalls;

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        for (H5ELayer layer : layers) {
            layer.act();
        }

        Batch batch = null;
        try {
            for (H5ELayer layer : layers) {
                if (batch != layer.getBatch()) {
                    if (batch != null && batch.isDrawing()) {
                        batch.end();
                    }
                    batch = layer.getBatch();
                    if (batch != null && !batch.isDrawing()) {
                        batch.begin();
                    }
                }
                try {
                    layer.draw();
                } catch (Throwable ex) {
                    log.error("Unable to draw layer " + Strings.inQuotes(layer.getName()), ex);
                }

            }
        } finally {
            if (batch != null && batch.isDrawing()) {
                batch.end();
            }
        }

        this.uiRenderingCalls = (uiViewport.getSpriteBatch().totalRenderCalls - uiRenderingCalls);
        this.gameRenderingCalls = (gameViewport.getSpriteBatch().totalRenderCalls - gameRenderingCalls);

        postRender.fire();

        frameTime = System.currentTimeMillis() - frameTime;
        if (meanFrameTime == 0) {
            meanFrameTime = frameTime;
        } else {
            meanFrameTime = frameTime * EMA_COEFFICIENT + meanFrameTime * EMA_COEFFICIENT;
        }
        drawDebugInfo();
    }

    protected void addBehaviour(H5EBehaviour behaviour) {
        if (behavioursAreProcessing)
            newBehaviours.add(behaviour);
        else
            allBehaviours.add(behaviour);

    }

    protected void removeBehaviour(H5EBehaviour behaviour) {
        if (behavioursAreProcessing)
            deleteBehaviours.add(behaviour);
        else
            allBehaviours.remove(behaviour);
    }

    public void processBehaviours(float delta) {
        try {
            behavioursAreProcessing = true;
            for (H5EBehaviour behaviour : allBehaviours) {
                H5ELayer.invalidateBufferForActor(behaviour.getElement());
                behaviour.act(delta);
            }
        } finally {
            behavioursAreProcessing = false;
        }

        newBehaviours.removeAll(deleteBehaviours); //Make sure no new behaviours were already removed

        // Now delete all the behaviours that were attempted to be deleted while the behaviours were processing
        allBehaviours.removeAll(deleteBehaviours);
        deleteBehaviours.clear();

        // Now add all the behaviours that were attempted to be added while the behaviours were processing
        allBehaviours.addAll(newBehaviours);
        newBehaviours.clear();
    }

    private void measureFPS() {
        this.fpsFrameCount++;

        long msSinceLastFpsCalc = System.currentTimeMillis() - this.lastFpsCalcTime;
        if (msSinceLastFpsCalc >= 1000) {
            double secondsSinceLast = (double) msSinceLastFpsCalc / 1000d;
            this.fps = ((double) this.fpsFrameCount / secondsSinceLast);
            this.fpsFrameCount = 0;
            this.lastFpsCalcTime = System.currentTimeMillis();

            if (log.isDebugEnabled()) {
                log.debug("fps=" + fps);
            }
        }
    }

    private void drawDebugInfo() {
        if (!this.debugMode || debugInfoLabel == null) {
            return;
        }

        StringBuilder debugInfo = debugInfoLabel.getText();
        debugInfo.setLength(0);
        debugInfo.append("MSPT: ").append(this.tpt).append(" OBJs: ").append(this.objCount).append(" FPS: ").append(Math.round(this.fps * 100) / 100f);

        if (showRenderCalls) {
            debugInfo.append(", UI: ")
                    .append(uiRenderingCalls)
                    .append(", Game: ")
                    .append(gameRenderingCalls)
                    .append(", FT: ")
                    .append(Math.round(meanFrameTime * 100) / 100F);
        }

        //noinspection unchecked
        for (Object key : UPUtils.fastIterable(GWTDebug.debugValuesPerTick.keySet())) {
            debugInfo.append(", ").append(key).append("=").append(GWTDebug.debugValuesPerTick.get(key));
        }

        debugInfoLabel.invalidateHierarchy();
        debugInfoStage.draw();
    }

    public void resize(int width, int height) {
        if (fixUiResolution) {
            uiViewport.getViewport().setMinWorldWidth(UPMath.max(UPMath.round(uiResolution / uiScale), minimumUiResolution));
            uiViewport.getViewport().setMinWorldHeight(UPMath.max(UPMath.round(uiResolution / uiScale), minimumUiResolution));
        } else {
            uiViewport.getViewport().setMinWorldWidth(UPMath.max(UPMath.round(width / uiScale), minimumUiResolution));
            uiViewport.getViewport().setMinWorldHeight(UPMath.max(UPMath.round(height / uiScale), minimumUiResolution));
        }
        uiViewport.update(width, height, true);
        gameViewport.update(width, height, true);
        onResize.fire();
    }


    public void dispose() {
        for (H5ELayer layer : layers) {
            layer.dispose();
        }
        removeInput();
    }

    /**
     * Sets the target TPS for this instance of the engine.
     *
     * @param value The TPS that you wish the engine to attempt to achieve.
     */
    public void setTargetTps(int value) {
        if (value < 0)
            throw new IllegalArgumentException("Invalid target TPS: " + value);

        targetTps = value;
    }

    /**
     * @return the target TPS that this engine instance will attempt to achieve.
     */
    public int getTargetTps() {
        return targetTps;
    }

    /**
     * Sets the target FPS for this instance of the engine.
     *
     * @param value The FPS that you wish the engine to attempt to achieve.
     */
    public void setTargetFps(int value) {
        if (value < 0)
            throw new IllegalArgumentException("Invalid target FPS: " + value);

        targetFps = value;
    }

    /**
     * @return the target FPS that this engine instance will attempt to achieve.
     */
    public int getTargetFps() {
        return targetFps;
    }

    public void setFrameSkip(boolean value) {
        this.frameSkip = value;
    }

    public double getFps() {
        return fps;
    }

    public boolean getFrameSkip() {
        return frameSkip;
    }

    public void setDebugMode(boolean value) {
        debugMode = value;
    }

    public void setLayerDebugMode(boolean value) {
        for (H5ELayer layer : layers) {
            layer.setDebugAll(true);
        }
    }

    public boolean getDebugMode() {
        return debugMode;
    }

    /**
     * Gets the resource manager for this engine.
     * The resource manager allows full access to image/audio resources that
     * get loaded in.
     *
     * @return The resource manager for this engine. There is only one.
     */
    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    /**
     * Creates a new basic {@link H5ELayer} on top of the layer stack.
     * To add a custom layer that extends {@link H5ELayer}, use {@link #addLayer(H5ELayer)}.
     *
     * @return The newly created layer
     */
    public H5ELayer newUILayer(String name) {
        if (Strings.isEmpty(name)) {
            name = "Layer " + layers.size();
        }
        return addLayer(new H5ELayer(this, name, getUiViewport()));
    }

    public H5ELayer newGameLayer(String name) {
        if (Strings.isEmpty(name)) {
            name = "Layer " + layers.size();
        }
        return addLayer(new H5ELayer(this, name, getGameViewport()));
    }


    /**
     * This allows you to add a custom layer to the list of layers in the engine.
     * A custom layer extends {@link H5ELayer} to add special capabilities, not available in the base class.
     */
    public <T extends H5ELayer> T addLayer(T layer) {
        checkLayer(layer);

        layers.add(layer);
        layersByName.put(layer.getName(), layer);

        return layer;
    }

    /**
     * This allows you to add a custom layer to the list of layers in the engine.
     * A custom layer extends {@link H5ELayer} to add special capabilities, not available in the base class.
     */
    public <T extends H5ELayer> UILayerProxy addUiProxiedLayer(T layer) {
        checkLayer(layer);
        String layerName;

        UILayerProxy proxy = new UILayerProxy(layer);
        layers.add(proxy);
        layersByName.put(layer.getName(), layer);

        return proxy;
    }

    private <T extends H5ELayer> void checkLayer(T layer) {
        if (layer == null) {
            throw new IllegalArgumentException("Layer can't be null");
        }
        String layerName = layer.getName();
        if (Strings.isEmpty(layerName)) {
            throw new IllegalStateException("Layer must have a name");
        }
        if (layersByName.containsKey(layerName)) {
            throw new IllegalStateException("The engine already has layer " + Strings.inQuotes(layerName));
        }
    }

    /**
     * Returns the viewport this engine is currently using. A viewport is used
     * as a "camera", use the viewport's "translateViewport" function to move
     * the "camera".
     *
     * @return {H5EViewport} The viewport this engine is currently using.
     */
    public UPViewport<ExtendViewport> getUiViewport() {
        return uiViewport;
    }

    public UPViewport<?> getGameViewport() {
        return gameViewport;
    }

    public OrthographicCamera getUiCamera() {
        return uiCamera;
    }

    public OrthographicCamera getGameCamera() {
        return gameCamera;
    }

    public void updateGameCamera(Callable1Args<OrthographicCamera> callback) {
        callback.call(gameCamera);

        // We don't know exactly what the client has done to the camera
        gameViewport.onCameraZoomUpdate();
        gameViewport.onCameraPositionRotationUpdate();
    }

    public List<H5ELayer> getLayers() {
        return layers_view;
    }

    public ListIterator<H5ELayer> getLayersIterator() {
        return layers_view.listIterator(layers.size());
    }

    public H5ELayer getLayer(String name) {
        if (Strings.isEmpty(name)) {
            throw new IllegalArgumentException("Name can't be null or empty");
        }
        return layersByName.get(name);
    }


    public void setSkin(Skin skin) {
        this.skin = skin;
    }

    public int getWidth() {
        return (int) uiViewport.getWorldWidth();
    }

    public int getHeight() {
        return (int) uiViewport.getWorldHeight();
    }

    public float getGameZoom() {
        return gameCamera.zoom * uiScale;
    }

    public void setGameZoom(float zoom, float minZoom) {
        zoom = zoom < minZoom ? minZoom : zoom;

        gameCamera.zoom = zoom / uiScale;
        gameViewport.onCameraZoomUpdate();
        gameViewport.onCameraPositionRotationUpdate();
        onZoom.fire(zoom);
    }

    public void setGameZoom(float zoom) {
        setGameZoom(zoom, MIN_ZOOM);
    }

    public boolean isRotationMode() {
        return rotationMode;
    }

    public void setRotationMode(boolean rotationMode) {
        this.rotationMode = rotationMode;
    }

    public float getRotation() {
        return rotation;
    }

    public void setGameCamera(float x, float y, float rotation) {
        final OrthographicCamera camera = getGameCamera();
        camera.position.set(x, y, 0);
        if (!Check.equal(this.rotation, rotation)) {
            camera.up.set(0, 1, 0);
            if (rotation != 0) {
                camera.up.rotate(camera.direction, rotation);
            }
            this.rotation = rotation;
        }
        gameViewport.onCameraPositionRotationUpdate();
    }

    public boolean isShowRenderCalls() {
        return showRenderCalls;
    }

    public void setShowRenderCalls(boolean showRenderCalls) {
        this.showRenderCalls = showRenderCalls;
        this.debugInfoLabel.setStyle(getSkin().get("label-mono", Label.LabelStyle.class));
    }

    public void clearLayers() {
        for (H5ELayer layer : layers) {
            layer.dispose();
        }
        layers.clear();
    }

    public float getUiScale() {
        return uiScale;
    }

    public void setUiScale(float uiScale) {
        if (uiScale == this.uiScale) return;
        this.uiScale = uiScale;
        resize(uiViewport.getScreenWidth(), uiViewport.getScreenHeight());
    }

    public void setUiScale(float uiScale, boolean forceResize) {
        this.uiScale = uiScale;
        if (forceResize) resize(uiViewport.getScreenWidth(), uiViewport.getScreenHeight());
    }

    public int getUiResolution() {
        return uiResolution;
    }

    public void setUiResolution(int uiResolution) {
        if (uiResolution == this.uiResolution) return;
        this.uiResolution = uiResolution;
        resize(uiViewport.getScreenWidth(), uiViewport.getScreenHeight());
    }

    public boolean isUiResolutionFixed() {
        return fixUiResolution;
    }

    public void fixUiResolution(boolean fixUiResolution) {
        if (fixUiResolution == this.fixUiResolution) return;
        this.fixUiResolution = fixUiResolution;
        resize(uiViewport.getScreenWidth(), uiViewport.getScreenHeight());
    }

    public int getMinimumUiResolution() {
        return minimumUiResolution;
    }

    public void setMinimumUiResolution(int minimumUiResolution) {
        this.minimumUiResolution = minimumUiResolution;
    }

    public float calculatePrefUiScale() {
        if(GdxUtils.isType(Application.ApplicationType.iOS)) {
            return 0.8f;
        }
        if(GdxUtils.isType(Application.ApplicationType.Desktop, Application.ApplicationType.WebGL) && !GdxUtils.isMobileWeb()) {
            return 1f;
        }
        /*  Determines the best UI scale for mobile devices. If the width/height of the display
            is smaller than the minimum, the scale will be adjusted so the minimum is effectively
            the UI resolution. If the display is larger, the UI will be scaled based on DPI. */
        float windowSize;
        float displayModeSize;
        float minimumUiSize = 464;

        float windowWidth = Gdx.graphics.getWidth();
        float windowHeight = Gdx.graphics.getHeight();
        if (windowHeight > windowWidth) {
            windowSize = windowWidth;
            displayModeSize = Gdx.graphics.getDisplayMode().width;
        } else {
            windowSize = windowHeight;
            displayModeSize = Gdx.graphics.getDisplayMode().height;
        }

        /* Note:    The approximate DPI of the screen is 160*windowSize/displayModeSize
                    The UI is being scaled such that the game DPI is approximately 160 */
        float dpi = 160f * 1.2f;
        if(DisplayDoubleSized) {
            dpi *= 1.75f;
        }
        if (Gdx.app.getType() == Application.ApplicationType.iOS) {
            dpi = 160f;
        }
        float dpiScale = (float) getDPI() / dpi;
        float result = windowSize / UPMath.max(displayModeSize, minimumUiSize) * dpiScale;
        return UPMath.cap(result, 0.5f, 2f);
    }

    public double getDPI() {
        return Math.min(Gdx.graphics.getPpiX(), Gdx.graphics.getPpiY());
    }

    public boolean isMobileMode() {
        return isForceMobileMode() || (GdxUtils.getDensity() > 1.0 && isDisplayMobileSize());
    }

    public boolean isDisplayMobileSize() {
        // This should accurately identify mobile devices, however it may return mobile for certain desktop displays
        Graphics.DisplayMode displayMode = Gdx.graphics.getDisplayMode();
        double displayModeSizeInches = UPMath.hypotenuse(
                displayMode.width / Gdx.graphics.getPpiX(),
                displayMode.height / Gdx.graphics.getPpiY()
        ); // This resolution already has dpr factored in
        return displayModeSizeInches < 10.0;
    }

    public void setFullscreenWindowOverlaysGameLayer(boolean value) {
    }
}
