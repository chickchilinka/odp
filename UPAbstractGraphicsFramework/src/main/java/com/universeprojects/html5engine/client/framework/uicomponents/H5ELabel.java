package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class H5ELabel extends Label implements GraphicElement {

    public H5ELabel(H5ELayer layer) {
        this("", layer);
    }

    public H5ELabel(String text, H5ELayer layer) {
        super(text, layer.getEngine().getSkin());
        setStage(layer);
    }

    public H5ELabel(String text, H5ELayer layer, String styleName) {
        super(text, layer.getEngine().getSkin(), styleName);
        setStage(layer);
    }

    public H5ELabel(String text, H5ELayer layer, LabelStyle style) {
        super(text, layer.getEngine().getSkin());
        setStyle(style);
        setStage(layer);
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    @Override
    public void invalidate() {
        H5ELayer.invalidateBufferForActor(this);
        super.invalidate();
    }
}
