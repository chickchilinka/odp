package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.universeprojects.common.shared.math.UPVector;

public class H5EMoveTowardsBehaviour extends H5EBehaviour {


    private Vector2 destination;
    private Vector2 location;
    private H5EGraphicElement mainObject;
    private H5EGraphicElement destinationObject;
    private Color fadeColor;
    private float speed;
    private float dist;
    private float scale;
    private final float FADE_DIST = 0.1f; //keep less than 1;

    public <T extends H5EGraphicElement> H5EMoveTowardsBehaviour(T toMove, T moveTo, Float speed) {
        super(toMove);
        if(toMove == null || moveTo == null)return;
        mainObject = toMove;
        destinationObject = moveTo;
        destination = new Vector2(destinationObject.getX(), destinationObject.getY());
        location = new Vector2(mainObject.getX(), mainObject.getY());
        fadeColor = mainObject.getColor();
        calcSpeed(speed);
    }
    public <T extends H5EGraphicElement> H5EMoveTowardsBehaviour(T toMove, UPVector dest, Float speed, Float scale) {
        super(toMove);
        if(toMove == null || dest == null)return;
        mainObject = toMove;
        destinationObject = null;
        destination = new Vector2(dest.x, dest.y);
        location = new Vector2(mainObject.getX(), mainObject.getY());
        fadeColor = mainObject.getColor();
        calcSpeed(speed);
        dist = location.dst(destination);
        this.scale = scale;
    }
    public <T extends H5EGraphicElement> H5EMoveTowardsBehaviour(T toMove, Float x, Float y, Float speed) {
        super(toMove);
        if(toMove == null || x == null || y == null)return;
        mainObject = toMove;
        destinationObject = null;
        destination = new Vector2(x,y);
        location = new Vector2(mainObject.getX(), mainObject.getY());
        fadeColor = mainObject.getColor();
        calcSpeed(speed);

    }

    private void calcSpeed(Float speed){
        if(speed != null){
            this.speed = 0.1f /speed;
        }else{
            this.speed = 0.1f;
        }
    }


    @Override
    public void act(float delta) {
        if(destinationObject != null){
            destination.x = destinationObject.getX();
            destination.y = destinationObject.getY();
            location = location.lerp(destination, speed);
            mainObject.setPosition(location.x, location.y);
        }else{
            location = location.lerp(destination, speed);
            mainObject.setPosition(location.x, location.y);
        }
        if(location.dst(destination) < (dist * FADE_DIST)){
            fadeColor.a = MathUtils.lerp(fadeColor.a, 0, speed);
            mainObject.setColor(fadeColor);
        }
    }

    @Override
    public String getName() {
        return "move-towards";
    }
}
