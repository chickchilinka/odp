package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator1Args;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EGamepadButtonBinding<C extends H5ECommand> extends H5EGamepadBinding<C> {

    public final int buttonCode;
    private boolean wasDown = false;
    private final H5ECommandInputTranslator1Args<Boolean, C> translator;

    public H5EGamepadButtonBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator1Args<Boolean, C> translator, C command, int gamepadIndex, int buttonCode) {
        super(controlSetup, command, gamepadIndex);
        this.translator = translator;
        this.buttonCode = buttonCode;
    }

    @Override
    public void doTickWhenActive() {
        boolean down = gamePad.isDown(buttonCode);
        if (down && !wasDown) {
            processEvent(true);
        } else if (!down && wasDown) {
            processEvent(false);
        }
        wasDown = down;
    }

    public void processEvent(boolean on) {
        fire(translator.generateParams(command, on));
    }
}