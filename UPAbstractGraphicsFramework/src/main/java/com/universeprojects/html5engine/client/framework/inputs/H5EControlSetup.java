package com.universeprojects.html5engine.client.framework.inputs;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.html5engine.client.framework.inputs.bindings.*;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EJoystick;

import java.util.LinkedHashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class H5EControlSetup implements InputProcessor, H5EKeyboardBindable {

    private final H5EInputManager inputManager;
    private final String name;

    private final Map<H5ECommand, H5ECommandHandler> commandHandlers = new LinkedHashMap<>();

    // desktop / laptop controls
    private final H5EControlBindings<Integer, H5EKeyboardBinding> keyboardBindings = new H5EControlBindings<>();
    private final H5EControlBindings<Integer, H5EMouseButtonBinding> mouseButtonBindings = new H5EControlBindings<>();
    private final H5EControlBindings<Integer, H5EMouseWheelBinding> mouseWheelBindings = new H5EControlBindings<>();

    // gamepad controls
    private final H5EControlBindings<Integer, H5EGamepadStickSeparateAxisBinding> gamepadBindings_SeparateAxisSticks = new H5EControlBindings<>();
    private final H5EControlBindings<String, H5EGamepadStickCombinedAxisBinding> gamepadBindings_CombinedAxisSticks = new H5EControlBindings<>();
    private final H5EControlBindings<Integer, H5EGamepadButtonBinding> gamepadBindings_Buttons = new H5EControlBindings<>();
    private final H5EControlBindings<Integer, H5EGamepadDPadBinding> gamepadBindings_DPad = new H5EControlBindings<>();
    private final H5EControlBindings<Integer, H5EGamepadAnalogTriggerBinding> gamepadBindings_AnalogTriggers = new H5EControlBindings<>();

    // on-screen controls
    private final H5EControlBindings<String, H5EJoystickAxisBinding> uiJoystickAxisBindings = new H5EControlBindings<>();
    private final H5EControlBindings<Actor, H5EButtonBinding> uiButtonBindings = new H5EControlBindings<>();

    private final int MOUSE_WHEEL_IDX = 0; // <-- only one mouse wheel supported for now
    private int uiJoystickIndex = 0;
    private int gamepadStickSeparateAxisIndex = 0;
    private int gamepadStickCombinedAxisIndex = 0;
    private int gamepadButtonIndex = 0;
    private int gamepadAnalogTriggerIndex = 0;

    private H5EGamePad[] gamePads;
    private boolean gamepadsAreRegistered = false;

    H5EControlSetup(H5EInputManager inputManager, String name) {
        this.inputManager = inputManager;
        this.name = name;
        initGamePads();
    }

    public String getName() {
        return name;
    }

    public void clear() {
        commandHandlers.clear();

        keyboardBindings.clear();
        mouseButtonBindings.clear();
        mouseWheelBindings.clear();
        gamepadBindings_SeparateAxisSticks.clear();
        gamepadBindings_CombinedAxisSticks.clear();
        gamepadBindings_Buttons.clear();
        gamepadBindings_DPad.clear();
        gamepadBindings_AnalogTriggers.clear();
        uiJoystickAxisBindings.clear();
        Map<Actor, H5EButtonBinding> bindingsByKey = uiButtonBindings.getBindingsByKey();
        if (bindingsByKey != null) {
            for (H5EButtonBinding binding : bindingsByKey.values()) {
                binding.clear();
            }
        }
        uiButtonBindings.clear();
    }

    public <T extends H5ECommand> T addCommandHandler(T command, H5ECommandHandler handler) {
        commandHandlers.put(command, handler);
        return command;
    }

    public boolean isEnabled() {
        return inputManager.isEnabled() && inputManager.activeControlSetups.contains(this);
    }

    public void fire(H5ECommand command, H5ECommandParams params) {
        if (!isEnabled()) {
            return; // <-- shouldn't happen but just to be sure
        }
        H5ECommandHandler cmd = commandHandlers.get(command);
        if (cmd == null || params == null) {
            return; // if there is no command for this command-name, abort
        }
        if (!cmd.isValid()) {
            return; // If the owner of the action has been marked for destruction, abort
        }

        inputManager.addCommandToExecute(cmd, params);
    }

    public H5ECommandHandler getCommandHandler(H5ECommand command) {
        return commandHandlers.get(command);
    }

    private void initGamePads() {
        if (!gamepadsAreRegistered) {
            gamePads = new H5EGamePad[H5EGamePad.getGamepadCount()];
            for (int i = 0; i < gamePads.length; i++) {
                gamePads[i] = new H5EGamePad(i);
            }
            gamepadsAreRegistered = true;
        }
    }

    public H5EGamePad getGamePad(int index) {
        return gamePads[index];
    }


    /* -------------------------------------------------------
     * addControl Methods, for all control types
     */

    public <C extends H5ECommand> void addKeyboardBinding(C command, H5ECommandInputTranslator1Args<Boolean, C> translator, int... keyCodes) {
        for (int keyCode : keyCodes) {
            keyboardBindings.add(keyCode, new H5EKeyboardBinding<>(this, translator, command, keyCode));
        }
    }

    public void removeKeyboardBinding(int keyCode) {
        keyboardBindings.remove(keyCode);
    }

    public <C extends H5ECommand> void addMouseButtonBinding(C command, H5ECommandInputTranslator2Args<Boolean, Vector2, C> translator, int buttonID, int inputParameterX, int inputParameterY) {
        mouseButtonBindings.add(buttonID, new H5EMouseButtonBinding<>(this, translator, command, buttonID, inputParameterX, inputParameterY));
    }

    public <C extends H5ECommand> void addMouseWheelBinding(C command, H5ECommandInputTranslator2Args<Float, Float, C> translator, int inputParameter) {
        H5EMouseWheelBinding<C> binding = new H5EMouseWheelBinding<>(this, translator, command, inputParameter);
        mouseWheelBindings.add(MOUSE_WHEEL_IDX, binding);
    }

    public <C extends H5ECommand> void addGamepadStickSeparateAxisBinding(C command, H5ECommandInputTranslator2Args<Boolean, Float, C> translator, int gamepadIndex, int axisCode, int inputParameter, float factor) {
        gamepadBindings_SeparateAxisSticks.add(gamepadStickSeparateAxisIndex++, new H5EGamepadStickSeparateAxisBinding<>(this, translator, command, gamepadIndex, axisCode, inputParameter, factor));
    }

    public <CX extends H5ECommand, CY extends H5ECommand> void addGamepadStickCombinedAxisBinding(
            CX commandX, CY commandY,
            H5ECommandInputTranslator2Args<Boolean, Float, CX> translatorX,
            H5ECommandInputTranslator2Args<Boolean, Float, CY> translatorY,
            int gamepadIndex, int axisCodeX, int axisCodeY, int inputParameterX, int inputParameterY, float factorX, float factorY) {

        H5EGamepadStickCombinedAxisBinding<CX> bindingX = new H5EGamepadStickCombinedAxisBinding<>(this, translatorX, commandX, gamepadIndex, axisCodeX, inputParameterX, true);
        H5EGamepadStickCombinedAxisBinding<CY> bindingY = new H5EGamepadStickCombinedAxisBinding<>(this, translatorY, commandY, gamepadIndex, axisCodeY, inputParameterY, false);
        bindingX.setOtherAxis(bindingY);
        bindingY.setOtherAxis(bindingX);

        gamepadBindings_CombinedAxisSticks.add(gamepadStickCombinedAxisIndex + "x", bindingX);
        gamepadBindings_CombinedAxisSticks.add(gamepadStickCombinedAxisIndex + "y", bindingY);
        gamepadStickCombinedAxisIndex++;
    }

    public <C extends H5ECommand> void addGamepadButtonBinding(C command, H5ECommandInputTranslator1Args<Boolean, C> translator, int gamepadIndex, int buttonCode) {
        gamepadBindings_Buttons.add(gamepadButtonIndex++, new H5EGamepadButtonBinding<>(this, translator, command, gamepadIndex, buttonCode));
    }

    public <C extends H5ECommand> void addGamepadDPadBinding(C command, H5ECommandInputTranslator1Args<Boolean, C> translator, int gamepadIndex, int buttonCode) {
        gamepadBindings_DPad.add(gamepadButtonIndex++, new H5EGamepadDPadBinding<>(this, translator, command, gamepadIndex, buttonCode));
    }

    public <C extends H5ECommand> void addGamepadAnalogTriggerBinding(C command, H5ECommandInputTranslator2Args<Boolean, Float, C> translator, int gamepadIndex, int buttonCode, int inputParameter) {
        gamepadBindings_AnalogTriggers.add(gamepadAnalogTriggerIndex++, new H5EGamepadAnalogTriggerBinding<>(this, translator, command, gamepadIndex, buttonCode, inputParameter));
    }

    public <C extends H5ECommand> void addUIButtonBinding(C command, H5ECommandInputTranslator1Args<Boolean, C> translator, Actor button) {
        ClickListener listener = new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int buttonId) {
                uiButtonBindings.processFor(button, (binding) -> binding.fire(true));
                event.handle();
                return super.touchDown(event, x, y, pointer, buttonId);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int buttonId) {
                uiButtonBindings.processFor(button, (binding) -> binding.fire(false));
                event.handle();
                super.touchUp(event, x, y, pointer, buttonId);
            }
        };
        uiButtonBindings.add(button, new H5EButtonBinding<>(this, translator, command, button, listener));
        button.addListener(listener);
    }

    public <C extends H5ECommand> void addTouchscreenUIButtonBinding(C command, H5ECommandInputTranslator1Args<Boolean, C> translator, Actor button) {
        ClickListener listener = new ClickListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                uiButtonBindings.processFor(button, (binding) -> binding.fire(true));
                event.handle();
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                uiButtonBindings.processFor(button, (binding) -> binding.fire(false));
                event.handle();
            }
        };
        uiButtonBindings.add(button, new H5EButtonBinding<>(this, translator, command, button, listener));
        button.addListener(listener);
    }

    public <CX extends H5ECommand, CY extends H5ECommand> void addUIJoystickAxisBinding(
            H5ECommandInputTranslator2Args<Boolean, Float, CX> translatorX,
            H5ECommandInputTranslator2Args<Boolean, Float, CY> translatorY,
            CX commandX, CY commandY, H5EJoystick joystick, int inputParameterX, int inputParameterY) {

        H5EJoystickAxisBinding<CX> bindingX = new H5EJoystickAxisBinding<>(this, translatorX, commandX, joystick, inputParameterX, true);
        H5EJoystickAxisBinding<CY> bindingY = new H5EJoystickAxisBinding<>(this, translatorY, commandY, joystick, inputParameterY, false);
        bindingX.setOtherAxis(bindingY);
        bindingY.setOtherAxis(bindingX);

        uiJoystickAxisBindings.add(uiJoystickIndex + "x", bindingX);
        uiJoystickAxisBindings.add(uiJoystickIndex + "y", bindingY);
        uiJoystickIndex++;
    }

    public <C extends H5ECommand> void removeCommandHandler(C command) {
        commandHandlers.remove(command);
    }


    /* -------------------------------------------------------------
     * Polling/Event handling
     */

    public void doTick() {
        uiJoystickAxisBindings.doTick();

        if (H5EGamePad.getGamepadCount() > 0) {
            for (H5EGamePad pad : gamePads) {
                pad.poll();
            }
            gamepadBindings_SeparateAxisSticks.doTick();
            gamepadBindings_CombinedAxisSticks.doTick();
            gamepadBindings_Buttons.doTick();
            gamepadBindings_DPad.doTick();
            gamepadBindings_AnalogTriggers.doTick();
        }
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (!isEnabled()) return false;
        keyboardBindings.processFor(keyCode, (binding) -> binding.fire(true));
        return false;
    }

    @Override
    public boolean keyUp(int keyCode) {
        if (!isEnabled()) return false;
        keyboardBindings.processFor(keyCode, (binding) -> binding.fire(false));
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!isEnabled()) return false;
        mouseButtonBindings.processFor(button, (binding) -> binding.fire(true, screenX, screenY));
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!isEnabled()) return false;
        mouseButtonBindings.processFor(button, (binding) -> binding.fire(false, screenX, screenY));
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        if (!isEnabled()) return false;
        mouseWheelBindings.processFor(MOUSE_WHEEL_IDX, (binding) -> binding.fire(amountX, amountY));
        return false;
    }


}
