package com.universeprojects.html5engine.client.framework;

public interface H5EZoomAwareLayer {
    float getZoom();
}
