package com.universeprojects.html5engine.client.framework;

public class H5EDestroySelfBehaviour extends H5EBehaviour {

    private float delay;
    private float elapsed;
    private float pace = 0.02f;
    private H5EGraphicElement element;

    public void setPace(float pace) {
        this.pace = pace;
    }

    public float getPace() {
        return this.pace;
    }

    public <T extends H5EGraphicElement> H5EDestroySelfBehaviour(T graphicElement, float delay) {
        super(graphicElement);
        this.delay = delay;
        element = graphicElement;
    }

    @Override
    public void act(float delta) {
        elapsed += pace;
        if(elapsed >= delay) {
            element.remove();
            element = null;
        }
    }

    @Override
    public String getName() {
        return "destroy-self";
    }
}
