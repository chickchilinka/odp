package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.InputMultiplexer;
import com.universeprojects.common.shared.util.Log;

public class SafeInputMultiplexer extends InputMultiplexer {
    @Override
    public boolean keyDown(int keycode) {
        try {
            return super.keyDown(keycode);
        } catch (Throwable ex) {
            Log.error("Error processing input", ex);
            return true;
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        try {
            return super.keyUp(keycode);
        } catch (Throwable ex) {
            Log.error("Error processing input", ex);
            return true;
        }
    }

    @Override
    public boolean keyTyped(char character) {
        try {
            return super.keyTyped(character);
        } catch (Throwable ex) {
            Log.error("Error processing input", ex);
            return true;
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        try {
            return super.touchDown(screenX, screenY, pointer, button);
        } catch (Throwable ex) {
            Log.error("Error processing input", ex);
            return true;
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        try {
            return super.touchUp(screenX, screenY, pointer, button);
        } catch (Throwable ex) {
            Log.error("Error processing input", ex);
            return true;
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        try {
            return super.touchDragged(screenX, screenY, pointer);
        } catch (Throwable ex) {
            Log.error("Error processing input", ex);
            return true;
        }
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        try {
            return super.mouseMoved(screenX, screenY);
        } catch (Throwable ex) {
            Log.error("Error processing input", ex);
            return true;
        }
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        try {
            return super.scrolled(amountX, amountY);
        } catch (Throwable ex) {
            Log.error("Error processing input", ex);
            return true;
        }
    }
}
