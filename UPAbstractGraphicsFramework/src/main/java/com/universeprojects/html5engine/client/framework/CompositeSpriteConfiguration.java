package com.universeprojects.html5engine.client.framework;

import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.json.shared.parser.JSONParserFactory;
import com.universeprojects.json.shared.parser.ParseException;

import java.util.LinkedHashMap;
import java.util.Map;

public class CompositeSpriteConfiguration {
    private final JSONObject baseConfiguration;
    private JSONObject finalConfiguration;
    private Map<String, String> variables;

    public CompositeSpriteConfiguration(JSONObject baseConfiguration) {
        this.baseConfiguration = baseConfiguration;
    }

    public void setVariable(String name, String value) {
        if(variables == null) {
            variables = new LinkedHashMap<>();
        }
        variables.put(name, value);
        finalConfiguration = null;
    }

    public JSONObject getBaseConfiguration() {
        return baseConfiguration;
    }

    public JSONObject getFinalConfiguration() {
        generateFinalConfiguration();
        return finalConfiguration;
    }

    private void generateFinalConfiguration() {
        if(finalConfiguration != null) return;
        if(variables == null || variables.isEmpty()) {
            finalConfiguration = baseConfiguration;
            return;
        }
        String json = baseConfiguration.toJSONString();

        for (Map.Entry<String, String> entry : variables.entrySet()) {
            json = json.replace("${" + entry.getKey() + "}", entry.getValue());
        }

        try {
            finalConfiguration = (JSONObject) JSONParserFactory.getParser().parse(json);
        } catch (ParseException e) {
            throw new IllegalStateException("Composite sprite configuration (with variables) failed to reparse after variable substitutions. JSON: " + json);
        }
    }
}
