package com.universeprojects.html5engine.client.framework.inputs;

import java.util.Arrays;

public abstract class H5ECommandParams<C extends H5ECommand> {
    protected final Object[] parameters;

    protected H5ECommandParams(Object... parameters) {
        this.parameters = parameters;
    }

    public Object[] getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        return "H5ECommandParams{" +
            "parameters=" + Arrays.toString(parameters) +
            '}';
    }
}
