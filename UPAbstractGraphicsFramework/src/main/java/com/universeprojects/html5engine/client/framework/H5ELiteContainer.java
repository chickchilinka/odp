package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.universeprojects.html5engine.shared.abstractFramework.Container;

public class H5ELiteContainer extends Group implements Container {

    public H5ELiteContainer(H5ELayer layer) {
        setStage(layer);
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }
}
