package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.client.framework.resourceloader.DownloadingSprite;
import com.universeprojects.html5engine.shared.abstractFramework.BaseSpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteType;

import java.util.Objects;


public class H5ESpriteType extends BaseSpriteType implements SpriteType {

    private H5EImage image;
    /**
     * The canvas element that may have been created by a call to getCanvas(). This would only be used if the user
     * needed to get the pixel data for an image.
     */
    private String imageKey;
    /**
     * The key that points to this sprite type
     */
    private final String spriteTypeKey;
    private TextureRegion region;

    H5ESpriteType(H5EResourceManager resourceManager, String spriteTypeKey) {
        this.resourceManager = resourceManager;
        this.spriteTypeKey = spriteTypeKey;
    }

    public H5ESpriteType(H5EResourceManager resourceManager, String imageKey, String spriteTypeKey) {
        Dev.checkNotNull(resourceManager);
        Dev.checkNotEmpty(imageKey);

        this.resourceManager = resourceManager;
        this.imageKey = imageKey;
        this.spriteTypeKey = spriteTypeKey != null ? spriteTypeKey : imageKey;

        reloadImage();
    }

    /**
     * Call this method to ensure that all variables are valid, given the dimensions of the image.
     *
     * @throws RuntimeException image is not available
     */
    @Override
    public void stabilize() {
        if (isStable) {
            return;
        }

        if (image == null) {
            return;
        }
        // TODO: Do we really want to throw?
        //	throw new RuntimeException("Can't perform operation: image not available");

        int imageWidth = image.getWidth();
        int imageHeight = image.getHeight();

        if (areaX < 0) {
            areaX = 0;
        }
        if (areaX > imageWidth) {
            areaX = imageWidth;
        }
        if (areaY < 0) {
            areaY = 0;
        }
        if (areaY > imageHeight) {
            areaY = imageHeight;
        }
        if (areaX + areaWidth > imageWidth) {
            areaWidth = imageWidth - areaX;
        }
        if (areaY + areaHeight > imageHeight) {
            areaHeight = imageHeight - areaY;
        }

        isStable = true;
    }

    public void setImage(H5EImage image) {
        this.image = image;
    }

    public String getSpriteTypeKey() {
        return spriteTypeKey;
    }

    /**
     * This method will change the image key to the image the sprite is using. The next time this sprite is drawn, the
     * new image will be displayed.
     *
     * @param imageKey The image key that will be used for sprite type's image data acquisition
     */
    @Override
    public void setImage(String imageKey) {
        Dev.checkNotEmpty(imageKey);
        if (imageKey.equals(this.imageKey)) {
            return;
        }

        this.imageKey = imageKey;
        reloadImage();
    }

    private void reloadImage() {
        isStable = false;
        image = (H5EImage) resourceManager.getImage(imageKey);
    }

    /**
     * Returns the image being used for this sprite type. If the image does not exist or is not loaded, this method will
     * return null.
     *
     * @return The H5EImage currently being used for this sprite type's image data
     */
    @Override
    public H5EImage getImage() {
        if (image == null) {
            reloadImage();
        }

        return image;
    }

    @Override
    public String getImageKey() {
        return imageKey;
    }

    @Override
    public TextureRegion getGraphicData() {
        H5EImage img = getImage();
        if (img == null) {
            return null;
        }
        if (region == null) {
            TextureRegion imageRegion = img.getTextureRegion();
            if (areaX == 0 && areaY == 0 && (areaWidth == image.getWidth() || areaWidth == 0) && (areaHeight == image.getHeight() || areaHeight == 0)) {
                region = imageRegion;
            } else {
                region = new TextureRegion(imageRegion, areaX, areaY, areaWidth, areaHeight);
            }

        }
        return region;
    }

    public DownloadingSprite.DownloadState getDownloadState() {
        final TextureRegion graphicData = getGraphicData();
        if(graphicData instanceof DownloadingSprite) {
           return ((DownloadingSprite) graphicData).getDownloadState();
        }
        return DownloadingSprite.DownloadState.LOADED;
    }

    public boolean isLoading() {
        final TextureRegion graphicData = getGraphicData();
        if(graphicData instanceof DownloadingSprite) {
            return ((DownloadingSprite) graphicData).isLoading();
        }
        return false;
    }

    public void subscribeToLoadStateChange(Callable1Args<DownloadingSprite.DownloadState> callable) {
        final TextureRegion graphicData = getGraphicData();
        if(graphicData == null) {
            return;
        }
        if(graphicData instanceof DownloadingSprite) {
            ((DownloadingSprite) graphicData).subscribeToLoadStateChange(callable);
        } else {
            callable.call(DownloadingSprite.DownloadState.LOADED);
        }
    }

    public TextureRegion getTextureRegion() {
        if (region!=null)
            return region;
        else
            return getImage().getTextureRegion();
    }

    private void updateRegion() {
        if (region != null) {
            region.setRegion(getAreaX(), getAreaY(), getAreaWidth(), getAreaHeight());
        }
    }

    @Override
    public void setAreaHeight(int areaHeight) {
        super.setAreaHeight(areaHeight);
        updateRegion();
    }


    @Override
    public void setAreaWidth(int areaWidth) {
        super.setAreaWidth(areaWidth);
        updateRegion();
    }

    @Override
    public void setAreaX(int areaX) {
        super.setAreaX(areaX);
        updateRegion();
    }

    @Override
    public void setAreaY(int areaY) {
        super.setAreaY(areaY);
        updateRegion();
    }

    @Override
    public int getAreaHeight() {
        int value = super.getAreaHeight();
        if(value == 0 && image != null) {
            return image.getHeight();
        } else {
            return value;
        }
    }

    @Override
    public int getAreaWidth() {
        int value = super.getAreaWidth();
        if(value == 0 && image != null) {
            return image.getWidth();
        } else {
            return value;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        H5ESpriteType that = (H5ESpriteType) o;
        return Objects.equals(spriteTypeKey, that.spriteTypeKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(spriteTypeKey);
    }
}
