package com.universeprojects.html5engine.client.framework.inputs;

@SuppressWarnings("unused")
public class H5ECommand1Args<A1> extends H5ECommand {
    @Override
    public int getNumArgs() {
        return 1;
    }
}
