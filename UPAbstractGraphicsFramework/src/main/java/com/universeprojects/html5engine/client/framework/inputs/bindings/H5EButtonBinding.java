package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator1Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlBinding;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EButtonBinding<C extends H5ECommand> extends H5EControlBinding<C> {

    public final Actor button;
    private final H5ECommandInputTranslator1Args<Boolean, C> translator;
    private final ClickListener listener;

    public H5EButtonBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator1Args<Boolean, C> translator, C command, Actor button, ClickListener listener) {
        super(controlSetup, command);
        this.button = button;
        this.translator = translator;
        this.listener = listener;
    }

    public void fire(boolean on) {
        H5ECommandParams<C> params = translator.generateParams(command, on);
        fire(params);
    }

    public void clear() {
        button.removeListener(listener);
    }
}
