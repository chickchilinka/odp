package com.universeprojects.html5engine.client.framework.inputs;

public class H5ECommandParams2Args<C extends H5ECommand2Args<A1, A2>, A1, A2> extends H5ECommandParams<C> {
    public H5ECommandParams2Args(A1 arg1, A2 arg2) {
        super(arg1, arg2);
    }
}
