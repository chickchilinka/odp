package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.viewport.Viewport;

public class UPViewport<T extends Viewport> extends Viewport {
    private final static float CULLING_COMPENSATION_VERTICAL = 200;
    private final static float CULLING_COMPENSATION_HORIZONTAL = 0;
    private static final Vector2 tmp = new Vector2();

    private final T viewport;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private final Rectangle cullingArea = new Rectangle();

    /**
     * Distance from the center of the screen to any corner
     * of the screen in world units. Used to compute the "visability circle".
     */
    private float viewportRadius = 0;

    public UPViewport(T viewport) {
        this.viewport = viewport;
    }

    public T getViewport() {
        return viewport;
    }

    public void apply() {
        viewport.apply();
    }

    public void apply(boolean centerCamera) {
        viewport.apply(centerCamera);
    }

    public void update(int screenWidth, int screenHeight, boolean centerCamera) {
        viewport.update(screenWidth, screenHeight, centerCamera);
        if (centerCamera) {
            onCameraZoomUpdate();
            onCameraPositionRotationUpdate();
        }
    }

    public Vector2 unproject(Vector2 screenCoords) {
        return viewport.unproject(screenCoords);
    }

    public Vector2 project(Vector2 worldCoords) {
        return viewport.project(worldCoords);
    }

    public Vector3 unproject(Vector3 screenCoords) {
        return viewport.unproject(screenCoords);
    }

    public Vector3 project(Vector3 worldCoords) {
        return viewport.project(worldCoords);
    }

    public Ray getPickRay(float screenX, float screenY) {
        return viewport.getPickRay(screenX, screenY);
    }

    public void calculateScissors(Matrix4 batchTransform, Rectangle area, Rectangle scissor) {
        viewport.calculateScissors(batchTransform, area, scissor);
    }

    public Vector2 toScreenCoordinates(Vector2 worldCoords, Matrix4 transformMatrix) {
        return viewport.toScreenCoordinates(worldCoords, transformMatrix);
    }

    public Camera getCamera() {
        return viewport.getCamera();
    }

    public void setCamera(Camera camera) {
        viewport.setCamera(camera);
        onCameraZoomUpdate();
        onCameraPositionRotationUpdate();
    }

    public float getWorldWidth() {
        return viewport.getWorldWidth();
    }

    public void setWorldWidth(float worldWidth) {
        viewport.setWorldWidth(worldWidth);
    }

    public float getWorldHeight() {
        return viewport.getWorldHeight();
    }

    public void setWorldHeight(float worldHeight) {
        viewport.setWorldHeight(worldHeight);
    }

    public void setWorldSize(float worldWidth, float worldHeight) {
        viewport.setWorldSize(worldWidth, worldHeight);
    }

    public int getScreenX() {
        return viewport.getScreenX();
    }

    public void setScreenX(int screenX) {
        viewport.setScreenX(screenX);
    }

    public int getScreenY() {
        return viewport.getScreenY();
    }

    public void setScreenY(int screenY) {
        viewport.setScreenY(screenY);
    }

    public int getScreenWidth() {
        return viewport.getScreenWidth();
    }

    public void setScreenWidth(int screenWidth) {
        viewport.setScreenWidth(screenWidth);
    }

    public int getScreenHeight() {
        return viewport.getScreenHeight();
    }

    public void setScreenHeight(int screenHeight) {
        viewport.setScreenHeight(screenHeight);
    }

    public void setScreenPosition(int screenX, int screenY) {
        viewport.setScreenPosition(screenX, screenY);
    }

    public void setScreenSize(int screenWidth, int screenHeight) {
        viewport.setScreenSize(screenWidth, screenHeight);
    }

    public void setScreenBounds(int screenX, int screenY, int screenWidth, int screenHeight) {
        viewport.setScreenBounds(screenX, screenY, screenWidth, screenHeight);
    }

    public int getLeftGutterWidth() {
        return viewport.getLeftGutterWidth();
    }

    public int getRightGutterX() {
        return viewport.getRightGutterX();
    }

    public int getRightGutterWidth() {
        return viewport.getRightGutterWidth();
    }

    public int getBottomGutterHeight() {
        return viewport.getBottomGutterHeight();
    }

    public int getTopGutterY() {
        return viewport.getTopGutterY();
    }

    public int getTopGutterHeight() {
        return viewport.getTopGutterHeight();
    }

    public SpriteBatch getSpriteBatch() {
        return spriteBatch;
    }

    public Rectangle getCullingArea() {
        return cullingArea;
    }

    public void onCameraZoomUpdate() {
        tmp.set(0, 0);
        unproject(tmp);
        viewportRadius = tmp.dst(getCamera().position.x, getCamera().position.y);
    }

    public void onCameraPositionRotationUpdate() {
        Vector3 cameraPosition = getCamera().position;

        // Culling area is defined as a square around the "visability circle"
        getCullingArea().set(cameraPosition.x - viewportRadius - CULLING_COMPENSATION_HORIZONTAL,
                cameraPosition.y - viewportRadius - CULLING_COMPENSATION_VERTICAL,
                viewportRadius * 2 + CULLING_COMPENSATION_HORIZONTAL * 2,
                viewportRadius * 2 + CULLING_COMPENSATION_VERTICAL * 2);

        Camera camera = viewport.getCamera();
        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);
    }
}
