package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public abstract class H5EAction extends Action {
    private boolean active = true;

    public <T extends Actor & GraphicElement> H5EAction(T element)
    {
        element.addAction(this);
        setActor(element);
    }


    protected void setActive(boolean newVal) {
        if (newVal == active) {
            return;
        }
        active = newVal;
    }

    public boolean isActive() {
        return active;
    }


    public float getElementTransparency() {
        Color color = getActor().getColor();
        return color != null ? color.a : 1f;
    }

    public void setElementTransparency(float value) {
        Color color = getActor().getColor();
        if(color == null) {
            color = new Color(Color.WHITE);
            getActor().setColor(color);
        }
        color.a = value;
    }

    public void activate() {
        setActive(true);
    }

    public void deactivate() {
        setActive(false);
    }

}
