package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;

@SuppressWarnings("unused")
public class BlendingMode {
    public static final BlendingMode COPY = new BlendingMode("COPY", GL20.GL_ONE, GL20.GL_ZERO);
    //    public static final BlendingMode COLOR = new BlendingMode();
//    public static final BlendingMode COLOR_BURN = new BlendingMode();
//    public static final BlendingMode COLOR_DODGE = new BlendingMode();
//    public static final BlendingMode DARKEN = new BlendingMode();
    public static final BlendingMode DIFFERENCE = new BlendingMode("DIFFERENCE", GL20.GL_ZERO, GL20.GL_ONE);
    //    public static final BlendingMode EXCLUSION = new BlendingMode();
//    public static final BlendingMode HARD_LIGHT = new BlendingMode();
//    public static final BlendingMode HUE = new BlendingMode();
    public static final BlendingMode SOURCE_IN = new BlendingMode("SOURCE_IN", GL20.GL_DST_ALPHA, GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_ZERO, GL20.GL_ONE);
    public static final BlendingMode SOURCE_OUT = new BlendingMode("SOURCE_OUT", GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_ZERO);
    public static final BlendingMode SOURCE_ATOP = new BlendingMode("SOURCE_ATOP", GL20.GL_DST_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    public static final BlendingMode SOURCE_OVER = new BlendingMode("SOURCE_OVER", GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    public static final BlendingMode DESTINATION_IN = new BlendingMode("DESTINATION_IN", GL20.GL_ZERO, GL20.GL_SRC_ALPHA);
    public static final BlendingMode DESTINATION_OUT = new BlendingMode("DESTINATION_OUT", GL20.GL_ZERO, GL20.GL_ONE_MINUS_DST_ALPHA);
    public static final BlendingMode DESTINATION_ATOP = new BlendingMode("DESTINATION_ATOP", GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_SRC_ALPHA);
    public static final BlendingMode DESTINATION_OVER = new BlendingMode("DESTINATION_OVER", GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_ONE);
    public static final BlendingMode LIGHTER = new BlendingMode("LIGHTER", GL20.GL_SRC_ALPHA, GL20.GL_ONE);
    public static final BlendingMode LIGHTEN = new BlendingMode("LIGHTEN", GL20.GL_SRC_COLOR, GL20.GL_ONE);
    public static final BlendingMode EXPERIMENT = new BlendingMode("EXPERIMENT", GL20.GL_ONE_MINUS_DST_COLOR, GL20.GL_ONE);
    //    public static final BlendingMode LUMINOSITY = new BlendingMode();
    public static final BlendingMode MULTIPLY = new BlendingMode("MULTIPLY", GL20.GL_DST_COLOR, GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    //    public static final BlendingMode OVERLAY = new BlendingMode();
//    public static final BlendingMode SATURATION =y new BlendingMode();
    public static final BlendingMode SCREEN = new BlendingMode("SCREEN", GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_COLOR);

    public static final BlendingMode MAPPED_ALPHA = new BlendingMode("MAPPED_ALPHA", GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_ONE, GL20.GL_ZERO);

    public static final BlendingMode XOR = new BlendingMode("XOR", GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

    public static final BlendingMode DEFAULT = SOURCE_OVER;
    public static final BlendingMode TO_FRAMEBUFFER = new BlendingMode("TO_FRAMEBUFFER", GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);
    public static final BlendingMode FRAMEBUFFER_TO_SCREEN = new BlendingMode("FB_TO_SCREEN", GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);


//    public static final BlendingMode SOFT_LIGHT = new BlendingMode();
    private final int eqCol;
    private final int eqAl;
    private final int srcCol;
    private final int dstCol;
    private final int srcAl;
    private final int dstAl;
    private final String name;

    public BlendingMode(int eqCol, int eqAl, int srcCol, int dstCol, int srcAl, int dstAl) {
        this(null, eqCol, eqAl, srcCol, dstCol, srcAl, dstAl);
    }

    public BlendingMode(String name, int eqCol, int eqAl, int srcCol, int dstCol, int srcAl, int dstAl) {
        this.eqCol = eqCol;
        this.eqAl = eqAl;
        this.srcCol = srcCol;
        this.dstCol = dstCol;
        this.srcAl = srcAl;
        this.dstAl = dstAl;
        this.name = name;
    }

    public BlendingMode(int src, int dst) {
        this(null, src, dst, src, dst);
    }

    public BlendingMode(String name, int src, int dst) {
        this(name, src, dst, src, dst);
    }

    public BlendingMode(int srcCol, int dstCol, int srcAl, int dstAl) {
        this(null, GL20.GL_FUNC_ADD, GL20.GL_FUNC_ADD, srcCol, dstCol, srcAl, dstAl);
    }

    public BlendingMode(String name, int srcCol, int dstCol, int srcAl, int dstAl) {
        this(name, GL20.GL_FUNC_ADD, GL20.GL_FUNC_ADD, srcCol, dstCol, srcAl, dstAl);
    }

    public static BlendingMode fromBatch(Batch batch) {
        int srcCol = batch.getBlendSrcFunc();
        int dstCol = batch.getBlendDstFunc();
        int srcAl = batch.getBlendSrcFuncAlpha();
        int dstAl = batch.getBlendDstFuncAlpha();
        return new BlendingMode(srcCol, dstCol, srcAl, dstAl);
    }

    public static BlendingMode valueOf(String text) {
        switch (text) {
            case "DEFAULT":
                return DEFAULT;
            case "COPY":
                return COPY;
            case "DIFFERENCE":
                return DIFFERENCE;
            case "SOURCE_IN":
                return SOURCE_IN;
            case "SOURCE_OUT":
                return SOURCE_OUT;
            case "SOURCE_ATOP":
                return SOURCE_ATOP;
            case "SOURCE_OVER":
                return SOURCE_OVER;
            case "DESTINATION_IN":
                return DESTINATION_IN;
            case "DESTINATION_OUT":
                return DESTINATION_OUT;
            case "DESTINATION_ATOP":
                return DESTINATION_ATOP;
            case "DESTINATION_OVER":
                return DESTINATION_OVER;
            case "LIGHTER":
                return LIGHTER;
            case "LIGHTEN":
                return LIGHTEN;
            case "SCREEN":
                return SCREEN;
            case "MULTIPLY":
                return MULTIPLY;
            case "EXPERIMENT":
                return EXPERIMENT;
            case "XOR":
                return XOR;
            case "MAPPED_ALPHA":
                return MAPPED_ALPHA;
        }
        throw new IllegalArgumentException("Unknown mode: " + text);
    }

    public boolean isSameAsBatch(Batch batch) {
        int srcCol = batch.getBlendSrcFunc();
        int dstCol = batch.getBlendDstFunc();
        int srcAl = batch.getBlendSrcFuncAlpha();
        int dstAl = batch.getBlendDstFuncAlpha();
        return srcCol == this.srcCol && dstCol == this.dstCol && srcAl == this.srcAl && dstAl == this.dstAl;
    }

    public int getSrcCol() {
        return srcCol;
    }

    public int getDstCol() {
        return dstCol;
    }

    public int getSrcAl() {
        return srcAl;
    }

    public int getDstAl() {
        return dstAl;
    }

    public void applyToBatch(Batch batch) {
        batch.setBlendFunctionSeparate(srcCol, dstCol, srcAl, dstAl);
    }

    public void ifNotEqualsAssignTo(Batch batch) {
        if (!isSameAsBatch(batch)) {
            applyToBatch(batch);
        }
    }

    public void applyEquationToGdx() {
        Gdx.gl.glBlendEquationSeparate(eqCol, eqAl);
    }

    public void applyToGdx() {
        applyEquationToGdx();
        Gdx.gl.glBlendFuncSeparate(srcCol, dstCol, srcAl, dstAl);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        if (name != null) {
            return name;
        }
        return "eqCol: " + Integer.toHexString(eqCol) + ", " +
            "eqAl: " + Integer.toHexString(eqAl) + ", " +
            "srcCol: " + Integer.toHexString(srcCol) + ", " +
            "dstCol: " + Integer.toHexString(dstCol) + ", " +
            "srcAl: " + Integer.toHexString(srcAl) + ", " +
            "dstAl: " + Integer.toHexString(dstAl);
    }
}
