package com.universeprojects.html5engine.client.framework.inputs;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class H5EButtonInvalidateListener extends InputListener {
    private final Actor actor;

    public H5EButtonInvalidateListener(Button actor) {
        this.actor = actor;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        H5ELayer.invalidateBufferForActor(actor);
        return true;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        H5ELayer.invalidateBufferForActor(actor);
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        H5ELayer.invalidateBufferForActor(actor);
    }

    @Override
    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
        H5ELayer.invalidateBufferForActor(actor);
    }

    @Override
    public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        H5ELayer.invalidateBufferForActor(actor);
    }
}
