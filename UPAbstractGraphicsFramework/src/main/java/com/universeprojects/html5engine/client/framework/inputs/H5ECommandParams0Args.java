package com.universeprojects.html5engine.client.framework.inputs;

public class H5ECommandParams0Args<C extends H5ECommand0Args> extends H5ECommandParams<C> {

    private static final H5ECommandParams0Args instance = new H5ECommandParams0Args<>();

    @SuppressWarnings("unchecked")
    public static <T extends H5ECommand0Args> H5ECommandParams0Args<T> getInstance() {
        return instance;
    }


    public H5ECommandParams0Args() {
        super();
    }
}
