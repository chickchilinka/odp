package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator2Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EGamepadStickSeparateAxisBinding<C extends H5ECommand> extends H5EGamepadBinding<C> {

    public final int axisCode;
    public final int inputParameter;
    public final float factor;
    public float previousValue = 0f;
    private final H5ECommandInputTranslator2Args<Boolean, Float, C> translator;

    public H5EGamepadStickSeparateAxisBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator2Args<Boolean, Float, C> translator, C command,
                                              int gamepadIndex, int axisCode, int inputParameter, float factor) {
        super(controlSetup, command, gamepadIndex);
        this.translator = translator;
        this.axisCode = axisCode;
        this.inputParameter = inputParameter;
        this.factor = factor;
    }

    @Override
    public void doTickWhenActive() {
        float value = gamePad.getAxisWithDeadzone(axisCode);
        if (previousValue == value) return;

        H5ECommandParams<C> inp = null;
        if (value != 0d) {
            inp = translator.generateParams(command, true, value * factor);
        } else {// if (otherAxis.isActive()) {
            inp = translator.generateParams(command, false, 0f);
        }
        fire(inp);
        previousValue = value;
    }
}
