package com.universeprojects.html5engine.client.framework.inputs;

public abstract class H5ECommand {

    public H5ECommand() {
    }

    public abstract int getNumArgs();
}
