package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

public class GdxUtils {
    private static boolean mobileWeb = false;

    public static void setup(boolean mobileWeb) {
        GdxUtils.mobileWeb = mobileWeb;
    }

    public static Application.ApplicationType getType() {
        if(Gdx.app == null) {
            return Application.ApplicationType.HeadlessDesktop;
        } else {
            return Gdx.app.getType();
        }
    }

    public static float getDensity() {
        if(Gdx.graphics == null) {
            return 1f;
        } else {
            return Gdx.graphics.getDensity();
        }
    }

    public static boolean isType(Application.ApplicationType... types) {
        Application.ApplicationType actualType = getType();
        for (Application.ApplicationType type : types) {
            if(actualType == type) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMobileWeb() {
        return mobileWeb;
    }
}
