package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.drawable.ShapeDrawable;

public class H5ERectangle extends H5EContainer {

    /**
     * The radius to use for rounded corners (in pixels). If value is 0, the corners will not be rounded.
     */
    private int roundedCornerRadiusTL = 0;
    private int roundedCornerRadiusTR = 0;
    private int roundedCornerRadiusBR = 0;
    private int roundedCornerRadiusBL = 0;

    private Color fillColor;
    private Color strokeColor;
    private Integer strokeWidth;

    public H5ERectangle(H5ELayer layer) {
        super(layer);
        background(new ShapeDrawable(getColor(), ShapeRenderer.ShapeType.Filled) {
            @Override
            protected void drawShape(ShapeRenderer shapeRenderer, float x, float y, float width, float height) {
                if (fillColor != null) {
                    shapeRenderer.setColor(fillColor);
                    shapeRenderer.rect(x, y, width, height);
                }
                if (strokeColor != null && strokeWidth != null) {
                    shapeRenderer.end();
                    shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
                    shapeRenderer.setColor(strokeColor);
                    for (int i = 0; i < strokeWidth; i++) {
                        drawRect(shapeRenderer, x + i, y + i, width - 2 * i, height - 2 * i);
                    }
                    shapeRenderer.end();
                    shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                }
            }

            private void rotate(Vector2 point, float originX, float originY, float angle) {
                angle = angle * (float)UPMath.PI / 180f;

                point.set(
                        UPMath.cos(angle) * (point.x-originX) - UPMath.sin(angle) * (point.y-originY) + originX,
                        UPMath.sin(angle) * (point.x - originX) + UPMath.cosDeg(angle) * (point.y - originY) + originY);
            }

            private Vector2 point = new Vector2();
            private Vector2 point2 = new Vector2();

            private void drawRect(ShapeRenderer shapeRenderer, float x, float y, float width, float height) {
                float cornerTLX = x + roundedCornerRadiusTL;
                float cornerTLY = y + height - roundedCornerRadiusTL;
                float cornerTRX = x + width - roundedCornerRadiusTR;
                float cornerTRY = y + height - roundedCornerRadiusTR;
                float cornerBLX = x + roundedCornerRadiusBL;
                float cornerBLY = y + roundedCornerRadiusBL;
                float cornerBRX = x + width - roundedCornerRadiusBR;
                float cornerBRY = y + roundedCornerRadiusBR;
                float rotation = getRotation();

                rotate(point.set(cornerBLX, y), x, y, rotation);
                rotate(point2.set(cornerBRX, y), x, y, rotation);

                shapeRenderer.line(point.x, point.y, point2.x, point2.y);

                rotate(point.set(cornerTLX, y + height), x, y, rotation);
                rotate(point2.set(cornerTRX, y+ height), x, y, rotation);

                shapeRenderer.line(point.x, point.y, point2.x, point2.y);

                rotate(point.set(x, cornerBLY), x, y, rotation);
                rotate(point2.set(x, cornerTLY), x, y, rotation);

                shapeRenderer.line(point.x, point.y, point2.x, point2.y);

                rotate(point.set(x+width, cornerBRY), x, y, rotation);
                rotate(point2.set(x+width, cornerTRY), x, y, rotation);

                shapeRenderer.line(point.x, point.y, point2.x, point2.y);

//                shapeRenderer.rect(cornerBLX, cornerBLY, cornerTRX - cornerBLX, cornerTRY - cornerBLY);
                if (roundedCornerRadiusTL > 0) {
                    shapeRenderer.arc(cornerTLX, cornerTLY, roundedCornerRadiusTL, 90, 90);
                }
                if (roundedCornerRadiusTR > 0) {
                    shapeRenderer.arc(cornerTRX, cornerTRY, roundedCornerRadiusTR, 0, 90);
                }
                if (roundedCornerRadiusBL > 0) {
                    shapeRenderer.arc(cornerBLX, cornerBLY, roundedCornerRadiusBL, 180, 90);
                }
                if (roundedCornerRadiusBR > 0) {
                    shapeRenderer.arc(cornerBRX, cornerBRY, roundedCornerRadiusBR, -90, 90);
                }
            }
        });
    }

    public void setStrokeColor(Color strokeColor) {
        this.strokeColor = strokeColor;
    }

    public void setStrokeWidth(Integer strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    /**
     * @return The radius of rounded corners (in pixels)
     */
    public int getRoundedCornerTopLeftRadius() {
        return roundedCornerRadiusTL;
    }

    /**
     * @return The radius of rounded corners (in pixels)
     */
    public int getRoundedCornerTopRightRadius() {
        return roundedCornerRadiusTR;
    }

    /**
     * @return The radius of rounded corners (in pixels)
     */
    public int getRoundedCornerBottomRightRadius() {
        return roundedCornerRadiusBR;
    }

    /**
     * @return The radius of rounded corners (in pixels)
     */
    public int getRoundedCornerBottomLeftRadius() {
        return roundedCornerRadiusBL;
    }

    /**
     * Sets the radius of rounded corners, in pixels (a value of 0 or greater)
     */
    public void setRoundedCornerRadius(int val) {
        setRoundedCornerRadius(val, val, val, val);
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    /**
     * Sets the radius of rounded corners, in pixels (a value of 0 or greater)
     */
    public void setRoundedCornerRadius(int newTL, int newTR, int newBR, int newBL) {
        newTL = UPMath.max(newTL, 0);
        newTR = UPMath.max(newTR, 0);
        newBR = UPMath.max(newBR, 0);
        newBL = UPMath.max(newBL, 0);
        if (newTL == roundedCornerRadiusTL && newTR == roundedCornerRadiusTR && newBR == roundedCornerRadiusBR && newBL == roundedCornerRadiusBL) {
            return;
        }
        //TODO: change arc forumula above. currently draws extra lines. maybe use https://stackoverflow.com/questions/30699321/libgdx-drawing-arc-curve
//        roundedCornerRadiusTL = newTL;
//        roundedCornerRadiusTR = newTR;
//        roundedCornerRadiusBR = newBR;
//        roundedCornerRadiusBL = newBL;
    }

}
