package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.universeprojects.html5engine.shared.abstractFramework.Container;

import java.util.List;
import java.util.ArrayList;

/**
 * This element has no rendering of its own, it is solely for the purpose of grouping other elements
 */
public class H5EStack extends Stack implements Container {

    public H5EStack() {
    }

    public H5EStack(Actor... actors) {
        super(actors);
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }
}
