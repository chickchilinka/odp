package com.universeprojects.html5engine.client.framework;

public class H5ESimpleAnimationBehaviour extends H5EBehaviour {
    //Operating variables...
    /**
     * An internal variable keeping track of the current X speed
     */
    private float speedX = 0;
    /**
     * An internal variable keeping track of the current Y speed
     */
    private float speedY = 0;
    /**
     * An internal variable keeping track of the current rotation speed
     */
    private float speedRotation = 0;
    /**
     * An internal variable keeping track of the current scale speed
     */
    private float speedScale = 0;

    public <T extends H5EGraphicElement> H5ESimpleAnimationBehaviour(T graphicElement) {
        super(graphicElement);
    }

    public void setTranslateSpeed(Float xPerSecond, Float yPerSecond) {
        speedX = xPerSecond;
        speedY = yPerSecond;
    }

    public Float getTranslateSpeedX() {
        return speedX;
    }

    public Float getTranslateSpeedY() {
        return speedY;
    }


    public void setRotationSpeed(Float newRotPerSecond) {
        speedRotation = newRotPerSecond;
    }

    public Float getRotationSpeed() {
        return speedRotation;
    }


    public void setScaleSpeed(Float newScalePerSecond) {
        speedScale = newScalePerSecond;
    }

    public Float getScaleSpeed() {
        return speedScale;
    }

    @Override
    public void act(float delta) {
        if (speedX != 0) {
            element.setX(element.getX() + speedX * delta);
        }
        if (speedY != 0) {
            element.setY(element.getY() + speedY * delta);
        }

        if (element instanceof H5ESprite)
        {
            H5ESprite sprite = (H5ESprite) element;

            if (speedRotation != 0) {
                sprite.setRotation(sprite.getRotation() + speedRotation * delta);
            }
            if (speedScale != 0) {
                sprite.setScale(sprite.getScaleX() + speedScale * delta, sprite.getScaleY() + speedScale * delta);
            }
        }
    }

    @Override
    public String getName() {
        return "ConstantMotion";
    }


}
