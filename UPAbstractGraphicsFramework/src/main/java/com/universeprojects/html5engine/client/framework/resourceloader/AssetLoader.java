package com.universeprojects.html5engine.client.framework.resourceloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.net.HttpStatus;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.html5engine.client.framework.GdxRunnableHelper;

public abstract class AssetLoader {
    public static AssetLoader getInstance() {
        return INSTANCE;
    }

    protected static AssetLoader INSTANCE = new AssetLoader() {
        @Override
        public void downloadPixmap(String url, Callable1Args<Pixmap> successCallback, Callable1Args<Throwable> errorCallback) {
            errorCallback.call(new IllegalStateException("Not supported"));
        }

        @Override
        public void downloadSound(String url, Callable1Args<Sound> successCallback, Callable1Args<Throwable> errorCallback) {
            errorCallback.call(new IllegalStateException("Not supported"));
        }

        @Override
        public void downloadMusic(String url, Callable1Args<Music> successCallback, Callable1Args<Throwable> errorCallback) {
            errorCallback.call(new IllegalStateException("Not supported"));
        }
    };

    public abstract void downloadPixmap(String url, Callable1Args<Pixmap> successCallback, Callable1Args<Throwable> errorCallback);

    public abstract void downloadSound(String url, Callable1Args<Sound> successCallback, Callable1Args<Throwable> errorCallback);

    public abstract void downloadMusic(String url, Callable1Args<Music> successCallback, Callable1Args<Throwable> errorCallback);

    public void downloadFileAsString(String url, Callable1Args<String> successCallback, Callable1Args<Throwable> errorCallback) {
        Net.HttpRequest request = new Net.HttpRequest("GET");
        request.setUrl(url);
        request.setFollowRedirects(true);
        Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                final int statusCode = httpResponse.getStatus().getStatusCode();
                if(statusCode != HttpStatus.SC_OK) {
                    errorCallback.call(new IllegalStateException("Bad status-code: "+statusCode));
                    return;
                }
                final String result = httpResponse.getResultAsString();

                GdxRunnableHelper.run(() -> successCallback.call(result));
            }

            @Override
            public void failed(Throwable t) {
                GdxRunnableHelper.run(() -> errorCallback.call(t));
            }

            @Override
            public void cancelled() {
            }
        });
    }
}
