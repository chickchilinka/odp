package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.Collection;
import java.util.LinkedHashMap;


@SuppressWarnings("unused")
public class H5EGraphicElement extends Actor implements GraphicElement {
    private final H5EEngine engine;
    LinkedHashMap<Class<? extends H5EBehaviour>, H5EBehaviour> behaviours = null;

    public H5EGraphicElement(H5ELayer layer) {
        engine = layer.getEngine();
        setStage(layer);
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return engine;
    }

    /**
     * Effectively deletes the graphic element.
     * Destroys all behaviours.
     */
    public boolean remove() {
        if (hasBehaviours()) {
            for (Class behaviourClass : behaviours.keySet()) {
                H5EBehaviour behaviour = behaviours.get(behaviourClass);
                if (behaviour != null) {
                    behaviour.deactivate();
                    this.getEngine().removeBehaviour(behaviour);
                    behaviour.onDestroy();
                }
            }
            behaviours.clear();
        }
        return super.remove();
    }

    public void addTo(H5ELayer layer, Integer level) {
        layer.addActorToTop(this, level);
    }

    public void addTo(H5ELayer layer) {
        layer.addActorToTop(this);
    }

    public void addBehaviour(H5EBehaviour behaviour) {
        if (behaviour == null) return;
        if (behaviours == null)
            behaviours = new LinkedHashMap<>();
        else if (behaviours.containsKey(behaviour.getClass()))
            throw new IllegalStateException("Attempted to add " + behaviour.getClass().getSimpleName() + " more than once to the same element.");

        behaviours.put(behaviour.getClass(), behaviour);
        engine.addBehaviour(behaviour);
    }

    public void removeBehaviour(H5EBehaviour behaviour) {
        if (behaviour == null) return;
        if (behaviours == null) return;

        behaviours.remove(behaviour.getClass());
        engine.removeBehaviour(behaviour);
    }


    public <T extends H5EBehaviour> T getBehaviour(Class<T> behaviourClass) {
        if (behaviours == null) return null;
        return (T) behaviours.get(behaviourClass);
    }

    public Collection<H5EBehaviour> getBehaviours() {
        if (behaviours == null) return null;
        return behaviours.values();
    }

    public boolean hasBehaviours() {
        if (behaviours == null) return false;
        return behaviours.isEmpty() == false;
    }

}
