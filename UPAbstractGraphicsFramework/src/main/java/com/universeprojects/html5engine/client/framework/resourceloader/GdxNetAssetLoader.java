package com.universeprojects.html5engine.client.framework.resourceloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.common.shared.math.Base58;
import com.universeprojects.common.shared.util.GwtIncompatible;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@GwtIncompatible
public class GdxNetAssetLoader extends AssetLoader {

    public static void initialize() {
        if (AssetLoader.INSTANCE instanceof GdxNetAssetLoader) {
            return;
        }
        AssetLoader.INSTANCE = new GdxNetAssetLoader();
    }

    private final List<String> validImageContentTypes = Arrays.asList("image/jpeg", "image/png");

    protected final Map<String, String> contentTypeToExtensionMap = new LinkedHashMap<>();
    {
        contentTypeToExtensionMap.put("audio/mpeg", "mp3");
        contentTypeToExtensionMap.put("audio/x-wav", "wav");
        contentTypeToExtensionMap.put("audio/wav", "wav");
        contentTypeToExtensionMap.put("audio/ogg", "ogg");
        contentTypeToExtensionMap.put("audio/mp3", "mp3");
    }

    private final HttpRequestBuilder requestBuilder = new HttpRequestBuilder();

    @Override
    public void downloadPixmap(String url, Callable1Args<Pixmap> successCallback, Callable1Args<Throwable> errorCallback) {
        download(url, validImageContentTypes, (contentType, response) -> {
            final byte[] bytes = response.getResult();
            Gdx.app.postRunnable(() -> {
                final Pixmap pixmap = new Pixmap(bytes, 0, bytes.length);
                try {
                    successCallback.call(pixmap);
                } finally {
                    pixmap.dispose();
                }
            });
        }, errorCallback);
    }

    @Override
    public void downloadSound(String url, Callable1Args<Sound> successCallback, Callable1Args<Throwable> errorCallback) {
        downloadSoundToCache(url, (handle) -> {
            final Sound sound = Gdx.audio.newSound(handle);
            successCallback.call(sound);
        }, errorCallback);
    }

    @Override
    public void downloadMusic(String url, Callable1Args<Music> successCallback, Callable1Args<Throwable> errorCallback) {
        downloadSoundToCache(url, (handle) -> {
            final Music music = Gdx.audio.newMusic(handle);
            successCallback.call(music);
        }, errorCallback);
    }

    private void downloadSoundToCache(String url, Callable1Args<FileHandle> successCallback, Callable1Args<Throwable> errorCallback) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            successCallback.call(Gdx.files.internal(url));
            return;
        }
        final String encodedUrl = Base58.encode(url.getBytes(StandardCharsets.UTF_8));
        for(String ext : contentTypeToExtensionMap.values()) {
            if(url.endsWith("."+ext)) {
                final FileHandle handle = Gdx.files.external("voidspace/audioCache/" + encodedUrl + "." + ext);
                if(handle.exists()) {
                    successCallback.call(handle);
                    return;
                }
            }
        }

        download(url, contentTypeToExtensionMap.keySet(), (contentType, response) -> {
            final String ext = contentTypeToExtensionMap.get(contentType);
            final FileHandle handle = Gdx.files.external("voidspace/audioCache/" + encodedUrl + "." + ext);
            handle.write(response.getResultAsStream(), false);
            Gdx.app.postRunnable(() -> successCallback.call(handle));
        }, errorCallback);
    }

    protected void download(String url, Collection<String> validContentTypes, Callable2Args<String, Net.HttpResponse> successCallback, Callable1Args<Throwable> errorCallback) {
        final Net.HttpRequest request = requestBuilder
            .newRequest()
            .followRedirects(false)
            .method(Net.HttpMethods.GET)
            .url(url)
            .build();

        try {
            Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {
                @Override
                public void handleHttpResponse(Net.HttpResponse httpResponse) {
                    final List<String> contentTypeHeaders = httpResponse.getHeaders().get("Content-Type");
                    String contentType = null;
                    boolean validContentType = contentTypeHeaders != null && contentTypeHeaders.size() == 1;
                    if (validContentType) {
                        contentType = contentTypeHeaders.get(0).split(";")[0];
                        validContentType = validContentTypes == null || validContentTypes.contains(contentType);
                    }
                    if (!validContentType) {
                        failed(new IllegalStateException("Bad content-type: " + contentType));
                        return;
                    }

                    final String loadedContentType = contentType;
                    successCallback.call(loadedContentType, httpResponse);
                }

                @Override
                public void failed(Throwable t) {
                    errorCallback.call(t);
                }

                @Override
                public void cancelled() {
                    failed(new IllegalStateException("Cancelled"));
                }
            });
        } catch (Throwable ex) {
            errorCallback.call(ex);
        }
    }
}
