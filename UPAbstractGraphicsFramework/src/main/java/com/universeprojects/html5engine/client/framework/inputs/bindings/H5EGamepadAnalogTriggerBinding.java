package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator2Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EGamepadAnalogTriggerBinding<C extends H5ECommand> extends H5EGamepadBinding<C> {

    public final int buttonCode;
    public final int inputParameter;
    private float lastValue = 0;
    private H5ECommandInputTranslator2Args<Boolean, Float, C> translator;

    public H5EGamepadAnalogTriggerBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator2Args<Boolean, Float, C> translator, C command, int gamepadIndex, int buttonCode, int inputParameter) {
        super(controlSetup, command, gamepadIndex);
        this.buttonCode = buttonCode;
        this.inputParameter = inputParameter;
        this.translator = translator;
    }

    @Override
    public void doTickWhenActive() {
        float value = gamePad.getAxis(buttonCode);
        if (UPMath.abs(value - lastValue) < 0.2) {
            return;
        }
        H5ECommandParams<C> params;
        if (value > gamePad.deadZone) {
            params = translator.generateParams(command, true, value);
        } else {
            params = translator.generateParams(command, false, 0f);
        }
        fire(params);
        lastValue = value;
    }
}
