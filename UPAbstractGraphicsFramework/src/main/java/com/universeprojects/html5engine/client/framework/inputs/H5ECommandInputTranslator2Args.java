package com.universeprojects.html5engine.client.framework.inputs;

public interface H5ECommandInputTranslator2Args<A1, A2, C extends H5ECommand> extends H5ECommandInputTranslator {
    H5ECommandParams<C> generateParams(C command, A1 arg1, A2 arg2);
}
