package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip;
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager;

public class H5ETextTooltip extends TextTooltip {
    public H5ETextTooltip(String text, Skin skin) {
        super(text, skin);
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        TooltipManager.getInstance().hideAll();
        mouseMoved(event, x, y);
        return super.touchDown(event, x, y, pointer, button);
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        TooltipManager.getInstance().hideAll();
        super.touchUp(event, x, y, pointer, button);
    }
}
