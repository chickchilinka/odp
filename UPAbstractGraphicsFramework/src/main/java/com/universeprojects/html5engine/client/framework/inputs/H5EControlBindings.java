package com.universeprojects.html5engine.client.framework.inputs;

import java.util.LinkedHashMap;
import java.util.Map;

class H5EControlBindings<K, B extends H5EControlBinding> {

    private Map<K, B> bindingsByKey;

    H5EControlBindings() {
    }

    final void clear() {
        bindingsByKey = null;
    }

    final boolean isEmpty() {
        return bindingsByKey == null || bindingsByKey.isEmpty();
    }

    final void add(K key, B binding) {
        if (bindingsByKey == null)
            bindingsByKey = new LinkedHashMap<>();

        bindingsByKey.put(key, binding);
    }

    final void remove(K key) {
        if (bindingsByKey == null)
            return;

        bindingsByKey.remove(key);
    }

    final void doTick() {
        if (bindingsByKey != null) {
            for (B binding : bindingsByKey.values()) {
                binding.doTick();
            }
        }
    }

    final void processFor(K key, BindingAction<B> action) {
        if (bindingsByKey != null && bindingsByKey.containsKey(key)) {
            action.apply(bindingsByKey.get(key));
        }
    }

    public interface BindingAction<B> {
        void apply(B binding);
    }

    public Map<K, B> getBindingsByKey() {
        return bindingsByKey;
    }
}
