package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StringBuilder;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class H5EClickableLabel extends Label implements GraphicElement {
    private final Set<TextRegion> regions = new HashSet<>();

    public H5EClickableLabel(H5ELayer layer) {
        this("", layer);
    }

    public H5EClickableLabel(String text, H5ELayer layer) {
        super(text, layer.getEngine().getSkin());
        setStage(layer);
        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                float height = getHeight();
                float lineHeight = getStyle().font.getLineHeight() * getFontScaleY();

                int characterOffset = 0;
                GlyphLayout.GlyphRun selectedRun = null;
                Array<GlyphLayout.GlyphRun> runs = getGlyphLayout().runs;

                outer:
                for (int i = 0; i < runs.size; i++) {
                    GlyphLayout.GlyphRun run = runs.get(i);
                    if (run.glyphs.size == 0)
                        continue;

                    // Glyph array doesn't contain new line characters.
                    // In the following block we make sure that "offset" variable points exactly
                    // to the start character of current run
                    int firstCharacterCodepoint = run.glyphs.get(0).id;
                    StringBuilder textStringBuilder = getText();
                    while (characterOffset < textStringBuilder.length() && textStringBuilder.codePointAt(characterOffset) != firstCharacterCodepoint) {
                        characterOffset++;
                        if(characterOffset >= textStringBuilder.length) {
                            continue outer;
                        }
                    }

                    if ((run.y + height) - lineHeight <= y && x >= run.x && run.x + run.width >= x) {
                        selectedRun = run;
                        break;
                    }

                    characterOffset += run.glyphs.size;
                }
                if (selectedRun != null) {
                    float textOffset = x - selectedRun.x;
                    int runOffset = 0;
                    while (runOffset + 1 < selectedRun.glyphs.size
                            && textOffset > selectedRun.glyphs.get(runOffset).width) {
                        textOffset -= selectedRun.xAdvances.get(runOffset);
                        runOffset++;
                    }
                    characterOffset += runOffset;
                    onCharacterClicked(characterOffset);
                    event.handle();
                }
            }
        });
    }

    public void append(String text) {
        getText().append(text);
        invalidateHierarchy();
    }

    public void append(String text, Runnable onClick) {
        int start = getText().length;
        int end = getText().length + text.length();
        regions.add(new TextRegion(start, end, onClick));
        append(text);
    }

    public void deleteFromStart(int count) {
        getText().delete(0, count + 1);    // If we have a newline, copy to 0 what's after newLinePosition+1
        invalidateHierarchy();
        // Move all regions to the left
        final int shiftBy = count + 1;
        Iterator<TextRegion> iterator = regions.iterator();
        while (iterator.hasNext()) {
            TextRegion region = iterator.next();
            region.start -= shiftBy;
            region.end -= shiftBy;

            if (region.end < 0) {
                iterator.remove();
            }
        }
    }

    protected void onCharacterClicked(int index) {
        for (TextRegion region : regions) {
            if (region.start <= index && region.end >= index) {
                Runnable callback = region.callback;
                if (callback != null) {
                    callback.run();
                }
            }
        }
    }

    @Override
    public void setText(CharSequence newText) {
        super.setText(newText);
        regions.clear();
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    private static final class TextRegion {
        private int start;
        private int end;
        private Runnable callback;

        private TextRegion(int start, int end, Runnable callback) {
            this.start = start;
            this.end = end;
            this.callback = callback;
        }
    }
}
