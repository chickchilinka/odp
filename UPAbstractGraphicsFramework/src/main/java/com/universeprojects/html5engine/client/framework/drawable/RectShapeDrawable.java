package com.universeprojects.html5engine.client.framework.drawable;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class RectShapeDrawable extends ShapeDrawable<RectShapeDrawable> {

    public RectShapeDrawable(Color color) {
        super(color);
    }

    protected void drawShape(ShapeRenderer shapeRenderer, float x, float y, float width, float height) {
        shapeRenderer.rect(x, y, width, height);
    }

}
