package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class H5EInputBox extends TextField implements GraphicElement {
    @Override
    public H5ELayer getLayer() {
        return (H5ELayer)getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void focus() {
        H5ELayer layer = getLayer();
        if (layer == null) {
            return;
        }
        layer.setKeyboardFocus(this);
    }

    public void unfocus() {
        H5ELayer layer = getLayer();
        if (layer == null) {
            return;
        }
        layer.setKeyboardFocus(getLayer().getRoot());
    }

    public boolean hasFocus() {
        H5ELayer layer = getLayer();
        if(layer == null) {
            return false;

        }
        return getLayer().getKeyboardFocus() == this;
    }

    public H5EInputBox(H5ELayer layer, String placeholder) {
        super("", layer.getEngine().getSkin());
        setMessageText(placeholder);
        setStage(layer);
    }

    public H5EInputBox(H5ELayer layer) {
        super("", layer.getEngine().getSkin());
        setStage(layer);
    }

    public void setTypeNumber() {
        setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
    }

    public void setTypeText() {
        setTextFieldFilter(null);
    }

    @Override
    protected InputListener createInputListener() {
        return new TextFieldClickListener(){
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                // We want to ignore when escape is pressed in a textbox
                if (keycode == Input.Keys.ESCAPE) return false;

                return super.keyDown(event, keycode);
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                // We want to ignore when escape is pressed in a textbox
                if (keycode == Input.Keys.ESCAPE) return false;

                return super.keyUp(event, keycode);
            }
        };
    }
}
