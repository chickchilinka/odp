package com.universeprojects.html5engine.client.framework.resourceloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.universeprojects.html5engine.client.framework.H5EAudio;
import com.universeprojects.html5engine.client.framework.H5EImage;

import java.util.Collection;

public class SimpleResourceLoader implements ResourceLoader {

    public static final String UNKNOWN_ICON_KEY = "images/icons/unknown-icon-2.png";

    @Override
    public void loadResources(Collection<H5EImage> imageFiles, Collection<H5EAudio> audioFiles) {
        for (H5EImage image : imageFiles) {
            final FileHandle fileHandle = Gdx.files.internal(image.getOriginalUrl());
            TextureRegion textureRegion = new TextureRegion(new Texture(fileHandle));
            image.setTexture(textureRegion);
        }
        for (H5EAudio audio : audioFiles) {
            Sound sound = Gdx.audio.newSound(Gdx.files.internal(audio.getURL()));
            audio.setSound(sound);
        }
    }

    @Override
    public H5EImage loadSingleImage(String imageUrl) {
        final FileHandle fileHandle = Gdx.files.internal(imageUrl);
        if(!fileHandle.exists()) {
            return null;
        }
        H5EImage image = new H5EImage(imageUrl);
        TextureRegion textureRegion = new TextureRegion(new Texture(fileHandle));
        image.setTexture(textureRegion);
        return image;
    }

    @Override
    public String getDefaultSpriteKey() {
        return UNKNOWN_ICON_KEY;
    }
}
