package com.universeprojects.html5engine.client.framework.resourceloader;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Pools;
import com.universeprojects.common.shared.hooks.Hooks;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.html5engine.client.framework.H5EImage;
import com.universeprojects.html5engine.client.framework.H5EResourceManager;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteType;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadingResourceLoader extends AtlasResourceLoader {
    private static final Pixmap.Format PIXMAP_FORMAT = Pixmap.Format.RGBA8888;
    private static final int TEXTURE_WIDTH = 4096;
    private static final int TEXTURE_HEIGHT = 4096;
    private static final boolean USE_MIP_MAPS = false;
    private static final Texture.TextureFilter TEXTURE_FILTER_MIN = Texture.TextureFilter.Linear;
    private static final Texture.TextureFilter TEXTURE_FILTER_MAG = Texture.TextureFilter.Linear;

    private final String baseUrl;
    private final boolean allowFullUrls;
    private final Logger log = Logger.getLogger(getClass());

    private final Map<String, DownloadingSprite> loadingSprites = new LinkedHashMap<>();
    private final List<Rectangle> currentTexturePlacedRectangles = new ArrayList<>();
    private Pixmap currentPixmap;
    private Texture currentTexture;
    private PixmapTextureData currentTextureData;
    public static final Pattern H5L_IMAGE_PATTERN = Pattern.compile("^(http(?:s)?://.*/)([a-zA-Z0-9-_]+\\.h5l)((?:\\.9)?\\.(?:jpg|jpeg|png|gif))$");

    public DownloadingResourceLoader(TextureAtlas atlas) {
        this(atlas, "", true);
    }

    public DownloadingResourceLoader(TextureAtlas atlas, String baseUrl, boolean allowFullUrls) {
        super(atlas);
        this.allowFullUrls = allowFullUrls;
        if(!isFullUrl(baseUrl) && !allowFullUrls) {
            throw new IllegalStateException("You need to provide a non-empty baseUrl");
        }
        this.baseUrl = baseUrl.isEmpty() || baseUrl.endsWith("/") ? baseUrl : baseUrl + "/";
    }

    public static DownloadingSprite getDownloadingSprite(SpriteType spriteType) {
        if(!(spriteType.getImage() instanceof H5EImage)) {
            return null;
        }
        H5EImage h5EImage = (H5EImage) spriteType.getImage();
        if(!(h5EImage.getTextureRegion() instanceof DownloadingSprite)) {
            return null;
        }
        return (DownloadingSprite)h5EImage.getTextureRegion();
    }

    private static boolean isFullUrl(String url) {
        return url.startsWith("http://") || url.startsWith("https://");
    }

    @Override
    protected Sprite getSprite(String url) {
        Sprite existingSprite = super.getSprite(url);
        if (existingSprite == null) {
            return generateAndDownloadSprite(url);
        }
        return existingSprite;
    }

    private Sprite generateAndDownloadSprite(String url) {
        final Sprite sprite = loadingSprites.get(url);
        if (sprite != null) {
            return sprite;
        }
        final String fullUrl;
        if(isFullUrl(url)) {
            if(!allowFullUrls) {
                throw new IllegalStateException("Full urls are not allowed: "+url);
            }
            fullUrl = url;
        } else {
            fullUrl = baseUrl + url;
        }
        final DownloadingSprite downloadingSprite = generateDownloadingSprite(fullUrl);
        loadingSprites.put(url, downloadingSprite);

        try {
            if(downloadingSprite.getH5lUrl() != null) {
                AssetLoader.getInstance().downloadFileAsString(downloadingSprite.getH5lUrl(), (content) -> {
                    final H5EResourceManager rm = getResourceManager();
                    if(rm == null) {
                        setH5lToFailed(downloadingSprite);
                        return;
                    }
                    try {
                        rm.readLibraryFile(content, downloadingSprite.getImageBaseName(), fullUrl);
                    } catch (Throwable ex) {
                        log.error("Error loading h5l-data from url " + downloadingSprite.getH5lUrl() + ":\n" + content, ex);
                        setH5lToFailed(downloadingSprite);
                        return;
                    }
                    downloadingSprite.setH5lDownloadState(DownloadingSprite.DownloadState.LOADED);

                    Hooks.call(rm.onDownloadableSpriteUpdated, downloadingSprite.getH5lUrl());
                }, (throwable) -> {
                    log.error("Error loading h5l-data from url "+downloadingSprite.getH5lUrl(), throwable);
                    setH5lToFailed(downloadingSprite);
                });
            }
            AssetLoader.getInstance().downloadPixmap(fullUrl,
                (pixmap) -> {
                    if (pixmap.getWidth() > TEXTURE_WIDTH || pixmap.getHeight() > TEXTURE_HEIGHT) {
                        log.error("Unable to fit image " + fullUrl + " with size " + pixmap.getWidth() + ", " + pixmap.getHeight());
                        setToFailed(downloadingSprite);
                        return;
                    }
                    Rectangle rectangle = findRectangle(pixmap.getWidth(), pixmap.getHeight());
                    if (rectangle == null) {
                        closeTexture();
                        rectangle = findRectangle(pixmap.getWidth(), pixmap.getHeight());
                        if (rectangle == null) {
                            throw new IllegalStateException("This should not happen");
                        }
                    }
                    currentPixmap.drawPixmap(pixmap, (int) rectangle.x, (int) rectangle.y);
                    currentTexture.load(currentTextureData);
                    currentTexturePlacedRectangles.add(rectangle);
                    downloadingSprite.setTexture(currentTexture);
                    downloadingSprite.setRegion((int) rectangle.x, (int) rectangle.y, (int) rectangle.width, (int) rectangle.height);
                    downloadingSprite.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                    downloadingSprite.setDownloadState(DownloadingSprite.DownloadState.LOADED);
                    loadingSprites.remove(url);

                    H5EResourceManager rm = getResourceManager();
                    if (rm != null) {
                        Hooks.call(rm.onDownloadableSpriteUpdated, downloadingSprite.getUrl());
                    }
                },
                (ex) -> {
                    // This is a hack to make GWT compile the project,
                    // since it doesn't have a MalformedURLException class built in
                    if (ex != null && ex.getClass().getSimpleName().equals("MalformedURLException")) {
                        log.error("Error downloading " + fullUrl + ". Malformed URL.");
                    } else {
                        log.error("Error downloading " + fullUrl + ": " + ex);
                    }
                    setToFailed(downloadingSprite);
                    loadingSprites.remove(url);
                }
            );
        } catch (Throwable ex) {
            log.error("Error starting download " + fullUrl, ex);
            setToFailed(downloadingSprite);
            loadingSprites.remove(url);
        }

        return downloadingSprite;
    }

    private void setToFailed(DownloadingSprite downloadingSprite) {
        downloadingSprite.setToSprite(super.getSprite(UNKNOWN_ICON_KEY));
        downloadingSprite.setDownloadState(DownloadingSprite.DownloadState.FAILED);
    }

    private void setH5lToFailed(DownloadingSprite downloadingSprite) {
        downloadingSprite.setToSprite(super.getSprite(UNKNOWN_ICON_KEY));
        downloadingSprite.setH5lDownloadState(DownloadingSprite.DownloadState.FAILED);
    }

    private void closeTexture() {
        if (currentTexture == null) {
            return;
        }
        currentPixmap.dispose();
        currentTexture = null;
        currentTextureData = null;
        currentPixmap = null;
        for (Rectangle rectangle : currentTexturePlacedRectangles) {
            Pools.free(rectangle);
        }
        currentTexturePlacedRectangles.clear();
    }

    private Rectangle findRectangle(int width, int height) {
        setupTexture();
        if (currentTexturePlacedRectangles.isEmpty()) {
            final Rectangle rectangle = Pools.obtain(Rectangle.class);
            rectangle.set(0, 0, width, height);
            return rectangle;
        }
        int maxWidth = 0;
        for (Rectangle rectangle : currentTexturePlacedRectangles) {
            maxWidth = Math.max(maxWidth, (int) (rectangle.x + rectangle.width));
        }
        if (maxWidth + width <= TEXTURE_WIDTH) {
            final Rectangle rectangle = Pools.obtain(Rectangle.class);
            rectangle.set(maxWidth, 0, width, height);
            return rectangle;
        }
        return null;
    }

    private DownloadingSprite generateDownloadingSprite(String url) {
        final Sprite defaultSprite = getSprite(getLoadingSpriteKey());
        final DownloadingSprite downloadingSprite = new DownloadingSprite(defaultSprite);
        downloadingSprite.setUrl(url);
        downloadingSprite.setDownloadState(DownloadingSprite.DownloadState.DOWNLOADING);
        final Matcher matcher = H5L_IMAGE_PATTERN.matcher(url);
        if(matcher.matches()) {
            String baseName = matcher.group(2);
            final String h5lUrl = matcher.group(1) + baseName;
            String imageBaseName = baseName+matcher.group(3);
            downloadingSprite.setImageBaseName(imageBaseName);
            downloadingSprite.setH5lUrl(h5lUrl);
            downloadingSprite.setH5lDownloadState(DownloadingSprite.DownloadState.DOWNLOADING);
        }
        return downloadingSprite;
    }

    private String getLoadingSpriteKey() {
        return "images/GUI/ui2/preload/loading1.png";
    }

    private void setupTexture() {
        if (currentTexture != null) {
            return;
        }
        currentPixmap = new Pixmap(TEXTURE_WIDTH, TEXTURE_HEIGHT, PIXMAP_FORMAT);
        currentTextureData = new PixmapTextureData(currentPixmap, PIXMAP_FORMAT, USE_MIP_MAPS, false, true);
        currentTexture = new Texture(currentTextureData);
        currentTexture.setFilter(TEXTURE_FILTER_MIN, TEXTURE_FILTER_MAG);
    }
}
