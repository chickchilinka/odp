package com.universeprojects.html5engine.client.framework.inputs;

public class H5ECommandParams1Args<C extends H5ECommand1Args<A1>, A1> extends H5ECommandParams<C> {
    public H5ECommandParams1Args(A1 arg1) {
        super(arg1);
    }
}
