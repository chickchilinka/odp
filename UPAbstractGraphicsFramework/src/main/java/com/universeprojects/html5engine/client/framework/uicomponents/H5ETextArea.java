package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class H5ETextArea extends TextArea implements GraphicElement {

    public H5ETextArea(H5ELayer layer) {
        super("", layer.getEngine().getSkin());
        setStage(layer);
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void focus() {
        H5ELayer layer = getLayer();
        if (layer == null) {
            return;
        }
        layer.setKeyboardFocus(this);
    }

    public void unfocus() {
        H5ELayer layer = getLayer();
        if (layer == null) {
            return;
        }
        layer.setKeyboardFocus(getLayer().getRoot());
    }

    public boolean hasFocus() {
        H5ELayer layer = getLayer();
        if (layer == null) {
            return false;

        }
        return getLayer().getKeyboardFocus() == this;
    }

    public void setTypeNumber() {
        setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
    }

    private ChangeListener changeListener;

    public ChangeListener getChangeListener() {
        if (changeListener != null) {
            return changeListener;
        } else {
            for (EventListener listener : getListeners()) {
                if (listener instanceof ChangeListener) {
                    changeListener = (ChangeListener) listener;
                    break;
                }
            }
            return changeListener;
        }
    }

    @Override
    protected InputListener createInputListener() {
        return new TextAreaListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                // We want to ignore when escape is pressed in a textbox
                if (keycode == Input.Keys.ESCAPE) return false;
                if (keycode == Input.Keys.UP || keycode == Input.Keys.DOWN) {
                    if (getChangeListener() != null)
                        getChangeListener().changed(null, null);
                }
                if (keycode == Input.Keys.PAGE_UP) {
                    goHome(true);
                    Timer.schedule(new Task() {
                        @Override
                        public void run() {
                            if (getChangeListener() != null)
                                getChangeListener().changed(null, null);
                        }
                    }, 0.01f);
                } else if (keycode == Input.Keys.PAGE_DOWN) {
                    goEnd(true);
                    Timer.schedule(new Task() {
                        @Override
                        public void run() {
                            if (getChangeListener() != null)
                                getChangeListener().changed(null, null);
                        }
                    }, 0.01f);
                }
                return super.keyDown(event, keycode);
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                // We want to ignore when escape is pressed in a textbox
                if (keycode == Input.Keys.ESCAPE) return false;

                return super.keyUp(event, keycode);
            }
        };
    }
}
