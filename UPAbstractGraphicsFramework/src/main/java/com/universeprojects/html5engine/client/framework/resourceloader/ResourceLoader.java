package com.universeprojects.html5engine.client.framework.resourceloader;

import com.universeprojects.html5engine.client.framework.H5EAudio;
import com.universeprojects.html5engine.client.framework.H5EImage;

import java.util.Collection;

public interface ResourceLoader {
    void loadResources(Collection<H5EImage> imageFiles, Collection<H5EAudio> audioFiles);

    H5EImage loadSingleImage(String imageUrl);

    String getDefaultSpriteKey();
}
