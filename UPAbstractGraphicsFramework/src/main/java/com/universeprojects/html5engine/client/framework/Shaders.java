package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import java.util.LinkedHashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class Shaders {
    private static final Map<String, String> errorMessages = new LinkedHashMap<>();

    private static synchronized void initAllShaders() {
    }

    private static synchronized void readErrors(String name, ShaderProgram shaderProgram) {
        if(shaderProgram.isCompiled()) {
            return;
        }
        String message = "Shader compilation failed: " + shaderProgram.getLog();
        errorMessages.put(name, message);
    }

    public static synchronized Map<String, String> getErrorMessages() {
        initAllShaders();
        return errorMessages;
    }
}
