package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.badlogic.gdx.controllers.PovDirection;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator1Args;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EGamepadDPadBinding<C extends H5ECommand> extends H5EGamepadBinding<C> {

    int[] dpadCentered = new int[] {0,0,0,0};
    int[] dpadUp = new int[] {1,0,0,0};
    int[] dpadDown = new int[] {0,1,0,0};
    int[] dpadLeft = new int[] {0,0,1,0};
    int[] dpadRight = new int[] {0,0,0,1};
    int[] dpadTopLeft = new int[] {1,0,1,0};
    int[] dpadTopRight = new int[] {1,0,0,1};
    int[] dpadBottomLeft = new int[] {0,1,1,0};
    int[] dpadBottomRight = new int[] {0,1,0,1};

    int[] currentDpad = dpadCentered;

    public final int buttonCode;
    private final H5ECommandInputTranslator1Args<Boolean, C> translator;

    public H5EGamepadDPadBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator1Args<Boolean, C> translator, C command, int gamepadIndex, int buttonCode) {
        super(controlSetup, command, gamepadIndex);
        this.translator = translator;
        this.buttonCode = buttonCode;
    }

    @Override
    public void doTickWhenActive() {
        PovDirection value = gamePad.getDPad(0);

        int[] newDpad = null;
        if (value == PovDirection.north) {
            newDpad = dpadUp;
        }
        if (value == PovDirection.south) {
            newDpad = dpadDown;
        }
        if (value == PovDirection.west) {
            newDpad = dpadLeft;
        }
        if (value == PovDirection.east) {
            newDpad = dpadRight;
        }
        if (value == PovDirection.northWest) {
            newDpad = dpadTopLeft;
        }
        if (value == PovDirection.northEast) {
            newDpad = dpadTopRight;
        }
        if (value == PovDirection.southWest) {
            newDpad = dpadBottomLeft;
        }
        if (value == PovDirection.southEast) {
            newDpad = dpadBottomRight;
        }
        if (value == PovDirection.center) {
            newDpad = dpadCentered;
        }

        for(int i = 0; i<4; i++) {
            if (currentDpad[i] != newDpad[i] && i+12 == buttonCode) {
                if (newDpad[i] == 1) processEvent(true);
                if (newDpad[i] == 0) processEvent(false);
            }
        }

        currentDpad = newDpad;
    }

    public void processEvent(boolean on) {
        fire(translator.generateParams(command, on));
    }
}