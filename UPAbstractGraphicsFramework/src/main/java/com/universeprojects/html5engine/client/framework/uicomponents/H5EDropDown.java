package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.List;

public class H5EDropDown extends SelectBox<String> implements GraphicElement {

	public H5EDropDown (List<String> options, H5ELayer layer) {
		super(layer.getEngine().getSkin());
		setStage(layer);
		this.setItems(options.toArray(new String[0]));
	}

	@Override
	public H5ELayer getLayer() {
		return (H5ELayer)getStage();
	}

	@Override
	public H5EEngine getEngine() {
		return getLayer().getEngine();
	}

	public void focus() {
		getLayer().setKeyboardFocus(this);
	}

	public void unfocus() {
		getLayer().setKeyboardFocus(getLayer().getRoot());
	}

	public boolean hasFocus() {
		return getLayer().getKeyboardFocus() == this;
	}

	public void addDropDownTextHandler(Callable1Args<String> callable) {
		addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (isVisible()) {
					callable.call(getSelected());
				}

				event.handle();
			}
		});
	}

	public void addDropDownIndexHandler(Callable1Args<Integer> callable) {
		addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (isVisible()) {
					callable.call(getSelectedIndex());
				}

				event.handle();
			}
		});
	}
}
