package com.universeprojects.html5engine.client.framework.resourceloader;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.hooks.Hook;
import com.universeprojects.common.shared.hooks.Hooks;

public class DownloadingSprite extends Sprite {
    private String url;
    private String h5lUrl;
    private String imageBaseName;

    public enum DownloadState {
        DOWNLOADING, LOADED, FAILED
    }

    private Hook<Callable1Args<DownloadState>> loadStateHook;

    private DownloadState downloadState;
    private DownloadState h5lDownloadState;

    public DownloadingSprite(Sprite sprite) {
        super(sprite);
    }

    public synchronized void subscribeToLoadStateChange(Callable1Args<DownloadState> onLoadDone) {
        if(isLoaded()) {
            onLoadDone.call(downloadState);
            return;
        }
        if(loadStateHook == null) {
            loadStateHook = Hooks.createHook1Args();
        }
        Hooks.subscribe(loadStateHook, onLoadDone);
    }

    public void setToSprite(Sprite sprite) {
        setTexture(sprite.getTexture());
        setRegion(sprite.getRegionX(), sprite.getRegionY(), sprite.getRegionWidth(), sprite.getRegionHeight());
        setBounds(sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());
    }

    public boolean isLoaded() {
        return !isLoading();
    }

    public boolean isLoading() {
        return downloadState == DownloadState.DOWNLOADING || (h5lUrl != null && (h5lDownloadState ==  null || h5lDownloadState == DownloadState.DOWNLOADING));
    }

    public String getImageBaseName() {
        return imageBaseName;
    }

    public String getUrl() {
        return url;
    }

    public DownloadState getDownloadState() {
        return downloadState;
    }

    synchronized void setDownloadState(DownloadState state) {
        this.downloadState = state;
        callLoadStateHook();
    }

    public DownloadState getH5lDownloadState() {
        return h5lDownloadState;
    }

    synchronized void setH5lDownloadState(DownloadState h5lDownloadState) {
        this.h5lDownloadState = h5lDownloadState;
        callLoadStateHook();
    }

    private synchronized void callLoadStateHook() {
        if(loadStateHook == null) {
            return;
        }
        if(!isLoading()) {
            callHook();
            loadStateHook = null;
        }
    }

    private void callHook() {
        Hooks.call(loadStateHook, downloadState);
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getH5lUrl() {
        return h5lUrl;
    }

    public void setH5lUrl(String h5lUrl) {
        this.h5lUrl = h5lUrl;
    }

    public void setImageBaseName(String imageBaseName) {
        this.imageBaseName = imageBaseName;
    }
}
