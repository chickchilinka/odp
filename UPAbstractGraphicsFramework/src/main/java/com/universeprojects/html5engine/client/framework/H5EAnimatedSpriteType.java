package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.shared.abstractFramework.AnimatedSpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.Marker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class H5EAnimatedSpriteType extends H5ESpriteType implements AnimatedSpriteType {
    private Integer animationTime = null;
    /**
     * All frames (H5EAnimatedSpriteTypeFrame) that this animated sprite type object has
     */
    protected final List<H5EAnimatedSpriteTypeFrame> frames = new ArrayList<>();
    private final List<H5EAnimatedSpriteTypeFrame> frames_view = Collections.unmodifiableList(frames);

    @Override
    public List<H5EAnimatedSpriteTypeFrame> getFrames() {
        return frames_view;
    }

    public H5EAnimatedSpriteType(H5EResourceManager resourceManager, String animatedSpriteTypeKey) {
        super(resourceManager, animatedSpriteTypeKey);
    }

    /**
     * Appends a new frame to this animation using the given spriteTypeKey and returns the resulting frame for further
     * editing.
     *
     * @param spriteTypeKey    Sprite type key that is used to reference a spriteType object.
     * @param durationInMillis OPTIONAL - The duration will default to 50 milliseconds if this field is not given.
     *                         Setting this parameter will set the duration that the new frame will play (in milliseconds).
     * @return The newly created H5EAnimatedSpriteTypeFrame
     */
    @Override
    public H5EAnimatedSpriteTypeFrame newFrame(String spriteTypeKey, int durationInMillis) {
        H5EAnimatedSpriteTypeFrame frame = new H5EAnimatedSpriteTypeFrame((H5EResourceManager) resourceManager, spriteTypeKey, durationInMillis);
        frames.add(frame);
        return frame;
    }

    /**
     * Moves the frame at the given index to a new index (newIndex) in the frames array. If the newIndex is out of
     * bounds, it will be forced to the closest bound. If index is out of bounds, an exception will be thrown.
     *
     * @param index    The index of the frame you wish to move
     * @param newIndex The new index the frame you wish to move will now have
     */
    @Override
    public void reorderFrame(int index, int newIndex) {
        if (newIndex < 0) {
            newIndex = 0;
        }
        if (newIndex > frames.size() - 1) {
            newIndex = frames.size() - 1;
        }
        if (index < 0 || index > frames.size() - 1) {
            throw new RuntimeException("Index out of bounds when attempting to reorder an animation frame");
        }

        H5EAnimatedSpriteTypeFrame frameToMove = frames.get(index);
        frames.remove(index);
        frames.add(newIndex, frameToMove);
    }

    /**
     * Removes the frame at the given index.
     * <p>
     * This method will throw an exception when the index is out of bounds.
     *
     * @param index The index of the frame you wish to remove
     */
    @Override
    public void removeFrame(int index) {
        if (index < 0 || index > frames.size() - 1) {
            throw new RuntimeException("Index out of bounds when attempting to remove an animation frame");
        }

        frames.remove(index);
    }

    @Override
    public int getFrameNumber() {
        return frames_view.size();
    }

    @Override
    public H5EAnimatedSpriteTypeFrame getFrame(int index) {
        return frames_view.get(index);
    }

    /**
     * Determines the number of milliseconds it would take to complete this animation from start to
     * finish by looking at each frame and adding the wait time in EXCLUDING the speed multiplier setting.
     */
    private int calculateAnimationTime() {
        int total = 0;
        for(H5EAnimatedSpriteTypeFrame frame:frames)
            total+=frame.getDurationMs();
        return total;
    }

    /**
     * @return The number of milliseconds it would take to complete this animation from start to finish by looking at each frame and adding the wait time in EXCLUDING the speed multiplier setting.
     */
    public int getAnimationTimeMs()
    {
        if (this.animationTime==null)
            this.animationTime = calculateAnimationTime();
        return animationTime;
    }

    @Override
    public H5EImage getImage() {
        return getPrimarySpriteType().getImage();
    }

    @Override
    public String getImageKey() {
        return getPrimarySpriteType().getImageKey();
    }

    @Override
    public TextureRegion getGraphicData() {
        return getPrimarySpriteType().getGraphicData();
    }

    @Override
    public TextureRegion getTextureRegion() {
        return getPrimarySpriteType().getTextureRegion();
    }

    @Override
    public int getAreaHeight() {
        return getPrimarySpriteType().getAreaHeight();
    }

    @Override
    public int getAreaWidth() {
        return getPrimarySpriteType().getAreaWidth();
    }

    @Override
    public int getAreaX() {
        return getPrimarySpriteType().getAreaX();
    }

    @Override
    public int getAreaY() {
        return getPrimarySpriteType().getAreaY();
    }

    @Override
    public int getHeight() {
        return getPrimarySpriteType().getHeight();
    }

    @Override
    public int getWidth() {
        return getPrimarySpriteType().getWidth();
    }

    @Override
    public void setImage(String imageKey) {
        getPrimarySpriteType().setImage(imageKey);
    }

    @Override
    public void stabilize() {
        getPrimarySpriteType().stabilize();
    }

    @Override
    public Marker getMarker(int index) {
        return getPrimarySpriteType().getMarker(index);
    }

    @Override
    public List<Marker> getMarkers() {
        return getPrimarySpriteType().getMarkers();
    }

    public H5ESpriteType getPrimarySpriteType() {
        H5ESpriteType spriteType = frames.get(0).getSpriteType();
        Dev.check(spriteType != this, "Sprite "+getSpriteTypeKey()+" references itself as frame");
        return spriteType;
    }
}
