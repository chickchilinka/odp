package com.universeprojects.log.shared;

import com.universeprojects.common.shared.log.LogLevel;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.log.LoggerConfig;
import com.universeprojects.common.shared.log.LoggerConfigManager;
import com.universeprojects.common.shared.log.LoggerFactory;
import com.universeprojects.common.shared.log.MarkerFactory;
import com.universeprojects.common.shared.log.MarkerFilter;
import com.universeprojects.common.shared.log.MarkerFilterType;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;


public class UPLoggerConfigManager extends LoggerConfigManager {

    private final LoggerConfig rootLoggerConfig = new LoggerConfig(LoggerFactory.ROOT_LOGGER_NAME);
    private final Map<String, LoggerConfig> loggerConfigMap = new TreeMap<>(new MarkerNameComparator());
    private final Map<String, MarkerFilter> markerFilterMap = new TreeMap<>();


    public UPLoggerConfigManager() {
        rootLoggerConfig.setMinLevel(LogLevel.INFO);
        loggerConfigMap.put(LoggerFactory.ROOT_LOGGER_NAME, rootLoggerConfig);
    }

    @Override
    public LoggerConfig findLoggerConfig(String name) {
        synchronized (loggerConfigMap) {
            for (Map.Entry<String, LoggerConfig> entry : loggerConfigMap.entrySet()) {
                if (name.startsWith(entry.getKey())) {
                    return entry.getValue();
                }
            }
        }
        return rootLoggerConfig;
    }

    @Override
    public MarkerFilter findMarkerFilter(String name) {
        synchronized (markerFilterMap) {
            return markerFilterMap.get(name);
        }
    }

    public void clearLoggerConfig() {
        synchronized (loggerConfigMap) {
            loggerConfigMap.clear();
            loggerConfigMap.put(LoggerFactory.ROOT_LOGGER_NAME, rootLoggerConfig);
            Map<String, Logger> loggers = LoggerFactory.getInstance().getLoggers();
            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (loggers) {
                for (Logger logger : loggers.values()) {
                    logger.setLoggerConfig(rootLoggerConfig);
                }
            }
        }
    }

    public void clearMarkerConfig() {
        synchronized (markerFilterMap) {
            for (MarkerFilter filter : markerFilterMap.values()) {
                filter.setFilterType(MarkerFilterType.CONTINUE);
            }
        }
    }

    protected void setMarkerFilterInternal(String name, MarkerFilterType type) {
        synchronized (markerFilterMap) {
            MarkerFilter filter = findMarkerFilter(name);
            if (filter == null) {
                if (type != null) {
                    filter = new MarkerFilter(name);
                    filter.setFilterType(type);
                    markerFilterMap.put(name, filter);
                    if (MarkerFactory.getInstance().exists(name)) {
                        MarkerFactory.getInstance().getMarker(name).setFilter(filter);
                    }
                }
            } else {
                filter.setFilterType(type);
            }
        }
    }

    protected void setLoggerConfigInternal(String name, LogLevel level) {
        synchronized (loggerConfigMap) {
            LoggerConfig config = loggerConfigMap.get(name);
            if (config != null) {
                config.setMinLevel(level);
            } else {
                config = new LoggerConfig(name);
                config.setMinLevel(level);

                loggerConfigMap.put(name, config);

                applyConfig(name, config);
            }
        }
    }

    private void applyConfig(String name, LoggerConfig config) {
        Map<String, Logger> loggers = LoggerFactory.getInstance().getLoggers();
        //noinspection SynchronizationOnLocalVariableOrMethodParameter
        synchronized (loggers) {
            for (Map.Entry<String, Logger> entry : loggers.entrySet()) {
                if (entry.getKey().equals(name)) {
                    entry.getValue().setLoggerConfig(config);
                } else if (entry.getKey().startsWith(name)) {
                    if (entry.getValue().getLoggerConfig() == null || name.length() > entry.getValue().getLoggerConfig().getName().length()) {
                        entry.getValue().setLoggerConfig(config);
                    }
                }
            }
        }
    }

    public Map<String, LoggerConfig> getLoggerConfigMap() {
        return loggerConfigMap;
    }

    public Map<String, MarkerFilter> getMarkerFilterMap() {
        return markerFilterMap;
    }

    class MarkerNameComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            if (o1.equals(o2)) {
                return 0;
            } else if (o1.equals(LoggerFactory.ROOT_LOGGER_NAME)) {
                return 1;
            } else if (o2.equals(LoggerFactory.ROOT_LOGGER_NAME)) {
                return -1;
            }
            int lengthComparison = o2.length() - o1.length();
            if (lengthComparison != 0) {
                return lengthComparison;
            }
            return o1.compareTo(o2);
        }
    }
}
